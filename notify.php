<?php

if (empty($_POST)) {
    exit;
}

$servername = "51.195.89.143:3306";
$dbname = "gjirafashop_bf";
$username = "root";
$password = "mJdaLcYUiAAq";

$order_id_with_timestamp = explode('-', $_POST['OrderID']);
$OrderID = $order_id_with_timestamp[0];
$userID = $order_id_with_timestamp[2];

$db = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$stmt = $db->prepare("INSERT INTO gjshop_notified (id, order_id, user_id, merchant_id, terminal_id, total_amount, x_id, purchase_time, trancode, insert_time)
                    VALUES (null, ?, ?, ?, ?, ?, ?, ?, ?, now())");

$stmt->bindValue(1, $OrderID, PDO::PARAM_INT);
$stmt->bindValue(2, $userID, PDO::PARAM_INT);
$stmt->bindValue(3, $_POST['MerchantID'], PDO::PARAM_INT);
$stmt->bindValue(4, $_POST['TerminalID'], PDO::PARAM_INT);
$stmt->bindValue(5, $_POST['TotalAmount'], PDO::PARAM_INT);
$stmt->bindValue(6, $_POST['XID'], PDO::PARAM_INT);
$stmt->bindValue(7, date("Y-m-d H:i:s"), PDO::PARAM_INT);
$stmt->bindValue(8, $_POST['TranCode'], PDO::PARAM_INT);

$count = $stmt->execute();

$db = null;

$TranCode = $_POST['TranCode'];
echo "MerchantID=\"" . $_POST['MerchantID'] . "\"\n";
echo "TerminalID=\"" . $_POST['TerminalID'] . "\"\n";
echo "OrderID=\"" . $_POST['OrderID'] . "\"\n";
echo "Currency=\"" . $_POST['Currency'] . "\"\n";
echo "TotalAmount=\"" . $_POST['TotalAmount'] . "\"\n";
echo "XID=\"" . $_POST['XID'] . "\"\n";
echo "PurchaseTime=\"" . $_POST['PurchaseTime'] . "\"\n";

$signatureAR = md5($OrderID . $_POST['TotalAmount'] . "ARLINDWASHEREANDFUCKUIFUDONTKNOWTHISSTRING!@#$%^");
$url = preg_replace('/www\./i', '', $_SERVER['SERVER_NAME']);

if ($TranCode == "000") {
    echo "Response.action=approve\n";
    echo "Response.reason=\n";
    echo "Response.forwardUrl=https://" . $url . "/index.php?dispatch=payment_notification.approved&payment=raiffeisenbank&awshr=" . $signatureAR;
} else {
    echo "Response.action=reverse\n";
    echo "Response.reason=\n";
    echo "Response.forwardUrl=https://" . $url . "/index.php?dispatch=payment_notification.declined&payment=raiffeisenbank";
}