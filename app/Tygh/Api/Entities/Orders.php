<?php
/***************************************************************************
 *                                                                          *
 *   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 *                                                                          *
 ****************************************************************************
 * PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
 * "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
 ****************************************************************************/

namespace Tygh\Api\Entities;

use Tygh\Api\AEntity;
use Tygh\Api\Response;
use Tygh\Registry;
use Tygh\Api\Traits\ApiHelper;

class Orders extends AEntity
{
    use ApiHelper;

    public function index($id = 0, $params = array())
    {
        if (!empty($id)) {
            $data = fn_get_order_info($id, false, false);

            if (empty($data)) {
                $status = Response::STATUS_NOT_FOUND;
            } else {
                $this->getOrderInfoCity($data);
                $this->getOrderQboName($data);
                $this->getOrderVat($data);

                $status = Response::STATUS_OK;
            }

        } else {
            $items_per_page = $this->safeGet($params, 'items_per_page', Registry::get('settings.Appearance.admin_orders_per_page'));
            list($data, $params) = fn_get_orders($params, $items_per_page);

            foreach ($data as $key => $order_info) {
                fn_display_list_price_discount($order_info, 'A');
                $data[$key]['discount'] = $order_info['discount'];
                $data[$key]['subtotal_discount'] = $order_info['subtotal_discount'];
                $additional_data = db_get_hash_single_array("SELECT type, data FROM ?:order_data WHERE order_id = ?i", array('type', 'data'), $order_info['order_id']);

                fn_get_order_items($data[$key]);

                foreach ($data[$key]['items'] as $k => $item) {
                    $extra = unserialize($item['extra']);
                    $data[$key]['items'][$k]['extra'] = $extra;
                    $data[$key]['items'][$k]['extra']['gjflex'] = false;
                    $data[$key]['items'][$k]['extra']['gjflex_fixed'] = $data[$key]['items'][$k]['extra']['gjflex_fixed'] ?: 0;

                    if (!empty($additional_data['G'])) {
                        $get_gjflex_status = current(unserialize($additional_data['G']));
                        $data[$key]['items'][$k]['extra']['gjflex'] = is_null(array_column($get_gjflex_status['products'], 'gjflex', $item['product_id'])[0]) ? false : array_column($get_gjflex_status['products'], 'gjflex', $item['product_id'])[0];

                        if ($extra['gjflex_fixed']) {
                            $data[$key]['items'][$k]['extra']['gjflex_value'] = $extra['gjflex_fixed'];
                        } else {
                            $data[$key]['items'][$k]['extra']['gjflex_value'] = 0;
                            if ($data[$key]['items'][$k]['extra']['gjflex']) {
                                $data[$key]['items'][$k]['extra']['gjflex_value'] = fn_calculate_gjflex($extra['base_price']);
                            }
                        }
                    }
                }
            }

            $data = array(
                'orders' => $data,
                'params' => $params,
            );
            $status = Response::STATUS_OK;
        }

        return array(
            'status' => $status,
            'data' => $data
        );
    }

    public function create($params)
    {
        $data = array();
        $valid_params = true;
        $status = Response::STATUS_BAD_REQUEST;
        $notify = false;
        $notification_settings = array(
            'C' => $params['notify_user'],
            'A' => $params['notify_department']
        );

        foreach ($params['user_data'] as $ud_key => $ud_value) {
            if (empty($ud_value) && $ud_key != 'b_address_2' && $ud_key != 's_address_2') {
                return array(
                    'status' => $status,
                    'data' => __('api_required_field', array(
                        '[field]' => $ud_key,
                    ))
                );
            }
        }

        if ($params['notify_user'] || $params['notify_department']) {
            $notify = true;
        }

        if (!apcu_exists('orders_api_ks_cities') || empty(apcu_fetch('orders_api_ks_cities'))) {
            $cities = db_get_array("SELECT object_id, description FROM ?:profile_field_descriptions WHERE object_type = 'V'");
            $cities = array_slice($cities, 1, 38);
            $cities = fn_array_values_recursive($cities);
            $cities = fn_array_group_by($cities, 2);
            $cities = array_flip($cities);

            apcu_add('orders_api_ks_cities', $cities);
        } else {
            $cities = apcu_fetch('orders_api_ks_cities');
        }

        $this->mapUserData($params, $cities);

        $user_id = fn_is_user_exists(0, array(
            'email' => $params['user_data']['email']
        ));

        if ($user_id) {
            $params['user_id'] = $user_id;
        } else {
            // If the user doesn't exist, register user
            $user_id = 0;
            $profile_id = 0;
            $new_user = true;

            $params['user_data']['user_type'] = 'C';
            $params['user_data']['company_id'] = fn_isAL() ? '19' : '1';
            $params['user_data']['status'] = 'A';
            $params['user_data']['is_root'] = 'N';

            $this->registerUser($user_id, $new_user, $profile_id, $params, $data, $valid_params);
        }

        $shipping_ids = array();

        if (isset($params['shipping_ids'])) {
            $shipping_ids = (array) $params['shipping_ids'];
        } elseif (isset($params['shipping_id'])) {
            $shipping_ids = (array) $params['shipping_id'];
        }

        fn_clear_cart($cart, true);
        if (!empty($params['user_id'])) {
            unset(
                $params['user_data']['password']
            );

            $cart['user_data'] = array_merge(fn_get_user_info($params['user_id']), $params['user_data']);
        } elseif (!empty($params['user_data'])) {
            $cart['user_data'] = $params['user_data'];
        }

        $cart['user_data'] = array_merge($cart['user_data'], $params);
        $cart['notes'] = $params['customer_notes'];

        if (empty($params['user_id']) && empty($params['user_data'])) {
            $data['message'] = __('api_required_field', array(
                '[field]' => 'user_id/user_data'
            ));
            $valid_params = false;

        } elseif (empty($params['payment_id'])) {
            $data['message'] = __('api_required_field', array(
                '[field]' => 'payment_id'
            ));
            $valid_params = false;
        } elseif (empty($params['shipping_id'])) {
            $data['message'] = __('api_required_field', array(
                '[field]' => 'shipping_id'
            ));
            $valid_params = false;
        }

        if (empty($shipping_ids)) {
            $data['message'] = __('api_required_field', array(
                '[field]' => 'shipping_id'
            ));
            $valid_params = false;
        }

        if ($valid_params) {

            $cart['payment_id'] = $params['payment_id'];

            $customer_auth = fn_fill_auth($cart['user_data']);

            fn_add_product_to_cart($params['products'], $cart, $customer_auth);
            fn_calculate_cart_content($cart, $customer_auth);

            if (!empty($cart['product_groups']) && !empty($shipping_ids)) {
                if (count($shipping_ids) == 1) { //back capability
                    $shipping_ids = array_fill_keys(array_keys($cart['product_groups']), reset($shipping_ids));
                }

                foreach ($cart['product_groups'] as $key => $group) {
                    foreach ($group['shippings'] as $shipping_id => $shipping) {
                        if (isset($shipping_ids[$key]) && $shipping_id == $shipping_ids[$key]) {
                            $cart['chosen_shipping'][$key] = $shipping_id;
                            break;
                        }
                    }
                }
            }

            $cart['calculate_shipping'] = true;
            fn_calculate_cart_content($cart, $customer_auth);

            if (empty($cart['shipping_failed']) || empty($shipping_ids)) {

                fn_update_payment_surcharge($cart, $customer_auth);

                $cart['custom_order_id'] = $params['order_id'];
                list($order_id, ) = fn_checkout_place_order($cart, $customer_auth, array('api' => true));

                if (!empty($order_id)) {
                    $order_info = fn_get_order_info($order_id, true);
                    fn_verify_order($order_info, 'C', null, true);

                    $status = Response::STATUS_CREATED;

                    $tag_data = array(
                        'tag_id' => 609,
                        'object_id' => $order_id,
                        'type'=> 'O'
                    );

                    db_query('INSERT INTO ?:sd_search_tag_links ?e', $tag_data);

                    if ($notify) {
                        $order_info = fn_get_order_info($order_id);
                        fn_order_notification($order_info, array(), $notification_settings);
                    }

                    $data['order_id'] = $order_id;
                }
            }
        }

        return array(
            'status' => $status,
            'data' => $data
        );
    }

    public function update($id, $params)
    {
        fn_define('ORDER_MANAGEMENT', true);
        $shipping_ids = $data = array();
        $valid_params = true;
        $status = Response::STATUS_BAD_REQUEST;

        if (isset($params['shipping_ids'])) {
            $shipping_ids = (array) $params['shipping_ids'];
        } elseif (isset($params['shipping_id'])) {
            $shipping_ids = (array) $params['shipping_id'];
        }

        if ($valid_params) {

            fn_clear_cart($cart, true);
            $customer_auth = fn_fill_auth(array(), array(), false, 'C');
            $cart_status = md5(serialize($cart));

            // Order info was not found or customer does not have enought permissions
            if (fn_form_cart($id, $cart, $customer_auth) && $cart_status != md5(serialize($cart))) {
                unset($params['product_groups']);
                $cart['calculate_shipping'] = true;

                if (empty($shipping_ids)) {
                    $shipping_ids = $cart['chosen_shipping'];
                }

                $cart['order_id'] = $id;

                if (!empty($params['products'])) {
                    $product = reset($params['products']);
                    if (isset($product['product_id'], $product['price'])) {
                        $cart['products'] = $params['products'];
                    } else {
                        $cart['products'] = array();
                        fn_add_product_to_cart($params['products'], $cart, $customer_auth);
                    }
                }

                fn_calculate_cart_content($cart, $customer_auth);

                if (!empty($params['user_id'])) {
                    $cart['user_data'] = fn_get_user_info($params['user_id']);
                } elseif (!empty($params['user_data'])) {
                    $cart['user_data'] = $params['user_data'];
                }
                $cart['user_data'] = array_merge($cart['user_data'], $params);

                if (!empty($cart['product_groups']) && !empty($shipping_ids)) {
                    if (count($shipping_ids) == 1) { //back capability
                        $shipping_ids = array_fill_keys(array_keys($cart['product_groups']), reset($shipping_ids));
                    }

                    foreach ($cart['product_groups'] as $key => $group) {
                        foreach ($group['shippings'] as $shipping_id => $shipping) {
                            if (isset($shipping_ids[$key]) && $shipping_id == $shipping_ids[$key]) {
                                $cart['chosen_shipping'][$key] = $shipping_id;
                                break;
                            }
                        }
                    }
                }

                if (!empty($params['payment_id'])) {

                    if (!empty($params['payment_info'])) {
                        $cart['payment_info'] = $params['payment_info'];
                    } elseif ($params['payment_id'] != $cart['payment_id']) {
                        $cart['payment_info'] = array();
                    }

                    $cart['payment_id'] = $params['payment_id'];
                }

                fn_calculate_cart_content($cart, $customer_auth);

                if (!empty($cart) && empty($cart['shipping_failed'])) {

                    $cart['parent_order_id'] = 0;
                    fn_update_payment_surcharge($cart, $customer_auth);

                    list($order_id, $order_status) = fn_update_order($cart, $id);

                    if ($order_id) {
                        if (!empty($params['status']) && fn_check_permissions('orders', 'update_status', 'admin')) {
                            fn_change_order_status($order_id, $params['status'], '', fn_get_notification_rules($params, false));
                        } elseif (!empty($order_status)) {
                            fn_change_order_status($order_id, $order_status, '', fn_get_notification_rules($params, false));
                        }

                        $status = Response::STATUS_OK;
                        $data = array(
                            'order_id' => $order_id
                        );
                    }
                }
            }
        }

        return array(
            'status' => $status,
            'data' => $data
        );
    }

    public function delete($id)
    {
        $data = array();
        $status = Response::STATUS_NOT_FOUND;

        if (fn_delete_order($id)) {
            $status = Response::STATUS_NO_CONTENT;
        }

        return array(
            'status' => $status,
            'data' => $data
        );
    }

    public function privileges()
    {
        return array(
            'create' => 'create_order',
            'update' => 'edit_order',
            'delete' => 'delete_orders',
            'index'  => 'view_orders'
        );
    }

    public function getOrderInfoCity(&$data) {
        $cities = $this->getStoreCities();

        $bCountryField = $data['fields'][37];
        $sCountryField = $data['fields'][38];
        $bCityField = $data['fields'][40];
        $sCityField = $data['fields'][41];

        if ($bCountryField == 115 && $sCountryField == 115) {
            $bCityField = $data['fields'][54];
            $sCityField = $data['fields'][55];
        }

        $bCity = $cities[$bCityField];
        $sCity = $cities[$sCityField];

        $data['b_city'] = $bCity;
        $data['s_city'] = $sCity;
    }

    public function getOrderQboName(&$data) {
        foreach ($data['products'] as $key => $product) {
            $data['products'][$key]['product'] = db_get_field('SELECT qbo_name FROM ?:product_descriptions WHERE product_id = ?i', $product['product_id']);
        }
    }

    public function getOrderVat(&$data) {
        $data['without_vat'] = false;

        if (in_array(236, array_column($data['tags'], 'tag_id'))) {
            $data['without_vat'] = true;
        }
    }
}
