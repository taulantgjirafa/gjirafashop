<?php

namespace Tygh\Api\Traits;

trait ApiHelper
{
	public function registerUser(&$user_id, &$new_user, &$profile_id, &$params, &$data, &$valid_params) {
		if (empty($params['user_data']['email'])) {
			$data['message'] = __('api_required_field', array(
				'[field]' => 'email'
			));
			$valid_params = false;
		}

		if (empty($params['user_data']['user_type'])) {
			$data['message'] = __('api_required_field', array(
				'[field]' => 'user_type'
			));
			$valid_params = false;
		}

		if (!isset($params['user_data']['company_id'])) {
			$data['message'] = __('api_required_field', array(
				'[field]' => 'company_id'
			));
			$valid_params = false;
		}

		if ($valid_params) {
			$params['user_data']['salt'] = fn_generate_salt();
			$params['user_data']['password'] = fn_generate_salted_password($params['user_data']['email'], $params['user_data']['salt']);

			list($user_id, $profile_id) = fn_update_user($user_id, $params['user_data'], $this->auth, false, false);

			if ($user_id) {
				$params['user_id'] = $user_id;

				$data['user_id'] = $user_id;
			}
		}
	}

	public function mapUserData(&$params, $cities) {
		$params['user_data']['b_firstname'] = $params['user_data']['b_firstname'];
		$params['user_data']['b_lastname'] = $params['user_data']['b_lastname'];
		$params['user_data']['b_address'] = $params['user_data']['b_address'];
		$params['user_data']['b_address_2'] = $params['user_data']['b_address_2'];
		$params['user_data']['b_phone'] = $params['user_data']['b_phone'];
		$params['user_data']['s_firstname'] = $params['user_data']['s_firstname'];
		$params['user_data']['s_lastname'] = $params['user_data']['s_lastname'];
		$params['user_data']['s_address'] = $params['user_data']['s_address'];
		$params['user_data']['s_address_2'] = $params['user_data']['s_address_2'];
		$params['user_data']['s_phone'] = $params['user_data']['s_phone'];
		$params['user_data']['fields'][37] = 2; // Billing country is Kosovë by default
		$params['user_data']['fields'][38] = 2; // Shipping country is Kosovë by default
		$params['user_data']['fields'][40] = $cities[$params['user_data']['b_city']] ? $cities[$params['user_data']['b_city']] : '';
		$params['user_data']['fields'][41] = $cities[$params['user_data']['s_city']] ? $cities[$params['user_data']['s_city']] : '';
		$params['user_data']['b_country'] = '';
		$params['user_data']['b_city'] = '';
		$params['user_data']['b_state'] = '';
		$params['user_data']['b_zipcode'] = '';
		$params['user_data']['s_country'] = '';
		$params['user_data']['s_city'] = '';
		$params['user_data']['s_state'] = '';
		$params['user_data']['s_zipcode'] = '';
	}

	public function getStoreCities($storeId = null) {
	    $cities = null;
	    $keyName = 'api_helper_all_cities';

	    if ($storeId) {
	        $keyName = 'api_helper_' . $storeId . '_cities';
        }

        if (!apcu_exists($keyName) || empty(apcu_fetch($keyName))) {
            $cities = db_get_array("SELECT object_id, description FROM ?:profile_field_descriptions WHERE object_type = 'V'");

            if ($storeId == 1) {
                $cities = array_slice($cities, 1, 38);
            }

            $cities = fn_array_values_recursive($cities);
            $cities = fn_array_group_by($cities, 2);

            apcu_add($keyName, $cities, 3600);
        } else {
            $cities = apcu_fetch($keyName);
        }

        return $cities;
    }
}
