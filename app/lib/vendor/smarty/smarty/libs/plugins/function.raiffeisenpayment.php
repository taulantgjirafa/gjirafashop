<?php
/**
 * Smarty plugin
 *
 * @package    Smarty
 * @subpackage PluginsFunction
 */

/**
 * Smarty {html_checkboxes} function plugin
 * File:       function.html_checkboxes.php<br>
 * Type:       function<br>
 * Name:       html_checkboxes<br>
 * Date:       24.Feb.2003<br>
 * Purpose:    Prints out a list of checkbox input types<br>
 * Examples:
 * <pre>
 * {html_checkboxes values=$ids output=$names}
 * {html_checkboxes values=$ids name='box' separator='<br>' output=$names}
 * {html_checkboxes values=$ids checked=$checked separator='<br>' output=$names}
 * </pre>
 * Params:
 * <pre>
 * - name       (optional) - string default "checkbox"
 * - values     (required) - array
 * - options    (optional) - associative array
 * - checked    (optional) - array default not set
 * - separator  (optional) - ie <br> or &nbsp;
 * - output     (optional) - the output next to each checkbox
 * - assign     (optional) - assign the output as an array to this variable
 * - escape     (optional) - escape the content (not value), defaults to true
 * </pre>
 *
 * @link       http://www.smarty.net/manual/en/language.function.html.checkboxes.php {html_checkboxes}
 *             (Smarty online manual)
 * @author     Christopher Kvarme <christopher.kvarme@flashjab.com>
 * @author     credits to Monte Ohrt <monte at ohrt dot com>
 * @version    1.0
 *
 * @param array  $params   parameters
 * @param object $template template object
 *
 * @return string
 * @uses       smarty_function_escape_special_chars()
 */
function smarty_function_raiffeisenpayment()
{
$_SESSION['user_id'];
	
	
$cart = $_SESSION['cart'];

$MerchantID = '8600058';
$TerminalID = 'E0899005';
if (isset($_REQUEST['order_id'])) {
	$OrderID = $_REQUEST['order_id'];
}
else{
	$OrderID = "";
}
$PurchaseTime = date("ymdHis") ;
//$TotalAmount = $_GET["a"];
$TotalAmount = 1;
$CurrencyID = 978;
$signature = "zE2eOwkvRsvrO2yJd2/A3tErAE6RY6GDUxwNBrIcXcP9Plxp/O0LWT2R1m93tM0G
zoNvJF1mCmjm67nCOgGzEnwArlZ1Lno0iwZQc6lBAar9CQXQyphQSI22igQqQRNf
gQgAIcNFVXo95Qh35cn/hWKNoM/Kz/wGqesRcaGEAw0=";
$data = "$MerchantID;$TerminalID;$PurchaseTime;$OrderID;$CurrencyID;$TotalAmount;;";
$fp = fopen("$MerchantID.pem", "r");
$priv_key = fread($fp, 8192);
fclose($fp);
$pkeyid = openssl_get_privatekey($priv_key);
openssl_sign( $data, $signature, $pkeyid);
openssl_free_key($pkeyid);
$b64sign = base64_encode($signature);

return '<input name="Version" type="hidden" value="1" />
<input name="MerchantID" type="hidden" value="'.$MerchantID.'" />
<input name="TerminalID" type="hidden" value="'.$TerminalID.'" />
<input name="TotalAmount" type="hidden" value="'.$TotalAmount.'" />
<input name="Currency" type="hidden" value="'.$CurrencyID.'" />
<input name="locale" type="hidden" value="RU" />
<input name="PurchaseTime" type="hidden" value="'.$PurchaseTime .'" />
<input name="OrderID" type="hidden" value="'.$OrderID.'" />
<input name="PurchaseDesc" type="hidden" value="Pershkrimi">
<input name="Signature" type="hidden" value="'.$b64sign.'"/>
<input type="submit" value="Paguaj tani"/>
';


}