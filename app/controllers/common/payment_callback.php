<?php

if (!defined('BOOTSTRAP')) {
    die('Access denied');
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $order_id_with_timestamp = explode('-', $_POST['OrderID']);
    $order_id = $order_id_with_timestamp[0];
    $user_id = $order_id_with_timestamp[2];
    $signature = md5($order_id . $_POST['TotalAmount'] . "ARLINDWASHEREANDFUCKUIFUDONTKNOWTHISSTRING!@#$%^");
    $url = preg_replace('/www\./i', '', (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]");
    $date = date('Y-m-d H:i:s');
    $data = [
        'order_id' => $order_id,
        'user_id' => $user_id,
        'merchant_id' => $_POST['MerchantID'],
        'terminal_id' => $_POST['TerminalID'],
        'total_amount' => $_POST['TotalAmount'],
        'x_id' => $_POST['XID'],
        'purchase_time' => $_POST['PurchaseTime'],
        'trancode' => $_POST['TranCode'],
        'insert_time' => $date
    ];

    db_query('INSERT INTO ?:notified ?e', $data);

    echo "MerchantID=\"" . $_POST['MerchantID'] . "\"\n";
    echo "TerminalID=\"" . $_POST['TerminalID'] . "\"\n";
    echo "OrderID=\"" . $_POST['OrderID'] . "\"\n";
    echo "Currency=\"" . $_POST['Currency'] . "\"\n";
    echo "TotalAmount=\"" . $_POST['TotalAmount'] . "\"\n";
    echo "XID=\"" . $_POST['XID'] . "\"\n";
    echo "PurchaseTime=\"" . $_POST['PurchaseTime'] . "\"\n";

    if ($_POST['TranCode'] == "000") {
        fn_log_event('orders', 'payment_status_callback', array(
            'order_id' => $order_id,
            'tags' => 'approved'
        ));

        $pp_response['order_status'] = 'O';
        $pp_response['payment_status'] = 'A';
        $pp_response['reason_text'] = __('transaction_approved');
        $pp_response['approval_code'] = $_POST['ApprovalCode'];

        fn_finish_payment($order_id, $pp_response);

        echo "Response.action=approve\n";
        echo "Response.reason=\n";
        echo "Response.forwardUrl=" . $url . "/index.php?dispatch=payment_notification.approved&payment=raiffeisenbank&awshr=" . $signature;
    } else {
        fn_log_event('orders', 'payment_status_callback', array(
            'order_id' => $order_id,
            'tags' => 'declined'
        ));

        $pp_response['order_status'] = 'F';
        $pp_response['payment_status'] = 'C';
        $pp_response['reason_text'] = __('transaction_cancelled');

        fn_finish_payment($order_id, $pp_response);

        echo "Response.action=reverse\n";
        echo "Response.reason=\n";
        echo "Response.forwardUrl=" . $url . "/index.php?dispatch=payment_notification.declined&payment=raiffeisenbank";
    }

    exit(200);
}