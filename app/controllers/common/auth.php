<?php
/***************************************************************************
 *                                                                          *
 *   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 *                                                                          *
 ****************************************************************************
 * PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
 * "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
 ****************************************************************************/

use Tygh\Development;
use Tygh\Registry;
use Tygh\Helpdesk;

if (!defined('BOOTSTRAP')) { die('Access denied'); }
 if(DEVELOPMENT && ENVIRONMENT == 'local'){ fn_login_user(1, true); } // Admin login locally
// if(DEVELOPMENT && ENVIRONMENT == 'local'){ fn_login_user(117693, true); } // VIP login locally (gaziway)
// if(DEVELOPMENT && ENVIRONMENT == 'local'){ fn_login_user(107969, true); } // Test login locally (krootaulant@gmail.com)

$ip = !empty($_SERVER['HTTP_CLIENT_IP']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if($ip == "84.22.57.102" || $ip == "46.99.178.114"){
        header('Location: https://gjirafa50.com');
    }
    //print_r($_SERVER);
    //$ip = !empty($_SERVER['HTTP_CLIENT_IP']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
    //echo $ip;

    //Tygh::$app['session']->regenerateID();
    //
    //fn_login_user(34, true);
    //fn_set_notification('N', __('notice'), __('successful_login'));
    //return array(CONTROLLER_STATUS_OK, !empty($redirect_url)? $redirect_url : fn_url());
    //exit;

    //
    // Login mode
    //
    if ($mode == 'login') {


        //return array(CONTROLLER_STATUS_OK, !empty($redirect_url)? $redirect_url : fn_url());

        $redirect_url = '';

        if (AREA != 'A') {
            if (fn_image_verification('login', $_REQUEST) == false) {
                fn_save_post_data('user_login');

                return array(CONTROLLER_STATUS_REDIRECT);
            }
        }

        fn_restore_processed_user_password($_REQUEST, $_POST);

        list($status, $user_data, $user_login, $password, $salt) = fn_auth_routines($_REQUEST, $auth);

        if (!empty($_REQUEST['redirect_url'])) {
            $redirect_url = $_REQUEST['redirect_url'];
        } else {
            $redirect_url = fn_url('auth.login' . !empty($_REQUEST['return_url']) ? '?return_url=' . $_REQUEST['return_url'] : '');
        }

        if ($status === false) {
            fn_save_post_data('user_login');

            return array(CONTROLLER_STATUS_REDIRECT, $redirect_url);
        }
        //
        // Success login
        //
        if (!empty($user_data) && !empty($password) && fn_generate_salted_password($password, $salt) == $user_data['password']) {

            // Regenerate session ID for security reasons
//            Tygh::$app['session']->regenerateID();

            //
            // If customer placed orders before login, assign these orders to this account
            //
            if (!empty($auth['order_ids'])) {
                foreach ($auth['order_ids'] as $k => $v) {
                    db_query("UPDATE ?:orders SET ?u WHERE order_id = ?i", array('user_id' => $user_data['user_id']), $v);
                }
            }

            fn_login_user($user_data['user_id'], true);

            Helpdesk::auth();

            // Set system notifications
            if (Registry::get('config.demo_mode') != true && AREA == 'A') {
                // If username equals to the password
                if (!fn_is_development() && fn_compare_login_password($user_data, $password)) {
                    $lang_var = 'warning_insecure_password_email';

                    fn_set_notification('E', __('warning'), __($lang_var, array(
                        '[link]' => fn_url('profiles.update')
                    )), 'S', 'insecure_password');
                }
                if (empty($user_data['company_id']) && !empty($user_data['user_id'])) {
                    // Insecure admin script
                    if (!fn_is_development() && Registry::get('config.admin_index') == 'admin.php') {
                        fn_set_notification('E', __('warning'), __('warning_insecure_admin_script', array('[href]' => Registry::get('config.resources.admin_protection_url'))), 'S');
                    }

                    if (!fn_is_development() && is_file(Registry::get('config.dir.root') . '/install/index.php')) {
                        fn_set_notification('W', __('warning'), __('delete_install_folder'), 'S');
                    }

                    if (Development::isEnabled('compile_check')) {
                        fn_set_notification('W', __('warning'), __('warning_store_optimization_dev', array('[link]' => fn_url("themes.manage"))));
                    }

                    fn_set_hook('set_admin_notification', $user_data);
                }

            }

            if (!empty($_REQUEST['remember_me'])) {
                fn_set_session_data(AREA . '_user_id', $user_data['user_id'], COOKIE_ALIVE_TIME);
                fn_set_session_data(AREA . '_password', $user_data['password'], COOKIE_ALIVE_TIME);
            }

            if (!empty($_REQUEST['return_url'])) {
                $redirect_url = $_REQUEST['return_url'];
            }

            unset($_REQUEST['redirect_url']);

            if (AREA == 'C') {
                fn_set_notification('N', __('notice'), __('successful_login'));
            }

            if (AREA == 'A' && Registry::get('runtime.unsupported_browser')) {
                $redirect_url = "upgrade_center.ie7notify";
            }

            unset(Tygh::$app['session']['cart']['edit_step']);

        } else {

            //Login incorrect
            $user_login .= ';' . $user_data['password'] . ';' . $password . ';' . $salt;
            // Log user failed login
            fn_log_event('users', 'failed_login', array (
                'user' => $user_login
            ));

            $auth = fn_fill_auth();
            fn_set_notification('E', __('error'), __('error_incorrect_login'));
            fn_save_post_data('user_login');

            return array(CONTROLLER_STATUS_REDIRECT, $redirect_url);
        }

        unset(Tygh::$app['session']['edit_step']);
    }
//    else {
//        // Login user failed login
//        fn_log_event('users', 'failed_login', array (
//           'user' => $user_login
//        ));
//
//        //$auth = fn_fill_auth();
//        if (!empty($_REQUEST['return_url'])) {
//            $redirect_url = $_REQUEST['return_url'];
//        }
//
//        fn_set_notification('E', __('error'), __('error_incorrect_login'));
//        //fn_save_post_data('user_login');
//
//        return array(CONTROLLER_STATUS_REDIRECT, $redirect_url);
//    }

    //
    // Recover password mode
    //
    if ($mode == 'recover_password') {
        if (AREA == 'C') {
            header('Location: ' . Registry::get('config.sso.provider_url'));
            exit;
        }

        $user_email = !empty($_REQUEST['user_email']) ? $_REQUEST['user_email'] : '';
        $redirect_url = '';
        if (!fn_recover_password_generate_key($user_email)) {
            $redirect_url = "auth.recover_password";
        }
    }

    //
    // Change expired password
    //
    if ($mode == 'password_change') {
        if (AREA == 'C') {
            header('Location: ' . Registry::get('config.sso.provider_url'));
            exit;
        }

        fn_restore_processed_user_password($_REQUEST['user_data'], $_POST['user_data']);

        if (fn_update_user($auth['user_id'], $_REQUEST['user_data'], $auth, false, true)) {
            $redirect_url = !empty($_REQUEST['return_url']) ? $_REQUEST['return_url'] : '';
        } else {
            $redirect_url = 'auth.password_change';
            if (!empty($_REQUEST['return_url'])) {
                $redirect_url .= '?return_url=' . urlencode($_REQUEST['return_url']);
            }
        }
    }

    return array(CONTROLLER_STATUS_OK, !empty($redirect_url)? $redirect_url : fn_url());
}


if($_SERVER['REQUEST_METHOD'] == 'GET' && !empty($_GET['hash'])){

    if ($mode == 'login' && !empty($_GET['hash']) && !empty($_GET['dawsh'])) {

        $requestHash = "";
        try {
            $requestHash = fn_decrypt_data(rawurldecode(urlencode($_GET['hash'])));
        } catch (Exception $e) {
            $error = array();
            $error['Error: '] = "Ska mujt me decrypt hash ne log in!";
            $error['Hash'] = $_GET['hash'];
            $error['Error Message: '] = $e->getMessage();
            fn_send_email_error($error);
        }

        $hash_dawsh = strtoupper(md5($requestHash[0] . $requestHash[1] . "gjirafa.comarlindidrogafestaagonix2gaziAWSHR!@#$%)(*&^"));

        if($hash_dawsh != $_GET['dawsh']){
            echo "<script>parent.postMessage('false','*');</script>";
        }else if(count($requestHash) <= 5){
            $row = db_get_array("SELECT * FROM ?:users WHERE email = ?s", $requestHash[0]);
            $redirect_url = "";

            if(count($row) == 0){
                $id = registerGjirafaUser($requestHash);
                fn_sso_gjirafa_login($id);
            }else if(count($row) > 0){
                $compare_passwords = fn_compare_passwords($row[0]['password'], $row[0]['salt'], $requestHash[1], $requestHash[0]);
                if($compare_passwords)
                    fn_sso_gjirafa_login($row[0]['user_id']);
            }

            if(!empty($_GET['sf_redirect'])){
                if(!check_url_param($_GET['sf_redirect']))
                    $redir = 'https://gjirafa.com';
                else
                    $redir = $_GET['sf_redirect'];

                header("Location: $redir");
                exit;
            }
            echo "<script>parent.postMessage('true','*');</script>";
        }
        exit;
    }
}

//https://gjirafa50.com/index.php?dispatch=auth.login&hash=b29vb29vKSgnJiUkb29vYGJWIV9cVGBaM1RtWGNlWGdXYVxfZVQ%3d&dawsh=DE98BEB66178B09DC54CDEC6055C9F1B
//
// Perform user log out
//
if ($mode == 'logout') {
    fn_user_logout($auth, $_REQUEST['redirect_url']);

    return array(CONTROLLER_STATUS_OK, fn_url());
}

if($mode == 'check_cookie'){
    $redirect_to = $_GET['sf_redirect'];
    if(!empty($redirect_to)){
        if(!check_url_param($redirect_to))
            $redirect_to = 'https://gjirafa.com';
    }

    if($_COOKIE['safari_browser'] == '1'){
        header('Location: https://gjirafa50.com/index.php?dispatch=auth.login&hash=' . urlencode($_GET['hash']) . '&dawsh=' . urlencode($_GET['dawsh']) . '&sf_redirect=' . urlencode($redirect_to));
    }else{
        echo 'Ka ndodhur një gabim!';
    }
    exit;
}

if($mode == 'set_cookie'){
    $redirect_to = $_GET['sf_redirect'];
    if(!empty($redirect_to)){
        if(!check_url_param($redirect_to))
            $redirect_to = 'https://gjirafa.com';
    }

    if(empty($_COOKIE['safari_browser']))
        setcookie('safari_browser', '1');
    header('Location: https://gjirafa50.com/index.php?dispatch=auth.check_cookie&hash=' . urlencode($_GET['hash']) . '&dawsh=' . urlencode($_GET['dawsh']) . '&sf_redirect=' . urlencode($redirect_to));
    exit;
}

//
// Recover password mode
//
if ($mode == 'recover_password') {
    if (AREA == 'C') {
        header('Location: ' . Registry::get('config.sso.provider_url'));
        exit;
    }

    $ekey = !empty($_REQUEST['ekey']) ? $_REQUEST['ekey'] : '';
    $redirect_url = '';
    $result = fn_recover_password_login($ekey);

    if (!is_null($result)) {
        if ($result === LOGIN_STATUS_USER_NOT_FOUND || $result === LOGIN_STATUS_USER_DISABLED) {
            $redirect_url = fn_url();
        } elseif ($result === false) {
            $redirect_url = 'auth.recover_password';
        } else {
            $redirect_url = "profiles.update?user_id=$result";
        }
    }

    if ($redirect_url) {
        return array(CONTROLLER_STATUS_OK, $redirect_url);
    }

    if (AREA != 'A') {
        fn_add_breadcrumb(__('recover_password'));
    }
    Tygh::$app['view']->assign('view_mode', 'simple');
}

if ($mode == 'ekey_login') {

    $ekey = !empty($_REQUEST['ekey']) ? $_REQUEST['ekey'] : '';
    $redirect_url = fn_url();
    $result = fn_recover_password_login($ekey);

    if (!is_null($result)) {
        if ($result === LOGIN_STATUS_USER_NOT_FOUND || $result === LOGIN_STATUS_USER_DISABLED) {
            $redirect_url = fn_url();
        } elseif ($result === false) {
            $redirect_url = fn_url();
        } else {
            fn_delete_notification('notice_text_change_password');

            if (!empty($_REQUEST['redirect_url'])) {
                $redirect_url = $_REQUEST['redirect_url'];

                if (strpos($redirect_url, '://') === false) {
                    $redirect_url = 'http://' . $redirect_url;
                }
            } else {
                $redirect_url = fn_url();
            }
        }
    }

    fn_redirect($redirect_url, true);
}

//
// Display login form in the mainbox
//
//if ($mode == 'login_form'  && $ip == "84.22.57.102") {
if ($mode == 'login_form') {
    if (defined('AJAX_REQUEST') && empty($auth)) {
        exit;
    }

    if (!empty($auth['user_id'])) {
        return array(CONTROLLER_STATUS_REDIRECT, fn_url());
    }

    $stored_user_login = fn_restore_post_data('user_login');
    if (!empty($stored_user_login)) {
        Tygh::$app['view']->assign('stored_user_login', $stored_user_login);
    }

    if (AREA != 'A') {
        fn_add_breadcrumb(__('sign_in'));
    }

    Tygh::$app['view']->assign('view_mode', 'simple');

} elseif ($mode == 'password_change' && AREA == 'A') {
    if (defined('AJAX_REQUEST') && empty($auth)) {
        exit;
    }

    if (empty($auth['user_id'])) {
        return array(CONTROLLER_STATUS_REDIRECT, fn_url());
    }

    $profile_id = 0;
    $user_data = fn_get_user_info($auth['user_id'], true, $profile_id);

    Tygh::$app['view']->assign('user_data', $user_data);
    Tygh::$app['view']->assign('view_mode', 'simple');

} elseif ($mode == 'change_login') {
    $auth = Tygh::$app['session']['auth'];

    if (!empty($auth['user_id'])) {
        fn_log_user_logout($auth);
    }

    unset(Tygh::$app['session']['cart']['user_data']);
    fn_login_user();

    fn_delete_session_data(AREA . '_user_id', AREA . '_password');

    return array(CONTROLLER_STATUS_OK, 'checkout.checkout');
}

if($mode == 'force_login'){
    $init_user = new gjirafa_auth_new();
    $init_user->initiate_force_login();
    $urlRedirect = $init_user->GetRedirection();
    if($urlRedirect != '')
    {
        header('Location: ' . $urlRedirect);
        exit();
    }
}

if($mode == 'force_logout'){
    $init_user = new gjirafa_auth_new();
    $init_user->initiate_force_logout();
    $urlRedirect = $init_user->GetRedirection();
    if($urlRedirect != '')
    {
        header('Location: ' . $urlRedirect);
        exit();
    }
}

function check_url_param($url){
    try{
        $url_obj = parse_url($url);
        $check_sub = explode(".", $url_obj['host']);

        if(count($check_sub) >= 2)
            $url_obj['host'] = $check_sub[count($check_sub)-2] . "." . $check_sub[count($check_sub)-1];

        if ($url_obj['host'] === 'gjirafa.com' || $url_obj['host'] === 'gjirafa.biz' || $url_obj['host'] === 'gjirafa50.com')
            return true;
        else
            return false;
    }catch(Exception $e){
        return false;
    }
}

function registerGjirafaUser($request_user_data){

    $user_data['is_root'] = 'N';
    $user_data['salt'] = fn_generate_salt();
    $user_data['password'] = fn_generate_salted_password($request_user_data[1], $user_data['salt']);
    $user_data['lang_code'] = !empty($user_data['lang_code']) ? $user_data['lang_code'] : CART_LANGUAGE;
    $user_data['timestamp'] = TIME;
    $user_data['company_id'] = Registry::ifGet('runtime.company_id', 1);
    $user_data['email'] = $request_user_data[0];
    $user_data['firstname'] = $request_user_data[2];
    $user_data['lastname'] = $request_user_data[3];

    $user_id = db_query("INSERT INTO ?:users ?e" , $user_data);

    if (empty($user_data['user_login'])) { // if we're using email as login or user type does not require login, fill login field
        db_query("UPDATE ?:users SET user_login = 'user_?i' WHERE user_id = ?i AND user_login = ''", $user_id, $user_id);
    }


    db_query("INSERT INTO ?:user_profiles (user_id, profile_type) values ('$user_id','P')");
    if(!empty(Tygh::$app['session']['cart'])){
        fn_save_cart_content(Tygh::$app['session']['cart'], $user_id);
    }
    //fn_update_user(0, $_REQUEST, $auth, !empty($_REQUEST['ship_to_another']), !empty($_REQUEST['notify_customer']), $send_password);

    return $user_id;
}

//function registerGjirafaUser(){
//
//	$salt = fn_generate_salt($_REQUEST['password']);
//	$send_password = fn_generate_salted_password($_REQUEST['password'], $salt);
//	
//	$user_data['is_root'] = 'N';
//	$user_data['salt'] = fn_generate_salt();
//	$user_data['password'] = fn_generate_salted_password($_REQUEST['password'], $user_data['salt']);
//	$user_data['lang_code'] = !empty($user_data['lang_code']) ? $user_data['lang_code'] : CART_LANGUAGE;
//	$user_data['timestamp'] = TIME;
//	$user_data['company_id'] = Registry::ifGet('runtime.company_id', 1);
//	$user_data['email'] = $_REQUEST['user_login'];
//	
//	$user_id = db_query("INSERT INTO ?:users ?e" , $user_data);
//	
//	if (empty($user_data['user_login'])) { // if we're using email as login or user type does not require login, fill login field
//		db_query("UPDATE ?:users SET user_login = 'user_?i' WHERE user_id = ?i AND user_login = ''", $user_id, $user_id);
//	}
//
//
//	db_query("INSERT INTO ?:user_profiles (user_id, profile_type) values ('$user_id','P')");
//	
//	//fn_update_user(0, $_REQUEST, $auth, !empty($_REQUEST['ship_to_another']), !empty($_REQUEST['notify_customer']), $send_password);
//
//	return true;
//}

function fn_compare_passwords($db_password, $salt, $request_password, $user_id){
    $salted_request_password = fn_generate_salted_password($request_password, $salt);

    if($db_password == $salted_request_password)
        return true;
    else
        fn_update_password_on_login($user_id, $request_password);
}

function fn_update_password_on_login($user_id, $password){
    $salt = fn_generate_salt();
    $new_password = fn_generate_salted_password($password, $salt);
    $data = array (
        'password' => $new_password,
        'salt' => $salt
    );

    $a = db_query('UPDATE ?:users SET ?u WHERE email = ?s', $data, $user_id);

    if($a > 0){
        return true;
    }
    return false;

}

function fn_login_user_gjirafa($user,$password){

    $url = 'http://1ee501ab50bd4a9091a4f49f02266da8.cloudapp.net/Account/checkUser';
    $fields = array(
        'user_login' => $user,
        'password' => $password,
    );
    $fields_string = "";
//url-ify the data for the POST
    foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
    rtrim($fields_string, '&');

//open connection
    $ch = curl_init();
//set the url, number of POST vars, POST data
    curl_setopt($ch,CURLOPT_URL, $url);
    curl_setopt($ch,CURLOPT_POST, count($fields));
    curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER, TRUE);

//execute post
    $result = curl_exec($ch);
    if($result === 'True')
        $res = true;
    else
        $res = false;
//close connection
    curl_close($ch);
    return $res;
}

function fn_decrypt_data($r_data){
    $data = $r_data;
    $decoded = base64_decode ($data);
    $reverse = strrev($decoded);
    $charAsInt = ord('a');
    $thisWordCodeVerdeeld = str_split($reverse);
    $input = '';
    for ($i=0; $i<count($thisWordCodeVerdeeld); $i++) {
        $charAsInt = ord($thisWordCodeVerdeeld[$i]);
        $charAsInt += 13;
        $input .= chr($charAsInt);
    }

    return (explode('|||', $input));
}

function fn_sso_gjirafa_login($user_id){
    fn_login_user($user_id);
    fn_set_notification('N', __('notice'), __('successful_login'));

}

