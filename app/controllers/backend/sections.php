<?php
/***************************************************************************
 *                                                                          *
 *   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 *                                                                          *
 ****************************************************************************
 * PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
 * "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
 ****************************************************************************/

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if ($mode == 'update') {
        db_query('DELETE FROM ?:sections');

        foreach ($_REQUEST['sections'] as $key => $section) {
            $data = array(
                'section_id' => $section['id'],
                'section_title' => $section['title'],
                'section_description' => $section['description'],
                'section_products' => $section['products'],
                'section_sub' => $section['sub']
            );

            db_query('REPLACE INTO ?:sections (section_id, data) VALUES (?i, ?s)', $key, serialize($data));
        }
    }

    return array(CONTROLLER_STATUS_REDIRECT, 'sections.manage');
}

if ($mode == 'manage') {
    $sections = db_get_array('SELECT * FROM ?:sections');

    Tygh::$app['view']->assign('page_title', 'Seksionet');
    Tygh::$app['view']->assign('section_1_data', unserialize($sections[0]['data']));
    Tygh::$app['view']->assign('section_2_data', unserialize($sections[1]['data']));
    Tygh::$app['view']->assign('section_3_data', unserialize($sections[2]['data']));
    Tygh::$app['view']->assign('section_4_data', unserialize($sections[3]['data']));
}
