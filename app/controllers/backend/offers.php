<?php
/***************************************************************************
 *                                                                          *
 *   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 *                                                                          *
 ****************************************************************************
 * PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
 * "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
 ****************************************************************************/

use Tygh\Registry;


if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($mode == 'create') {
    //Tygh::$app['view']->assign('logs', "ASDASD");
}

if ($mode == 'preview') {
    $product_ids = get_product_ids(explode(",", $_REQUEST['product_code']));
    if($product_ids == null){
        Tygh::$app['view']->assign('products', null);
        return;
    }
    $products = array();
    foreach($product_ids as $pid){
        $product = fn_get_product_data(
            $pid['product_id'],
            $auth,
            CART_LANGUAGE,
            '',
            true,
            true,
            true,
            true,
            fn_is_preview_action($auth, $_REQUEST),
            true,
            false,
            true
        );
        fn_gather_additional_product_data($product, true, true);
        fn_get_product_features_list($product);
        array_push($products, $product);
    }
    Tygh::$app['view']->assign('products', $products);
}
