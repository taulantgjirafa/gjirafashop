<?php

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

$months = array(
    '1' => 'Janar',
    '2' => 'Shkurt',
    '3' => 'Mars',
    '4' => 'Prill',
    '5' => 'Maj',
    '6' => 'Qershor',
    '7' => 'Korrik',
    '8' => 'Gusht',
    '9' => 'Shtator',
    '10' => 'Tetor',
    '11' => 'Nentor',
    '12' => 'Dhjetor'
);

$years = array();
for ($year = 2016; $year <= date('Y'); $year++) { 
    $years[$year] = $year;
}

if ($mode == 'user_sales') {
    $params = $_REQUEST;
    $results_per_page = 20;
    $results = fn_get_user_sales_reports($params);
    $total = ceil($results['total_page']/ $results_per_page);
    if (isset($params["page"])) { $page  = $params["page"]; } else { $page=1; };
    $pages = array(
        "total"=>$total,
        "current"=> $page
    );
    $statuses = db_get_array("select ?:statuses.status , description as name from ?:statuses inner join ?:status_descriptions on ?:statuses.status_id = ?:status_descriptions.status_id where type='o'");

    Tygh::$app['view']->assign('statuses', $statuses);
    Tygh::$app['view']->assign('user_sales', $results['result']);
    Tygh::$app['view']->assign('search_params', $params);
    Tygh::$app['view']->assign('pages', $pages);
}

if ($mode == 'details') {
    $user_id = $_REQUEST['user_id'];
    $user_data = db_get_array("select * from ?:users where user_id = ?i",$user_id);
    $orders = fn_get_detailed_orders($user_id);

    Tygh::$app['view']->assign('user_order_details', $orders['order_details']);
    Tygh::$app['view']->assign('user_data', $user_data);
    Tygh::$app['view']->assign('user_order_count', $orders['order_count']);
}

if ($mode == 'product_sales') {
    $params = $_REQUEST;
    $results_per_page = 20;
    $reports = fn_get_product_sales_reports($params);
    $total = ceil($reports['total_page']/ $results_per_page);
    if (isset($params["page"])) { $page  = $params["page"]; } else { $page=1; };
    $pages = array(
        "total"=>$total,
        "current"=> $page
    );
    Tygh::$app['view']->assign('search_params', $params);
    $statuses = db_get_array("select ?:statuses.status , description as name from ?:statuses inner join ?:status_descriptions on ?:statuses.status_id = ?:status_descriptions.status_id where type='o'");

    Tygh::$app['view']->assign('reports', $reports['result']);
    Tygh::$app['view']->assign('statuses', $statuses);
    Tygh::$app['view']->assign('pages', $pages);
}

if ($mode == 'user_stats') {
    $params = $_REQUEST;
    $results_per_page = 20;
    $reports = fn_get_user_stats_report($params);
    $total = ceil($reports['total_page']/ $results_per_page);
    if (isset($params["page"])) { $page  = $params["page"]; } else { $page=1; };
    $pages = array(
        "total"=>$total,
        "current"=> $page
    );
    Tygh::$app['view']->assign('search_params', $params);
    $statuses = db_get_array("select ?:statuses.status , description as name from ?:statuses inner join ?:status_descriptions on ?:statuses.status_id = ?:status_descriptions.status_id where type='o'");

    Tygh::$app['view']->assign('reports', $reports['result']);
    Tygh::$app['view']->assign('statuses', $statuses);
    Tygh::$app['view']->assign('pages', $pages);
}

if ($mode == 'monthly_users') {
    $params = $_REQUEST;
    $results_per_page = 20;
    $results = fn_get_monthly_users($params);
    $total = ceil($results['total_page'] / $results_per_page);
    if (isset($params["page"])) { $page = $params["page"]; } else { $page = 1; };

    $pages = array(
        "total" => $total,
        "current" => $page
    );

    $statuses = db_get_array("select ?:statuses.status , description as name from ?:statuses inner join ?:status_descriptions on ?:statuses.status_id = ?:status_descriptions.status_id where type='o'");

    Tygh::$app['view']->assign('statuses', $statuses);
    Tygh::$app['view']->assign('monthly_users', $results['result']);
    Tygh::$app['view']->assign('search_params', $params);
    Tygh::$app['view']->assign('pages', $pages);
    Tygh::$app['view']->assign('months', $months);
    Tygh::$app['view']->assign('years', $years);
}

if ($mode == 'monthly_sales') {
    $params = $_REQUEST;
    $results_per_page = 20;
    $results = fn_get_monthly_sales($params);
    $total = ceil($results['total_page'] / $results_per_page);
    if (isset($params["page"])) { $page = $params["page"]; } else { $page = 1; };

    $pages = array(
        "total" => $total,
        "current" => $page
    );

    $statuses = db_get_array("select ?:statuses.status , description as name from ?:statuses inner join ?:status_descriptions on ?:statuses.status_id = ?:status_descriptions.status_id where type='o'");

    Tygh::$app['view']->assign('statuses', $statuses);
    Tygh::$app['view']->assign('monthly_sales', $results['result']);
    Tygh::$app['view']->assign('search_params', $params);
    Tygh::$app['view']->assign('pages', $pages);
    Tygh::$app['view']->assign('months', $months);
    Tygh::$app['view']->assign('years', $years);
}

if ($mode == 'unique_user_sales') {
    $params = $_REQUEST;
    $results_per_page = 20;
    $results = fn_get_unique_user_sales($params);
    $total = ceil($results['total_page'] / $results_per_page);
    if (isset($params["page"])) { $page = $params["page"]; } else { $page = 1; };

    $pages = array(
        "total" => $total,
        "current" => $page
    );

    $statuses = db_get_array("select ?:statuses.status , description as name from ?:statuses inner join ?:status_descriptions on ?:statuses.status_id = ?:status_descriptions.status_id where type='o'");

    Tygh::$app['view']->assign('statuses', $statuses);
    Tygh::$app['view']->assign('unique_user_sales', $results['result']);
    Tygh::$app['view']->assign('search_params', $params);
    Tygh::$app['view']->assign('pages', $pages);
    Tygh::$app['view']->assign('months', $months);
    Tygh::$app['view']->assign('years', $years);
}

if ($mode == 'brand_sales') {
    $params = $_REQUEST;
    $results_per_page = 20;
    $results = fn_get_brand_sales($params);
    $total = ceil($results['total_page'] / $results_per_page);
    if (isset($params["page"])) { $page = $params["page"]; } else { $page = 1; };

    $pages = array(
        "total" => $total,
        "current" => $page
    );

    $statuses = db_get_array("select ?:statuses.status , description as name from ?:statuses inner join ?:status_descriptions on ?:statuses.status_id = ?:status_descriptions.status_id where type='o'");

    Tygh::$app['view']->assign('statuses', $statuses);
    Tygh::$app['view']->assign('brand_sales', $results['result']);
    Tygh::$app['view']->assign('search_params', $params);
    Tygh::$app['view']->assign('pages', $pages);
    Tygh::$app['view']->assign('months', $months);
    Tygh::$app['view']->assign('years', $years);
}

if ($mode == 'recurring_buyers') {
    $params = $_REQUEST;
    $results_per_page = 20;
    $results = fn_get_recurring_buyers($params);
    $total = ceil($results['total_page'] / $results_per_page);
    if (isset($params["page"])) { $page = $params["page"]; } else { $page = 1; };

    $pages = array(
        "total" => $total,
        "current" => $page
    );

    $statuses = db_get_array("select ?:statuses.status , description as name from ?:statuses inner join ?:status_descriptions on ?:statuses.status_id = ?:status_descriptions.status_id where type='o'");

    Tygh::$app['view']->assign('statuses', $statuses);
    Tygh::$app['view']->assign('recurring_buyers', $results['result']);
    Tygh::$app['view']->assign('search_params', $params);
    Tygh::$app['view']->assign('pages', $pages);
    Tygh::$app['view']->assign('months', $months);
    Tygh::$app['view']->assign('years', $years);
}

if ($mode == 'product_ratings') {
    $params = $_REQUEST;
    $results_per_page = 20;
    $results = fn_get_product_ratings($params);
    $total = ceil($results['total_page'] / $results_per_page);
    if (isset($params["page"])) { $page = $params["page"]; } else { $page = 1; };

    $pages = array(
        "total" => $total,
        "current" => $page
    );

    $statuses = db_get_array("select ?:statuses.status , description as name from ?:statuses inner join ?:status_descriptions on ?:statuses.status_id = ?:status_descriptions.status_id where type='o'");

    Tygh::$app['view']->assign('statuses', $statuses);
    Tygh::$app['view']->assign('product_ratings', $results['result']);
    Tygh::$app['view']->assign('search_params', $params);
    Tygh::$app['view']->assign('pages', $pages);
    Tygh::$app['view']->assign('months', $months);
    Tygh::$app['view']->assign('years', $years);
}

if ($mode == 'yearly_sales') {
    $params = $_REQUEST;
    $results_per_page = 20;
    $results = fn_get_yearly_sales($params);
    $total = ceil($results['total_page'] / $results_per_page);
    if (isset($params["page"])) { $page = $params["page"]; } else { $page = 1; };

    $pages = array(
        "total" => $total,
        "current" => $page
    );

    $statuses = db_get_array("select ?:statuses.status , description as name from ?:statuses inner join ?:status_descriptions on ?:statuses.status_id = ?:status_descriptions.status_id where type='o'");

    Tygh::$app['view']->assign('statuses', $statuses);
    Tygh::$app['view']->assign('yearly_sales', $results['result']);
    Tygh::$app['view']->assign('search_params', $params);
    Tygh::$app['view']->assign('pages', $pages);
    Tygh::$app['view']->assign('months', $months);
    Tygh::$app['view']->assign('years', $years);
}

if ($mode == 'order_details') {
    $params = $_REQUEST;
    $results_per_page = 20;
    $results = fn_get_order_details($params);
    $total = ceil($results['total_page'] / $results_per_page);
    if (isset($params["page"])) { $page = $params["page"]; } else { $page = 1; };

    $pages = array(
        "total" => $total,
        "current" => $page
    );

    $statuses = db_get_array("select ?:statuses.status , description as name from ?:statuses inner join ?:status_descriptions on ?:statuses.status_id = ?:status_descriptions.status_id where type='o'");

    Tygh::$app['view']->assign('statuses', $statuses);
    Tygh::$app['view']->assign('order_details', $results['result']);
    Tygh::$app['view']->assign('search_params', $params);
    Tygh::$app['view']->assign('pages', $pages);
    Tygh::$app['view']->assign('months', $months);
    Tygh::$app['view']->assign('years', $years);
}

if ($mode == 'warehouse_sales') {
    $params = $_REQUEST;
    $params['ws_start'] = strtotime(str_replace('/', '-', $params['ws_start']));
    $params['ws_end'] = strtotime(str_replace('/', '-', $params['ws_end']));
    $results_per_page = 20;
    $results = fn_get_warehouse_sales($params);
    $total = ceil($results['total_page'] / $results_per_page);
    if (isset($params["page"])) { $page = $params["page"]; } else { $page = 1; };

    $pages = array(
        "total" => $total,
        "current" => $page
    );

    Tygh::$app['view']->assign('sales', $results['result']);
    Tygh::$app['view']->assign('search_params', $params);
    Tygh::$app['view']->assign('pages', $pages);
}

if ($mode == 'monthly_orders') {
    $params = $_REQUEST;
    $params['from_date'] = strtotime(str_replace('/', '-', $params['from_date']));
    $params['to_date'] = strtotime(str_replace('/', '-', $params['to_date']));
    $results_per_page = 20;
    $results = fn_get_monthly_orders($params);
    $total = ceil($results['total_page'] / $results_per_page);
    if (isset($params["page"])) { $page = $params["page"]; } else { $page = 1; };

    $pages = array(
        "total" => $total,
        "current" => $page
    );

    Tygh::$app['view']->assign('sales', $results['result']);
    Tygh::$app['view']->assign('search_params', $params);
    Tygh::$app['view']->assign('pages', $pages);
}