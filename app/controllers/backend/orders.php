<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Mailer;
use Tygh\Pdf;
use Tygh\Registry;
use Tygh\Storage;
use Tygh\Settings;
use Tygh\Http;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $suffix = '';

    if ($mode == 'm_delete' && !empty($_REQUEST['order_ids'])) {
        foreach ($_REQUEST['order_ids'] as $v) {
            fn_delete_order($v);
        }
    }

    if ($mode == 'update_details') {
        fn_trusted_vars('update_order');

        $u_id = db_get_field("SELECT user_id FROM ?:orders WHERE order_id = ?i", $_REQUEST['order_id']);

        // Update customer's email if its changed in customer's account
        if (!empty($_REQUEST['update_customer_details']) && $_REQUEST['update_customer_details'] == 'Y') {
            // $u_id = db_get_field("SELECT user_id FROM ?:orders WHERE order_id = ?i", $_REQUEST['order_id']);
            $current_email = db_get_field("SELECT email FROM ?:users WHERE user_id = ?i", $u_id);
            db_query("UPDATE ?:orders SET email = ?s WHERE order_id = ?i", $current_email, $_REQUEST['order_id']);
        }

        // // Add user to blacklist
        // if($_REQUEST['add_user_to_blacklist'] == '1') {
        //     fn_add_user_to_blacklist($u_id);
        // }

        // // Remove user from blacklist
        // if($_REQUEST['remove_user_from_blacklist'] == '1') {
        //     fn_remove_user_from_blacklist($u_id);
        // }

        // Log order update
        fn_log_event('orders', 'update', array(
            'order_id' => $_REQUEST['order_id'],
            'tags' => $_REQUEST['order_info']['tags'],
        ));


        db_query('UPDATE ?:orders SET ?u WHERE order_id = ?i', $_REQUEST['update_order'], $_REQUEST['order_id']);

        fn_update_order_notes($_REQUEST['order_id'], $_REQUEST['update_order']['details'], 'A');
        fn_update_order_notes($_REQUEST['order_id'], $_REQUEST['update_order']['notes'], 'C');
        fn_update_order_delivery_dates($_REQUEST);

        //Update shipping info
        if (!empty($_REQUEST['update_shipping'])) {
            foreach ($_REQUEST['update_shipping'] as $group_key => $shipment_group) {
                foreach($shipment_group as $shipment_id => $shipment) {
                    $shipment['order_id'] = $_REQUEST['order_id'];
                    fn_update_shipment($shipment, $shipment_id, $group_key, true);
                }
            }
        }

        // Add new shipping info
        /*if (!empty($_REQUEST['add_shipping'])) {
            $shipping = db_get_field('SELECT shipping FROM ?:shipping_descriptions WHERE shipping_id = ?i', $_REQUEST['add_shipping']['shipping_id']);
            $shippings[$_REQUEST['add_shipping']['shipping_id']] = array(
                'shipping' => $shipping,
                'tracking_number' => $_REQUEST['add_shipping']['tracking_number'],
                'carrier' => $_REQUEST['add_shipping']['carrier'],
            );

            $_data = array(
                'data' => serialize($shippings),
                'order_id' => $_REQUEST['order_id'],
                'type' => 'L',
            );

            db_query('REPLACE INTO ?:order_data ?e', $_data);
        }*/

        $edp_data = array();
        $order_info = fn_get_order_info($_REQUEST['order_id'], false, true, false, false);
        if (!empty($_REQUEST['activate_files'])) {
            $edp_data = fn_generate_ekeys_for_edp(array(), $order_info, $_REQUEST['activate_files']);
        }
        fn_order_notification($order_info, $edp_data, fn_get_notification_rules($_REQUEST));

        if (!empty($_REQUEST['prolongate_data']) && is_array($_REQUEST['prolongate_data'])) {
            foreach ($_REQUEST['prolongate_data'] as $ekey => $v) {
                $newttl = fn_parse_date($v, true);
                db_query('UPDATE ?:product_file_ekeys SET ?u WHERE ekey = ?s', array('ttl' => $newttl), $ekey);
            }
        }

        // Update file downloads section
        if (!empty($_REQUEST['edp_downloads'])) {
            foreach ($_REQUEST['edp_downloads'] as $ekey => $v) {
                foreach ($v as $file_id => $downloads) {
                    $max_downloads = db_get_field("SELECT max_downloads FROM ?:product_files WHERE file_id = ?i", $file_id);
                    if (!empty($max_downloads)) {
                        db_query('UPDATE ?:product_file_ekeys SET ?u WHERE ekey = ?s', array('downloads' => $max_downloads - $downloads), $ekey);
                    }
                }
            }
        }
        if ($_REQUEST['gift_card']!=null && $order_info['subtotal'] > 50){
            Mailer::queueEmail(array(
                'to' => $order_info['email'],
                'from' => 'default_company_orders_department',
                'data' => array(
                    'order_info' => $order_info,
                ),
                'tpl' => 'orders/gift_cart_email_template.tpl',
                'company_id' => '1',
            ), 'A', 'al');

        }

        if($_REQUEST['create_credit_note']){
            $url = Registry::get('config.accounting_api_server') . '/KrijoCreditNote/' . $_REQUEST['order_id'];
            $result = file_get_contents($url);
            if ($result === FALSE || $result == "false") {
                fn_set_notification('E', __('error'), __('credit_note_not_created'));
            }else if($result == "true"){
                fn_set_notification('N', __('notice'), __('credit_note_created'));

            }
        }
        if($_REQUEST['create_order_refund']){
            $url = Registry::get('config.accounting_api_server') . '/KrijoRefund/' . $_REQUEST['order_id'];
            $result = file_get_contents($url);
            if ($result === FALSE || $result == "false") {
                fn_set_notification('E', __('error'), __('refund_not_created'));
            }else if($result == "true"){
                fn_set_notification('N', __('notice'), __('refund_created'));

            }
        }

        fn_process_order_totals($_REQUEST);

        $suffix = ".details?order_id=$_REQUEST[order_id]";
    }

    if ($mode == 'bulk_print' && !empty($_REQUEST['order_ids'])) {

        fn_print_order_invoices($_REQUEST['order_ids'], Registry::get('runtime.dispatch_extra') == 'pdf');
        exit;
    }

    if ($mode == 'packing_slip' && !empty($_REQUEST['order_ids'])) {

        fn_print_order_packing_slips($_REQUEST['order_ids'], Registry::get('runtime.dispatch_extra') == 'pdf');
        exit;
    }

    if ($mode == 'remove_cc_info' && !empty($_REQUEST['order_ids'])) {

        fn_set_progress('parts', sizeof($_REQUEST['order_ids']));

        foreach ($_REQUEST['order_ids'] as $v) {
            $payment_info = db_get_field("SELECT data FROM ?:order_data WHERE order_id = ?i AND type = 'P'", $v);
            fn_cleanup_payment_info($v, $payment_info);
        }

        fn_set_notification('N', __('notice'), __('done'));

        if (count($_REQUEST['order_ids']) == 1) {
            $o_id = array_pop($_REQUEST['order_ids']);
            $suffix = ".details?order_id=$o_id";
        } else {
            exit;
        }
    }

    if ($mode == 'export_range') {
        if (!empty($_REQUEST['order_ids'])) {
            if (empty(Tygh::$app['session']['export_ranges'])) {
                Tygh::$app['session']['export_ranges'] = array();
            }

            if (empty(Tygh::$app['session']['export_ranges']['orders'])) {
                Tygh::$app['session']['export_ranges']['orders'] = array('pattern_id' => 'orders');
            }

            Tygh::$app['session']['export_ranges']['orders']['data'] = array('order_id' => $_REQUEST['order_ids']);

            unset($_REQUEST['redirect_url']);

            return array(CONTROLLER_STATUS_REDIRECT, 'exim.export?section=orders&pattern_id=' . Tygh::$app['session']['export_ranges']['orders']['pattern_id']);
        }
    }

    if ($mode == 'products_range') {
        if (!empty($_REQUEST['order_ids'])) {
            unset($_REQUEST['redirect_url']);

            return array(CONTROLLER_STATUS_REDIRECT, 'products.manage?order_ids=' . implode(',', $_REQUEST['order_ids']));
        }
    }


    if ($mode == 'delete') {
        fn_delete_order($_REQUEST['order_id']);

        return array(CONTROLLER_STATUS_REDIRECT);
    }

    if ($mode == 'update_status') {

        $order_info = fn_get_order_short_info($_REQUEST['id']);
        $order_info_extend = fn_get_order_info($_REQUEST['id']);
        $order_id = $order_info_extend['order_id'];
        $old_status = $order_info['status'];
        if (fn_change_order_status($_REQUEST['id'], $_REQUEST['status'], '', fn_get_notification_rules($_REQUEST))) {
            $order_info = fn_get_order_short_info($_REQUEST['id']);
            fn_check_first_order($order_info);
            $new_status = $order_info['status'];
            if ($_REQUEST['status'] != $new_status) {
                Tygh::$app['ajax']->assign('return_status', $new_status);
                Tygh::$app['ajax']->assign('color', fn_get_status_param_value($new_status, 'color'));

                fn_set_notification('W', __('warning'), __('status_changed'));
            } else {
                if($_REQUEST['notify_quickbooks']){
                    $url = Registry::get('config.accounting_api_server') . '/KrijoFaturen/' . $_REQUEST['id'];
                    $result = file_get_contents($url);
                    $qb_notified = "0";
                    if ($result === FALSE || $result == "false") {
                        fn_set_notification('E', __('error'), __('invoice_not_sent_to_qb'));
                    }else if($result == "true"){
                        fn_set_notification('N', __('notice'), __('invoice_sent_to_qb'));
                        $qb_notified = "1";
                        $cart['notify_quickbooks'] = $qb_notified;
                        fn_update_order_data($order_id,$cart);
                    }
                }

                if($_REQUEST['notify_pro_quickbooks']) {
                    $url = Registry::get('config.accounting_api_server') . '/Estimate/' . $_REQUEST['id'];
                    $result = file_get_contents($url);
                    if ($result === FALSE || $result == "false") {
                        fn_set_notification('E', __('error'), __('pro_invoice_not_sent_to_qb'));
                    } else if ($result == "true") {
                        fn_set_notification('N', __('notice'), __('pro_invoice_sent_to_qb'));
                    }
                }

//                if($order_info) {
//                    if($_REQUEST['notify_user'] == 'Y'){
//                        if($new_status == 'C') {
//                            $order_info = fn_get_order_info($_REQUEST['id']);
//
//                            $email = $order_info['email'];
//                            $e_key = md5("email_rating_security_hash_654sdf546fe.$email" );
//                            //mail $order_info['email'];
//                            Mailer::queueEmail(array(
//                                'to' => $email,
//                                'from' => 'default_company_orders_department',
//                                'data' => array(
//                                    'order_id' => $order_info,
//                                    'e_key' => $e_key
//                                ),
//                                'tpl' => 'rating/email_rating.tpl',
//                                'company_id' => '1',
//                            ), 'A', 'al');
//                        }
//                    }
//                }

                if($old_status == 'J' && $new_status == 'I'){

                    Mailer::queueEmail(array(
                        'to' => 'default_company_orders_department',
                        'from' => 'default_company_orders_department',
                        'data' => array(
                            'order_id' => $_REQUEST['id'],
                        ),
                        'tpl' => 'orders/status_changed.tpl',
                        'company_id' => '1',
                    ), 'A', 'al');
                }
                fn_set_notification('N', __('notice'), __('status_changed'));
            }
        } else {
            fn_set_notification('E', __('error'), __('error_status_not_changed'));
            Tygh::$app['ajax']->assign('return_status', $old_status);
            Tygh::$app['ajax']->assign('color', fn_get_status_param_value($old_status, 'color'));
        }

        if (empty($_REQUEST['return_url'])) {
            exit;
        } else {
            return array(CONTROLLER_STATUS_REDIRECT, $_REQUEST['return_url']);
        }
    }

    if ($mode == 'update_payment_status') {
        fn_change_order_payment_status($_REQUEST['id'], $_REQUEST['status']);
    }

    //
    // Reserve products from retailer
    //
    if ($mode == 'reserve_order') {
        fn_reserve_order($_POST['order_id'], $_POST['order_status'], 'manual');
        fn_set_notification('N', __('notice'), 'Kërkesa për rezervim u realizua!');

        exit();
    }

    if ($mode == 'notify_build_order') {
        fn_order_notification(fn_get_order_info($_POST['order_id']), array(), array('C' => true, 'A' => true, 'S' => true, 'skip' => true));
        fn_set_notification('N', __('notice'), __('status_changed'));

        exit();
    }

    if ($mode == 'notify_quickbooks') {
        $url = Registry::get('config.accounting_api_server') . '/KrijoFaturen/' . $_REQUEST['id'];
        $result = file_get_contents($url);
        $qb_notified = "0";

        if ($result === FALSE || $result == "false") {
            fn_set_notification('E', __('error'), __('invoice_not_sent_to_qb'));
        } else if ($result == "true") {
            fn_set_notification('N', __('notice'), __('invoice_sent_to_qb'));
            $qb_notified = "1";
            $cart['notify_quickbooks'] = $qb_notified;
            fn_update_order_data($order_id,$cart);
        }
    }

    if ($mode == 'notify_pro_quickbooks') {
        $url = Registry::get('config.accounting_api_server') . '/Estimate/' . $_REQUEST['id'];
        $result = file_get_contents($url);

        if ($result === FALSE || $result == "false") {
            fn_set_notification('E', __('error'), __('pro_invoice_not_sent_to_qb'));
        } else if ($result == "true") {
            fn_set_notification('N', __('notice'), __('pro_invoice_sent_to_qb'));
        }
    }

    return array(CONTROLLER_STATUS_OK, 'orders' . $suffix);
}

$params = $_REQUEST;

if ($mode == 'update_with_vat') {
    fn_set_notification('N', __('notice'), 'Porosia e llogaritur me TVSH.');
    db_query('UPDATE ?:orders SET calculate_without_vat = ?s WHERE order_id = ?i', 'N', $_REQUEST['order_id']);

    return array(CONTROLLER_STATUS_REDIRECT, fn_url("orders.details&order_id=" . $_REQUEST['order_id']));
}

if ($mode == 'update_without_vat') {
    fn_set_notification('N', __('notice'), 'Porosia e llogaritur pa TVSH.');
    db_query('UPDATE ?:orders SET calculate_without_vat = ?s WHERE order_id = ?i', 'Y', $_REQUEST['order_id']);

    return array(CONTROLLER_STATUS_REDIRECT, fn_url("orders.details&order_id=" . $_REQUEST['order_id']));
}

if ($mode == 'print_invoice') {
    if (!empty($_REQUEST['order_id'])) {
        fn_print_order_invoices($_REQUEST['order_id'], !empty($_REQUEST['format']) && $_REQUEST['format'] == 'pdf');
    }
    exit;

} elseif ($mode == 'print_short_invoice') {
    if (!empty($_REQUEST['order_id'])) {
        fn_print_order_invoices($_REQUEST['order_id'], !empty($_REQUEST['format']) && $_REQUEST['format'] == 'pdf', AREA, CART_LANGUAGE, 'short');
    }
    exit;

} elseif ($mode == 'print_kosgiro_invoice') {
    if (!empty($_REQUEST['order_id'])) {
        fn_print_order_invoices($_REQUEST['order_id'], !empty($_REQUEST['format']) && $_REQUEST['format'] == 'pdf', AREA, CART_LANGUAGE, 'kosgiro');
    }
    exit;

} elseif ($mode == 'print_packing_slip') {
    if (!empty($_REQUEST['order_id'])) {
        fn_print_order_packing_slips($_REQUEST['order_id'], !empty($_REQUEST['format']) && $_REQUEST['format'] == 'pdf');
    }
    exit;

} elseif ($mode == 'details') {

    $_REQUEST['order_id'] = empty($_REQUEST['order_id']) ? 0 : $_REQUEST['order_id'];

    $order_info = fn_get_order_info($_REQUEST['order_id'], false, true, true, false);
    fn_check_first_order($order_info);

//    $credit = fn_get_user_qbo_credit($order_info);
    $credit = 0;
    Tygh::$app['view']->assign('credit', $credit);

    if (empty($order_info)) {
        return array(CONTROLLER_STATUS_NO_PAGE);
    }

    if (!empty($order_info['is_parent_order']) && $order_info['is_parent_order'] == 'Y') {
        // Get children orders
        $children_order_ids = db_get_fields('SELECT order_id FROM ?:orders WHERE parent_order_id = ?i', $order_info['order_id']);

        return array(CONTROLLER_STATUS_REDIRECT, 'orders.manage?order_id=' . implode(',', $children_order_ids));
    }

    if (isset($order_info['need_shipping']) && $order_info['need_shipping']) {
        $company_id = !empty($order_info['company_id']) ? $order_info['company_id'] : null;

        $shippings = fn_get_available_shippings($company_id);
        Tygh::$app['view']->assign('shippings', $shippings);
        Tygh::$app['view']->assign('shippings', $shippings);
    }

    Registry::set('navigation.tabs', array (
        'general' => array (
            'title' => __('general'),
            'js' => true
        ),
        'addons' => array (
            'title' => __('addons'),
            'js' => true
        ),
    ));

    if (fn_allowed_for('MULTIVENDOR')) {
        Tygh::$app['view']->assign('take_surcharge_from_vendor', fn_take_payment_surcharge_from_vendor($order_info['products']));
    }

    $downloads_exist = false;
    foreach ($order_info['products'] as $k => $v) {
        if (!$downloads_exist && !empty($v['extra']['is_edp']) && $v['extra']['is_edp'] == 'Y') {
            $downloads_exist = true;
        }

        // $stock_wms = fn_get_external_data('GET','https://wms50.gjirafa.com/ApiRequest/Stock?productId='.$order_info['products'][$k]['product_id'],'','stock_wms');
        // $stock_wms = json_decode($stock_wms,true);

        // $czc_price =  fn_get_external_data('GET','http://perkthimet.gjirafa.com/Pricing/GetCzcPrices?products='.$order_info['products'][$k]['product_code'],'','czc_price'); FIXME

        $czc_price = 0;
        $czc_price = json_decode($czc_price,true);
        // $order_info['products'][$k]['stock_wms'] = $stock_wms['count'];
        $order_info['products'][$k]['czc_price'] = $czc_price[$order_info['products'][$k]['product_code']];
        $price_difference = $order_info['products'][$k]['czc_price'] - $order_info['products'][$k]['price'];
        $percent_difference =  $price_difference > (($order_info['products'][$k]['price'] / 100) * 15);
        if($price_difference >= 100 || $percent_difference == true){
          $order_info['products'][$k]['price_alert'] = true;
        }

        $extra_product_data = db_get_array("SELECT list_price, amount FROM ?:products WHERE product_id = ?i", $v['product_id']);
        // $order_info['products'][$k]['list_price'] = db_get_field("SELECT list_price FROM ?:products WHERE product_id = ?i", $v['product_id']);
        $order_info['products'][$k]['list_price'] = $extra_product_data[0]['list_price'];
        $order_info['products'][$k]['stock_wms'] = $extra_product_data[0]['amount'];

        //$order_info['products'][$k]['main_pair'] = fn_get_cart_product_icon(
        //    $v['product_id'], $order_info['products'][$k]
        //);
    }

    if ($downloads_exist) {
        Registry::set('navigation.tabs.downloads', array (
            'title' => __('downloads'),
            'js' => true
        ));
        Tygh::$app['view']->assign('downloads_exist', true);
    }

    if (!empty($order_info['promotions'])) {
        Registry::set('navigation.tabs.promotions', array (
            'title' => __('promotions'),
            'js' => true
        ));
    }

    list($shipments) = fn_get_shipments_info(array('order_id' => $params['order_id'], 'advanced_info' => true));
    $use_shipments = !fn_one_full_shipped($shipments);

    // Check for the shipment access
    // If current edition is FREE, we still need to check shipments accessibility (need to display promotion link)
    if (Settings::instance()->getValue('use_shipments', '', $order_info['company_id']) == 'Y') {
        if (!fn_check_user_access($auth['user_id'], 'edit_order')) {
            $order_info['need_shipment'] = false;
        }
        $use_shipments = true;
    }

    $carriers_schema = fn_get_schema('shippings', 'carriers');
    foreach ($shipments as $shipment_key => $shipment) {
        $carrier = null;
        if (!empty($shipment['carrier'])) {
            $carrier = $shipment['carrier'];
        } else {
            $shipping_by_id = fn_get_shipping_info($shipment['shipping_id']);
            $service = fn_get_shipping_service_data($shipping_by_id['service_id']);
            if (!empty($service['module'])) {
                $carrier = $service['module'];
                $shipments[$shipment_key]['carrier'] = $carrier;
            }
        }
        if ($carrier && !empty($shipment['tracking_number']) && isset($carriers_schema[$carrier])) {
            $shipments[$shipment_key]['tracking_url'] = str_replace('[tracking_number]', $shipment['tracking_number'], $carriers_schema[$carrier]['tracking_url_template']);
        }
        $order_info['shipping'][$shipment['group_key']]['shipment_keys'][] = $shipment_key;
    }

    $notify_statuses = array( "C", "B" , "I" , "E", "J", "R", "V");
    $order_info['notify_status'] = false;
    if (in_array($order_info['status'], $notify_statuses)) {
        $order_info['notify_status'] = true;
    }
    $order_id = $order_info['order_id'];
    $order_data = db_get_field("Select data from ?:order_data where order_id = $order_id and type='Q' ");
    $unserialized_order_data = unserialize($order_data);
    $quickbooks_notified = db_get_field("Select tag_id from ?:sd_search_tag_links where object_id = ?i and tag_id = 147",$order_id);
    $fiscal_coupon_added = db_get_field("Select tag_id from ?:sd_search_tag_links where object_id = ?i and tag_id = 148", $order_id);

    if (((isset ($quickbooks_notified) && $quickbooks_notified == 147) || (isset ($fiscal_coupon_added) && $fiscal_coupon_added == 148)) && $auth['user_id'] != 1) {
        $order_info['disable_editing'] = 1;
    } else {
        $order_info['disable_editing'] = 0;
    }

//    $user_restrictions = array(1,2175,19867,106293,122563,122595,177455,177463);
    $usergroups = fn_get_user_usergroups(Tygh::$app['session']['auth']['user_id']);
    $order_info['user_tags'] = db_get_array('select tag from ?:sd_search_tags inner join ?:sd_search_tag_links on ?:sd_search_tag_links.tag_id = ?:sd_search_tags.tag_id where ?:sd_search_tags.type = ?s and object_id = ?i','U',$order_info['user_id']);
    $blacklisted = fn_user_is_blacklisted($order_info['user_id']);
//    $iute_status = fn_get_iutecredit_loan_status($order_info['order_id']);
    $reserve_order = false;

    if (ENVIRONMENT == 'live' && !fn_isAL() && $order_info['company_id'] == 1) {
        $reserve_order = fn_check_reserve_order($order_info);
    }

    $display_reserve_button = true;
    $has_reserve_tag_1 = array_search('86', array_column($order_info['tags'], 'tag_id'));
    $has_reserve_tag_2 = array_search('303', array_column($order_info['tags'], 'tag_id'));

    if (is_int($has_reserve_tag_2)) {
        $display_reserve_button = false;
    }

    fn_display_list_price_discount($order_info, 'A');

    Tygh::$app['view']->assign('shipments', $shipments);
    Tygh::$app['view']->assign('use_shipments', $use_shipments);
    Tygh::$app['view']->assign('carriers', fn_get_carriers());
//    Tygh::$app['view']->assign('user_restrictions', $user_restrictions);
    Tygh::$app['view']->assign('order_info', $order_info);
    Tygh::$app['view']->assign('blacklisted', $blacklisted);
//    Tygh::$app['view']->assign('iute_status', $iute_status);
    Tygh::$app['view']->assign('status_settings', fn_get_status_params($order_info['status']));
    Tygh::$app['view']->assign('reserve_order', $reserve_order);
    Tygh::$app['view']->assign('display_reserve_button', $display_reserve_button);
    Tygh::$app['view']->assign('usergroups', $usergroups);
    Tygh::$app['view']->assign('display_notify_build_order', fn_order_has_tag($order_info['tags'], 272));

    // Delete order_id from new_orders table
    db_query("DELETE FROM ?:new_orders WHERE order_id = ?i AND user_id = ?i", $_REQUEST['order_id'], $auth['user_id']);

    // Check if customer's email is changed
    if (!empty($order_info['user_id'])) {
        $current_email = db_get_field("SELECT email FROM ?:users WHERE user_id = ?i", $order_info['user_id']);
        if (!empty($current_email) && $current_email != $order_info['email']) {
            Tygh::$app['view']->assign('email_changed', true);
        }
    }

//    if (!fn_isAL()) {
        Tygh::$app['view']->assign('delivery_dates', fn_insert_order_delivery_dates($order_info['order_id']));
//    }

    $order_admin_notes = db_get_array('SELECT * FROM ?:order_notes WHERE order_id = ?i AND type = ?s ORDER BY id DESC', $_REQUEST['order_id'], 'A');
    Tygh::$app['view']->assign('order_admin_notes', $order_admin_notes);

    $order_client_notes = db_get_array('SELECT * FROM ?:order_notes WHERE order_id = ?i AND type = ?s ORDER BY id DESC', $_REQUEST['order_id'], 'C');
    Tygh::$app['view']->assign('order_client_notes', $order_client_notes);
} elseif ($mode == 'picker') {
    $_REQUEST['skip_view'] = 'Y';

    list($orders, $search) = fn_get_orders($_REQUEST, Registry::get('settings.Appearance.admin_orders_per_page'));
    Tygh::$app['view']->assign('orders', $orders);
    Tygh::$app['view']->assign('search', $search);

    Tygh::$app['view']->display('pickers/orders/picker_contents.tpl');
    exit;

} elseif ($mode == 'manage') {

    if (!empty($params['status']) && $params['status'] == STATUS_INCOMPLETED_ORDER) {
        $params['include_incompleted'] = true;
    }

    if (fn_allowed_for('MULTIVENDOR')) {
        $params['company_name'] = true;
    }

    list($orders, $search, $totals) = fn_get_orders($params, Registry::get('settings.Appearance.admin_orders_per_page'), true);

    if (!empty($params['include_incompleted']) || !empty($search['include_incompleted'])) {
        Tygh::$app['view']->assign('incompleted_view', true);
    }

    if (!empty($_REQUEST['redirect_if_one']) && count($orders) == 1) {
        return array(CONTROLLER_STATUS_REDIRECT, 'orders.details?order_id=' . $orders[0]['order_id']);
    }

    $shippings = fn_get_shippings(true, CART_LANGUAGE);
    if (Registry::get('runtime.company_id')) {
        $company_shippings = fn_get_companies_shipping_ids(Registry::get('runtime.company_id'));
        if (fn_allowed_for('ULTIMATE')) {
            $company_shippings = db_get_fields('SELECT shipping_id FROM ?:shippings');
        }
        foreach ($shippings as $shipping_id => $shipping) {
            if (!in_array($shipping_id, $company_shippings)) {
                unset($shippings[$shipping_id]);
            }
        }
    }

    $categories = db_get_array(
        "Select  category ,gjshop_categories.category_id, left(id_path ,4) from ?:category_descriptions left join ?:categories on ?:categories.category_id = ?:category_descriptions.category_id where status = 'A'"
    );

    $remove_cc = db_get_field(
        "SELECT COUNT(*)"
        . " FROM ?:status_data"
        . " WHERE status_id IN (?n)"
            . " AND param = 'remove_cc_info'"
            . " AND value = 'N'",
        array_keys(fn_get_statuses_by_type(STATUSES_ORDER))
    );

    if(isset($params['time_from'])){
        $time = array(
            'time_from'=>$params['time_from'],
            'time_to'=>$params['time_to']
        );
        $times =  str_replace("/",'-',$time);
        $gross_margin = fn_get_external_data('Post','http://perkthimet.gjirafa.com/Api/PricingApi/GetGrossMarginAdmin',$times,'gross_margin');
    }

    if (!apcu_exists('orders_api_ks_cities') || empty(apcu_fetch('orders_api_ks_cities'))) {
        $cities = db_get_array("SELECT object_id, description FROM ?:profile_field_descriptions WHERE object_type = 'V'");
        $cities = array_slice($cities, 1, 38);
        $cities = fn_array_values_recursive($cities);
        $cities = fn_array_group_by($cities, 2);
        $cities = array_flip($cities);

        apcu_add('orders_api_ks_cities', $cities);
    } else {
        $cities = apcu_fetch('orders_api_ks_cities');
    }

    $remove_cc = $remove_cc > 0 ? true : false;
    Tygh::$app['view']->assign('remove_cc', $remove_cc);
    Tygh::$app['view']->assign('categories', $categories);
    Tygh::$app['view']->assign('orders', $orders);
    Tygh::$app['view']->assign('search', $search);
    Tygh::$app['view']->assign('gross_margin', $gross_margin);
    Tygh::$app['view']->assign('totals', $totals);
    Tygh::$app['view']->assign('display_totals', fn_display_order_totals($orders));
    Tygh::$app['view']->assign('shippings', $shippings);
    Tygh::$app['view']->assign('cities', $cities);

    $payments = fn_get_payments(array('simple' => true));
    Tygh::$app['view']->assign('payments', $payments);

} elseif ($mode == 'get_custom_file') {
    if (!empty($_REQUEST['file']) && !empty($_REQUEST['order_id'])) {
        $file_path = 'order_data/' . $_REQUEST['order_id'] . '/' . $_REQUEST['file'];

        if (Storage::instance('custom_files')->isExist($file_path)) {

            $filename = !empty($_REQUEST['filename']) ? $_REQUEST['filename'] : '';
            Storage::instance('custom_files')->get($file_path, $filename);
        }
    }
} elseif($mode == 'get_user_order_count'){

    $s = db_get_array("SELECT user_id AS uid
FROM (
 SELECT user_id FROM ?:orders
 GROUP BY user_id HAVING COUNT(user_id) > 1
) AS t
WHERE user_id IN (?a)", json_decode($_GET['user_ids']));

    echo json_encode($s);
    exit;
} elseif ($mode == 'order_logs') {
    list($order_logs, $order_logs_search) = fn_get_order_logs($_REQUEST, Registry::get('settings.Appearance.admin_elements_per_page'));

    Tygh::$app['view']->assign('logs', $order_logs);
    Tygh::$app['view']->assign('search', $order_logs_search);
} elseif ($mode == 'order_log_details') {
    $fields_map = array(
        'fields (37)' => 'Shteti i faturimit',
        'fields (38)' => 'Shteti i transportit',
        'fields (40)' => 'Qyteti i faturimit (KS)',
        'fields (43)' => 'Emri i kompanise',
        'fields (41)' => 'Qyteti i transportit (KS)',
        'fields (51)' => 'Numri fiskal',
        'fields (54)' => 'Qyteti i faturimit (AL)',
        'fields (55)' => 'Qyteti i transportit (AL)',
        'fields (56)' => 'Telefoni 2 i faturimit',
        'fields (57)' => 'Telefoni 2 i transportit'
    );

    $profile_fields = db_get_array("SELECT object_id, description FROM ?:profile_field_descriptions WHERE object_type = ?i", "V");
    $order_edit_data = db_get_row("SELECT order_id, data_from, data_to, updated_at FROM ?:order_edit_logs WHERE log_id = ?i", $_GET['log_id']);

    $old_order = unserialize(base64_decode($order_edit_data['data_from']));
    $new_order = unserialize(base64_decode($order_edit_data['data_to']));

    Tygh::$app['view']->assign('order_id', $order_edit_data['order_id']);
    Tygh::$app['view']->assign('updated_at', $order_edit_data['updated_at']);
    Tygh::$app['view']->assign('order_edit_log', fn_get_order_log_details($old_order, $new_order));
    Tygh::$app['view']->assign('fields_map', $fields_map);
    Tygh::$app['view']->assign('profile_fields', $profile_fields);
}

//
// Calculate gross total and totally paid values for the current set of orders
//
function fn_display_order_totals($orders)
{
    $result = array();
    $result['gross_total'] = 0;
    $result['totally_paid'] = 0;

    if (is_array($orders)) {
        foreach ($orders as $k => $v) {
            $result['gross_total'] += $v['total'];
            if ($v['status'] == 'C' || $v['status'] == 'P') {
                $result['totally_paid'] += $v['total'];
            }
        }
    }

    return $result;
}

function fn_print_order_packing_slips($order_ids, $pdf = false, $lang_code = CART_LANGUAGE)
{
    $view = Tygh::$app['view'];
    $html = array();

    if (!is_array($order_ids)) {
        $order_ids = array($order_ids);
    }

    foreach ($order_ids as $order_id) {
        $order_info = fn_get_order_info($order_id, false, true, false, false);

        if (empty($order_info)) {
            continue;
        }

        $view->assign('order_info', $order_info);

        if ($pdf == true) {
            fn_disable_live_editor_mode();
            $html[] = $view->displayMail('orders/print_packing_slip.tpl', false, 'A', $order_info['company_id'], $lang_code);
        } else {
            $view->displayMail('orders/print_packing_slip.tpl', true, 'A', $order_info['company_id'], $lang_code);
        }

        if ($order_id != end($order_ids)) {
            echo("<div style='page-break-before: always;'>&nbsp;</div>");
        }
    }

    if ($pdf == true) {
        Pdf::render($html, __('packing_slip') . '-' . implode('-', $order_ids));
    }

    return true;
}

function fn_get_order_log_details($old_order, $new_order) {
    $order_edit_log = array();

    foreach ($old_order as $key_first => $order_first) {
        if (!empty($new_order[$key_first]) && $new_order[$key_first] != $order_first && !is_array($order_first)) {
            array_push($order_edit_log, array(
                'attribute' => str_replace('_', ' ', ucfirst($key_first)),
                'old_value' => $order_first,
                'new_value' => $new_order[$key_first]
            ));
        }

        if (is_array($order_first)) {
            foreach ($order_first as $key_second => $order_second) {
                if (!empty($new_order[$key_first][$key_second]) && $new_order[$key_first][$key_second] != $order_second && !is_array($order_second)) {
                    array_push($order_edit_log, array(
                        'attribute' => str_replace('_', ' ', $key_first . ' (' . $key_second . ')'),
                        'old_value' => $order_second,
                        'new_value' => $new_order[$key_first][$key_second]
                    ));
                }

                if (is_array($order_second)) {
                    foreach ($order_second as $key_third => $order_third) {
                        if (!empty($new_order[$key_first][$key_second][$key_third]) && $new_order[$key_first][$key_second][$key_third] != $order_third && !is_array($order_third)) {
                            array_push($order_edit_log, array(
                                'attribute' => str_replace('_', ' ', ucfirst($key_first) . ' (' .  $order_second['product_id'] . ') ' . ucfirst($key_third)),
                                'old_value' => $order_third,
                                'new_value' => $new_order[$key_first][$key_second][$key_third]
                            ));
                        }
                    }
                }
            }
        }
    }

    return $order_edit_log;
}

function fn_get_order_logs($params, $items_per_page) {
    $default_params = array(
        'page' => 1,
        'items_per_page' => $items_per_page
    );

    $params = array_merge($default_params, $params);

    $join = "";
    $condition = "";
    $users_join = "LEFT JOIN ?:users ON ?:order_edit_logs.user_id = ?:users.user_id";
    $order_by = "ORDER BY updated_at DESC";

    if (!empty($params['order_id'])) {
        $condition .= db_quote(" AND order_id = ?s", $params['order_id']);
    }

    $limit = '';
    if (!empty($params['items_per_page'])) {
        $params['total_items'] = db_get_field("SELECT COUNT(*) FROM ?:order_edit_logs ?p WHERE 1 ?p", $join, $condition);
        $limit = db_paginate($params['page'], $params['items_per_page'], $params['total_items']);
    }

    $data = db_get_array("SELECT ?:order_edit_logs.log_id, order_id, ?:order_edit_logs.user_id, updated_at, firstname, lastname FROM ?:order_edit_logs ?p $users_join WHERE 1 ?p $order_by $limit", $join, $condition);

    return array($data, $params);
}

function fn_check_reserve_order($order_info) {
    return true;
//    $order_count = db_get_field("SELECT COUNT(order_id) FROM ?:orders WHERE user_id = ?i AND status IN (?a)", $order_info['user_id'], array('R','E','L','C','K'));
//    $has_stock = fn_order_has_stock($order_info['product_groups'][0]['products']);
//
//    if ($order_count == 0 && !$has_stock && ($order_info['status'] == 'O' || $order_info['status'] == 'U') && fn_verify_order($order_info)) {
//        return true;
//    }
//
//    return false;
}