<?php
/***************************************************************************
 *                                                                          *
 *   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 *                                                                          *
 ****************************************************************************
 * PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
 * "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
 ****************************************************************************/

use Tygh\AmazonS3Client;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if ($mode == 'manage') {
        db_query('DELETE FROM ?:builder');

        foreach ($_REQUEST['sections'] as $key => $section) {
            $data = array(
                'item_id' => $section['id'],
                'item_title' => $section['title'],
                'item_description' => $section['description'],
                'item_products' => $section['products'],
                'item_product_codes' => $section['product_codes'],
                'item_categories' => $section['categories'],
            );

            if ($_FILES['file_sections']) {
                foreach ($_FILES['file_sections']['tmp_name'] as $file_key => $file) {
                    if ($file_key == $key) {
                        $file_data = fn_get_csv_data($file['csv'], array('product_code'));
                        $product_codes = implode(',', array_column($file_data, 'product_code'));

                        $product_ids = db_get_array('SELECT product_id FROM ?:products WHERE product_code IN (?a) AND status = ?s', explode(',', $product_codes), 'A');
                        $product_ids = implode(',', array_column($product_ids, 'product_id'));

                        $data['item_products'] = $product_ids;
                        $data['item_product_codes'] = $product_codes;
                    }
                }
            } else {
                $product_ids = db_get_array('SELECT product_id FROM ?:products WHERE product_code IN (?a) AND status = ?s', explode(',', $section['product_codes']), 'A');
                $product_ids = implode(',', array_column($product_ids, 'product_id'));

                $data['item_products'] = $product_ids;
            }

            db_query('REPLACE INTO ?:builder (build_item_id, data) VALUES (?i, ?s)', $key, base64_encode(serialize($data)));
        }
    }

    if ($mode == 'config') {
        db_query('DELETE FROM ?:builder_config');

        foreach ($_REQUEST['builder'] as $key => $item) {
            if (!empty($item['title'])) {
                $data = array(
                    'item' => $item['title'],
                    'required' => is_null($item['required']) ? false : true,
                    'multiple' => is_null($item['multiple']) ? false : true,
                );

                db_query('REPLACE INTO ?:builder_config (build_item_id, data) VALUES (?i, ?s)', $key, serialize($data));
            }
        }

        if ($_FILES) {
            foreach ($_FILES as $key => $file) {
                $name = 'builder/icons/builder_' . $key . '.' . end(explode('.', $file['name']));
                $client = new AmazonS3Client();
                $result = $client->createObject('gj50', $name, $file['tmp_name']);
            }
        }
    }

    return array(CONTROLLER_STATUS_REDIRECT, 'builder.' . $mode);
}

if ($mode == 'manage') {
    $items_data = [];
    $sections = db_get_array('SELECT * FROM ?:builder');
    $config = db_get_array('SELECT * FROM ?:builder_config');

    Tygh::$app['view']->assign('page_title', 'Builder management');
    Tygh::$app['view']->assign('item_count', count($config));

    foreach ($config as $key => $config_item) {
        $data = unserialize(base64_decode($sections[$key]['data']));
        $items_data[$config_item['build_item_id']] = $data;
        $items_data[$config_item['build_item_id']]['title'] = unserialize($config_item['data'])['item'];
    }

    Tygh::$app['view']->assign('items_data', $items_data);
}

if ($mode == 'config') {
    $config = db_get_array('SELECT * FROM ?:builder_config');

    Tygh::$app['view']->assign('page_title', 'Builder configuration');

    foreach ($config as $key => $config_item) {
        Tygh::$app['view']->assign('item_' . $config_item['build_item_id'] . '_data', unserialize($config[$key]['data']));
    }
}
