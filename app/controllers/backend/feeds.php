<?php

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if($mode == 'manage'){

    $files = array();
    foreach (glob("feeds/*.csv") as $file) {
        $files[] = $file;
    }

    $csv_file = isset($_GET['csv_file']) ? $_GET['csv_file'] : $_POST['file_name'];

    if(isset($csv_file) && $_SERVER['REQUEST_METHOD'] === "GET"){
        $csv = read_csv($csv_file);
    }

    if(isset($csv_file) && $_SERVER['REQUEST_METHOD'] === "POST"){
        $data = $_POST['data'];
        if (count($data) == 0) {
            return null;
        }

        $df = fopen($csv_file, 'w');

        foreach ($data as $row) {
            fputcsv($df, $row);
        }
        fclose($df);
        fn_set_notification('N', __('notice'), 'Ndryshimet u ruajtën me sukses!');
        return array(CONTROLLER_STATUS_OK, 'feeds.manage');
    }

    Tygh::$app['view']->assign('csv_list', $files);
    Tygh::$app['view']->assign('csv_name', $csv_file);
    Tygh::$app['view']->assign('csv', $csv);
}

if($mode == 'category_images'){
    $category_id = $_REQUEST['category_id'];

    if(apcu_exists("subcat_images"))
        $category_images =  apcu_fetch("subcat_images");
    else{
        $category_images = file_get_contents('feeds/subcat_images.txt');
        $category_images = unserialize($category_images);
        apcu_store("subcat_images", $category_images);
    }

    if($_SERVER['REQUEST_METHOD'] === "POST"){

        $data = $_REQUEST['data'];
        if (count($data) == 0) {
            return null;
        }
        
        unlink('feeds/subcat_images.txt');
        $filename = 'feeds/subcat_images.txt';
        $df = fopen($filename, 'w');
        $serialiezed_data = serialize($data);
        file_put_contents($filename, $serialiezed_data);

        fclose($df);
        apcu_delete(new APCUIterator("/subcat_images/"));
        fn_set_notification('N', __('notice'), 'Ndryshimet u ruajtën me sukses!');
        return array(CONTROLLER_STATUS_OK, 'feeds.category_images');
    }
    Tygh::$app['view']->assign('category_images', $category_images);

}