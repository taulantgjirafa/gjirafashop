<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

$_REQUEST['category_id'] = empty($_REQUEST['category_id']) ? 0 : $_REQUEST['category_id'];

if ($mode == 'catalog') {
    fn_add_breadcrumb(__('catalog'));

    $root_categories = fn_get_subcategories(0);

    foreach ($root_categories as $k => $v) {
        $root_categories[$k]['main_pair'] = fn_get_image_pairs($v['category_id'], 'category', 'M');
    }

    Tygh::$app['view']->assign('root_categories', $root_categories);

} elseif ($mode == 'view') {
    $company_id = Registry::get('runtime.company_id');
    $_statuses = array('A', 'H');
    $_condition = fn_get_localizations_condition('localization', true);
    $preview = fn_is_preview_action($auth, $_REQUEST);

    if (!$preview) {
        $_condition .= ' AND (' . fn_find_array_in_set($auth['usergroup_ids'], 'usergroup_ids', true) . ')';
        $_condition .= db_quote(' AND status IN (?a)', $_statuses);
    }

    if (fn_allowed_for('ULTIMATE')) {
        $_condition .= fn_get_company_condition('?:categories.company_id');
    }

    $category_exists = db_get_field(
        "SELECT category_id FROM ?:categories WHERE category_id = ?i ?p",
        $_REQUEST['category_id'],
        $_condition
    );

    if ($category_exists == '' && $company_id == 19) {
        $category_id = db_get_field('SELECT category_al FROM ?:category_mappings WHERE category_ks = ?i', $_REQUEST['category_id']);

        $category_exists = db_get_field(
            "SELECT category_id FROM ?:categories WHERE category_id = ?i ?p",
            $category_id,
            $_condition
        );
    }

//    var_dump($_REQUEST['category_id']);
    if (!empty($category_exists)) {

        // Save current url to session for 'Continue shopping' button
        Tygh::$app['session']['continue_url'] = "categories.view?category_id=$_REQUEST[category_id]";

        // Save current category id to session
        Tygh::$app['session']['current_category_id'] = Tygh::$app['session']['breadcrumb_category_id'] = $_REQUEST['category_id'];

        // Get subcategories list for current category
        if(apcu_exists("subcategories_" . $_REQUEST['category_id']))
            Tygh::$app['view']->assign('subcategories', apcu_fetch("subcategories_" . $_REQUEST['category_id']));
        else{
            $subcategories = fn_get_subcategories($_REQUEST['category_id']);
            Tygh::$app['view']->assign('subcategories', $subcategories);
            apcu_store("subcategories_" . $_REQUEST['category_id'], $subcategories);
        }


        Tygh::$app['view']->assign('category_brands_meta', fn_init_category_brands($_REQUEST['category_id']));

        // Get full data for current category
        if(apcu_exists("category_data_" . $_REQUEST['category_id']))
            $category_data = apcu_fetch("category_data_" . $_REQUEST['category_id']);
        else{
            $category_data = fn_get_category_data($_REQUEST['category_id'], CART_LANGUAGE, '*', true, false, $preview);
            apcu_store("category_data_" . $_REQUEST['category_id'], $category_data);
        }

        $category_parent_ids = fn_explode('/', $category_data['id_path']);
        array_pop($category_parent_ids);

        Tygh::$app['view']->assign('category_parent_ids', $category_parent_ids);

        if (!empty($category_data['meta_description']) || !empty($category_data['meta_keywords'])) {
            Tygh::$app['view']->assign('meta_description', $category_data['meta_description']);
            Tygh::$app['view']->assign('meta_keywords', $category_data['meta_keywords']);
        }

        $params = $_REQUEST;

        if ($items_per_page = fn_change_session_param(Tygh::$app['session'], $_REQUEST, 'items_per_page')) {
            $params['items_per_page'] = $items_per_page;
        }


        if ($_GET['isAjax']) {
            if ($sort_by = fn_change_session_param(Tygh::$app['session'], $_REQUEST, 'sort_by')) {
                $params['sort_by'] = $sort_by;
            }
        }

        if ($sort_order = fn_change_session_param(Tygh::$app['session'], $_REQUEST, 'sort_order')) {
            $params['sort_order'] = $sort_order;
        }

        $params['cid'] = $_REQUEST['category_id'];
        $params['extend'] = array('categories', 'description');
        $params['subcats'] = '';
        if (Registry::get('settings.General.show_products_from_subcategories') == 'Y') {
            $params['subcats'] = 'Y';
        }

        if(empty($params['q'])) {
            // if ($params['features_hash'] && strstr($params['features_hash'], 'EUR')) {
            //     var_dump($params['features_hash']);
            //     $index = strpos($params['features_hash'], 'EUR');
            //     var_dump(substr($params['features_hash'], 0, $index));
            //     $features_hash = explode('-', $index);
            // }

            $params['extend'] = array('description');
            $params['filters'] = true;
            $elastic = new elasticSearch();
            $db_switch = Registry::get('settings.General.switch_to_db');
            if ($db_switch == '1'){
                $params['sort_by'] = 'popularity';
                $params['sort_order'] = 'desc';
                list($products, $search) = fn_get_products($params, Registry::get('settings.Appearance.products_per_page'), CART_LANGUAGE);
            }
            else{
                // if category is Cka ka t're, sort by product ids desc
                if ($_REQUEST['category_id'] == 989) {
                    $params['sort_by'] = 'product_id';
                    $params['sort_order'] = 'desc';
                }

                $lista = list($products, $search) = $elastic->search($params);

                if (apcu_exists($_REQUEST['category_id'] . '_recommended_' . Registry::get('runtime.company_id'))) {
                    $category_recommended_products = apcu_fetch($_REQUEST['category_id'] . '_recommended_' . Registry::get('runtime.company_id'));
                } else {
                    $category_recommended_products = fn_get_category_recommended($_REQUEST['category_id'], $products);
                    apcu_store($_REQUEST['category_id'] . '_recommended_' . Registry::get('runtime.company_id'), $category_recommended_products);
                }

                if ($search['rand_elastic_url'] === Registry::get('config.elastic_services')[0]) {
                    $rand_elastic_url = 1;
                } else {
                    $rand_elastic_url = 2;
                }

                Tygh::$app['view']->assign('rand_elastic_url', $rand_elastic_url);

                if ($products === false) {
                    // if elastic doesn't return products, query database
                    $params['sort_by'] = $params['sort_by'] ? $params['sort_by'] : 'popularity';
                    $params['sort_order'] = $params['sort_order'] ? $params['sort_order'] : 'desc';
                    $params['cid'] = $_REQUEST['category_id'];
                    $lista = list($products, $search) = fn_get_products($params, Registry::get('settings.Appearance.products_per_page'), CART_LANGUAGE);
                }

                Tygh::$app['view']->assign('elastic', true);
            }

            if ($search['sort_order'] == 'asc')
                $search['sort_order_rev'] = 'desc';
            else
                $search['sort_order_rev'] = 'asc';

            $show_no_products_block = (!empty($params['features_hash']) && !$products);
            if ($show_no_products_block && defined('AJAX_REQUEST')) {
                fn_filters_not_found_notification();
                exit;
            }

            Tygh::$app['view']->assign('show_no_products_block', $show_no_products_block);

            $selected_layout = fn_get_products_layout($_REQUEST);

            if ($_GET['isAjax'] == 1) {
                Tygh::$app['view']->assign('products', $products);
                Tygh::$app['view']->assign('isAjax', true);
                Tygh::$app['view']->assign('selected_layout', $selected_layout);
                Registry::set('runtime.root_template', 'views/products/is_products.tpl',$selected_layout);
            } else {
                $cat_id = '2161';

                if (fn_isAL()) {
                    $cat_id = '3233';
                }

                $piece = $cat_id . '/%';
                $recommended_subcats = [];

                if (!apcu_exists('cat_rec_sub') || empty(apcu_fetch('cat_rec_sub'))) {
                    $recommended_subcats = db_get_fields('SELECT category_id FROM ?:categories WHERE id_path LIKE ?l', $piece);
                    apcu_store('cat_rec_sub', $recommended_subcats);
                } else {
                    $recommended_subcats = apcu_fetch('cat_rec_sub');
                }

                $recommended_subcats[] = $cat_id;

                if (in_array($_REQUEST['category_id'], $recommended_subcats)) {
                    // remove feature filters on the 'Recommended' category
                    unset($search['feature_filters']);

                    // get subcategories of 'Recommended'
                    Tygh::$app['view']->assign('recommended_subcats', $recommended_subcats);
                }

                Tygh::$app['view']->assign('price_filter', $search['price_params']);
                Tygh::$app['view']->assign('show_qty', true);
                Tygh::$app['view']->assign('products', $products);
                Tygh::$app['view']->assign('search', $search);
                Tygh::$app['view']->assign('selected_layout', $selected_layout);
                Tygh::$app['view']->assign('category_data', $category_data);
                Tygh::$app['view']->assign('category_recommended_products', $category_recommended_products);
                Tygh::$app['view']->assign('banners', fn_get_category_banners($_REQUEST['category_id']));
            }
            // If page title for this category is exist than assign it to template
            if (!empty($category_data['page_title'])) {
                Tygh::$app['view']->assign('page_title', $category_data['page_title']);
            }
            Tygh::$app['view']->assign('db_switch', $db_switch);

            if(in_array($_REQUEST['category_id'], explode(',', __("category_percentage_remove"))))
                Tygh::$app['view']->assign('no_percentage', true);

            Tygh::$app['view']->assign('gj_analytics_views', array(
                "category_id" => $category_data['category_id'],
                "category_name" => $category_data['category'],
                "timestamp" => time()
            ));

            // [Breadcrumbs]
            if (!empty($category_parent_ids)) {
                Registry::set('runtime.active_category_ids', $category_parent_ids);
                $cats = fn_get_category_name($category_parent_ids);
                foreach ($category_parent_ids as $c_id) {
                    fn_add_breadcrumb($cats[$c_id], "categories.view?category_id=$c_id");
                }
            }

            fn_add_breadcrumb($category_data['category'], (empty($_REQUEST['features_hash'])) ? '' : "categories.view?category_id=$_REQUEST[category_id]");
            // [/Breadcrumbs]
        }
    } else {
        return array(CONTROLLER_STATUS_NO_PAGE);
    }

}
elseif ($mode == 'picker') {

    $category_count = db_get_field("SELECT COUNT(*) FROM ?:categories");
    if ($category_count < CATEGORY_THRESHOLD) {
        $params = array (
            'simple' => false
        );
         list($categories_tree, ) = fn_get_categories($params);
         Tygh::$app['view']->assign('show_all', true);
    } else {
        $params = array (
            'category_id' => $_REQUEST['category_id'],
            'current_category_id' => $_REQUEST['category_id'],
            'visible' => true,
            'simple' => false
        );
        list($categories_tree, ) = fn_get_categories($params);
    }

    if (!empty($_REQUEST['root'])) {
        array_unshift($categories_tree, array('category_id' => 0, 'category' => $_REQUEST['root']));
    }
    Tygh::$app['view']->assign('categories_tree', $categories_tree);
    if ($category_count < CATEGORY_SHOW_ALL) {
        Tygh::$app['view']->assign('expand_all', true);
    }
    if (defined('AJAX_REQUEST')) {
        Tygh::$app['view']->assign('category_id', $_REQUEST['category_id']);
    }
    Tygh::$app['view']->display('pickers/categories/picker_contents.tpl');
    exit;
}

if ($mode == 'cv_gjanalytics') {
    if ($_REQUEST['gja_view_data']) {
        send_view_to_gjirafa_analytics('POST', 'http://164.132.160.4:1500/gjblob/log?filePath=/home/gjirafa/Gjirafa50/CategoryViews/' . date('Ymd') . '.txt' . '&content=' . urlencode($_REQUEST['gja_view_data']), 'true');
    }
    echo json_encode(array());
    http_response_code(200);
    header("HTTP/1.1 200 OK");
    exit;
}

function fn_init_category_brands($category_id){

    if(!apcu_exists("category_brands_meta_tags")){
        $rows = db_get_array('select DISTINCT(variant), category_id from ?:products
  inner join ?:products_categories
    on ?:products_categories.product_id = ?:products.product_id
  inner JOIN ?:product_features_values
    on ?:product_features_values.product_id = ?:products.product_id
  inner join ?:product_feature_variant_descriptions
    on ?:product_features_values.variant_id = ?:product_feature_variant_descriptions.variant_id
where feature_id = 12');

        $categories = array();
        foreach($rows as $category){
            if(!isset($categories[$category['category_id']])){
                $categories[$category['category_id']][] = $category['variant'];
            } else{
                array_push($categories[$category['category_id']], $category['variant']);
            }
        }
        apcu_store('category_brands_meta_tags', $categories, 0);
    }
    $categories = apcu_fetch('category_brands_meta_tags');

    return $categories[$category_id];
}