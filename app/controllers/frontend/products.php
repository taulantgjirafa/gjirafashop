<?php

/***************************************************************************
 *                                                                          *
 *   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 *                                                                          *
 ****************************************************************************
 * PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
 * "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
 ****************************************************************************/

use Tygh\Registry;
use Tygh\BlockManager\ProductTabs;

if (!defined('BOOTSTRAP')) {
    die('Access denied');
}

//
// Search products
//

if ($mode == 'ac' && defined('AJAX_REQUEST')) {

    $elastic = new elasticSearch();
    $a = $elastic->searchAutoComplete($_REQUEST['q']);

    echo $a;
    exit;
}

//if ($mode == 'dpfi' && defined('AJAX_REQUEST')) {
//    if (!is_null($_COOKIE['gj50__gjci'])) {
//        $products = ($elastic = new elasticSearch())->get_dmp_products(array('gjci' => $_COOKIE['gj50__gjci'], 'limit' => 20));
//        Tygh::$app['view']->assign('products', $products);
//        Registry::set('runtime.root_template', 'views/products/is_products.tpl');
//    }
//}

if ($mode == 'is_products' && defined('AJAX_REQUEST')) {

    $params['offset'] = $_REQUEST['offset'];
    $params['limit'] = $_REQUEST['limit'];
    $params['category_id'] = $_REQUEST['category_id'];

    $elastic = new elasticSearch();
    $db_switch = Registry::get('settings.General.switch_to_db');
    if ($db_switch == '1') {
        $params['sort_by'] = 'popularity';
        $params['sort_order'] = 'desc';
        list($products, $search) = fn_get_products($params, Registry::get('settings.Appearance.products_per_page'));
    } else {
        list($products, $search) = $elastic->search($params);
        Tygh::$app['view']->assign('elastic', true);

        if (empty($products)) {
            list($products, $search) = fn_get_products($params, Registry::get('settings.Appearance.products_per_page'));
        }
    }

    //$selected_layout = fn_get_products_layout($params);
    Tygh::$app['view']->assign('products', $products);
    Tygh::$app['view']->assign('isAjax', $_REQUEST['isAjax']);
    //Tygh::$app['view']->assign('selected_layout', $selected_layout);
    Registry::set('runtime.root_template', 'views/products/is_products.tpl');
    // exit;
}

if ($mode == 'load_products' && defined('AJAX_REQUEST')) {

    $params['offset'] = $_REQUEST['offset'];
    $params['limit'] = $_REQUEST['limit'];
    $params['category_id'] = $_REQUEST['category_id'];

    $elastic = new elasticSearch();
    $db_switch = Registry::get('settings.General.switch_to_db');
    if ($db_switch == '1') {
        $params['sort_by'] = 'popularity';
        $params['sort_order'] = 'desc';
        list($products, $search) = fn_get_products($params, Registry::get('settings.Appearance.products_per_page'));
    } else {
        list($products, $search) = $elastic->search($params);
        Tygh::$app['view']->assign('elastic', true);

        if ($products === false) {
            list($products, $search) = fn_get_products($params, Registry::get('settings.Appearance.products_per_page'));
        }
    }

    Tygh::$app['view']->assign('products', $products);
    Tygh::$app['view']->assign('isAjax', $_REQUEST['isAjax']);

    $products_block = apcu_fetch('products_block_data');
    if ($products_block['template'] == 'blocks/products/products_sections.tpl') {
        Registry::set('runtime.root_template', 'views/products/load_products.tpl');
    } else {
        Registry::set('runtime.root_template', 'views/products/is_products.tpl');
    }
}

if ($mode == 'search') {

    $params = $_REQUEST;
    fn_add_breadcrumb(__('search_results'));

    // if (!empty($params['q']) && ($params['security_hash'] == Tygh::$app['session']['security_hash'])) {
    if (!empty($params['q'])) {

        $params['extend'] = array('description');
        $params['filters'] = true;
        $elastic = new elasticSearch();
        $db_switch = Registry::get('settings.General.switch_to_db');
        if ($db_switch == '1') {
            $params['sort_by'] = 'popularity';
            $params['sort_order'] = 'desc';
            list($products, $search) = fn_get_products($params, Registry::get('settings.Appearance.products_per_page'));
        } else {
            list($products, $search) = $elastic->search($params);

            // if ($search['rand_elastic_url'] === 'http://164.132.164.207') {
            //     $rand_elastic_url = 1;
            // } else {
            //     $rand_elastic_url = 2;
            // }

            // Tygh::$app['view']->assign('rand_elastic_url', $rand_elastic_url);

            if ($products === false) {
                $params['sort_by'] = 'popularity';
                $params['sort_order'] = 'desc';
                $params['limit'] = 48;
                list($products, $search) = fn_get_products($params, Registry::get('settings.Appearance.products_per_page'));

                Tygh::$app['view']->assign('search_limited', true);
            } else {
                Tygh::$app['view']->assign('elastic', true);
            }
        }

        if ($search['sort_order'] == 'asc')
            $search['sort_order_rev'] = 'desc';
        else
            $search['sort_order_rev'] = 'asc';

        if (defined('AJAX_REQUEST') && (!empty($params['features_hash']) && !$products)) {
            fn_filters_not_found_notification();
            exit;
        }

        if (!empty($products)) {
            Tygh::$app['session']['continue_url'] = Registry::get('config.current_url');
        }

        $selected_layout = fn_get_products_layout($params);

        if ($_GET['isAjax'] == 1) {
            Tygh::$app['view']->assign('products', $products);
            Tygh::$app['view']->assign('isAjax', true);
            Registry::set('runtime.root_template', 'views/products/is_products.tpl');
        } else {
            //Tygh::$app['view']->assign('elastic_filters', $search['feature_filters']);
            Tygh::$app['view']->assign('price_filter', $search['price_params']);
            Tygh::$app['view']->assign('products', $products);
            //            Tygh::$app['view']->assign('did_you_mean', (empty($products)) ? $elastic->didYouMean($params['q']) : NULL);
            Tygh::$app['view']->assign('search', $search);
            Tygh::$app['view']->assign('selected_layout', $selected_layout);
        }
        Tygh::$app['view']->assign('db_switch', $db_switch);

        if ($products) {
            Tygh::$app['view']->assign('result_product_ids', json_encode(array_keys($products)));
        }
    }


    //
    // View product details
    //
} elseif ($mode == 'view' || $mode == 'quick_view') {

    apcu_delete(new APCUIterator('#^cs-cart:cache:gj50_:block_content_11_#', APC_ITER_VALUE));

    $_REQUEST['product_id'] = empty($_REQUEST['product_id']) ? 0 : $_REQUEST['product_id'];

    if (!empty($_REQUEST['product_id']) && empty($auth['user_id'])) {

        $uids = explode(',', db_get_field("SElECT usergroup_ids FROM ?:products WHERE product_id = ?i", $_REQUEST['product_id']));

        if (!in_array(USERGROUP_ALL, $uids) && !in_array(USERGROUP_GUEST, $uids)) {
            return array(CONTROLLER_STATUS_REDIRECT, 'auth.login_form?return_url=' . urlencode(Registry::get('config.current_url')));
        }
    }

    $product = fn_get_product_data(
        $_REQUEST['product_id'],
        $auth,
        CART_LANGUAGE,
        '',
        true,
        true,
        true,
        true,
        fn_is_preview_action($auth, $_REQUEST),
        true,
        false,
        true
    );

    //    if(apcu_exists("bot_score_" . $product['product_id']))
    //        $product['score'] =   apcu_fetch("bot_score_" . $product['product_id']);
    //    else{
    //        $thread_id = db_get_field("select thread_id from ?:discussion where object_id = ?i",$product['product_id']);
    //        $product['score'] = db_get_field("select AVG(rating_value) from ?:discussion_posts inner join ?:discussion_rating on ?:discussion_rating.thread_id = ?:discussion_posts.thread_id where ?:discussion_posts.thread_id = ?i and name = ?s",$thread_id,'Bot');
    //        apcu_store('bot_score_'. $product['product_id'],$product['score']);
    //    }

    if (empty($product)) {
        return array(CONTROLLER_STATUS_NO_PAGE);
    }

    if ($product['status'] == 'D' && !fn_is_preview_action($auth, $_REQUEST)) {
        return array(CONTROLLER_STATUS_NO_PAGE);
    }

    if (in_array($product['product_id'], fn_get_limited_to_store_products(fn_isAL()))) {
        return array(CONTROLLER_STATUS_NO_PAGE);
    }

    $best_price_guaranteed_products = apcu_fetch('best_price_guaranteed_products');

    if (!apcu_exists('best_price_guaranteed_products') || empty($best_price_guaranteed_products)) {
        $file = fopen(DIR_ROOT . '/var/best_price_guaranteed.csv', 'r');
        $best_price_guaranteed_products = fn_get_csv_data(DIR_ROOT . '/var/best_price_guaranteed.csv', array('product_code'));
        $best_price_guaranteed_products = array_column($best_price_guaranteed_products, 'product_code');
        apcu_store('best_price_guaranteed_products', $best_price_guaranteed_products);
    }

    if (in_array($product['product_code'], $best_price_guaranteed_products)) {
        $product['best_price_guaranteed'] = true;
    }

    if (in_array($product['product_code'], explode(',', __('no_delivery_date_products'))) || fn_disable_product_delivery_dates($product)) {
        $product['no_delivery_date'] = true;
    }

    if ($product['out_of_stock_actions'] == 'B' && $product['avail_since'] > time() && fn_isAL()) {
        $product['amount'] = 0;
        $product['real_amount'] = 0;
        $product['czc_stock'] = 0;
        $product['czc_retailer_stock'] = 0;
        $product['al_stock'] = 0;
    }

    $product['flag'] = fn_get_product_flag($product['header_features'][14]['variant'], $product['product_id']);
    fn_modify_product_features($product);

    //add_product_to_promotion_dmp($_REQUEST['product_id']);

    if (apcu_exists("bot_score_" . $product['product_id']))
        $product['score'] = apcu_fetch("bot_score_" . $product['product_id']);
    else {
        $product['score'] = 0;
        $thread_id = db_get_field("SELECT AVG(rating_value)
            FROM ?:discussion_posts
            INNER JOIN ?:discussion_rating on gjshop_discussion_rating.thread_id = ?:discussion_posts.thread_id
            where ?:discussion_posts.thread_id IN (select thread_id from ?:discussion where object_id = ?i)
            HAVING COUNT(*) >= 3", $product['product_id']);
        if ($thread_id != null)
            $product['score'] = round(($thread_id / 5 * 100) / 10) * 10;
        apcu_store('bot_score_' . $product['product_id'], $product['score']);
    }


    if ((empty(Tygh::$app['session']['current_category_id']) || empty($product['category_ids'][Tygh::$app['session']['current_category_id']])) && !empty($product['main_category'])) {
        if (!empty(Tygh::$app['session']['breadcrumb_category_id']) && in_array(Tygh::$app['session']['breadcrumb_category_id'], $product['category_ids'])) {
            Tygh::$app['session']['current_category_id'] = Tygh::$app['session']['breadcrumb_category_id'];
        } else {
            Tygh::$app['session']['current_category_id'] = $product['main_category'];
        }
    }

    if (!empty($product['meta_description']) || !empty($product['meta_keywords'])) {
        Tygh::$app['view']->assign('meta_description', $product['meta_description']);
        Tygh::$app['view']->assign('meta_keywords', $product['meta_keywords']);
    } else {
        $meta_tags = db_get_row(
            "SELECT meta_description, meta_keywords"
                . " FROM ?:category_descriptions"
                . " WHERE category_id = ?i AND lang_code = ?s",
            Tygh::$app['session']['current_category_id'],
            CART_LANGUAGE
        );
        if (!empty($meta_tags)) {
            Tygh::$app['view']->assign('meta_description', $meta_tags['meta_description']);
            Tygh::$app['view']->assign('meta_keywords', $meta_tags['meta_keywords']);
        }
    }
    if (!empty(Tygh::$app['session']['current_category_id'])) {
        Tygh::$app['session']['continue_url'] = "categories.view?category_id=" . Tygh::$app['session']['current_category_id'];

        $parent_ids = fn_explode(
            '/',
            db_get_field(
                "SELECT id_path FROM ?:categories WHERE category_id = ?i",
                Tygh::$app['session']['current_category_id']
            )
        );

        if (!empty($parent_ids)) {
            Registry::set('runtime.active_category_ids', $parent_ids);
            $cats = fn_get_category_name($parent_ids);
            foreach ($parent_ids as $c_id) {
                fn_add_breadcrumb($cats[$c_id], "categories.view?category_id=$c_id");
            }
        }
    }
    fn_add_breadcrumb($product['product']);

    if (!empty($_REQUEST['combination'])) {
        $product['combination'] = $_REQUEST['combination'];
    }

    // if ($product['list_price'] && $product['list_price'] > $product['price']) {
    //     $product_discount_list_price = $product['list_price'] - $product['price'];
    //     $product['discount'] += $product_discount_list_price;
    //     $product['discount_prc'] += round(($product_discount_list_price / $product['list_price']) * 100);
    // }

    fn_gather_additional_product_data($product, true, true);

    if ($product['list_price'] && $product['list_price'] > $product['base_price']) {
        $product_discount_list_price = $product['list_price'] - $product['price'];
        $product['discount'] = $product_discount_list_price;
        $product['discount_prc'] = round(($product_discount_list_price / $product['list_price']) * 100);
    }

    if ($product['amount'] < 0) {
        $product['amount'] = 0;
    }

    Tygh::$app['view']->assign('product', $product);

    // If page title for this product is exist than assign it to template
    if (!empty($product['page_title'])) {
        Tygh::$app['view']->assign('page_title', $product['page_title']);
    }

    $params = array(
        'product_id' => $_REQUEST['product_id'],
        'preview_check' => true
    );
    list($files) = fn_get_product_files($params);

    if (!empty($files)) {
        Tygh::$app['view']->assign('files', $files);
    }

    /* [Product tabs] */
    $tabs = ProductTabs::instance()->getList(
        '',
        $product['product_id'],
        DESCR_SL
    );
    foreach ($tabs as $tab_id => $tab) {
        if ($tab['status'] == 'D') {
            continue;
        }
        if (!empty($tab['template'])) {
            $tabs[$tab_id]['html_id'] = fn_basename($tab['template'], ".tpl");
        } else {
            $tabs[$tab_id]['html_id'] = 'product_tab_' . $tab_id;
        }

        if ($tab['show_in_popup'] != "Y") {
            Registry::set('navigation.tabs.' . $tabs[$tab_id]['html_id'], array(
                'title' => $tab['name'],
                'js' => true
            ));
        }
    }
    Tygh::$app['view']->assign('tabs', $tabs);
    /* [/Product tabs] */

    $recently_viewed_products = fn_get_recently_viewed_products();
    Tygh::$app['view']->assign('recently_viewed_products', $recently_viewed_products);

    // Set recently viewed products history
    fn_add_product_to_recently_viewed(trim($product['product_code']));

    // Increase product popularity
    fn_set_product_popularity($_REQUEST['product_id']);

    $product_notification_enabled = (isset(Tygh::$app['session']['product_notifications']) ? (isset(Tygh::$app['session']['product_notifications']['product_ids']) && in_array($_REQUEST['product_id'], Tygh::$app['session']['product_notifications']['product_ids']) ? 'Y' : 'N') : 'N');

    if ($product_notification_enabled) {
        if ((Tygh::$app['session']['auth']['user_id'] == 0) && !empty(Tygh::$app['session']['product_notifications']['email'])) {
            if (!db_get_field("SELECT subscription_id FROM ?:product_subscriptions WHERE product_id = ?i AND email = ?s", $_REQUEST['product_id'], Tygh::$app['session']['product_notifications']['email'])) {
                $product_notification_enabled = 'N';
            }
        } elseif (!db_get_field("SELECT subscription_id FROM ?:product_subscriptions WHERE product_id = ?i AND user_id = ?i AND sent = ?s", $_REQUEST['product_id'], Tygh::$app['session']['auth']['user_id'], 'N')) {
            $product_notification_enabled = 'N';
        }
    }

    Tygh::$app['view']->assign('is_mobile', fn_is_mobile());
    Tygh::$app['view']->assign('recommendations', fn_get_recommended_products($product['product_code']));
    Tygh::$app['view']->assign('show_qty', true);
    Tygh::$app['view']->assign('product_notification_enabled', $product_notification_enabled);
    Tygh::$app['view']->assign('product_notification_email', (isset(Tygh::$app['session']['product_notifications']) ? Tygh::$app['session']['product_notifications']['email'] : ''));
    // Tygh::$app['view']->assign('price_without_vat', fn_price_without_vat($product['product_id']));

    Tygh::$app['view']->assign('gj_analytics_views', array(
        "product_id" => $product['product_id'],
        "product_code" => $product['product_code'],
        "product" => $product['product'],
        "price" => $product['price'],
        "old_price" => $product['list_price'],
        "user_agent" => $_SERVER['HTTP_USER_AGENT'],
        "timestamp" => time()
    ));

    if ($product['product_features'][1]['subfeatures'][14]['variants']) {
        Tygh::$app['view']->assign('ean', reset($product['product_features'][1]['subfeatures'][14]['variants'])['variant']);
    }

    if ($mode == 'quick_view') {
        if (defined('AJAX_REQUEST')) {
            fn_prepare_product_quick_view($_REQUEST);
            Registry::set('runtime.root_template', 'views/products/quick_view.tpl');
        } else {
            return array(CONTROLLER_STATUS_REDIRECT, 'products.view?product_id=' . $_REQUEST['product_id']);
        }
    }
} elseif ($mode == 'options') {

    if (!defined('AJAX_REQUEST') && !empty($_REQUEST['product_data'])) {
        list($product_id, $_data) = each($_REQUEST['product_data']);
        $product_id = isset($_data['product_id']) ? $_data['product_id'] : $product_id;

        return array(CONTROLLER_STATUS_REDIRECT, 'products.view?product_id=' . $product_id);
    }
} elseif ($mode == 'product_notifications') {

    if (Tygh::$app['session']['auth']['user_id']) {
        $product_id = $_GET['product_id'];
        $user_id = Tygh::$app['session']['auth']['user_id'];
        $email = fn_get_user_email(Tygh::$app['session']['auth']['user_id']);
        $enable = $_GET['enable'];
    } else {
        $product_id = $_POST['product_id'];
        $user_id = 0;
        $email = $_POST['email'];
        $enable = $_POST['enable'];
    }

    fn_update_product_notifications(array(
        'product_id' => $product_id,
        'user_id' => $user_id,
        'email' => $email,
        'enable' => $enable,
        'insert_timestamp' => time()
    ));

    exit;
}

if ($mode == 'fb_product_feed') {

    ini_set("memory_limit", "-1");
    header('Content-Type: text/xml; charset=utf-8', true); //set document header content type to be XML

    $rss = new SimpleXMLElement('<rss xmlns:g="http://base.google.com/ns/1.0" version="2.0"></rss>');
    $rss->addAttribute('version', '1.0');

    $channel = $rss->addChild('channel'); //add channel node

    $title = $channel->addChild('title', 'Gjirafa50'); //title of the feed
    $link = $channel->addChild('link', 'https://www.gjirafa50.com'); //feed site
    $description = $channel->addChild('description', 'Dyqani online më i madh per teknologji në Kosovë dhe rajon. Shumë i sigurtë dhe me oferta në produktet e teknologjisë.'); //feed description

    $all_products = db_get_array('SELECT a.product_id, a.status, a.amount, b.product, b.short_description, b.full_description, c.price, d.czc_stock, d.czc_retailer_stock, f.category, h.variant FROM ?:products as a, ?:product_descriptions as b, ?:product_prices as c, ?:product_stock as d, ?:products_categories as e, ?:category_descriptions as f, ?:product_features_values as g, ?:product_feature_variant_descriptions as h WHERE a.product_id = b.product_id AND a.product_id = c.product_id AND a.product_id = d.product_id AND a.status = \'A\' AND (d.czc_stock != 0 OR d.czc_retailer_stock != 0) AND a.product_id = e.product_id AND e.category_id = f.category_id AND e.link_type = \'M\'  AND g.feature_id = 11 AND g.product_id = a.product_id AND g.variant_id = h.variant_id ORDER BY a.product_id LIMIT 10');

    $all_products_brands = db_get_array('SELECT a.product_id, b.variant FROM ?:product_features_values as a, ?:product_feature_variant_descriptions as b WHERE a.variant_id = b.variant_id AND a.feature_id = 9');

    $brand_index = array();

    foreach ($all_products_brands as $apb_id) {
        $brand_index[$apb_id['product_id']] = $apb_id['variant'];
    }

    $item = $channel->addChild($_GET);
    $item = $channel->addChild($_POST);

    foreach ($all_products as $product) {
        $item = $channel->addChild('item');
        $item->addChild('xmlns:g:id', $product['product_id']);

        if (strpos($product['product'], '&') !== false)
            $product['product'] = str_replace('&', '&amp;', $product['product']);

        $item->addChild('xmlns:g:title', $product['product']);

        if (strpos($product['full_description'], '&') !== false)
            $product['full_description'] = str_replace('&', '&amp;', $product['full_description']);

        if (strlen($product['full_description']) > 120)
            $product['full_description'] = substr($product['full_description'], 0, 120) . "...";

        $item->addChild('xmlns:g:description', $product['full_description']);
        $url = "https://gjirafa50.com/index.php?dispatch=products.view%26product_id=" . $product["product_id"];
        $item->addChild('xmlns:g:link', $url);

        $container = intval($product['product_id'] / 8000 + 1);
        if ($container > 5)
            $container = 5;

        $item->addChild('xmlns:g:image_link', "https://hhstsyoejx.gjirafa.net/gj50/img/" . $product['product_id'] . "/thumb/0.jpg");

        $item->addChild('xmlns:g:brand', $brand_index[$product['product_id']]);
        $item->addChild('xmlns:g:condition', "new");
        $item->addChild('xmlns:g:availability', "in stock");
        $item->addChild('xmlns:g:price', $product['price'] . " EUR");
        $item->addChild('xmlns:g:gtin', $product['variant']);
        $shipping = $item->addChild('g:shipping');
        $shipping->addChild('xmlns:g:country', "XK");
        if ($product['amount'] != 0)
            $shipping->addChild('xmlns:g:service', "GjirafaSWIFT");
        else
            $shipping->addChild('xmlns:g:service', "GjirafaSTANDARD");
        $shipping->addChild('xmlns:g:price', "0.00 EUR");
        $item->addChild('xmlns:g:google_product_category', $product['category']);
        $item->addChild('xmlns:g:custom_label_0', "xxxxxxxx");
    }

    Header('Content-type: text/xml');
    $dom = new DOMDocument('1.0', LIBXML_NOBLANKS);
    $dom->formatOutput = true;
    $dom->loadXML($rss->asXML('test.xml'));
    //$dom->saveXML('test.xml ');

    exit;
}


if ($mode == 'fb_update_product') {
    $fb_catalog = new fb_catalog();
    $product = array(
        'product_id' => 100,
        'product' => 'TEST_UPDATE',
        'price' => '2000'
    );
    $fb_catalog->fn_catalog_update_product($product);
    exit;
}

if ($mode == 'add_discussion_from_mail') {

    $product_ids = $_REQUEST['post_data']['product_ids'];
    $redirect_url = '/index.php';
    foreach ($product_ids as $product_id) {
        $data = $_REQUEST['post_data_' . $product_id];

        if (!empty($data['rating_value'])) {
            if (isset($_REQUEST['post_id']) && $_REQUEST['post_id'] != '' && $data['object_type'] == 'P') {
                fn_update_discussion_post($_REQUEST['post_id'], $data);

                return array(CONTROLLER_STATUS_REDIRECT, 'pages.add-discussion&order_id=' . $_REQUEST['order_id'] . '&e_key=' . $_REQUEST['e_key']);
            } else {
                $data['thread_id'] = db_get_field("SELECT thread_id FROM ?:discussion WHERE object_id = $product_id");
                fn_add_discussion_post($data);

                return array(CONTROLLER_STATUS_REDIRECT, 'pages.add-discussion&order_id=' . $_REQUEST['order_id'] . '&e_key=' . $_REQUEST['e_key']);
            }
        } else {
            fn_set_notification('W', __('notice'), 'Ju lutem zgjedhni një vlerësim.');

            return array(CONTROLLER_STATUS_REDIRECT, 'pages.add-discussion&order_id=' . $_REQUEST['order_id'] . '&e_key=' . $_REQUEST['e_key']);
        }
    }

    return array(CONTROLLER_STATUS_REDIRECT, $redirect_url);
}

function fn_add_product_to_recently_viewed($product_id, $max_list_size = MAX_RECENTLY_VIEWED)
{
    $added = false;

    if (!empty(Tygh::$app['session']['recently_viewed_products'])) {
        $is_exist = array_search($product_id, Tygh::$app['session']['recently_viewed_products']);
        // Existing product will be moved on the top of the list
        if ($is_exist !== false) {
            // Remove the existing product to put it on the top later
            unset(Tygh::$app['session']['recently_viewed_products'][$is_exist]);
            // Re-sort the array
            Tygh::$app['session']['recently_viewed_products'] = array_values(Tygh::$app['session']['recently_viewed_products']);
        }

        array_unshift(Tygh::$app['session']['recently_viewed_products'], $product_id);
        $added = true;
    } else {
        Tygh::$app['session']['recently_viewed_products'] = array($product_id);
    }

    if (count(Tygh::$app['session']['recently_viewed_products']) > $max_list_size) {
        array_pop(Tygh::$app['session']['recently_viewed_products']);
    }

    return $added;
}

function fn_set_product_popularity($product_id, $popularity_view = POPULARITY_VIEW)
{
    if (empty(Tygh::$app['session']['products_popularity']['viewed'][$product_id])) {
        $_data = array(
            'product_id' => $product_id,
            'viewed' => 1,
            'total' => $popularity_view
        );

        db_query("INSERT INTO ?:product_popularity ?e ON DUPLICATE KEY UPDATE viewed = viewed + 1, total = total + ?i", $_data, $popularity_view);

        Tygh::$app['session']['products_popularity']['viewed'][$product_id] = true;

        return true;
    }

    return false;
}

function fn_update_product_notifications($data)
{
    if (!empty($data['email']) && fn_validate_email($data['email'])) {
        Tygh::$app['session']['product_notifications']['email'] = $data['email'];
        if ($data['enable'] == 'Y') {
            db_query("REPLACE INTO ?:product_subscriptions ?e", $data);
            if (!isset(Tygh::$app['session']['product_notifications']['product_ids']) || (is_array(Tygh::$app['session']['product_notifications']['product_ids']) && !in_array($data['product_id'], Tygh::$app['session']['product_notifications']['product_ids']))) {
                Tygh::$app['session']['product_notifications']['product_ids'][] = $data['product_id'];
            }

            fn_set_notification('N', __('notice'), __('product_notification_subscribed'));
        } else {
            $deleted = db_query("DELETE FROM ?:product_subscriptions WHERE product_id = ?i AND user_id = ?i AND email = ?s", $data['product_id'], $data['user_id'], $data['email']);

            if (isset(Tygh::$app['session']['product_notifications']) && isset(Tygh::$app['session']['product_notifications']['product_ids']) && in_array($data['product_id'], Tygh::$app['session']['product_notifications']['product_ids'])) {
                Tygh::$app['session']['product_notifications']['product_ids'] = array_diff(Tygh::$app['session']['product_notifications']['product_ids'], array($data['product_id']));
            }

            if (!empty($deleted)) {
                fn_set_notification('N', __('notice'), __('product_notification_unsubscribed'));
            }
        }
    } else {
        fn_set_notification('W', __('warning'), 'E-mail adresa është e pavlefshme.');
    }
}

if ($mode == 'pv_gjanalytics') {
    if (ENVIRONMENT == 'live') {
        if ($_REQUEST['gja_view_data']) {
            send_view_to_gjirafa_analytics('POST', 'http://164.132.160.4:1500/gjblob/log?filePath=/home/gjirafa/Gjirafa50/ProductViews/' . date('Ymd') . '.txt' . '&content=' . urlencode($_REQUEST['gja_view_data']), 'true');
            exit;
        }
    }
}

if ($mode == 'delivery_dates') {
    $product_data = fn_get_product_data($_REQUEST['product_id'], $auth);

    $delivery_dates = fn_get_product_delivery_dates($product_data);

    Tygh::$app['view']->assign('product_data', $product_data);
    Tygh::$app['view']->assign('delivery_dates', $delivery_dates);
//        fn_set_notification('I', $product_data['product'], Tygh::$app['view']->fetch('views/products/components/product_delivery.tpl'));
    Registry::set('runtime.root_template', 'views/products/components/product_delivery.tpl');

//    exit(0);
}