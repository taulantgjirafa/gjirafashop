<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $redirect_url = '';

    if ($mode == 'auth') {
        $secret_key = 'asuihfa[opsdinfaosihernqwergj13094ujq4351q3np';
        $store_key = 'gj50';
        $key = $secret_key . $store_key;
        $request = file_get_contents('php://input');
        $auth = Tygh::$app['session']['auth'];
        $auth_id = Tygh::$app['session']['auth']['user_id'];

        $decoded = fn_decrypt_sso($request, $key);

        if ($decoded) {
            $user_id = fn_validate_sso_login($decoded->email);

            if (($user_id && $user_id != $auth_id) || ($user_id && !$auth_id)) {
                fn_login_user($user_id, true);

                header('Content-Type: application/json');
                echo 'success';
            }
        } else if (!$decoded && $auth_id) {
            fn_user_logout($auth);

            header('Content-Type: application/json');
            echo 'success';
        }

        exit();
    }

    return array(CONTROLLER_STATUS_OK, !empty($redirect_url)? $redirect_url : fn_url());
}

if ($mode == 'redirect') {
    header('Location: https://sso.gjirafa.com/Account/LogOff?returnUrl=' . $_REQUEST['returnUrl']);
    exit();
}

function fn_decrypt_sso($request, $key) {
    $request = json_decode($request);
    $token = str_replace('Token:', '', $request->token);

    if ($token) {
        $byte_array_token = unpack('C*', $token);
        $byte_array_token_reversed = array_reverse($byte_array_token);
        $jwt = implode(array_map('chr', $byte_array_token_reversed));

        JWT::$leeway = 60;

        try {
            $decoded = JWT::decode($jwt, $key, array('HS256'));
        } catch (ExpiredException $e) {
            fn_log_fatal_error([
                'type' => 'PHP_FATAL_ERROR',
                'message' => $e->getMessage(),
                'line' => 0
            ]);

            return false;
        }

        return $decoded;
    }

    return false;
}

function fn_validate_sso_login($email) {
    $user_id = fn_is_user_exists(0, array(
        'email' => $email
    ));

    return $user_id;
}