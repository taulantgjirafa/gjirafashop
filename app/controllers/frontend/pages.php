<?php

/***************************************************************************
 *                                                                          *
 *   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 *                                                                          *
 ****************************************************************************
 * PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
 * "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
 ****************************************************************************/

use Tygh\GjirafaSso;
use Tygh\Registry;
use Jumbojett\OpenIDConnectClient;
use Tygh\GjirafaIS;
use Tygh\Backend\Cache\Redis;
use Tygh\Http;

if (!defined('BOOTSTRAP')) {
    die('Access denied');
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if ($mode == 'change_currency') {
        setcookie('currency', $_POST['currency'], time() + (10 * 365 * 24 * 60 * 60), '/');
        $_COOKIE['currency'] = $_POST['currency'];

        return array(CONTROLLER_STATUS_REDIRECT, fn_url($_POST['redirect_url']));
    }

    return;
}

//
// View page details
//
if ($mode == 'view') {

    $_REQUEST['page_id'] = empty($_REQUEST['page_id']) ? $_REQUEST['id'] : $_REQUEST['page_id'];
    $preview = fn_is_preview_action($auth, $_REQUEST);
    $page = fn_get_page_data($_REQUEST['page_id'], CART_LANGUAGE, $preview);

    if (empty($page) || ($page['status'] == 'D' && !$preview)) {
        return array(CONTROLLER_STATUS_NO_PAGE);
    }

    if (!empty($page['meta_description']) || !empty($page['meta_keywords'])) {
        Tygh::$app['view']->assign('meta_description', $page['meta_description']);
        Tygh::$app['view']->assign('meta_keywords', $page['meta_keywords']);
    }

    // If page title for this page is exist than assign it to template
    if (!empty($page['page_title'])) {
        Tygh::$app['view']->assign('page_title', $page['page_title']);
    }

    $parent_ids = explode('/', $page['id_path']);
    foreach ($parent_ids as $p_id) {
        $_page = fn_get_page_data($p_id);
        fn_add_breadcrumb($_page['page'], ($p_id == $page['page_id']) ? '' : ($_page['page_type'] == PAGE_TYPE_LINK && !empty($_page['link']) ? $_page['link'] : "pages.view?page_id=$p_id"));
    }

    Tygh::$app['view']->assign('page', $page);
}

if ($mode == '1plus1gratis') {

    $list = array(array(187815, 75510), array(175289, 203126), array("204121a", 195842), array(200713, 178965), array(202648, 113192), array(172906, 164154), array(174987, 176409), array(158564, 143510), array(201534, 174755), array(209974, 186640), array(197960, 188719), array(175289, 203126), array(183573, 193825), array(201695, 175362), array(206129, 209233), array("199624a", 178974), array(180284, 199628), array(201859, 203882), array(201950, 164155), array(198350, 202397), array(211464, 55639), array("211357a", 168256), array(208872, 208375), array(213696, 125881), array(177387, 56686), array(192366, 151155), array(193760, 193785), array(110669, 125881), array(200033, 205330), array(199584, 208964), array(198365, 198368), array(185204, 189603), array(201938, 197756), array(182750, 182750), array(61283, 183194), array(178453, 179404), array(185076, 174484), array(185039, 176612), array(205810, 198209), array(113979, 75476), array(163849, 148692), array(200998, 199741), array(197762, 160812), array(174430, 174428), array(171339, 114956), array(201694, 176627), array(204356, 158882), array(171339, 171339), array(201003, 205990), array(177177, 168919), array(191651, 195226), array("187815a", 214167), array(209173, 210538), array(201859, 197824), array(190575, 206559), array(190541, 41290), array(197933, 190575), array(186909, 138019), array("175289a", 203864), array(171384, 210233), array(190410, 185249), array(205278, 169664), array(169664, 169664), array(204330, 190998), array(152890, 190998), array("210314a", 182553), array(204404, 204404), array(205988, "KO-032"), array(155334, 155334), array("KO-0014", 202397), array(176076, 176076), array(90366, 90366), array(186811, 186811), array(187799, 208964), array(198187, 96667), array(205052, 186682), array(211391, 183397), array(190198, 183395), array(202089, 170506), array("187519a", 114954), array(199624, 202689), array(205150, 197824), array(187519, 199494), array(160711, 160711), array(203816, 206872), array(198470, 190743), array(198747, 173290), array(171218, 160812), array(210314, 103206), array(148928, 148928), array(208340, 208340), array(210545, 211467), array("185076a", 196716), array(206783, 210570), array(203151, 179507), array(163238, 125881), array(192090, 149766), array(210299, 145527), array(211357, 195309));

    $products = db_get_array('
        select prod.product_id,
         prod.product_code,
        prod_desc.product, 
        prod.amount,
        prod_stock.czc_stock,
        prod_stock.czc_retailer_stock,
        prod_prices.price from ?:products as prod, 
        ?:product_descriptions as prod_desc, 
        ?:product_prices as prod_prices, ?:product_stock as prod_stock where prod.product_code
         IN (?a) and prod.product_id = prod_desc.product_id and prod.product_id = prod_prices.product_id and prod.product_id = prod_stock.product_id', array(187815, 175289, 204121, 204121, 200713, 202648, 172906, 174987, 158564, 201534, 209974, 197960, 175289, 183573, 201695, 206129, 199624, 180284, 201859, 201950, 198350, 211464, 211357, 208872, 213696, 177387, 192366, 193760, 110669, 200033, 199584, 198365, 185204, 201938, 182750, 61283, 178453, 185076, 185039, 205810, 113979, 163849, 200998, 197762, 174430, 171339, 201694, 204356, 171339, 201003, 177177, 199624, 191651, 187815, 209173, 201859, 190575, 190541, 197933, 186909, 175289, 171384, 190410, 205278, 169664, 204330, 152890, 210314, 204404, 205988, 155334, "KO-0014", 176076, 90366, 186811, 187799, 198187, 205052, 211391, 190198, 202089, 187519, 199624, 205150, 187519, 160711, 203816, 198470, 198747, 171218, 210314, 148928, 208340, 210545, 185076, 206783, 203151, 163238, 192090, 210299, 211357, 75510, 203126, 192641, 195842, 178965, 113192, 164154, 176409, 143510, 174755, 186640, 188719, 203126, 193825, 175362, 209233, 178974, 199628, 203882, 164155, 202397, 55639, 168256, 208375, 125881, 56686, 151155, 193785, 125881, 205330, 208964, 198368, 189603, 197756, 182750, 183194, 179404, 174484, 176612, 198209, 75476, 148692, 199741, 160812, 174428, 114956, 176627, 158882, 171339, 205990, 168919, 167855, 195226, 214167, 210538, 197824, 206559, 41290, 190575, 138019, 203864, 210233, 185249, 169664, 169664, 190998, 190998, 182553, 204404, "KO-032", 155334, 202397, 176076, 90366, 186811, 208964, 96667, 186682, 183397, 183395, 170506, 114954, 202689, 197824, 199494, 160711, 206872, 190743, 173290, 160812, 103206, 148928, 208340, 211467, 196716, 210570, 179507, 125881, 149766, 145527, 195309, "187815a", "175289a", "204121a", "199624a", "201859a", "211357a", "185076a", "171339a", "210314a", "187519a"));

    $u_products = array();
    foreach ($products as $product) {
        $u_products[$product['product_code']] = $product;
    }

    Tygh::$app['view']->assign('list', $list);
    Tygh::$app['view']->assign('products', $u_products);
}

if ($mode == 'oferta') {
    //
    //    $categories = db_get_array('select a.product_id, a.category_id, b.category from ?:products_categories as a, ?:category_descriptions as b where a.category_id = b.category_id');
    //    $cat2 = array();
    //    foreach($categories as $category){
    //        if(isset($cat2[$category['category_id']])){
    //            array_push($cat2[$category['category_id']], $category);
    //        }else
    //            $cat2[$category['category_id']][] = $category;
    //    }
    //$d = array();
    //    foreach($cat2 as $category){
    //        $s = $category[array_rand($category)];
    //        array_push($d, $s);
    //        $param = array(
    //            'product_id' => $s['product_id'],
    //            'count' => 1
    //        );
    //        echo "<div style='width:90px;float:left;'><img style='max-width:100%;' src='" . smarty_function_get_images_from_blob($param) . "'/>";
    //        echo $s['category'] . "</div>";
    //    }
    //
    //    echo json_encode($d);
    //
    //    exit;

    if (!apcu_exists('cm_obj') || empty(apcu_fetch('cm_obj'))) {
        $offers = db_get_array('SELECT conditions_hash FROM gjirafashop.gjshop_promotions t WHERE t.zone = "catalog" and conditions_hash like "products=%" LIMIT 501');
        $products = array();
        foreach ($offers as $offer) {
            $product = str_replace("products=", "", $offer['conditions_hash']);
            $a = explode(",", $product);
            if (count($a) > 1) {
                $products = array_merge_recursive($products, explode(',', $product));
            } else
                array_push($products, $product);
        }
        $obj = array('where' => [array("field" => "product_id", "values" => $products)], 'limit' => count($products));
        $products = (new elasticSearch())->sendObject($obj, array());

        $cm_obj = array(
            'products' => current($products)
        );
        //apcu_store('cm_obj', $cm_obj, 3600);
    }

    //$cm_obj = apcu_fetch('cm_obj');

    foreach ($cm_obj as $key => $view_obj) {
        Tygh::$app['view']->assign($key, $view_obj);
    }
    var_dump($cm_obj);
    exit;
}

if ($mode == 'cybermonday') {
    if (!apcu_exists('cm_obj') || empty(apcu_fetch('cm_obj'))) {

        $product_categorized = array();
        $elastic_array = array();
        $product_newprice = array();

        $products_obj = array_map(function ($product) {
            return array(
                'product_id' => (int)$product['0'],
                'product_code' => $product['1'],
                'new_price' => number_format((float)$product['2'], 2, '.', ''),
                'amazon_price' => $product['3'],
            );
        }, array_map('str_getcsv', file('feeds/cm_pc_n.csv')));


        for ($i = 0; $i < count($products_obj); $i++) {
            array_push($elastic_array, $products_obj[$i]['product_code']);
            //$product_categorized[$products_obj[$i]['category']][$products_obj[$i]['product_code']] = $products_obj[$i];
        }


        $obj = array('where' => [array("field" => "product_code", "values" => $elastic_array)], 'limit' => count($elastic_array));
        $elastic = new elasticSearch();
        $products = $elastic->sendObject($obj, array());

        $categories_img = array(
            'https://gjirafa50.com/gaming/',
            'https://gjirafa50.com/aksesore-7/altoparlante-2/portativ/',
            'https://gjirafa50.com/kompjutere-laptop/aksesore-per-laptop/canta/',
            'https://gjirafa50.com/tv-video-dhe-audio/tv/'
        );

        $cm_obj = array(
            'cm' => (new DateTime() > new DateTime("2017-11-25 00:00:00")) ? true : false,
            'cm_s' => (new DateTime() > new DateTime("2017-11-27 00:00:00")) ? true : false,
            'elastic_products' => $products[0],
            'csv_products' => $products_obj,
            'categories_img' => $categories_img
        );

        apcu_store('cm_obj', $cm_obj, 180);
    } else {

        $cm_obj = apcu_fetch('cm_obj');
    }

    foreach ($cm_obj as $key => $view_obj) {
        Tygh::$app['view']->assign($key, $view_obj);
    }
}

if ($mode == 'newyear-sale') {
    if (!apcu_exists('ny_sale') || empty(apcu_fetch('ny_sale'))) {

        $product_categorized = array();
        $elastic_array = array();
        $product_newprice = array();
        $product_code_products = array();

        $droga = array();

        $products_obj = array_map(function ($product) {
            return array(
                'product_code' => $product['0'],
                'new_price' => number_format((float)$product['1'], 2, '.', '')
            );
        }, array_map('str_getcsv', file('feeds/zbritjafundvitit.csv')));


        for ($i = 0; $i < count($products_obj); $i++) {
            array_push($elastic_array, $products_obj[$i]['product_code']);
            //$product_categorized[$products_obj[$i]['category']][$products_obj[$i]['product_code']] = $products_obj[$i];
        }


        // $obj = array('where' => [array("field" => "product_code", "values" => $elastic_array)], 'limit' => count($elastic_array));
        // $elastic = new elasticSearch();
        // $products = $elastic->sendObject($obj, "");

        $products = fn_query_elastic('product_code', $elastic_array, count($elastic_array), 0, 'values');

        foreach ($products[0] as $product) {
            $product_code_products[$product['product_code']] = $product;
        }

        foreach ($products_obj as $product) {
            $droga[$product_code_products[$product['product_code']]['product_id']] = array(
                'product_id' => $product_code_products[$product['product_code']]['product_id'],
                'product' => $product_code_products[$product['product_code']]['product'],
                'price' => (int)$product['new_price'] + 0.50
            );
        }

        $categories_img = array(
            'https://gjirafa50.com/gaming/',
            'https://gjirafa50.com/aksesore-7/altoparlante-2/portativ/',
            'https://gjirafa50.com/kompjutere-laptop/aksesore-per-laptop/canta/',
            'https://gjirafa50.com/tv-video-dhe-audio/tv/'
        );

        $cm_obj = array(
            'cm' => true,
            'cm_s' => true,
            'elastic_products' => $product_code_products,
            'csv_products' => $products_obj,
            'categories_img' => $categories_img
        );

        apcu_store('ny_sale', $cm_obj, 180);
    } else {

        $cm_obj = apcu_fetch('ny_sale');
    }

    foreach ($cm_obj as $key => $view_obj) {
        Tygh::$app['view']->assign($key, $view_obj);
    }
}

if ($mode == 'top-offers-2018') {
    if (!apcu_exists('to2018') || empty(apcu_fetch('to2018'))) {
        $page = 'top-offers-2018';

        $structured_csv = array();
        $elastic_array = array();
        $product_newprice = array();

        $config = array(
            'header_bg' => '111',
            'header_color' => 'fff',
            'cover_txt1' => '',
            'cover_txt2' => '',
            'heading_1' => '',
            'heading_2' => '',
            'buynow_color' => '000',
            'bg_color' => '2b2b2b',
            'percentage_bg_color' => '111',
            'percentage_color' => 'fff',
            'hide_prices' => true,
            'hide_prices_until' => '11/22/2018 11:59:59 PM'
        );

        if (!empty($config['offer_active']) && $config['offer_active'] == false) {
            return array(CONTROLLER_STATUS_REDIRECT, '/');
        }


        $products_obj = array_map(function ($product) {
            return array(
                'product_code' => $product['0'],
                'new_price' => number_format((float)$product['1'], 2, '.', ''),
                'category' => $product['2']
            );
        }, array_map('str_getcsv', file('feeds/' . $page . '.csv')));

        if ($products_obj == null)
            return CONTROLLER_STATUS_NO_PAGE;

        $categorized_products = fn_group_by_array('category', $products_obj);

        $combo = array(
            "190804" => "Kabllo InCharge MicroUSB 8cm, e zezë",
            "201937" => "USB 3.0, Belkin, 4 Porte Hub + kabllo USB-C",
            "193218" => "MyMAX mikro USB magnetik",
            "204093" => "Pllakë amë Biostar B250GT3 - Intel B250",
            "212488" => "Mass Effect: Andromeda - PC",
            "233311" => "Transmetues Google Chromecast 3",
            "245560" => "Dëgjuese Huawei AM61",
            "238164" => "Maus SteelSeries Sensei 310",
            "220273" => "Mousepad SteelSeries QcK +",
            "232227" => "Çantë për laptop Sleeve HP 14.1\"",
            "248531" => "Maus pa kabllo Apple Magic 2",
            "246374" => "Burim energjie Zalman ZM600-GSII",
            "239098" => "Monitor Dell Alienware AW2518HF - LED 25\"",
            "216882" => "Kabllo lidhese PremiumCord ,3,5mm ",
            "244179" => "Rrip për GoPro + kapëse",
            "212003" => "Kabllo OEM, HDMI-HDMI, 1.8m",
            "249243" => "Joystick SONY PS4 DualShock 4 v2",
            "198505" => "Dëgjuese Samsung, të zeza",
            "244344" => "Kontroller wireless Xbox ONE S",
            "246584" => "Matës aktiviteti Xiaomi Amazfit Cor",
            "246584" => "Maus SteelSeries Sensei 310"
        );

        for ($i = 0; $i < count($products_obj); $i++) {
            array_push($elastic_array, $products_obj[$i]['product_code']);
            $structured_csv[$products_obj[$i]['product_code']] = $products_obj[$i];
        }

        $obj = array('where' => [array("field" => "product_code", "values" => $elastic_array)], 'limit' => count($elastic_array));
        $elastic = new elasticSearch();
        $products = $elastic->sendObject($obj, array());

        $product_code_sorted = array();

        foreach ($products[0] as $product) {
            $product_code_sorted[$product['product_code']] = $product;
        }

        $offers = array('upto70', 'laptopweekend', 'begamer', 'stock', 'oneplusone', 'discount10', 'outlet', 'buildpc', 'feboffer');

        $cm_obj = array(
            'key' => $page,
            'hide_price' => ($config['hide_prices'] && new DateTime() < new DateTime($config['hide_prices_until'])) ? true : false,
            'elastic_products' => $product_code_sorted,
            'categories_img' => $categories_img,
            'csv_products' => $structured_csv,
            'categorized_products' => $categorized_products,
            'landing_config' => $config,
            'offers' => $offers,
            'combo' => $combo
        );
        apcu_store('to2018', $cm_obj, 180);
    } else {

        $cm_obj = apcu_fetch('to2018');
    }

    foreach ($cm_obj as $key => $view_obj) {
        Tygh::$app['view']->assign($key, $view_obj);
    }
}

if ($mode == 'terms-and-conditions') {

    $terms = __('terms_and_conditions_content');
    Tygh::$app['view']->assign('terms', $terms);
}
if ($mode == 'add-discussion') {
    if (empty($_SESSION['auth']['user_id'])) {
        return array(CONTROLLER_STATUS_REDIRECT, '/index.php');
    }

    $e_key = $_GET['e_key'];
    $order_id = $_GET['order_id'];
    $order_info = fn_get_order_info($order_id);
    $email = $order_info['email'];
    $e_key_check = md5("email_rating_security_hash_654sdf546fe.$email");

    if ($e_key != $e_key_check || $_SESSION['auth']['user_id'] != $order_info['user_id']) {
        return array(CONTROLLER_STATUS_REDIRECT, '/index.php');
    }

    $reviewed_products_data = fn_get_reviewed_products($_SESSION['auth']['user_id'], $order_info['products']);

    Tygh::$app['view']->assign('order_info', $order_info);
    Tygh::$app['view']->assign('reviewed_products_data', $reviewed_products_data);
}

if ($mode == 'black-friday') {
    return array(CONTROLLER_STATUS_NO_PAGE);

    if (!apcu_exists('bf_2018') || empty(apcu_fetch('bf_2018'))) {
        $page = 'black-friday';

        $structured_csv = array();
        $elastic_array = array();
        $product_newprice = array();

        $config = array(
            'header_bg' => '111',
            'header_color' => 'fff',
            'cover_txt1' => '',
            'cover_txt2' => '',
            'heading_1' => '',
            'heading_2' => '',
            'buynow_color' => '000',
            'bg_color' => '111',
            'percentage_bg_color' => '111',
            'percentage_color' => 'fff',
            'hide_prices' => true,
            'hide_prices_until' => '11/22/2018 11:59:59 PM'
        );

        if (!empty($config['offer_active']) && $config['offer_active'] == false) {
            return array(CONTROLLER_STATUS_REDIRECT, '/');
        }


        $products_obj = array_map(function ($product) {
            return array(
                'product_code' => $product['0'],
                'new_price' => number_format((float)$product['1'], 2, '.', ''),
                'category' => $product['2']
            );
        }, array_map('str_getcsv', file('feeds/' . $page . '.csv')));

        if ($products_obj == null)
            return CONTROLLER_STATUS_NO_PAGE;

        $categorized_products = fn_group_by_array('category', $products_obj);

        $combo = array();

        for ($i = 0; $i < count($products_obj); $i++) {
            array_push($elastic_array, $products_obj[$i]['product_code']);
            $structured_csv[$products_obj[$i]['product_code']] = $products_obj[$i];
        }

        $obj = array('where' => [array("field" => "product_code", "values" => $elastic_array)], 'limit' => count($elastic_array));
        $elastic = new elasticSearch();
        $products = $elastic->sendObject($obj, array());

        $product_code_sorted = array();

        foreach ($products[0] as $product) {
            $product_code_sorted[$product['product_code']] = $product;
        }

        $categories_img = array(
            'https://gjirafa50.com/gaming/',
            'https://gjirafa50.com/aksesore-7/altoparlante-2/portativ/',
            'https://gjirafa50.com/kompjutere-laptop/aksesore-per-laptop/canta/',
            'https://gjirafa50.com/tv-video-dhe-audio/tv/'
        );

        $cm_obj = array(
            'key' => $page,
            'hide_price' => ($config['hide_prices'] && new DateTime() < new DateTime($config['hide_prices_until'])) ? true : false,
            'elastic_products' => $product_code_sorted,
            'categories_img' => $categories_img,
            'csv_products' => $structured_csv,
            'categorized_products' => $categorized_products,
            'landing_config' => $config,
            'combo' => $combo
        );
        apcu_store('bf_2018', $cm_obj, 300);
    } else
        $cm_obj = apcu_fetch('bf_2018');

    foreach ($cm_obj as $key => $view_obj) {
        Tygh::$app['view']->assign($key, $view_obj);
    }
    Tygh::$app['view']->assign('is_mobile', ((fn_is_mobile()) ? true : false));
}

if ($mode == 'begamer') {

    if ($_REQUEST['range_min']) {
        $min = $_REQUEST['range_min'];
        $max = $_REQUEST['range_max'];
        if ($max == null) {
            $max = 1000000;
        }
    }
    if ($_REQUEST['sort']) {
        $sort = $_REQUEST['sort'];
    }
    $obj = array(
        'where' =>
            array(
                array(
                    'field' => 'category_ids',
                    'value' => 2055,
                ),
                array(
                    'field' => 'price',
                    'range' =>
                        array(
                            'gte' => $min,
                            'lte' => $max,
                        ),
                ),
            ),
        'search' => NULL,
        'offset' => (isset($_GET['offset'])) ? (int)$_GET['offset'] : 0,
        'limit' => (isset($_GET['limit'])) ? (int)$_GET['limit'] : 132,

        'sort' => array(
            'field' => (isset($sort)) ? 'price' : '',
            'order' => (isset($sort)) ? $sort : '',
        ),
        'filters' => true,
        'store' => 'ks',
    );

    $elastic = new elasticSearch();
    $products = $elastic->sendObject($obj, array(), 'Gamer');

    if ($_GET['isAjax'] == 1) {
        Tygh::$app['view']->assign('products', $products[0]);
        Tygh::$app['view']->assign('isAjax', true);
        Registry::set('runtime.root_template', 'views/pages/begamer.tpl');
    } else {
        $config = json_decode("{\"header_bg\" : \"111\",\"header_color\" : \"fff\",\"cover_txt1\" : \"Blej 1 gaming produkt dhe bonu pjesë e lojës shpërblyese\",\"cover_txt2\" : \"Loja fillon me 21 mars dhe zgjat deri me 31 mars <br/> *Fituesi zgjedhet në Facebook të gjirafa50 me LIVE VIDEO\",\"heading_1\" : \"\",\"heading_2\" : \"\",\"buynow_color\" : \"000\",\"bg_color\" : \"111\",\"percentage_bg_color\" : \"111\",\"percentage_color\" : \"fff\",\"hide_prices\" : false,\"hide_prices_until\" : \"02/01/2018 6:00 PM\"}", true);

        $categories_img = array(
            'razer',
            'steelseries',
            'zowie',
            'msi',
            'corsair',
            'rog'
        );

        $cm_obj = array(
            'key' => 'begamer',
            'hide_price' => ($config['hide_prices'] && new DateTime() < new DateTime($config['hide_prices_until'])) ? true : false,
            'is_mobile' => (fn_is_mobile()) ? true : false,
            'landing_config' => $config,
            'products' => $products[0],
            'isAjax' => false,
            'categories_img' => $categories_img
        );

        foreach ($cm_obj as $key => $view_obj) {
            Tygh::$app['view']->assign($key, $view_obj);
        }

        Tygh::$app['view']->assign('selected_sort', $sort);
    }
}

if ($mode == 'bonugamer') {

    if ($_REQUEST['range_min']) {
        $min = $_REQUEST['range_min'];
        $max = $_REQUEST['range_max'];
        if ($max == null) {
            $max = 1000000;
        }
    }
    if ($_REQUEST['sort']) {
        $sort = $_REQUEST['sort'];
    }
    $obj = array(
        'where' =>
            array(
                array(
                    'field' => 'category_ids',
                    'value' => 2055,
                ),
                array(
                    'field' => 'price',
                    'range' =>
                        array(
                            'gte' => $min,
                            'lte' => $max,
                        ),
                ),
            ),
        'search' => NULL,
        'offset' => (isset($_GET['offset'])) ? (int)$_GET['offset'] : 0,
        'limit' => (isset($_GET['limit'])) ? (int)$_GET['limit'] : 64,

        'sort' => array(
            'field' => (isset($sort)) ? 'price' : '',
            'order' => (isset($sort)) ? $sort : '',
        ),
        'filters' => true,
        'store' => 'ks',
    );

    $elastic = new elasticSearch();
    $products = $elastic->sendObject($obj, array(''), 'Gamer');

    if ($_GET['isAjax'] == 1) {
        Tygh::$app['view']->assign('products', $products[0]);
        Tygh::$app['view']->assign('isAjax', true);
        Registry::set('runtime.root_template', 'views/pages/begamer.tpl');
    } else {
        $config = json_decode("{\"header_bg\" : \"0b3c4d\",\"header_color\" : \"fff\",\"cover_txt1\" : \"\",\"cover_txt2\" : \"\",\"heading_1\" : \"\",\"heading_2\" : \"\",\"buynow_color\" : \"0b3c4d\",\"bg_color\" : \"0b3c4d\",\"percentage_bg_color\" : \"0b3c4d\",\"percentage_color\" : \"fff\",\"hide_prices\" : false,\"hide_prices_until\" : \"02/01/2018 6:00 PM\"}", true);

        $categories_img = array(
            'razer',
            'steelseries',
            'zowie',
            'msi',
            'corsair',
            'rog'
        );

        $cm_obj = array(
            'key' => 'bonugamer',
            'hide_price' => ($config['hide_prices'] && new DateTime() < new DateTime($config['hide_prices_until'])) ? true : false,
            'is_mobile' => (fn_is_mobile()) ? true : false,
            'landing_config' => $config,
            'products' => $products[0],
            'isAjax' => false,
            'categories_img' => $categories_img
        );

        foreach ($cm_obj as $key => $view_obj) {
            Tygh::$app['view']->assign($key, $view_obj);
        }

        Tygh::$app['view']->assign('selected_sort', $sort);
    }
}

if ($mode == 'landing') {

    $page = $_GET['id'];
    if (!apcu_exists('custom_landing_' . $page) || empty(apcu_fetch('custom_landing_' . $page))) {

        $structured_csv = array();
        $elastic_array = array();
        $product_newprice = array();

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, "https://hhstsyoejx.gjirafa.net/gj50/landings/" . $page . "/config.txt");
        $result = curl_exec($ch);
        curl_close($ch);

        $config = json_decode($result, true);

        if (!empty($config['offer_active']) && $config['offer_active'] == false) {
            return array(CONTROLLER_STATUS_REDIRECT, '/');
        }


        $products_obj = array_map(function ($product) {
            return array(
                'product_code' => $product['0'],
                'new_price' => number_format((float)$product['1'], 2, '.', '')
            );
        }, array_map('str_getcsv', file('feeds/' . $page . '.csv')));

        if ($products_obj == null)
            return CONTROLLER_STATUS_NO_PAGE;

        $combo = array(
            "233943" => "Mass Effect: Andromeda - PC",
            "240556" => "Kufje për gaming SteelSeries Siberia 100, të zeza",
            "202629" => "Pllakë amë Biostar H110MHC - Intel H110",
            "234585" => "Evolveo Android Media Player Box H4 ",
            "222138" => "Maus optik gaming Trust GXT 148, i zi",
            "228587" => "Levë shpejtësie Driving Force Logitech ",
            "212749" => "ADATA PT100 10000mAh ",
            "224732" => "Shiriti NZXT HUE+ LED 30cm +dy kabllo 30cm dhe 10cm",
            "219839" => "Tastierë Corsair Gaming K65 RGB RAPIDFIRE, Cherry MX Speed, NA ",
            "177443" => "Transmetues i radios FM Hyundai FMT 419",
            "215997gj50" => "Fotoaparat Canon PowerShot SX430 IS",
            "216070" => "Disk SSD A-Data SU800, 256 gigabytes",
            "213959gj50" => "Orë e mençur IMMAX SW7, e zezë",
            "231678gj50" => "Dëgjuese HP H3200, e gjelbërt",
            "217107" => "Maus Gaming Zowie by FK1 BenQ, i zi ",
            "210956" => "Altoparlant System Yamaha TSX-70, kafe ",
            "182483" => "Kufje Plantronics Audio 355",
            "148120" => "USB Kingston DataTraveler microDuo, USB 3.0 - 16GB ",
            "218231" => "Mbrojtëse Krusell TUMBA 2 CARD për Apple iPhone X, e kaftë",
            "154673" => "Battlefield 4 - Xbox ONE",
            "211067" => "Sistem zërimi Panasonic SC-BTT465EGK",
            "203849" => "Tablet për lexim Amazon Kindle 8 Touch 2016, i zi",
            "209693" => "Set Tastierë dhe Maus pa kabllo Dell KM636, CZ",
            "219328" => "Monitor AOC G2460PF - LED 24\" "
        );

        for ($i = 0; $i < count($products_obj); $i++) {
            array_push($elastic_array, $products_obj[$i]['product_code']);
            $structured_csv[$products_obj[$i]['product_code']] = $products_obj[$i];
        }

        $obj = array('where' => [array("field" => "product_code", "values" => $elastic_array)], 'limit' => count($elastic_array));
        $elastic = new elasticSearch();
        $products = $elastic->sendObject($obj, array());

        $product_code_sorted = array();

        foreach ($products[0] as $product) {
            $product_code_sorted[$product['product_code']] = $product;
        }

        $categories_img = array(
            'https://gjirafa50.com/gaming/',
            'https://gjirafa50.com/aksesore-7/altoparlante-2/portativ/',
            'https://gjirafa50.com/kompjutere-laptop/aksesore-per-laptop/canta/',
            'https://gjirafa50.com/tv-video-dhe-audio/tv/'
        );


        $cm_obj = array(
            'key' => $page,
            'hide_price' => ($config['hide_prices'] && new DateTime() < new DateTime($config['hide_prices_until'])) ? true : false,
            'elastic_products' => $product_code_sorted,
            'categories_img' => $categories_img,
            'csv_products' => $structured_csv,
            'landing_config' => $config,
            'combo' => $combo,
            'boosted_products' => boosted_products()
        );
        $a = 0;
        //apcu_store('custom_landing_' . $page, $cm_obj, 600);

    } else {

        $cm_obj = apcu_fetch('custom_landing_' . $page);
    }

    foreach ($cm_obj as $key => $view_obj) {
        Tygh::$app['view']->assign($key, $view_obj);
    }
    Tygh::$app['view']->assign('is_mobile', ((fn_is_mobile()) ? true : false));
}


if ($mode == 'brands') {

    $params = $_REQUEST;
    if ($_REQUEST['range_min']) {
        $min = $_REQUEST['range_min'];
        $max = $_REQUEST['range_max'];
        if ($max == null) {
            $max = 1000000;
        }
    }
    if ($_REQUEST['sort']) {
        $sort = $_REQUEST['sort'];
    }
    if ($_REQUEST['category_id'] && $_REQUEST['category_id'] != 'none') {
        $category_id = $_REQUEST['category_id'];
    }
    $brand_name = $_REQUEST['brand'];
    $page = $_REQUEST['page'];
    $is_ajax = $_REQUEST['isAjax'];
    $brand_val = $_REQUEST['brand_value'];
    $selected_layout = fn_get_products_layout($params);
    if ($page == null) {
        $page = 1;
    }
    if ($brand_val == null) {
        $brand_val = 771;
    }
    if (empty($brand_name)) {
        if (apcu_exists('all_brands'))
            $brands = apcu_fetch('all_brands');
        else {
            $brands = db_get_array('select a.variant_id, trim(b.variant) as variant, substr(trim(UPPER(b.variant)),1,1) as letter FROM ?:product_feature_variants as a, ?:product_feature_variant_descriptions as b where a.feature_id = 12 and b.variant_id = a.variant_id group by variant order by letter');
            apcu_store('all_brands', $brands, 0);
        }
        Tygh::$app['view']->assign('brands', $brands);
    } else {
        if (apcu_exists('brand_' . $brand_name . '_page_' . $page)) {
            $products = apcu_fetch('brand_' . $brand_name . '_page_' . $page);
            if (!is_array($products[0]))
                return false;
        } else {
            $variants = db_get_array("select fv.variant_id from ?:product_feature_variants as fv left join ?:product_feature_variant_descriptions as fd on fv.variant_id = fd.variant_id where feature_id = 12 and variant like ?l", $brand_name);
            foreach ($variants as $variant) {
                $variant_ids[] = $variant['variant_id'];
            }
            if (isset($brand_name) && $variant != "") {
                $obj = array(
                    'where' =>
                        array(
                            array(
                                'field' => 'features.12',
                                'values' => $variant_ids,
                            ),
                            array(
                                'field' => 'category_ids',
                                'value' => ($_REQUEST['category_id'] != 'none' ? $_REQUEST['category_id'] : ''),
                            ),
                            array(
                                'field' => 'price',
                                'range' =>
                                    array(
                                        'gte' => $min,
                                        'lte' => $max,
                                    ),
                            ),
                        ),
                    'offset' => (isset($_GET['offset'])) ? (int)$_GET['offset'] : 0,
                    'limit' => (isset($_GET['limit'])) ? (int)$_GET['limit'] : 20,
                    'sort' => array(
                        'field' => (isset($sort)) ? 'price' : '',
                        'order' => (isset($sort)) ? $sort : '',
                    ),
                    'filters' => true,
                );

                $brand_images = array(
                    'id' => current($variant_ids),
                    'image' => 'https://hhstsyoejx.gjirafa.net/gj50/brand_images/' . current($variant_ids) . '.png'
                );

                $elastic = new elasticSearch();
                $products = $elastic->sendObject($obj, array());
                apcu_store('brand_' . $brand_name . '_page_' . $page, $products, 0);
            } else {
                return array(CONTROLLER_STATUS_REDIRECT, '/index.php');
            }
        }

        if (isset($brand_name)) {
            if (apcu_exists('brand_' . $brand_name . '_page_' . $page)) {
                $subs_fetched = apcu_fetch('brand_' . $brand_name . '_page_' . $page);
                $subs = $subs_fetched[1]['subcategory_filters'];
            } else {
                $subs = $products[1]['subcategory_filters'];
            }
        }
        foreach ($subs as $key => $val) {
            $subs[$key]['image'] = fn_get_subcat_img($subs[$key]['category_id']);
        }


        Tygh::$app['view']->assign('subcategories', $subs);
        Tygh::$app['view']->assign('isAjax', $is_ajax);
        Tygh::$app['view']->assign('selected_layout', $selected_layout);
        Tygh::$app['view']->assign('brand_images', $brand_images);
        Tygh::$app['view']->assign('products', $products[0]);
    }
}

function boosted_products()
{
    if (!apcu_exists('boosted_products') || empty(apcu_fetch('boosted_products'))) {

        $structured_csv = array();
        $elastic_array = array();
        $product_newprice = array();

        $products_obj = array_map(function ($product) {
            return array(
                'product_code' => $product['0'],
                'new_price' => number_format((float)$product['1'], 2, '.', '')
            );
        }, array_map('str_getcsv', file('feeds/boosted_products.csv')));

        if ($products_obj == null)
            return NULL;

        for ($i = 0; $i < count($products_obj); $i++) {
            array_push($elastic_array, $products_obj[$i]['product_code']);
            $structured_csv[$products_obj[$i]['product_code']] = $products_obj[$i];
        }

        $obj = array('where' => [array("field" => "product_code", "values" => $elastic_array)], 'limit' => count($elastic_array));
        $elastic = new elasticSearch();
        $products = $elastic->sendObject($obj, "");

        $product_code_sorted = $products[0];

        apcu_store('boosted_products', $product_code_sorted, 600);
    } else {
        $product_code_sorted = apcu_fetch('boosted_products');
    }

    return $product_code_sorted;
}

//S9


if ($mode == 'samsung-galaxy-note9') {

    $page = 'samsung-galaxy-note9';
    if (!apcu_exists('custom_landing_' . $page) || empty(apcu_fetch('custom_landing_' . $page))) {

        $structured_csv = array();
        $elastic_array = array();
        $product_newprice = array();
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, "https://hhstsyoejx.gjirafa.net/gj50/landings/" . $page . "/config.txt");
        $result = curl_exec($ch);
        curl_close($ch);

        $config = json_decode($result, true);

        if ($config['offer_active'] == false) {
            return array(CONTROLLER_STATUS_REDIRECT, '/');
        }

        $products_obj = array_map(function ($product) {
            return array(
                'product_code' => $product['0'],
                'new_price' => number_format((float)$product['1'], 2, '.', '')
            );
        }, array_map('str_getcsv', file('feeds/' . $page . '.csv')));

        if ($products_obj == null)
            return CONTROLLER_STATUS_NO_PAGE;

        $combo = array(
            //            222802 => 'Orë SMART TCL Moveband 2',
            //            222115 => 'Amazon Kindle 8 Touch 2016',
            //            222433 => 'Kufje Apple Beats EP, të zeza',
            //            222520 => 'Altoparlant JBL GO'
        );

        for ($i = 0; $i < count($products_obj); $i++) {
            array_push($elastic_array, $products_obj[$i]['product_code']);
            $structured_csv[$products_obj[$i]['product_code']] = $products_obj[$i];
        }

        $obj = array('where' => [array("field" => "product_code", "values" => $elastic_array)], 'limit' => count($elastic_array));
        $elastic = new elasticSearch();
        $products = $elastic->sendObject($obj, "");

        $product_code_sorted = array();

        foreach ($products[0] as $product) {
            $product_code_sorted[$product['product_code']] = $product;
        }

        $categories_img = array(
            'https://gjirafa50.com/gaming/',
            'https://gjirafa50.com/aksesore-7/altoparlante-2/portativ/',
            'https://gjirafa50.com/kompjutere-laptop/aksesore-per-laptop/canta/',
            'https://gjirafa50.com/tv-video-dhe-audio/tv/'
        );


        $config = json_decode($result, true);

        $cm_obj = array(
            'key' => $page,
            'hide_price' => ($config['hide_prices'] && new DateTime() < new DateTime($config['hide_prices_until'])) ? true : false,
            'elastic_products' => $product_code_sorted,
            'categories_img' => $categories_img,
            'csv_products' => $structured_csv,
            'landing_config' => $config,
            'combo' => $combo,
            'boosted_products' => boosted_products()
        );

        apcu_store('custom_landing_' . $page, $cm_obj, 120);
    } else {

        $cm_obj = apcu_fetch('custom_landing_' . $page);
    }

    foreach ($cm_obj as $key => $view_obj) {
        Tygh::$app['view']->assign($key, $view_obj);
    }
    Tygh::$app['view']->assign('is_mobile', ((fn_is_mobile()) ? true : false));
}

if ($mode == 'related') {

    $results = db_get_array('select a.name, b.parent_code, b.related_code from gjshop_product_recommended_categories as a, gjshop_product_recommended as b where a.id in (160,230,231,232,233,271,272,394,395,396,403,406,409,410,446,456,461,466,467,468,469,480,481,509,580,585,597,835,836,869,870,3488,3536,3626,3685,6409,6421,6573,8669,8699,8702) and a.id = b.category_id');
    $result = array();
    $products = array();
    foreach ($results as $element) {
        $result[$element['name']][$element['parent_code']][] = array(
            'related_code' => $element['related_code']
        );
    }

    $obj = array('where' => [array("field" => "product_code", "values" => array_keys($result))], 'limit' => count($result));
    ($elastic = new elasticSearch())->sendObject($obj, array());


    Tygh::$app['view']->assign('related', $result);
}

if ($mode == 'calendar' || $mode == 'quick_view') {

    if ($mode == 'quick_view') {
        if (defined('AJAX_REQUEST')) {
            // fn_prepare_product_quick_view($_REQUEST);
            Registry::set('runtime.root_template', 'views/pages/components/quick_view.tpl');
            Tygh::$app['view']->assign('whatever', 'asdasdasdasd');
        } else {
            return array(CONTROLLER_STATUS_REDIRECT, 'products.view?product_id=' . $_REQUEST['product_id']);
        }
    }
}

if ($mode == 'msi-landing' || $mode == 'newyear-landing') {

    $filter = false;

    if ($mode == 'msi-landing') {
        $categories = array(
            '1' => 'Kompjuterë, laptopë',
            '2' => 'Kartela grafike',
            '3' => 'Maus',
            '4' => 'Tastiera',
            '5' => 'Monitorë',
            '6' => 'Pllaka Amë',
            '7' => 'Aksesorë'
        );
    } elseif ($mode == 'newyear-landing') {
        $categories = array(
            '1' => 'Kompjuterë, laptopë',
            '2' => 'Kartela grafike',
            '3' => 'Maus',
            '4' => 'Tastiera',
            '5' => 'Monitorë',
            '6' => 'Pllaka Amë',
            '7' => 'Aksesorë'
        );
    }

    $categories_map = fn_map_categories($categories);

    $categories_seo_map = $categories_map[0];
    $categories = $categories_map[1];

    if (isset($_GET['kategoria'])) {
        $filter = true;

        $category_id = fn_parse_category_id($categories_seo_map, $_GET['kategoria']);
    }

    $csv_products = fn_get_csv_data("feeds/$mode.csv", array('product_code', 'category', 'priority'));

    usort($csv_products, function ($a, $b) {
        return $a['priority'] <=> $b['priority'];
    });

    if ($filter) {
        $product_codes = fn_filter_csv_products($csv_products, $category_id);
    } else {
        $all_product_codes = fn_get_csv_products($csv_products, ($mode == 'newyear-landing' ? true : false));
        $product_codes = $all_product_codes[0];
        $priority_product_codes = $all_product_codes[1];

        if (isset($all_product_codes[2])) {
            $priority_product_codes_2 = $all_product_codes[2];
        }
    }

    if ($_GET['isAjax'] == 1) {
        $product_codes_sliced = $product_codes;
    } else {
        // $product_codes_sliced = array_slice($product_codes, 0, 100);
        // $product_codes_sliced = array_slice($product_codes_sliced, 0, (100 - fn_closest_number(count($priority_product_codes), 4)) + count($priority_product_codes));
        $product_codes_sliced = $product_codes;
    }

    $no_products = false;

    if ($product_codes_sliced) {
        $elastic_products = fn_query_elastic('product_code', $product_codes_sliced, (isset($_GET['limit'])) ? (int)$_GET['limit'] : 50, (isset($_GET['offset'])) ? (int)$_GET['offset'] : 0, 'values');

        $products = fn_extract_product_data($elastic_products, $priority_product_codes, $auth);

        if (fn_isAL()) {
            if (empty($products)) {
                $no_products = true;
            }

            $products = fn_sort_array_by_key($products, 'total_stock', SORT_DESC);
        }
    } else {
        $no_products = true;
    }

    if ($priority_product_codes_2) {
        $priority_products_2 = fn_query_elastic('product_code', $priority_product_codes_2, 50, 0, 'values');
        $priority_products_2 = $priority_products_2[0];
    }

    if (isset($priority_product_codes)) {
        Tygh::$app['view']->assign('priority_products', $priority_product_codes);
    }
    if (isset($priority_product_codes_2)) {
        Tygh::$app['view']->assign('priority_products_2', $priority_products_2);
    }
    if (isset($priority_product_codes_2)) {
        Tygh::$app['view']->assign('priority_product_codes_2', $priority_product_codes_2);
    }
    if ($filter) {
        Tygh::$app['view']->assign('hide_show_more', true);
    }
    Tygh::$app['view']->assign('categories', $categories);

    if ($_GET['isAjax'] == 1) {
        Tygh::$app['view']->assign('product_count', count($elastic_products));
        Tygh::$app['view']->assign('products', $products);
        Tygh::$app['view']->assign('isAjax', true);
        Registry::set('runtime.root_template', 'views/pages/landing/products.tpl');
    } else {
        Tygh::$app['view']->assign('products', $products);
        Tygh::$app['view']->assign('no_products', $no_products);
    }
}

if ($mode == 'weekly-deals') {

    $csv_data = fn_get_csv_data('feeds/weekly-deals.csv', array('product_code'));

    if (!apcu_exists('weekly_deals_csv_products') || empty(apcu_fetch('weekly_deals_csv_products'))) {
        $csv_products = fn_get_csv_products($csv_data);

        apcu_add('weekly_deals_csv_products', $csv_products);
    } else {
        $csv_products = apcu_fetch('weekly_deals_csv_products');
    }

    $product_codes = $csv_products[0];

    if (!apcu_exists('weekly_deals_elastic_products') || empty(apcu_fetch('weekly_deals_elastic_products'))) {
        $elastic_products = fn_query_elastic('product_code', $product_codes, count($product_codes), 0, 'values');

        apcu_add('weekly_deals_elastic_products', $elastic_products);
    } else {
        $elastic_products = apcu_fetch('weekly_deals_elastic_products');
    }

    $products = fn_add_sort_key($elastic_products, 'category_sort', 'brand_sort');

    $grouped_products_cat = fn_group_by($products, 'category_sort');
    $grouped_products_brand = fn_group_by($products, 'brand_sort');
    $grouped_products_cat = fn_pop_elements($grouped_products_cat, 1); // limits categories to 11 products
    $grouped_products_brand = fn_pop_elements($grouped_products_brand, 1); // limits categories to 11 products
    ksort($grouped_products_cat, SORT_NUMERIC);
    ksort($grouped_products_brand, SORT_NUMERIC);

    $gp_cat_keys = array_keys($grouped_products_cat);
    $gp_brand_keys = array_keys($grouped_products_brand);

    if (!apcu_exists('weekly_deals_categories') || empty(apcu_fetch('weekly_deals_categories'))) {
        $categories = fn_get_category_names($gp_cat_keys);

        apcu_add('weekly_deals_categories', $categories);
    } else {
        $categories = apcu_fetch('weekly_deals_categories');
    }

    if (!apcu_exists('weekly_deals_brands') || empty(apcu_fetch('weekly_deals_brands'))) {
        $brands = fn_get_brand_names($gp_brand_keys);

        apcu_add('weekly_deals_brands', $brands);
    } else {
        $brands = apcu_fetch('weekly_deals_brands');
    }

    $brand_group = array_map(function ($key, $val) {
        return array($key => $val);
    }, $brands, $grouped_products_brand);
    $grouped_products_brand = array_merge_recursive(...$brand_group);

    if (array_key_exists('Të tjera', $grouped_products_brand)) {
        $elem = $grouped_products_brand['Të tjera'];
        unset($grouped_products_brand['Të tjera']);
        $grouped_products_brand['Të tjera'] = $elem;
    }

    $brands = array_keys($grouped_products_brand);
    $mapped_categories = fn_map_categories($categories);
    $mapped_categories = $mapped_categories[0];
    $mapped_brands = fn_map_categories($brands);
    $mapped_brands = $mapped_brands[0];

    Tygh::$app['view']->assign('categories', $categories);
    Tygh::$app['view']->assign('brands', $brands);

    Tygh::$app['view']->assign('mapped_categories', json_encode($mapped_categories));
    Tygh::$app['view']->assign('mapped_brands', json_encode($mapped_brands));

    if (defined('AJAX_REQUEST')) {
        if ($_GET['filter'] == 'category') {
            Tygh::$app['view']->assign('item_names', $categories);
            Tygh::$app['view']->assign('group', $grouped_products_cat);
        } elseif ($_GET['filter'] == 'brand') {
            Tygh::$app['view']->assign('item_names', $brands);
            Tygh::$app['view']->assign('group', $grouped_products_brand);
        }

        Tygh::$app['view']->assign('filter', $_GET['filter']);

        Registry::set('runtime.root_template', 'views/pages/weekly-deals/slider.tpl');
    } else {
        if ($_GET['filter'] == 'category') {
            Tygh::$app['view']->assign('item_names', $categories);
            Tygh::$app['view']->assign('group', $grouped_products_cat);
        } elseif ($_GET['filter'] == 'brand') {
            Tygh::$app['view']->assign('item_names', $brands);
            Tygh::$app['view']->assign('group', $grouped_products_brand);
        } else {
            Tygh::$app['view']->assign('item_names', $categories);
            Tygh::$app['view']->assign('group', $grouped_products_cat);
        }
    }
}

if ($mode == 'steelseries-landing') {

    $filter = false;
    $categories = array(
        '1' => 'Maus',
        '2' => 'Mauspad',
        '3' => 'Tastiera',
        '4' => 'Kufje',
        '5' => 'Kontroller'
    );
    $colors = array(
        '1' => 'e736a9',
        '2' => 'd9333f',
        '3' => 'ceab4c',
        '4' => 'bad925',
        '5' => 'ca7035'
    );

    $categories_map = fn_map_categories($categories);

    $categories_seo_map = $categories_map[0];
    $categories = $categories_map[1];

    if (isset($_GET['kategoria'])) {
        $filter = true;

        $category_id = fn_parse_category_id($categories_seo_map, $_GET['kategoria']);
    }

    $csv_products = fn_get_csv_data('feeds/steelseries-landing.csv', array('product_code', 'category', 'priority'));

    usort($csv_products, function ($a, $b) {
        return $a['priority'] <=> $b['priority'];
    });

    if ($filter) {
        $product_codes = fn_filter_csv_products($csv_products, $category_id);
    } else {
        $all_product_codes = fn_get_csv_products($csv_products);
        $product_codes = $all_product_codes[0];
        $priority_product_codes = $all_product_codes[1];
    }

    if ($_GET['isAjax'] == 1) {
        $product_codes_sliced = $product_codes;
    } else {
        // $product_codes_sliced = array_slice($product_codes, 0, 100);
        // $product_codes_sliced = array_slice($product_codes_sliced, 0, (100 - fn_closest_number(count($priority_product_codes), 4)) + count($priority_product_codes));
        $product_codes_sliced = $product_codes;
    }

    $no_products = false;

    if ($product_codes_sliced) {
        $elastic_products = fn_query_elastic('product_code', $product_codes_sliced, (isset($_GET['limit'])) ? (int)$_GET['limit'] : 1000, (isset($_GET['offset'])) ? (int)$_GET['offset'] : 0, 'values');

        $products = fn_extract_product_data($elastic_products, $priority_product_codes, $auth);

        if (fn_isAL()) {
            if (empty($products)) {
                $no_products = true;
            }

            $products = fn_sort_array_by_key($products, 'brand', SORT_DESC);
        }
    } else {
        $no_products = true;
    }

    if (isset($priority_product_codes)) {
        Tygh::$app['view']->assign('priority_products', $priority_product_codes);
    }
    if ($filter) {
        Tygh::$app['view']->assign('hide_show_more', true);
    }
    Tygh::$app['view']->assign('categories', $categories);
    Tygh::$app['view']->assign('colors', $colors);

    if ($_GET['isAjax'] == 1) {
        Tygh::$app['view']->assign('product_count', count($elastic_products));
        Tygh::$app['view']->assign('products', $products);
        Tygh::$app['view']->assign('isAjax', true);
        Registry::set('runtime.root_template', 'views/pages/msi-landing/products.tpl');
    } else {
        Tygh::$app['view']->assign('products', $products);
        Tygh::$app['view']->assign('no_products', $no_products);
    }
}

if ($mode == 'benq-zowie') {

    $filter = false;
    $brand = 'zowie';
    $zowie_categories = array(
        '1' => 'Maus',
        '2' => 'Monitorë',
        '3' => 'Tastiera',
        // '4' => 'Kufje'
    );
    $benq_categories = array(
        '1' => 'Monitorë',
        '2' => 'Projektorë',
        '3' => 'Business displays'
    );

    $zowie_categories_map = fn_map_categories($zowie_categories);
    $zowie_categories_seo_map = $zowie_categories_map[0];
    $zowie_categories = $zowie_categories_map[1];

    $benq_categories_map = fn_map_categories($benq_categories);
    $benq_categories_seo_map = $benq_categories_map[0];
    $benq_categories = $benq_categories_map[1];

    if (isset($_GET['kategoria'])) {
        $filter = true;
        $brand = ltrim(strrchr($_GET['kategoria'], '-'), '-');
        $req_category = rtrim(rtrim($_GET['kategoria'], $brand), '-');

        if ($brand == 'zowie') {
            $categories_seo_map = $zowie_categories_seo_map;
        } elseif ($brand == 'benq') {
            $categories_seo_map = $benq_categories_seo_map;
        }

        $category_id = fn_parse_category_id($categories_seo_map, $req_category);
    }

    if (isset($_GET['brendi'])) {
        $brand = $_GET['brendi'];
    }

    $csv_products = fn_get_csv_data('feeds/' . $brand . '-landing.csv', array('product_code', 'category', 'priority'));

    usort($csv_products, function ($a, $b) {
        return $a['priority'] <=> $b['priority'];
    });

    if ($filter) {
        $product_codes = fn_filter_csv_products($csv_products, $category_id);
    } else {
        $all_product_codes = fn_get_csv_products($csv_products);
        $product_codes = $all_product_codes[0];
        $priority_product_codes = $all_product_codes[1];
    }

    if ($_GET['isAjax'] == 1) {
        $product_codes_sliced = $product_codes;
    } else {
        $product_codes_sliced = $product_codes;
    }

    $no_products = false;

    if ($product_codes_sliced) {
        $elastic_products = fn_query_elastic('product_code', $product_codes_sliced, (isset($_GET['limit'])) ? (int)$_GET['limit'] : 1000, (isset($_GET['offset'])) ? (int)$_GET['offset'] : 0, 'values');

        $products = fn_extract_product_data($elastic_products, $priority_product_codes, $auth);

        if (fn_isAL()) {
            if (empty($products)) {
                $no_products = true;
            }

            $products = fn_sort_array_by_key($products, 'total_stock', SORT_DESC);
        }
    } else {
        $no_products = true;
    }

    if (isset($priority_product_codes)) {
        Tygh::$app['view']->assign('priority_products', $priority_product_codes);
    }
    if ($filter) {
        Tygh::$app['view']->assign('hide_show_more', true);
        Tygh::$app['view']->assign('active_category', $req_category);
    }
    // Tygh::$app['view']->assign('categories', $categories);
    Tygh::$app['view']->assign('zowie_categories', $zowie_categories);
    Tygh::$app['view']->assign('benq_categories', $benq_categories);
    Tygh::$app['view']->assign('brand', $brand);

    if ($_GET['isAjax'] == 1) {
        Tygh::$app['view']->assign('product_count', count($elastic_products));
        Tygh::$app['view']->assign('products', $products);
        Tygh::$app['view']->assign('isAjax', true);
        Registry::set('runtime.root_template', 'views/pages/msi-landing/products.tpl');
    } else {
        Tygh::$app['view']->assign('products', $products);
        Tygh::$app['view']->assign('no_products', $no_products);
    }
}

if ($mode == 'gaming-landing') {

    $cat_filter = false;
    $sub_filter = false;
    $no_products = false;

    $platforms = array(
        1 => 'PC',
        2 => 'PlayStation',
        3 => 'XBOX'
    );

    $brands = array(
        10 => 'Zowie',
        11 => 'Steelseries',
        12 => 'Razer',
        13 => 'MSI'
    );

    $sub_categories = array(
        1 => 'Laptop',
        2 => 'Kompjuterë',
        3 => 'Pllaka amë',
        4 => 'Kartela grafike',
        5 => 'Monitorë',
        6 => 'Mausë',
        7 => 'Tastierë'
    );

    $sub_categories_common = array(
        8 => 'Kufje',
        9 => 'Kontroller'
    );

    $sub_categories_console = array(
        10 => 'Videolojëra',
        11 => 'Konzola'
    );

    if ($_GET['kategoria'] == 'playstation' || $_GET['kategoria'] == 'xbox') {
        $sub_categories = $sub_categories_common + $sub_categories_console;
    } else {
        $sub_categories = $sub_categories + $sub_categories_common;
    }

    $main_categories = $platforms + $brands;
    $main_categories_map = fn_map_categories($main_categories);
    $main_categories_seo = $main_categories_map[0];
    $main_categories_parsed = $main_categories_map[1];

    $sub_categories_map = fn_map_categories($sub_categories);
    $sub_categories_seo = $sub_categories_map[0];
    $sub_categories_parsed = $sub_categories_map[1];

    if ($_GET['kategoria']) {
        $cat_filter = true;

        $main_category_id = fn_parse_category_id($main_categories_seo, $_GET['kategoria']);
    }

    if ($_GET['nenkategoria']) {
        $sub_filter = true;

        $sub_category_id = fn_parse_category_id($sub_categories_seo, $_GET['nenkategoria']);
    }

    $csv_products = fn_get_csv_data('feeds/gaming-landing.csv', array('product_code', 'product', 'brand', 'category', 'platform'));

    if ($cat_filter) {
        $cat_filter_type = 'brand';

        if ($main_category_id < 10) {
            $cat_filter_type = 'platform';
        }

        $cat_filter_product_codes = fn_filter_csv_products_by($csv_products, $main_category_id, $cat_filter_type);

        if (!$sub_filter) {
            $product_codes = $cat_filter_product_codes;
        }
    }

    if ($sub_filter) {
        $sub_filter_product_codes = fn_filter_csv_products_by($csv_products, $sub_category_id, 'category');

        if (!$cat_filter) {
            $product_codes = $sub_filter_product_codes;
        }
    }

    if ($cat_filter && $sub_filter) {
        $product_codes = array_values(array_intersect($sub_filter_product_codes, $cat_filter_product_codes));
    } elseif ((!$cat_filter || $_GET['kategoria'] == 'all') && !$sub_filter) {
        if (!apcu_exists('gaming_landing_all_product_codes') || empty(apcu_fetch('gaming_landing_all_product_codes'))) {
            $all_product_codes = fn_get_csv_products($csv_products);

            apcu_add('gaming_landing_all_product_codes', $all_product_codes);
        } else {
            $all_product_codes = apcu_fetch('gaming_landing_all_product_codes');
        }

        $product_codes = $all_product_codes[0];
        $no_filter = true;
    }

    if ($product_codes) {

        if ($no_filter) {
            if (!apcu_exists('gaming_landing_elastic_products') || empty(apcu_fetch('gaming_landing_elastic_products'))) {
                $elastic_products = fn_query_elastic('product_code', $product_codes, (isset($_GET['limit'])) ? (int)$_GET['limit'] : 32, (isset($_GET['offset'])) ? (int)$_GET['offset'] : 0, 'values');

                apcu_add('gaming_landing_elastic_products', $elastic_products);
            } else {
                $elastic_products = apcu_fetch('gaming_landing_elastic_products');
            }
        } else {
            $elastic_products = fn_query_elastic('product_code', $product_codes, (isset($_GET['limit'])) ? (int)$_GET['limit'] : 32, (isset($_GET['offset'])) ? (int)$_GET['offset'] : 0, 'values');
        }

        $products = fn_extract_product_data($elastic_products, NULL, $auth);
        $priority_products = fn_array_get_random($products, 3);
    } else {
        $no_products = true;
    }

    $products_by_brand = fn_group_by_int($csv_products, 'brand');
    $products_by_category = fn_group_by($csv_products, 'category');
    $products_by_platform = fn_group_by($csv_products, 'platform');
    {
        $slider_products_by_brand = $products_by_brand;

        $slider_random_products = fn_array_get_random_multi($slider_products_by_brand, 3);
        $slider_random_products = fn_get_slider_products($slider_random_products, $slider_products_by_brand);
        $slider_random_products = fn_array_remove_first_level($slider_random_products);

        if (!apcu_exists('gaming_landing_slider') || empty(apcu_fetch('gaming_landing_slider'))) {
            $slider = fn_prepare_slider_products($slider_random_products);

            apcu_add('gaming_landing_slider', $slider, 3600);
        } else {
            $slider = apcu_fetch('gaming_landing_slider');
        }

        $slider = fn_array_remove_first_level($slider);
    }

    if (defined('AJAX_REQUEST')) {
        Tygh::$app['view']->assign('products', $products);
        // Tygh::$app['view']->assign('product_count', count($products));
        Tygh::$app['view']->assign('no_products', $no_products);
        Tygh::$app['view']->assign('sub_categories', $sub_categories_parsed);

        if ($_GET['load_more']) {
            Registry::get('ajax')->assign('product_count', count($products));
            Registry::set('runtime.root_template', 'views/pages/gaming/products.tpl');
        } elseif ($_GET['filtering']) {
            Tygh::$app['view']->assign('priority_products', $priority_products);

            Registry::get('ajax')->assign('product_count', count($products));
            Registry::set('runtime.root_template', 'views/pages/gaming/sections.tpl');
        }
    } else {
        Tygh::$app['view']->assign('products', $products);
        Tygh::$app['view']->assign('product_count', count($products));
        Tygh::$app['view']->assign('priority_products', $priority_products);
        Tygh::$app['view']->assign('platforms', $platforms);
        Tygh::$app['view']->assign('brands', $brands);
        Tygh::$app['view']->assign('categories', $main_categories_parsed);
        Tygh::$app['view']->assign('categories_seo', $main_categories_seo);
        Tygh::$app['view']->assign('sub_categories', $sub_categories_parsed);
        Tygh::$app['view']->assign('slider', $slider);
        Tygh::$app['view']->assign('no_products', $no_products);
        Tygh::$app['view']->assign('cat_filter', $cat_filter);
        Tygh::$app['view']->assign('sub_filter', $sub_filter);
    }
}

if ($mode == 'newyear_csv') {
    $asd = fn_get_csv_data('feeds/newyear.csv', array('product_code'));

    foreach ($asd as $a) {
        $f[] = $a['product_code'];
    }

    $products = fn_query_elastic('product_code', $f, count($f), 0, 'values');
    $g = array();

    foreach ($products[0] as $p) {
        $g[] = array($p['product_code'] . ';' . $p['price']);
    }

    $fp = fopen('feeds/newyearrrrrrrr.csv', 'w');

    foreach ($g as $gg) {
        fputcsv($fp, $gg, ';');
    }

    var_dump($g);
    exit();
}

if ($mode == 'clear_wishlist') {
    unset(Tygh::$app['session']['wishlist']);
    db_query("DELETE FROM ?:user_session_products WHERE user_id = ?i AND type = ?i", Tygh::$app['session']['auth']['user_id'], "W");

    return array(CONTROLLER_STATUS_REDIRECT, 'wishlist.view');
}

if ($mode == 'update_product_brand') {
    $products = fn_get_csv_data('feeds/BrandNull.csv', array('product_id', 'product_code', 'brand'));
    $variant_id = null;

    foreach ($products as $product) {
        if ($product['brand'] != 'NULL') {
            $variant_ids = db_get_fields('SELECT variant_id FROM ?:product_feature_variant_descriptions WHERE variant LIKE ?l', $product['brand']);

            foreach ($variant_ids as $vid) {
                $valid_variant_id = db_get_field('SELECT variant_id FROM ?:product_feature_variants WHERE variant_id = ?i AND feature_id = ?i', $vid, 12);

                if ($valid_variant_id && $valid_variant_id != '') {
                    $variant_id = $valid_variant_id;

                    break;
                }
            }

            if ($variant_id != null) {
                $data[] = array(
                    'feature_id' => 12,
                    'product_id' => $product['product_id'],
                    'variant_id' => $variant_id,
                    'lang_code' => 'al'
                );

                db_query('DELETE FROM ?:product_features_values WHERE feature_id = ?i AND product_id = ?i', 12, $product['product_id']);
                db_query('REPLACE INTO ?:product_features_values ?m', $data);
            }
        }
    }

    var_dump('Complete');
    exit;
}

if ($mode == 'ps-landing') {
    Tygh::$app['view']->assign('page_title', 'PlayStation 5');
}

if ($mode == 'samsung-note-landing') {
    Tygh::$app['view']->assign('page_title', 'Samsung Galaxy Note 20 | 20 Ultra - Gjirafa50');
}

if ($mode == 'clear_cache') {
    fn_clear_cache();
    echo __('cache_cleared');

    exit();
}

if ($mode == 'ddbi') {
    $order_ids = explode(',', $_REQUEST['order_ids']);

    foreach ($order_ids as $order_id) {
        fn_insert_order_delivery_dates($order_id);
    }

    echo 'Done.';

    exit();
}

if ($mode == 'rtx') {
    Tygh::$app['view']->assign('page_title', 'GEFORCE RTX™ 30 SERIES - Gjirafa50');
}

if ($mode == 'samsung-20-landing') {
    Tygh::$app['view']->assign('page_title', 'Samsung Galaxy S20 FE - Gjirafa50');
}

if ($mode == 'orders_bulk_status_update') {
    $orders = [
        92536 => "C",
        92466 => "C",
        92479 => "C",
        92592 => "B",
        92547 => "C",
        92451 => "C",
        92505 => "C",
        92499 => "C",
        92450 => "C",
        92539 => "C",
        92561 => "C",
        92580 => "C",
        92534 => "C",
        92624 => "C",
        92578 => "C",
        92558 => "C",
        92568 => "C",
        93010 => "C",
        93026 => "C",
        93028 => "B",
        93015 => "B",
        93030 => "E"
    ];

    foreach ($orders as $key => $value) {
        db_query('UPDATE ?:orders SET status = ?s WHERE order_id = ?i', $value, $key);
    }

    var_dump('Done');

    exit();
}

if ($mode == 'flags') {
    $flags = array(
        array('al', '530', 'Albania'),
        array('us', '100–139', 'United States'),
        array('us', '060–099', 'United States'),
        array('fr', '300–379', 'France'),
        array('mc', '300–379', NULL),
        array('bg', '380', 'Belgium'),
        array('si', '383', 'Slovenia'),
        array('hr', '385', 'Croatia'),
        array('ba', '387', NULL),
        array('me', '389', NULL),
        array('de', '400–440', 'Germany'),
        array('jp', '450–459', 'Japan'),
        array('jp', '490–499', 'Japan'),
        array('ru', '460–469', NULL),
        array('kg', '470', NULL),
        array('tw', '471', NULL),
        array('ee', '474', NULL),
        array('lv', '475', NULL),
        array('az', '476', NULL),
        array('lt', '477', NULL),
        array('uz', '478', NULL),
        array('lk', '479', NULL),
        array('ph', '480', NULL),
        array('by', '481', NULL),
        array('ua', '482', NULL),
        array('tm', '483', NULL),
        array('md', '484', NULL),
        array('am', '485', NULL),
        array('ge', '486', NULL),
        array('kz', '487', NULL),
        array('tj', '488', NULL),
        array('hk', '489', NULL),
        array('gb', '500–509', 'United Kingdom'),
        array('gr', '520–521', NULL),
        array('lb', '528', NULL),
        array('cy', '529', NULL),
        array('al', '530', NULL),
        array('mk', '531', NULL),
        array('mt', '535', NULL),
        array('ie', '539', NULL),
        array('be', '540–549', NULL),
        array('lu', '540–549', NULL),
        array('pt', '560', NULL),
        array('is', '569', NULL),
        array('dk', '570–579', NULL),
        array('fo', '570–579', NULL),
        array('gl', '570–579', NULL),
        array('pl', '590', NULL),
        array('ro', '594', NULL),
        array('hu', '599', NULL),
        array('za', '600–601', NULL),
        array('gh', '603', NULL),
        array('sn', '604', NULL),
        array('bh', '608', NULL),
        array('mu', '609', NULL),
        array('ma', '611', NULL),
        array('dz', '613', NULL),
        array('ng', '615', NULL),
        array('ke', '616', NULL),
        array('cm', '617', NULL),
        array('tn', '619', NULL),
        array('tz', '620', NULL),
        array('sy', '621', NULL),
        array('eg', '622', NULL),
        array('bn', '623', NULL),
        array('ly', '624', NULL),
        array('jo', '625', NULL),
        array('ir', '626', NULL),
        array('kw', '627', NULL),
        array('sa', '628', NULL),
        array('ae', '629', NULL),
        array('qa', '630', NULL),
        array('fi', '640–649', NULL),
        array('cn', '690–699', NULL),
        array('no', '700–709', NULL),
        array('il', '729', 'Israel'),
        array('se', '730–739', NULL),
        array('gt', '740', NULL),
        array('sv', '741', NULL),
        array('hn', '742', NULL),
        array('ni', '743', NULL),
        array('cr', '744', NULL),
        array('pa', '745', NULL),
        array('do', '746', NULL),
        array('mx', '750', NULL),
        array('ca', '754–755', NULL),
        array('ve', '759', NULL),
        array('ch', '760–769', NULL),
        array('li', '760–769', NULL),
        array('co', '770–771', NULL),
        array('uy', '773', NULL),
        array('pe', '775', NULL),
        array('bo', '777', NULL),
        array('ar', '778–779', NULL),
        array('cl', '780', NULL),
        array('py', '784', NULL),
        array('ec', '786', NULL),
        array('br', '789–790', NULL),
        array('it', '800–839', NULL),
        array('sm', '800–839', NULL),
        array('va', '800–839', NULL),
        array('es', '840–849', NULL),
        array('ad', '840–849', NULL),
        array('cu', '850', NULL),
        array('sk', '858', NULL),
        array('cz', '859', NULL),
        array('rs', '860', NULL),
        array('mn', '865', NULL),
        array('tr', '868–869', NULL),
        array('nl', '870–879', NULL),
        array('mm', '883', NULL),
        array('kh', '884', NULL),
        array('th', '885', NULL),
        array('sg', '888', NULL),
        array('in', '890', NULL),
        array('vn', '893', NULL),
        array('pk', '896', NULL),
        array('id', '899', NULL),
        array('at', '900–919', NULL),
        array('au', '930–939', NULL),
        array('nz', '940–949', NULL),
        array('my', '955', NULL),
        array('mo', '958', NULL),
        array('kr', '880', NULL),
        array('xk', '390', 'Kosovo'),
        array('us', '000–019', NULL),
        array('us', '020–029', NULL),
        array('us', '030–039',),
        array('us', '040–049', NULL),
        array('us', '050–059', NULL),
        array('us', '060–069', NULL),
    );

    foreach ($flags as $flag) {
        $data = [
            'flag' => $flag[0],
            'country' => $flag[1],
            'country_name' => $flag[2]
        ];
        db_query('INSERT INTO ?:flags ?e', $data);
    }

    exit();
}

if ($mode == 'kinguin') {
    header("X-Kinguin-Verification-Token: ODM0YmI0ZjBhY2U4ZjNlMmEyZDk4ZWMxMTljMWRiNDc=");
    header("api-ecommerce-auth: ODM0YmI0ZjBhY2U4ZjNlMmEyZDk4ZWMxMTljMWRiNDc=");
    exit();
}

if ($mode == 'bulk_register') {
//    $users = [
//        'Eris Çunaku' => 'eris@kodelabs.com',
//        'Leonita Krasniqi' => 'leonita@kodelabs.com',
//        'Ard  Stavileci' => 'ard@kodelabs.com',
//        'Ardita Morina' => 'ardita@kodelabs.com',
//        'Fjollë Morina' => 'fjolle@kodelabs.com',
//        'Gresa Kadriu' => 'gresa@kodelabs.com',
//        'Natyra Ferati' => 'natyra@kodelabs.com',
//        'Rron Jahiri' => 'rron@kodelabs.com',
//        'Ymer  Bruti' => 'ymer@kodelabs.com',
//        'Dukagjin Maloku' => 'dukagjin@kodelabs.com',
//        'Ilir Koci' => 'ilir@kodelabs.com',
//        'Ariana Daka' => 'ariana@kodelabs.com',
//        'Arianit Haxha' => 'arianit@kodelabs.com',
//        'Ilir Beqiri' => 'ilir.beqiri@kodelabs.com',
//        'Meriton Çela' => 'meriton@kodelabs.com',
//        'Rinor Murturi' => 'rinor@kodelabs.com',
//        'Shqiprim Bunjaku' => 'shqiprim@kodelabs.com',
//        'Ardit Islami' => 'arditislami@kodelabs.com',
//        'Ardit Podrimaj' => 'ardit@kodelabs.com',
//        'Arven  Syla' => 'arven@kodelabs.com',
//        'Buhar Morina' => 'buhar@kodelabs.com',
//        'Edon Sopjani' => 'edon@kodelabs.com',
//        'Emanuel Zalli' => 'emanuel@kodelabs.com',
//        'Endrit Gojani' => 'endrit@kodelabs.com',
//        'Festim Krasniqi' => 'festim@kodelabs.com',
//        'Granit Gojani' => 'granit@kodelabs.com',
//        'Nakshije Emërllahu' => 'nakshije@kodelabs.com',
//        'Shpend Bytyqi' => 'shpend@kodelabs.com',
//        'Tea Pula' => 'tea@kodelabs.com',
//        'Zana Beqiri' => 'zana@kodelabs.com',
//        'Zell  Bakalli' => 'zell@kodelabs.com',
//        'Gentrit Gojani' => 'gentrit@kodelabs.com',
//        'Nue Nikolla' => 'nue@kodelabs.com',
//        'Leutrim  Musliu' => 'leutrim@kodelabs.com',
//        'Lorëza Qehaja' => 'lorez@kodelabs.com',
//        'Nora Gjergji' => 'nora@kodelabs.com',
//        'Yll Haziri' => 'yll@kodelabs.com',
//    ];

    $users = [
        'Zell  Bakalli' => 'zell@kodelabs.com',
        'Arven  Syla' => 'arven@kodelabs.com',
        'Ymer  Bruti' => 'ymer@kodelabs.com',
        'Leutrim  Musliu' => 'leutrim@kodelabs.com',
        'Gëzim Lumi' => 'gezim@kodelabs.com',
    ];

    foreach ($users as $full_name => $email) {
        $register_user_data = [
            'name' => explode(' ', $full_name)[0],
            'lastname' => explode(' ', $full_name)[1],
            'email' => $email

        ];

        $gjirafa_sso = new GjirafaSso();
        $user_data = $gjirafa_sso->parseCallRequestUserData($register_user_data);
        $registered = $gjirafa_sso->register($user_data);

        $user_id = fn_is_user_exists(0, array(
            'email' => $email
        ));

        if ($user_id == '') {
            $register_user_data['phone'] = '000000000';
            $register_user_data['address'] = 'Adresa';
            list($user_id, $profile_id) = fn_update_user(0, $gjirafa_sso->parseRegisterUserData($register_user_data), $auth, false, false);
        }

        $usergroup_data = [
            'user_id' => $user_id,
            'usergroup_id' => 13,
            'status' => 'A'
        ];
        db_query('REPLACE INTO ?:usergroup_links ?e', $usergroup_data);
    }

    var_dump('Done');
    exit();
}

//if ($mode == 'insert_delivery_dates') {
//    $asd = db_get_array('SELECT * FROM ?:order_data WHERE type = ?s', 'D');
//
//    foreach ($asd as $fff) {
//        $ffd = unserialize($fff['data']);
//
//        $from_date = new DateTime($ffd['standard']);
//        $from_date = $from_date->getTimestamp();
//
//        $to_date = new DateTime($ffd['standard_range']);
//        $to_date = $to_date->getTimestamp();
//
//        $dd_data = array(
//            'order_id' => $fff['order_id'],
//            'date_from' => $from_date,
//            'date_to' => $to_date,
//        );
//
//        db_query('REPLACE INTO ?:order_delivery_dates ?e', $dd_data);
//
//        var_dump($ffd);
//    }
//
//    exit();
//}
//
//if ($mode == 'delivery_dates_shift') {
//    $orders = db_get_array('SELECT * FROM ?:order_delivery_dates WHERE date_from = 1609714800 OR date_to = 1609714800 OR date_from = 1609801200 OR date_to = 1609801200');
//
//    foreach ($orders as $order) {
//        $res = db_query('UPDATE ?:order_delivery_dates SET date_from = ?i, date_to = ?i WHERE order_id = ?i', strtotime('+2 days', $order['date_from']), strtotime('+2 days', $order['date_to']), $order['order_id']);
//        var_dump($res);
//    }
//
//    exit();
//}

if ($mode == 'samsung-galaxy-s21') {
    Tygh::$app['view']->assign('page_title', 'Samsung Galaxy S21 - Gjirafa50');
}

if ($mode == 'samsung-main') {
    Tygh::$app['view']->assign('page_title', 'Samsung - Gjirafa50');
}

if ($mode == 'ping-samsung') {
    if (isset($_GET['_ref']) && $_GET['_ref'] == 'esis-atc') {
        $_COOKIE['esis_tracking_code'] = $_GET['_refu'];
        setcookie('esis_tracking_code', $_GET['_refu'], time() + SECONDS_IN_DAY);
    }

    exit();
}

if ($mode == 'long2ip') {
    $ip = fn_get_ip(true);
    $ip2 = fn_get_user_ip();

    var_dump(long2ip($ip['host']));
    var_dump($ip2);
    exit();
}

if ($mode == 'recommendations') {
    echo fn_get_user_recommended_products($_REQUEST['bisko_sid']);
    exit();
}

if ($mode == 'product_view') {
    fn_ping_product_view($_REQUEST['bisko_sid'], $_REQUEST['product_id']);
    exit();
}

$boi = array(
    149509,
    149511,
    149512,
    149513,
    149514,
    149515,
    149516,
    149520,
    149528,
    149532,
    149534,
    149536,
    149538,
    149539,
    149543,
    149544,
    149546,
    149547,
    149549,
    149550,
    149553,
    149554,
    149560,
    149561,
    149564,
    149565,
    149567,
    149568,
    149569,
    149570,
    149571,
    149572,
    149573,
    149575,
    149577,
    149579,
    149582,
    149583,
    149593,
    149594,
    149595,
    149596,
    149597,
    149598,
    149603,
    149604,
    149606,
    149608,
    149609,
    149610,
    149612,
    149613,
    149614,
    149615,
    149620,
    149621,
    149624,
    149625,
    149627,
    149628,
    149629,
    149630,
    149631,
    149632,
    149633,
    149634,
    149635,
    149638,
    149641,
    149643,
    149645,
    149649,
    149650,
    149651,
    149652,
    149654,
    149655,
    149656,
    149657,
    149659,
    149660,
    149661,
    149662,
    149668,
    149670,
    149672,
    149674,
    149675,
    149676,
    149677,
    149679,
    149680,
    149682,
    149685,
    149686,
    149688,
    149690,
    149691,
    149693,
    149694,
    149695,
    149696,
    149698,
    149700,
    149701,
    149703,
    149704,
    149705,
    149712,
    149713,
    149714,
    149715,
    149717,
    149718,
    149719,
    149722,
    149723,
    149724,
    149725,
    149726,
    149727,
    149728,
    149729,
    149730,
    149732,
    149737,
    149738,
    149740,
    149742,
    149743,
    149746,
    149747,
    149749,
    149752,
    149753,
    149755,
    149756,
    149759,
    149760,
    149761,
    149764,
    149766,
    149767,
    149768,
    149771,
    149773,
    149776,
    149778,
    149779,
    149780,
    149782,
    149783,
    149784,
    149787,
    149788,
    149789,
    149790,
    149791,
    149792,
    149793,
    149795,
    149796,
    149797,
    149800,
    149804,
    149805,
    149806,
    149808,
    149809,
    149810,
    149814,
    149815,
    149816,
    149818,
    149820,
    149821,
    149822,
    149823,
    149826,
    149827,
    149828,
    149829,
    149830,
    149832,
    149835,
    149838,
    149839,
    149840,
    149842,
    149846,
    149847,
    149851,
    149853,
    149856,
    149857,
    149861,
    149866,
    149867,
    149869,
    149870,
    149871,
    149873,
    149874,
    149878,
    149879,
    149882,
    149944
);
$bbb = array(
    143376
);
if ($mode == 'bulk_payment_total') {
    if ($_REQUEST['key'] == '7Kl3YWgxi6nR2bBeaqD1eGaG32vgY0MTxpFcBA4h') {
        $orders = db_get_array('SELECT * FROM ?:orders WHERE order_id IN (?a)', $boi);

        foreach ($orders as $order) {
            $total = $order['total'];
            if ($order['payment_total'] != -1 && $order['payment_total'] != 0) {
                $total = $order['payment_total'];
            }

            $discount_price = $total * 0.05;
            $payment_total = $total - $discount_price;
            $gift_amount = $order['gift_amount'] + $discount_price;

            $updated_order = array(
                'payment_total' => $payment_total,
                'gift_amount' => $gift_amount
            );

            db_query('UPDATE ?:orders SET ?u WHERE order_id = ?i', $updated_order, $order['order_id']);
        }

        var_dump("Success!");
    } else {
        var_dump("Nope!");
    }

    exit();
}

if ($mode == 'promotions_data') {
    $promotion_ids = explode(',', $_REQUEST['promotion_ids']);
    $data = db_get_array('SELECT promotion_id, conditions FROM ?:promotions WHERE promotion_id IN (?a)', $promotion_ids);
    $conditions = array();

    foreach ($data as $d) {
        $pconditions = unserialize($d['conditions']);

        if ($pconditions['conditions'][1]['condition'] == 'products') {
            array_push($conditions, array(
                    'promotion_id' => $d['promotion_id'],
                    'product_ids' => $pconditions['conditions'][1]['value'])
            );
        }
    }

    echo json_encode($conditions);

    exit();
}

if ($mode == 'calculate_gjflex') {
    echo(fn_calculate_gjflex($_REQUEST['price']));
    exit();
}