<?php
/***************************************************************************
 *                                                                          *
 *   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 *                                                                          *
 ****************************************************************************
 * PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
 * "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
 ****************************************************************************/

use Tygh\Hashids;
use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

$items_map = fn_builder_get_items_map();
$selected_items = Tygh::$app['session']['builder_items'];
$accessories_map = [
    'keyboard' => 'Tastierë',
    'tv' => 'Monitor',
    'headset' => 'Kufje',
    'mouse' => 'Maus'
];

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if ($mode == 'create') {
        if (!fn_builder_order_disabled($items_map, $selected_items)) {
            $order_has_stock = fn_builder_order_has_stock($items_map, $selected_items);

            if ($order_has_stock) {
                $builder_products_added = true;

                foreach ($selected_items as $key => $product) {
                    if ($items_map[$key]['multiple'] == true) {
                        foreach ($product as $single_key => $single_product) {
                            if (!fn_builder_add_to_cart($single_product)) {
                                $builder_products_added = false;
                            }
                        }
                    } else {
                        if (!fn_builder_add_to_cart($product)) {
                            $builder_products_added = false;
                        }
                    }
                }

                fn_save_cart_content(Tygh::$app['session']['cart'], Tygh::$app['session']['auth']['user_id']);
                Tygh::$app['session']['cart']['change_cart_products'] = true;
                fn_calculate_cart_content(Tygh::$app['session']['cart'], Tygh::$app['session']['auth'], 'S');

                if ($builder_products_added) {
                    unset($selected_items);
                    unset(Tygh::$app['session']['builder_items']);

                    fn_set_notification('N', __('notice'), __('builder_order_added'));

                    return array(CONTROLLER_STATUS_REDIRECT, 'checkout.cart');
                }
            } else {
                fn_set_notification('W', __('notice'), __('builder_order_no_stock'));
            }
        } else {
            fn_set_notification('W', __('notice'), __('builder_order_disabled'));
        }
    }

    return array(CONTROLLER_STATUS_REDIRECT, 'builder.' . $mode);
}

if ($mode == 'create') {
    $can_save_build = true;

    if (!$_REQUEST['item']) {
        $_REQUEST['item'] = 1;
    }

    if (defined('AJAX_REQUEST')) {
        if ($_REQUEST['selected_item']) {
            $multiple = false;

            $amount = fn_check_stock($_REQUEST['product']['product_id']);

            if ($amount > 0 && $_REQUEST['product']['amount'] > $amount) {
                $_REQUEST['product']['amount'] = $amount;
            }

            if ($amount > 0) {
                if ($items_map[$_REQUEST['selected_item']]['multiple'] == true) {
                    $selected_items[$_REQUEST['selected_item']][] = $_REQUEST['product'];
                    Tygh::$app['session']['builder_items'][$_REQUEST['selected_item']][] = $_REQUEST['product'];
                    $multiple = true;
                } else {
                    $selected_items[$_REQUEST['selected_item']] = $_REQUEST['product'];
                    Tygh::$app['session']['builder_items'][$_REQUEST['selected_item']] = $_REQUEST['product'];
                }


                Tygh::$app['view']->assign('added_product', $_REQUEST['product']);
                Tygh::$app['view']->assign('multiple', $multiple);

                if ($_REQUEST['item'] + 1 <= count($items_map)) {
                    Tygh::$app['view']->assign('item', $_REQUEST['item'] + 1);
                }

    //            fn_set_notification('I', __('product_added_to_build'), Tygh::$app['view']->fetch('views/builder/notifications/item_added.tpl'));
                fn_set_notification('N', __('notice'), __('product_added_to_build'));

                $can_save_build = true;
            } else {
                fn_set_notification('W', __('important'), __('text_cart_amount_corrected', array(
                    '[product]' => $_REQUEST['product']['product']
                )));
            }
        }

        if ($_REQUEST['remove']) {
            if ($items_map[$_REQUEST['item']]['multiple']) {
                foreach ($selected_items[$_REQUEST['item']] as $key => $item) {
                    if ($_REQUEST['product_id'] == $item['product_id']) {
                        unset($selected_items[$_REQUEST['item']][$key]);
                        unset(Tygh::$app['session']['builder_items'][$_REQUEST['item']][$key]);
                    }
                }
            } else {
                unset($selected_items[$_REQUEST['item']]);
                unset(Tygh::$app['session']['builder_items'][$_REQUEST['item']]);
            }

            $can_save_build = true;
        }

        if ($_REQUEST['build'] && $_REQUEST['remove']) {
            db_query('DELETE FROM ?:builder_sets WHERE builder_set_id = ?i', $_REQUEST['build']);

            fn_set_notification('N', __('notice'), __('builder_set_deleted'));
        }
    }

    if ($_REQUEST['item'] || $_REQUEST['offset'] || ($_REQUEST['item'] && ($_REQUEST['filter'] || $_REQUEST['filter_query']))) {
        $item = db_get_row('SELECT * FROM ?:builder WHERE build_item_id = ?i', $_REQUEST['item']);
        $item_data = unserialize(base64_decode($item['data']));

        if ($_REQUEST['filter_query']) {
            parse_str($_REQUEST['filter_query'], $filter_query_parsed);
            $_REQUEST['filter'] = $filter_query_parsed['filter'];
        }

        list($products, $has_more) = fn_builder_get_items($item_data['item_categories'], $item_data['item_products'], $_REQUEST['item'], $_REQUEST['offset'], $_REQUEST['filter']);

        if (empty($products)) {
            $can_save_build = false;
        }

        Tygh::$app['view']->assign('item', $_REQUEST['item']);
        Tygh::$app['view']->assign('products', $products);
        Tygh::$app['view']->assign('has_more', $has_more);
        Tygh::$app['view']->assign('filter', $_REQUEST['filter']);
        Tygh::$app['view']->assign('filter_query', http_build_query(array('filter' => $_REQUEST['filter'])));
    }

    if ($_REQUEST['reset']) {
        $selected_items = [];
        Tygh::$app['session']['builder_items'] = [];
    }

    if ($_REQUEST['store']) {
        $data = [
            'user_id' => Tygh::$app['session']['auth']['user_id'],
            'name' => $_REQUEST['build_set_name'],
            'data' =>  base64_encode(serialize(array_replace_recursive($selected_items, $items_map)))
        ];
        db_query('INSERT INTO ?:builder_sets ?e', $data);

        fn_set_notification('N', __('notice'), __('builder_set_saved'));

        $can_save_build = false;
    }

    if ($_REQUEST['load']) {
        $saved_build = db_get_row('SELECT data FROM ?:builder_sets WHERE builder_set_id = ?i', $_REQUEST['load']);
        $selected_items = unserialize(base64_decode($saved_build['data']));

        fn_builder_cleanup_loaded($selected_items);

        Tygh::$app['session']['builder_items'] = $selected_items;

        fn_set_notification('N', __('notice'), __('builder_set_loaded'));
    }

    if ($_REQUEST['load_shared']) {
        $hashids = new Hashids('gj50builder');
        $decoded = $hashids->decode($_REQUEST['load_shared']);

        $saved_build = db_get_row('SELECT data FROM ?:builder_sets WHERE builder_set_id = ?i', $decoded[0]);
        $selected_items = unserialize(base64_decode($saved_build['data']));

        fn_builder_cleanup_loaded($selected_items);

        Tygh::$app['session']['builder_items'] = $selected_items;
    }

    if (Tygh::$app['session']['auth']['user_id']) {
        $saved_builds = db_get_array('SELECT * FROM ?:builder_sets WHERE user_id = ?i', Tygh::$app['session']['auth']['user_id']);

        Tygh::$app['view']->assign('saved_builds', $saved_builds);
    }

    Tygh::$app['view']->assign('page_title', 'Builder');
    Tygh::$app['view']->assign('items_map', $items_map);
    Tygh::$app['view']->assign('selected_items', $selected_items);
    Tygh::$app['view']->assign('total', fn_builder_get_total($selected_items, $items_map));
    Tygh::$app['view']->assign('offset', 20 + $_REQUEST['offset']);
    Tygh::$app['view']->assign('order_disabled', fn_builder_order_disabled($items_map, $selected_items));
    Tygh::$app['view']->assign('can_save_build', $can_save_build);
    Tygh::$app['view']->assign('accessories_map', $accessories_map);
    Tygh::$app['view']->assign('accessories', fn_builder_get_accessories($selected_items, $accessories_map));
    Tygh::$app['view']->assign('progress', fn_builder_calculate_progress($selected_items, $items_map));
}

if ($mode == 'landing') {
    if (!apcu_exists('builder_landing_sets')) {
        $sets = db_get_array('SELECT * FROM ?:builder_sets LIMIT 3');
        $sets = array_map(function($set) use ($accessories_map) {
            $data = unserialize(base64_decode($set['data']));
            return [
                'id' => $set['builder_set_id'],
                'user_id' => $set['user_id'],
                'name' => $set['name'],
                'data' => $data,
                'accessories' =>  fn_builder_get_accessories($data, $accessories_map),
                'features' => fn_builder_get_product_features($data)
            ];
        }, $sets);

        foreach ($sets as $key => $set) {
            foreach ($set['data'] as $inner_key => $data) {
                if ($data['item'] == 'cpu' || $data['item'] == 'gpu') {
                    if (filter_var($data['multiple'], FILTER_VALIDATE_BOOLEAN)) {
                        $sets[$key]['images'][0][] = $data[0]['product_id'];
                    } else {
                        $sets[$key]['images'][0][] = $data['product_id'];
                    }
                }

                if ($data['item'] == 'case') {
                    if (filter_var($data['multiple'], FILTER_VALIDATE_BOOLEAN)) {
                        $sets[$key]['images'][1][] = $data[0]['product_id'];
                    } else {
                        $sets[$key]['images'][1][] = $data['product_id'];
                    }
                }

                if ($data['item'] == 'ram' || $data['item'] == 'hdd') {
                    if (filter_var($data['multiple'], FILTER_VALIDATE_BOOLEAN)) {
                        $sets[$key]['images'][2][] = $data[0]['product_id'];
                    } else {
                        $sets[$key]['images'][2][] = $data[0]['product_id'];
                    }
                }
            }

            ksort($sets[$key]['images']);
        }

        apcu_store('builder_landing_sets', $sets, 14400);
    } else {
        $sets = apcu_fetch('builder_landing_sets');
    }

    Tygh::$app['view']->assign('sets', $sets);
    Tygh::$app['view']->assign('page_title', 'Ndërto PC tuaj - Gjirafa50');
}

if ($mode == 'share') {
    Tygh::$app['view']->assign('build_share_url', fn_builder_get_share_url($_REQUEST));

    fn_set_notification('I', __('builder_set_share'), Tygh::$app['view']->fetch('views/builder/notifications/share_set.tpl'));

    exit();
}

if ($mode == 'features') {
    $features = fn_builder_get_product_features($selected_items);
    $accessories = fn_builder_get_accessories($selected_items, $accessories_map);

    var_dump($features);
    var_dump($accessories);
    exit();
}

function fn_builder_get_items($item_categories, $item_ids, $item, $offset = null, $filters) {
    $products = [];

    if (apcu_exists('builder_products_' . $item) && !$filters) {
        $products = apcu_fetch('builder_products_' . $item);
    } else {
        if ($item_categories) {
            $category_ids = explode(',', $item_categories);

            if ($category_ids) {
                $queried_products = fn_query_elastic('category_ids', $category_ids, 1000, 0, 'values', $filters['search'], 0);

                if ($queried_products[0]) {
                    $products += $queried_products[0];
                }
            }
        }

        if ($item_ids) {
            $ids = explode(',', $item_ids);

            if ($filters['sort']) {
                if ($filters['sort']['field'] == 'bestsellers') {
                    $products_bought = db_get_array('SELECT * FROM ?:product_popularity WHERE product_id IN (?n)', $ids);

                    usort($products_bought, function ($a, $b) {
                        return $a['bought'] < $b['bought'];
                    });

                    $ids = array_column($products_bought, 'product_id');
                }
            }

            if ($ids) {
                $queried_products = fn_query_elastic('product_id', $ids, count($ids), 0, 'values', $filters['search'], 0);

                if ($queried_products[0]) {
                    $products += $queried_products[0];
                }
            }
        }

        foreach ($products as $key => $product) {
            if ($product['total_stock'] == 0) {
                unset($products[$key]);
            }
        }

        if ($filters['sort']) {
            if ($filters['sort']['field'] != 'popularity') {
                usort($products, function($a, $b) {
                    return $a['price'] <=> $b['price'];
                });

                if ($filters['sort']['order'] == 'desc') {
                    $products = array_reverse($products);
                }
            }
        }

        if (!$filters) {
            apcu_store('builder_products_' . $item, $products, 86400);
        }
    }

    return array(
        array_slice($products, 0, 20 + $offset),
        count($products) > 20 + $offset ? true : false
    );
}

function fn_builder_get_total($selected_items, $items_map) {
    $total = 0;

    if ($selected_items) {
        foreach ($selected_items as $key => $item) {
            if ($items_map[$key]['multiple'] == true) {
                foreach ($item as $single_item) {
                    $total += $single_item['price'] * $single_item['amount'];
                }
            } else {
                $total += $item['price'] * $item['amount'];
            }
        }
    }

    return $total;
}

function fn_builder_order_disabled($items_map, $selected_items) {
    $disabled = false;

    if (empty($items_map)) {
        $disabled = true;
    }

    if (!empty($items_map)) {
        foreach ($items_map as $key => $item) {
            if ($item['required'] == true && !is_array($selected_items[$key])) {
                $disabled = true;
            }
        }
    }

    return $disabled;
}

function fn_builder_add_to_cart($product) {
    $product_data = [];
    $product_data[$product['product_id']] = [
        'product_id' => $product['product_id'],
        'amount' => $product['amount']
    ];
    $added = fn_add_product_to_cart($product_data,Tygh::$app['session']['cart'],Tygh::$app['session']['auth']);

    return $added;
}

function fn_builder_get_items_map() {
    $items_map = [];

    if (apcu_exists('builder_items_map')) {
        $items_map = apcu_fetch('builder_items_map');
    } else {
        $items_config = db_get_array('SELECT * FROM ?:builder_config');

        foreach ($items_config as $item_config) {
            $items_map[$item_config['build_item_id']] = unserialize($item_config['data']);
        }

        apcu_store('builder_items_map', $items_map);
    }

    return $items_map;
}

function fn_builder_order_has_stock($items_map, $selected_items) {
    $order_has_stock = true;

    foreach ($selected_items as $key => $product) {
        if ($items_map[$key]['multiple'] == true) {
            foreach ($product as $single_key => $single_product) {
                if (!fn_builder_product_has_stock($single_product['product_id'], $single_product['product'])) {
                    $order_has_stock = false;
                }
            }
        } else {
            if (!fn_builder_product_has_stock($product['product_id'], $product['product'])) {
                $order_has_stock = false;
            }
        }
    }

    return $order_has_stock;
}

function fn_builder_product_has_stock($product_id, $product) {
    $product_has_stock = true;

    $product_stock = db_get_row('SELECT amount, gjps.czc_stock, gjps.czc_retailer_stock FROM ?:products AS gjp INNER JOIN ?:product_stock AS gjps ON gjps.product_id = gjp.product_id WHERE gjp.product_id = ?i', $product_id);
    $stock = array_filter($product_stock);

    if (empty($stock)) {
        $product_has_stock = false;

        fn_set_notification('E', __('notice'), __('text_cart_zero_inventory', array(
            '[product]' => $product
        )));
    }

    return $product_has_stock;
}

function fn_builder_get_share_url($request) {
    $hashids = new Hashids('gj50builder');
    $hashed_id = $hashids->encode($request['build']);

    return fn_url('builder.create&load_shared=' . $hashed_id);
}

function fn_builder_get_product_features($selected_items) {
    $features_merged = [];

    foreach ($selected_items as $key => $product) {
        $features = [];

        if ($product[0]) {
            foreach ($product as $inner_key => $inner_product) {
                $product_data = fn_get_product_data($inner_product['product_id'], Tygh::$app['session']['auth']);
                fn_product_bundles_get_default_features(0, $product_data, $features, true);

                if (!empty($features)) {
                    foreach ($features as $key => $feature) {
                        $features_merged[$key] = $feature;
                    }
                }
            }
        } else {
            $product_data = fn_get_product_data($product['product_id'], Tygh::$app['session']['auth']);
            fn_product_bundles_get_default_features(0, $product_data, $features, true);

            if (!empty($features)) {
                foreach ($features as $key => $feature) {
                    $features_merged[$key] = $feature;
                }
            }
        }
    }

    return $features_merged;
}

function fn_builder_get_accessories($selected_items, $accessories_map) {
    $contains_accessories = [];

    if (!empty($selected_items)) {
        foreach ($selected_items as $product) {
            if ($product[0]) {
                foreach ($product as $inner_product) {
                    foreach ($accessories_map as $key => $accessory) {
                        if (substr($inner_product['product'], 0, strlen($accessory)) === $accessory) {
                            $contains_accessories[$key] = $accessory;
                        }
                    }
                }
            } else {
                foreach ($accessories_map as $key => $accessory) {
                    if (substr($product['product'], 0, strlen($accessory)) === $accessory) {
                        $contains_accessories[$key] = $accessory;
                    }
                }
            }
        }
    }

    return $contains_accessories;
}

function fn_builder_calculate_progress($selected_items, $items_map) {
    if (empty($selected_items)) {
        return 0;
    }

    return (count($selected_items) / count($items_map)) * 100;
}

function fn_builder_cleanup_loaded(&$selected_items) {
    foreach ($selected_items as $key => $item) {
        if ($item['multiple']) {
            unset($selected_items[$key]['item']);
            unset($selected_items[$key]['required']);
            unset($selected_items[$key]['multiple']);

            if (empty($selected_items[$key])) {
                unset($selected_items[$key]);
            }
        } else {
            if (!$item['product_id']) {
                unset($selected_items[$key]);
            }
        }
    }
}