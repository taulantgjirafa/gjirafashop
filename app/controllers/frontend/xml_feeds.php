<?php
/***************************************************************************
 *                                                                          *
 *   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 *                                                                          *
 ****************************************************************************
 * PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
 * "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
 ****************************************************************************/

use Tygh\FacebookCatalog;
use Tygh\Registry;
use Tygh\Pdf;

use MicrosoftAzure\Storage\Blob\BlobRestProxy;
use MicrosoftAzure\Storage\Common\Exceptions\ServiceException;
use MicrosoftAzure\Storage\Blob\Models\CreateBlockBlobOptions;

if (!defined('BOOTSTRAP')) {
    die('Access denied');
}

$xml = '<?xml version="1.0" encoding="' . CHARSET . '"?>';

if($mode == 'get_products_batch') {
    if ($_REQUEST['psw'] == '2yd1092VwfmWe80dB57da48SbEC2Ux87') {
        $elastic = new elasticSearch();
        //[79056,56333]
        $obj = array('where' => [array("field" => "product_id", "values" => range($_GET['from'], $_GET['to']))], 'filters' => false, 'limit' => ($_GET['to'] - $_GET['from']));

        $products = $elastic->sendObject($obj, array());

        $dmp_products = array();
        foreach ($products[0] as $p) {
            $img = smarty_function_get_images_from_blob(array(
                'product_id' => $p['product_id'],
                'count' => 1
            ));

            $dmp_products[] = array(
                "id" => $p['product_id'],
                "name" => $p['product'],
                "pictureUrl" => $img,
                "stockQuantity" => ($p['stock'] != null) ? $p['stock'] : 0,
                "czcStockQuantity" => ($p['czc_stock'] != null) ? $p['czc_stock'] : 0,
                "czcSupplierStockQuantity" => ($p['czc_retailer_stock'] != null) ? $p['czc_retailer_stock'] : 0,
                "oldPrice" => ($p['old_price'] != null) ? $p['old_price'] : 0,
                "price" => $p['price'],
                "priceAl" => $p['price_al'],
                "categoryIds" => $p['category_ids']
            );
        }
        echo json_encode($dmp_products);
    }
    exit;
}

if($mode == 'get_product_details') {
    if($_GET['product_ids'] && $_GET['psw'] == '2yd1092VwfmWe80dB57da48SbEC2Ux87'){
$product_ids = explode(',', $_GET['product_ids']);
        $obj = array('where' => [array("field" => "product_id", "values" => $product_ids)], 'limit' => count($product_ids));
        $products = (new elasticSearch())->sendObject($obj, array());

        $products_list = array();
        foreach ($products[0] as $k => $p) {
            if ($k) {
                $img = smarty_function_get_images_from_blob(array(
                    'product_id' => $p['product_id'],
                    'count' => 1
                ));

                $products_list[] = array(
                    "id" => $p['product_id'],
                    "name" => $p['product'],
                    "pictureUrl" => $img,
                    "stockQuantity" => ($p['stock'] != null) ? $p['stock'] : 0,
                    "czcStockQuantity" => ($p['czc_stock'] != null) ? $p['czc_stock'] : 0,
                    "czcSupplierStockQuantity" => ($p['czc_retailer_stock'] != null) ? $p['czc_retailer_stock'] : 0,
                    "oldPrice" => ($p['old_price'] != null) ? $p['old_price'] : 0,
                    "price" => $p['price'],
                );
            }
        }

        echo json_encode($products_list);
    }
    exit;

}


if($mode == 'ifp') {
    if(!empty($_GET['p'])){
        $products = explode(',', $_GET['p']);
        $products_structured = array_flip($products);
        $obj = array('where' => [array("field" => "product_code", "values" => $products)], 'limit' => count($products));
        $elastic = new elasticSearch();
        $products = $elastic->sendObject($obj, array());

        foreach($products[0] as $product){
            $products_structured[$product['product_code']] = $product;
        }

        foreach($products_structured as $product){
            $img = smarty_function_get_images_url(array('product_id' => $product['product_id']));
            echo "
                <div style='width:12.5%;float:left;border:1px solid #eee;height:280px;text-align:center;font-family:sans-serif;padding:15px;-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;'>
                    <a target='_blank' href='https://gjirafa50.com/index.php?dispatch=products.view&product_id=" . $product['product_id'] . "'>
<img style='max-width:100%;max-height:170px;' src='" . str_replace('thumb', 'img', $img) . "0.jpg'/></a>
                    <h4>" . $product['product'] . "</h4>
                </div>
            ";
        }
        exit;
    }
}

// Products management
if ($mode == 'get_products') {

    $_REQUEST['extend'] = empty($_REQUEST['type']) ? array('description') : array($_REQUEST['type']);

    list($products) = fn_get_products($_REQUEST, Registry::get('settings.Appearance.products_per_page'));

    fn_gather_additional_products_data($products, array('get_icon' => true));

    $xml .= fn_array_to_xml($products, 'products');
}

if ($mode == "get_gjflex") {


    if ($_REQUEST['psw'] == '2yd1092VwfmWe80dB57da48SbEC2Ux87') {

        $get_gjflex = db_get_array("select order_id, data from gjshop_order_data where order_id IN (?a) and type = 'G'", explode(",", $_REQUEST['order_ids']));

        $orders = array();
        foreach ($get_gjflex as $order) {


            $serialized_obj = unserialize($order['data']);

//            if($serialized_obj === false){
//
//                $html = $order['data'];
//                $needle = 'gjflex';
//                $lastPos = 0;
//                $positions = array();
//
//                $words = array(
//                    'product_code',
//                    'gjflex'
//                );
//                $i = 0;
//                foreach($words as $word){
//                    while (($lastPos = strpos($html, $word, $lastPos))!== false) {
//                        $positions[$i][$word] = $lastPos;
//                        $lastPos = $lastPos + strlen($needle);
//
//                    }
//                    if($word == 'gjflex')
//                        $i++;
//                }
//                var_dump($positions);
//
//                $products = array();
//                foreach ($positions as $product) {
//
//                    $product['product_code'] = preg_replace("/[^0-9]/","",substr($order['data'], ($product['product_code'] +17), 9));
//                    $products['orderid'] = $order['order_id'];
//                    $products['productid'] = $product['product_code'];
//                    $products['gjflex'] = unserialize("a:1:{" . substr($order['data'], ($product['gjflex'] - 5), 18))['gjflex'];
//var_dump($products);
//                }
//                array_push($orders, $products);
//            }else{
            $products = array();

            if (!empty($serialized_obj)) {
                foreach ($serialized_obj['0']['products'] as $g => $p) {

                    $products = array();
                    $products['orderid'] = $order['order_id'];
                    $products['productid'] = $p['product_code'];
                    $products['price'] = $p['price'];
                    $products['gjflex'] = (!empty($p['gjflex'])) ? $p['gjflex'] : false;
                    array_push($orders, $products);
                }


                // }
            }

        }


        echo json_encode($orders);
        //var_dump($orders);

    }
    exit;
}

if ($mode == 'update_products_stock') {

    $obj = file_get_contents('php://input');
    $obj = json_decode($obj);

    if ($obj->psw == '2yd1092VwfmWe80dB57da48SbEC2Ux87') {
        if (!empty($obj->product_ids)) {
            $result = array();

            foreach ($obj->product_ids as $product) {

                //$product_id = $product->product_id;
                $get_product_id = db_get_row('SELECT product_id 
                                FROM gjshop_product_feature_variant_descriptions AS vd
                                INNER JOIN (
                                  SELECT product_id, variant_id
                                    FROM gjshop_product_features_values
                                    WHERE feature_id = 14
                                ) AS v
                                ON vd.variant_id = v.variant_id
                                where variant in (?s)
                                ', $product->product_ean);

                if (empty($get_product_id) || $get_product_id == "") {
                    array_push($result, array(
                        $product->product_ean => true
                    ));
                    continue;
                }

                $product_id = (int)$get_product_id['product_id'];

                $get_product = db_get_row("SELECT amount, product_code FROM ?:products WHERE product_id = ?i", $product_id);
                $retailers_amount = db_get_row("SELECT czc_stock, czc_retailer_stock FROM ?:product_stock WHERE product_id = ?i", $product_id);
                $current_amount = empty($get_product['amount']) ? 0 : $get_product['amount'];
                $stock_amount = $get_product['amount'];


                if ($product->sign == "+") {
                    db_query("UPDATE ?:products SET amount = ?i WHERE product_id = ?i", ($current_amount + $product->amount->stock), $product_id);
                    db_query("UPDATE ?:product_stock SET czc_stock = ?i, czc_retailer_stock = ?i WHERE product_id = ?i", ($retailers_amount['czc_stock'] + $product->amount->czc_stock), ($retailers_amount['czc_retailer_stock'] + $product->amount->czc_retailer_stock), $product_id);

                    array_push($result, array(
                        $product->product_ean => ($product->amount->stock + $product->amount->czc_stock + $product->amount->czc_retailer_stock)
                    ));

                } else if ($product->sign == "-") {

                    $amount = $product->amount;

                    if ($amount > $current_amount) {
                        $current_amount += $retailers_amount['czc_stock'];
                        if ($amount >= $current_amount) {
                            $current_amount += $retailers_amount['czc_retailer_stock'];
                        }
                    }

                    $new_amount = $stock_amount - $amount;

                    if (($stock_amount - $amount) <= 0)
                        $new_amount = 0;

                    db_query("UPDATE ?:products SET amount = ?i WHERE product_id = ?i", $new_amount, $product_id);

                    if ($stock_amount < $amount) {
                        if ($retailers_amount['czc_stock'] >= ($amount - $stock_amount)) {
                            db_query("UPDATE ?:product_stock SET czc_stock = ?i WHERE product_id = ?i", ($retailers_amount['czc_stock'] - ($amount - $stock_amount)), $product_id);
                        } else {
                            if ($retailers_amount['czc_retailer_stock'] > ($amount - ($stock_amount + $retailers_amount['czc_stock']))) {
                                db_query("UPDATE ?:product_stock SET czc_stock = ?i, czc_retailer_stock = ?i WHERE product_id = ?i", 0, ($retailers_amount['czc_retailer_stock'] - ($amount - $stock_amount - $retailers_amount['czc_stock'])), $product_id);

                            } else {
                                db_query("UPDATE ?:product_stock SET czc_stock = ?i, czc_retailer_stock = ?i WHERE product_id = ?i", 0, 0, $product_id);

                            }
                        }

                        if (($current_amount - $amount) < 0) {
                            array_push($result, array(
                                $product->product_ean => ($current_amount - $amount)
                            ));
                        } else {
                            array_push($result, array(
                                $product->product_ean => true
                            ));
                        }
                    } else {
                        array_push($result, array(
                            $product->product_ean => true
                        ));
                    }
                }
            }
            echo json_encode($result);
        }
    }
    exit;
}

if($mode == "analyse_data") {
    $a = new user_precomputed_data();
    $b = $a->fn_analyse_user_data();


    foreach($b as $key => $value){
        $i = 0;
        //foreach($c as $key => $value){
        $container = (($key / 8000 + 1) > 5) ? 5 : ($key / 8000 + 1);
        echo "<div style='border:1px solid #777;float:left;width:312.5px;height:300px;'><img src='https://hhstsyoejx.gjirafa.net/gj50/img/" . $key . "/thumb/0.jpg'/>" . $value . "</div>";
        if($i == 5)
            break;
        $i++;
        // }
    }
    exit;
}

if ($mode == 'get_promotion_price') {

    if ($_REQUEST['psw'] == '2yd1092VwfmWe80dB57da48SbEC2Ux87') {


        $servername = "149.202.193.59";
        $username = "root";
        $password = "Pl@yt1po09";
        $dbname = "gjirafashop";

// Create connection
        $conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }

        $sql = "SELECT company_id, conditions, bonuses, conditions_hash FROM gjshop_promotions WHERE status = 'A' AND conditions_hash LIKE '%products=%' AND zone = 'catalog' AND IF(from_date, from_date <= UNIX_TIMESTAMP(), 1) AND IF(to_date, to_date >= UNIX_TIMESTAMP(), 1)";
        $result = $conn->query($sql);
        $products = array();
        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                array_push($products, array(
                    'store' => ($row['company_id'] == 1) ? "ks" : "al",
                    'products' => str_replace('products=', '', $row['conditions_hash']),
                    'bonuses' => unserialize($row['bonuses'])
                ));
            }
        } else {
            echo "0 results";
        }
        $conn->close();


//        $get_promotion_price = db_get_array("select company_id, conditions, bonuses, conditions_hash from ?:promotions where status = 'A' and conditions_hash like '%products=%' and zone = 'catalog' and IF(from_date, from_date <= UNIX_TIMESTAMP(), 1) AND IF(to_date, to_date >= UNIX_TIMESTAMP(), 1)");
////select company_id, conditions, bonuses, conditions_hash from gjshop_promotions where status = 'A' and conditions_hash like '%products=%' and zone = 'catalog' and IF(from_date, from_date <= NOW(), 1) AND IF(to_date, to_date >= NOW(), 1)
//        $products = array();
//        foreach ($get_promotion_price as $product) {
//            array_push($products, array(
//                'store' => ($product['company_id'] == 1) ? "ks" : "al",
//                'products' => str_replace('products=', '', $product['conditions_hash']),
//                'bonuses' => unserialize($product['bonuses'])
//            ));
//        }

        echo json_encode($products);

    }
    exit;
}

if ($mode == 'print_invoice') {
    if (!empty($_REQUEST['order_id']) && $_REQUEST['psw'] == '2yd1092VwfmWe80dB57da48SbEC2Ux87') {
        fn_print_order_invoices($_REQUEST['order_id'], 'pdf', 'A', CART_LANGUAGE, null, true);
    }
    exit;
}

if ($mode == 'print_short_invoice') {
    if (!empty($_REQUEST['order_id']) && $_REQUEST['psw'] == '2yd1092VwfmWe80dB57da48SbEC2Ux87') {
        fn_print_order_invoices($_REQUEST['order_id'], 'pdf', 'A', CART_LANGUAGE, 'short', true);
    }
    exit;
}

if ($mode == 'send_invoice') {
    if (!empty($_REQUEST['order_id']) && $_REQUEST['psw'] == '2yd1092VwfmWe80dB57da48SbEC2Ux87') {
        if(fn_change_order_status($_REQUEST['order_id'], $_REQUEST['status'], '', $_REQUEST['notifications']))
            echo "true";
        else
            echo "false";
    }
    exit;
}
//
// View product details
//
//if ($mode == 'get_product') {
//
//    $_REQUEST['product_id'] = empty($_REQUEST['product_id']) ? 0 : $_REQUEST['product_id'];
//
//    $product = fn_get_product_data($_REQUEST['product_id'], $auth, CART_LANGUAGE);
//    if (!empty($product)) {
//        if (!empty($_REQUEST['combination'])) {
//            $product['combination'] = $combination;
//        }
//
//        fn_gather_additional_product_data($product, true, true);
//
//        $xml .= fn_array_to_xml($product, 'product_data');
//    }
//}

//if ($mode == 'get_categories') {
//
//    $_REQUEST['category_id'] = empty($_REQUEST['category_id']) ? 0 : $_REQUEST['category_id'];
//
//    $params = array(
//        'category_id' => $_REQUEST['category_id'],
//        'visible' => false,
//        'plain' => (!empty($_REQUEST['format']) && $_REQUEST['format'] == 'plain') ? true : false
//    );
//    list($categories,) = fn_get_categories($params, CART_LANGUAGE);
//    $xml .= fn_array_to_xml($categories, 'categories');
//}
//
//if ($mode == 'get_category') {
//
//    $_REQUEST['category_id'] = empty($_REQUEST['category_id']) ? 0 : $_REQUEST['category_id'];
//
//    $category_data = fn_get_category_data($_REQUEST['category_id'], CART_LANGUAGE, '*');
//    if (!empty($category_data)) {
//        $xml .= fn_array_to_xml($category_data, 'category_data');
//    }
//}

if($mode == 'get_banners' && $_REQUEST['psw'] == '2yd1092VwfmWe80dB57da48SbEC2Ux87') {
    $banners = fn_get_banners(array());
    foreach(current($banners) as $banner){
        if($banner['position'] == '99'){
            echo json_encode(array(
                'url' =>  $banner['url'],
                'img' =>  'https://hhstsyoejx.gjirafa.net/gj50/banners/' . $banner['image_path']
            ));
            break;
        }
    }
    exit;
}

if ($mode == 'fb_catalog') {
    return;
    $category_id = $_REQUEST['category_id'];
    $category_name = db_get_field('SELECT category FROM ?:category_descriptions WHERE category_id = ?i', $category_id);
    $dec_category_name = utf8_decode($category_name);
    $company_id = $_REQUEST['company'];
    if($company_id == 'KS' || $company_id == null){
        $company_id = 1;
    }
    else{
        $company_id = 19;
    }

    $ip = !empty($_SERVER['HTTP_CLIENT_IP']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];

    if (isset($_SERVER['PHP_AUTH_USER']) && $_SERVER['PHP_AUTH_USER'] == 'arlind' && $_SERVER['PHP_AUTH_PW'] == '4RL1NDGJ1R4F4!@#$%^&*()1234567890') {

        header($_SERVER["SERVER_PROTOCOL"] . " 200 OK");
        header("Cache-Control: public"); // needed for internet explorer
        header("Content-Type: application/rss+xml");
        header("Content-Transfer-Encoding: Binary");
        $name = preg_replace("/[\s_]/", "-", utf8_encode(strtolower($dec_category_name)));
        $name_replaced = str_replace(["ë", "Ç" , "ç"], ["e", "C", "c"], $name);
        if(isset($category_id)){
           fn_set_content($category_id,$name_replaced,$company_id);
        }
        else{
            fn_set_content(0,$name_replaced,$company_id);
        }

        if(isset($category_id) && isset($company_id)){

            $url = 'https://hhstsyoejx.gjirafa.net/gj50/feeds/'.$company_id.'/'.$name_replaced.'.xml';
            $header = array("Accept: application/xml");
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)');


            $data = curl_exec($ch);
            $size = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
            header("Content-Length:" .$size);
            header('Content-Disposition: attachment; filename='. $name_replaced);
            readfile('https://hhstsyoejx.gjirafa.net/gj50/feeds/'.$company_id.'/'.$name_replaced.'.xml');
            curl_close($ch);


        }else{
            header("Content-Length:" . filesize('feeds/'.$company_id.'/'.'fb_catalog.xml'));
            header('Content-Disposition: attachment; filename=fb_catalog.xml');
            $url = 'https://hhstsyoejx.gjirafa.net/gj50/feeds/'.$company_id.'/fb_catalog.xml';
            $header = array("Accept: application/xml");
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)');


            $data = curl_exec($ch);
            $size = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
            header("Content-Length:" .$size);
            header('Content-Disposition: attachment; filename=fb_catalog.xml');
            readfile('https://hhstsyoejx.gjirafa.net/gj50/feeds/'.$company_id.'/fb_catalog.xml');
            curl_close($ch);
        }
    }
    exit;
}

if($mode == 'get_reviews') {
    if ($_REQUEST['psw'] == '2yd1092VwfmWe80dB57da48SbEC2Ux87') {
        $reviews = db_get_array('
                                    select a.object_id as product_id, ROUND(AVG(b.rating_value), 2) as val
                                        from gjshop_discussion as a
                                        inner join gjshop_discussion_rating as b on a.thread_id = b.thread_id
                                    where a.type = \'B\' and a.object_type = \'P\' and b.rating_value > 0 
                                    group by product_id
                                    ');

        echo json_encode($reviews);
    }
    exit;
}

if ($mode == 'get_product_warranty') {
    if (!empty($_GET['order_id']) && $_GET['psw'] == '2yd1092VwfmWe80dB57da48SbEC2Ux87') {
        $data = file_get_contents('php://input');
        return fn_get_warranty_page($_GET['order_id'], 'pdf', 'A', CART_LANGUAGE, $data);
    }
    exit;

}

if($mode == 'get_product_declaration') {
    if(!apcu_exists('product_declaration_categories')) {
        $a = array_reduce(array_map(function ($product) {
            return array(
                'category_id' => $product['0'], //(strpos($product['0'], ' ') !== false) ? explode(",", $product['0']) : $product['0'],
                'feature_ids' => explode(",", $product['1'])
            );
        }, array_map('str_getcsv', file('feeds/deklaracion.csv'))), function ($current, $item) {
            if (strpos($item['category_id'], ',') !== false) {
                $cat_ids = explode(",", $item['category_id']);
                foreach ($cat_ids as $cid) {
                    $current[trim($cid)] = [
                        'feature_ids' => array_map('trim', $item['feature_ids']),
                    ];
                }
            } else {
                $current[$item['category_id']] = [
                    'feature_ids' => array_map('trim', $item['feature_ids']),
                ];
            }

            return $current;
        }, []);
        apcu_store('product_declaration_categories', $a, 0);
    }else
        $a = apcu_fetch('product_declaration_categories');

    if(apcu_exists('product_declaration_' . $_GET['product_code'])){
        echo json_encode(apcu_fetch('product_declaration_' . $_GET['product_code']));
        exit;
    }
    $categories = get_categories_by_product_code($_GET['product_code']);

    foreach($categories as $category){
        if(isset($a[$category['category_id']])){

            // Default per krejt
            $a[$category['category_id']]['feature_ids'][] = "12"; // Prodhuesi
            $a[$category['category_id']]['feature_ids'][] = "13"; // Kodi (modeli)

            $c = get_variants_name($category['product_id'], $a[$category['category_id']]['feature_ids']);

            $f = [];
            if(count($c) && is_array($c[12]) && is_array($c[13]) && !is_null($c['product'])){
                $f = array(
                    'manufacturer' => $c[12]['variant'],
                    'product' => $c['product'],
                    'type' => $c[13]['variant'],
                    'model' => $category['category'],
                    'specs' => array_slice($c, 2, -1)
                );
                break;
            }
        }
    }
    apcu_store('product_declaration_' . $_GET['product_code'], $f, 0);
    echo json_encode($f);
    exit;
}

if($mode == 'update_vip_promotion'){
    if($_REQUEST['psw'] == '2yd1092VwfmWe80dB57da48SbEC2Ux87'){
        $promotion = db_get_array('SELECT * FROM ?:promotions WHERE promotion_id = ?i', $_GET['promotion_id']);
        $promotion = current($promotion);
        $promotion['conditions'] = unserialize($promotion['conditions']);
        $promotion['bonuses'] = unserialize($promotion['bonuses']);
        //$promotion['conditions_hash'] = unserialize($promotion['conditions_hash']);

        $product_condition_hash = explode(',', $promotion['conditions_hash']);

        $product_condition = explode(',', $promotion['conditions']['conditions'][2]['value']);

        $pids = explode(',', $_GET['pids']);
        foreach($pids as $pid){
            array_push($product_condition, $pid);
            array_push($product_condition_hash, $pid);
        }
        $promotion['conditions']['conditions'][2]['value'] = implode(',', $product_condition);
        $promotion['conditions_hash']= implode(',', $product_condition_hash);

        $promotion['conditions'] = serialize($promotion['conditions']);
        $promotion['bonuses'] = serialize($promotion['bonuses']);
        $promotion['conditions_hash'] = serialize($promotion['conditions_hash']);

        $data = array (
            'conditions_hash' =>  $promotion['conditions_hash'],
            'conditions' =>  $promotion['conditions']
        );

        fn_log_event('promotions', 'update', array(
            'promotion_id' => $promotion['promotion_id'],
            'data' => array(
                'xml' => true,
                'object' => $data
            )
        ));

        db_query("UPDATE ?:promotions SET ?u WHERE promotion_id = ?i", $data, $promotion['promotion_id']);
    }
}

if ($mode == 'print_kosgiro_invoice') {
    // make request
    // filter data
    // generate barcode
    // delete barcode file
}

if ($mode == 'fb_catalog_csv' || $mode == 'fb_catalog_new') {
    return;
    if ($_REQUEST['key'] == 'b89de4249e2d4aba836b1d86820fe031') {
        $facebookCatalog = new FacebookCatalog();
        $facebookCatalog->parseRequestData($_REQUEST['category_id'], $_REQUEST['company']);
    } else {
        exit();
    }

//    $category_id = $_REQUEST['category_id'];
//    $category_name = db_get_field('SELECT category FROM ?:category_descriptions WHERE category_id = ?i', $category_id);
//    $dec_category_name = utf8_decode($category_name);
//    $name = preg_replace("/[\s_]/", "-", utf8_encode(strtolower($dec_category_name)));
//    $name_replaced = str_replace(["ë", "Ç" , "ç"], ["e", "C", "c"], $name);
//    $company_id = $_REQUEST['company'];
//
//    if($company_id == 'KS' || $company_id == null){
//        $company_id = 1;
//    }
//    else{
//        $company_id = 19;
//    }
//
//    $ip = !empty($_SERVER['HTTP_CLIENT_IP']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
//
//    // if (isset($_SERVER['PHP_AUTH_USER']) && $_SERVER['PHP_AUTH_USER'] == 'arlind' && $_SERVER['PHP_AUTH_PW'] == '4RL1NDGJ1R4F4!@#$%^&*()1234567890') {
//
//        if (isset($category_id)) {
//           fn_generate_catalog_csv($category_id, $name_replaced, $company_id);
//        }
//        else {
//            fn_generate_catalog_csv(0, $name_replaced, $company_id);
//        }
//
//    // }

}

function fn_generate_catalog_csv($category_id, $category_name, $company_id) {
    $fb_catalog = new fb_catalog();

    if ($company_id == 19) {
        $currency = 'ALL';
        $domain = 'gjirafa50.al';
    } else {
        $currency = 'EUR';
        $domain = 'gjirafa50.com';
    }

    $all_products = false;

    if (isset($category_id) && isset($company_id) && $category_id != 0) {
        $products['products'] = $fb_catalog->fn_catalog_get_category_products($category_id, $company_id);
    } else {
        $all_products = true;
        $category_name = 'catalog';
        $products = $fb_catalog->fn_catalog_get_all_products($company_id);
    }

    $company_dir = 'feeds/' . $company_id;
    $category_dir = 'feeds/' . $company_id . '/' . $category_name;

    if (!is_dir($company_dir) && !file_exists($company_dir)) {
        mkdir($company_dir);
    }

    if (!is_dir($category_dir) && !file_exists($category_dir)) {
        mkdir($category_dir);
    }

    // create downloadable file, store the file to server
    header('Content-Type: application/csv');
    header('Content-Disposition: attachment; filename="' . $category_name . '.csv";');

    $fp = fopen('php://output', 'w');
    $fp_local = fopen($category_dir . '/catalog.csv', 'w');

    $fields = array(
        'id',
        'title',
        'description',
        'availability',
        'condition',
        'price',
        'link',
        'image_link',
        'brand',
        'additional_image_link',
        // 'google_product_category',
        'product_type',
        'sale_price',
        'custom_label_0'
    );

    $path = '';

    fputcsv($fp, $fields);
    fputcsv($fp_local, $fields);

    foreach ($products['products'] as $product) {
        $container = intval($product['product_id'] / 8000 + 1);

        if ($container > 5){
            $container = 5;
        }

        $img_count = fn_get_image_count($product['product_id']);

        $discounted = false;

        if ($product['old_price']) {
            $discounted = true;
        }

        $img_links = '';

        $csv_product['id'] = $product['product_id'];
        $csv_product['title'] = $product['product'];
        $csv_product['description'] = $product['full_description'];

        if (empty(trim($csv_product['description']))) {
            $csv_product['description'] = 'Përshkrimi është duke u përpunuar, ndërkohë mund të shikoni specifikat teknike, apo kontaktoni në chat, email, messenger për detaje të mëtutjeshme. Ju faleminderit për mirëkuptim.';
        }

        $csv_product['availability'] = 'in stock';
        $csv_product['condition'] = 'new';
        $csv_product['price'] = $discounted ? number_format((float)$product['old_price'], 2, '.', '') . $currency : number_format((float)$product['price'], 2, '.', '') . $currency;
        $csv_product['link'] = 'https://' . $domain . '/index.php?dispatch=products.view%26product_id=' . $product['product_id'];
        $csv_product['image_link'] = 'https://hhstsyoejx.gjirafa.net/gj50/img/' . $product['product_id'] . '/img/0.jpg';
        $csv_product['brand'] = $product['variant'];

        if ($img_count > 1) {
            for ($i = 1; $i < $img_count; $i++) {
                $img_links .= 'https://hhstsyoejx.gjirafa.net/gj50/img/' . $product['product_id'] . '/img/' . $i . '.jpg,';
            }

            $csv_product['additional_image_link'] = rtrim($img_links, ',');
        } else {
            $csv_product['additional_image_link'] = '';
        }

        $csv_product['product_type'] = fn_get_product_categories_path($product['product_id'], $company_id);

        $csv_product['sale_price'] = $discounted ? number_format((float)$product['price'], 2, '.', '') . $currency : '';
        $csv_product['custom_label_0'] = $product['amount'] ? '48h' : false;

        fputcsv($fp, $csv_product);
        fputcsv($fp_local, $csv_product);
    }

    fclose($fp);
    fclose($fp_local);
}

function get_categories_by_product_code($product_code) {
    return db_get_array('SELECT product.product_id, a.category_id, c.category, b.level FROM gjshop_products_categories AS a, gjshop_categories AS b, gjshop_category_descriptions AS c, gjshop_products AS product WHERE product.product_code = ?i AND product.product_id = a.product_id AND b.category_id = c.category_id AND c.category_id = a.category_id AND b.company_id = 1 ORDER BY b.level DESC', $product_code);
}

function upload_feed_to_blob($blobClient, $company_id, $category_name) {
    if(!empty($blobClient)){
        if($category_name == 'none') {
            $content = fopen('feeds/'.$company_id.'/fb_catalog.xml', "r");
            $blob_name = "fb_catalog.xml";
        }
        else{
            $content = fopen('feeds/'.$company_id.'/'.$category_name.'/'.strtolower($category_name).'_catalog.xml', "r");
            $blob_name = $category_name.'.xml';
        }
        if($content){
            try {
                //Upload blob
                $contentType = 'application/rss+xml';
                $options = new CreateBlockBlobOptions();
                $options->setContentType($contentType);
                $blobClient->createBlockBlob("gjirafa50/feeds/".$company_id, $blob_name, $content,$options);
            } catch (ServiceException $e) {
                $code = $e->getCode();
                $error_message = $e->getMessage();
                echo $code.": ".$error_message.PHP_EOL;
            }
        }
    }

}

function get_variants_name($product_id, $variants) {

    $q = db_get_array('SELECT c.feature_id, p.product, c.description AS descr, b.variant AS var FROM gjshop_product_features_values AS a, gjshop_product_feature_variant_descriptions AS b, gjshop_product_features_descriptions AS c, gjshop_product_descriptions AS p WHERE a.product_id = ?i AND a.variant_id = b.variant_id AND a.feature_id = c.feature_id AND p.product_id = a.product_id AND a.feature_id IN (?a) ORDER BY field(a.feature_id, "12", "13", ?a);', $product_id, $variants, $variants);

    $variants = array_reduce($q, function($current, $item) {
        $a = 0;
        $current[$item['feature_id']] = array(
            'description' => $item['descr'],
            'variant' => $item['var']
        );
        return $current;
    }, []);

    $variants['product'] = (is_array($q)) ? current($q)['product'] : '';

    return $variants;
}

function fn_set_content($category_id,$category_name , $company_id){

    $fb_catalog = new fb_catalog();
    header('Content-Type: text/xml; charset=utf-8', true); //set document header content type to be XML
    if($company_id == 19){
        $currency = 'ALL';
        $domain = 'gjirafa50.al';
    }
    else{
        $currency = 'EUR';
        $domain = 'gjirafa50.com';
    }
    $rss = new SimpleXMLElement('<rss xmlns:g="http://base.google.com/ns/1.0"></rss>');
    $rss->addAttribute('version', '1.0');

    $channel = $rss->addChild('channel'); //add channel node

    $title = $channel->addChild('title', 'Gjirafa50'); //title of the feed
    $link = $channel->addChild('link', 'https://www.'.$domain); //feed site
    $description = $channel->addChild('description', 'Dyqani online më i madh per teknologji në Kosovë dhe rajon. Shumë i sigurtë dhe me oferta në produktet e teknologjisë.'); //feed description

    if (isset($category_id )&& isset($company_id) && $category_id != 0 ) {
        $products['products'] = $fb_catalog->fn_catalog_get_category_products($category_id, $company_id);
    } else {
        $products = $fb_catalog->fn_catalog_get_all_products($company_id);
    }
    foreach ($products['products'] as $product) {
        $price = (double)$product['price'];
        $price_res = number_format($price, 2, '.', '');

        $item = $channel->addChild('item');
        $item->addChild('xmlns:g:id', $product['product_id']);

        if (strpos($product['product'], '&') !== false)
            $product['product'] = str_replace('&', '&amp;', $product['product']);

        $item->addChild('xmlns:g:title', $product['product']);

        if($category_id == null){
            $description = $product['full_descriptions'];
        }
        else{
            $description = $product['full_description'];
        }

        if(stristr($description, 'Përshkrimi është duke u përpunuar') !== FALSE)
            $description = str_replace('Përshkrimi', 'Përshkrimi për produktin ' .  $product['product'], $description);

        if (strpos($description, '&') !== false)
            $description = str_replace('&', '&amp;', $description);

        if (strlen($description) > 120)
            $description = substr($description, 0, 120) . "...";



        $item->addChild('xmlns:g:description',$description);
        $url = 'https://'.$domain.'/index.php?dispatch=products.view%26product_id=' . $product["product_id"];
        $item->addChild('xmlns:g:link', $url);

        $container = intval($product['product_id'] / 8000 + 1);
        if ($container > 5)
            $container = 5;

        $item->addChild('xmlns:g:image_link', "https://hhstsyoejx.gjirafa.net/gj50/img/" . $product['product_id'] . "/img/0.jpg");

        $item->addChild('xmlns:g:brand', $product['variant']);
        $item->addChild('xmlns:g:condition', "new");
        $item->addChild('xmlns:g:availability', ($product['stock'] == 0) ? "out of stock" : "in stock");
        $item->addChild('xmlns:g:price', ($price_res . $currency));
        $item->addChild('xmlns:g:gtin', $product['variant']);
        if($category_id == NULL){
            $item->addChild('xmlns:g:custom_label_0', $product['category']);
        }
        // $item->addChild('xmlns:g:custom_label_1', );
        $item->addChild('xmlns:g:google_product_category', $category_name);
    }
    Header('Content-type: text/xml');
    $dom = new DOMDocument('1.0', LIBXML_NOBLANKS);
    $dom->formatOutput = true;
    $connectionString = 'DefaultEndpointsProtocol=https;AccountName=gjirafa;AccountKey=NaqSLMHF6WN0LiydX7a81ECdBWQ5UeN9urLDf8ta2nlvpHznDdaLJx1iSK0tbW0rjnaYupa86o5M68weNzfSIQ==';
    $blobClient = BlobRestProxy::createBlobService($connectionString);
    if (isset($category_id) && isset($company_id) && $category_id != 0) {
        mkdir('feeds/'.$company_id. '/' . $category_name, 0777, true);
        $dom->loadXML($rss->asXML('feeds/'.$company_id.'/'. $category_name . '/' . $category_name . '_catalog.xml'));
        upload_feed_to_blob($blobClient,$company_id,$category_name);
    } else {
        mkdir('feeds/'.$company_id, 0777, true);
        $dom->loadXML($rss->asXML('feeds/'.$company_id.'/'.'fb_catalog.xml'));
        upload_feed_to_blob($blobClient,$company_id,'none');
    }
}

exit;