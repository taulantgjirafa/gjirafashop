<?php

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Exception\ClientException;

defined('BOOTSTRAP') or die('Access denied');

fn_define('PCB_TOKEN', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6ImdqaXJhZmFCbGFja0ZyaWRheUxhbmRpbmciLCJuYmYiOjE2MzY0MDMyMDIsImV4cCI6MTY2NzkzOTIwMiwiaWF0IjoxNjM2NDAzMjAyfQ._0bxPNs7YhaEVJswAay4DmYhnhAcM3aIuCCpdmawEV8');
fn_define('PCB_BASE_URI', 'https://gjirafapaymentservice.gjirafa.com');
fn_define('PCB_ENDPOINT', 'api/Payment/PostRequest');
fn_define('PCB_PLATFORM_ID', 4);

if (defined('PAYMENT_NOTIFICATION') && !empty($mode) && $mode != 'place_order') {

    /**
     * Receiving and processing the answer
     * from third-party services and payment systems.
     *
     * Available variables:
     * @var string $mode The purpose of the request
     */

    if ($mode == 'approved') {
        $pp_response['order_status'] = 'O';
        $pp_response['payment_status'] = 'A';
        $pp_response['reason_text'] = __('transaction_approved');
        $pp_response['approval_code'] = $_POST['ApprovalCode'];

        fn_set_notification('N', __('order_placed'), __('text_order_placed_successfully'));
        fn_finish_payment($_POST['order_id'], $pp_response);
        fn_order_placement_routines('route', $_POST['order_id'], true, true, AREA, 'approved', true, array());

    } elseif ($mode == 'declined') {
        $pp_response['order_status'] = 'F';
        $pp_response['payment_status'] = 'C';
        $pp_response['reason_text'] = __('transaction_cancelled');

        fn_set_notification('W', __('text_transaction_cancelled'), __('text_order_placed_error'));
        fn_finish_payment($_POST['order_id'], $pp_response);
        fn_order_placement_routines('route', $_POST['order_id'], false, true, AREA, 'declined', true, array());
    } elseif ($mode == 'canceled') {
        fn_set_notification('W', __('text_transaction_cancelled'), __('text_order_placed_error'));
        fn_redirect(fn_url('checkout.checkout'));
    }
} else {
    $client = new Client([
        'base_uri' => PCB_BASE_URI,
    ]);

    $headers = [
        'Authorization' => 'Bearer ' . PCB_TOKEN,
        'Content-Type' => 'application/json',
        'Accept' => 'application/json'
    ];

    $payload = [
        'Amount' => (int) round($order_info['total'] * 100),
        'Description' => (string) $order_id,
        'PlatformId' => PCB_PLATFORM_ID,
        'OrderId' => $order_id
    ];

    $request = new Request('POST', PCB_ENDPOINT, $headers, json_encode($payload));

    try {
        $response = $client->send($request);
    } catch (ClientException $e) {
//        return $e->getResponse()->getStatusCode();
        header('Location: ' . fn_url('payment_notification.canceled&payment=pcb'));
        exit();
    }

    $location = substr($response->getBody()->getContents(), 1, -1);

    header('Location: ' . $location);
    exit();
}