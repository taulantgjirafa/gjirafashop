<?php

// Preventing direct access to the script, because it must be included by the "include" directive.
defined('BOOTSTRAP') or die('Access denied');

if (defined('PAYMENT_NOTIFICATION') && !empty($mode) && $mode != 'place_order')
{

    /**
     * Receiving and processing the answer
     * from third-party services and payment systems.
     *
     * Available variables:
     * @var string $mode The purpose of the request
     */

    fn_log_event('orders', 'payment_status', array(
        'order_id' => 0,
        'tags' => serialize(array(
            'post' => $_POST,
            'dawh' => $_SESSION['DAWH']
        ))
    ));

    $order_id_with_timestamp = explode('-', $_POST['OrderID']);
    $OrderID = $order_id_with_timestamp[0];
    $signatureAR = md5($OrderID . $_POST['TotalAmount'] . "ARLINDWASHEREANDFUCKUIFUDONTKNOWTHISSTRING!@#$%^");

//    $previous_order_logs = db_get_array("SELECT * FROM gjshop_logs WHERE type = 'orders' AND action = 'payment_status' AND content LIKE '%{$OrderID}%' AND content LIKE '%approved%'");
//    if (!empty($previous_order_logs) && $mode != 'approved') {
//        $mode = 'approved';
//    }

    if ($mode == 'declined' && !empty($_POST['TranCode']) && $_POST['TranCode'] != "000")
    {
        //$pp_response['order_status'] = 'F';
        //fn_change_order_status($OrderID , 'F');
        //fn_order_placement_routines('route', $OrderID, false);
        unset($_SESSION['DAWH']);

        if (empty($OrderID)) {
            $OrderID = $order_info['order_id'];
        }

        fn_log_event('orders', 'payment_status', array(
            'order_id' => $OrderID,
            'tags' => $mode
        ));

        $pp_response['order_status'] = 'F';
        $pp_response['payment_status'] = 'C';
        $pp_response['reason_text'] = __('transaction_cancelled');

        fn_set_notification('W', __('text_transaction_cancelled'), __('text_order_placed_error'));
        fn_finish_payment($OrderID, $pp_response);
        fn_order_placement_routines('route', $OrderID, false, true, AREA, 'declined', true, array());
    }
    else if ($mode == 'approved' && empty($_GET['awshr']))
    {
        unset($_SESSION['DAWH']);

        if (empty($OrderID)) {
            $OrderID = $order_info['order_id'];
        }

        if ($OrderID == null)
            return CONTROLLER_STATUS_NO_PAGE;

        fn_log_event('orders', 'payment_status', array(
            'order_id' => $OrderID,
            'tags' => $mode
        ));

        $pp_response['order_status'] = 'Y';
        $pp_response['payment_status'] = 'B';
        $pp_response['reason_text'] = __('transaction_pending');

        fn_set_notification('Y', __('order_placed'), __('text_order_check_success'));
        fn_finish_payment($OrderID, $pp_response);
        fn_order_placement_routines('route', $OrderID, true, true, AREA, 'pending', true, array());
    }
    else if ($mode == 'approved' && !empty($_POST['TranCode']) && $_POST['TranCode'] == "000" && $signatureAR == $_GET['awshr'])
    {
        unset($_SESSION['DAWH']);

        if (empty($OrderID)) {
            $OrderID = $order_info['order_id'];
        }

        fn_log_event('orders', 'payment_status', array(
            'order_id' => $OrderID,
            'tags' => $mode
        ));

        $pp_response['order_status'] = 'O';
        $pp_response['payment_status'] = 'A';
        $pp_response['reason_text'] = __('transaction_approved');
        $pp_response['approval_code'] = $_POST['ApprovalCode'];

        //fn_change_order_status($OrderID, 'O');
        fn_set_notification('N', __('order_placed'), __('text_order_placed_successfully'));
        fn_finish_payment($OrderID, $pp_response);

//        if(!empty($_SESSION['rbko_products'])){
//            foreach($_SESSION['rbko_products'] as $product => $value){
//                if($p[$product] != null && $redis_connected){
//                    $p_count = $redis->get('rbko_' . $product);
//                    if($p_count !== false){
//                        $redis->decrBy('rbko_' . $product, 1);
//                    }
//                }
//            }
//            unset($_SESSION['rbko_products']);
//        }

        fn_order_placement_routines('route', $OrderID, true, true, AREA, 'approved', true, array());
    }
} else {
    if ($mode != '' && ($mode == 'place_order' || $mode == 'request'))
    {

//        foreach($order_info['products'] as $product){
//            if($p[$product['product_id']] != null && $redis_connected){
//                $p_count = $redis->get('rbko_' . $product['product_id']);
//                if($p_count !== false){
//                    if($p_count < $p[$product['product_id']])
//                        $redis->incr('rbko_' . $product['product_id']);
//                    else{
//                        fn_set_notification('O', __('text_transaction_cancelled'), "Oops! Kemi shumë kërkesa për produktin '<strong>" . $product['product'] . "</strong>' duke u bërë përnjëherë, provo përsëri duke klikuar butonin <strong>KONFIRMO POROSINË</strong>!");
//                        return true;
//                    }
//                }else
//                    $redis->set('rbko_' . $product['product_id'], 1);
//                $_SESSION['rbko_products'][$product['product_id']] = 0;
//            }
//        }

        $MerchantID = $processor_data['processor_params']['merchant_id'];
        $TerminalID = $processor_data['processor_params']['terminal_id'];
        $signature = $processor_data['processor_params']['signature'];
        $CurrencyID = $processor_data['processor_params']['currency_id'];
        $url = $processor_data['processor_params']['terminal_url'];
        $description = "Pagesa për blerjen e produkteve në Gjirafa50";

        $OrderID = $order_id . "-" . "0" . "-" . $_SESSION['auth']['user_id'];
        $PurchaseTime = date("ymdHis");
        $TotalAmount = bcmul($order_info['total'], 100);
        $data = "$MerchantID;$TerminalID;$PurchaseTime;$OrderID;$CurrencyID;$TotalAmount;;";
        $fp = fopen("./app/payments/$MerchantID.pem", "r");
        $priv_key = fread($fp, 8192);
        fclose($fp);
        $pkeyid = openssl_get_privatekey($priv_key);
        openssl_sign($data, $signature, $pkeyid);
        openssl_free_key($pkeyid);
        $b64sign = base64_encode($signature);
        $email = \Tygh\Tygh::$app['session']['auth']['email'];

        $_SESSION["DAWH"] = md5($order_id . $TotalAmount . "ARLINDWASHEREANDFUCKUIFUDONTKNOWTHISSTRING!@#$%^");

        echo '<noscript><META HTTP-EQUIV="Refresh" CONTENT="0;URL=/index.php?dispatch=payment_notification.declined&payment=raiffeisenbank"></noscript>
		<form id="s" action="' . $url . '" method="post"><input name="Version" type="hidden" value="1" />
		<input name="MerchantID" type="hidden" value="' . $MerchantID . '" />
		<input name="TerminalID" type="hidden" value="' . $TerminalID . '" />
		<input name="TotalAmount" type="hidden" value="' . $TotalAmount . '" />
		<input name="Currency" type="hidden" value="' . $CurrencyID . '" />
		<input name="locale" type="hidden" value="RU" />
		<input name="PurchaseTime" type="hidden" value="' . $PurchaseTime . '" />
		<input name="OrderID" type="hidden" value="' . $OrderID . '" />
		<input name="PurchaseDesc" type="hidden" value="' . $description . '">
		<input name="Signature" type="hidden" value="' . $b64sign . '"/>
		<input name="Email" type="hidden" value="' . $email . '"/>
		<input type="submit" style="visibility:hidden" value="Paguaj tani"/></form>
		<style>body {text-align: center;font-family: sans-serif;color:#333;}</style><img src="https://hhstsyoejx.gjirafa.net/gj50/icons/processor-payment-loading.svg" style="display: block;margin: 50px auto;max-width: 50px;
"/>		Duke u lidhur me <strong>Raiffeisen Bank</strong>
<script  data-cfasync="false" type="text/javascript">document.getElementById("s").submit();</script>
		';
        exit;
    }
}
