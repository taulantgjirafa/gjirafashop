<?php

defined('BOOTSTRAP') or die('Access denied');

if (defined('PAYMENT_NOTIFICATION')) {
    // unset($_SESSION['auth']['iutecredit']);

	if ($mode == 'approved') {
		$pp_response['order_status'] = 'O';

		fn_finish_payment($_REQUEST['order_id'], $pp_response);
        fn_order_placement_routines('route', $_REQUEST['order_id'], false);
	} elseif ($mode == 'bad_request') {
        fn_set_notification('W', __('warning'), 'Porosia dështoi, ju lutem kontrolloni të dhënat tuaja.');
        fn_redirect('checkout.checkout');
    } elseif ($mode == 'declined') {
		fn_set_notification('W', __('warning'), 'Porosia dështoi, ju lutem provoni një metodë tjetër të pagesës.');
        fn_redirect('checkout.checkout');
	}
} else {

	// $url = 'http://test.iute.tripledev.ee:7101/rest/e-shop/application/1.0';
 //    $token = base64_encode('girafa:test123');

 //    // $data = array(
 //    //     'agentId' => 542,
 //    //     'productId' => 2,
 //    //     'amount' => $_SESSION['auth']['iutecredit']['amount'],
 //    //     'period' => $_SESSION['auth']['iutecredit']['period'],
 //    //     'code' => $order_id,
 //    //     'firstName' => $order_info['firstname'],
 //    //     'lastName' => $order_info['lastname'],
 //    //     'idnp' => $_SESSION['auth']['iutecredit']['personal_number'],
 //    //     'email' => $order_info['email'],
 //    //     'phoneNr' => $order_info['b_phone'],
 //    //     'birthDate' => $_SESSION['auth']['iutecredit']['birthday'],
 //    //     'country' => 'XK',
 //    //     'consent' => true,
 //    // );

 //    $ch = curl_init();
 //    curl_setopt($ch, CURLOPT_URL, $url);
 //    curl_setopt($ch, CURLOPT_POST, 1);
 //    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
 //    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
 //    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
 //        'Authorization: Basic ' . $token
 //    ));

 //    $response = curl_exec($ch);
 //    $response_object = json_decode($response);
 //    $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

 //    curl_close($ch);

    $status = 200;

    if ($status == 200) {
		// fn_save_order_transaction(27, $response_object->refNr, $order_id, 'P', $data);

		$location = fn_url('payment_notification.approved&payment=iutecredit&order_id=' . $order_id);
		header('Location: ' . $location);
    } elseif ($status == 400) {
        // fn_save_order_transaction(27, 0, $order_id, 'D', $data);

        $location = fn_url('payment_notification.bad_request&payment=iutecredit&order_id=' . $order_id);
        header('Location: ' . $location);
    } else {
        // fn_save_order_transaction(27, 0, $order_id, 'D', $data);

		$location = fn_url('payment_notification.declined&payment=iutecredit&order_id=' . $order_id);
		header('Location: ' . $location);
    }

	exit();
}