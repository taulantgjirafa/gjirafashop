<?php
if (!defined('BOOTSTRAP')) { die('Access denied'); }
use Tygh\Registry;

function fn_minify_init_templater_post(&$view)
{
    $view->addPluginsDir(Registry::get('config.dir.addons') . 'minify/functions/smarty_plugins');
	$view->loadFilter('output', 'gzip');
}