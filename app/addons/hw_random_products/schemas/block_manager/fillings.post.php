<?php
/*
 * © 2014 Hungryweb
 * 
 * PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  
 * IN  THE "HW-LICENSE.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE. 
 * 
 * @website: www.hungryweb.net
 * @support: support@hungryweb.net
 *  
 */

$schema['auto_random'] = array (
        'subcategories' => array (
            'type' => 'checkbox',
            'default_value' => 'N'
        ),
        'cid' => array (
            'type' => 'picker',
            'option_name' => 'filter_by_categories',
            'picker' => 'pickers/categories/picker.tpl',
            'picker_params' => array(
                    'multiple' => true,
                    'use_keys' => 'N',
                    'view_mode' => 'table',
                    'no_item_text' => __('default_filter_by_location'),
            ),
            'unset_empty' => true, // remove this parameter from params list if the value is empty
        ),
    );

return $schema;