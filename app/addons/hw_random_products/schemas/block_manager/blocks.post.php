<?php
/*
 * © 2014 Hungryweb
 * 
 * PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  
 * IN  THE "HW-LICENSE.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE. 
 * 
 * @website: www.hungryweb.net
 * @support: support@hungryweb.net
 *  
 */

$schema['random_products'] = array (
        'content' => array (
            'items' => array (
                'type' => 'enum',
                'object' => 'products',
                'items_function' => 'fn_hw_random_products_get_products',
                'remove_indent' => true,
                'hide_label' => true,
                'fillings' => array (
                    'auto_random' => array (
                      'params' => array ( )
                    )
                )
            ),
        ),
        'templates' => 'blocks/products',
        'settings' => array(
        'hide_add_to_cart_button' => array (
            'type' => 'checkbox',
            'default_value' => 'Y'
        ),
        'limit' => array (
            'type' => 'input',
            'default_value' => 3
        ),            
        ),
        'wrappers' => 'blocks/wrappers'
);

return $schema;