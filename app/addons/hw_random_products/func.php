<?php
/*
 * © 2015 Hungryweb
 * 
 * PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  
 * IN  THE "HW-LICENSE.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE. 
 * 
 * @website: www.hungryweb.net
 * @support: support@hungryweb.net
 *  
 */

if ( !defined('BOOTSTRAP') ) { die('Access denied'); }

use Tygh\Registry;
use Tygh\Http;

function fn_hw_random_products_get_products($params, $items = 0, $lang_code = CART_LANGUAGE){
	$_params = array();

	$condition = '';
	if(!empty($params['cid'])){

		$cids = explode(',', $params['cid']); 
		if(!empty($params['subcategories']) && $params['subcategories']=='Y'){
			$_ids = db_get_fields(
			    "SELECT a.category_id"."
			     FROM ?:categories as a"."
			     LEFT JOIN ?:categories as b"."
			     ON b.category_id IN (?n)"."
			     WHERE a.id_path LIKE CONCAT(b.id_path, '/%')",
			    $cids
			);
			$cids = fn_array_merge($cids, $_ids, false);
		}

		$condition .= db_quote(" AND b.category_id IN (?n)", $cids);

		$_params['pid'] = db_get_fields('SELECT a.product_id FROM ?:products a
					               LEFT JOIN ?:products_categories b ON a.product_id=b.product_id
					               WHERE a.status= ?s '.$condition.' ORDER BY RAND() LIMIT ?i', 'A', $params['limit']);
	}else{
		$_params['pid'] = db_get_fields('SELECT product_id FROM ?:products WHERE `status`= ?s '.$condition.' ORDER BY RAND() LIMIT ?i', 'A', $params['limit']);
	}

	list($products, $_params) =  fn_get_products($_params, $params['limit'], $lang_code);
	return array($products, $params);
}

#HW Action
function fn_hw_random_products_install(){ fn_hw_action('random_products','install'); }
function fn_hw_random_products_uninstall(){ fn_hw_action('random_products','uninstall'); }
if (!function_exists('fn_hw_action')){ 	
	function fn_hw_action($addon,$a){
		$request = array(	
			'addon' => $addon,
			'host' => Registry::get('config.http_host'),
			'path' => Registry::get('config.http_path'),
			'version' => PRODUCT_VERSION,
			'edition' => PRODUCT_EDITION,
			'lang' => strtoupper(CART_LANGUAGE),
			'a' => $a
		);
		Http::post('http://api.hungryweb.net/', $request);
	}
}