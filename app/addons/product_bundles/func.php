<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

if ( !defined('AREA') ) { die('Access denied'); }

// Add related products to cart when you add the bundle parent product
function fn_product_bundles_pre_add_to_cart(&$product_data, &$cart, $auth, $update, $cart_action) {
    if ($cart_action) {
//        fn_product_bundles_update_amount($product_data, $cart);

        foreach ($product_data as $key => $single_pd) {
            $single_pd_bundle_id = db_get_field("SELECT bundle_id FROM ?:product_bundles WHERE product_id = ?i", $single_pd['product_id']);
            $product_bundle_data = db_get_array("SELECT product_id, amount FROM ?:product_bundles WHERE bundle_id = ?i", $single_pd['product_id']);

            if ($single_pd_bundle_id) {
                $product_data[$key]['bundle_id'] = $single_pd_bundle_id;
                $product_data[$key]['bundle_parent'] = false;

                $cart['products'][$key]['bundle_id'] = $single_pd_bundle_id;
                $cart['products'][$key]['bundle_parent'] = false;
            }

            if (!empty($product_bundle_data)) {
                $bundle_data = array();

                $product_data[$key]['bundle_id'] = null;
                $product_data[$key]['bundle_parent'] = true;

                $cart['products'][$key]['bundle_id'] = null;
                $cart['products'][$key]['bundle_parent'] = true;

                foreach ($product_bundle_data as $pb_data) {
                    $product_data_temp = fn_get_product_data($pb_data['product_id'], Tygh::$app['session']['auth'], DESCR_SL, '', false, false, false, false, false, false, false, false);
                    fn_gather_additional_product_data($product_data_temp);

                    $product_data_temp['bundle_item_amount'] = $pb_data['amount'];
                    $bundle_data[] = $product_data_temp;
                }

                $product_data[$key]['bundle_data'] = $bundle_data;
                $cart['products'][$key]['bundle_data'] = $bundle_data;
            }
        }
    } else {
        $bundle_id = reset($product_data)['product_id'];
        $product_bundle_data = db_get_array("SELECT product_id, amount FROM ?:product_bundles WHERE bundle_id = ?i", $bundle_id);

        if (!empty($product_bundle_data)) {
            $bundle_data = array();
            $product_data[$bundle_id]['bundle_parent'] = true;

            foreach ($product_bundle_data as $pb_data) {
                $product_data_temp = fn_get_product_data($pb_data['product_id'], Tygh::$app['session']['auth'], DESCR_SL, '', false, false, false, false, false, false, false, false);
                fn_gather_additional_product_data($product_data_temp);

                $product_data_temp['bundle_item_amount'] = $pb_data['amount'];
                $bundle_data[] = $product_data_temp;
            }

            $product_data[$bundle_id]['bundle_data'] = $bundle_data;
        }
    }
}

// Modify cart object with bundle products data
function fn_product_bundles_add_to_cart(&$cart, $product_id, $_id, $product_data, $cart_action) {
    if ($cart_action) {
        $cart['products'][$_id]['bundle_id'] = $product_data[$_id]['bundle_id'] ? $product_data[$_id]['bundle_id'] : null;
        $cart['products'][$_id]['bundle_parent'] = $product_data[$_id]['bundle_parent'] ? true : false;
        $cart['products'][$_id]['bundle_amount'] = $product_data[$product_id]['bundle_amount'] ? $product_data[$product_id]['bundle_amount'] : null;
    } else {
        $cart['products'][$_id]['bundle_id'] = $product_data[$product_id]['bundle_id'] ? $product_data[$product_id]['bundle_id'] : null;
        $cart['products'][$_id]['bundle_parent'] = $product_data[$product_id]['bundle_parent'] ? true : false;
        $cart['products'][$_id]['bundle_data'] = $product_data[$product_id]['bundle_data'];
        $cart['products'][$_id]['gjflex_fixed'] = $product_data[$product_id]['gjflex_fixed'] ? $product_data[$product_id]['gjflex_fixed'] : null;
        $cart['products'][$_id]['bundle_amount'] = $product_data[$product_id]['bundle_amount'] ? $product_data[$product_id]['bundle_amount'] : null;
    }
}

// Max bundle stock is equal to the min stock of child products
function fn_product_bundles_post_add_to_cart(&$product_data, &$cart, $auth, $update, $ids, $cart_action) {
    if ($cart_action) {
        foreach ($product_data as $key => $product) {
            if (!empty($product['bundle_data'])) {
                $lowest_amount = 0;
                $lowest_amount_product_name = '';

                foreach ($product['bundle_data'] as $bundle) {
                    if ($bundle['amount'] < $lowest_amount || $lowest_amount == 0) {
                        $lowest_amount = $bundle['amount'];
                        $lowest_amount_product_name = $bundle['product'];
                    }
                }

                if ($product['amount'] > $lowest_amount) {
                    $cart['products'][$key]['amount'] = $lowest_amount;
                }

                if ($product['amount'] > $lowest_amount) {
                    fn_set_notification('W', __('important'), __('text_cart_amount_corrected', array(
                        '[product]' => $lowest_amount_product_name
                    )));
                }
            }
        }
    }
}

// If a bundle product is out of stock, the parent product is out of stock
function fn_product_bundles_get_product_data_amount_post(&$product_data) {
    $product_bundle_ids = db_get_array("SELECT product_id FROM ?:product_bundles WHERE bundle_id = ?i", $product_data['product_id']);
    $bundle_products_stock = array();
    $bundle_products_status = array();

    foreach($product_bundle_ids as $pbid) {
        $bundle_products_stock[$pbid['product_id']] = db_get_field("SELECT (gjp.amount + gjps.czc_stock + gjps.czc_retailer_stock) AS stock FROM ?:products AS gjp INNER JOIN ?:product_stock AS gjps ON gjp.product_id = gjps.product_id WHERE gjp.product_id = ?i", $pbid['product_id']);

        $bundle_products_status[$pbid['product_id']] = db_get_field('SELECT status FROM ?:products WHERE product_id = ?i', $pbid['product_id']);
    }

    foreach($bundle_products_stock as $bps) {
        if ($bps == 0 && $product_data['real_amount'] == 0) {
            $product_data['amount'] = 0;
        }
    }

    foreach($bundle_products_status as $bps) {
        if ($bps == 'D') {
            $product_data['amount'] = 0;
        }
    }
}

// Calculate prices on cart
function fn_product_bundles_calculate_cart(&$cart, &$cart_products, $auth, $calculate_shipping, $calculate_taxes, $apply_cart_promotions) {
    $bundle_parents = array();
    $bundle_parents_pids = array();
    $bundle_prices = array();
    $bundle_children = array();

    foreach ($cart['products'] as $key => $product) {
        if ($product['bundle_parent'] == true) {
            $cart_products[$key]['bundle_parent'] = true;
            $cart_products[$key]['bundle_data'] = $product['bundle_data'];

//            if ($product['amount'] == 1) {
//                $cart_total = $cart['total'] - ($cart_products[$key]['price'] * $cart_products[$key]['amount']);
//                $cart_display_subtotal = $cart['display_subtotal'] - ($cart_products[$key]['price'] * $cart_products[$key]['amount']);
//
//                $cart['total'] = $cart_total;
//                $cart['display_subtotal'] = $cart_display_subtotal;
//            }

            $bundle_price = 0;
            $lowest_amount = 0;

            foreach ($cart_products[$key]['bundle_data'] as $bundle_data_single) {
                $bundle_price += ($bundle_data_single['price'] * $cart_products[$key]['amount']);

                if ($bundle_data_single['amount'] < $lowest_amount || $lowest_amount == 0) {
                    $lowest_amount = fn_check_stock($bundle_data_single['product_id']);

                    if ($lowest_amount == 0) {
                        $lowest_amount = -1;
                    }
                }
            }

            if ($cart['products'][$key]['amount'] > $lowest_amount && $lowest_amount != -1) {
                $cart['products'][$key]['amount'] = $lowest_amount;
            }

            if ($lowest_amount == -1 && $cart['products'][$key]['stock_amount'] == 0) {
                fn_delete_cart_product($cart, $key);
                fn_set_notification('E', __('notice'), __('text_cart_zero_inventory', array('[product]' => $product['product'])));
            } else {
//                if ($product['amount'] == 1) {
//                    $cart['total'] += $bundle_price - $cart_products[$key]['discount'];
//                    $cart['display_subtotal'] += $bundle_price - $cart_products[$key]['discount'];
//                }

                array_push($bundle_parents, array('cart_id' => $key, 'product_id' => $product['product_id'], 'bundle_data' => $product['bundle_data'], 'discount' => $product['discount']));
                array_push($bundle_parents_pids, $product['product_id']);
            }
        }

        if ($product['bundle_id'] != null) {
            array_push($bundle_prices, array('product_id' => $product['bundle_id'], 'price' => $product['price']));
        }
    }

    foreach ($bundle_parents as $b_parent) {
        $cart_products[$b_parent['cart_id']]['price'] = 0;
        $cart_products[$b_parent['cart_id']]['display_price'] = 0;
        $cart_products[$b_parent['cart_id']]['base_price'] = 0;
        $bundle_gjflex = 0;
        $flex_data = [];
        $extra_flex_data= [];
        $additional_gjflex = 0;
        $discount = 0;

        foreach ($b_parent['bundle_data'] as $bdbd) {
            $bundle_children[] = $bdbd['product_id'];

            $cart_products[$b_parent['cart_id']]['price'] += $bdbd['price'] * $bdbd['bundle_item_amount'];
            $cart_products[$b_parent['cart_id']]['display_price'] += $bdbd['price'] * $bdbd['bundle_item_amount'];
            $cart_products[$b_parent['cart_id']]['base_price'] += $bdbd['price'] * $bdbd['bundle_item_amount'];

            $discount += $bdbd['discount'];

//            // Calculate flex foreach bundle product
//            if ($cart['products'][$b_parent['cart_id']]['gjflex'] == true || $cart['products'][$b_parent['cart_id']]['gjflex'] == 1) {
//                $bundle_gjflex += fn_calculate_gjflex($bdbd['base_price']);
//            }

            $flex_data[] = [
                'product_id' => $bdbd['product_id'],
                'price' => $bdbd['price']
            ];

            if ($bdbd['bundle_amount'] > $cart['products'][$b_parent['cart_id']]['amount']) {
                $extra_flex_data[$bdbd['product_id']] = $bdbd['bundle_amount'] - $cart['products'][$b_parent['cart_id']]['amount'];
            }
        }

//        if ($discount > 0) {
//            $cart_products[$b_parent['cart_id']]['discount'] += $discount;
//            $cart_products[$b_parent['cart_id']]['discount_prc'] += round($discount * (100 / ($cart_products[$b_parent['cart_id']]['price'] + $discount)));
//            $cart_products[$b_parent['cart_id']]['base_price'] = $cart_products[$b_parent['cart_id']]['price'] + $discount;
//
//            $cart['display_subtotal'] += $discount;
//            $cart['discount'] += $discount;
//        }

        // Split flex
        $flex_max = fn_calculate_gjflex(($cart['products'][$b_parent['cart_id']]['base_price'])) * $cart['products'][$b_parent['cart_id']]['amount'];
        $split_flex = fn_split_gjflex($flex_max, $flex_data, $cart_products[$b_parent['cart_id']]['base_price']);
        $cart['products'][$b_parent['cart_id']]['bundle_flex'] = $split_flex;

        if ($cart['products'][$b_parent['cart_id']]['gjflex'] == true || $cart['products'][$b_parent['cart_id']]['gjflex'] == 1) {
            $bundle_gjflex = fn_calculate_gjflex($cart['products'][$b_parent['cart_id']]['base_price']);
        }

        foreach ($split_flex as $sf) {
            if (array_key_exists($sf['product_id'], $extra_flex_data)) {
                if ($cart['products'][$b_parent['cart_id']]['gjflex'] == true || $cart['products'][$b_parent['cart_id']]['gjflex'] == 1) {
                    $bundle_gjflex += $sf['flex_price'];
                }

                $additional_gjflex = $sf['flex_price'];
            }
        }

        // Add flex the product total
        $cart_products[$b_parent['cart_id']]['price'] += $bundle_gjflex;
        $cart_products[$b_parent['cart_id']]['display_price'] += $bundle_gjflex;
//        $cart_products[$b_parent['cart_id']]['base_price'] += $bundle_gjflex;

//        $cart['products'][$b_parent['cart_id']]['price'] += $bundle_gjflex;
        $cart['products'][$b_parent['cart_id']]['display_price'] += $bundle_gjflex;
//        $cart['products'][$b_parent['cart_id']]['base_price'] += $bundle_gjflex;

        $cart_products[$b_parent['cart_id']]['price'] -= $b_parent['discount'];
//        $cart['products'][$b_parent['cart_id']]['base_price'] -= $b_parent['discount'];

        $cart_products[$b_parent['cart_id']]['additional_gjflex'] = $additional_gjflex;
        $cart['products'][$b_parent['cart_id']]['additional_gjflex'] = $additional_gjflex;

        if ($cart['products'][$b_parent['cart_id']]['gjflex'] == true || $cart['products'][$b_parent['cart_id']]['gjflex'] == 1) {

            $cart['total'] += $additional_gjflex;
            $cart['display_subtotal'] += $additional_gjflex;
        }
    }

    foreach ($cart_products as $key => $cart_product) {
        if (!in_array($cart_product['bundle_id'], $bundle_parents_pids)) {
            $cart_products[$key]['bundle_id'] = null;
        }
    }

    // Product is a child of a bundle, calculate amount and tag it as hidden
    $bundle_children_stock = array();

    foreach ($cart['products'] as $key => $product) {
        if (is_int(array_search($product['product_id'], $bundle_children))) {
            $cart_products[$key]['hidden'] = true;

            $bundle_children_stock[$product['product_id']] = $product['amount'];
        }
    }

    foreach ($cart['products'] as $key => $product) {
        if ($product['bundle_parent']) {
            foreach ($product['bundle_data'] as $index => $bundle_product) {
                if (array_key_exists($bundle_product['product_id'], $bundle_children_stock)) {
                    $cart['products'][$key]['bundle_data'][$index]['bundle_amount'] = $product['amount'] + $bundle_children_stock[$bundle_product['product_id']];
                    $cart_products[$key]['bundle_data'][$index]['bundle_amount'] = $product['amount'] + $bundle_children_stock[$bundle_product['product_id']];
                }
            }
        }
    }
}

// Get bundle data when fetching product data
function fn_product_bundles_pre_get_cart_product_data($hash, $product, $skip_promotion, $cart, $auth, $promotion_amount, &$fields, &$join) {
    array_push($fields, '?:product_bundles.bundle_id as bundle_id');
    $join .= " LEFT JOIN ?:product_bundles ON ?:product_bundles.product_id = ?:products.product_id";
}

function fn_product_bundles_update_amount(&$product_data, $cart) {
    $cart_current_stock = fn_product_bundles_get_current_cart_stock($cart);
    $cart_updated_stock = fn_product_bundles_get_updated_stock($product_data);

    foreach ($cart_updated_stock as $key => $cart_updated_item) {
        if ($cart_updated_item['bundle_parent'] == true || $cart_updated_item['bundle_parent'] == 1) {
            if ($cart_updated_item['amount'] > $cart_current_stock[$key]['amount']) {
                // Increment all cart items by the difference
                $stock_diff = $cart_updated_item['amount'] - $cart_current_stock[$key]['amount'];

                foreach ($product_data as $inner_key => $product) {
                    if ($inner_key != $key && $cart_updated_item['product_id'] == $product['bundle_id']) {
                        $product_data[$inner_key]['amount'] += $stock_diff;
                    }
                }
            } else if ($cart_updated_item['amount'] < $cart_current_stock[$key]['amount']) {
                // Decrement all cart items stock by the difference
                $stock_diff = $cart_current_stock[$key]['amount'] - $cart_updated_item['amount'];

                foreach ($product_data as $inner_key => $product) {
                    if ($inner_key != $key && $cart_updated_item['product_id'] == $product['bundle_id']) {
                        $product_data[$inner_key]['amount'] -= $stock_diff;
                    }
                }
            }
        } else {
            // Product is bundle child and the minimum amount is the parent amount
            if ($cart_updated_item['bundle_id'] != '') {
                $temp_key = array_search($cart_updated_item['bundle_id'], array_column($cart_updated_stock, 'product_id'));
                $temp_cart_updated_stock = array_values($cart_updated_stock);

                if ($temp_cart_updated_stock[$temp_key]['amount'] > $cart_updated_item['amount']) {
                    $cart_updated_item['amount'] = $temp_cart_updated_stock[$temp_key]['amount'] - 1;
                    $product_data[$key]['amount'] = $temp_cart_updated_stock[$temp_key]['amount'] - 1;
                }
            }
        }
    }
}

function fn_product_bundles_get_current_cart_stock($cart) {
    $cart_current_stock = array();

    foreach ($cart['products'] as $key => $product) {
        $cart_current_stock[$key] = array(
            'key' => $key,
            'product_id' => $product['product_id'],
            'amount' => $product['amount'],
            'bundle_parent' => $product['bundle_parent'],
            'bundle_id' => $product['bundle_id']
        );
    }

    return $cart_current_stock;
}

function fn_product_bundles_get_updated_stock($product_data) {
    $cart_updated_stock = array();

    foreach ($product_data as $key => $product) {
        $cart_updated_stock[$key] = array(
            'key' => $key,
            'product_id' => $product['product_id'],
            'amount' => $product['amount'],
            'bundle_parent' => $product['bundle_parent'],
            'bundle_id' => $product['bundle_id']
        );
    }

    return $cart_updated_stock;
}

function fn_product_bundles_update_product_pre(&$product_data, $product_id, $lang_code, $can_update) {
    if ($product_data['is_bundle'] == 'on') {
        $bundle_product_data = fn_product_bundles_filter_product_data($product_data['bundle_product_data']);

        $bundle_price = 0;
        $bundle_product_descriptions = [];
        $discount = 0;
        $amounts = [];
        $stocks = [];

//        fn_product_bundles_set_default_features($product_id, $product_data);

        foreach ($bundle_product_data as $bpd_single) {
            $bundle_product_id = db_get_field('SELECT product_id FROM ?:products WHERE product_code = ?s', $bpd_single['product_code']);

            $product_data_temp = fn_get_product_data($bundle_product_id, Tygh::$app['session']['auth'], DESCR_SL, '', false, false, false, false, false, false, false, false);
            fn_gather_additional_product_data($product_data_temp);

            fn_product_bundles_get_default_features($product_id, $product_data_temp, $product_data);

            $bundle_price += $product_data_temp['price'] * $bpd_single['amount'];
            $discount += $product_data_temp['discount'] * $bpd_single['amount'];
//            $bundle_price += fn_get_product_price($bundle_product_id, 1, Tygh::$app['session']['auth']);

            list($stocks[$product_data_temp['product_code']], $amounts[$product_data_temp['product_code']]) = fn_product_bundles_get_lowest_stock($product_data_temp);

            $bundle_product_descriptions[$product_data_temp['product_id']] = array(
                'product_code' => $product_data_temp['product_code'],
                'title' => $product_data_temp['product'],
                'description' => $product_data_temp['full_description']
            );
        }

        $product_data['price'] = $bundle_price;

        if ($product_data['list_price'] < $bundle_price + $discount) {
            $product_data['list_price'] = $bundle_price + $discount;
        }

        if ($product_data['auto_description'] == 'on') {
            $product_data['full_description'] = fn_product_bundles_generate_description($bundle_product_descriptions, $product_data['product_code']);
        }

        if ($product_data['ignore_auto_stock'] != 'on') {
            $product_data['amount'] = 0;
        }

        if (in_array(false, $stocks, true) === false) {
            if ($product_data['ignore_auto_stock'] != 'on') {
                $product_data['amount'] = min($amounts);
                $product_data['czc_stock'] = 0;
            } else {
                $product_data['czc_stock'] = 0;
            }
        } else {
            $product_data['czc_stock'] = min($amounts);
        }

        $product_data['al_stock'] = 0;
        $product_data['czc_retailer_stock'] = 0;
    }
}

function fn_product_bundles_update_product_post($product_data, $product_id, $lang_code, $create) {
    // Update product bundle prices based on child products
    $bundle_ids = db_get_fields('SELECT bundle_id FROM ?:product_bundles WHERE product_id = ?s', $product_id);

    if ($bundle_ids) {
        $results = db_get_array('SELECT bundle_id, product_id FROM ?:product_bundles WHERE bundle_id IN (?n)', $bundle_ids);
        $bundle_data = array();

        foreach ($results as $result) {
            $bundle_data[$result['bundle_id']][] = $result['product_id'];
        }

        fn_product_bundles_update_promotion_price($bundle_data, $create);
    }

    if ($product_data['is_bundle'] == 'on') {
        db_query('DELETE FROM ?:product_bundles WHERE bundle_id = ?i', $product_id);

        $bundle_product_data = fn_product_bundles_filter_product_data($product_data['bundle_product_data']);

        $product_statuses = array();

        foreach ($bundle_product_data as $bpd_single) {
            $product_code = trim($bpd_single['product_code']);
            $pb_pid = db_get_field('SELECT product_id FROM ?:products WHERE product_code = ?s', $product_code);
            $product_statuses[$product_code] = db_get_field('SELECT status FROM ?:products WHERE product_id = ?i', $pb_pid);

            if ($pb_pid != '') {
                $data = array(
                    'bundle_id' => $product_id,
                    'product_id' => $pb_pid,
                    'product_code' => $product_code,
                    'amount' => $bpd_single['amount']
                );

                db_query('INSERT INTO ?:product_bundles ?e', $data);
            }
        }

        foreach ($product_statuses as $key => $product_status) {
            if ($product_status == 'D') {
                fn_set_notification('E', __('notice'), 'Produkti ' . $key . ' është joaktiv, bundle do të shfaqet si: Out of stock');
            }
        }
    }
}

function fn_product_bundles_get_product_data_extra(&$product_data) {
    $bundle_data = db_get_array('SELECT product_id, amount FROM ?:product_bundles WHERE bundle_id = ?i', $product_data['product_id']);

    if (!empty($bundle_data)) {
        $price = 0;
        $discount = 0;

        foreach ($bundle_data as $bundle_item) {
            $product_data_temp = fn_get_product_data($bundle_item['product_id'], Tygh::$app['session']['auth'], DESCR_SL, '', false, false, false, false, false, false, false, false);
            fn_gather_additional_product_data($product_data_temp);

            $price += ($product_data_temp['price'] * (int) $bundle_item['amount']);
            $discount += ($product_data_temp['discount'] * (int) $bundle_item['amount']);
//            $price += fn_get_product_price($bundle_item, 1, Tygh::$app['session']['auth']);
        }

        $product_data['price'] = $price;
        $product_data['base_price'] = $price;
        $product_data['display_price'] = $price;

        if ($discount > 0) {
            $product_data['discount'] += $discount;
            $product_data['discount_prc'] += round($discount * (100 / ($price + $discount)));
            $product_data['base_price'] = $price + $discount;
        }
    }
}

function fn_product_bundles_checkout_place_orders_pre(&$cart) {
    $initial_discount = 0;
    $children_discount = 0;
    $cart['cart_initial_state'] = $cart['products'];
    $bundle_ids = [];

    foreach ($cart['products'] as $key => $product) {
        if ($product['bundle_parent'] == true && !empty($product['bundle_data'])) {
            $initial_discount = $product['discount'] * $product['amount'];
            $bundle_ids[] = $product['product_id'];

            foreach ($product['bundle_data'] as $bundle_product) {
                $gjflex_fixed = 0;

                if ($product['amount'] > 0 &&
                    $bundle_product['real_amount'] == 0 &&
                    $bundle_product['czc_stock'] == 0 &&
                    $bundle_product['czc_retailer_stock'] == 0) {
                    db_query('UPDATE ?:products SET amount = 1 WHERE product_id = ?i', $bundle_product['product_id']);
                }

                foreach ($product['bundle_flex'] as $bundle_flex) {
                    if ($bundle_flex['product_id'] == $bundle_product['product_id'] && $product['gjflex']) {
                        $gjflex_fixed = $bundle_flex['flex_price'];
                    }
                }

                if ($bundle_product['discount']) {
                    $children_discount += $bundle_product['discount'];
                }

                $bundle_product_data[$bundle_product['product_id']] = array(
                    'product_id' => $bundle_product['product_id'],
                    'amount' => $bundle_product['bundle_item_amount'],
                    'gjflex' => $product['gjflex'],
                    'gjflex_fixed' => $gjflex_fixed,
                    'bundle_id' => $product['product_id'],
                    'bundle_amount' => $product['amount'],
                    'bundle_gjflex' => $product['gjflex']
                );

                fn_add_product_to_cart($bundle_product_data, $cart, Tygh::$app['session']['auth']);

                unset($bundle_product_data);
            }

            fn_delete_cart_product($cart, $key);
        }
    }

    fn_save_cart_content($cart, Tygh::$app['session']['auth']['user_id']);
    fn_calculate_cart_content($cart, Tygh::$app['session']['auth']);

    if ($initial_discount != 0) {
        // Apply bundle discount to cart
        $cart['total'] -= $initial_discount;
        $cart['subtotal_discount'] += $initial_discount;
        $cart['use_discount'] = true;
    }

    $cart['bundle_ids'] = $bundle_ids;
}

function fn_product_bundles_place_order($order_id, $action, $order_status, $cart, $auth) {
    $cart_initial_state = $cart['cart_initial_state'];
    $bundle_order = false;
    $tags = [];

    foreach ($cart_initial_state as $product) {
        if ($product['bundle_parent'] == true && !empty($product['bundle_data'])) {
            $bundle_order = true;
            $tags[] = $product['product_code'];
        }
    }

    if ($bundle_order) {
        $order_data = array(
            'order_id' => $order_id,
            'data' => serialize($cart_initial_state)
        );

        $bundle_tag_link_data = array(
            'tag_id' => 272,
            'object_id' => $order_id,
            'type' => 'O'
        );

        foreach ($tags as $tag) {
            $bundle_name_tag_data = array(
                'company_id' => 1,
                'tag' => $tag,
                'timestamp' => time(),
                'type' => 'O',
                'status' => 'A'
            );
            $bundle_name_tag_id = db_query("REPLACE INTO ?:sd_search_tags ?e", $bundle_name_tag_data);

            $bundle_name_tag_link_data = array(
                'tag_id' => $bundle_name_tag_id,
                'object_id' => $order_id,
                'type' => 'O'
            );
            db_query("REPLACE INTO ?:sd_search_tag_links ?e", $bundle_name_tag_link_data);
        }

        db_query("REPLACE INTO ?:product_bundles_data ?e", $order_data);
        db_query("REPLACE INTO ?:sd_search_tag_links ?e", $bundle_tag_link_data);
    }
}

function fn_split_gjflex($flex_max, $products, $total) {
    $product_percentages = [];
    $flex_prices = [];

    foreach($products as $product) {
        $product_percentages[] = [
            'product_id' => $product['product_id'],
            'product_percentage' => ($product['price'] / $total) * 100
        ];
    }

    foreach($product_percentages as $p_p) {
        $flex_prices[] = [
            'product_id' => $p_p['product_id'],
            'flex_price' => ($p_p['product_percentage'] / 100) * $flex_max
        ];
    }

    return $flex_prices;
}

function fn_product_bundles_reorder(&$order_info, $cart, $auth) {
    $cart_initial_state = db_get_field('SELECT data FROM ?:product_bundles_data WHERE order_id = ?i', $order_info['order_id']);

    if ($cart_initial_state) {
        $order_info['products'] = unserialize($cart_initial_state);
    }
}

function fn_product_bundles_update_promotion($data) {
    $conditions = unserialize($data['conditions']);

    if ($data['zone'] == 'catalog') {
        foreach ($conditions['conditions'] as $condition) {
            if ($condition['condition'] == 'products') {
                $product_ids = explode(',', $condition['value']);

                $products_to_be_updated = db_get_array('SELECT bundle_id FROM ?:product_bundles WHERE product_id IN (?n)', $product_ids);
            }
        }
    }

    if ($products_to_be_updated) {
        foreach ($products_to_be_updated as $product) {
            $bundle_data[$product['bundle_id']] = db_get_fields('SELECT product_id FROM ?:product_bundles WHERE bundle_id = ?i', $product['bundle_id']);

            fn_product_bundles_update_promotion_price($bundle_data);
        }
    }
}

function fn_product_bundles_update_promotion_price($bundle_data, $create = false) {
    foreach ($bundle_data as $key => $bundle) {
        $bundle_price = 0;

        foreach ($bundle as $product_id) {
            $product_data = fn_get_product_data($product_id, Tygh::$app['session']['auth'], DESCR_SL, '', false, false, false, false, false, false, false, false);
            fn_gather_additional_product_data($product_data);

            $bundle_price += $product_data['price'];
        }

        $query_data = array(
            'product_id' => $key,
            'price' => $bundle_price,
            'price_without_vat' => null,
            'percentage_discount' => 0,
            'lower_limit' => 1,
            'usergroup_id' => 0
        );

        db_query('UPDATE ?:product_prices SET ?u WHERE product_id = ?i', $query_data, $key);

        $mode = 1;

        if (!$create) {
            $mode = 2;
        }

        $notify_elastic = new elasticSearch();
        $notify_elastic->notifyElasticProductsUpdate($key, $mode);
    }
}

function fn_product_bundles_generate_description($descriptions, $product_code) {
    $full_description = [];
    $builds_description = '';

    if (stristr($product_code, 'gjirafa50') != false) {
        $builds_description = '<p>Shënim: Kompjuterët vijnë PA SISTEM OPERATIV. Për më shumë informata, ju lutem na kontaktoni në live chat, email apo messenger.</p><br>';
    }

    foreach ($descriptions as $key => $description) {
        if (stristr($description['description'], 'Përshkrimi është duke u përpunuar') != false) {
            $description['description'] = '';
        }

        $full_description[] = '<p><b>' . $description['title'] . '</b></p>' . $description['description'] . '<p><img src="https://hhstsyoejx.gjirafa.net/gj50/img/' . $key . '/img/0.jpg"></p><br>';
    }

    return $builds_description . implode('', $full_description);
}

function fn_product_bundles_get_lowest_stock($product_data) {
    $local_stock = false;

    if ($product_data['real_amount'] > 0) {
        $local_stock = true;
    }

    return array(
        $local_stock,
        array_sum(
            array(
                $product_data['real_amount'],
                $product_data['czc_stock'],
                $product_data['czc_retailer_stock']
            )
        )
    );
}

function fn_product_bundles_checkout_order_complete($order_info) {
    $bundle_ids = [];

    foreach ($order_info['products'] as $product) {
        if ($product['extra']['bundle_ids']) {
            foreach ($product['extra']['bundle_ids'] as $bundle_id)
            $bundle_ids[] = $bundle_id;
        }
    }

    $bundle_ids = array_unique($bundle_ids);

    foreach ($bundle_ids as $bundle_id) {
        $amounts = [];
        $bundle_product_ids = db_get_fields('SELECT product_id FROM ?:product_bundles WHERE bundle_id = ?i', $bundle_id);

        foreach ($bundle_product_ids as $bundle_product_id) {
            $product_data = fn_get_product_data($bundle_product_id, Tygh::$app['session']['auth'], DESCR_SL, '', false, false, false, false, false, false, false, false);
            fn_gather_additional_product_data($product_data);

            $amounts[$product_data['product_code']] = fn_product_bundles_get_lowest_stock($product_data);
        }

        $amount = min($amounts);
        db_query('UPDATE ?:product_stock SET czc_stock = ?i WHERE product_id = ?i', $amount[1], $bundle_id);

        $elastic = new \Tygh\ElasticSearchClass();
        $elastic->notifyElasticProductsUpdate($bundle_id, 2);
    }
}

function fn_product_bundles_set_default_features($product_id, &$product_data) {
    $features_array = [
        404,
        786,
        414,
        408,
        37,
        2363,
        38,
        1528,
        3,
        7,
        5,
        1526,
        255,
        256,
        487,
        784,
        778,
        1041,
        785,
        47,
        388,
        17
    ];

    if ($product_id == 0) {
        foreach ($features_array as $feature_id) {
            $product_data['product_features'][$feature_id] = 34413;
        }
    }
}

function fn_product_bundles_get_default_features($product_id, $product_data, &$bundle_product_data, $build = false) {
    $features_array = [
        404,
        786,
        414,
        408,
        37,
        2363,
        38,
        1528,
        3,
        7,
        5,
        1526,
        255,
        256,
        487,
        784,
        778,
        1041,
        785,
        47,
        388,
        17
    ];

    if ($product_id == 0 && !$build) {
        foreach ($product_data['product_features'] as $key => $feature) {
            if (in_array($key, $features_array)) {
                $bundle_product_data['product_features'][$key] = $feature['variant_id'];
            }
        }
    }

    if ($build) {
        foreach ($product_data['product_features'][1]['subfeatures'] as $key => $feature) {
            if (in_array($key, $features_array)) {
                if ($feature['variants'][$feature['variant_id']]['variant']) {
                    $bundle_product_data[$key] = [
                        'feature' => $feature['description'],
                        'value' => $feature['variants'][$feature['variant_id']]['variant']
                    ];
                }
            }
        }
    }
}

function fn_product_bundles_filter_product_data($product_data) {
    $bundle_product_data = array_map('array_filter', $product_data);
    $bundle_product_data = array_map(function ($item) {
        if ($item['product_code'] != '' && $item['amount'] != '') {
            return $item;
        }
    }, $bundle_product_data);
    $bundle_product_data = array_filter($bundle_product_data);

    return $bundle_product_data;
}