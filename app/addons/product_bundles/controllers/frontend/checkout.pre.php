<?php
/***************************************************************************
 *                                                                          *
 *   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 *                                                                          *
 ****************************************************************************
 * PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
 * "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
 ****************************************************************************/

use Tygh\Registry;
use Tygh\Storage;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

$cart = & Tygh::$app['session']['cart'];

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    return;
}

if ($mode == 'remove') {
    $bundle_cart_ids = array();

    foreach ($cart['products'] as $x => $x_value) {
        if (!empty($product_id)) {
            // Delete bundle products when parent is deleted
            if ($x_value['bundle_id'] == $product_id) {
                $bundle_cart_ids[] = $x;
            }
        }
    }

    // Delete bundle products when child is deleted
    foreach ($cart['products'] as $x => $x_value) {
        if (!empty($cart_id)) {
            if ($x_value['bundle_parent'] == true && $cart['products'][$cart_id]['bundle_id'] == $x_value['product_id']) {
                $bundle_cart_ids[] = $x;
            }
        }
    }

    if (!empty($bundle_cart_ids)) {
        foreach ($bundle_cart_ids as $bcid) {
            fn_delete_cart_product($cart, $bcid);
        }
    }
}
