<?php
/***************************************************************************
 *                                                                          *
 *   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 *                                                                          *
 ****************************************************************************
 * PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
 * "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
 ****************************************************************************/

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($mode == 'update') {
    $bundle_products = db_get_array('SELECT product_code, amount FROM ?:product_bundles WHERE bundle_id = ?i', $_REQUEST['product_id']);

    if (!empty($bundle_products)) {
        Tygh::$app['view']->assign('is_bundle', true);
        Tygh::$app['view']->assign('bundle_products', $bundle_products);
    } else {
        Tygh::$app['view']->assign('is_bundle', false);
    }
}