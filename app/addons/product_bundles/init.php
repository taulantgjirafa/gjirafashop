<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

if ( !defined('AREA') ) { die('Access denied'); }

fn_register_hooks(
    'pre_add_to_cart',
    'get_product_data_amount_post',
    'add_to_cart',
    'post_add_to_cart',
    'calculate_cart',
    'pre_get_cart_product_data',
    'update_product_pre',
    'update_product_post',
    'get_product_data_extra',
    'checkout_place_orders_pre',
    'place_order',
    'reorder',
    'update_promotion',
    'checkout_order_complete'
);