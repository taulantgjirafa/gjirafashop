<?php
/***************************************************************************
 *                                                                          *
 *   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 *                                                                          *
 ****************************************************************************
 * PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
 * "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
 ****************************************************************************/

namespace Tygh\Api\Entities;

use Tygh;
use Tygh\Api\AEntity;
use Tygh\Api\Response;
use Tygh\Registry;
use Tygh\Api\Traits\ApiHelper;

class ProductBundles extends AEntity
{
    use ApiHelper;

    public function index($id = 0, $params = array())
    {
        // Update product bundle prices based on child products
        $results = db_get_array('SELECT bundle_id, product_id FROM ?:product_bundles');
        $bundle_data = array();

        foreach ($results as $result) {
            $bundle_data[$result['bundle_id']][] = $result['product_id'];
        }

        foreach ($bundle_data as $key => $bundle) {
            $bundle_price = 0;

            foreach ($bundle as $product_id) {
                $product_data = fn_get_product_data($product_id, Tygh::$app['session']['auth'], DESCR_SL, '', false, false, false, false, false, false, false, false);
                fn_gather_additional_product_data($product_data);

                $bundle_price += $product_data['price'];
            }

            $query_data = array(
                'product_id' => $key,
                'price' => $bundle_price,
                'price_without_vat' => null,
                'percentage_discount' => 0,
                'lower_limit' => 1,
                'usergroup_id' => 0
            );

            $data = db_query('UPDATE ?:product_prices SET ?u WHERE product_id = ?i', $query_data, $key);
        }

        $status = Response::STATUS_OK;

        return array(
            'status' => $status,
            'data' => $data
        );
    }

    public function create($params)
    {}

    public function update($id, $params)
    {}

    public function delete($id)
    {}

    public function privileges()
    {
        return array(
            'index'  => 'view_bundle',
            'create' => 'create_bundle',
            'update' => 'edit_bundle',
            'delete' => 'delete_bundle'
        );
    }
}
