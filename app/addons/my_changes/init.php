<?php

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

fn_register_hooks(
    'print_order_invoice_extra',
    'print_order_invoice_type',
    'checkout_order_complete',
    'delete_user',
    'tools_change_status',
    'get_product_data',
    'pre_get_cart_product_data',
    'user_logout_after'
);