<?php

$schema['central']['orders']['items']['order_logs'] = array(
    'href' => 'orders.order_logs',
    'alt' => 'order_logs_alt',
    'position' => 500
);

return $schema;