<?php
/***************************************************************************
 *                                                                          *
 *   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 *                                                                          *
 ****************************************************************************
 * PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
 * "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
 ****************************************************************************/

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7\Request;
use PalePurple\RateLimit\Adapter\APCu as APCAdapter;
use PalePurple\RateLimit\RateLimit;
use Tygh\EncryptionCollection;
use Tygh\Http;
use Tygh\Registry;
use Tygh\GjirafaIS;
use Tygh\EncryptionService;

if (!defined('BOOTSTRAP')) {die('Access denied');}

function ip_in_range($ip, $range) {
	if (strpos($range, '/') == false) {
		$range .= '/32';
	}
	// $range is in IP/CIDR format eg 127.0.0.1/24
	list($range, $netmask) = explode('/', $range, 2);
	$range_decimal = ip2long($range);
	$ip_decimal = ip2long($ip);
	$wildcard_decimal = pow(2, (32 - $netmask)) - 1;
	$netmask_decimal = ~$wildcard_decimal;
	return (($ip_decimal & $netmask_decimal) == ($range_decimal & $netmask_decimal));
}

function fn_notify_gjmall($order_id) {

	$products_obj = db_get_array('
          SELECT variant as ean ,?:products.amount as stock ,czc_stock , czc_retailer_stock FROM `?:order_details`
          Inner JOIN ?:product_stock
          on ?:product_stock.product_id = ?:order_details.product_id
          inner join ?:product_features_values
          On ?:product_features_values.product_id = ?:order_details.product_id
          Inner join ?:product_feature_variant_descriptions
          On ?:product_feature_variant_descriptions.variant_id = ?:product_features_values.variant_id
          inner join ?:products
          on ?:products.product_id = ?:order_details.product_id
          where order_id = ?i and ?:product_features_values.feature_id = 14', $order_id);

	foreach ($products_obj as $key => $value) {
		$products_obj[$key]['sign'] = '-';
	}

	try {
		$Elastic = new elasticSearch();
		$Elastic->getContent(Registry::get('config.gjmall_notifier'), json_encode($products_obj), 'Njqr|A$U}ye=Lh)K>htnRl*RNAD]e]+D69BrO|Fll>Y`zJIHp&hp1>D#FkNKgc|');

	} catch (Exception $e) {}

}

function fn_core_finish_payment(&$order_id, &$pp_response, &$force_notification) {
    if ($pp_response['order_status'] != 'I' || $pp_response['order_status'] != 'F' || $pp_response['order_status'] != 'B' && ENVIRONMENT == 'live') {
		fn_notify_gjmall($order_id);
	}
}

function fn_core_order_placement_routines($order_id, $force_notification, $order_info, $_error, $response, $checkout, $params) {
    fn_verify_order($order_info, AREA, $response, $checkout, $params);

    if (filter_var($params['prepaid'], FILTER_VALIDATE_BOOLEAN)) {
        fn_insert_order_tags('Parapagim', $order_info['order_id']);
    }

    foreach ($order_info['products'] as $product) {
        if (!is_numeric($product['product_code'])) {
            $split_product_code = preg_split('/(?<=[0-9])(?=[a-z]+)/i', $product['product_code']);
            fn_insert_order_tags($split_product_code[1], $order_info['order_id']);
        }

        if (is_string($product['product_code']) && in_array($product['product_code'], ['BiletaA', 'BiletaB'])) {
            fn_insert_order_tags('MastersLeague', $order_info['order_id']);
        }
    }
}

function fn_core_gather_additional_product_data_before_discounts(&$product, $auth, $params){
    $product['avail_since'] = db_get_field('SELECT avail_since from ?:products WHERE product_id = ?i', $product['product_id']);
}

function fn_core_before_dispatch(&$controller, &$mode, &$action, &$dispatch_extra, &$area) {
    define('IS_HOMEPAGE', ($controller == 'index' && $mode == 'index'));
//    define('IS_HOMEPAGE', false);

	if (AREA == 'C') {

        if (!defined('AJAX_REQUEST') && $area == 'C' && $controller != 'sso') {
            $gjirafa_auth = new gjirafa_auth();
        }

	    // Validate current user's authentication
//        fn_validate_user_auth($controller, $mode);

        $ip = fn_get_user_ip();

		$allowed_ips = array(
			'193.70.12.101',
			'217.182.134.12',
			'193.70.8.217',
			'137.74.203.11',
            '84.22.57.102',
            '84.22.58.78',
            '46.99.251.77',
            '178.132.223.5',
            '46.99.178.114',
            '46.99.251.215'
		);

		if (!in_array($ip, $allowed_ips)) {
//			fn_limit_rate(10, 1, new APCAdapter(), Tygh::$app['session']['security_hash']);
		}


		if (!$_COOKIE['rel'] || $_COOKIE['rel'] == '') {
			$rel_rand = mt_rand(1, 2);

			setcookie('rel', $rel_rand, time() + 86400);
			$_COOKIE['rel'] = $rel_rand;
		}
	}

	if (date('Y-m-d') == date('Y-m-d', strtotime('2018-04-17 00:00:00')) && $_GET['utm_source'] === 'GjirafaAdNetwork' && empty($_SESSION['anc'])) {
		$_SESSION['anc'] = $_GET['utm_source'] . "&utm_medium=" . $_GET['utm_medium'] . "&utm_campaign=" . $_GET['utm_campaign'] . "&utm_term=" . $_GET['utm_term'] . "&utm_content=" . $_GET['utm_content'];
		$_SESSION['anc_url'] = 'https://gjc.gjirafa.com/Home/anc?';
	}

//	if (!($controller == 'checkout' || $controller == 'wishlist')) {
//        fn_sync_cart_through_sessions();
//    }

    if (AREA == 'C' && !defined('AJAX_REQUEST') && $controller != 'checkout' && $controller != 'wishlist') {
        fn_extract_session_data();
    }
}

function fn_core_dispatch_before_send_response($status, $area, $controller, $mode, $action) {
//	$country = $_COOKIE["c"];
//    $req_domain = $_SERVER['SERVER_NAME'];
//    $req_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
//    $GJ50COM = "gjirafa50.com";
//    $GJ50AL = "gjirafa50.al";
////    $GJ50COM = "local.gjirafa50.com";
////    $GJ50AL = "192.168.0.29";
//
//    if (AREA == 'C') {
//        if (Tygh::$app['session']['auth']['user_company_id'] != Tygh::$app['session']['auth']['company_id']) {
//            if (Tygh::$app['session']['auth']['user_company_id'] == 19) {
//                $redirect_url = str_replace($GJ50COM, $GJ50AL, $req_link);
//                header("Location: " . $redirect_url);
//                exit;
//            }
//        }
//    }
//
//	if (empty($country)) {
//		$curl = curl_init();
//		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
//			$ip = $_SERVER['HTTP_CLIENT_IP'];
//		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
//			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
//		} else {
//			$ip = $_SERVER['REMOTE_ADDR'];
//		}
//		// $ip = "79.106.255.255";
//		// $ip = "84.22.57.102";
//		curl_setopt_array($curl, array(
//			CURLOPT_RETURNTRANSFER => 1,
//			CURLOPT_URL => 'http://ip.gjirafa.tech/' . $ip,
//		));
//
//		$resp = json_decode(curl_exec($curl), true);
//		curl_close($curl);
//
//		$country = $resp['CountryCode'];
//		($country == "") ? setcookie("c", 0) : setcookie("c", $country);
//
//		if ($country == "XK" && $req_domain == $GJ50AL) {
//			$redirect_url = str_replace($GJ50AL, $GJ50COM, $req_link);
//			$http_info = fn_url_exists($redirect_url);
//
//			if ($http_info['redirect_url']) {
//				$http_info = fn_url_exists($http_info['redirect_url']);
//			}
//
//			if ($http_info['http_code'] == 200) {
//				header("Location: " . $redirect_url);
//				exit;
//			}
//		} else if ($country == "AL" && $req_domain == $GJ50COM) {
//			$redirect_url = str_replace($GJ50COM, $GJ50AL, $req_link);
//			$http_info = fn_url_exists($redirect_url);
//
//			if ($http_info['redirect_url']) {
//				$http_info = fn_url_exists($http_info['redirect_url']);
//			}
//
//			if ($http_info['http_code'] == 200) {
//				header("Location: " . $redirect_url);
//				exit;
//			}
//		}
//	}
}

/**
 * Log order updates from admin dashboard
 */
function fn_log_order_update($order_id, $old_order = NULL, $new_order = NULL) {
	$user_id = $_SESSION['auth']['user_id'];

	if ($new_order == NULL) {
		$old_order = fn_filter_order_data($old_order);

		$_SESSION['log']['old_order_log'] = base64_encode(serialize($old_order));
	} else if ($old_order == NULL) {
		$new_order = fn_filter_order_data($new_order);

		$data = array(
			'order_id' => $order_id,
			'user_id' => $user_id,
			'data_from' => $_SESSION['log']['old_order_log'],
			'data_to' => base64_encode(serialize($new_order)),
		);

		db_query('INSERT INTO ?:order_edit_logs ?e', $data);

		unset($_SESSION['log']['old_order_log']);
	}
}

function fn_core_pre_update_order(&$cart, $order_id = 0) {
	if (AREA == 'A') {
		fn_log_order_update($order_id, fn_get_order_info($order_id), NULL);
	}
}

function fn_core_update_order(&$cart, $order_id = 0) {
	if (AREA == 'A') {
		fn_log_order_update($order_id, NULL, $cart);
//		fn_get_kosgiro_invoice(fn_get_order_info($order_id), true);
	}
}

/**
 * Log order status update after finished payment
 */
function fn_core_change_order_status($status_to, $status_from, $order_info, $force_notification, $order_statuses, $place_order) {
//	if ($status_from == 'N' && $status_to == 'O') {
//		$user_id = $_SESSION['auth']['user_id'];

//		$data = array(
//			'order_id' => $order_info['order_id'],
//			'user_id' => $user_id,
//			'data' => serialize($order_info),
//		);
//
//		db_query('INSERT INTO ?:order_history ?e', $data);
//	}
}

function smarty_function_get_images_from_blob($params, &$smarty = null) {
	$product_id = $params['product_id'];
	$count = $params['count'];
	$type = ($params['type'] == "large") ? "img" : "thumb";
	if ($count > 1) {
		$img = array();
		for ($i = 0; $i <= $params['count']; $i++) {
			$img[$i] = "https://hhstsyoejx.gjirafa.net/gj50/img/" . $product_id . "/" . $type . "/" . $i . ".jpg";
		}
		$smarty->assign('product_imgs', $img);
		return $img;
	} else if ($count = "1") {
		//$container = (($product_id / 8000 + 1) > 5) ? 5 : ($product_id / 8000 + 1);
		return "https://hhstsyoejx.gjirafa.net/gj50/img/" . $product_id . "/" . $type . "/0.jpg";
	}
}

function smarty_function_get_images_url($params) {
	$product_id = $params['product_id'];
	$container = (($product_id / 8000 + 1) > 5) ? 5 : ($product_id / 8000 + 1);
	return "https://hhstsyoejx.gjirafa.net/gj50/img/" . $product_id . "/thumb/";
}

function fn_get_reviews() {
	$reviews = db_get_array('SELECT a.object_id AS product_id, ROUND(AVG(b.rating_value), 2) AS val
                            FROM gjshop_discussion AS a
                            INNER JOIN gjshop_discussion_rating AS b ON a.thread_id = b.thread_id
                            WHERE a.type = \'B\' AND a.object_type = \'P\' AND b.rating_value > 0
                            GROUP BY product_id');

	return $reviews;
}

function fn_get_product_declaration($product_code) {
	$a = array_reduce(array_map(function ($product) {
		return array(
			'category_id' => $product['0'], //(strpos($product['0'], ' ') !== false) ? explode(",", $product['0']) : $product['0'],
			'feature_ids' => explode(",", $product['1']),
		);
	}, array_map('str_getcsv', file('feeds/deklaracion.csv'))), function ($current, $item) {
		if (strpos($item['category_id'], ',') !== false) {
			$cat_ids = explode(",", $item['category_id']);
			foreach ($cat_ids as $cid) {
				$current[trim($cid)] = [
					'feature_ids' => array_map('trim', $item['feature_ids']),
				];
			}
		} else {
			$current[$item['category_id']] = [
				'feature_ids' => array_map('trim', $item['feature_ids']),
			];
		}

		return $current;
	}, []);

	$categories = fn_get_categories_by_product_code($product_code);

	foreach ($categories as $category) {
		if (isset($a[$category['category_id']])) {

			// Default per krejt
			$a[$category['category_id']]['feature_ids'][] = "12"; // Prodhuesi
			$a[$category['category_id']]['feature_ids'][] = "13"; // Kodi (modeli)

			$c = fn_get_variants_name($category['product_id'], $a[$category['category_id']]['feature_ids']);

			$f = [];
			if (count($c) && is_array($c[12]) && is_array($c[13]) && !is_null($c['product'])) {
				$f = array(
					'manufacturer' => $c[12]['variant'],
					'product' => $c['product'],
					'type' => $c[13]['variant'],
					'model' => $category['category'],
					'specs' => array_slice($c, 2, -1),
				);
				break;
			}
		}
	}

	return $f;
}

function fn_get_categories_by_product_code($product_code) {
	return db_get_array('SELECT product.product_id, a.category_id, c.category, b.level FROM gjshop_products_categories AS a, gjshop_categories AS b, gjshop_category_descriptions AS c, gjshop_products AS product WHERE product.product_code = ?i AND product.product_id = a.product_id AND b.category_id = c.category_id AND c.category_id = a.category_id AND b.company_id = 1 ORDER BY b.level DESC', $product_code);
}

function fn_get_variants_name($product_id, $variants) {

	$q = db_get_array('SELECT c.feature_id, p.product, c.description AS descr, b.variant AS var FROM gjshop_product_features_values AS a, gjshop_product_feature_variant_descriptions AS b, gjshop_product_features_descriptions AS c, gjshop_product_descriptions AS p WHERE a.product_id = ?i AND a.variant_id = b.variant_id AND a.feature_id = c.feature_id AND p.product_id = a.product_id AND a.feature_id IN (?a) ORDER BY field(a.feature_id, "12", "13", ?a);', $product_id, $variants, $variants);

	$variants = array_reduce($q, function ($current, $item) {
		$a = 0;
		$current[$item['feature_id']] = array(
			'description' => $item['descr'],
			'variant' => $item['var'],
		);
		return $current;
	}, []);

	$variants['product'] = (is_array($q)) ? current($q)['product'] : '';

	return $variants;
}

function fn_filter_order_data($order_info) {
	unset($order_info['payment_method']['template']);
	unset($order_info['payment_method']['description']);
	unset($order_info['payment_method']['instructions']);
	unset($order_info['payment_method']['image']);
	unset($order_info['shipping'][0]['description']);
	unset($order_info['shipping'][0]['rate_info']);
	unset($order_info['product_groups']);

	foreach ($order_info['products'] as $id => $product) {
		unset($product['extra']);
		unset($product['full_description']);

		$order_info['products'][$id] = $product;
	}

	return $order_info;
}

function fn_my_changes_print_order_invoice_extra($order_info, &$extra, $pdf = false) {
	if ($order_info['payment_id'] == 28) {
		$extra = fn_get_kosgiro_invoice($order_info, false, $pdf);
	}
}

function fn_my_changes_print_order_invoice_type(&$order_info, &$view, &$view_name, $type, &$pdf_filename) {
	if ($type == 'short') {
        foreach ($order_info['tags'] as $tag) {
            if (in_array($tag['tag'], ORDER_DRIVER_TAGS)) {
                $order_info['display_order_tag'] = $tag['tag'];
            }
        }

		$city_code = '41';
		$is_al = false;

		if (fn_isAL() || $order_info['company_id'] == 19) {
			$city_code = '55';
			$is_al = true;
		}

		$view->assign('city', fn_get_order_city_name($order_info['fields'][$city_code]));
		$view->assign('country', fn_get_order_country_name($order_info['fields']['38']));
        $view->assign('is_al', $is_al);
        $view->assign('package_size', fn_my_changes_get_package_size($order_info['order_id']));

		$view_name = 'orders/print_short_invoice.tpl';
	}

	if ($type == 'kosgiro') {
		$view_name = 'orders/print_kosgiro_invoice.tpl';
		$pdf_filename = 'KosGiro ' . __('invoices');
	}
}

function fn_get_kosgiro_invoice($order_info, $update = false, $pdf = false) {
	$kosgiro_config = Registry::get('config.kosgiro');
	$html = '';
	$kosgiro_url = $kosgiro_config['get_url'];
	$postfix = '';

	if ($pdf) {
		$postfix = '_pdf';
	}

	if (!is_dir($kosgiro_config['image_directory'])) {
		fn_mkdir($kosgiro_config['image_directory']);
		apcu_delete('kosgiro_invoice_' . $order_info['order_id'] . $postfix);
	}

	if ($update) {
		$kosgiro_url = $kosgiro_config['update_url'];

		apcu_delete('kosgiro_invoice_' . $order_info['order_id'] . $postfix);
	}

	if (!apcu_exists('kosgiro_invoice_' . $order_info['order_id'] . $postfix) || empty(apcu_fetch('kosgiro_invoice_' . $order_info['order_id'] . $postfix))) {
		$kosgiro_data = array(
			'RequestBy' => Tygh::$app['session']['auth']['user_id'],
			'OrderId' => $order_info['order_id'],
			'ProvisionId' => 15,
			'ProjectId' => 1,
			'Amount' => $order_info['subtotal'],
			'Account' => '0',
		);
		$kosgiro_headers = array(
			'headers' => array(
				'KGKey: CmtSeYp6efBQ5Kb3T4w*f=W',
				'Content-Type: application/json',
			),
		);

		$response = Http::post($kosgiro_url, json_encode($kosgiro_data), $kosgiro_headers);
		$response = json_decode($response, true);

		if ($response['Success'] == true) {
			$html = fn_parse_html(base64_decode($response['Result']));

			if (!$pdf) {
				fn_parse_kosgiro_invoice_images($html, $kosgiro_config, $order_info['order_id']);
			}

			$html = str_replace('P?r: <b>Gjirafa, Inc. - Dega n? Kosov?</b>', 'Për: <b>Gjirafa, Inc. - Dega në Kosovë</b>', $html); // should be fixed from the endpoint

			apcu_add('kosgiro_invoice_' . $order_info['order_id'] . $postfix, $html, 86400);
		}
	} else {
		$html = apcu_fetch('kosgiro_invoice_' . $order_info['order_id'] . $postfix);
	}

	return $html;
}

function fn_my_changes_checkout_order_complete($order_info) {
	if ($order_info['payment_id'] == 27) {
		$order_tags_data = array(
			'company_id' => Registry::get('runtime.company_id'),
			'tag' => 'IuteCredit',
			'type' => 'O',
			'status' => 'A',
		);

		$order_tag_links_data = array(
			'object_id' => $order_info['order_id'],
			'type' => 'O',
		);

		fn_sd_update_order_tag($order_tags_data);
		fn_sd_update_order_tag_link($order_tag_links_data, $order_tags_data);
	}

	if ($order_info['payment_id'] == 28) {
		fn_get_kosgiro_invoice($order_info);
	}

	if (ENVIRONMENT == 'live') {
	    fn_notify_store_product_sale($order_info['products']);
//	    fn_notify_gjvideo($order_info);
    }

    fn_insert_order_delivery_dates($order_info['order_id'], $order_info['product_groups'][0]['products']);
}

function fn_my_changes_delete_user($user_id, $user_data, $sso) {
	if ($sso) {
		Registry::set('log_cut', true);

		$data = array(
			'firstname' => 'Deleted',
			'lastname' => 'User',
			'phone' => null,
			'email' => null,
			'profile_id' => null,
			'b_firstname' => 'Deleted',
			'b_lastname' => 'User',
			'b_address' => null,
			'b_address_2' => null,
			'b_phone' => null,
			's_firstname' => 'Deleted',
			's_lastname' => 'User',
			's_address' => null,
			's_address_2' => null,
			's_phone' => null,
		);

		db_query('UPDATE ?:orders SET ?u WHERE user_id = ?i', $data, $user_id);
	}
}

function fn_parse_kosgiro_invoice_images(&$html, $kosgiro_config, $order_id) {
	$attributes = fn_parse_html_attributes($html, 'img', 'src');

	$index = 0;
	foreach ($attributes as $attribute) {
		fn_base64_to_image($attribute, $kosgiro_config['image_directory'] . $order_id . '_' . $index++ . '.png');
	}

	$new_attributes = array(
		$kosgiro_config['image_directory'] . $order_id . '_0.png',
		$kosgiro_config['image_directory'] . $order_id . '_1.png',
	);

	$html = str_replace($attributes, $new_attributes, $html);
}

function fn_notify_store_product_sale($products) {
	foreach ($products as $product) {
		if ($product['store_id'] && $product['store_id'] == '15') {
			$url = 'https://frasheri.gjirafa.com:1234/Api/Product/Sync?product=gjirafa50&code=' . $product['product_code'];
			$data = array();

			$res = Http::post($url, $data);
		}
	}
}

function fn_limit_rate($max_requests, $period, $adapter, $identifier) {
	$rateLimit = new RateLimit('rate_limit', $max_requests, $period, $adapter);

	if (!$rateLimit->check($identifier)) {
		http_response_code(429);
		include 'design/themes/responsive/templates/429.tpl';
		exit;
	}
}

if (!function_exists('apache_request_headers')) {
	function apache_request_headers() {
		foreach ($_SERVER as $key => $value) {
			if (substr($key, 0, 5) == "HTTP_") {
				$key = str_replace(" ", "-", ucwords(strtolower(str_replace("_", " ", substr($key, 5)))));
				$out[$key] = $value;
			} else {
				$out[$key] = $value;
			}
		}
		return $out;
	}
}

function fn_my_changes_tools_change_status($params, $result) {
	if ($result && ($params['table'] == 'promotions') && ($params['status'] == 'A' || $params['status'] == 'D' || $params['status'] == 'H')) {
		fn_notify_promotion_status_change($params['id']);
	}
}

function fn_notify_gjvideo($order_info) {
    $url = 'https://vp-api.gjirafa.com/api/VerifiedAward?token=2e5034t2rvfkzsmsrba4';
    $data = [
        'email' => $order_info['email']
    ];

    Http::post($url, $data);
}

function fn_sync_cart_through_sessions() {
    if (Tygh::$app['session']['auth']['user_id']) {
        $this_session_id = Tygh::$app['session']->getID();
        $session_id = db_get_field('SELECT session_id FROM ?:user_session_products WHERE user_id = ?i ORDER BY timestamp DESC LIMIT 1', Tygh::$app['session']['auth']['user_id']);

        if ($this_session_id != $session_id) {
            $cart_products = db_get_array('SELECT * FROM ?:user_session_products WHERE session_id = ?s AND type = ?s', $session_id, 'C');
            $wishlist_products = db_get_array('SELECT * FROM ?:user_session_products WHERE session_id = ?s AND type = ?s', $session_id, 'W');

            if (count($cart_products) != count(Tygh::$app['session']['cart']['products'])) {
                fn_clear_cart(Tygh::$app['session']['cart']);
                fn_save_cart_content(Tygh::$app['session']['cart'], Tygh::$app['session']['auth']['user_id']);

                Tygh::$app['session']['cart']['recalculate'] = true;
                fn_calculate_cart_content(Tygh::$app['session']['cart'], Tygh::$app['session']['auth'], 'A', true, 'F', true);

                if ($cart_products) {
                    foreach ($cart_products as $product) {
                        $product = unserialize($product['extra']);

                        $product_data = [];
                        $product_data[$product['product_id']] = [];
                        $product_data[$product['product_id']]['product_id'] = $product['product_id'];
                        $product_data[$product['product_id']]['amount'] = $product['amount'];

                        fn_add_product_to_cart($product_data, Tygh::$app['session']['cart'], Tygh::$app['session']['auth']);
                    }

                    fn_save_cart_content(Tygh::$app['session']['cart'], Tygh::$app['session']['auth']['user_id']);

                    Tygh::$app['session']['cart']['change_cart_products'] = true;
                    fn_calculate_cart_content(Tygh::$app['session']['cart'], Tygh::$app['session']['auth'], 'S', true, 'F', true);
                }

                if (count($wishlist_products) != count(Tygh::$app['session']['wishlist']['products']) && count(Tygh::$app['session']['wishlist']['products']) > 0) {
                    $wishlist = array();
                    fn_save_cart_content($wishlist, Tygh::$app['session']['auth']['user_id'], 'W');
                    fn_save_cart_content(Tygh::$app['session']['wishlist'], Tygh::$app['session']['auth']['user_id'], 'W');

                    if ($wishlist_products) {
                        foreach ($wishlist_products as $product) {
                            $product = unserialize($product['extra']);

                            $product_data = [];
                            $product_data[$product['product_id']] = [];
                            $product_data[$product['product_id']]['product_id'] = $product['product_id'];
                            $product_data[$product['product_id']]['amount'] = $product['amount'];

                            fn_add_product_to_wishlist($product_data, Tygh::$app['session']['wishlist'], Tygh::$app['session']['auth']);
                        }

                        fn_save_cart_content(Tygh::$app['session']['wishlist'], Tygh::$app['session']['auth']['user_id'], 'W');
                    }
                }
            }
        }
    }
}

function fn_validate_user_auth($controller, $mode) {
    if (
        Tygh::$app['session']['auth']['user_id'] &&
        Tygh::$app['session']['auth']['user_id'] != 0 &&
        !$_REQUEST['as_user'] &&
        !$_COOKIE['as_user'] &&
        ($controller != 'sso' && $mode != 'callback')
    ) {
        fn_log_event('users', 'sso', array(
            'previous_session_user_id' => Tygh::$app['session']['auth']['user_id']
        ));

        if (!Tygh::$app['session']['prev_logged_sso'] && Tygh::$app['session']['prev_logged_sso'] != true) {

            Tygh::$app['session']['prev_logged_sso'] = true;

            $email = db_get_field('SELECT email FROM ?:users WHERE user_id = ?s', Tygh::$app['session']['auth']['user_id']);

            fn_log_event('users', 'sso', array(
                'previous_session_email' => $email
            ));

            // Login user to IS
            $encryptionService = new EncryptionService();
            $authParams = [
                'pvu' => $encryptionService->encryptString('eekvtAsQ4peKX5VW', $email),
                'isPopup' => false
            ];

            GjirafaIS::init();
            GjirafaIS::authenticate($authParams);
        } else {
            GjirafaIS::init();
            GjirafaIS::validateLogin();
        }
    }
}

function fn_my_changes_get_product_data($product_id) {
    $product_categories = db_get_array('SELECT category_id FROM ?:products_categories WHERE product_id = ?i', $product_id);
    $product_categories = fn_array_remove_first_level($product_categories);

    $company_ids = db_get_array('SELECT company_id FROM ?:categories WHERE category_id IN (?a)', $product_categories['category_id']);
    $company_ids = fn_array_remove_first_level($company_ids);

    if (!in_array(1, $company_ids['company_id'])) {
        foreach ($product_categories['category_id'] as $product_category) {
            $category_id_ks = db_get_field('SELECT category_ks FROM ?:category_mappings WHERE category_al = ?s', $product_category);

            if ($category_id_ks != '') {
                $data = [
                    'product_id' => $product_id,
                    'category_id' => $category_id_ks
                ];
                db_query('INSERT INTO ?:products_categories ?e', $data);
            }
        }
    }

    if (!in_array(19, $company_ids['company_id'])) {
        foreach ($product_categories['category_id'] as $product_category) {
            $category_id_al = db_get_field('SELECT category_al FROM ?:category_mappings WHERE category_ks = ?s', $product_category);

            if ($category_id_al != '') {
                $data = [
                    'product_id' => $product_id,
                    'category_id' => $category_id_al
                ];
                db_query('REPLACE INTO ?:products_categories ?e', $data);
            }
        }
    }

    if (in_array(1976, $product_categories['category_id']) && !in_array(989, $product_categories['category_id']) || empty($product_categories)) {
        $data = [
            'product_id' => $product_id,
            'category_id' => 989
        ];
        db_query('INSERT INTO ?:products_categories ?e', $data);

        if (empty($product_categories)) {
            $data = [
                'product_id' => $product_id,
                'category_id' => 1976
            ];
            db_query('INSERT INTO ?:products_categories ?e', $data);
        }
    }
}

function fn_my_changes_pre_get_cart_product_data($hash, $product, $skip_promotion, $cart, $auth, $promotion_amount, $fields, $join) {
    fn_my_changes_get_product_data($product['product_id']);
}

function fn_my_changes_user_logout_after($auth, $redirect_url) {
    unset($_COOKIE['re']);
    setcookie('re', '');

    header('Location: https://sso.gjirafa.com/Account/LogOff?returnUrl=' . $redirect_url);

//    $local_redirect = fn_url('sso.redirect');
//    header("Location: $local_redirect&returnUrl=" . $redirect_url);
    exit();
}

if (!function_exists('array_key_first')) {
    function array_key_first($array) {
        foreach($array as $key => $value) {
            return $key;
        }

        return null;
    }
}

if (!function_exists(bcmul)) {
    function bcmul($_ro, $_lo, $_scale=0) {
        return round($_ro*$_lo, $_scale);
    }
}

if (!function_exists(bcdiv)) {
    function bcdiv($_ro, $_lo, $_scale=0) {
        return round($_ro/$_lo, $_scale);
    }
}

function fn_extract_session_data()
{
    return;
    if (Tygh::$app['session']['auth']['user_id']) {
        $user_id = Tygh::$app['session']['auth']['user_id'];
        $stored_data = db_get_field('SELECT data FROM ?:sessions_data WHERE user_id = ?i', $user_id);

        if ($stored_data != '') {
            $stored_data = unserialize($stored_data);

            if (count($stored_data['cart']) != count(Tygh::$app['session']['cart']['products'])) {
                fn_clear_cart(Tygh::$app['session']['cart']);
                fn_save_cart_content(Tygh::$app['session']['cart'], Tygh::$app['session']['auth']['user_id'], 'C', 'R', false);

                Tygh::$app['session']['cart']['recalculate'] = true;
                fn_calculate_cart_content(Tygh::$app['session']['cart'], Tygh::$app['session']['auth'], 'A', true, 'F', true);

                if ($stored_data['cart']) {
                    foreach ($stored_data['cart'] as $product) {
                        $product_data = [];
                        $product_data[$product['product_id']] = [];
                        $product_data[$product['product_id']]['product_id'] = $product['product_id'];
                        $product_data[$product['product_id']]['amount'] = $product['amount'];

                        fn_add_product_to_cart($product_data, Tygh::$app['session']['cart'], Tygh::$app['session']['auth']);
                    }

                    fn_save_cart_content(Tygh::$app['session']['cart'], Tygh::$app['session']['auth']['user_id'], 'C', 'R', false);

                    Tygh::$app['session']['cart']['change_cart_products'] = true;
                    fn_calculate_cart_content(Tygh::$app['session']['cart'], Tygh::$app['session']['auth'], 'S', true, 'F', true);
                }

                Tygh::$app['session']['cart']['amount'] = count(Tygh::$app['session']['cart']['products']);
            }

            if (count($stored_data['wishlist']) != count(Tygh::$app['session']['wishlist']['products']) && count(Tygh::$app['session']['wishlist']['products']) > 0) {
                $wishlist = array();
                fn_save_cart_content($wishlist, Tygh::$app['session']['auth']['user_id'], 'W', 'R', false);
                fn_save_cart_content(Tygh::$app['session']['wishlist'], Tygh::$app['session']['auth']['user_id'], 'W', 'R', false);

                if ($stored_data['wishlist']) {
                    foreach ($stored_data['wishlist'] as $product) {
                        $product_data = [];
                        $product_data[$product['product_id']] = [];
                        $product_data[$product['product_id']]['product_id'] = $product['product_id'];
                        $product_data[$product['product_id']]['amount'] = $product['amount'];

                        fn_add_product_to_wishlist($product_data, Tygh::$app['session']['wishlist'], Tygh::$app['session']['auth']);
                    }

                    fn_save_cart_content(Tygh::$app['session']['wishlist'], Tygh::$app['session']['auth']['user_id'], 'W', 'R', false);
                }
            }
        }
    }
}

function fn_my_changes_get_package_size($order_id) {
    $body = array(
        'OrderId' => $order_id,
        'PlatformId' => 2
    );
    $client = new Client([
        'base_uri' => 'https://perkthimet.gjirafa.com/'
    ]);
    $headers = [
        'ApiKey' => 'gd45h8FE.3R4oP0@fgpll65lks89whq45[sgQ_gP8]29iZz',
        'Content-Type' => 'application/json'
    ];

    $request = new Request('POST', 'api/OrderPackageSize/GetOrderPackageSizeAbbreviation', $headers, json_encode($body));

    try {
        $response = $client->send($request);
    } catch (ClientException $e) {
//        return $e->getResponse()->getStatusCode();
        return false;
    }

    return $response->getBody()->getContents();
}