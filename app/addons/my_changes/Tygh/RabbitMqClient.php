<?php

namespace Tygh;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class RabbitMqClient {
    public static function send($body, $queue) {
        $connection = new AMQPStreamConnection(
            RABBIT_HOST,
            RABBIT_PORT,
            RABBIT_USERNAME,
            RABBIT_PASSWORD
        );

        $channel = $connection->channel();

        $msg = new AMQPMessage(
            json_encode($body, JSON_UNESCAPED_SLASHES),
            array('delivery_mode' => 2)
        );

        $channel->basic_publish($msg, '', $queue);
    }
}