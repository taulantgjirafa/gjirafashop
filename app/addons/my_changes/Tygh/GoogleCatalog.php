<?php

namespace Tygh;

use FacebookAds\Api;
use FacebookAds\Http\RequestInterface;
use Tygh\ElasticSearchClass;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

class GoogleCatalog {
    public function catalogGetAllProducts($company_id) {
        $all_products_brands = db_get_array('SELECT a.product_id, b.variant FROM ?:product_features_values as a, ?:product_feature_variant_descriptions as b WHERE a.variant_id = b.variant_id AND a.feature_id = 14');

        $brand_index = array();

        foreach ($all_products_brands as $apb_id) {
            $brand_index[$apb_id['product_id']] = $apb_id['variant'];
        }

        // change route to https
        $products = fn_get_external_data('GET', 'https://perkthimet.gjirafa.com/Pricing/GetElasticProducts?apiKey=5CF5316C562BB9368DD857211572C');
        $products_decoded = json_decode($products,true);

        foreach ($products_decoded as $key => $product) {
            $features = db_get_array('SELECT feature_id, variant_id FROM ?:product_features_values WHERE product_id = ?i', $product['product_id']);

            foreach ($features  as $feature) {
                if ($feature['feature_id'] == 12) {
                    $products_decoded[$key]['brand'] = db_get_field('SELECT variant FROM ?:product_feature_variant_descriptions WHERE variant_id = ?i', $feature['variant_id']);
                }
            }
        }

        if ($company_id == 19) {
            foreach ($products_decoded as $key=>$val) {
                $price_al = $products_decoded[$key]['price_al'];
                $products_decoded[$key]['price'] = $price_al;

                if (isset($products_decoded[$key]['old_price_al'])) {
                    $old_price_al = $products_decoded[$key]['old_price_al'];
                    $products_decoded[$key]['old_price'] = $old_price_al;
                }
            }
        }

        return array(
            'products' => $products_decoded,
            'brands' => $brand_index
        );
    }

    public function catalogGetCategoryProducts($category_id, $company_id) {
        if($company_id == 19){
            $category_id = db_get_field('select category_ks from ?:category_mappings where category_al = ?i', $category_id);
        }
        $joins = "  inner join ?:product_descriptions on ?:product_descriptions.product_id = ?:products.product_id inner join ?:product_features_values on ?:product_features_values.product_id = ?:products.product_id inner join ?:product_feature_variant_descriptions on ?:product_features_values.variant_id = ?:product_feature_variant_descriptions.variant_id ";

        $elastic = new ElasticSearchClass();

        $key = true;
        $obj = array(
            'where' =>
                array(
                    array(
                        'field' => 'features.14',
                    ),
                    array(
                        'field' => 'category_ids',
                        'value' => $category_id,
                    ),

                ),
            'offset' => 0,
            'limit' => (isset($_GET['limit'])) ? (int)$_GET['limit'] : 1000,
            'filters' => false,
            'search'=> null,
            'store' => 'ks',
        );
        $products_array = [];
        while($key) {
            $products_elastic = $elastic->sendObject($obj, array(), 'SearchGj', false, true);
            $obj['offset'] = $obj['offset'] + $obj['limit'];
            array_push($products_array,$products_elastic[0]);

            if($products_elastic[0] == false) $key = false;
        }

        foreach ($products_array as $product_array) {
            foreach ($product_array as $key=>$value) {
                $prod_ids [] = $value['product_id'];
            }
        }

        $comma_separated = implode(",", $prod_ids);
        $variant_and_desc = db_get_array('select ?:products.product_id,variant, full_description  from ?:products '.$joins.' where feature_id = 14 and ?:products.product_id IN('.$comma_separated.')');

        foreach ($products_array as $product_array) {
            foreach($product_array as $key => $val) {
                $found_key = array_search($key, array_column($variant_and_desc, 'product_id'));

                $price = ($company_id == 19 ? $val['price_al'] : $val['price']);
                $old_price = ($company_id == 19 ? $val['old_price_al'] : $val['old_price']);
                $list_price = ($company_id == 19 ? $val['list_price_al'] : $val['list_price']);

                $old_price_display = $old_price;

                if ($list_price > $old_price && $list_price > $price) {
                    $old_price_display = $list_price;
                }

                foreach ($val['features'] as $key => $feature) {
                    if ($key == '12') {
                        $val['brand'] = db_get_field('SELECT variant FROM ?:product_feature_variant_descriptions WHERE variant_id = ?i', $feature);
                    }
                }

                $products[] = array(
                    'product_id' => $val['product_id'],
                    'product' => $val['product'],
                    'full_description' =>  $variant_and_desc[$found_key]['full_description'],
                    'price' => $price,
                    'old_price' => $old_price_display,
                    'variant' => $variant_and_desc[$found_key]['variant'],
                    'stock' => $val['amount'] + $val['czc_stock'] + $val['czc_retailer_stock'],
                    'amount' => $val['amount'],
                    'brand' => $val['brand']
                );
            }
        }

        return $products;
    }

    public function parseRequestData($categoryId, $companyId = null) {
        $categoryName = db_get_field('SELECT category FROM ?:category_descriptions WHERE category_id = ?i', $categoryId);
        $decCategoryName = utf8_decode($categoryName);
        $name = preg_replace("/[\s_]/", "-", utf8_encode(strtolower($decCategoryName)));
        $parsedName = str_replace(["ë", "Ç" , "ç"], ["e", "C", "c"], $name);

        if ($companyId == 'KS' || $companyId == null) {
            $companyId = 1;
        } else {
            $companyId = 19;
        }

        if (isset($categoryId)) {
            $this->generateCatalogCsv($categoryId, $parsedName, $companyId);
        } else {
            $this->generateCatalogCsv(0, $parsedName, $companyId);
        }
    }

    public function generateCatalogCsv($category_id, $category_name, $company_id) {
        if ($company_id == 19) {
            $currency = ' ALL';
            $domain = 'gjirafa50.al';
        } else {
            $currency = ' EUR';
            $domain = 'gjirafa50.com';
        }

        $all_products = false;

        if (isset($category_id) && isset($company_id) && $category_id != 0) {
            $products['products'] = $this->catalogGetCategoryProducts($category_id, $company_id);
        } else {
            $all_products = true;
            $category_name = 'catalog';
            $products = $this->catalogGetAllProducts($company_id);
        }

        $company_dir = 'feeds/' . $company_id;
        $category_dir = 'feeds/' . $company_id . '/' . $category_name;

        if (!is_dir($company_dir) && !file_exists($company_dir)) {
            mkdir($company_dir);
        }

        if (!is_dir($category_dir) && !file_exists($category_dir)) {
            mkdir($category_dir);
        }

        // create downloadable file, store the file to server
        header('Content-Type: text/tab-separated-values');
        header('Content-Disposition: attachment; filename="' . $category_name . '.txt";');

        $fp = fopen('php://output', 'w');
        $fp_local = fopen($category_dir . '/catalog.csv', 'w');

        $fields = array(
            'id',
            'title',
            'description',
            'availability',
            'condition',
            'price',
            'link',
            'image_link',
            'brand',
            'additional_image_link',
            // 'google_product_category',
            'product_type',
            'sale_price',
            'shipping',
            'rate',
            'custom_label_0',
            'custom_label_1',
            'custom_label_2',
        );

        $path = '';

        fputcsv($fp, $fields, "\t");
        fputcsv($fp_local, $fields, "\t");

        foreach ($products['products'] as $product) {
            $container = intval($product['product_id'] / 8000 + 1);

            if ($container > 5){
                $container = 5;
            }

            $img_count = fn_get_image_count($product['product_id']);

            $discounted = false;

            if ($product['old_price']) {
                $discounted = true;
            }

            $img_links = '';

            $csv_product['id'] = $product['product_id'];
            $csv_product['title'] = $product['product'];
            $csv_product['description'] = str_replace(["\t", "\r", "\n"], ' ', $product['full_description']);

            if (empty(trim($csv_product['description']))) {
                $csv_product['description'] = 'Përshkrimi është duke u përpunuar, ndërkohë mund të shikoni specifikat teknike, apo kontaktoni në chat, email, messenger për detaje të mëtutjeshme. Ju faleminderit për mirëkuptim.';
            }

            $csv_product['availability'] = 'in stock';
            $csv_product['condition'] = 'new';
            $csv_product['price'] = $discounted ? number_format((float)$product['old_price'], 2, '.', '') . $currency : number_format((float)$product['price'], 2, '.', '') . $currency;
            $csv_product['link'] = 'https://' . $domain . '/index.php?dispatch=products.view%26product_id=' . $product['product_id'];
            $csv_product['image_link'] = 'https://hhstsyoejx.gjirafa.net/gj50/img/' . $product['product_id'] . '/img/0.jpg';
            $csv_product['brand'] = $product['variant'] ? $product['variant'] : 'Gjirafa50';

            if ($img_count > 1) {
                for ($i = 1; $i < $img_count; $i++) {
                    $img_links .= 'https://hhstsyoejx.gjirafa.net/gj50/img/' . $product['product_id'] . '/img/' . $i . '.jpg,';
                }

                $csv_product['additional_image_link'] = rtrim($img_links, ',');
            } else {
                $csv_product['additional_image_link'] = false;
            }

            if ($all_products) {
                $csv_product['product_type'] = str_replace('|', '>', $product['category']);
            } else {
                $csv_product['product_type'] = fn_get_product_categories_path($product['product_id'], $company_id);
            }

            $csv_product['sale_price'] = $discounted ? number_format((float)$product['price'], 2, '.', '') . $currency : false;
            $csv_product['shipping'] = 'DE:::0 EUR';
            $csv_product['rate'] = '0';
            $csv_product['custom_label_0'] = $product['amount'] ? '48h' : false;

            if (isset($product['brand'])) {
                $csv_product['custom_label_1'] = $product['brand'];
            } else {
                $csv_product['custom_label_1'] = false;
            }

            $csv_product['custom_label_2'] = $this->getProductDiscount($product['price'], $product['old_price']);

            array_walk($csv_product, function(&$value, $key) {
                $value = trim($value);
            });

            fputcsv($fp, $csv_product, "\t");
            fputcsv($fp_local, $csv_product, "\t");
        }

        fclose($fp);
        fclose($fp_local);
    }

    public function getProductDiscount($price, $oldPrice) {
        $discountPercentage = '';

        if ($oldPrice && $oldPrice != 0) {
            $discountPercentage = round((1 - $oldPrice / $price) * 100) . '%';
        }

        return $discountPercentage;
    }
}