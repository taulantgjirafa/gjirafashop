<?php

namespace Tygh;

use FacebookAds\Api;
use FacebookAds\Http\RequestInterface;
use Tygh\ElasticSearchClass;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

class FacebookCatalog {

    static $businessId = "240963846323750";
    static $catalogId = "347813059073823";
    static $appId = '451710858928511';
    static $appSecret = 'ae0d4b91bf686a885fca647c67cea946';
    static $token = 'EAAGa1CFJoX8BAHxIoZBT0J2nO3MTGv0fK9nzYrMHUpMwt6x4VJP6m9GsmmUiAKz3wiu6xeYZCCFfbcayY0ZBOxLcrRWJUyNFdU1hPEgnZCuyJVM1o1Chdnb79M6QiXlZArqZCdIrA0Ox7OmXu5i9VK0CVmxQsBBMfLOCfNzHZBGvV1jMWNX5VKo';

    function __construct()
    {
        Api::init(self::$appId, self::$appSecret, self::$token);
    }

    private function getMainImage($product_id) {
        return "https://hhstsyoejx.gjirafa.net/gj50/img/" . $product_id . "/img/0.jpg";
    }

    private function getProductLink($product_id) {
        return "https://gjirafa50.com/index.php?dispatch=products.view%26product_id=" . $product_id;
    }

    private function getProduct($product_id) {
        $product = db_get_array('SELECT a.product_id, a.status, a.amount, b.product, b.short_description, b.full_description, c.price, d.czc_stock, d.czc_retailer_stock, f.category, h.variant FROM ?:products as a, ?:product_descriptions as b, ?:product_prices as c, ?:product_stock as d, ?:products_categories as e, ?:category_descriptions as f, ?:product_features_values as g, ?:product_feature_variant_descriptions as h WHERE a.product_id = b.product_id AND a.product_id = c.product_id AND a.product_id = d.product_id AND a.status = \'A\' AND (d.czc_stock != 0 OR d.czc_retailer_stock != 0) AND a.product_id = e.product_id AND e.category_id = f.category_id AND e.link_type = \'M\'  AND g.feature_id = 11 AND g.product_id = a.product_id AND g.variant_id = h.variant_id AND a.product_id = ?i', $product_id);

        if(strlen($product['full_description']) > 120)
            $product['full_description'] = substr($product['full_description'], 0, 120) ."...";

        $product['brand'] = $this->getProductBrand($product_id);

        return $product;
    }

    private function getProductBrand($product_id) {
        return db_get_array('SELECT a.product_id, b.variant FROM ?:product_features_values as a, ?:product_feature_variant_descriptions as b WHERE a.variant_id = b.variant_id AND a.feature_id = 9 AND a.product_id = ?i', $product_id);
    }


    public function catalogInsertProduct($product_id) {
        $product = $this->getProduct($product_id);

        $obj = array(
            'id' => $product['product_id'],
            'title' => $product['product'],
            'description' => $product['full_description'],
            'link' => $this->getProductLink($product['product_id']),
            'image_link' => $this->getMainImage($product['product_id']),
            'brand' => $this->getProductBrand($product['product_id']),
            'condition' => 'new',
            'availability' => 'in stock',
            'price' => $product['price'],
            'gtin' => $product['variant'],
            'category' => $product['category']
        );

        $this->catalogCallApi($this->catalogObject($product['product_id'], $obj, 'CREATE'));
    }

    public function catalogDeleteProduct($product_id) {

        $this->catalogCallApi($this->catalogObject($product_id, array(), 'DELETE'), RequestInterface::METHOD_DELETE);
    }


    public function catalogUpdateStock($product_data) {
        $obj = array();

        foreach ($product_data as $product) {
            $data = array(
                'availability' => ($product['product_stock']) ? 'in stock' : 'out of stock'
            );
            array_push($obj, $this->catalogObject($product['product_id'], $data, 'UPDATE'));
        }

        return $this->catalogCallApi($obj);
    }


    public function catalogUpdatePricing($product_data) {
        $obj = array();

        foreach ($product_data as $product) {
            $data = array(
                'price' => (int) $product['product_price']
            );
            array_push($obj, $this->catalogObject($product['product_id'], $data, 'UPDATE'));
        }

        return $this->catalogCallApi($obj);
    }

    public function catalogGetAllProducts($company_id) {
        $all_products_brands = db_get_array('SELECT a.product_id, b.variant FROM ?:product_features_values as a, ?:product_feature_variant_descriptions as b WHERE a.variant_id = b.variant_id AND a.feature_id = 14');

        $brand_index = array();

        foreach ($all_products_brands as $apb_id) {
            $brand_index[$apb_id['product_id']] = $apb_id['variant'];
        }

        // change route to https
        $products = fn_get_external_data('GET', 'https://perkthimet.gjirafa.com/Pricing/GetElasticProducts?apiKey=5CF5316C562BB9368DD857211572C');
        $products_decoded = json_decode($products,true);

        foreach ($products_decoded as $key => $product) {
            $features = db_get_array('SELECT feature_id, variant_id FROM ?:product_features_values WHERE product_id = ?i', $product['product_id']);

            foreach ($features  as $feature) {
                if ($feature['feature_id'] == 12) {
                    $products_decoded[$key]['brand'] = db_get_field('SELECT variant FROM ?:product_feature_variant_descriptions WHERE variant_id = ?i', $feature['variant_id']);
                }
            }
        }

        if ($company_id == 19) {
            foreach ($products_decoded as $key=>$val) {
                $price_al = $products_decoded[$key]['price_al'];
                $products_decoded[$key]['price'] = $price_al;

                if (isset($products_decoded[$key]['old_price_al'])) {
                    $old_price_al = $products_decoded[$key]['old_price_al'];
                    $products_decoded[$key]['old_price'] = $old_price_al;
                }
            }
        }

        return array(
            'products' => $products_decoded,
            'brands' => $brand_index
        );
    }

    public function catalogGetCategoryProducts($category_id, $company_id) {
//        if ($company_id == 19) {
//            $category_id = db_get_field('select category_ks from ?:category_mappings where category_al = ?i', $category_id);
//        }
        $joins = "  inner join ?:product_descriptions on ?:product_descriptions.product_id = ?:products.product_id inner join ?:product_features_values on ?:product_features_values.product_id = ?:products.product_id inner join ?:product_feature_variant_descriptions on ?:product_features_values.variant_id = ?:product_feature_variant_descriptions.variant_id ";

        $elastic = new ElasticSearchClass();

        $key = true;
        $obj = array(
            'where' =>
                array(
                    array(
                        'field' => 'features.14',
                    ),
                    array(
                        'field' => 'category_ids',
                        'value' => $category_id,
                    ),

                ),
            'offset' => 0,
            'limit' => (isset($_GET['limit'])) ? (int)$_GET['limit'] : 1000,
            'filters' => false,
            'search'=> null,
            'store' => 'ks',
        );
        $products_array = [];
        while($key) {
            $products_elastic = $elastic->sendObject($obj, array(), 'SearchGj', false, true);
            $obj['offset'] = $obj['offset'] + $obj['limit'];
            array_push($products_array,$products_elastic[0]);

            if($products_elastic[0] == false) $key = false;
        }

        foreach ($products_array as $product_array) {
            foreach ($product_array as $key=>$value) {
                $prod_ids [] = $value['product_id'];
            }
        }

        $comma_separated = implode(",", $prod_ids);
        $variant_and_desc = db_get_array('select ?:products.product_id,variant, full_description  from ?:products '.$joins.' where feature_id = 14 and ?:products.product_id IN('.$comma_separated.')');

        foreach ($products_array as $product_array) {
            foreach($product_array as $key => $val) {
                $found_key = array_search($key, array_column($variant_and_desc, 'product_id'));

                $price = ($company_id == 19 ? $val['price_al'] : $val['price']);
                $old_price = ($company_id == 19 ? $val['old_price_al'] : $val['old_price']);
                $list_price = ($company_id == 19 ? $val['list_price_al'] : $val['list_price']);

                $old_price_display = $old_price;

                if ($list_price > $old_price && $list_price > $price) {
                    $old_price_display = $list_price;
                }

                foreach ($val['features'] as $key => $feature) {
                    if ($key == '12') {
                        $val['brand'] = db_get_field('SELECT variant FROM ?:product_feature_variant_descriptions WHERE variant_id = ?i', $feature);
                    }
                }

                $products[] = array(
                    'product_id' => $val['product_id'],
                    'product' => $val['product'],
                    'full_description' =>  $variant_and_desc[$found_key]['full_description'],
                    'price' => $price,
                    'old_price' => $old_price_display,
                    'variant' => $variant_and_desc[$found_key]['variant'],
                    'stock' => $val['amount'] + $val['czc_stock'] + $val['czc_retailer_stock'],
                    'amount' => $val['amount'],
                    'brand' => $val['brand']
                );
            }
        }

        return $products;
    }

    private function catalogObject($product_id, $product_data, $method) {
        return array(
            'access_token' => self::$token,
            'method' => $method,
            'retailer_id' => $product_id,
            'data' => $product_data
        );
    }


    public function catalogCallApi($product_data, $request_method) {

        try {
            $data = Api::instance()->call(
                '/' . self::$catalogId . '/batch',
                $request_method,
                array('requests' => $product_data))->getContent();
        } catch (Exception $e) {
            $data = false;
        }

        return $data;
    }

    public function getCurrencyExchangeRate($from, $to, $amount = 1) {
        $url  = "http://finance.yahoo.com/d/quotes.csv?s=" . $from . $to . "=X&f=l1";
        $data = file_get_contents($url);
        $exchange_rate = (double) $data;

        if($amount != 1)
            return $amount * $data;

        return $exchange_rate;
        //return round($exchange_rate, 2);
    }

    public function parseRequestData($categoryId, $companyId = null) {
        $categoryName = db_get_field('SELECT category FROM ?:category_descriptions WHERE category_id = ?i', $categoryId);
        $decCategoryName = utf8_decode($categoryName);
        $name = preg_replace("/[\s_]/", "-", utf8_encode(strtolower($decCategoryName)));
        $parsedName = str_replace(["ë", "Ç" , "ç"], ["e", "C", "c"], $name);

        if ($companyId == 'KS' || $companyId == null) {
            $companyId = 1;
        } else {
            $companyId = 19;
        }

        if (isset($categoryId)) {
            $this->generateCatalogCsv($categoryId, $parsedName, $companyId);
        } else {
            $this->generateCatalogCsv(0, $parsedName, $companyId);
        }
    }

    public function generateCatalogCsv($category_id, $category_name, $company_id) {
        if ($company_id == 19) {
            $currency = 'ALL';
            $domain = 'gjirafa50.al';
        } else {
            $currency = 'EUR';
            $domain = 'gjirafa50.com';
        }

        $all_products = false;

        if (isset($category_id) && isset($company_id) && $category_id != 0) {
            $products['products'] = $this->catalogGetCategoryProducts($category_id, $company_id);
        } else {
            $all_products = true;
            $category_name = 'catalog';
            $products = $this->catalogGetAllProducts($company_id);
        }

        $company_dir = 'feeds/' . $company_id;
        $category_dir = 'feeds/' . $company_id . '/' . $category_name;

        if (!is_dir($company_dir) && !file_exists($company_dir)) {
            mkdir($company_dir);
        }

        if (!is_dir($category_dir) && !file_exists($category_dir)) {
            mkdir($category_dir);
        }

        // create downloadable file, store the file to server
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="' . $category_name . '.csv";');

        $fp = fopen('php://output', 'w');
        $fp_local = fopen($category_dir . '/catalog.csv', 'w');

        $fields = array(
            'id',
            'title',
            'description',
            'availability',
            'condition',
            'price',
            'link',
            'image_link',
            'brand',
            'additional_image_link',
            'google_product_category',
//            'fb_product_category',
            'product_type',
            'sale_price',
            'custom_label_0',
            'custom_label_1',
            'custom_label_2',
        );

        $path = '';

        fputcsv($fp, $fields);
        fputcsv($fp_local, $fields);

        foreach ($products['products'] as $product) {
            $out_of_stock = false;

            $stock = db_get_field('SELECT (amount + czc_stock + czc_retailer_stock) FROM gjshop_product_stock AS s
                                    INNER JOIN gjshop_products AS p ON p.product_id = s.product_id
                                    WHERE s.product_id = ?i', $product['product_id']);

            if ($stock == 0) {
                $out_of_stock = true;
            }

            $img_count = fn_get_image_count($product['product_id']);

            $discounted = false;

            if ($product['old_price']) {
                $discounted = true;
            }

            $img_links = '';

            $csv_product['id'] = $product['product_id'];
            $csv_product['title'] = substr($product['product'], 0, 150);
            $csv_product['description'] = $product['full_description'];

            if (empty(trim($csv_product['description']))) {
                $csv_product['description'] = 'Përshkrimi është duke u përpunuar, ndërkohë mund të shikoni specifikat teknike, apo kontaktoni në chat, email, messenger për detaje të mëtutjeshme. Ju faleminderit për mirëkuptim.';
            }

            $csv_product['availability'] = $out_of_stock ? 'out of stock' : 'in stock';
            $csv_product['condition'] = 'new';
            $csv_product['price'] = $discounted ? number_format((float)$product['old_price'], 2, '.', '') . $currency : number_format((float)$product['price'], 2, '.', '') . $currency;
            $csv_product['link'] = 'https://' . $domain . '/index.php?dispatch=products.view%26product_id=' . $product['product_id'];
            $csv_product['image_link'] = 'https://hhstsyoejx.gjirafa.net/gj50/img/' . $product['product_id'] . '/img/0.jpg';
            $csv_product['brand'] = $product['variant'] ? $product['variant'] : 'Gjirafa50';

            if ($img_count > 1) {
                for ($i = 1; $i < $img_count; $i++) {
                    $img_links .= 'https://hhstsyoejx.gjirafa.net/gj50/img/' . $product['product_id'] . '/img/' . $i . '.jpg,';
                }

                $csv_product['additional_image_link'] = rtrim($img_links, ',');
            } else {
                $csv_product['additional_image_link'] = '';
            }

            if ($all_products) {
                $csv_product['google_product_category'] = '';
                $csv_product['product_type'] = str_replace('|', '>', $product['category']);
            } else {
                list($product_type, $google_category) = fn_get_product_categories_path($product['product_id'], $company_id);

                $csv_product['google_product_category'] = $google_category;
                $csv_product['product_type'] = $product_type;
            }

            $csv_product['sale_price'] = $discounted ? number_format((float)$product['price'], 2, '.', '') . $currency : '';
            $csv_product['custom_label_0'] = $product['amount'] ? '48h' : false;

            if (isset($product['brand'])) {
                $csv_product['custom_label_1'] = $product['brand'];
            } else {
                $csv_product['custom_label_1'] = '';
            }

            $csv_product['custom_label_2'] = $this->getProductDiscount($product['price'], $product['old_price']);

            fputcsv($fp, $csv_product);
            fputcsv($fp_local, $csv_product);
        }

        fclose($fp);
        fclose($fp_local);
    }

    public function getProductCategoriesPath($product_id, $company_id) {
        $data = array(
            'object_id' => $product_id,
            'company_id' => $company_id,
            'type' => 'P'
        );

        $path = db_get_field('SELECT path FROM ?:seo_names WHERE ?w', $data);
        $ids = explode('/', $path);
        $names = db_get_fields('SELECT category FROM ?:category_descriptions WHERE category_id IN (?a)', $ids);
        $path_names = implode(' > ', $names);

        return $path_names;
    }

    public function getProductDiscount($price, $oldPrice) {
        $discountPercentage = '';

        if ($oldPrice && $oldPrice != 0) {
            $discountPercentage = round((1 - $oldPrice / $price) * 100) . '%';
        }

        return $discountPercentage;
    }
}