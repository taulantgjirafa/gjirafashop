<?php


namespace Tygh;

use Tygh\EncryptionCollection;
use DateTime;
use DateTimeZone;


class EncryptionService {
    public function encryptMessage() {
        $encryptionCollection = new EncryptionCollection();

        $word = $encryptionCollection->getWord();

        $timezone = 'GMT+2';
        $timestamp = time();
        $date = new DateTime("now", new DateTimeZone($timezone));
        $date->setTimestamp($timestamp);

        $word = $word . '&' . $date->format('Y-m-d H:i:s');

        $encryptedMessage = $this->encryptString('eekvtAsQ4peKX5VW', $word);
        $encryptedMessage = str_replace(['+', '/'], ['gazi', 'izag'], $encryptedMessage);

        return $encryptedMessage;
    }

    public function encryptString($key, $word) {
        $method = 'aes-256-cbc';

        $key = substr(hash('sha256', $key, true), 0, 32);

        $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);

        $encrypted = base64_encode(openssl_encrypt($word, $method, $key, OPENSSL_RAW_DATA, $iv));

        return $encrypted;
    }
}