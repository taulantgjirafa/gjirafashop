<?php


namespace Tygh;


class EncryptionCollection {
    public $encryptionWords = [
        "happen",
        "check",
        "shape",
        "mount",
        "tackle",
        "transmit",
        "spot",
        "hold",
        "warn",
        "shift",
        "suit",
        "concede",
        "interview",
        "calculate",
        "believe",
        "point",
        "supervise",
        "dedicate",
        "part",
        "act",
        "close",
        "wrap",
        "reproduce",
        "spill",
        "inspect",
        "result",
        "term",
        "delay",
        "question",
        "float",
        "swallow",
        "employ",
        "determine",
        "hurt",
        "separate",
        "thrust",
        "guide",
        "try",
        "shout",
        "announce",
        "round",
        "explore",
        "bind",
        "presume",
        "link",
        "educate",
        "transport",
        "underline",
        "honour",
        "operate"
    ];

    public function containsWord($word) {
        return in_array($word, $this->encryptionWords);
    }

    public function getWord() {
        return $this->encryptionWords[array_rand($this->encryptionWords)];
    }
}