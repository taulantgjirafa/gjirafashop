<?php

namespace Tygh\Api\Entities;

use Tygh\Api\AEntity;
use Tygh\Api\Response;

class Promotions extends AEntity {
	public function index($id = '', $params = array()) {
		$promotions = fn_get_promotions_data($id, $params);

		return array(
			'status' => Response::STATUS_OK,
			'data' => $promotions,
		);
	}

	public function create($params) {
		return array(
			'status' => Response::STATUS_CREATED,
			'data' => array(),
		);
	}

	public function update($id, $params = null) {
		$valid_params = true;
		$mode_params = true;

		if (empty($params['mode'])) {
			$data['message'] = __('api_required_field', array(
				'[field]' => '[mode]',
			));
			$valid_params = false;

		} elseif (empty($params['action'])) {
			$data['message'] = __('api_required_field', array(
				'[field]' => '[action]',
			));
			$valid_params = false;
		}

		if ($valid_params) {
			if ($params['mode'] == 'products') {
				if (empty($params['product_id'])) {
					$data['message'] = __('api_required_field', array(
						'[field]' => '[product_id]',
					));
					$mode_params = false;
				}

				if ($mode_params) {
					$promotion_hash = db_get_row('SELECT conditions, conditions_hash FROM ?:promotions WHERE promotion_id = ?i', $id);

					// UPDATE CONDITIONS_HASH
					if (strstr($promotion_hash['conditions_hash'], 'products=')) {
						if ($params['action'] == 'add') {
							// add
							$index = strpos($promotion_hash['conditions_hash'], 'products=') + strlen('products=');
							$promotion_hash['conditions_hash'] = fn_insert_at_position($promotion_hash['conditions_hash'], $params['product_id'] . ',', $index);
						} elseif ($params['action'] == 'delete') {
							// delete
							if (fn_get_string_between($promotion_hash['conditions_hash'], 'products=', '";')) {
								$promotion_hash_products = fn_get_string_between($promotion_hash['conditions_hash'], 'products=', '";');
							} elseif (fn_get_string_between($promotion_hash['conditions_hash'], 'products=', ';')) {
								$promotion_hash_products = fn_get_string_between($promotion_hash['conditions_hash'], 'products=', ';');
							} else {
								$promotion_hash_products = substr($promotion_hash['conditions_hash'], $index);
							}

							if (strstr($promotion_hash_products, $params['product_id'] . ',')) {
								$promotion_hash_products_parsed = str_replace($params['product_id'] . ',', '', $promotion_hash_products);
							} elseif (strstr($promotion_hash_products, ',' . $params['product_id'])) {
								$promotion_hash_products_parsed = str_replace(',' . $params['product_id'], '', $promotion_hash_products);
							}

							$promotion_hash['conditions_hash'] = str_replace($promotion_hash_products, $promotion_hash_products_parsed, $promotion_hash['conditions_hash']);
						}
					} elseif (empty($promotion_hash['conditions_hash'])) {
						if ($params['action'] == 'add') {
							$promotion_hash['conditions_hash'] = 'products=' . $params['product_id'];
						}
					}

					// UPDATE CONDITIONS
					$promotion_hash['conditions'] = unserialize($promotion_hash['conditions']);
					if ($promotion_hash['conditions']['conditions']) {
						// has conditions
						$has_products = false;
						foreach ($promotion_hash['conditions']['conditions'] as $key => $condition) {
							if ($condition['condition'] == 'products') {
								if ($params['action'] == 'add') {
									// add
									$condition['value'] = $params['product_id'] . ',' . $condition['value'];
								} elseif ($params['action'] == 'delete') {
									// delete
									if (strstr($condition['value'], $params['product_id'] . ',')) {
										$condition['value'] = str_replace($params['product_id'] . ',', '', $condition['value']);
									} elseif (strstr($condition['value'], ',' . $params['product_id'])) {
										$condition['value'] = str_replace(',' . $params['product_id'], '', $condition['value']);
									} elseif (strstr($condition['value'], $params['product_id'])) {
										$condition['value'] = str_replace($params['product_id'], '', $condition['value']);
									}
								}

								$promotion_hash['conditions']['conditions'][$key]['value'] = $condition['value'];
								$has_products = true;
							}
						}

						if (!$has_products && $params['action'] == 'add') {
							$promotion_hash['conditions']['conditions'][] = array(
								'operator' => 'in',
								'condition' => 'products',
								'value' => $params['product_id'],
							);
						}
					} else {
						// does not have conditions
						if ($params['action'] == 'add') {
							$promotion_hash['conditions']['conditions'] = array(
								array(
									'operator' => 'in',
									'condition' => 'products',
									'value' => $params['product_id'],
								),
							);
						}
					}

					$query_data = array(
						'conditions' => serialize($promotion_hash['conditions']),
						'conditions_hash' => $promotion_hash['conditions_hash'],
					);

					fn_log_event('promotions', 'update', array(
					    'promotion_id' => $id,
					    'data' => array(
					        'api' => true,
					        'object' => $query_data
                        )
                    ));

					$query_result = db_query('UPDATE ?:promotions SET ?u WHERE promotion_id = ?i', $query_data, $id);

					if ($query_result == 1) {
						$data['message'] = 'Success';
					} else {
						$data['message'] = 'Something went wrong!';
					}
				}
			}
		}

		return array(
			'status' => Response::STATUS_OK,
			'data' => $data,
		);
	}

	public function delete($id) {
		return array(
			'status' => Response::STATUS_NO_CONTENT,
		);
	}

	public function privileges() {
		return array(
			'create' => 'create_promotions',
			'update' => 'edit_promotions',
			'delete' => 'delete_promotions',
			'index' => 'view_promotions',
		);
	}

	public function privilegesCustomer() {
		return array(
			'index' => false,
			'create' => false,
			'update' => false,
			'delete' => false,
		);
	}
}