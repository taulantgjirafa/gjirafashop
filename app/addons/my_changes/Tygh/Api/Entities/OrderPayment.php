<?php

namespace Tygh\Api\Entities;

use Tygh\Api\AEntity;
use Tygh\Api\Response;

class OrderPayment extends AEntity
{
    public function index($id = '', $params = array())
    {
        $data = fn_get_order_short_info($id);
        $payment_statuses = fn_get_statuses('P');

        return array(
            'status' => Response::STATUS_OK,
            'data' => array(
                'status' => $data['payment_status'],
                'description' => $payment_statuses[$data['payment_status']]['description'],
            )
        );
    }

    public function create($params)
    {
        foreach ($params as $order_id) {
            $payment_info = db_get_array('SELECT payment_id, payment_status, status FROM gjshop_orders WHERE order_id = ?i', $order_id);

            $response[] = [
                'order_id' => $order_id,
                'payment_status' => $payment_info[0]['payment_status'],
                'payment_id' => intval($payment_info[0]['payment_id']),
                'status' => $payment_info[0]['status']
            ];
        }

        return array(
            'status' => Response::STATUS_CREATED,
            'data' => $response
        );
    }

    public function update($id = '', $params = null)
    {
//        if (filter_var($params['bulk'], FILTER_VALIDATE_BOOLEAN) == true) {
        if ($id == '') {
            $successUpdateList = [];
            $failedUpdateList = [];

            foreach ($params as $order) {
//                $payment_method = fn_change_order_payment_method($order['order_id'], $order['payment_method']);
                $payment_method = fn_log_order_payment_method_update($order['order_id'], $order['payment_method'], $order['payment_status']);
                $payment_status = fn_change_order_payment_status($order['order_id'], $order['payment_status']);

//                $response[] = [
//                    'order_id' => $order['order_id'],
//                    'payment_status' => filter_var($payment_status, FILTER_VALIDATE_BOOLEAN),
//                    'payment_method' => filter_var($payment_method, FILTER_VALIDATE_BOOLEAN)
//                ];

                if (filter_var($payment_method, FILTER_VALIDATE_BOOLEAN) == true) {
                    $successUpdateList[] = $order['order_id'];
                } else {
                    $failedUpdateList[] = $order['order_id'];
                }
            }

            $response = [
                'status' => true,
                'successUpdateList' => $successUpdateList,
                'failedUpdateList' => $failedUpdateList
            ];
        } else {
            $payment_method = fn_change_order_payment_method($id, $params['payment_method']);
            $payment_status = fn_change_order_payment_status($id, $params['payment_status']);

            $response = [
                'order_id' => $id,
                'payment_status' => filter_var($payment_status, FILTER_VALIDATE_BOOLEAN),
                'payment_method' => filter_var($payment_method, FILTER_VALIDATE_BOOLEAN)
            ];
        }

        return array(
            'status' => Response::STATUS_OK,
            'data' => $response
        );
    }

    public function delete($id)
    {
        return array(
            'status' => Response::STATUS_NO_CONTENT,
        );
    }

    public function privileges()
    {
        return array(
            'create' => 'create_order_payment_status',
            'update' => 'edit_order_payment_status',
            'delete' => 'delete_order_payment_status',
            'index'  => 'view_order_payment_status'
        );
    }

    public function privilegesCustomer()
    {
        return array(
            'index' => false,
            'create' => false,
            'update' => false,
            'delete' => false
        );
    }
}