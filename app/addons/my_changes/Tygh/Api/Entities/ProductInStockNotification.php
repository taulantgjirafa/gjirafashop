<?php

namespace Tygh\Api\Entities;

use Tygh\Api\AEntity;
use Tygh\Api\Response;

class ProductInStockNotification extends AEntity
{
    public function index($id = '', $params = array())
    {
        return array(
            'status' => Response::STATUS_OK,
            'data' => array()
        );
    }

    public function create($params)
    {
        $sent = fn_send_product_notifications($params['product_id'], true);

        return array(
            'status' => Response::STATUS_CREATED,
            'data' => array(
                'sent' => $sent
            )
        );
    }

    public function update($id, $params = null)
    {
        return array(
            'status' => Response::STATUS_OK,
            'data' => array()
        );
    }

    public function delete($id)
    {
        return array(
            'status' => Response::STATUS_NO_CONTENT,
        );
    }

    public function privileges()
    {
        return array(
            'create' => 'create_product_in_stock_notification',
            'update' => 'edit_product_in_stock_notification',
            'delete' => 'delete_product_in_stock_notification',
            'index'  => 'view_product_in_stock_notification'
        );
    }

    public function privilegesCustomer()
    {
        return array(
            'index' => false,
            'create' => false,
            'update' => false,
            'delete' => false
        );
    }
}