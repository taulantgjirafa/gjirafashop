<?php

namespace Tygh\Api\Entities;

use Tygh\Api\AEntity;
use Tygh\Api\Response;
use DateTime;
use DateTimeZone;
use Tygh\Http;

class GiftCardNotification extends AEntity
{
    public function index($id = '', $params = array())
    {
        $sent = false;

        $date = new DateTime('today', new DateTimeZone('Europe/Warsaw'));
        $date = $date->format('Y-m-d');

        $data = array(
            'send_date' => $date,
            'sent' => 'N'
        );

        $gift_certificates = db_get_array('SELECT * FROM ?:gift_certificates_email_queue WHERE ?w', $data);

        $force_notification = array(
            'C' => true,
            'A' => true
        );

        if ($gift_certificates) {
            foreach ($gift_certificates as $gift_certificate) {
                $gift_cert_data = unserialize($gift_certificate['data']);
                $gift_cert_data['send_date'] = '';
                $is_sent = fn_gift_certificate_notification($gift_cert_data, $force_notification);

                if ($is_sent) {
                    $data = array('sent' => 'Y');
                    $gift_cert_id = $gift_cert_data['gift_cert_id'];
                    db_query('UPDATE ?:gift_certificates_email_queue SET ?u WHERE gift_cert_id = ?i', $data, $gift_cert_id);
                }
            }

            $sent = true;
        }

        $response = array(
            'date' => $date,
            'sent' => $sent ? 'true' : 'false'
        );

        // ping Envoyer Heartbeat
        Http::get('http://beats.envoyer.io/heartbeat/v7PayMyw4JgnSJa');

        return array(
            'status' => Response::STATUS_OK,
            'data' => $response
        );
    }

    public function create($params)
    {
        return array(
            'status' => Response::STATUS_CREATED,
            'data' => array()
        );
    }

    public function update($id, $params = null)
    {
        return array(
            'status' => Response::STATUS_OK,
            'data' => array()
        );
    }

    public function delete($id)
    {
        return array(
            'status' => Response::STATUS_NO_CONTENT,
        );
    }

    public function privileges()
    {
        return array(
            'create' => 'create_gift_card_notification',
            'update' => 'edit_gift_card_notification',
            'delete' => 'delete_gift_card_notification',
            'index'  => 'view_gift_card_notification'
        );
    }

    public function privilegesCustomer()
    {
        return array(
            'index' => false,
            'create' => false,
            'update' => false,
            'delete' => false
        );
    }
}