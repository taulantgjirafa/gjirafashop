<?php

namespace Tygh\Api\Entities;

use Tygh\Api\AEntity;
use Tygh\Api\Response;

class Sections extends AEntity {
    public function index($id = '', $params = array()) {
        $params['item_ids'] = db_get_fields('SELECT product_id FROM ?:products WHERE status = "A" ORDER BY product_id DESC LIMIT 48');
        $params['item_ids'] = implode(',', $params['item_ids']);
        $sections = fn_get_sections($params);

        foreach ($sections as $section_key => $section) {
            if ($section_key != 'manual') {
                foreach ($section as $data_key => $data) {
                    if ($data_key != 'extra') {
                        $found_key = array_search($data['product_id'], $section['extra']['products']);

                        if ($found_key != false) {
                            $sections[$section_key]['extra']['products'][$data_key] = $data;
                            unset($sections[$section_key]['extra']['products'][$found_key]);
                        }

                        unset($sections[$section_key][$data_key]);
                    }
                }
            }
        }

        foreach ($sections as $section_key => $section) {
            if ($section_key != 'manual') {
                $sections[$section_key] = $section['extra'];
                unset($sections[$section_key]['extra']);
            }
//            if ($section_key == 'extra') {
//                foreach ($data as $detail_key => $detail) {
//                    $sections[$section_key][$detail_key] = $detail;
//                    unset($sections[$section_key][$data_key]);
//                }
//            }
        }

        return array(
            'status' => Response::STATUS_OK,
            'data' => $sections,
        );
    }

    public function create($params) {
        return array(
            'status' => Response::STATUS_CREATED,
            'data' => array(),
        );
    }

    public function update($id, $params = null) {
        return array(
            'status' => Response::STATUS_OK,
            'data' => '',
        );
    }

    public function delete($id) {
        return array(
            'status' => Response::STATUS_NO_CONTENT,
        );
    }

    public function privileges() {
        return array(
            'create' => 'create_sections',
            'update' => 'edit_sections',
            'delete' => 'delete_sections',
            'index' => 'view_sections',
        );
    }

    public function privilegesCustomer() {
        return array(
            'index' => false,
            'create' => false,
            'update' => false,
            'delete' => false,
        );
    }
}