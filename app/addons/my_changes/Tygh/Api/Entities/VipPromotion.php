<?php

namespace Tygh\Api\Entities;

use Tygh\Api\AEntity;
use Tygh\Api\Response;
use Tygh\FacebookCatalog;

class VipPromotion extends AEntity
{
    public function index($id = '', $params = array())
    {
        return array(
            'status' => Response::STATUS_OK,
            'data' => ''
        );
    }

    public function create($params)
    {
        return array(
            'status' => Response::STATUS_CREATED,
            'data' => array()
        );
    }

    public function update($id, $params = null)
    {
        fn_update_vip_promotion($id, $params['pids']);

        return array(
            'status' => Response::STATUS_OK,
            'data' => array()
        );
    }

    public function delete($id)
    {
        return array(
            'status' => Response::STATUS_NO_CONTENT,
        );
    }

    public function privileges()
    {
        return array(
            'create' => 'create_vip_promotion',
            'update' => 'edit_vip_promotion',
            'delete' => 'delete_vip_promotion',
            'index'  => 'view_vip_promotion'
        );
    }

    public function privilegesCustomer()
    {
        return array(
            'index' => false,
            'create' => false,
            'update' => false,
            'delete' => false
        );
    }
}