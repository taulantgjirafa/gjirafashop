<?php

namespace Tygh\Api\Entities;

use Tygh\Api\AEntity;
use Tygh\Api\Response;
use Tygh\GoogleCatalog;

class GoogleCatalogProducts extends AEntity
{
    public function index($id = '', $params = array())
    {
        $googleCatalog = new GoogleCatalog();
        $googleCatalog->parseRequestData($params['category_id'], $params['company']);

        return array(
            'status' => Response::STATUS_OK,
            'data' => ''
        );
    }

    public function create($params)
    {
        return array(
            'status' => Response::STATUS_CREATED,
            'data' => array()
        );
    }

    public function update($id, $params = null)
    {
        return array(
            'status' => Response::STATUS_OK,
            'data' => array()
        );
    }

    public function delete($id)
    {
        return array(
            'status' => Response::STATUS_NO_CONTENT,
        );
    }

    public function privileges()
    {
        return array(
            'create' => 'create_google_catalog_products',
            'update' => 'edit_google_catalog_products',
            'delete' => 'delete_google_catalog_products',
            'index'  => 'view_google_catalog_products'
        );
    }

    public function privilegesCustomer()
    {
        return array(
            'index' => true,
            'create' => false,
            'update' => false,
            'delete' => false
        );
    }
}