<?php

namespace Tygh\Api\Entities;

use Tygh\Api\AEntity;
use Tygh\Api\Response;

class PaymentNotification extends AEntity
{
    public function index($id = '', $params = array())
    {
        return array(
            'status' => Response::STATUS_OK,
            'data' => array()
        );
    }

    public function create($params)
    {
        $data = array (
            'order_id' => $params['OrderId'],
            'type' => 'X', //fraud checking data
            'data' => serialize($params),
        );
        db_query("REPLACE INTO ?:order_data ?e", $data);

        $params = array_merge($params, array('api' => true));

        if ($params['Paid']) {
            $pp_response['order_status'] = 'O';
            $pp_response['payment_status'] = 'A';
            $pp_response['reason_text'] = __('transaction_approved');
            $pp_response['approval_code'] = $params['ApprovalCodeScr'];

            fn_finish_payment($params['OrderId'], $pp_response);
            fn_order_placement_routines('route', $params['OrderId'], true, true, AREA, 'approved', true, array('api' => true));
        } else {
            $pp_response['order_status'] = 'F';
            $pp_response['payment_status'] = 'C';
            $pp_response['reason_text'] = __('transaction_cancelled');

            fn_finish_payment($params['OrderId'], $pp_response);
            fn_order_placement_routines('route', $params['OrderId'], false, true, AREA, 'declined', true, array('api' => true));
        }

        $payment_statuses = [
            'APPROVED' => 'A',
            'DECLINED' => 'C',
            'CANCELLED' => 'C'
        ];

        fn_change_order_payment_status($params['OrderId'], $payment_statuses[$params['PurchaseStatus']]);

        return array(
            'status' => Response::STATUS_CREATED,
            'data' => array(
                'success' => true
            )
        );
    }

    public function update($id = '', $params = null)
    {
        return array(
            'status' => Response::STATUS_CREATED,
            'data' => array()
        );
    }

    public function delete($id)
    {
        return array(
            'status' => Response::STATUS_NO_CONTENT,
        );
    }

    public function privileges()
    {
        return array(
            'create' => 'create_payment_notification',
            'update' => 'edit_payment_notification',
            'delete' => 'delete_payment_notification',
            'index'  => 'view_payment_notification'
        );
    }

    public function privilegesCustomer()
    {
        return array(
            'index' => false,
            'create' => false,
            'update' => false,
            'delete' => false
        );
    }
}