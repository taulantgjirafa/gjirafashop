<?php

namespace Tygh\Api\Entities;

use Tygh\Api\AEntity;
use Tygh\Api\Response;
use Tygh\Http;
use Tygh\Mailer;

class EmailNotification extends AEntity
{
    public function index($id = '', $params = array())
    {
        Mailer::dispatchQueuedEmails();

        // Ping Envoyer Heartbeat
        if (ENVIRONMENT == 'live') {
            Http::get('http://beats.envoyer.io/heartbeat/5pKXkOLz1G6l1Co');
        } else {
            Http::get('http://beats.envoyer.io/heartbeat/eSVNlblSxwzk8XK');
        }

        return array(
            'status' => Response::STATUS_OK,
            'data' => array(
                'success' => true
            )
        );
    }

    public function create($params)
    {
        return array(
            'status' => Response::STATUS_CREATED,
            'data' => array()
        );
    }

    public function update($id, $params = null)
    {
        return array(
            'status' => Response::STATUS_OK,
            'data' => array()
        );
    }

    public function delete($id)
    {
        return array(
            'status' => Response::STATUS_NO_CONTENT,
        );
    }

    public function privileges()
    {
        return array(
            'create' => 'create_email_notification',
            'update' => 'edit_email_notification',
            'delete' => 'delete_email_notification',
            'index'  => 'view_email_notification'
        );
    }

    public function privilegesCustomer()
    {
        return array(
            'index' => false,
            'create' => false,
            'update' => false,
            'delete' => false
        );
    }
}