<?php

namespace Tygh\Api\Entities;

use Tygh\Api\AEntity;
use Tygh\Api\Response;
use Tygh\FacebookCatalog;

class FacebookCatalogProducts extends AEntity
{
    public function index($id = '', $params = array())
    {
        return array(
            'status' => Response::STATUS_OK,
        );

        $facebookCatalog = new FacebookCatalog();
        $facebookCatalog->parseRequestData($params['category_id'], $params['company']);
    }

    public function create($params)
    {
        return array(
            'status' => Response::STATUS_CREATED,
            'data' => array()
        );
    }

    public function update($id, $params = null)
    {
        return array(
            'status' => Response::STATUS_OK,
            'data' => array()
        );
    }

    public function delete($id)
    {
        return array(
            'status' => Response::STATUS_NO_CONTENT,
        );
    }

    public function privileges()
    {
        return array(
            'create' => 'create_facebook_catalog_products',
            'update' => 'edit_facebook_catalog_products',
            'delete' => 'delete_facebook_catalog_products',
            'index'  => 'view_facebook_catalog_products'
        );
    }

    public function privilegesCustomer()
    {
        return array(
            'index' => true,
            'create' => false,
            'update' => false,
            'delete' => false
        );
    }
}