<?php

namespace Tygh\Api\Entities;

use Tygh\Api\AEntity;
use Tygh\Api\Response;

class DmpProducts extends AEntity
{
    public function index($id = '', $params = array())
    {
        $products = fn_get_dmp_products($params['from'], $params['to']);

        return array(
            'status' => Response::STATUS_OK,
            'data' => $products
        );
    }

    public function create($params)
    {
        return array(
            'status' => Response::STATUS_CREATED,
            'data' => array()
        );
    }

    public function update($id, $params = null)
    {
        return array(
            'status' => Response::STATUS_OK,
            'data' => array()
        );
    }

    public function delete($id)
    {
        return array(
            'status' => Response::STATUS_NO_CONTENT,
        );
    }

    public function privileges()
    {
        return array(
            'create' => 'create_dmp_products',
            'update' => 'edit_dmp_products',
            'delete' => 'delete_dmp_products',
            'index'  => 'view_dmp_products'
        );
    }

    public function privilegesCustomer()
    {
        return array(
            'index' => false,
            'create' => false,
            'update' => false,
            'delete' => false
        );
    }
}