<?php

namespace Tygh\Api\Entities;

use Tygh\Api\AEntity;
use Tygh;
use Tygh\Api\Response;

class OrderPrepayment extends AEntity
{

    public function index($id = '', $params = array())
    {
        // TODO: Implement index() method.
    }

    public function create($params)
    {
        foreach ($params as $prepayment_params) {
            $total = db_get_field('SELECT total FROM ?:orders WHERE order_id = ?i', $prepayment_params['order_id']);

            if (!empty($total)) {
                $payment_total = $prepayment_params['prepaid_value'];
                db_query('UPDATE ?:orders SET payment_total = ?i WHERE order_id = ?i', (double) $total - $payment_total, $prepayment_params['order_id']);
            }
        }

        return array(
            'status' => Response::STATUS_CREATED,
        );
    }

    public function update($id, $params)
    {
        // TODO: Implement update() method.
    }

    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    public function privileges()
    {
        return array(
            'create' => 'create_order_prepayment',
            'update' => 'edit_order_prepayment',
            'delete' => 'delete_order_prepayment',
            'index'  => 'view_order_prepayment'
        );
    }

    public function privilegesCustomer()
    {
        return array(
            'index' => false,
            'create' => false,
            'update' => false,
            'delete' => false
        );
    }
}