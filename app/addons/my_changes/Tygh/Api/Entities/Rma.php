<?php


namespace Tygh\Api\Entities;


use Tygh\Api\AEntity;
use Tygh\Api\Response;

class Rma extends AEntity
{

    /**
     * Handles REST GET request. Must return Api_Response with list of entities
     * or one entity data if id specified
     *
     * @param mixed $id
     * @param array $params
     * @return Response
     */
    public function index($id = '', $params = array())
    {
        // TODO: Implement index() method.
    }

    /**
     * Handles REST POST request. Must create resource and return Api_Response
     * with STATUS_CREATED on success.
     *
     * @param array $params POST data
     * @return Response
     */
    public function create($params)
    {
        // TODO: Implement create() method.
    }

    /**
     * Handles REST PUT request. Must update resource and return Api_Response
     * with STATUS_OK on success.
     *
     * @param int $id
     * @param array $params POST data
     * @return Response
     */
    public function update($id, $params)
    {
        // Change return status
        $params['change_return_status'] = array (
            'order_id' => $id,
            'action' => $params['action'],
            'return_id' => $params['id'],
            'status_from' => $params['status_from'],
            'status_to' => $params['status_to'],
            'recalculate_order' => 'D',
        );

        fn_rma_update_details($params);

        return array(
            'status' => Response::STATUS_OK,
            'data' => '',
        );
    }

    /**
     * Handles REST DELETE request. Must create resource and return Api_Response
     * with STATUS_NO_CONTENT on success.
     *
     * @param int $id
     * @return Response
     */
    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    public function privileges() {
        return array(
            'create' => 'create_return',
            'update' => 'edit_return',
            'delete' => 'delete_return',
            'index' => 'view_return',
        );
    }
}