<?php

namespace Tygh\Api\Entities;

use Tygh\Api\AEntity;
use Tygh\Api\Response;

class Tags extends AEntity {
    public function index($id = '', $params = array()) {


        return array(
            'status' => Response::STATUS_OK,
            'data' => ''
        );
    }

    public function create($params) {
//        $tags = fn_get_order_user_tags(array(
//            'type' => 'O',
//            'object_id' => $params['order_id'],
//            'check_links' => true,
//            'status' => 'A',
//        ));

//        $tag_exists = array_search($params['tag'], array_column($tags[0], 'tag'));

        if ($params['new_tag']) {
            $delete_params = [
                'tag' => $params['tag'],
                'object_id' => $params['order_id'],
                'object_type' => 'O'
            ];
            fn_delete_order_user_tag_links($delete_params);

            $update_params = [
                'tag' => trim($params['new_tag']),
                'object_id' => $params['order_id'],
                'type' => 'O',
            ];
            fn_update_order_user_tag_links($update_params);
        } else {
            $update_params = [
                'tag' => trim($params['tag']),
                'object_id' => $params['order_id'],
                'type' => 'O',
            ];
            fn_update_order_user_tag_links($update_params);
        }

        return array(
            'status' => Response::STATUS_CREATED,
            'data' => array(),
        );
    }

    public function update($id, $params = null) {
        return array(
            'status' => Response::STATUS_OK,
            'data' => array(),
        );
    }

    public function delete($id) {
        return array(
            'status' => Response::STATUS_NO_CONTENT,
        );
    }

    public function privileges() {
        return array(
            'create' => 'create_tag',
            'update' => 'edit_tag',
            'delete' => 'delete_tag',
            'index' => 'view_tag',
        );
    }
}