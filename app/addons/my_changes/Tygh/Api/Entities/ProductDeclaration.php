<?php

namespace Tygh\Api\Entities;

use Tygh\Api\AEntity;
use Tygh\Api\Response;

class ProductDeclaration extends AEntity
{
    public function index($id = '', $params = array())
    {
        return array(
            'status' => Response::STATUS_OK,
            'data' => array()
        );
    }

    public function create($params)
    {
        $productDeclaration = fn_get_product_declaration($params['product_code']);

        return array(
            'status' => Response::STATUS_CREATED,
            'data' => $productDeclaration
        );
    }

    public function update($id, $params = null)
    {
        return array(
            'status' => Response::STATUS_OK,
            'data' => array()
        );
    }

    public function delete($id)
    {
        return array(
            'status' => Response::STATUS_NO_CONTENT,
        );
    }

    public function privileges()
    {
        return array(
            'create' => 'create_product_declaration',
            'update' => 'edit_product_declaration',
            'delete' => 'delete_product_declaration',
            'index'  => 'view_product_declaration'
        );
    }

    public function privilegesCustomer()
    {
        return array(
            'index' => false,
            'create' => false,
            'update' => false,
            'delete' => false
        );
    }
}