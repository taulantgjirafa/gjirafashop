<?php


namespace Tygh\Api\Entities;


use Tygh\Api\AEntity;
use Tygh\Api\Response;

class Apcu extends AEntity
{
    public function index($id = '', $params = array())
    {
        if ($params['method'] == 'delete') {
            fn_clear_apcu_key($params['key']);
        }

        return array(
            'status' => Response::STATUS_OK,
            'data' => 'success'
        );
    }

    public function create($params)
    {
        // TODO: Implement create() method.
    }

    public function update($id, $params)
    {
        // TODO: Implement update() method.
    }

    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    public function privileges()
    {
        return array(
            'create' => 'create_apcu',
            'update' => 'edit_apcu',
            'delete' => 'delete_apcu',
            'index'  => 'view_apcu'
        );
    }

    public function privilegesCustomer()
    {
        return array(
            'index' => true,
            'create' => false,
            'update' => false,
            'delete' => false
        );
    }
}