<?php

namespace Tygh\Api\Entities;

use Tygh\Api\AEntity;
use Tygh\Api\Response;
use Tygh\Api\Traits\ApiHelper;

class UserData extends AEntity {

    use ApiHelper;

    public function index($id = '', $params = array()) {
        $user_id = db_get_field('SELECT user_id FROM ?:user_profiles WHERE b_phone LIKE ?s OR s_phone LIKE ?s', $params['number'], $params['number']);

        if ($user_id) {
            $user_info = fn_get_user_info($user_id);

            $cities = $this->getStoreCities();

            $data = array(
                'Emri' => $user_info['firstname'],
                'Mbiemri' => $user_info['lastname'],
                'Email' => $user_info['email'],
                'Adresa' => $user_info['b_address'] . ' ' . $user_info['b_address_2'],
                'Shteti' => $user_info['fields'][37] == 2 ? 'Kosovë' : 'Shqipëri',
                'Qyteti' => $cities[$user_info['fields'][40]]
            );
        }

        return array(
            'status' => Response::STATUS_OK,
            'data' => $data
        );
    }

    public function create($params) {
        return array(
            'status' => Response::STATUS_CREATED,
            'data' => array(),
        );
    }

    public function update($id, $params = null) {
                return array(
            'status' => Response::STATUS_OK,
            'data' => array(),
        );
    }

    public function delete($id) {
        return array(
            'status' => Response::STATUS_NO_CONTENT,
        );
    }

    public function privileges() {
        return array(
            'create' => 'create_user_data',
            'update' => 'edit_user_data',
            'delete' => 'delete_user_data',
            'index' => 'view_user_data',
        );
    }
}