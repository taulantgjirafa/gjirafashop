<?php

namespace Tygh\Api\Entities;

use Tygh\Api\AEntity;
use Tygh\Api\Response;

class Banners extends AEntity
{
    public function index($id = '', $params = array())
    {
        $banners = fn_get_banners($params);

        return array(
            'status' => Response::STATUS_OK,
            'data' => $banners[0]
        );
    }

    public function create($params)
    {
        return array(
            'status' => Response::STATUS_CREATED,
            'data' => array()
        );
    }

    public function update($id, $params = null)
    {
        return array(
            'status' => Response::STATUS_OK,
            'data' => array()
        );
    }

    public function delete($id)
    {
        return array(
            'status' => Response::STATUS_NO_CONTENT,
        );
    }

    public function privileges()
    {
        return array(
            'create' => 'create_banners',
            'update' => 'edit_banners',
            'delete' => 'delete_banners',
            'index'  => 'view_banners'
        );
    }

    public function privilegesCustomer()
    {
        return array(
            'index' => true,
            'create' => false,
            'update' => false,
            'delete' => false
        );
    }
}