<?php

namespace Tygh\Api\Entities;

use Tygh\Api\AEntity;
use Tygh\Api\Response;

class OrderStatus extends AEntity
{
    public function index($id = '', $params = array())
    {
        $data = fn_get_order_short_info($id);
        $order_statuses = fn_get_statuses();
        $payment_statuses = fn_get_statuses('P');

        $has_greenlist_tag = false;
        $has_whitelist_tag = false;
        $tags = db_get_array('SELECT tag_id FROM ?:sd_search_tag_links WHERE object_id = ?i AND type = ?s', $data['user_id'], 'U');
        $greenlist_tag_key = array_search('265', array_column($tags, 'tag_id'));
        $whitelist_tag_key = array_intersect(['368','373'], array_column($tags, 'tag_id'));

        if ($greenlist_tag_key !== false) {
            $has_greenlist_tag = true;
        }

        if (!empty($whitelist_tag_key)) {
            $has_whitelist_tag = true;
        }

        return array(
            'status' => Response::STATUS_OK,
            'data' => array(
                'status' => $data['status'],
                'description' => $order_statuses[$data['status']]['description'],
                'payment_status' => $data['payment_status'],
                'payment_description' => $payment_statuses[$data['payment_status']]['description'],
                'payment_id' => $data['payment_id'],
                'user_is_greenlisted' => $has_greenlist_tag,
                'user_is_whitelisted' => $has_whitelist_tag
            )
        );
    }

    public function create($params)
    {
        return array(
            'status' => Response::STATUS_CREATED,
            'data' => array()
        );
    }

    public function update($id = '', $params = null)
    {
//        if (filter_var($params['bulk'], FILTER_VALIDATE_BOOLEAN) == true) {
        if ($id == '') {
            $successUpdateList = [];
            $failedUpdateList = [];

            foreach ($params['order_ids'] as $order_id) {
                if ($params['notifications']) {
                    $notifications = $params['notifications'][0];
                } else {
                    $notifications = [
                        'C' => true,
                        'A' => true,
                        'S' => false
                    ];
                }

                $oldOrderStatusBulk = db_get_field('SELECT status FROM ?:orders WHERE order_id = ?i', $order_id);
                if (in_array($oldOrderStatusBulk, ['B', 'S']) && $params['status'] == 'C') {
                    $status = false;
                } else {
                    $status = fn_change_order_status($order_id, $params['status'], '', $notifications, false, true, true);
                }

//                $response[] = [
//                    'order_id' => $order_id,
//                    'status' => $status
//                ];

                if ($status) {
                    $successUpdateList[] = $order_id;
                } else {
                    $failedUpdateList[] = $order_id;
                }
            }

            $response = [
                'status' => true,
                'successUpdateList' => $successUpdateList,
                'failedUpdateList' => $failedUpdateList
            ];
        } else {
            $oldOrderStatusSingle = db_get_field('SELECT status FROM ?:orders WHERE order_id = ?i', $id);
            if (in_array($oldOrderStatusSingle, ['B', 'S']) && $params['status'] == 'C') {
                $status = false;
            } else {
                $status = fn_change_order_status($id, $params['status'], '', $params['notifications'], false, false, true);
            }

            $response = [
                'order_id' => $id,
                'status' => $status
            ];
        }

        return array(
            'status' => Response::STATUS_OK,
            'data' => $response
        );
    }

    public function delete($id)
    {
        return array(
            'status' => Response::STATUS_NO_CONTENT,
        );
    }

    public function privileges()
    {
        return array(
            'create' => 'create_order_status',
            'update' => 'edit_order_status',
            'delete' => 'delete_order_status',
            'index'  => 'view_order_status'
        );
    }

    public function privilegesCustomer()
    {
        return array(
            'index' => false,
            'create' => false,
            'update' => false,
            'delete' => false
        );
    }
}