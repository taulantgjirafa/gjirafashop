<?php

namespace Tygh;

use Tygh\Http;
use Tygh\Api\Traits\ApiHelper;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

class GjirafaSso {

    use ApiHelper;

    private $token;
    private $registerHeaders;
    private static $getTokenUrl = 'https://stgsso.gjirafa.com/token';
    private static $registerUserUrl = 'https://stgsso.gjirafa.com/api/LlogariaApi';
    private static $getTokenBody = 'grant_type=client_credentials&client_id=123456&client_secret=device_id';
    private static $getTokenHeaders = array(
        'headers' => array(
            'Content-Type: application/x-www-form-urlencoded'
        )
    );

    function __construct() {
        $response = Http::post(self::$getTokenUrl, self::$getTokenBody, self::$getTokenHeaders);
        $response = json_decode($response);
        $this->token = $response->access_token;
        $this->registerHeaders = array(
            'headers' => array(
                'Authorization: Bearer ' . $this->token,
                'Accept: application/json',
                'Content-Type: application/json'
            )
        );
    }

    public function register($userData) {
        $response = Http::post(self::$registerUserUrl, json_encode($userData), $this->registerHeaders);

        return json_decode($response)->Success;
    }

    public function parseUserData($userData) {
        $cities = $this->getStoreCities();

        $birthdayString = '01/01/1950';
        $cityIndex = 40;

        if ($userData['birthday'] != '') {
            $birthday = explode('-', $userData['birthday']);
            $birthdayString = $birthday[1] . '/' . $birthday[2] . '/' . $birthday[0];
        }

        if ($userData['fields'][37] != '2') {
            $cityIndex = 54;
        }

        return array(
            'Email' => $userData['email'],
            'Password' => $userData['password1'],
            'ConfirmPassword' => $userData['password2'],
            'Name' => $userData['b_firstname'],
            'Lastname' => $userData['b_lastname'],
            'Gjinia' => $userData['gender'] == 'm' ? 'Mashkull' : 'Femer',
            'Ditelindja' => $birthdayString,
            'Shteti' => $userData['fields'][37] == '2' ? 'Kosove' : 'Shqiperi',
            'Qyteti' => $cities[$userData['fields'][$cityIndex]] != null ? $cities[$userData['fields'][$cityIndex]] : ($cityIndex == 40 ? 'Prishtina' : 'Tirane'),
            'Terms' => 1
        );
    }

    public function parseCallRequestUserData($userData) {
        $cities = $this->getStoreCities();

        $birthdayString = '01/01/1950';
        $cityIndex = 40;

        if ($userData['birthday'] != '') {
            $birthday = explode('-', $userData['birthday']);
            $birthdayString = $birthday[1] . '/' . $birthday[2] . '/' . $birthday[0];
        }

        if ($userData['fields'][37] != '2') {
            $cityIndex = 54;
        }

        return array(
            'Email' => $userData['email'],
            'Password' => 123456,
            'ConfirmPassword' => 123456,
            'Name' => $userData['name'],
            'Lastname' => $userData['lastname'],
            'Gjinia' => 'Mashkull',
            'Ditelindja' => $birthdayString,
            'Shteti' => $userData['fields'][37] == '2' ? 'Kosove' : 'Shqiperi',
            'Qyteti' => $cities[$userData['fields'][$cityIndex]] != null ? $cities[$userData['fields'][$cityIndex]] : ($cityIndex == 40 ? 'Prishtina' : 'Tirane'),
            'Terms' => 1
        );
    }

    public function parseRegisterUserData($userData) {
        return array(
            'status' => 'A',
            'user_type' => 'C',
            'email' => $userData['email'],
            'password1' => 123456,
            'password2' => 123456,
            'tax_exempt' => 'N',
            'lang_code' => 'al',
            'company_id' => 1,
            'fields' => $userData['fields'],
            'b_firstname' => $userData['name'],
            'b_lastname' => $userData['lastname'],
            'b_phone' => $userData['phone'],
            'b_address' => $userData['address']
        );
    }
}