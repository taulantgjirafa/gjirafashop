<?php

namespace Tygh;

use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
use GuzzleHttp\Psr7\Stream;
use GuzzleHttp\Psr7\CachingStream;

class AmazonS3Client
{
    static private $client = null;

    public function __construct() {
        self::$client = new S3Client([
            'region' => '',
            'version' => '2006-03-01',
            'endpoint' => AWS_ENDPOINT,
            'credentials' => [
                'key' => AWS_KEY,
                'secret' => AWS_SECRET_KEY
            ],
            'use_path_style_endpoint' => true
        ]);
    }

    public function createObject($bucket, $name, $file) {
        try {
            $result = self::$client->putObject([
                'Bucket' => $bucket,
                'Key' => $name,
                'Body' => new CachingStream(
                    new Stream(fopen($file, 'r'))
                )
            ]);

            self::$client->putObjectAcl([
                'Bucket' => $bucket,
                'Key' => $name,
                'ACL' => 'public-read'
            ]);
        } catch (S3Exception $e) {
//            echo "There was an error uploading the file.\n";
        }

        return $result;
    }

    public function deleteObject($bucket, $name) {
        self::$client->deleteObject(['Bucket' => $bucket, 'Key' => $name]);
    }

    public function listObjects($bucket) {
        $objectsListResponse = self::$client->listObjects(['Bucket' => $bucket]);
        $objects = $objectsListResponse['Contents'] ? $objectsListResponse['Contents'] : [];

        foreach ($objects as $object) {
            var_dump($object['Key'] . "\t" . $object['Size'] . "\t" . $object['LastModified'] . "\n");
        }
    }

    public function getObject($bucket, $name) {
        return self::$client->getObject([
            'Bucket'    => $bucket,
            'Key'       => $name
        ]);
    }
}