<?php


namespace Tygh;

use Jumbojett\OpenIDConnectClientException;
use Tygh;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use Jumbojett\OpenIDConnectClient;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

class GjirafaIS {
    static $ssoConfig;

    public static function init() {
        self::$ssoConfig = Registry::get('config.sso');
    }

    public static function authenticate($authParams = []) {
        $oidc = new OpenIDConnectClient(self::$ssoConfig['provider_url'], self::$ssoConfig['client_id'], self::$ssoConfig['client_secret']);
        $oidc->setRedirectURL(fn_url(self::$ssoConfig['redirect_url']));
        $oidc->setResponseTypes(array('code'));
        $oidc->addScope(array('email'));

        foreach ($authParams as $key => $param) {
            $oidc->addAuthParam(array($key => $param));
        }

        $oidc->authenticate();
    }

    public static function processCallback() {
        $_REQUEST['code'] = (new self)->fetchAuthorizationCode();

        $oidc = new OpenIDConnectClient(self::$ssoConfig['provider_url'], self::$ssoConfig['client_id'], self::$ssoConfig['client_secret']);

        try {
            $oidc->authenticate();
            $sid = $oidc->getVerifiedClaims('sid');
            $email = $oidc->getVerifiedClaims('email');
            $idToken = $oidc->getIdToken();

            (new self)->authenticateUser($sid, $email, $idToken);
        } catch (OpenIDConnectClientException $e) {
            fn_redirect(Tygh::$app['session']['sso_return_url']);
        }
    }

    public static function validateLogin() {
        $encryptionService = new EncryptionService();
        $encryptedMessage = $encryptionService->encryptMessage();
        $encryptedMessage = urlencode($encryptedMessage);

        $url = self::$ssoConfig['provider_url'] . '/api/Account/Session/' . Tygh::$app['session']['auth']['openid_connect_sid'] . '/' . $encryptedMessage;

        try {
            $client = new Client();
            $res = $client->request('GET', $url, ['timeout' => 10]);
            $status = $res->getStatusCode();

            fn_log_event('users', 'sso', array(
                'validate_login_response_code' => $status
            ));
        } catch (ClientException $e) {
            $status = 401;
        } catch (RequestException $e) {
            $status = 401;
        }

        if ($status != 200) {
            fn_user_logout(Tygh::$app['session']['auth']);
        }
    }

    public static function logout($redirectUrl) {
        fn_user_logout(Tygh::$app['session']['auth']);

        $oidc = new OpenIDConnectClient(self::$ssoConfig['provider_url'], self::$ssoConfig['client_id'], self::$ssoConfig['client_secret']);
        $oidc->signOut(Tygh::$app['session']['auth']['openid_connect_id_token'], $redirectUrl);
    }

    public function fetchAuthorizationCode() {
        $url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $params = parse_url($url);
        parse_str($params['query'], $query);

        return $query['code'];
    }

    public function authenticateUser($sid, $email, $idToken) {
        $userId = fn_is_user_exists(0, array(
            'email' => $email
        ));

        if ($userId) {
            fn_login_user($userId, true);

            Tygh::$app['session']['auth']['openid_connect_sid'] = $sid;
            Tygh::$app['session']['auth']['openid_connect_id_token'] = $idToken;

            fn_redirect(Tygh::$app['session']['sso_return_url']);
        } else {
            $registerParams = [
                'email' => $email,
                'user_type' => 'C',
                'company_id' => Registry::get('runtime.company_id')
            ];

            list($registeredUserId, $registeredProfileId) = fn_update_user(0, $registerParams, $auth, false, false);

            if ($registeredUserId) {
                fn_login_user($registeredUserId, true);

                Tygh::$app['session']['auth']['openid_connect_sid'] = $sid;
                Tygh::$app['session']['auth']['openid_connect_id_token'] = $idToken;

                fn_redirect(Tygh::$app['session']['sso_return_url']);
            }
        }
    }
}