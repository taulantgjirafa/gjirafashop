<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Enum\SearchTagTypes;
use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

$schema['export_fields']['Tags'] = array(
    'linked' => false,
    'process_get' => array('fn_get_order_user_tags_list', '#key', SearchTagTypes::USER),
    'process_put' => array('fn_put_order_user_tags_list', '#this', '#key', SearchTagTypes::USER)
);

return $schema;