<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Navigation\LastView;
use Tygh\Enum\SearchTagTypes;
use Tygh\Enum\SearchTagStatuses;
use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

function fn_order_users_tags_update_tag_statuses($tag_ids, $status)
{
    if ($tag_ids && $status) {
        db_query("UPDATE ?:sd_search_tags SET status = ?s WHERE tag_id IN (?n)", $status, $tag_ids);
    }
}

function fn_update_order_user_tags($tag_id, $tag_data)
{
    if (!empty($tag_data)) {
        $tag_data['tag'] = strip_tags($tag_data['tag']);
        if ($tag_id) {
            db_query('UPDATE ?:sd_search_tags SET ?u WHERE tag_id = ?i', $tag_data, $tag_id);
        } else {
            $exist = db_get_field('SELECT tag_id FROM ?:sd_search_tags WHERE tag = ?s AND company_id = ?i AND type = ?s', trim($tag_data['tag']), $tag_data['company_id'], $tag_data['type']);
            if ($exist) {
                fn_set_notification('E', __('error'), __('sd_order_user_tags.tag_is_exist'));
            } else {
                $tag_data['timestamp'] = TIME;
                if($_SESSION['auth']['user_id'] == 1){
                    $tag_id = db_query("INSERT INTO ?:sd_search_tags ?e", $tag_data);
                }
            }
        }
    }

    return $tag_id;
}

function fn_get_order_user_tags($params = array(), $items_per_page = 0)
{

    $runtime = Registry::get('runtime');
    $params = LastView::instance()->update('tags', $params);
    $default_params = array (
        'page' => 1,
        'items_per_page' => $items_per_page
    );
    $joins = $conditions = array();
    $params = array_merge($default_params, $params);
    if (fn_allowed_for('ULTIMATE')) {
        $conditions[] = fn_get_company_condition('tags.company_id');
    }
    if (!empty($params['company_id']) && $runtime['controller'] == 'exim' && $runtime['mode'] == 'import') {
        $conditions[] = db_quote(' AND tags.company_id = ?i', $params['company_id']);
    }
    if (isset($params['ctag']) && fn_string_not_empty($params['ctag'])) {
        $conditions[] = db_quote('AND tags.tag LIKE ?l', '%'.trim($params['ctag']).'%');
    }
    if (isset($params['tag']) && fn_string_not_empty($params['tag'])) {
        $conditions[] = db_quote('AND tags.tag = ?s', trim($params['tag']));
    }
    if (isset($params['type']) && fn_string_not_empty($params['type'])) {
        $conditions[] = db_quote('AND tags.type = ?s', trim($params['type']));
    }
    if (isset($params['status']) && fn_string_not_empty($params['status'])) {
        $conditions[] = db_quote('AND tags.status = ?s', trim($params['status']));
    }
    if (isset($params['object_id']) && isset($params['type']) && isset($params['check_links'])) {
        $joins[] = db_quote('LEFT JOIN ?:sd_search_tag_links AS tag_links ON tags.tag_id = tag_links.tag_id');
        $conditions[] = db_quote('AND tag_links.object_id = ?i AND tag_links.type = ?s', $params['object_id'], $params['type']);
    }
    $limit = '';
    if (!empty($params['items_per_page'])) {
        $params['total_items'] = db_get_field('SELECT COUNT(*) FROM ?:sd_search_tags AS tags ?p WHERE 1 ?p', implode(' ', $joins), implode(' ', $conditions));
        $limit = db_paginate($params['page'], $params['items_per_page'], $params['total_items']);
    }
    $tags = db_get_array('SELECT tags.* FROM ?:sd_search_tags AS tags ?p WHERE 1 ?p ORDER BY tags.tag_id ?p', implode(' ', $joins), implode(' ', $conditions), $limit);

    return array($tags, $params);
}

function fn_get_order_user_tag_data($tag_id)
{
    $tag = !empty($tag_id) ? db_get_field('SELECT * FROM ?:sd_search_tags WHERE tag_id = ?i', $tag_id) : false;

    return $tag;
}

function fn_delete_order_user_tags($tag_id)
{
    $result = !empty($tag_id) ? db_query('DELETE FROM ?:sd_search_tags WHERE tag_id = ?i', $tag_id) : false;
    if ($result) {
        db_query('DELETE FROM ?:sd_search_tag_links WHERE tag_id = ?i', $tag_id);
    }

    return $result;
}

function fn_get_order_user_tag_names($params = array())
{
    $conditions = fn_order_users_tags_build_conditions($params);

    return db_get_fields('SELECT DISTINCT tag FROM ?:sd_search_tags WHERE 1 ?p', $conditions);
}

function fn_order_users_tags_build_conditions($params)
{
    $conditions =array();

    if (!empty($params['company_id'])) {
        $conditions[] = db_quote(' AND ?:sd_search_tags.company_id = ?i', $params['company_id']);
    } elseif (fn_allowed_for('ULTIMATE')) {
        $conditions[] = fn_get_company_condition('?:sd_search_tags.company_id');
    }

    if (!empty($params['object_type'])) {
        $conditions[] = db_quote(' AND ?:sd_search_tags.type = ?s', $params['object_type']);
    }

    if (isset($params['tag']) && fn_string_not_empty($params['tag'])) {
        $conditions[] = db_quote(' AND ?:sd_search_tags.tag LIKE ?l', trim($params['tag']).'%');
    }

    if (isset($params['status']) && fn_string_not_empty($params['status'])) {
        $conditions[] = db_quote('AND ?:sd_search_tags.status = ?s', trim($params['status']));
    }

    return implode(' ', $conditions);
}

function fn_update_order_user_tag_links($params)
{
    if (!empty($params)) {
        list($tags) = fn_get_order_user_tags($params);
        if (empty($tags)) {
            $tag_id = fn_update_order_user_tags(0, $params);
        } else {
            $tag = array_shift($tags);
            $tag_id = $tag['tag_id'];
        }
        if ($tag_id) {
            db_query('REPLACE INTO ?:sd_search_tag_links ?e', array(
                'tag_id' => $tag_id,
                'object_id' => $params['object_id'],
                'type' => $params['type']
            ));
        }
    }
}

function fn_delete_order_user_tag_links($params)
{
    if (!empty($params)) {
        list($tags) = fn_get_order_user_tags($params);
        if (!empty($tags)) {
            foreach ($tags as $tag) {
                if ($tag['tag'] == $params['tag']) {
                    if ($tag['tag_id']) {
                        db_query('DELETE FROM ?:sd_search_tag_links WHERE tag_id = ?i AND object_id = ?i AND type = ?s', $tag['tag_id'], $params['object_id'], $params['object_type']);
                    }
                }
            }
        }
    }
}

function fn_get_order_user_tags_list($id, $type)
{
    $tag_list = array();
    if ($id && $type) {
        list($tags) = fn_get_order_user_tags(array(
            'type' => $type, 
            'object_id' => $id,
            'check_links' => true,
            'status' => SearchTagStatuses::ACTIVE,
        ));
        foreach ($tags as $tag) {
            $tag_list[] = $tag['tag'];
        }
    }

    return implode(', ', $tag_list);
}

function fn_put_order_user_tags_list($tags, $id, $type)
{
    if ($id && $type) {
        db_query('DELETE FROM ?:sd_search_tag_links WHERE object_id = ?i AND type = ?s', $id, $type);
        if ($tags) {
            $tags = fn_explode(',', $tags);
            $table = ($type == SearchTagTypes::USER) ? '?:users' : '?:orders';
            $field = ($type == SearchTagTypes::USER) ? 'user_id' : 'order_id';
            $company_id = db_get_field("SELECT company_id FROM $table WHERE $field = ?i", $id);
            foreach ($tags as $tag) {
                if ($tag) {
                    fn_update_order_user_tag_links(array(
                        'tag' => trim($tag),
                        'object_id' => $id,
                        'type' => $type,
                        'company_id' => $company_id
                    ));
                }
            }
        }
    }
}

function fn_sd_order_user_tags_get_users(&$params, $fields, $sortings, &$condition, &$join)
{
    if (isset($params['search']['tags'])) {
        $_tags = $params['search']['tags'];
        unset($params['search']['tags']);
        foreach ($_tags as $tag) {
            if (trim($tag)) {
                $params['search']['tags'][] = trim($tag);
            }
        }
        if (!empty($params['search']['tags'])) {
            $join .= db_quote(' INNER JOIN ?:sd_search_tag_links AS tag_links ON tag_links.object_id = ?:users.user_id AND tag_links.type = ?s', SearchTagTypes::USER);
            $join .= db_quote(' INNER JOIN ?:sd_search_tags AS tags ON tag_links.tag_id = tags.tag_id');
            $condition[] = db_quote(' AND tags.tag IN (?a) AND tags.status = ?s AND tags.type = tag_links.type', $params['search']['tags'], SearchTagStatuses::ACTIVE);
            foreach ($params['search']['tags'] as $tag_name) {
                if ($tag_name) {
                    list($tags) = fn_get_order_user_tags(array('tag' => $tag_name, 'status' => SearchTagStatuses::ACTIVE, 'type' => SearchTagTypes::USER));
                    $params['tags'][] = array_shift($tags);
                }
            }
            unset($params['search']['tags']);
        }
    }
}

function fn_sd_order_user_tags_get_orders(&$params, $fields, $sortings, &$condition, &$join, $group)
{
    if (isset($params['search']['tags'])) {
        $_tags = $params['search']['tags'];
        unset($params['search']['tags']);
        foreach ($_tags as $tag) {
            if (trim($tag)) {
                $params['search']['tags'][] = trim($tag);
            }
        }
        if (!empty($params['search']['tags'])) {
            $join .= db_quote(' INNER JOIN ?:sd_search_tag_links AS tag_links ON tag_links.object_id = ?:orders.order_id AND tag_links.type = ?s', SearchTagTypes::ORDER);
            $join .= db_quote(' INNER JOIN ?:sd_search_tags AS tags ON tag_links.tag_id = tags.tag_id');
            $condition .= db_quote(' AND tags.tag IN (?a) AND tags.status = ?s', $params['search']['tags'], SearchTagStatuses::ACTIVE);
            $condition .= db_quote(' AND tags.type = tag_links.type');
            foreach ($params['search']['tags'] as $tag_name) {
                if ($tag_name) {
                    list($tags) = fn_get_order_user_tags(array('tag' => $tag_name, 'status' => SearchTagStatuses::ACTIVE, 'type' => SearchTagTypes::ORDER));
                    $params['tags'][] = array_shift($tags);
                }
            }
            unset($params['search']['tags']);
        }
    }
}

function fn_sd_order_user_tags_delete_company($company_id, $result)
{
    if (!empty($company_id) && $result) {
        $tag_ids = db_get_fields('SELECT tag_id FROM ?:sd_search_tags WHERE company_id = ?i', $company_id);
        $result = db_query('DELETE FROM ?:sd_search_tags WHERE tag_id IN (?n)', $tag_ids);
        if ($result) {
            db_query('DELETE FROM ?:sd_search_tag_links WHERE tag_id IN (?n)', $tag_ids);
        }
    }
}

function fn_sd_order_user_tags_delete_order($order_id)
{
    if (!empty($order_id)) {
        db_query('DELETE FROM ?:sd_search_tag_links WHERE object_id = ?i AND type = ?s', $order_id, SearchTagTypes::ORDER);
    }
}

function fn_sd_order_user_tags_post_delete_user($user_id, $user_data, $result)
{
    if (!empty($user_id) && $result) {
        db_query('DELETE FROM ?:sd_search_tag_links WHERE object_id = ?i AND type = ?s', $user_id, SearchTagTypes::USER);
    }
}

function fn_sd_order_user_tags_get_user_info($user_id, $get_profile, $profile_id, &$user_data)
{
    if ($user_id && AREA == 'A') {
        list($user_data['tags']) = fn_get_order_user_tags(array(
            'type' => SearchTagTypes::USER, 
            'object_id' => $user_id,
            'check_links' => true,
            'status' => SearchTagStatuses::ACTIVE,
        ));
    }
}

function fn_sd_order_user_tags_get_order_info(&$order, $additional_data)
{
    if ($order['order_id'] && AREA == 'A') {
        list($order['tags']) = fn_get_order_user_tags(array(
            'type' => SearchTagTypes::ORDER, 
            'object_id' => $order['order_id'],
            'check_links' => true,
            'status' => SearchTagStatuses::ACTIVE,
        ));
    }
}

function fn_sd_update_order_tag($tag_data) {
    if (!empty($tag_data)) {
        $tag_data['tag'] = strip_tags($tag_data['tag']);
        $exists = db_get_field('SELECT tag_id FROM ?:sd_search_tags WHERE tag = ?s AND company_id = ?i AND type = ?s', trim($tag_data['tag']), $tag_data['company_id'], $tag_data['type']);

        if (!$exists) {
            $tag_data['timestamp'] = TIME;
            $tag_id = db_query("INSERT INTO ?:sd_search_tags ?e", $tag_data);
        }
    }
}

function fn_sd_update_order_tag_link($tag_link_data, $tag_data) {
    if (!empty($tag_link_data)) {
        $tag_data['tag'] = strip_tags($tag_data['tag']);
        $tag_link_data['tag_id'] = db_get_field('SELECT tag_id FROM ?:sd_search_tags WHERE tag = ?s AND company_id = ?i AND type = ?s', trim($tag_data['tag']), $tag_data['company_id'], $tag_data['type']);

        $tag_id = db_query("REPLACE INTO ?:sd_search_tag_links ?e", $tag_link_data);
    }
}

function fn_insert_order_tags($tag_name, $order_id) {
    $order_tags_data = array(
        'company_id' => Registry::get('runtime.company_id'),
        'tag' => $tag_name,
        'type' => 'O',
        'status' => 'A'
    );

    $order_tag_links_data = array(
        'object_id' => $order_id,
        'type' => 'O'
    );

    fn_sd_update_order_tag($order_tags_data);
    fn_sd_update_order_tag_link($order_tag_links_data, $order_tags_data);
}

function fn_order_has_tag($tags, $tag_id) {
    foreach ($tags as $tag) {
        if ($tag['tag_id'] == $tag_id) {
            return true;
        }
    }

    return false;
}