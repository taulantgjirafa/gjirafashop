<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;
use Tygh\Enum\SearchTagTypes;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    return;
}

if ($mode == 'manage') {
    Registry::get('view')->assign('tag_type', SearchTagTypes::ORDER);
    Registry::get('view')->assign('show_search_tags_field', fn_check_view_permissions('search_tags.manage', 'GET'));
} elseif ($mode == 'details') {
    Registry::get('view')->assign('show_search_tags_field', fn_check_view_permissions('search_tags.manage', 'GET'));
}

