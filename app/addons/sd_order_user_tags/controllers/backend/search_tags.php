<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;
use Tygh\Enum\SearchTagStatuses;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

$params = $_REQUEST;

if ($_SERVER['REQUEST_METHOD']	== 'POST') {
    if ($mode == 'update') {
        if (!empty($params['tag_data'])) {
            $tag_id = fn_update_order_user_tags(0, $params['tag_data']);
            if (empty($tag_id)) {
                fn_set_notification('E', __('error'), __('sd_order_user_tags.tag_not_updated'));
            }
        }
    } elseif ($mode == 'update_link') {
        if (defined('AJAX_REQUEST') && !empty($params)) {
            fn_update_order_user_tag_links($params);

            fn_log_event('orders', 'update', array(
                'order_id' => $params['object_id']
            ));

            Registry::get('ajax')->assign('tag_name', fn_get_order_user_tag_names($params));

            exit();
        }
    } elseif ($mode == 'delete_link') {
        if (defined('AJAX_REQUEST') && !empty($params)) {
            fn_delete_order_user_tag_links($params);

            fn_log_event('orders', 'update', array(
                'order_id' => $params['object_id']
            ));

            exit();
        }
    } elseif ($mode == 'm_update') {
        if (!empty($params['tags_data'])) {
            foreach ($params['tags_data'] as $key => $value) {
                if (!empty($value['tag'])) {
                    fn_update_order_user_tags($value['tag_id'], $value);
                }
            }
        }
    } elseif ($mode == 'm_delete') {
        if (!empty($params['tag_ids'])) {
            foreach ($params['tag_ids'] as $v) {
                fn_delete_order_user_tags($v);
            }
            fn_set_notification('N', __('notice'), __('sd_order_user_tags.tags_deleted'));
        }
    } elseif ($mode == 'delete') {
        if (!empty($params['tag_id'])) {
            $result = fn_delete_order_user_tags($params['tag_id']);
            if ($result) {
                fn_set_notification('N', __('notice'), __('sd_order_user_tags.tag_deleted'));
            } else {
                fn_set_notification('E', __('error'), __('sd_order_user_tags.tag_not_deleted'));
            }
        }
    } elseif ($mode == 'approve') {
        if (!empty($params['tag_ids'])) {
            fn_order_users_tags_update_tag_statuses($params['tag_ids'], SearchTagStatuses::ACTIVE);
        }
    } elseif ($mode == 'disapprove') {
        if (!empty($params['tag_ids'])) {
            fn_order_users_tags_update_tag_statuses($params['tag_ids'], SearchTagStatuses::DISABLE);
        }
    }

    return array(CONTROLLER_STATUS_OK, 'search_tags.manage');
}

if ($mode == 'manage') {
    list($tags, $search) = fn_get_order_user_tags($params, Registry::get('settings.Appearance.admin_elements_per_page'));

    Registry::get('view')->assign('tags', $tags);
    Registry::get('view')->assign('search', $search);

} elseif ($mode == 'list') {
    if (defined('AJAX_REQUEST')) {
        $params['status'] = SearchTagStatuses::ACTIVE;
        $tags = fn_get_order_user_tag_names($params);
        Registry::get('ajax')->assign('autocomplete', $tags);

        exit();
    }
}
