<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if (empty($auth['user_id']) && empty($auth['order_ids'])) {
    return array(CONTROLLER_STATUS_REDIRECT, 'auth.login_form?return_url=' . urlencode(Registry::get('config.current_url')));
}

if($mode == 'pay'){

    $payment_id = $_REQUEST['payment_id'];
    $refund_id = $_REQUEST['return_id'];

    if(isset($_GET['r'])){
        $order_id_with_timestamp = explode('-', $_POST['OrderID']);
        $refund_id = $order_id_with_timestamp[0];
        $signatureAR = md5($refund_id . $_POST['TotalAmount'] . "ARLINDWASHEREANDFUCKUIFUDONTKNOWTHISSTRING!@#$%^");

        if(!empty($_POST['TranCode']) && $_POST['TranCode'] == "000" && $signatureAR == $_GET['awshr'] && $signatureAR == $_SESSION["DAWH"] && $_SESSION["DAWH"] == $_GET['awshr']){
            unset($_SESSION['DAWH']);
            rma_payment_accepted($refund_id, $auth);
            return array(CONTROLLER_STATUS_REDIRECT, 'rma.payment&' . 'refund_id=' . $refund_id);
        }else{
            return array(CONTROLLER_STATUS_DENIED);
        }
    }

    if($payment_id == 13 || $payment_id == 22){
        $processor_data = fn_get_payments(array('payment_id' => $_REQUEST['payment_id']));
        rma_process_rbko_payment(current($processor_data), $refund_id);
    }elseif($payment_id == 15 || $payment_id == 24){
        rma_payment_accepted($refund_id, $auth);
        return array(CONTROLLER_STATUS_REDIRECT, 'rma.payment&' . 'refund_id=' . $refund_id);
    }
}

if($mode == 'payment'){
    if(isset($_SESSION['rma_payment']) && $_SESSION['rma_payment'] == true){
        Tygh::$app['view']->assign('payment_accepted', true);
        unset($_SESSION['rma_payment']);
    }//else
       // return array(CONTROLLER_STATUS_DENIED);
}

function rma_payment_accepted($refund_id, $auth){
    $query = db_query('UPDATE ?:rma_returns SET status = ?s WHERE return_id = ?i and user_id = ?i', 'B', $refund_id, $auth['user_id']);
    if($query){
        $_SESSION['rma_payment'] = true;
        return true;
    }
    return false;
}

function rma_process_rbko_payment($processor_data, $refund_id){
    $processor_data['processor_params'] = unserialize($processor_data['processor_params']);
    $MerchantID = $processor_data['processor_params']['merchant_id'];
    $TerminalID = $processor_data['processor_params']['terminal_id'];
    $signature = $processor_data['processor_params']['signature'];
    $CurrencyID = $processor_data['processor_params']['currency_id'];
    $url = $processor_data['processor_params']['terminal_url'];
    $description = "Pagesa për rikthimin e produkteve në Gjirafa50";

    $OrderID = $refund_id . "-" . "1" . "-" . $_SESSION['auth']['user_id'];
    $PurchaseTime = date("ymdHis");
    $TotalAmount = 100;
    $data = "$MerchantID;$TerminalID;$PurchaseTime;$OrderID;$CurrencyID;$TotalAmount;;";
    $fp = fopen("./app/payments/$MerchantID.pem", "r");
    $priv_key = fread($fp, 8192);
    fclose($fp);
    $pkeyid = openssl_get_privatekey($priv_key);
    openssl_sign($data, $signature, $pkeyid);
    openssl_free_key($pkeyid);
    $b64sign = base64_encode($signature);

    $_SESSION["DAWH"] = md5($refund_id . $TotalAmount . "ARLINDWASHEREANDFUCKUIFUDONTKNOWTHISSTRING!@#$%^");

    echo '<noscript><META HTTP-EQUIV="Refresh" CONTENT="0;URL=/index.php?dispatch=payment_notification.declined&payment=raiffeisenbank"></noscript>
		<form id="s" action="' . $url . '" method="post"><input name="Version" type="hidden" value="1" />
		<input name="MerchantID" type="hidden" value="' . $MerchantID . '" />
		<input name="TerminalID" type="hidden" value="' . $TerminalID . '" />
		<input name="TotalAmount" type="hidden" value="' . $TotalAmount . '" />
		<input name="Currency" type="hidden" value="' . $CurrencyID . '" />
		<input name="locale" type="hidden" value="RU" />
		<input name="PurchaseTime" type="hidden" value="' . $PurchaseTime . '" />
		<input name="OrderID" type="hidden" value="' . $OrderID . '" />
		<input name="PurchaseDesc" type="hidden" value="' . $description . '">
		<input name="Signature" type="hidden" value="' . $b64sign . '"/>
		<input type="submit" style="visibility:hidden" value="Paguaj tani"/></form>
		<style>body {text-align: center;font-family: sans-serif;color:#333;}</style><img src="https://hhstsyoejx.gjirafa.net/gj50/icons/processor-payment-loading.svg" style="display: block;margin: 50px auto;max-width: 50px;
"/>		Duke u lidhur me <strong>Raiffeisen Bank</strong>
<script  data-cfasync="false" type="text/javascript">document.getElementById("s").submit();</script>
		';
    exit;
}