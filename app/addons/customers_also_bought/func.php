<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

if (!defined('BOOTSTRAP')) { die('Access denied'); }

function fn_customers_also_bought_place_order(&$order_id)
{

    $product_ids = db_get_fields("SELECT product_id FROM ?:order_details WHERE order_id = ?i GROUP BY product_id", $order_id);

    if (count($product_ids) < 2) {
        return;
    }

    foreach ($product_ids as $_origin) {
        foreach ($product_ids as $_target) {
            if ($_origin != $_target) {
                $_data = array (
                    'product_id' => $_origin,
                    'related_id' => $_target,
                    'amount' => db_get_field("SELECT amount FROM ?:also_bought_products WHERE product_id = ?i AND related_id = ?i", $_origin, $_target),
                );

                $_data['amount']++;

                db_query("REPLACE INTO ?:also_bought_products ?e", $_data);
            }
        }
    }
}

function fn_customers_also_bought_delete_product_post(&$product_id)
{
    db_query("DELETE FROM ?:also_bought_products WHERE product_id = ?i OR related_id = ?i", $product_id, $product_id);

    return true;
}

function fn_customers_also_bought_get_products(&$params, &$fields, &$sortings, &$condition, &$join, &$sorting, &$group_by)
{
    if (!empty($params['also_bought_for_product_id'])) {
        $fields[] = 'SUM(?:also_bought_products.amount) amnt';
        $join .= ' LEFT JOIN ?:also_bought_products ON ?:also_bought_products.related_id = products.product_id ';
        $condition .= db_quote(' AND ?:also_bought_products.product_id = ?i', $params['also_bought_for_product_id']);
        $group_by = '?:also_bought_products.related_id';
    }

    return true;
}

function fn_cart_get_also_bought_products($product_ids) {
    $recently_viewed = db_get_fields("SELECT product_id FROM ?:products WHERE product_code IN(?a)", Tygh::$app['session']['recently_viewed_products']);
    $product_categories = fn_get_product_categories(array_merge($product_ids, $recently_viewed));

    if (!empty($product_ids)) {
        $related_ids = fn_get_also_bought_products($product_ids);
    }

    $imploded_related_ids = implode(',', array_column($related_ids,'related_id'));
    $products = fn_get_also_bought_products_data($imploded_related_ids);

    if (empty($products)) {
        return array();
    }

    foreach($products as $key => $value) {
        if (!in_array($value['category_id'], $product_categories)) {
            unset($products[$key]);
        }

        foreach ($product_ids as $product_id) {
            if ($products[$key]['product_id'] == $product_id) {
                unset($products[$key]);
            }
        }
    }

    return $products;
}

function fn_get_also_bought_products($product_ids) {
    return db_get_array("SELECT related_id from ?:also_bought_products WHERE product_id IN(?n) ORDER BY amount DESC", implode(',', $product_ids));
}

function fn_get_also_bought_products_data($related_ids) {
    if (!empty($related_ids)) {
        $products = db_get_array("SELECT ?:products_categories.category_id, ?:products.product_id, product, ?:product_prices.price, ?:ult_product_prices.price AS price_al FROM gjshop_products
                                    INNER JOIN ?:product_descriptions
                                    ON ?:product_descriptions.product_id = ?:products.product_id
                                    INNER JOIN ?:product_prices
                                    ON ?:product_prices.product_id = ?:products.product_id
                                      INNER JOIN ?:ult_product_prices
                                      ON ?:ult_product_prices.product_id = ?:products.product_id
                                      INNER JOIN ?:products_categories
                                      ON ?:products_categories.product_id = ?:products.product_id
                                    WHERE ?:products.product_id IN($related_ids) GROUP BY ?:products.product_id
                                    ORDER BY FIELD(?:products.product_id, $related_ids)
                                    ");
    }

    return $products;
}