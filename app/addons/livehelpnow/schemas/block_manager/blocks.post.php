<?php
$schema['livehelpnow'] = array(
    'templates' => array (
        'addons/livehelpnow/blocks/lhnchat.tpl' => array(
            'settings' => array (
                'lhnaccount' => array (
                    'type' => 'input',
                    'tooltip' => '*Must be numeric<br /><a href="http://www.livehelpnow.net/lnd/trial.aspx" target="_blank">Sign Up</a> if you do not have a LiveHelpNow Account'
                ),
                'lhnbutton' => array (
                    'type' => 'input',
                    'default_value' => '35',
                    'tooltip' => '*Must be numeric<br /><a href="http://www.livehelpnow.net/products/live_chat_system/buttons/" target="_blank">View Buttons</a>'
                ),
                'lhnchatwindow' => array (
                    'type' => 'input',
                    'default_value' => '0',
                    'tooltip' => '*Must be numeric<br />This is usaully 0'
                ),
                'lhninvite' => array (
                    'type' => 'input',
                    'default_value' => '0',
                    'tooltip' => '*Must be numeric<br />This is usaully 0'
                ),
                'lhndepartment' => array (
                    'type' => 'input',
                    'default_value' => '0',
                    'tooltip' => '*Must be numeric<br />This is usaully 0'
                ),
                'lhnautochat' => array (
                    'type' => 'checkbox'
                )
            )
        ),
        'addons/livehelpnow/blocks/lhnhelpout.tpl' => array(
            'settings' => array (
                'lhnaccount' => array (
                    'type' => 'input',
                    'tooltip' => '*Must be numeric<br /><a href="http://www.livehelpnow.net/lnd/trial.aspx" target="_blank">Sign Up</a> if you do not have a LiveHelpNow Account'
                ),
                'lhnchatwindow' => array (
                    'type' => 'input',
                    'default_value' => '0'
                ),
                'lhninvite' => array (
                    'type' => 'input',
                    'default_value' => '0'
                ),
                'lhndepartment' => array (
                    'type' => 'input',
                    'default_value' => '0'
                ),
                'lhnautochat' => array (
                    'type' => 'checkbox'
                ),
                'lhntheme' => array (
                    'type' => 'selectbox',
                    'values' => array(
                        'lhndefault' => 'lhndefault',
                        'lhnred' => 'lhnred',
                        'lhnorange' => 'lhnorange',
                        'lhnyellow' => 'lhnyellow',
                        'lhngreen' => 'lhngreen',
                        'lhnblue' => 'lhnblue',
                        'lhnpurple' => 'lhnpurple'
                    ),
                    'default_value' => 'default_value'
                ),
                'lhnslideout' => array (
                    'type' => 'checkbox',
                    'tooltip' => 'Uncheck if you will only be using the Live Chat system'
                ),
                'lhnchatbutton' => array (
                    'type' => 'checkbox',
                    'tooltip' => 'Uncheck to hide the "chat with us" option'
                ),
                'lhnticket' => array (
                    'type' => 'checkbox',
                    'tooltip' => 'Uncheck to hide the "submit a ticket" option'
                ),
                'lhncallback' => array (
                    'type' => 'checkbox',
                    'tooltip' => 'Uncheck to hide the "request callback" option'
                ),
                'lhnknowbase' => array (
                    'type' => 'checkbox',
                    'tooltip' => 'Uncheck to hide the Knowledge Base search bar, and skip to "More Options"'
                ),
                'lhnmoreop' => array (
                    'type' => 'checkbox',
                    'tooltip' => 'Uncheck to display the "More options" slider automatically'
                ),
                'lhnsearchtitle' => array (
                    'type' => 'input',
                    'default_value' => 'Find answers'
                ),
                'lhnsearchmessage' => array (
                    'type' => 'input',
                    'default_value' => 'Please search our knowledge base for answers or click [More options]'
                ),
                'lhnnoresults' => array (
                    'type' => 'input',
                    'default_value' => 'No results found for'
                ),
                'lhnviews' => array (
                    'type' => 'input',
                    'default_value' => 'view'
                )
            )
        )
    )
);

return $schema;
?>