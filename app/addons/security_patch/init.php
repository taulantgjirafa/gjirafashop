<?php
/***************************************************************************
 *                                                                          *
 *   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 *                                                                          *
 ****************************************************************************
 * PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
 * "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
 ****************************************************************************/

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if (isset($_SERVER['PHP_AUTH_USER'])) {
    $_SERVER['PHP_AUTH_USER'] = trim($_SERVER['PHP_AUTH_USER']);
}

if (isset($_SERVER['PHP_AUTH_PW'])) {
    $value = trim($_SERVER['PHP_AUTH_PW']);

    if ($_SERVER['PHP_AUTH_PW'] !== $value) {
        $_SERVER['PHP_AUTH_PW'] = $value;

        if (defined('API') && function_exists('fn_init_api')) {
            if (class_exists('\Tygh\Tygh')) {
                unset(Tygh::$app['api']);
            }

            fn_init_api();
        }
    }
}

if (!empty($_REQUEST)) {
    foreach ($_REQUEST as &$item) {
        if (is_array($item)) {
            $item = fn_security_patch_strip_tags($item);
        }
    }
    unset($item);
}

function fn_security_patch_strip_tags($data)
{
    $prefix = preg_quote(\Tygh\Registry::get('config.table_prefix'));

    foreach ($data as $key => $value) {
        if (preg_match("/(.+\b{$prefix}\w+|.+\?:\w+|;.+)/", $key)) {
            unset($data[$key]);
        } elseif (is_array($value)) {
            $data[$key] = fn_security_patch_strip_tags($value);
        }
    }

    return $data;
}