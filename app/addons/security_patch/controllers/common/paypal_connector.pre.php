<?php
/***************************************************************************
 *                                                                          *
 *   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 *                                                                          *
 ****************************************************************************
 * PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
 * "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
 ****************************************************************************/

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    return array(CONTROLLER_STATUS_OK);
}

$validation = isset($_REQUEST['validation']) ? $_REQUEST['validation'] : '';
$payment_id = isset($_REQUEST['payment_id']) ? $_REQUEST['payment_id'] : null;

if ($mode == 'end_signup'
    && !db_get_field('SELECT ekey FROM ?:ekeys WHERE ekey = ?s AND object_type = ?s', $validation, 'I')
) {
    if ($payment_id) {
        fn_set_notification(
            'N',
            '',
            "You now have a PayPal business account, and you've successfully integrated " . PRODUCT_NAME . " with PayPal."
            . "<br>Don't forget to enable the configured payment method from the administration panel."
            . "<br>You will also need to confirm your email to activate your account so you can access the payments. To do this, follow the instructions PayPal sent to your email.",
            'K'
        );

        return array(CONTROLLER_STATUS_REDIRECT, fn_url('', 'C'));
    }

    return array(CONTROLLER_STATUS_NO_PAGE);
}
