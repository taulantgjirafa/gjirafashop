<?php
/***************************************************************************
 *                                                                          *
 *   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 *                                                                          *
 ****************************************************************************
 * PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
 * "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
 ****************************************************************************/

use Tygh\Hashids;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($mode == 'claim') {
    $hashids = new Hashids('gj50licensekeyproducts');
    $order_id = $hashids->decode($_REQUEST['key'])[0];

    $key_object = db_get_field('SELECT key_object FROM ?:license_key_orders WHERE order_id = ?i', 321321);
    $key_object = unserialize($key_object);

    Tygh::$app['view']->assign('key', $key_object->serial);
}