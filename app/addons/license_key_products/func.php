<?php
/***************************************************************************
 *                                                                          *
 *   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 *                                                                          *
 ****************************************************************************
 * PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
 * "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
 ****************************************************************************/

if (!defined('AREA')) {
    die('Access denied');
}

use Tygh\Hashids;
use Tygh\KinguinClient;
use Tygh\Mailer;

function fn_license_key_products_get_product_data_extra(&$product_data)
{
    if ($product_data['store_id'] == LICENSE_KEY_PRODUCTS_STORE_ID) {
        $product_data['license_key_product'] = true;
    }
}

function fn_license_key_products_add_to_cart(&$cart, $product_id, $_id, $product_data, $cart_action)
{
    if ($product_data[$product_id]['store_id'] == LICENSE_KEY_PRODUCTS_STORE_ID) {
        $cart['products'][$_id]['license_key_product'] = true;
    }
}

function fn_license_key_products_calculate_cart_post(&$cart, $auth, $calculate_shipping, $calculate_taxes, $options_style, $apply_cart_promotions, $cart_products, $product_groups)
{
    $license_key_product = false;
    $cart_license_products_only = [];

    foreach ($cart['products'] as $product) {
        if ($product['license_key_product'] == 1) {
            $license_key_product = true;
            break;
        }
    }

    foreach ($cart['products'] as $product) {
        if ($product['license_key_product'] == 1) {
            $cart_license_products_only[] = true;
        }
    }

    if (in_array(false, $cart_license_products_only, true) === false){
        unset($cart['product_groups']);
    }

    $cart['has_license_key_product'] = $license_key_product;
    $cart['shipping_required'] = false;
}

function fn_license_key_products_get_cart_product_data($product_id, &$_pdata, $product, $auth, $cart, $hash)
{
    $_pdata['license_key_product'] = filter_var($product['license_key_product'], FILTER_VALIDATE_BOOLEAN);
}

function fn_license_key_products_place_order($order_id, &$action, &$order_status, $cart, $auth)
{
    if ($cart['has_license_key_product']) {
        $data = [
            'products' => [],
            'orderExternalId' => $order_id
        ];

        foreach ($cart['products'] as $product) {
            if ($product['license_key_product']) {
                $data['products'][] = [
                    'kinguinId' => $product['product_id'],
                    'qty' => $product['amount'],
                    'name' => $product['product'],
                    'price' => $product['price'] * $product['amount'],
                ];
            }
        }

        $order_created = KinguinClient::createOrder($order_id, json_encode($data));

        if ($order_created) {
            $order_status = 'O';
        } else {
            $action = 'lk_incomplete_order';
        }
    }
}

function fn_license_key_products_placement_routines($order_id, &$order_info, $force_notification, $clear_cart, $action, $display_notification)
{
    foreach ($order_info['products'] as $product) {
        if ($product['store_id'] == LICENSE_KEY_PRODUCTS_STORE_ID) {
            $order_info['license_key_order'] = true;
            break;
        }
    }
}

function fn_license_key_products_send_order_notification($order_info, $edp_data, $force_notification, $notified, $send_order_notification)
{
    if ($send_order_notification) {
        $key = db_get_field('SELECT data FROM ?:order_data WHERE order_id = ?i AND type = ?s', $order_info['order_id'], 'L');

        $email_params = array(
            'to' => $order_info['email'],
            'from' => 'company_orders_department',
            'data' => array(
                'order_info' => $order_info,
                'key' => $key
            ),
            'tpl' => 'orders/license_key_notification.tpl',
            'company_id' => $order_info['company_id'],
        );

//        Mailer::sendMail($email_params, 'C', $order_info['lang_code']);
    }
}

function fn_license_key_products_verify_order(&$cart, &$verify)
{
    foreach ($cart['products'] as $key => $product) {
        if ($product['store_id'] == LICENSE_KEY_PRODUCTS_STORE_ID) {
//            unset($cart['products'][$key]);
            $verify = false;
        }
    }
}

function fn_license_key_products_order_placement_routines($order_id, $force_notification, $order_info, $_error, $response, $checkout, $params)
{
    if ($response == 'approved') {
        $hashids = new Hashids('gj50licensekeyproducts');
        $hashed_id = $hashids->encode($order_id);

        $email_params = array(
            'to' => $order_info['email'],
            'from' => 'company_orders_department',
            'data' => array(
                'order_info' => $order_info,
                'key' => fn_url('license_key_products.claim&key=' . $hashed_id)
            ),
            'tpl' => 'orders/license_key_notification.tpl',
            'company_id' => $order_info['company_id'],
        );

//        Mailer::sendMail($email_params, 'C', $order_info['lang_code']);
    }
}