<?php

namespace Tygh;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Exception\ClientException;

class KinguinClient
{
    public static function createOrder($orderId, $body) {
        $orderObject = json_decode(self::placeOrder($body));

        if (!is_int($orderObject)) {
            $dispatchObject = json_decode(self::dispatch($orderObject->orderId));
            $keysObject = json_decode(self::getKeys($dispatchObject->dispatchId));

            $data = [
                'order_id' => $orderId,
                'order_object' => serialize($orderObject),
                'dispatch_object' => serialize($dispatchObject),
                'key_object' => serialize($keysObject[0])
            ];
            db_query('REPLACE INTO ?:license_key_orders ?e', $data);

            return true;
        }

        return false;
    }

    public static function placeOrder($body)
    {
        $client = new Client([
            'base_uri' => KINGUIN_BASE_URL
        ]);
        $headers = [
            'X-Api-Key' => KINGUIN_API_KEY,
            'Content-Type' => 'application/json'
        ];
        $request = new Request('POST', 'v1/order', $headers, $body);

        try {
            $response = $client->send($request);
        } catch (ClientException $e) {
            return $e->getResponse()->getStatusCode();
        }

        return $response->getBody()->getContents();
    }

    public static function dispatch($orderId)
    {
        $body = [
            'orderId' => $orderId
        ];

        $client = new Client([
            'base_uri' => KINGUIN_BASE_URL
        ]);
        $headers = [
            'X-Api-Key' => KINGUIN_API_KEY,
            'Content-Type' => 'application/json'
        ];
        $request = new Request('POST', 'v1/order/dispatch', $headers, json_encode($body));

        try {
            $response = $client->send($request);
        } catch (ClientException $e) {
            return $e->getResponse()->getStatusCode();
        }

        return $response->getBody()->getContents();
    }

    public static function getKeys($dispatchId)
    {
        $client = new Client([
            'base_uri' => KINGUIN_BASE_URL
        ]);
        $headers = [
            'X-Api-Key' => KINGUIN_API_KEY,
        ];
        $request = new Request('GET', "v1/order/dispatch/keys?dispatchId={$dispatchId}", $headers);

        try {
            $response = $client->send($request);
        } catch (ClientException $e) {
            return $e->getResponse()->getStatusCode();
        }

        return $response->getBody()->getContents();
    }

    public static function getOrder($orderId)
    {
        $client = new Client([
            'base_uri' => KINGUIN_BASE_URL
        ]);
        $headers = [
            'X-Api-Key' => KINGUIN_API_KEY,
        ];
        $request = new Request('GET', "v1/order/{$orderId}", $headers);

        try {
            $response = $client->send($request);
        } catch (ClientException $e) {
            return $e->getResponse()->getStatusCode();
        }

        return $response->getBody()->getContents();
    }
}