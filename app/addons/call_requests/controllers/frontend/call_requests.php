<?php
/***************************************************************************
 *                                                                          *
 *   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 *                                                                          *
 ****************************************************************************
 * PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
 * "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
 ****************************************************************************/

use Tygh\GjirafaSso;
use Tygh\Registry;

if (!defined('BOOTSTRAP')) {
    die('Access denied');
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $return_url = '';
    $order_id = '';

    if ($mode == 'request') {
        if (fn_image_verification('call_request', $_REQUEST) == false) {
            fn_save_post_data('cr_product_data');
        } elseif (!empty($_REQUEST['cr_product_data'])) {
            $valid = fn_call_requests_validate_user_data($_REQUEST['cr_product_data']);
            $user_id = fn_is_user_exists(0, array(
                'email' => $_REQUEST['cr_product_data']['email']
            ));
            $user_is_blacklisted = fn_user_is_blacklisted($user_id);

            if (!$valid) {
                fn_set_notification('W', __('error'), 'Ju lutem plotësoni të gjitha fushat e nevojshme!');

                return array(CONTROLLER_STATUS_REDIRECT, fn_url('products.view&product_id=' . $_REQUEST['cr_product_data']['product_id']));
            }

            if ($user_is_blacklisted && in_array($_REQUEST['cr_product_data']['payment_id'], [15, 24])) {
                fn_set_notification('W', __('error'), 'Ju lutem zgjedhni një metodë tjetër të pagesës.');
                Tygh::$app['session']['auth']['blacklisted'] = true;

                return array(CONTROLLER_STATUS_REDIRECT, fn_url('products.view&product_id=' . $_REQUEST['cr_product_data']['product_id']));
            }

            fn_call_requests_map_profile_fields($_REQUEST['cr_product_data']);

            $gjirafa_sso = new GjirafaSso();
            $user_data = $gjirafa_sso->parseCallRequestUserData($_REQUEST['cr_product_data']);
            $registered_to_sso = $gjirafa_sso->register($user_data);

            if ($user_id == '') {
                list($user_id, $profile_id) = fn_update_user(0, $gjirafa_sso->parseRegisterUserData($_REQUEST['cr_product_data']), Tygh::$app['session']['auth'], false, false);

                if (!$user_id) {
                    fn_log_event('cr_users', 'create', array(
                            'cr_product_data' => $_REQUEST['cr_product_data'],
                            'cr_auth' => Tygh::$app['session']['auth']
                        )
                    );
                }
            }

            fn_login_user($user_id);

            $product_data = !empty($_REQUEST['product_data']) ? $_REQUEST['product_data'] : array();

            if ($res = fn_do_call_request($_REQUEST['cr_product_data'], $product_data, Tygh::$app['session']['cart'], Tygh::$app['session']['auth'])) {
                if (!empty($res['result']['error'])) {
                    fn_set_notification('E', __('error'), $res['result']['error']);
                } elseif (!empty($res['result']['notice'])) {
                    fn_set_notification('N', __('notice'), $res['result']['notice']);
                    $order_id = $res['order_id'];
                }
            }
        }
    }
}

if ($mode == 'request') {
    $auth = Tygh::$app['session']['auth'];
    $user_id = Tygh::$app['session']['auth']['user_id'];

    if ($user_id) {
        $user_info = fn_get_user_info($user_id);
        $user_profile_fields = fn_get_profile_fields('C', $auth);

        if (fn_isAL()) {
            $city_id = $user_info['fields'][55];
        } else {
            $city_id = $user_info['fields'][41];
        }

        Tygh::$app['view']->assign('cr_auth', true);
        Tygh::$app['view']->assign('cr_user_info', $user_info);
        Tygh::$app['view']->assign('cr_user_profile_fields', $user_profile_fields);
        Tygh::$app['view']->assign('cr_city_id', $city_id);
    }

    if (!empty($_REQUEST['product_id'])) {
        $product = fn_get_product_data($_REQUEST['product_id'], $auth, DESCR_SL);
        fn_gather_additional_product_data($product, true, true);
        Tygh::$app['view']->assign('product', $product);
        $_REQUEST['obj_id'] = $product['product_id'];
    }

    if (isset($order_id) && $order_id != '') {
        $security_hash = md5($order_id);
        return array(CONTROLLER_STATUS_REDIRECT, 'checkout.complete&order_id=' . $order_id . '&security_hash=' . $security_hash . '&t=oneClick');
    } else {
        Tygh::$app['view']->assign('obj_prefix', !empty($_REQUEST['obj_prefix']) ? $_REQUEST['obj_prefix'] : '');
        Tygh::$app['view']->assign('obj_id', !empty($_REQUEST['obj_id']) ? $_REQUEST['obj_id'] : '');
    }

    Tygh::$app['view']->assign('payments', fn_call_requests_get_payments($product, Tygh::$app['session']['auth']['blacklisted']));
    Tygh::$app['view']->assign('cities', fn_call_requests_get_cities($product));
    Tygh::$app['view']->assign('shippings', fn_call_requests_get_shippings($product));
    Tygh::$app['view']->assign('delivery_dates', fn_get_product_delivery_dates($product));
}
