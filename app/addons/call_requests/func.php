<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

if (!defined('BOOTSTRAP')) { die('Access denied'); }

use Tygh\Registry;
use Tygh\Navigation\LastView;
use Tygh\Mailer;

function fn_call_requests_info()
{
    if (isset(Tygh::$app['view'])) {
        return Tygh::$app['view']->fetch('addons/call_requests/settings/info.tpl');
    }
}

function fn_call_requests_get_phone()
{
    return Registry::ifGet('addons.call_requests.phone', Registry::get('settings.Company.company_phone'));
}

function fn_call_requests_get_splited_phone()
{
    $phone_number = fn_call_requests_get_phone();
    $length = Registry::get('addons.call_requests.phone_prefix_length');

    if (empty($length) || intval($length) == 0) {
        $length = 0;
    }

    return array(
        'prefix' => substr($phone_number, 0, $length),
        'postfix' => substr($phone_number, $length)
    );
}

function fn_get_call_requests($params = array(), $lang_code = CART_LANGUAGE)
{
    // Init filter
    $params = LastView::instance()->update('call_requests', $params);

    $params = array_merge(array(
        'items_per_page' => 0,
        'page' => 1,
    ), $params);

    $fields = array(
        'r.*',
        'o.status as order_status',
        'd.product',
    );

    $joins = array(
        db_quote("LEFT JOIN ?:users u USING(user_id)"),
        db_quote("LEFT JOIN ?:orders o USING(order_id)"),
        db_quote("LEFT JOIN ?:product_descriptions d ON d.product_id = r.product_id AND d.lang_code = ?s", $lang_code),
    );

    $sortings = array (
        'id' => 'r.request_id',
        'date' => 'r.timestamp',
        'status' => 'r.status',
        'name' => 'r.name',
        'phone' => 'r.phone',
        'user_id' => 'r.user_id',
        'user' => array('u.lastname', 'u.firstname'),
        'order' => 'r.order_id',
        'order_status' => 'o.status',
    );

    $condition = array();

    if (isset($params['id']) && fn_string_not_empty($params['id'])) {
        $params['id'] = trim($params['id']);
        $condition[] = db_quote("r.request_id = ?i", $params['id']);
    }

    if (isset($params['name']) && fn_string_not_empty($params['name'])) {
        $params['name'] = trim($params['name']);
        $condition[] = db_quote("r.name LIKE ?l", '%' . $params['name'] . '%');
    }

    if (isset($params['phone']) && fn_string_not_empty($params['phone'])) {
        $params['phone'] = trim($params['phone']);
        $condition[] = db_quote("r.phone LIKE ?l", '%' . $params['phone'] . '%');
    }

    if (!empty($params['status'])) {
        $condition[] = db_quote("r.status = ?s", $params['status']);
    }

    if (!empty($params['company_id'])) {
        $condition[] = db_quote("r.company_id = ?i", $params['company_id']);
    }

    if (!empty($params['order_status'])) {
        $condition[] = db_quote("o.status = ?s", $params['order_status']);
    }

    if (!empty($params['user_id'])) {
        $condition[] = db_quote("r.user_id = ?s", $params['user_id']);
    }

    if (!empty($params['order_exists'])) {
        $sign = $params['order_exists'] == 'Y' ? '<>' : '=';
        $condition[] = db_quote("r.order_id ?p 0", $sign);
    }

    $fields_str = implode(', ', $fields);
    $joins_str = ' ' . implode(' ', $joins);
    $condition_str = $condition ? (' WHERE ' . implode(' AND ', $condition)) : '';
    $sorting_str = db_sort($params, $sortings, 'date', 'desc');

    $limit = '';
    if (!empty($params['items_per_page'])) {
        $params['total_items'] = db_get_field(
            "SELECT COUNT(r.request_id) FROM ?:call_requests r" . $joins_str . $condition_str
        );
        $limit = db_paginate($params['page'], $params['items_per_page'], $params['total_items']);
    }

    $items = db_get_array(
        "SELECT " . $fields_str
        . " FROM ?:call_requests r"
        . $joins_str
        . $condition_str
        . $sorting_str
        . $limit
    );

    if (!empty($items)) {
        $cart_product_ids = array();
        foreach ($items as &$item) {
            if (!empty($item['cart_products'])) {
                $item['cart_products'] = unserialize($item['cart_products']);
                foreach ($item['cart_products'] as $cart_product) {
                    $cart_product_ids[] = $cart_product['product_id'];
                }
            }
        }
        $cart_product_names = db_get_hash_single_array(
            "SELECT product_id, product FROM ?:product_descriptions WHERE product_id IN(?n) AND lang_code = ?s",
            array('product_id', 'product'), array_unique($cart_product_ids), $lang_code
        );
        foreach ($items as &$item) {
            if (!empty($item['cart_products'])) {
                foreach ($item['cart_products'] as &$cart_product) {
                    if (!empty($cart_product_names[$cart_product['product_id']])) {
                        $cart_product['product'] = $cart_product_names[$cart_product['product_id']];
                    }
                }
            }
        }
    }

    LastView::instance()->processResults('call_requests', $items, $params);

    return array($items, $params);
}

function fn_update_call_request($data, $request_id = 0)
{
    if (isset($data['cart_products'])) {
        if (!empty($data['cart_products']) && is_array($data['cart_products'])) {
            foreach ($data['cart_products'] as $key => $product) {
                if (empty($product['product_id'])) {
                    unset($data['cart_products'][$key]);
                }
            }
            $data['cart_products'] = !empty($data['cart_products']) ? serialize($data['cart_products']) : '';
        } else {
            $data['cart_products'] = '';
        }
    }

    if ($request_id) {
        db_query("UPDATE ?:call_requests SET ?u WHERE request_id = ?i", $data, $request_id);
    } else {
        if (empty($data['timestamp'])) {
            $data['timestamp'] = TIME;
        }
        if (empty($data['company_id']) && $company_id = Registry::get('runtime.company_id')) {
            $data['company_id'] = $company_id;
        }
        $request_id = db_query("INSERT INTO ?:call_requests ?e", $data);
    }

    return $request_id;
}

function fn_delete_call_request($request_id)
{
    return db_query("DELETE FROM ?:call_requests WHERE request_id = ?i", $request_id);
}

function fn_do_call_request($params, $product_data, &$cart, &$auth)
{
    $result = array();

    $params['cart_products'] = fn_call_requests_get_cart_products($cart);

    if (!empty($params['product_id']) && $params['phone'] && $params['email']) {
        $params['order_id'] = fn_call_requests_placing_order($params, $product_data, $cart, $auth);
    }
    else {
        fn_set_notification('E', __('warning'), __('call_requests.request_not_recieved_please_fill'));
        return array(CONTROLLER_STATUS_REDIRECT, 'products.view&product_id='.$params['product_id']);
    }

    if (fn_allowed_for('ULTIMATE')) {
        $company_id = Registry::get('runtime.company_id');
    } elseif (!empty($params['order_id'])) {
        $company_id = db_get_field('SELECT company_id FROM ?:orders WHERE order_id = ?i', $params['order_id']);
    } elseif (!empty($params['product_id'])) {
        $company_id = db_get_field('SELECT company_id FROM ?:products WHERE product_id = ?i', $params['product_id']);
    } else {
        $company_id = 0;
    }

    $params['company_id'] = $company_id;

    $request_id = fn_update_call_request($params);

    $lang_code = fn_get_company_language($company_id);
    if (empty($lang_code)) {
        $lang_code = CART_LANGUAGE;
    }
    $url = fn_url('call_requests.manage?id=' . $request_id, 'A', 'current', $lang_code, true);

    if (empty($params['product_id'])) { // Call request
        Mailer::queueEmail(array(
            'to' => 'company_orders_department',
            'from' => 'default_company_orders_department',
            'data' => array(
                'url' => $url,
                'customer' => $params['name'],
                'phone_number' => $params['phone'],
                'time_from' => $params['time_from'] ?: CALL_REQUESTS_DEFAULT_TIME_FROM,
                'time_to' => $params['time_to'] ?: CALL_REQUESTS_DEFAULT_TIME_TO,
            ),
            'tpl' => 'addons/call_requests/call_request.tpl',
            'company_id' => $company_id,
        ), 'A', $lang_code);

    } elseif (empty($params['order_id'])) { // Buy with one click without order
        Mailer::queueEmail(array(
            'to' => 'company_orders_department',
            'from' => 'default_company_orders_department',
            'data' => array(
                'url' => $url,
                'customer' => $params['name'],
                'phone_number' => $params['phone'],
                'product_url' => fn_url('products.view?product_id=' . $params['product_id'], 'C'),
                'product_name' => fn_get_product_name($params['product_id'], $lang_code),
            ),
            'tpl' => 'addons/call_requests/buy_with_one_click.tpl',
            'company_id' => $company_id,
        ), 'A', $lang_code);
    }

    if (!empty($params['order_id'])) {
        $result['notice'] = __('call_requests.order_placed', array('[order_id]' => $params['order_id']));
        $data = array(
            "tag_id" => 251,
            "object_id" => $params['order_id'],
            "type"=> 'O'
        );
        db_query('INSERT INTO ?:sd_search_tag_links ?e',$data);
    } else {
        fn_set_notification('E', __('warning'), __('call_requests.request_not_recieved'));
//        $result['notice'] = __('call_requests.request_not_recieved');
    }

    $results = array(
        'result' => $result,
        'order_id' => $params['order_id']
    );

    if ($results['order_id'] && ($params['payment_id'] == 13 || ($params['payment_id'] == 22 && fn_isAL()))) {
        $processor_data = fn_get_payments(array('payment_id' => $params['payment_id']));
        fn_call_requests_process_rbko_payment(current($processor_data), $params['order_id']);
    }

    return $results;
}

function fn_call_requests_get_cart_products(&$cart)
{
    $products = array();

    if (!empty($cart['products'])) {
        foreach ($cart['products'] as $product) {
            $products[] = array(
                'product_id' => $product['product_id'],
                'amount'     => $product['amount'],
                'price'      => $product['price'],
            );
        }
    }

    return $products;
}

function fn_call_requests_placing_order($params, $product_data, &$cart, &$auth)
{
    $coupons = array();
    $coupon_code = empty($params['coupon_code']) ? '' : strtoupper(trim($params['coupon_code']));

    if (!empty($coupon_code) && fn_get_coupon_code_id($coupon_code) != '') {
        $coupons = array (
            $coupon_code =>
                array (
                    0 => fn_get_coupon_code_id($coupon_code),
                ),
        );
    }

    // Save cart
    $buffer_cart = $cart;
    $buffer_auth = $auth;

    $cart = array(
        'products' => array(),
        'recalculate' => false,
        'payment_id' => $params['payment_id'],
        'is_call_request' => true,
        'chosen_shipping' => array(
            $params['shipping_id']
        ),
        'ship_to_another' => false
    );

    if (!empty($coupons)) {
        $cart['coupons'] = $coupons;
    }

    $firstname = $params['name'];
    $lastname = $params['lastname'];
    $cart['user_data']['email'] = $params['email'];
    if (!empty($firstname) && strpos($firstname, ' ')) {
        list($firstname, $lastname) = explode(' ', $firstname);
    }
    $cart['user_data']['firstname'] = $firstname;
    $cart['user_data']['b_firstname'] = $firstname;
    $cart['user_data']['s_firstname'] = $firstname;
    $cart['user_data']['lastname'] = $lastname;
    $cart['user_data']['b_lastname'] = $lastname;
    $cart['user_data']['s_lastname'] = $lastname;
    $cart['user_data']['phone'] = $params['phone'];
    $cart['user_data']['b_phone'] = $params['phone'];
    $cart['user_data']['s_phone'] = $params['phone'];
    $cart['user_data']['b_address'] = $params['address'];
    $cart['user_data']['s_address'] = $params['address'];
    $cart['user_data']['fields'] = $params['fields'];
    foreach (array('b_address', 's_address', 'b_city', 's_city', 'b_country', 's_country', 'b_state', 's_state') as $key) {
        if (!isset($cart['user_data'][$key])) {
            $cart['user_data'][$key] = '';
        }
    }

    if ($params['gjflex'] == 'true') {
        $params['gjflex'] = 1;
    } else {
        $params['gjflex'] = 0;
    }

    if (empty($product_data[$params['product_id']])) {
        $product_data[$params['product_id']] = array(
            'product_id' => $params['product_id'],
            'amount' => $params['amount'],
            'gjflex' => $params['gjflex'],
        );
    }

    $profile_fields = fn_get_profile_fields('O');
    fn_fill_address($cart['user_data'], $profile_fields);

    fn_add_product_to_cart($product_data, $cart, $auth);
    fn_calculate_cart_content($cart, $auth, 'A', true, 'F', true);

    $order_id = 0;
    if ($res = fn_checkout_place_order($cart, $auth, array(
        'call_request_order' => true,
        'buffer_cart' => $buffer_cart,
        'buffer_auth' => $buffer_auth
    ))) {
        list($order_id) = $res;
    }

    // Restore cart
    $cart = $buffer_cart;
    $auth = $buffer_auth;

    return $order_id;
}

function fn_call_requests_get_responsibles()
{
    $company_condition = '';
    if ($company_id = Registry::get('runtime.company_id')) {
        $company_condition = db_quote(' AND company_id = ?i', $company_id);
    }

    $items = db_get_hash_single_array(
        "SELECT user_id, CONCAT(lastname, ' ', firstname) as name FROM ?:users WHERE user_type = ?s ?p",
        array('user_id', 'name'), 'A', $company_condition
    );

    return $items;
}

function fn_call_requests_addon_install()
{
    // Order statuses
    $existing_status_id = fn_get_status_id('Y', STATUSES_ORDER);
    if (!$existing_status_id) {
        fn_update_status('', array(
            'status' => 'Y',
            'is_default' => 'Y',
            'description' => __('call_requests.awaiting_call'),
            'email_subj' => __('call_requests.awaiting_call'),
            'email_header' => __('call_requests.awaiting_call'),
            'params' => array(
                'color' => '#cc4125',
                'notify' => 'Y',
                'notify_department' => 'Y',
                'repay' => 'Y',
                'inventory' => 'D',
            ),
        ), STATUSES_ORDER);
    }
}

function fn_settings_variants_addons_call_requests_order_status()
{
    $data = array(
        '' => ' -- '
    );

    foreach (fn_get_statuses(STATUSES_ORDER) as $status) {
        $data[$status['status']] = $status['description'];
    }

    return $data;
}

function fn_call_requests_settings_variants_image_verification_use_for(&$objects)
{
    $objects['call_request'] = __('call_requests.use_for_call_requests');
}

/* Hooks */

function fn_call_requests_init_templater_post(&$view)
{
    $view->addPluginsDir(Registry::get('config.dir.addons') . 'call_requests/functions/smarty_plugins');
}

function fn_call_requests_allow_place_order(&$total, &$cart)
{
    if (!empty($cart['is_call_request'])) {
        // Need to skip shipping
        $cart['shipping_failed'] = false;
        $cart['company_shipping_failed'] = false;
    }
}

function fn_call_requests_place_order(&$order_id, &$action, &$order_status, &$cart, &$auth)
{
    if (!empty($cart['is_call_request'])) {
//        if ($cart['payment_id'] == 13 || ($cart['payment_id'] == 22 && fn_isAL())) {
//            $order_status = 'N';
//        } else {
//            $order_status = Registry::get('addons.call_requests.order_status');
//        }

        $data = array(
            'tag_id' => 251,
            'object_id' => $order_id,
            'type'=> 'O'
        );
        db_query('INSERT INTO ?:sd_search_tag_links ?e',$data);
    }
}

function fn_call_requests_delete_company(&$company_id, &$result)
{
    return db_query("DELETE FROM ?:call_requests WHERE company_id = ?i", $company_id);
}

/**
 * Hook handler. Add json phone masks.
 */
function fn_call_requests_dispatch_before_display()
{
    if (AREA != 'C') {
        return;
    }

    $json = '[]';
    $file = Registry::get('config.dir.root') . '/js/lib/inputmask-multi/phone-codes.json';

    if (file_exists($file)) {
        $json = file_get_contents($file);
    }

    Tygh::$app['view']->assign('call_requests_phone_mask_codes', $json);
}

function fn_call_requests_process_rbko_payment($processor_data, $order_id) {
    $order_info = fn_get_order_info($order_id);

    $processor_data['processor_params'] = unserialize($processor_data['processor_params']);
    $MerchantID = $processor_data['processor_params']['merchant_id'];
    $TerminalID = $processor_data['processor_params']['terminal_id'];
    $signature = $processor_data['processor_params']['signature'];
    $CurrencyID = $processor_data['processor_params']['currency_id'];
    $url = $processor_data['processor_params']['terminal_url'];
    $description = "Pagesa për blerjen e produkteve në Gjirafa50";

    $OrderID = $order_id . "-" . "1" . "-" . $_SESSION['auth']['user_id'];
    $PurchaseTime = date("ymdHis");
    $TotalAmount = bcmul($order_info['total'], 100);
    $data = "$MerchantID;$TerminalID;$PurchaseTime;$OrderID;$CurrencyID;$TotalAmount;;";
    $fp = fopen("./app/payments/$MerchantID.pem", "r");
    $priv_key = fread($fp, 8192);
    fclose($fp);
    $pkeyid = openssl_get_privatekey($priv_key);
    openssl_sign($data, $signature, $pkeyid);
    openssl_free_key($pkeyid);
    $b64sign = base64_encode($signature);

    $_SESSION["DAWH"] = md5($order_id . $TotalAmount . "ARLINDWASHEREANDFUCKUIFUDONTKNOWTHISSTRING!@#$%^");

    echo '<noscript><META HTTP-EQUIV="Refresh" CONTENT="0;URL=/index.php?dispatch=payment_notification.declined&payment=raiffeisenbank"></noscript>
		<form id="s" action="' . $url . '" method="post"><input name="Version" type="hidden" value="1" />
		<input name="MerchantID" type="hidden" value="' . $MerchantID . '" />
		<input name="TerminalID" type="hidden" value="' . $TerminalID . '" />
		<input name="TotalAmount" type="hidden" value="' . $TotalAmount . '" />
		<input name="Currency" type="hidden" value="' . $CurrencyID . '" />
		<input name="locale" type="hidden" value="RU" />
		<input name="PurchaseTime" type="hidden" value="' . $PurchaseTime . '" />
		<input name="OrderID" type="hidden" value="' . $OrderID . '" />
		<input name="PurchaseDesc" type="hidden" value="' . $description . '">
		<input name="Signature" type="hidden" value="' . $b64sign . '"/>
		<input type="submit" style="visibility:hidden" value="Paguaj tani"/></form>
		<style>body {text-align: center;font-family: sans-serif;color:#333;}</style><img src="https://hhstsyoejx.gjirafa.net/gj50/icons/processor-payment-loading.svg" style="display: block;margin: 50px auto;max-width: 50px;
"/>		Duke u lidhur me <strong>Raiffeisen Bank</strong>
<script  data-cfasync="false" type="text/javascript">document.getElementById("s").submit();</script>
		';
    exit;
}

function fn_call_requests_get_payments($product, $blacklisted = false) {
    $is_al = fn_isAL();
    $ks_payment_ids = [
        13 => 13,
        15 => 15,
        16 => 16,
        21 => 21,
        29 => 29,
        31 => 31,
        33 => 33,
        34 => 34
    ];
    $al_payment_ids = [
        22 => 22,
        23 => 23,
        24 => 24,
        25 => 25
    ];

    if ((fn_user_is_blacklisted(Tygh::$app['session']['auth']['user_id']) && Tygh::$app['session']['auth']['user_id'] != 0) || $blacklisted)  {
        unset($ks_payment_ids[15]);
        unset($al_payment_ids[24]);
        Tygh::$app['session']['auth']['blacklisted'] = null;
    }

    $payments = fn_get_payments(array(
        'company_id' => 1,
        'products' => array(
            $product
        ),
        'has_cart_products' => true
    ));

    if ($product['license_key_product']) {
        $ks_payment_ids = [
            13 => 13
        ];
        $al_payment_ids = [
            22 => 22
        ];
    }

    if ($is_al) {
        $payments = array_intersect_key($payments, $al_payment_ids);
    } else {
        $payments = array_intersect_key($payments, $ks_payment_ids);
    }

    foreach ($payments as $key => $payment) {
        $payments[$key]['image'] = fn_get_image_pairs($payment['payment_id'], 'payment', 'M');
    }

    ksort($payments);

    if ($is_al) {
        if ($payments[24]) {
            $move_payment = $payments[24];
            unset($payments[24]);
            array_unshift($payments, $move_payment);
        }
    } else {
        if ($payments[15]) {
            $move_payment = $payments[15];
            unset($payments[15]);
            array_unshift($payments, $move_payment);
        }
    }

    return $payments;
}

function fn_call_requests_get_cities($product) {
    $field_id = fn_isAL() ? 54 : 40;
    return fn_get_profile_fields('B', array(), CART_LANGUAGE, array('field_id' => $field_id));
}

function fn_call_requests_map_profile_fields(&$request) {
    if (fn_isAL()) {
        $request['fields'][55] = $request['fields'][54];
    } else {
        $request['fields'][41] = $request['fields'][40];
    }

    $request['fields'][38] = $request['fields'][37];
}

function fn_call_requests_get_shippings($product) {
    if ($product['license_key_product']) {
        $shippings = null;
    } else {
        $shippings = fn_get_shippings(false, CART_LANGUAGE, Registry::get('runtime.company_id'));

        if ($product['real_amount'] > 0) {
            $key = array_search('1', array_column($shippings, 'shipping_id'));

            if ($key) {
                unset($shippings[$key]);
            }
        } else {
            $key = array_search('7', array_column($shippings, 'shipping_id'));

            if ($key) {
                unset($shippings[$key]);
            }
        }

        $shippings = array_values($shippings);
    }

//    if (in_array($product['product_id'], [140046, 168842])) {
    if (in_array($product['product_id'], [168841, 168842])) {
        $keys = [];
        if (!fn_isAL() && time() >= 1633705200 || fn_isAL()) {
            foreach ($shippings as $shg) {
                if (!in_array($shg['shipping_id'], [9, 14])) {
                    $key = array_search($shg['shipping_id'], array_column($shippings, 'shipping_id'));
                    $keys[] = $key;
                }
            }

            foreach ($keys as $key) {
                unset($shippings[$key]);
            }

            sort($shippings);
        }
    }

    return $shippings;
}

function fn_call_requests_validate_user_data($user_data) {
    $valid = true;

    if (empty($user_data['coupon_code'])) {
        unset($user_data['coupon_code']);
    }

    foreach ($user_data as $data) {
        if (!is_array($data) && empty($data)) {
            $valid = false;
        }
    }

    $cart = array(
        'user_data' => $user_data
    );
    $valid_fields = fn_validate_profile_fields($cart);

    return $valid && $valid_fields;
}

function fn_get_coupon_code_id($coupon_code) {
    return db_get_field('SELECT promotion_id FROM ?:promotions WHERE conditions_hash LIKE ?l', '%' . $coupon_code . '%');
}