<?php
/**
 * Created by PhpStorm.
 * User: ARLIND
 * Date: 05/17/2017
 * Time: 3:43 PM
 */

use FacebookAds\Api;
use FacebookAds\Http\RequestInterface;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

class fb_catalog{

    static $business_id = "240963846323750";
    static $catalog_id = "1558266127518081";
    static $app_id = '436278419872688';
    static $app_secret = '6ae91246735e59061e567325b72c711c';
    static $token = 'EAAGMyvw9W7ABAN73jcjWZAk7ZANZAQkazyHJ0Vkj8UlM8i5OzpetyaJYT2jcYkZAt6gBBLURIitUCzLVoOrP6ZC8nIzf19EHaOvFIQsjZCTAu0vh4fwX564dalASiAYBKq5Nr9x6L8pVY6q0mZCZBnQETRX8GeLyRzUZD';

    function __construct()
    {
        Api::init(self::$app_id, self::$app_secret, self::$token);
    }

    private function fn_get_main_image($product_id){
        $container = intval($product_id / 8000 + 1);
        if ($container > 5)
            $container = 5;

        return "https://hhstsyoejx.gjirafa.net/gj50/img/" . $product_id . "/img/0.jpg";
    }

    private function fn_get_product_link($product_id){
        return "https://gjirafa50.com/index.php?dispatch=products.view%26product_id=" . $product_id;
    }

    private function fn_get_product($product_id){
        $product = db_get_array('SELECT a.product_id, a.status, a.amount, b.product, b.short_description, b.full_description, c.price, d.czc_stock, d.czc_retailer_stock, f.category, h.variant FROM ?:products as a, ?:product_descriptions as b, ?:product_prices as c, ?:product_stock as d, ?:products_categories as e, ?:category_descriptions as f, ?:product_features_values as g, ?:product_feature_variant_descriptions as h WHERE a.product_id = b.product_id AND a.product_id = c.product_id AND a.product_id = d.product_id AND a.status = \'A\' AND (d.czc_stock != 0 OR d.czc_retailer_stock != 0) AND a.product_id = e.product_id AND e.category_id = f.category_id AND e.link_type = \'M\'  AND g.feature_id = 11 AND g.product_id = a.product_id AND g.variant_id = h.variant_id AND a.product_id = ?i', $product_id);

        if(strlen($product['full_description']) > 120)
            $product['full_description'] = substr($product['full_description'], 0, 120) ."...";

        $product['brand'] = $this->fn_get_product_brand($product_id);

        return $product;
    }

    private function fn_get_product_brand($product_id){
        return db_get_array('SELECT a.product_id, b.variant FROM ?:product_features_values as a, ?:product_feature_variant_descriptions as b WHERE a.variant_id = b.variant_id AND a.feature_id = 9 AND a.product_id = ?i', $product_id);
    }


    public function fn_catalog_insert_product($product_id){
        $product = $this->fn_get_product($product_id);

        $obj = array(
            'id' => $product['product_id'],
            'title' => $product['product'],
            'description' => $product['full_description'],
            'link' => $this->fn_get_product_link($product['product_id']),
            'image_link' => $this->fn_get_main_image($product['product_id']),
            'brand' => $this->fn_get_product_brand($product['product_id']),
            'condition' => 'new',
            'availability' => 'in stock',
            'price' => $product['price'],
            'gtin' => $product['variant'],
            'category' => $product['category']
        );

        $this->fn_catalog_call_API($this->fn_catalog_object($product['product_id'], $obj, 'CREATE'));
    }

    public function fn_catalog_delete_product($product_id){

        $this->fn_catalog_call_API($this->fn_catalog_object($product_id, array(), 'DELETE'), RequestInterface::METHOD_DELETE);
    }


    public function fn_catalog_update_stock($product_data){
        $obj = array();

        foreach ($product_data as $product){
            $data = array(
                'availability' => ($product['product_stock']) ? 'in stock' : 'out of stock'
            );
            array_push($obj, $this->fn_catalog_object($product['product_id'], $data, 'UPDATE'));
        }

        return $this->fn_catalog_call_API($obj);
    }


    public function fn_catalog_update_pricing($product_data){
        $obj = array();

        foreach ($product_data as $product){
            $data = array(
                'price' => (int) $product['product_price']
            );
            array_push($obj, $this->fn_catalog_object($product['product_id'], $data, 'UPDATE'));
        }

        return $this->fn_catalog_call_API($obj);
    }

    public function fn_catalog_get_all_products($company_id){

        //        $all_products = db_get_array('SELECT a.product_id, a.status, a.amount, b.product, b.short_description, b.full_description, c.price, d.czc_stock, d.czc_retailer_stock, f.category, h.variant FROM ?:products as a, ?:product_descriptions as b, ?:product_prices as c, ?:product_stock as d, ?:products_categories as e, ?:category_descriptions as f, ?:product_features_values as g, ?:product_feature_variant_descriptions as h WHERE a.product_id = b.product_id AND a.product_id = c.product_id AND a.product_id = d.product_id AND a.status = \'A\' AND (d.czc_stock != 0 OR d.czc_retailer_stock != 0) AND a.product_id = e.product_id AND e.category_id = f.category_id AND e.link_type = \'M\'  AND g.feature_id = 14 AND g.product_id = a.product_id AND g.variant_id = h.variant_id ORDER BY a.product_id');
        //
        $all_products_brands = db_get_array('SELECT a.product_id, b.variant FROM ?:product_features_values as a, ?:product_feature_variant_descriptions as b WHERE a.variant_id = b.variant_id AND a.feature_id = 14');

        $brand_index = array();

        foreach ($all_products_brands as $apb_id){
            $brand_index[$apb_id['product_id']] = $apb_id['variant'];
        }

        // $products = fn_get_external_data('GET','https://frasheri.gjirafa.com:1234/Pricing/GetElasticProducts');
        $products = fn_get_external_data('GET','https://perkthimet.gjirafa.com/Pricing/GetElasticProducts');
        $products_decoded = json_decode($products,true);

        if($company_id == 19) {
            foreach($products_decoded as $key=>$val ){
                $price_al = $products_decoded[$key]['price_al'];
                $products_decoded[$key]['price'] = $price_al;
            }
        }

        return array(
            'products' => $products_decoded,
            'brands' => $brand_index
        );
    }

    public function fn_catalog_get_category_products($category_id,$company_id)
    {
        if($company_id == 19){
            $category_id = db_get_field('select category_ks from ?:category_mappings where category_al = ?i', $category_id);
        }
        $joins = "  inner join ?:product_descriptions on ?:product_descriptions.product_id = ?:products.product_id inner join ?:product_features_values on ?:product_features_values.product_id = ?:products.product_id inner join ?:product_feature_variant_descriptions on ?:product_features_values.variant_id = ?:product_feature_variant_descriptions.variant_id ";
        $elastic = new ElasticSearch();
        $key = true;
        $obj = array(
            'where' =>
                array(
                    array(
                        'field' => 'features.14',
                    ),
                    array(
                        'field' => 'category_ids',
                        'value' => $category_id,
                    ),

                ),
            'offset' => 0,
            'limit' => (isset($_GET['limit'])) ? (int)$_GET['limit'] : 1000,
            'filters' => false,
            'search'=> null,
            'store' => 'ks',
        );
        $products_array = [];
        while($key){
            // Do stuff
            $products_elastic = $elastic->sendObject($obj, array());
            $obj['offset'] = $obj['offset'] + $obj['limit'];
            array_push($products_array,$products_elastic[0]);

            if($products_elastic[0] == false) $key = false;
        }

        foreach ($products_array as $product_array) {
            foreach($product_array as $key=>$value){
                $prod_ids [] = $value['product_id'];
            }
        }
        $comma_separated = implode(",", $prod_ids);
        $variant_and_desc = db_get_array('select ?:products.product_id,variant, full_description  from ?:products '.$joins.' where feature_id = 14 and ?:products.product_id IN('.$comma_separated.')');

        foreach ($products_array as $product_array) {
            foreach($product_array as $key => $val) {
                $found_key = array_search($key, array_column($variant_and_desc, 'product_id'));
                if($found_key) {
                    $products[] = array(
                        'product_id' => $val['product_id'],
                        'product' => $val['product'],
                        'full_description' =>  $variant_and_desc[$found_key]['full_description'],
                        'price' => ($company_id == 19 ? $val['price_al'] : $val['price']),
                        'old_price' => ($company_id == 19 ? $val['old_price_al'] : $val['old_price']),
                        'variant' => $variant_and_desc[$found_key]['variant'],
                        'stock' => $val['amount'] + $val['czc_stock'] + $val['czc_retailer_stock'],
                        'amount' => $val['amount']
                    );
                }
            }
        }

        return $products;
    }



    private function fn_catalog_object($product_id, $product_data, $method){
        return array(
            'access_token' => self::$token,
            'method' => $method,
            'retailer_id' => $product_id,
            'data' => $product_data
        );
    }


    public function fn_catalog_call_API($product_data, $request_method = RequestInterface::METHOD_POST){

        try{
            $data = Api::instance()->call(
                '/' . self::$catalog_id . '/batch',
                $request_method,
                array('requests' => $product_data))->getContent();
        }catch (Exception $e){
            $data = false;
        }

        return $data;
    }

    public function fn_get_currency_exchange_rate($from, $to, $amount = 1){
        $url  = "http://finance.yahoo.com/d/quotes.csv?s=" . $from . $to . "=X&f=l1";
        $data = file_get_contents($url);
        $exchange_rate = (double) $data;

        if($amount != 1)
            return $amount * $data;

        return $exchange_rate;
        //return round($exchange_rate, 2);
    }

}


