<?php

use Tygh\Registry;

if (!defined('BOOTSTRAP')) {
    die('Access denied');
}

class elasticSearch
{

    static $price_filter_id = 961;
    static $is_al = false;

    public function __construct() {
        self::$is_al = fn_isAL();
    }

    public function search($params)
    {

        $obj = $this->params($params);

        $params['price_params'] = array(
            'min' => 0,
            'max' => 100000000,
            'round_to' => '0',
            'left' => '',
            'right' => '',
            'filter_uid' => self::$price_filter_id,
            'prefix' => '€',
            'display_count' => '10',
            'extra' => 'EUR',
            'slider' => true,
            'disable_slider' => false
        );

        return $this->sendObject($obj, $params);
    }


    public function categoryMap($category_id)
    {


        if (apcu_exists('categories_mapping')) {
            $category = apcu_fetch('categories_mapping');
        } else {
            $category = array_reduce(db_get_array('select category_ks, category_al from gjshop_category_mappings'), function ($current, $item) {
                $current[$item['category_al']] = $item['category_ks'];
                return $current;
            }, []);

            apcu_store('categories_mapping', $category, 0);
        }
        if (!isset($category[$category_id]))
            return $category_id;

        return $category[$category_id];
    }

    public function params($params)
    {
        $query = "";
        if (isset($params['q'])) {
            $query = $params['q'];
        }
        if (!empty($params['category_id']) || !empty($params['category_ids'])) {
            $category = ($params['category_id']) ? $params['category_id'] : $params['category_ids'];
            if (self::$is_al) {
                $category = $this->categoryMap($category);
            }
        }
        if (isset($_GET['filter'])) {
            $filter = $_GET['filter'];
        }
        if (!empty($params['features_hash']))
            $features = $this->parseFeatures($params['features_hash']);


        if (!empty($features[1])) {
            $price = $features[1];
            unset($features[1]);
        }

        $where_array = array();
        if (isset($category)) {
            array_push($where_array, array(
                'field' => 'category_ids',
                'value' => $category
            ));
        }

        if (!empty($features)) {

            foreach ($features as $field => $value) {
                if ($field == self::$price_filter_id) {
                    array_push($where_array, array(
                        'field' => (self::$is_al ? 'price_al' : 'price'),
                        'range' => array(
                            'gte' => $value[0],
                            'lte' => str_replace('EUR', '', $value[1])
                        )
                    ));
                } else {
                    array_push($where_array, array(
                        'field' => 'features.' . $field,
                        'values' => $value
                    ));
                }
            }
        }

        $limit = 24;
        if (isset($params['items_per_page']))
            $limit = $params['items_per_page'];
        else if (isset($params['limit']))
            $limit = $params['limit'];

        $offset = 0;

        if (isset($_GET['page'])) {
            $offset = ($_GET['page'] - 1) * $limit;
        } else if (isset($_GET['offset'])) {
            $offset = $_GET['offset'];
        } else if (isset($params['page']))
            $offset = ($params['page'] - 1) * $limit;
        else if (isset($params['offset']))
            $offset = $params['offset'];

        $sort = '';
        if (isset($params["sort_by"]) && isset($params["sort_order"]) && $params["sort_by"] != 'popularity') {
            $sort = array(
                'field' => $params["sort_by"],
                'order' => $params["sort_order"]
            );
        }

        $filters = (!empty($params['filters'])) ? true : false;


        $obj = array(
            'where' => $where_array,
            'search' => $query,
            'offset' => $offset,
            'limit' => $limit,
            'sort' => $sort,
            'filters' => $filters,
            //            'development' => true,
            'store' => (self::$is_al) ? "al" : "ks"
        );

        return $obj;
    }

    public function parseFeatures($features)
    {
        $pfeatures = array();
        $p = explode("_", $features);
        for ($i = 0; $i < count($p); $i++) {
            $f = explode("-", $p[$i]);
            $a = array();
            for ($j = 1; $j < count($f); $j++) {
                array_push($a, $f[$j]);
            }

            $pfeatures[$f[0]] = $a;
        }
        return $pfeatures;
    }

    public function get_dmp_products($params)
    {

        $product_ids = json_decode(file_get_contents(Registry::get('config.dmp_url') . "/api/recommendgj50/" . $params['gjci'] . "?count=" . $params['limit']));

        $obj = array('where' => [array("field" => "product_id", "values" => $product_ids)], 'filters' => false, 'limit' => $params['limit']);

        return $this->sendObject($obj, array())[0];
    }

    public function sendObject($obj, $params = array(), $action = 'SearchGj', $homepage = false)
    {
        $fo_key = md5(json_encode($obj));
        $instant_delivery = false;
        $price_filter = fn_get_string_between($params['features_hash'], '961', 'EUR');
        $price_filter = array_values(array_filter(explode('-', $price_filter)));
        $limited_to_store_products = fn_get_limited_to_store_products(self::$is_al);

        if (!empty($price_filter) && $price_filter[0] != 1 && $price_filter[1] != 100000000) {
            $price_filter_min = $price_filter[0];
            $price_filter_max = $price_filter[1];
        }

        try {
            // $url = Registry::get('config.elastic_url') . '/SearchService.svc/json/' . $action . '/5CF5316C562BB9368DD857211572C';

            $es_test_urls = Registry::get('config.elastic_services');

            $es_url = $es_test_urls[0];

            if ($params['es_service_index']) {
                $es_url = $es_test_urls[$params['es_service_index']];
                unset($params['es_service_index']);
            }

            $url = $es_url . '/SearchService.svc/json/' . $action . '/5CF5316C562BB9368DD857211572C';

            $result = $this->getContent($url, json_encode((object) $obj));

            if ($result === FALSE) {
                if (apcu_exists('elastic_failover_' . $fo_key))
                    return apcu_fetch('elastic_failover_' . $fo_key);
                return false;
            }

            $res = json_decode(substr($result, 3), true);

            foreach ($obj['where'] as $condition) {
                if ($condition['field'] == 'features.2041' && in_array('135554', $condition['values'])) {
                    $instant_delivery = true;
                }
            }

            $products = array();
            $product_ids = array_column($res['hits'], 'product_id');
//            $get_stock_from_db = in_array($params['dispatch'], ['categories.view', 'products.search']) || $params['recommended'] == true || $homepage;
            $get_stock_from_db = $params['recommended'] == true;

            if ($get_stock_from_db) {
                if (!self::$is_al) {
                    $products_stock = db_get_array('SELECT p.product_id, (p.amount + ps.czc_stock + ps.czc_retailer_stock) AS amount FROM gjshop_products AS p INNER JOIN gjshop_product_stock AS ps ON ps.product_id = p.product_id WHERE p.product_id IN (?n)', $product_ids);
                } else {
                    $products_stock = db_get_array('SELECT product_id, (al_stock + czc_stock + czc_retailer_stock) AS amount FROM gjshop_product_stock WHERE product_id IN (?n)', $product_ids);
                }
            }

            foreach ($res["hits"] as $item) {
                if ($instant_delivery && !($item['features']['2041'] == '135554')) {
                    $res['total_count'] -= 1;
                    continue;
                }

                if (in_array($item['product_id'], $limited_to_store_products)) {
                    $res['total_count'] -= 1;

                    if ($res['total_count'] == 0) {
                        unset($res['feature_filters']);
                    }

                    continue;
                }

                if (self::$is_al) {
                    $item['price'] = $item['price_al'];
                    $item['total_stock'] = (empty($item['amount_al']) ? 0 : $item['amount_al']) + (empty($item['czc_stock']) ? 0 : $item['czc_stock']) + (empty($item['czc_retailer_stock']) ? 0 : $item['czc_retailer_stock']);
                } else {
                    $item['total_stock'] = (empty($item['amount']) ? 0 : $item['amount']) + (empty($item['czc_stock']) ? 0 : $item['czc_stock']) + (empty($item['czc_retailer_stock']) ? 0 : $item['czc_retailer_stock']);
                }

                if ($homepage) {
                    if ($item['total_stock'] == 0) {
                        $res['total_count'] -= 1;
                        continue;
                    } else {
                        $products[$item['product_id']] = $item;
                    }
                } else {
                    $products[$item['product_id']] = $item;
                }

                if ($get_stock_from_db) {
                    $stock_key = array_search($item['product_id'], array_column($products_stock, 'product_id'));
                    if ($stock_key) {
                        if ($products_stock[$stock_key]['amount'] == 0) {
                            continue;
                        }

                        $products[$item['product_id']]['amount'] = $products_stock[$stock_key]['amount'];
                    }
                }

                //$products[$item['product_id']]['amount'] = $item['stock'] + $item['czc_stock'] + $item['czc_retailer_stock'];
            }

            $params['items_per_page'] = 48;
            $params['sort_by'] = 'popularity';
            $params['sort_order'] = 'desc';
            $params['total_items'] = $res["total_count"];

            if (!isset($params['page']))
                $params['page'] = 1;

            if (isset($_GET["items_per_page"]))
                $params['items_per_page'] = $_GET["items_per_page"];

            if (isset($_GET["sort_by"]))
                $params['sort_by'] = $_GET["sort_by"];

            if (isset($_GET["sort_order"]))
                $params['sort_order'] = $_GET["sort_order"];

            if ($obj['filters'] && !empty($res["feature_filters"]))
                $params['feature_filters'] = $this->getFilterNames($res["feature_filters"]);

            if ($price_filter_min && $price_filter_max) {
                $params['price_params']['min'] = $price_filter_min;
                $params['price_params']['max'] = $price_filter_max;
            } else {
                $params['price_params']['min'] = $res["price_filters"]["min_value"];
                $params['price_params']['max'] = $res["price_filters"]["max_value"];
            }

            $params['subcategory_filters'] = $this->get_subcategories_elastic($res['subcategory_filters']);
            fn_set_hook('get_products_elastic_post', $products);

            if (!apcu_exists('elastic_failover_' . $fo_key))
                apcu_store('elastic_failover_' . $fo_key, array($products, $params), 2700);

            $params['rand_elastic_url'] = $es_url;

            return array($products, $params);
        } catch (Exception $e) {
            if (apcu_exists('elastic_failover_' . $fo_key))
                return apcu_fetch('elastic_failover_' . $fo_key);
            else
                echo '';
        }
    }

    public function notifyElasticProductsUpdate($product_ids, $mode)
    {
        // 1 - Insert
        // 2 - Update
        // 3 - Delete
        if (DEVELOPMENT || ENVIRONMENT != 'live')
            return false;

        $elastic_method = 'UpdateProduct';
        if ($mode == 4)
            $elastic_method = 'UpdatePopularity';

        $obj = array(
            'product_ids' => $product_ids,
            'mode' => $mode
        );
        try {
            // $url = Registry::get('config.elastic_url') . '/SearchService.svc/json/' . $elastic_method . '/5CF5316C562BB9368DD857211572C';

            foreach (Registry::get('config.elastic_services') as $elastic_service) {
                $url = $elastic_service . '/SearchService.svc/json/' . $elastic_method . '/5CF5316C562BB9368DD857211572C';
                $result = $this->getContent($url, json_encode((object) $obj));

                if ($result === FALSE) {
                    if (AREA == 'A') {
                        fn_set_notification('E', __('error'), 'Ka ndodhur një gabim me sinkronizim me ElasticSearch');
                    }

                    return false;
                }
            }

            $notify = 'është fshirë nga';
            if ($mode == 2)
                $notify = 'është bërë update në';
            else if ($mode == 1)
                $notify = 'është insertuar në';

            if (AREA == 'A')
                fn_set_notification('N', __('notice'), 'Produkti ' . $notify . ' ElasticSearch');
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function searchAutoComplete($q)
    {
        $obj = array(
            'prefix' => $q,
            'store' => (self::$is_al) ? "al" : "ks"
        );
        try {
            $url = Registry::get('config.elastic_url') . '/SearchService.svc/json/Autocomplete/5CF5316C562BB9368DD857211572C';
            $result = $this->getContent($url, json_encode((object) $obj));

            if ($result === FALSE) {
                echo 'Error: ';
                return;
            }

            return $result;
        } catch (Exception $e) {
            echo 'Caught exception: ', $e->getMessage(), "\n";
        }
    }

    public function didYouMean($q)
    {
        $obj = array(
            'text' => $q
        );
        try {
            $url = Registry::get('config.elastic_url') . '/SearchService.svc/json/Didyoumean/5CF5316C562BB9368DD857211572C';
            $result = $this->getContent($url, json_encode((object) $obj));

            if ($result === FALSE) {
                echo 'Error: ';
                return;
            }

            return str_replace('"', "", $result);
        } catch (Exception $e) {
            echo 'Caught exception: ', $e->getMessage(), "\n";
        }
    }

    public function getFilterNames($elastic_fid)
    {
        $cache_md5 = md5(json_encode($elastic_fid));
        if (apcu_exists('filter_names_' . $cache_md5))
            return apcu_fetch('filter_names_' . $cache_md5);

        try {
            $filter_id = array();
            foreach ($elastic_fid as $element => $value) {
                $filter_id = array_merge($filter_id, $value);
            }

            $filter_value_ids = array_unique($filter_id);

            $string = join(',', $filter_value_ids);
            $get_features = db_get_array("SELECT variant_id, variant  FROM ?:product_feature_variant_descriptions WHERE variant_id IN ($string)");

            $features_dictionary = array();
            foreach ($get_features as $feature) {
                $features_dictionary[$feature['variant_id']] = $feature['variant'];
            }

            $filter_ids_list = array_keys($elastic_fid);
            $get_filters = db_get_array('SELECT filter_id, filter  FROM ?:product_filter_descriptions WHERE filter_id IN (?n)', $filter_ids_list);

            $filters_dictionary = array();
            foreach ($get_filters as $filter) {
                $filters_dictionary[$filter['filter_id']] = $filter['filter'];
            }

            $elastic_filters = array();
            foreach ($elastic_fid as $fid => $list) {
                if (empty($filters_dictionary[$fid]))
                    continue;
                $key = $fid . '§§' . $filters_dictionary[$fid];
                $values_array = array();
                foreach ($list as $key6 => $fid_value) {
                    if (isset($features_dictionary[$fid_value])) {
                        $key2 = $fid_value . '§§' . $features_dictionary[$fid_value];
                        array_push($values_array, $key2);
                    }
                }
                $elastic_filters[$key] = $values_array;
                if (self::$is_al) {
                    unset($filters_dictionary[2041]);
                }
            }
            apcu_store('filter_names_' . $cache_md5, $elastic_filters, 0);

            return $elastic_filters;
        } catch (Exception $e) {
            apcu_delete('filter_names_' . $cache_md5);
            return array();
        }
    }

    public function getContent($url, $params, $key = "")
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-type: application/json',
            'Connection: close',
            'session_id: ' . session_id(),
            'GjKey:' . (!empty($key) ? $key : "")
        ));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.1.4322)');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        $data = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        return ($httpcode >= 200 && $httpcode < 300) ? $data : false;
    }

    public function get_subcategories_elastic($sub_categories)
    {
        if ($sub_categories) {
            foreach ($sub_categories as $sub) {
                $sub_id = $sub['id'];
                $sub_counts[] = $sub['count'];
                $sub_ids[] = "'$sub_id'";
            }
            $imploded_subs = implode(",", $sub_ids);
            $sub_obj = db_get_array("select category_id , category from ?:category_descriptions where category_id IN($imploded_subs)");

            foreach ($sub_obj as $key => $value) {
                foreach ($sub_counts as $count_key => $count_value) {
                    $sub_obj[$key]['count'] = $sub_counts[$key];
                }
            }
        }
        return $sub_obj;
    }
}
