<?php

if (!defined('BOOTSTRAP')) { die('Access denied'); }

function fn_get_csv_data($file, $params) {
	return array_map(function ($product) use ($file, $params) {
        foreach ($params as $key => $value) {
            $data[$value] = $product[$key];
        }

        return $data;
    }, array_map('str_getcsv', file($file)));
}

function fn_filter_csv_products($products, $category_id) {
    $filtered_products = array_filter($products, function ($csv_product) use ($category_id) {
        return ($csv_product['category'] == $category_id);
    });

    foreach ($filtered_products as $filtered_product) {
        $filtered_product_codes[] = $filtered_product['product_code'];
    }

    return $product_codes = $filtered_product_codes;
}

function fn_filter_csv_products_by($products, $filter_id, $filter) {
    $filtered_products = array_filter($products, function ($csv_product) use ($filter_id, $filter) {
        return ($csv_product[$filter] == $filter_id);
    });

    foreach ($filtered_products as $filtered_product) {
        $filtered_product_codes[] = $filtered_product['product_code'];
    }

    return $product_codes = $filtered_product_codes;
}

function fn_get_csv_products($products, $multi = false) {
    $priority_products = array_filter($products, function ($csv_product) {
        return ($csv_product['priority'] == '1');
    });

    foreach ($priority_products as $priority_product) {
        $priority_product_codes[] = $priority_product['product_code'];
    }

    foreach ($products as $csv_product) {
        $product_codes[] = $csv_product['product_code'];
    }

    if ($multi) {
        $priority_products_2 = array_filter($products, function ($csv_product) {
            return ($csv_product['priority'] == '2');
        });

        foreach ($priority_products_2 as $priority_product) {
            $priority_product_codes_2[] = $priority_product['product_code'];
        }

        return array($product_codes, $priority_product_codes, $priority_product_codes_2);
    }

    return array($product_codes, $priority_product_codes);
}

function fn_extract_product_data($elastic_products, $priority_product_codes = NULL, $auth) {
    foreach ($elastic_products[0] as $product) {
        if (is_null($product['price'])) {
            continue;
        }

        if (isset($priority_product_codes) && in_array($product['product_code'], $priority_product_codes)) {
            $product_data = fn_get_product_data(
                $product['product_id'], $auth, CART_LANGUAGE, '', true, true, true, true, fn_is_preview_action($auth, $_REQUEST), true, false, true
            );

            foreach ($product_data['header_features'] as $feature) {
                $features[$feature['description']] = $feature['variant'];
            }

            $product['features'] = $features;
            $product['short_description'] = $product_data['short_description'];
        }

        $products[] = $product;
    }

    return $products;
}

function fn_query_elastic($filter, $params, $limit, $offset, $filter_by = 'value', $search = null, $es_service_index = null) {
    $obj = array(
        'where' => [array('field' => $filter, $filter_by => $params)],
        'search' => $search,
        'offset' => $offset,
        'limit' => $limit,
        'filters' => false,
    );
    $search_params = array();

    if ($es_service_index) {
        $search_params['es_service_index'] = $es_service_index;
    }

    $elastic = new elasticSearch();
    $elastic_products = $elastic->sendObject($obj, $search_params);

    return $elastic_products;
}

function fn_get_section_products($section) {
    if ($section == 'msi') {
        $product_codes = array_map(function ($product) {
            return $product['0'];
        }, array_map('str_getcsv', file('feeds/msi-landing.csv')));

        $limit = count($product_codes);

        $elastic_products = fn_query_elastic('product_code', $product_codes, $limit, 0, 'values');

        if (fn_isAL()) {
            foreach ($elastic_products[0] as $elastic_product) {
                if ($elastic_product['price_al']) {
                    $elastic_products_al[] = $elastic_product;
                }
            }

            $products = array_slice($elastic_products_al, rand(0, count($elastic_products_al) - 23), 22);
        } else {
            $products = array_slice($elastic_products[0], rand(0, count($elastic_products[0]) - 23), 22);
        }

        return $products;
    } elseif ($section == 'recommended') {
        $cat_id = '2161';

        if (fn_isAL()) {
            $cat_id = '3233';
        }

        $elastic_products = fn_query_elastic('category_ids', $cat_id, 23, 0);

        return $elastic_products[0];
    }
}

function fn_parse_category_id($categories, $category) {
    $category_id;

    foreach ($categories as $key => $value) {
        if ($category == $value) {
            $category_id = $key;
        }
    }

    return $category_id;
}

function fn_map_categories($categories) {
    $category_names = array_values($categories);

    $categories_map = array();

    foreach ($category_names as $category_name) {
        $category_name = strtolower(str_replace(array('ë', ' ', ','), array('e', '-', ''), $category_name));
        $categories_map[] = $category_name;
    }

    $categories_seo_map = array_combine(array_keys($categories), $categories_map);

    $categories = array_combine($categories_map, $categories);

    return array($categories_seo_map, $categories);
}

function fn_get_category_names($category_ids) {
    return db_get_fields('SELECT category FROM ?:category_descriptions WHERE category_id IN (?a)', $category_ids);
}

function fn_get_brand_names($variant_ids) {
    $brands = db_get_fields('SELECT variant FROM ?:product_feature_variant_descriptions WHERE variant_id IN (?a)', $variant_ids);

    return array_replace($brands,
        array_fill_keys(
            array_keys($brands, 'Transparent'),
            'Të tjera'
        )
    );
}

function fn_add_sort_key($elastic_products, $key1, $key2) {
    foreach ($elastic_products[0] as $product) {
        $product[$key1] = $product['category_ids'][0];
        $product[$key2] = $product['features'][12];

        $products[] = $product;
    }

    return $products;
}

function fn_pop_elements($products, $limit) {
    foreach ($products as $key => $value) {
        if (count($value) < $limit) {
            unset($products[$key]);
        }
    }

    return $products;
}

function fn_get_product_features_limited($features_map) {
    $feature_ids = array_keys($features_map);
    $variant_ids = array_values($features_map);

    $features = db_get_fields('SELECT description FROM ?:product_features_descriptions WHERE feature_id IN (?a) LIMIT 2', $feature_ids);
    $variants = db_get_fields('SELECT variant FROM ?:product_feature_variant_descriptions WHERE variant_id IN (?a) LIMIT 2', $variant_ids);

    $data = array_combine($features, $variants);

    return array($data);
}

function fn_array_get_random($array, $random_elements) {
    $grouped_array = array();

    $grouped_array[] = array_rand($array, $random_elements);

    foreach ($grouped_array as $item) {
        foreach ($item as $key => $value) {
            $suki[] = $array[$value];
        }
    }

    return $suki;
}

function fn_array_get_random_multi($array, $random_elements) {
    $grouped_array = array();

    foreach ($array as $key => $element) {
        $grouped_array[] = array($key => array_rand($element, $random_elements));
    }

    return $grouped_array;
}

function fn_get_slider_products($slider_random_products, $products_by_brand) {
    $finale = array();

    foreach ($slider_random_products as $srp) {
        foreach ($srp as $sister) {
            foreach ($sister as $qm) {
                foreach (array_keys($srp) as $key => $value) {
                    $nk = $value;
                }

                $finale[] = array(
                    $nk => $products_by_brand[$nk][$qm]['product_code']
                );
            }
        }
    }

    return $finale;
}

function fn_array_remove_first_level($array) {
    $processed = array();

    foreach($array as $subarray) {
        foreach($subarray as $id => $value) {
            if(!isset($processed[$id])) {
                $processed[$id] = array();
            }

            $processed[$id][] = $value;
        }
    }

    return $processed;
}

function fn_prepare_slider_products($slider_random_products) {
    $slider_groups = array();

    foreach ($slider_random_products as $srp_brand => $srp_product_codes) {
        $elastic_products = fn_query_elastic('product_code', $srp_product_codes, (isset($_GET['limit'])) ? (int)$_GET['limit'] : 1000, (isset($_GET['offset'])) ? (int)$_GET['offset'] : 0, 'values');

        $slider_groups[] = array(
            $srp_brand => $elastic_products[0]
        );
    }

    return $slider_groups;
}

function fn_get_recently_viewed_products() {
    $products = fn_query_elastic('product_code',
        Tygh::$app['session']['recently_viewed_products'],
        count(Tygh::$app['session']['recently_viewed_products']),
        0,
        'values'
    );

    return $products[0];
}