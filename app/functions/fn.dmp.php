<?php

use Tygh\Registry;

function fn_dmp_promotion_key($promotion_data)
{
    return substr(md5(json_encode($promotion_data)), 0, 12);
}

function fn_dmp_promotion_check()
{

    $promotion_data = fn_dmp_decrypt_promotion_data($_GET['dsch']);

    if (!is_array($promotion_data))
        return false;

    $is_used = db_get_field('SELECT count(*) from ?:promotions_dmp where adnetwork_id = ?s and product_id = ?i and discount = ?i', $promotion_data['a'], $promotion_data['p'], $promotion_data['d']);
    if ($is_used || !is_null($_SESSION['dmp_promotions'][fn_dmp_promotion_key($_GET['dsch'])]))
        return false;
    else if(is_array($promotion_data)) {
        db_query("INSERT INTO ?:promotions_dmp ?e", array(
            'user_id' => ($_SESSION['auth']['user_id'] == 0) ? NULL : $_SESSION['auth']['user_id'],
            'session_id' => session_id(),
            'adnetwork_id' => $promotion_data['a'],
            'product_id' => $promotion_data['p'],
            'discount' => $promotion_data['d'],
            'created_at' => $promotion_data['f'],
            'expires_at' => $promotion_data['t']
        ));
        setcookie("sid_customer_l", session_id(), time() + 3600, '/', NULL, 0 );
    }

    return true;
}

function fn_dmp_promotion_generate($product_id = null, $discount = null, $from_date = null, $to_date = null)
{
    if($product_id == null && $discount == null && $from_date == null && $to_date == null){
        $promotion_data = fn_dmp_decrypt_promotion_data($_GET['dsch']);
        if(!is_array($promotion_data))
            return false;
        $product_id = $promotion_data['p'];
        $discount = $promotion_data['d'];
        $from_date = (int) $promotion_data['f'];
        $to_date = (int) $promotion_data['t'];
    }

    return array(
        'promotion_id' => 0,
        'company_id' => Registry::get('runtime.company_id'),
        'conditions' =>
            array(
                'set' => 'all',
                'set_value' => '1',
                'conditions' =>
                    array(
                        1 =>
                            array(
                                'operator' => 'in',
                                'condition' => 'products',
                                'value' => $product_id,
                            )
                    ),
            ),
        'bonuses' =>
            array(
                1 =>
                    array(
                        'bonus' => 'product_discount',
                        'discount_bonus' => 'by_percentage',
                        'discount_value' => $discount,
                    ),
            ),
        'to_date' => $to_date,
        'from_date' => $from_date,
        'priority' => '0',
        'stop' => 'N',
        'zone' => 'catalog',
        'conditions_hash' => 'products=' . $product_id . ';',
        'status' => 'A',
        'number_of_usages' => '1',
        'users_conditions_hash' => '',
        'name' => 'Zbritje vetem per ju!',
        'detailed_description' => '',
        'short_description' => '',
    );
}

function fn_dmp_decrypt_promotion_data($enc)
{
    $decode_params = fn_dmp_decryptRJ256("E4mxEPl6NCOJn5K7qsP6vhwk85x2gE+tQOa3w869Ca0=", "fwvqHQ6QMj9vRiwncwCavA1Vys2teLYAA3s6GiQXFf0=", $enc);
    parse_str(parse_url("?" . $decode_params, PHP_URL_QUERY), $obj);

    return $obj;
}

function fn_dmp_decryptRJ256($key, $iv, $encrypted)
{
    $encrypted = str_replace(' ', '+', $encrypted);
    $key = base64_decode($key);
    $iv = base64_decode($iv);
    $encrypted = base64_decode($encrypted);
    $rtn = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $encrypted, MCRYPT_MODE_CBC, $iv);
    $rtn = fn_dmp_unpad($rtn);
    return ($rtn);
}

function fn_dmp_unpad($value)
{
    $blockSize = mcrypt_get_block_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC);
    $packing = ord($value[strlen($value) - 1]);
    if ($packing && $packing < $blockSize) {
        for ($P = strlen($value) - 1; $P >= strlen($value) - $packing; $P--) {
            if (ord($value{$P}) != $packing) {
                $packing = 0;
            }
        }
    }

    return substr($value, 0, strlen($value) - $packing);
}

function fn_dmp_init_promotions_data($user_id)
{
    $promotions = db_get_array("SELECT product_id, discount FROM ?:promotions_dmp WHERE user_id = ?i", (int) $user_id);
        $return = array();
        foreach ($promotions as $promotion) {
            array_push($return, fn_dmp_promotion_generate($promotion['product_id'], $promotion['discount'], $promotion['created_at'], $promotion['expires_at']));
        }
    return $return;
}

function fn_dmp_update_promotions_without_user_id($user_id){
    return db_query("UPDATE ?:promotions_dmp SET user_id = ?i WHERE session_id = ?i", $user_id, $_COOKIE['sid_customer_l']);
}

function fn_get_dmp_products($from, $to) {
    $elastic = new elasticSearch();
    $obj = array('where' => [array("field" => "product_id", "values" => range($from, $to))], 'filters' => false, 'limit' => ($to - $from));
    $products = $elastic->sendObject($obj, array());

    $dmp_products = array();

    foreach ($products[0] as $p) {
        $img = smarty_function_get_images_from_blob(array(
            'product_id' => $p['product_id'],
            'count' => 1
        ));

        $dmp_products[] = array(
            "id" => $p['product_id'],
            "name" => $p['product'],
            "pictureUrl" => $img,
            "stockQuantity" => ($p['stock'] != null) ? $p['stock'] : 0,
            "czcStockQuantity" => ($p['czc_stock'] != null) ? $p['czc_stock'] : 0,
            "czcSupplierStockQuantity" => ($p['czc_retailer_stock'] != null) ? $p['czc_retailer_stock'] : 0,
            "oldPrice" => ($p['old_price'] != null) ? $p['old_price'] : 0,
            "price" => $p['price'],
            "priceAl" => $p['price_al'],
            "categoryIds" => $p['category_ids']
        );
    }

    return $dmp_products;
}