<?php

use Tygh\Registry;

function fn_register_user($request = array()) {
    try {
        if (count($request) >= 4 && check_user_data_fields($request)) {
            $user_data['is_root'] = 'N';
            $user_data['salt'] = fn_generate_salt();
            $user_data['password'] = fn_generate_salted_password($request[1], $user_data['salt']);
            $user_data['lang_code'] = !empty($user_data['lang_code']) ? $user_data['lang_code'] : CART_LANGUAGE;
            $user_data['timestamp'] = TIME;
            $user_data['company_id'] = 1;
            $user_data['email'] = $request[0];
            $user_data['firstname'] = $request[2];
            $user_data['lastname'] = $request[3];

            $user_id = db_query("INSERT INTO ?:users ?e", $user_data);

            if (empty($user_data['user_login'])) { // if we're using email as login or user type does not require login, fill login field
                db_query("UPDATE ?:users SET user_login = 'user_?i' WHERE user_id = ?i AND user_login = ''", $user_id, $user_id);
            }

            db_query("INSERT INTO ?:user_profiles (user_id, profile_type) values ('$user_id','P')");

            if (!empty(Tygh::$app['session']['cart'])) {
                fn_save_cart_content(Tygh::$app['session']['cart'], $user_id);
            }
        } else {
            $user_id = "";
        }
    } catch (Exception $e) {
        $user_id = "";
    }

    return $user_id;
}

function check_user_data_fields($params){
    for ($i = 0; $i < count($params); $i++) {
        if ($params[$i] == null && $i != 4) {
            return false;
        }
    }

    return true;
}