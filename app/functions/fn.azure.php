<?php

use MicrosoftAzure\Storage\Blob\BlobRestProxy;
use MicrosoftAzure\Storage\Common\Exceptions\ServiceException;
use MicrosoftAzure\Storage\Blob\Models\ListBlobsOptions;
use MicrosoftAzure\Storage\Blob\Models\CreateContainerOptions;
use MicrosoftAzure\Storage\Blob\Models\PublicAccessType;
use MicrosoftAzure\Storage\Blob\Models\SetBlobPropertiesOptions;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

define('AZURE_CONNECTION', 'DefaultEndpointsProtocol=https;AccountName=gjirafa;AccountKey=NaqSLMHF6WN0LiydX7a81ECdBWQ5UeN9urLDf8ta2nlvpHznDdaLJx1iSK0tbW0rjnaYupa86o5M68weNzfSIQ');

function fn_get_blob_url($container, $folder, $file) {
	return 'https://gjirafa.blob.core.windows.net/' . $container . '/' . $folder . '/' . $file;

	//GET BASE64 FILE
	// // Connect to blob client
	// $blobClient = BlobRestProxy::createBlobService(AZURE_CONNECTION);

	// // Get blob
	// echo "This is the content of the blob uploaded: ";
	// $blob = $blobClient->getBlob($container, $file);
	// fpassthru($blob->getContentStream());
	// echo "<br />";
}

function fn_upload_blob($container, $folder, $file, $filename, $properties = false) {
	// Connect to blob client
	$blobClient = BlobRestProxy::createBlobService(AZURE_CONNECTION);

    try {
        $content = fopen($file, "rb");
        $size = filesize($file);

        $blobClient->createBlockBlob($container, $folder . '\\' . $filename, $content);
        
        if ($properties) {
            $opts = new SetBlobPropertiesOptions();
            $opts->setCacheControl('max-age=604800');

            $blobClient->setBlobProperties($container . '\\' . $folder, $filename, $opts);
        }

    } catch(ServiceException $e) {
        $code = $e->getCode();
        $error_message = $e->getMessage();
        echo $code.": ".$error_message."<br />";
    } catch(InvalidArgumentTypeException $e) {
        $code = $e->getCode();
        $error_message = $e->getMessage();
        echo $code.": ".$error_message."<br />";
    }
}

function fn_delete_blob() {
	// Connect to blob client
	$blobClient = BlobRestProxy::createBlobService(AZURE_CONNECTION);

	$blobClient->deleteBlob($container, 'C:/Users/taula/Desktop/storage-blobs-php-quickstart-master/HelloWorld.txt');
}

function fn_list_blobs() {
	// Connect to blob client
	$blobClient = BlobRestProxy::createBlobService(AZURE_CONNECTION);

	// List blobs.
    $listBlobsOptions = new ListBlobsOptions();
    // $listBlobsOptions->setPrefix("HelloWorld");

    echo "These are the blobs present in the container: ";

    do {
        $result = $blobClient->listBlobs($container, $listBlobsOptions);

        foreach ($result->getBlobs() as $blob) {
            echo $blob->getName().": ".$blob->getUrl()."<br />";
        }
    
        $listBlobsOptions->setContinuationToken($result->getContinuationToken());
    } while($result->getContinuationToken());

    echo "<br />";
}

function fn_blob_exists($url) {
    $error_codes = array(
        'HTTP/1.1 400',
        'HTTP/1.1 404'
    );

    if(fn_strpos_array(@get_headers($url)[0], $error_codes)) {
        return false;
    } else {
        return true;
    }
}