<?php
/**
 * Created by PhpStorm.
 * User: Dren's Gjirafa PC
 * Date: 12/21/2017
 * Time: 12:27 PM
 */


if (!defined('BOOTSTRAP')) {
    die('Access denied');
}

use Aws\S3\Exception\S3Exception;
use Tygh\AmazonS3Client;
use Tygh\Http;
use Tygh\Registry;
use Detection\MobileDetectFast;

define('STORE', fn_store_location());

const NO_DELIVERY_DATE_PRODUCTS = [];

function fn_isAL()
{
    if (Registry::get('runtime.company_id') == '1')
        return false;
    elseif (Registry::get('config.gj50al_cid') == '19' && Registry::get('runtime.company_id') != '0')
        return true;
    //return (intval(Registry::get('runtime.company_id')) === intval(Registry::get('config.gj50al_cid'))) ? true : false;
}

function fn_store_location()
{

    if (fn_isAL())
        return "al";
    else
        return "ks";
}

function fn_get_company_val()
{
    if (fn_isAL()) {
        return 'AL';
    } else {
        return 'KS';
    }
}

function fn_get_company_currency_symbol()
{
    if (fn_isAL()) {
        return 'Lekë';
    } else {
        return '€';
    }
}

function fn_get_contact_info($type = "")
{
    if ($type == "email" && !fn_isAL())
        return "contact@gjirafa50.com";
    else if ($type == "email")
        return "contact@gjirafa50.al";
    if ($type == "number" && !fn_isAL())
        return "+383 45 101 953 dhe +383 44 228 188";
    else
        return "+355 68 803 0303";
}

function fn_clear_apcu_key($key)
{
    if (apcu_exists($key))
        return apcu_delete($key);
}


function fn_get_store_url()
{
    if (!fn_isAL())
        return "https://gjirafa50.com";
    else
        return "https://gjirafa50.al";
}


function fn_get_tracking_steps($order_info)
{
    $canceled_statuses = ['F', 'D', 'I', 'X'];
    $approved_statuses_related = ['E', 'J', 'P', 'Y', 'A', 'M', 'U', '3', 'H'];

    $approved_statuses_details = [
        'O' => 'E hapur',
        'R' => 'E verifikuar',
        'K' => 'Dërgesa gati',
        'L' => 'E nisur',
        'C' => 'E realizuar'
    ];

    $related_statuses = [
        'O' => [
            'Y',
            'M',
            'U'
        ],
        'R' => [
            'J'
        ],
        'K' => [
            'A'
        ],
        'L' => [
            'H'
        ],
        'C' => [
            'E',
            'P',
            '3'
        ],
    ];

    if ($order_info['status'] == 'H') {
        $approved_statuses_details['L'] = 'Për pranim';
    }

    $return_statuses_details = [
        'B' => 'E kthyer',
        'S' => 'Pjeserisht e kthyer'
    ];

    $is_approved_by_client = false;
    $tag_res = db_get_array("select * from ?:sd_search_tag_links where object_id = ?i and tag_id='2'", $order_info['order_id']);

    if (!empty($tag_res)) {
        $is_approved_by_client = true;
    }

    if ($order_info['status'] == 'B') {
        $approved_statuses_details['B'] = 'E kthyer';
    } elseif ($order_info['status'] == 'S') {
        $approved_statuses_details['S'] = 'Pjeserisht e kthyer';
    }

    foreach ($approved_statuses_details as $key => $value) {
        $approved_statuses[] = $key;
        $stepper_statuses[] = array(
            'status' => $key,
            'name' => $value,
            'status_desc' => __('status_desc_' . $key),
            'status_desc_inactive' => __('status_desc_inactive_' . $key),
            'related_statuses' => $related_statuses[$key]
        );
    }

    foreach ($stepper_statuses as $key => $value) {
        $active = false;
        $current_status = false;
        if (in_array($order_info['status'], $approved_statuses) || in_array($order_info['status'], $approved_statuses_related)) {
            if ($order_info['status'] == $value['status'] || in_array($order_info['status'], $value['related_statuses'])) {
                $current_status = true;
                $current_key = $key;
            }
            $key_before = $current_key - 1;
            if ($current_key <= $key_before || $current_key == $key) {
                $active = true;
            }
            $value['current'] = $current_status;
            $value['active'] = $active;
            $stepper[] = $value;
        } elseif (in_array($order_info['status'], $canceled_statuses)) {
            if ($key == 0) {
                $value['name'] = __('status_name_declined');
                $value['status_desc'] = __('status_desc_declined');
                $value['active'] = true;
                $value['current'] = true;
            }
            $stepper[] = $value;
        } else {
            $stepper = null;
        }
    }

    if ($is_approved_by_client == true && $order_info['status'] == 'L') {
        $stepper[3]['current'] = false;
        $stepper[4]['current'] = true;
        $stepper[4]['active'] = true;
        $stepper[3]['is_aproved_by_client'] = true;
    }

    return $stepper;
}

function fn_get_banner_image_name($id)
{

    $field = "image_path";
    $joins = array(
        " inner join gjshop_images_links on gjshop_images.image_id = gjshop_images_links.image_id ",
        " inner join gjshop_banner_images on gjshop_images_links.object_id = gjshop_banner_images.banner_image_id"
    );

    $imploded_joins = implode(' ', $joins);
    $name = db_get_field("select $field from ?:images $imploded_joins where ?:banner_images.banner_id = ?i", $id);

    return $name;
}

function fn_check_if_offer($product_id)
{
    $offer = 'false';
    if (isset($_SESSION['offer']['banner'][$product_id])) {
        $offer = 'banner';
        unset($_SESSION['offer']['banner']);
    }
    if (isset($_SESSION['offer']['dod'][$product_id])) {
        $offer = 'deal_of_the_day';
        unset($_SESSION['offer']['dod']);
    }
    if (isset($_SESSION['offer']['landing'])) {
        $offer = key($_SESSION['offer']['landing']);
        unset($_SESSION['offer']['landing']);
    }
    if (isset($_SESSION['offer']['normal_offer'])) {
        $offer = key($_SESSION['offer']['normal_offer']);
        unset($_SESSION['offer']['normal_offer']);
    }
    return $offer;
}

function send_view_to_gjirafa_analytics($method, $url, $data = false)
{
    if (!empty($data)) {
        $curl = curl_init();
        switch ($method) {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);

                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'GjKey: Gj1raf@Bl0bUbuntuS3cr3tK3y(*&^P1kB1zV1d303C0MM3Rc#'
        ));
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);
        return $result;

    }

}


function get_product_ids($product_code)
{
    if (!is_null($product_code))
        return db_get_array("select product_id from ?:products where product_code in (?a) order by field(product_code, ?a)", $product_code, $product_code);
    return false;
}

function fn_get_mobile_main_categories()
{
    if (fn_isAL()) {
        $condition = ' and company_id = 19';
    } else {
        $condition = ' and company_id = 1';
    }
    $key = md5(json_encode($condition));
    if (apcu_exists("mobile_categories_" . $key)) {
        $categories = apcu_fetch("mobile_categories_" . $key);
    } else {
        $categories = db_get_array("Select ?:categories.category_id , category from ?:categories left join ?:category_descriptions on ?:categories.category_id = ?:category_descriptions.category_id  where parent_id = 0 and status = 'A' $condition");
        apcu_store("mobile_categories_" . $key, $categories);
    }
    return $categories;
}


function fn_get_external_data($method, $url, $data = false, $extra = '')
{
    try {
        $curl = curl_init();
        switch ($method) {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);

                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }
        if ($extra == 'stock_wms') {
            curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'api-key: JEUQ77S46GTZXD6M9HIWM9WZ38XQVZPTUWLU',

            ));
        }
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);


        $result = curl_exec($curl);
        curl_close($curl);
        return $result;
    } catch (Exception $e) {
        echo 'Message: ' . $e->getMessage();
    }

}

function image_compress($source, $destination, $quality)
{

    $info = getimagesize($source);

    if ($info['mime'] == 'image/jpeg')
        $image = imagecreatefromjpeg($source);

    elseif ($info['mime'] == 'image/gif')
        $image = imagecreatefromgif($source);

    elseif ($info['mime'] == 'image/png')
        $image = imagecreatefrompng($source);

    imagejpeg($image, $destination, $quality);

    return $destination;
}

function fn_get_subcat_img($category_id, $category_data)
{
    fn_auto_update_category_image($category_id, $category_data);

    $url = 'https://hhstsyoejx.gjirafa.net/gj50/subcategories/' . str_replace('/', '_', $category_data['id_path']) . ' ' . fn_parse_category_name($category_data['category'] . '.jpg');
    $url = str_replace(' ', '%20', $url);

    if (fn_blob_exists($url)) {
        return $url;
    } else {
        if (apcu_exists("subcat_images"))
            $category_images = apcu_fetch("subcat_images");
        else {
            $category_images = file_get_contents('feeds/subcat_images.txt');
            $category_images = unserialize($category_images);
            apcu_store("subcat_images", $category_images);
        }

        return $category_images[$category_id]['url'];
    }
}

function fn_url_exists($url, $error_code = null) {
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_NOBODY, true);
    curl_exec($ch);
    // $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    $info = curl_getinfo($ch);

	// if ($error_code) {
	// 	if ($code == $error_code) {
	//         $status = false;
	//     } else {
	//         $status = true;
	//     }
	// } else {
	//     if ($code == 200) {
	//         $status = true;
	//     } else {
	//         $status = false;
	//     }
	// }

    curl_close($ch);
	// return $status;
    return $info;
}

function fn_strpos_array($haystack, $needle, $offset = 0) {
    if(!is_array($needle)) {
        $needle = array($needle);
    }

    foreach($needle as $query) {
        if(strpos($haystack, $query, $offset) !== false) {
            return true;
        };
    }

    return false;
}

// function fn_is_mobile()
// {
//     $tablet_browser = 0;
//     $mobile_browser = 0;

//     if (preg_match('/(tablet|ipad|playbook)|(android(?!.*(mobi|opera mini)))/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
//         $tablet_browser++;
//     }

//     if (preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android|iemobile)/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
//         $mobile_browser++;
//     }

//     if ((strpos(strtolower($_SERVER['HTTP_ACCEPT']), 'application/vnd.wap.xhtml+xml') > 0) or ((isset($_SERVER['HTTP_X_WAP_PROFILE']) or isset($_SERVER['HTTP_PROFILE'])))) {
//         $mobile_browser++;
//     }

//     $mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'], 0, 4));
//     $mobile_agents = array(
//         'w3c ', 'acs-', 'alav', 'alca', 'amoi', 'audi', 'avan', 'benq', 'bird', 'blac',
//         'blaz', 'brew', 'cell', 'cldc', 'cmd-', 'dang', 'doco', 'eric', 'hipt', 'inno',
//         'ipaq', 'java', 'jigs', 'kddi', 'keji', 'leno', 'lg-c', 'lg-d', 'lg-g', 'lge-',
//         'maui', 'maxo', 'midp', 'mits', 'mmef', 'mobi', 'mot-', 'moto', 'mwbp', 'nec-',
//         'newt', 'noki', 'palm', 'pana', 'pant', 'phil', 'play', 'port', 'prox',
//         'qwap', 'sage', 'sams', 'sany', 'sch-', 'sec-', 'send', 'seri', 'sgh-', 'shar',
//         'sie-', 'siem', 'smal', 'smar', 'sony', 'sph-', 'symb', 't-mo', 'teli', 'tim-',
//         'tosh', 'tsm-', 'upg1', 'upsi', 'vk-v', 'voda', 'wap-', 'wapa', 'wapi', 'wapp',
//         'wapr', 'webc', 'winw', 'winw', 'xda ', 'xda-');

//     if (in_array($mobile_ua, $mobile_agents)) {
//         $mobile_browser++;
//     }

//     if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'opera mini') > 0) {
//         $mobile_browser++;
//         //Check for tablets on opera mini alternative headers
//         $stock_ua = strtolower(isset($_SERVER['HTTP_X_OPERAMINI_PHONE_UA']) ? $_SERVER['HTTP_X_OPERAMINI_PHONE_UA'] : (isset($_SERVER['HTTP_DEVICE_STOCK_UA']) ? $_SERVER['HTTP_DEVICE_STOCK_UA'] : ''));
//         if (preg_match('/(tablet|ipad|playbook)|(android(?!.*mobile))/i', $stock_ua)) {
//             $tablet_browser++;
//         }
//     }

//     if ($tablet_browser > 0) {
//         // do something for tablet devices
//         return array(
//             'mobile' => true,
//             'type' => 'tablet'
//         );
//     } else if ($mobile_browser > 0) {
//         // do something for mobile devices
//         return array(
//             'mobile' => true,
//             'type' => 'phone'
//         );
//     } else {
//         // do something for everything else
//         return array(
//             'mobile' => false,
//             'type' => 'desktop'
//         );
//     }
// }

function fn_is_mobile() {
    $detect = new MobileDetectFast;

    if ($detect->isPhoneFast()) {
        return array(
            'mobile' => true,
            'type' => 'phone'
        );
    } elseif ($detect->isTabletFast()) {
        return array(
            'mobile' => true,
            'type' => 'tablet'
        );
    } else {
        return array(
            'mobile' => false,
            'type' => 'desktop'
        );
    }
}

function fn_get_shipping_days($product, $date)
{
    $d = date('w');
    $h = date('h');
    $a = 9;

    if ($product['amount'] == 0 && $product['czc_stock'] == 0)
        return 14;
    else {
        if ($d == 1 && $h <= 20 || $d == 4 && $h < 20)
            $a = 5;
        else {
            if($d == 0)
                $d = 7;

            if($d < 4)
                $a = 4 - $d + 5;
            else if($d > 4)
                $a = 7 - $d + 5 + 1;
        }
    }
    return $a;
}

function fn_get_order_complete_remaining_days($order)
{
    if(in_array($order['status'], array('P', 'F', 'D', 'B', 'I', 'Y', 'U', '1', 'N', 'H', 'C'))) {
        return false;
    }

    foreach ($order['products'] as $product) {
        $czc_stock[] = $product['retailer_stock']['czc_stock'];
        $czc_retailer_stock[] = $product['retailer_stock']['czc_retailer_stock'];
    }

    if (!in_array(0, $czc_stock)) {
        $remaining_days = '5 - 8';
    } elseif ((!in_array(0, $czc_retailer_stock) && in_array(0, $czc_stock)) || (in_array(0, $czc_retailer_stock) && in_array(0, $czc_stock))) {
        $remaining_days = '8 - 12';
    }

    return $remaining_days;
}

function fn_get_order_delivery_dates($order_id, $cart_products = null, $order_info_inherited = null, $checkout_cart_products = null) {
    $preorder_delivery_dates = null;
    $no_delivery_date = false;
    $product_ids = [];

    if ($order_id != 0) {
        $order_info = fn_get_order_info($order_id);
        $order_short_info = fn_get_order_short_info($order_id);
        $timestamp = $order_short_info['timestamp'];
        $products = $order_info['product_groups'][0]['products'];

        foreach ($order_info['products'] as $product) {
            array_push($product_ids, $product['product_id']);
//            if ($product['avail_since'] != 0 && $product['avail_since'] > $timestamp) {
//                $timestamp = $product['avail_since'];
//            }

            if ($product['product_id'] == 169069 && $timestamp < 1635458399) {
                return array(
                    'standard' => '10/28/2021',
                    'standard_range' => '10/29/2021',
                    'stock' => 2,
                    'timestamp' => $timestamp
                );
            }

            $codd = fn_custom_order_delivery_dates($product, $timestamp, false, true);

            if ($codd) {
                return $codd;
            }

            if (in_array($product['product_code'], explode(',', __('no_delivery_date_products'))) || fn_disable_product_delivery_dates($product, true)) {
                $no_delivery_date = true;
            }

            if (in_array($product['product_id'], NO_DELIVERY_DATE_PRODUCTS)) {
                if (!$no_delivery_date) {
                    return array(
                        'standard' => date("m/d/Y", strtotime('+8 days', $timestamp)),
                        'standard_range' => date("m/d/Y", strtotime('+9 days', $timestamp)),
                        'stock' => 2,
                        'timestamp' => time()
                    );
                }
            }
        }

        if ($no_delivery_date) {
            return false;
        }

//        foreach ($order_info['products'] as $product) {
//            if ($product['store_id'] != LICENSE_KEY_PRODUCTS_STORE_ID) {
//                $calculate = true;
//                break;
//            }
//        }
//
//        if (!$calculate) {
//            return array();
//        }

        $has_stock = fn_order_has_stock($order_info['product_groups'][0]['products']);
        $has_retailer_stock = fn_order_has_retailer_stock($order_info['product_groups'][0]['products']);
    } else {
        $timestamp = time();
        $products = $cart_products;

        foreach ($checkout_cart_products as $product) {
            array_push($product_ids, $product['product_id']);
//            if ($product['avail_since'] != 0 && $product['avail_since'] > $timestamp) {
//                $timestamp = $product['avail_since'];
//            }

            if ($product['product_id'] == 169069 && $timestamp < 1635458399) {
                return array(
                    'standard' => '10/28/2021',
                    'standard_range' => '10/29/2021',
                    'stock' => 2,
                    'timestamp' => $timestamp
                );
            }

            $codd = fn_custom_order_delivery_dates($product, $timestamp);

            if ($codd) {
                return $codd;
            }

            if (in_array($product['product_code'], explode(',', __('no_delivery_date_products'))) || fn_disable_product_delivery_dates($product, true)) {
                $no_delivery_date = true;
            }

            if (in_array($product['product_id'], NO_DELIVERY_DATE_PRODUCTS)) {
                if (!$no_delivery_date) {
                    return array(
                        'standard' => date("m/d/Y", strtotime('+8 days', $timestamp)),
                        'standard_range' => date("m/d/Y", strtotime('+9 days', $timestamp)),
                        'stock' => 2,
                        'timestamp' => time()
                    );
                }
            }
        }

        if ($no_delivery_date) {
            return false;
        }

        $has_stock = fn_order_has_stock($cart_products);
        $has_retailer_stock = fn_order_has_retailer_stock($cart_products);
        $order_info = $order_info_inherited;
    }

    if (!$has_stock) {
        $order_custom_delivery_dates = fn_get_order_custom_delivery_dates($product_ids, $timestamp, $products);

        if ($order_custom_delivery_dates) {
            return $order_custom_delivery_dates;
        }
    }

    if ($preorder_delivery_dates) {
        return $preorder_delivery_dates;
    }

    $delivery_date = date("m/d/Y", strtotime('+1 day', $timestamp));
    $initial_extra_days = 0;
    $range_extra_days = 1;

    if ($order_info && $order_info['fields'][41] != '70') {
        $initial_extra_days += 1;
    }

    $hour = date('Hi', $timestamp);
    $day = date('N', $timestamp);

    list($delivers_monday, $delivers_wednesday, $delivers_friday) = fn_get_delivery_date_periods($day, $hour);

    $stock = 1;

    if (!$has_stock && $has_retailer_stock) {
        $extra_days = ' +' . $initial_extra_days . ' days';

        if ($delivers_monday) {
            $delivery_date = date('m/d/Y', strtotime('this monday' . $extra_days, $timestamp));

            // invoice friday
            $invoice_date = date('m/d/Y', strtotime('this friday', $timestamp));
            if (array_key_exists($invoice_date, fn_get_czech_holidays())) {
                $extra_days = ' +' . (intval($extra_days) + fn_get_czech_holidays()[$invoice_date]) . ' days';
                $delivery_date = date('m/d/Y', strtotime('this wednesday' . $extra_days, strtotime($invoice_date)));
            }
        } elseif ($delivers_wednesday) {
            $delivery_date = date('m/d/Y', strtotime('this wednesday' . $extra_days, $timestamp));

            // invoice monday
            $invoice_date = date('m/d/Y', strtotime('this monday', $timestamp));
            if (array_key_exists($invoice_date, fn_get_czech_holidays())) {
                $extra_days = ' +' . (intval($extra_days) + fn_get_czech_holidays()[$invoice_date]) . ' days';
                $delivery_date = date('m/d/Y', strtotime('this thursday' . $extra_days, strtotime($invoice_date)));
            }
        } elseif ($delivers_friday) {
            $delivery_date = date('m/d/Y', strtotime('this friday' . $extra_days, $timestamp));
//            $range_extra_days += 1;

            // invoice wednesday
            $invoice_date = date('m/d/Y', strtotime('this wednesday', $timestamp));
            if (array_key_exists($invoice_date, fn_get_czech_holidays())) {
                $extra_days = ' +' . (intval($extra_days) + fn_get_czech_holidays()[$invoice_date]) . ' days';
                $delivery_date = date('m/d/Y', strtotime('this monday' . $extra_days, strtotime($invoice_date)));
            }
        }

        $stock = 2;
    } elseif (!$has_stock && !$has_retailer_stock) {
        $extra_days = ' +' . (3 + $initial_extra_days) . ' days';

        if ($delivers_monday) {
            $extra_days = ' +' . (5 + $initial_extra_days) . 'days';
            $delivery_date = date('m/d/Y', strtotime('this monday' . $extra_days, $timestamp));

            // invoice friday
            $invoice_date = date('m/d/Y', strtotime('this friday', $timestamp));
            if (array_key_exists($invoice_date, fn_get_czech_holidays())) {
                $extra_days = ' +' . (intval($extra_days) + fn_get_czech_holidays()[$invoice_date]) . ' days';
                $delivery_date = date('m/d/Y', strtotime('this wednesday' . $extra_days, strtotime($invoice_date)));
            }
        } elseif ($delivers_wednesday) {
            $delivery_date = date('m/d/Y', strtotime('this wednesday' . $extra_days, $timestamp));

            // invoice monday
            $invoice_date = date('m/d/Y', strtotime('this monday', $timestamp));
            if (array_key_exists($invoice_date, fn_get_czech_holidays())) {
                $extra_days = ' +' . (intval($extra_days) + fn_get_czech_holidays()[$invoice_date]) . ' days';
                $delivery_date = date('m/d/Y', strtotime('this thursday' . $extra_days, strtotime($invoice_date)));
            }
        }  elseif ($delivers_friday) {
            $delivery_date = date('m/d/Y', strtotime('this friday' . $extra_days, $timestamp));

            // invoice wednesday
            $invoice_date = date('m/d/Y', strtotime('this wednesday', $timestamp));
            if (array_key_exists($invoice_date, fn_get_czech_holidays())) {
                $extra_days = ' +' . (intval($extra_days) + fn_get_czech_holidays()[$invoice_date]) . ' days';
                $delivery_date = date('m/d/Y', strtotime('this monday' . $extra_days, strtotime($invoice_date)));
            }
        }

        $stock = 3;
    }

    $local_holiday_delay = array_search($delivery_date, fn_get_local_holidays(), true);

    if ($local_holiday_delay) {
        $delivery_date = date('m/d/Y', strtotime('+' . $local_holiday_delay . ' days', strtotime($delivery_date)));
    }

    return array(
        'standard' => $delivery_date,
        'standard_range' => date('m/d/Y', strtotime($delivery_date . ' +' . $range_extra_days . ' days')),
        'stock' => $stock,
        'timestamp' => time()
    );
}

function fn_disable_product_delivery_dates($product, $cart = false) {
    if ($cart) {
        $product['real_amount'] = db_get_field('SELECT amount FROM ?:products WHERE product_id = ?i', $product['product_id']);
    }

    if (!apcu_exists('no_delivery_date_variants') || empty(apcu_fetch('no_delivery_date_variants'))) {
        $variants = db_get_fields("SELECT variant_id FROM gjshop_product_feature_variant_descriptions WHERE variant = 'Apple' OR variant = 'Lenovo'");
        apcu_store('no_delivery_date_variants', $variants, 3600);
    } else {
        $variants = apcu_fetch('no_delivery_date_variants');
    }

    $gc_categories = [
        20,
        695,
        1047,
        1701,
        3140
    ];

    $categories = fn_get_product_categories([$product['product_id']]);
    $features = db_get_array('SELECT * FROM ?:product_features_values WHERE product_id = ?i', $product['product_id']);
    $is_disabled_brand = false;
    $is_graphics_card = false;
    $disabled = false;

    if (count(array_intersect($gc_categories, $categories)) > 0) {
        $is_graphics_card = true;
    }

    foreach ($features as $feature) {
        if ($feature['feature_id'] == 12 && in_array($feature['variant_id'], $variants)) {
            $is_disabled_brand = true;
        }
    }

    if (($is_disabled_brand || $is_graphics_card) && $product['real_amount'] == 0) {
        $disabled = true;
    }

    return $disabled;
}

function fn_get_product_delivery_dates($product_data) {
    $has_stock = $product_data['real_amount'] > 0 ? true : false;
    $has_retailer_stock = $product_data['czc_stock'] > 0 ? true : false;
    $timestamp = time();

    $local_delivery_date = date("m/d/Y", strtotime(' +1 day', $timestamp))  ;
    $extra_days = ' +1 day';
    $initial_extra_days = 0;
    $range_extra_days = 1;

    if (in_array($product_data['product_id'], NO_DELIVERY_DATE_PRODUCTS)) {
        return array(
            'has_stock' => $has_stock,
            'local' => date("m/d/Y", strtotime('+8 days', $timestamp)),
            'local_range' => date("m/d/Y", strtotime('+9 days', $timestamp)),
            'standard' => date("m/d/Y", strtotime('+8 days', $timestamp)),
            'standard_range' => date("m/d/Y", strtotime('+9 days', $timestamp))
        );
    }

    $product_custom_delivery_dates = fn_get_product_custom_delivery_dates($product_data, $has_stock, $has_retailer_stock, $timestamp);
    if ($product_custom_delivery_dates) {
        return $product_custom_delivery_dates;
    }

    if (strpos($product_data['product_code'], 'gjirafa50') !== false) {
        $extra_days = ' +' . ($initial_extra_days + 1) . ' days';
        $initial_extra_days += 1;
    }

    $codd = fn_custom_order_delivery_dates($product_data, $timestamp, true);

    if ($codd) {
        return $codd;
    }

    if ($product_data['product_id'] == 169069 && $timestamp < 1635458399) {
        return array(
            'has_stock' => $has_stock,
            'local' => '10/28/2021',
            'local_range' => '10/29/2021',
            'standard' => '10/28/2021',
            'standard_range' => '10/29/2021'
        );
    }

    $delivery_date = date("m/d/Y", strtotime($extra_days, $timestamp));

    $hour = date('Hi', $timestamp);
    $day = date('N', $timestamp);

    list($delivers_monday, $delivers_wednesday, $delivers_friday) = fn_get_delivery_date_periods($day, $hour);

    if (!$has_stock && $has_retailer_stock) {
        $extra_days = ' +' . (1 + $initial_extra_days) . 'days';

        if ($delivers_monday) {
            $delivery_date = date('m/d/Y', strtotime('this monday' . $extra_days));

            // invoice friday
            $invoice_date = date('m/d/Y', strtotime('this friday', $timestamp));
            if (array_key_exists($invoice_date, fn_get_czech_holidays())) {
                $extra_days = ' +' . (intval($extra_days) + fn_get_czech_holidays()[$invoice_date]) . ' days';
                $delivery_date = date('m/d/Y', strtotime('this wednesday' . $extra_days, strtotime($invoice_date)));
            }
        } elseif ($delivers_wednesday) {
            $delivery_date = date('m/d/Y', strtotime('this wednesday' . $extra_days));

            // invoice monday
            $invoice_date = date('m/d/Y', strtotime('this monday', $timestamp));
            if (array_key_exists($invoice_date, fn_get_czech_holidays())) {
                $extra_days = ' +' . (intval($extra_days) + fn_get_czech_holidays()[$invoice_date]) . ' days';
                $delivery_date = date('m/d/Y', strtotime('this thursday' . $extra_days, strtotime($invoice_date)));
            }
        } elseif ($delivers_friday) {
            $delivery_date = date('m/d/Y', strtotime('this friday' . $extra_days));
//            $range_extra_days += 1;

            // invoice wednesday
            $invoice_date = date('m/d/Y', strtotime('this wednesday', $timestamp));
            if (array_key_exists($invoice_date, fn_get_czech_holidays())) {
                $extra_days = ' +' . (intval($extra_days) + fn_get_czech_holidays()[$invoice_date]) . ' days';
                $delivery_date = date('m/d/Y', strtotime('this monday' . $extra_days, strtotime($invoice_date)));
            }
        }

    } elseif (!$has_stock && !$has_retailer_stock) {
        $extra_days = ' +' . (4 + $initial_extra_days) . 'days';

        if ($delivers_monday) {
            $extra_days = ' +' . (6 + $initial_extra_days) . 'days';
            $delivery_date = date('m/d/Y', strtotime('this monday' . $extra_days));

            // invoice friday
            $invoice_date = date('m/d/Y', strtotime('this friday', $timestamp));
            if (array_key_exists($invoice_date, fn_get_czech_holidays())) {
                $extra_days = ' +' . (intval($extra_days) + fn_get_czech_holidays()[$invoice_date]) . ' days';
                $delivery_date = date('m/d/Y', strtotime('this wednesday' . $extra_days, strtotime($invoice_date)));
            }
        } elseif ($delivers_wednesday) {
            $delivery_date = date('m/d/Y', strtotime('this wednesday' . $extra_days));

            // invoice monday
            $invoice_date = date('m/d/Y', strtotime('this monday', $timestamp));
            if (array_key_exists($invoice_date, fn_get_czech_holidays())) {
                $extra_days = ' +' . (intval($extra_days) + fn_get_czech_holidays()[$invoice_date]) . ' days';
                $delivery_date = date('m/d/Y', strtotime('this thursday' . $extra_days, strtotime($invoice_date)));
            }
        } elseif ($delivers_friday) {
            $delivery_date = date('m/d/Y', strtotime('this friday' . $extra_days));

            // invoice wednesday
            $invoice_date = date('m/d/Y', strtotime('this wednesday', $timestamp));
            if (array_key_exists($invoice_date, fn_get_czech_holidays())) {
                $extra_days = ' +' . (intval($extra_days) + fn_get_czech_holidays()[$invoice_date]) . ' days';
                $delivery_date = date('m/d/Y', strtotime('this monday' . $extra_days, strtotime($invoice_date)));
            }
        }
    }

    if (!$has_stock) {
        $local_delivery_date = date('m/d/Y', strtotime($delivery_date . ' -1 day'));
    }

    return array(
        'has_stock' => $has_stock,
        'local' => $local_delivery_date,
        'local_range' => date('m/d/Y', strtotime($local_delivery_date . ' + ' . $range_extra_days .' days')),
        'standard' => $delivery_date,
        'standard_range' => date('m/d/Y', strtotime($delivery_date . ' +' . $range_extra_days .' days'))
    );
}

/**
 * Function that groups an array of associative arrays by some key.
 *
 * @param {String} $key Property to sort by.
 * @param {Array} $data Array that stores multiple associative arrays.
 */
function fn_group_by_array($key, $data) {
    $result = array();

    foreach($data as $val) {
        if(array_key_exists($key, $val)){
            $result[$val[$key]][] = $val;
        }else{
            $result[""][] = $val;
        }
    }

    return $result;
}


function fn_send_sms($params, $backup = false) {
    $gateway_params = Registry::get('config.gateway_params');
    $params = array(
        'to'            => $params['to'],
        'from'          => $gateway_params['from'],
        'message'       => $params['message'],
        'format'        => 'json'
    );
    $token = $gateway_params['token'];
    $url = $gateway_params['url'];

    if ($backup == true)
        $url = 'https://api2.smsapi.com/sms.do';

    $c = curl_init();
    curl_setopt($c, CURLOPT_URL, $url);
    curl_setopt($c, CURLOPT_POST, true);
    curl_setopt($c, CURLOPT_POSTFIELDS, $params);
    curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($c, CURLOPT_HTTPHEADER, array(
        "Authorization: Bearer $token"
    ));

    $content = curl_exec($c);
    $http_status = curl_getinfo($c, CURLINFO_HTTP_CODE);

    curl_close($c);

    if ($http_status != 200 && $backup == false) {
        return false;
        //$backup = true;
        //fn_send_sms($params, $token, $backup);
    }

    $content = json_decode($content, true);

    return ($content['count'] == 1) ? true : false;
}

/**
 * Parse phone numbers from existing file to new file with KS (+383) format.
 * File read and write format: CSV
 */
function fn_parse_numbers_from_csv($file_from, $file_to) {
    $numbers = array();

    if (($handle = fopen($file_from . '.csv', 'r')) !== false) {
        while (($row = fgetcsv($handle, 15)) !== false) {
            for ($i=0; $i < count($row); $i++) {
                array_push($numbers, $row[$i]);
            }
        }

        fclose($handle);

        fn_parse_to_ks_format($numbers, $file_to);
    }
}

function fn_parse_to_ks_format($numbers, $file_to = false) {
    $parsed_numbers = array();

    foreach($numbers as $number) {
        $number = preg_replace('/(\W)+/', '', $number); // trims spaces and non-numeric characters

        if (strlen($number) == 12) {
            if (strpos($number, '+') === 0 ||
                strpos($number, '+') === 0) {

                $number = str_replace('+', '', $number);
                fn_parse_to_ks_format(array($number));
            }
        } else if (strlen($number) == 11) {
            if (strpos($number, '377') === 0 ||
                strpos($number, '386') === 0) {

                array_push($parsed_numbers, substr_replace($number, '383', 0, 3));
            }

            if(strpos($number, '383') === 0) {

                array_push($parsed_numbers, $number);
            }
        } else if (strlen($number) == 9) {
            if (strpos($number, '043') === 0 ||
                strpos($number, '044') === 0 ||
                strpos($number, '045') === 0 ||
                strpos($number, '046') === 0 ||
                strpos($number, '047') === 0 ||
                strpos($number, '048') === 0 ||
                strpos($number, '049') === 0) {

                array_push($parsed_numbers, fn_replace_first($number, '0', '383'));
            }
        } else if (strlen($number) == 8) {
            if (strpos($number, '43') === 0 ||
                strpos($number, '44') === 0 ||
                strpos($number, '45') === 0 ||
                strpos($number, '46') === 0 ||
                strpos($number, '47') === 0 ||
                strpos($number, '48') === 0 ||
                strpos($number, '49') === 0) {

                array_push($parsed_numbers, '383' . $number);
            }
        }
    }

    if(!$file_to)
        return current($parsed_numbers);

    fn_save_to_csv($file_to, $parsed_numbers);
}

function fn_save_to_csv($file_to, $parsed_numbers) {
    $handle = fopen($file_to . '.csv', 'w');
    fputcsv($handle, $parsed_numbers, chr(13)); // chr(13) represents new line
    fclose($handle);
}

function fn_replace_first($string, $from, $to) {
    $start = strpos($string, $from);
    if ($start !== false) {
        return substr_replace($string, $to, $start, strlen($from));
    }
}

function fn_get_orderer_type($order_id) {
    $data = array(
        'object_id' => $order_id,
        'field_id'  => array(43, 51),
        array('value', '!=', '')
    );

    $result = db_query('SELECT value FROM ?:profile_fields_data WHERE ?w', $data);

    if($result->num_rows == 0) {
        $type = 'I'; // individ
    } else {
        $type = 'B'; // biznes
    }

    return $type;
}

function fn_get_gift_cert_orderer_type($gift_cert_code) {
    $gift_cert_code = '\'' . $gift_cert_code . '\'';
    $result = db_get_field('SELECT orderer_type FROM gjshop_gift_certificates WHERE gift_cert_code = ' . $gift_cert_code . ';');

    return $result;
}

function fn_parse_category_name($category) {
    $category = strtolower($category);
    $category = str_replace(array('/', 'ë', 'Ç', 'ç'), array('_', 'e', 'c', 'c'), $category);

    return $category;
}

function fn_closest_number($n, $m) {
    // find the quotient
    $q = (int) ($n / $m);

    // 1st possible closest number
    $n1 = $m * $q;

    // 2nd possible closest number
    $n2 = ($n * $m) > 0 ?
        ($m * ($q + 1)) : ($m * ($q - 1));

    // if true, then n1 is the
    // required closest number
    // if (abs($n - $n1) < abs($n - $n2))
    //     return $n1;

    // else n2 is the required
    // closest number
    return $n2;
}

function fn_generate_barcode($stream) {
    $content = base64_decode($stream);
    $barcode = imagecreatefromstring($content);

    if ($barcode !== false) {
        header('Content-Type: image/jpeg');
        imagejpeg($barcode, "images/test/1.jpg");
        imagedestroy($barcode);
    } else {
        echo 'An error occurred.';
    }
}

function fn_validate_personal_number($personal_number) {
    if (!ctype_digit(strval($personal_number)) || strlen($personal_number) != 10) {
        return false;
    }

    return true;
}

function fn_round_number($number, $round_to) {
    return ceil($number / $round_to) * $round_to;
}

function fn_parse_html($html) {
    $dom = new DOMDocument();
    $dom->loadHTML($html);
    $bodies = $dom->getElementsByTagName('body');
    assert($bodies->length === 1);
    $body = $bodies->item(0);

    for ($i = 0; $i < $body->children->length; $i++) {
        $body->remove($body->children->item($i));
    }

    $string_body = $dom->saveHTML($body);

    return $string_body;
}

function fn_parse_html_attributes($html, $tag, $attribute) {
    $dom = new DOMDocument();
    @$dom->loadHTML($html);

    $tags = $dom->getElementsByTagName($tag);
    $attributes = array();

    foreach ($tags as $tag) {
        $attributes[] = $tag->getAttribute($attribute);
    }

    return $attributes;
}

function fn_base64_to_image($base64_string, $output_file) {
    $fh = fopen($output_file, 'w');

    $data = explode(',', $base64_string);

    $encoded_img_string = trim(rawurldecode($data[1]));

    // $chunks = str_split($encoded_img_string, 1024);
    // foreach ($chunks as $chunk) {
    //     fwrite($fh, base64_decode($chunk), strlen(base64_decode($chunk)));
    // }

    fwrite($fh, base64_decode($encoded_img_string));

    fclose($fh);

    chmod($output_file, 0777);

    return $output_file;
}

function fn_array_values_recursive($array) {
    $list = array();

    foreach( array_keys($array) as $k ) {
        $v = $array[$k];

        if (is_scalar($v)) {
            $list[] = $v;
        } elseif (is_array($v)) {
            $list = array_merge($list, fn_array_values_recursive($v));
        }
    }

    return $list;
}

function fn_array_group_by($array, $number) {
    $grouped_array = array();

    for ($i=0; $i < count($array); $i+=$number) {
        $grouped_array[$array[$i]] = $array[$i + ($number - 1)];
    }

    return $grouped_array;
}

function fn_pluck_array_elements($array, $elements) {
    $parsed_array = array();

    foreach ($elements as $element) {
        if (array_search($element, $array)) {
            $parsed_array[$element] = $array[$element];
        }
    }

    return $parsed_array;
}

function fn_get_string_between($string, $start, $end) {
    $string = ' ' . $string;
    $ini = strpos($string, $start);
    if ($ini == 0) return '';
    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;
    if ($len < 0) $len = 0;
    return substr($string, $ini, $len);
}

function fn_insert_at_position($string, $insert, $position) {
    $string_array = str_split($string, $position);
    $string_array[0] .= $insert;

    return implode($string_array);
}

function fn_custom_date_format($date) {
    return substr($date, 0, -5);
}

function fn_insert_order_delivery_dates($order_id, $products = null, $skip = false, $update = false) {
    $order_delivery_dates = db_get_row('SELECT * FROM ?:order_delivery_dates WHERE order_id = ?i', $order_id);
    $company_id = db_get_field('SELECT company_id FROM ?:orders WHERE order_id = ?i', $order_id);

    if (!empty($order_delivery_dates) && $order_delivery_dates['date_from'] == $order_delivery_dates['date_to']) {
        $update = true;
    }

    if ((empty($order_delivery_dates) && !$skip && !fn_isAL() && $company_id == 1) || $update == true) {
        if ($products && !$order_id) {
            $delivery_dates = fn_get_order_delivery_dates(0, $products);
        } else {
            $delivery_dates = fn_get_order_delivery_dates($order_id);
        }

        if ($delivery_dates) {
            $from_date = new DateTime($delivery_dates['standard']);
            $from_date = $from_date->getTimestamp();

            $to_date = new DateTime($delivery_dates['standard_range']);
            $to_date = $to_date->getTimestamp();

            $dd = [
                'standard' => $delivery_dates['standard'],
                'standard_range' => $delivery_dates['standard_range'],
            ];

            $dd_data = array(
                'order_id' => $order_id,
                'date_from' => $from_date,
                'date_to' => $to_date,
            );

            db_query('DELETE FROM ?:order_delivery_dates WHERE order_id = ?i', $order_id);
            db_query('REPLACE INTO ?:order_delivery_dates ?e', $dd_data);
        }
    } else {
        if (!empty($order_delivery_dates)) {
            $date_from = date('m/d/Y', $order_delivery_dates['date_from']);
            $date_to = date('m/d/Y', $order_delivery_dates['date_to']);
            $dd = [
                'standard' => $date_from,
                'standard_range' => $date_to,
                'standard_timestamp' => $order_delivery_dates['date_from'],
                'standard_range_timestamp' => $order_delivery_dates['date_to']
            ];
        } else {
            $dd = array();
        }
    }

    return $dd;
}

function fn_get_products_delaying_delivery($products) {
    $pdd = [];
    $pndd = [];

    foreach ($products as $product) {
        if ($product['license_key_product']) {
            continue;
        }

        if (($product['stock_amount'] < 1 || $product['stock_amount'] < $product['amount']) && ($product['czc_stock'] < 1 || $product['czc_stock'] < $product['amount'])) {
            $pdd[$product['product_id']] = $product['product'];
        } else {
            $pndd[] = $product['product'];
        }
    }

    return array(
        'pdd' => $pdd,
        'pndd' => $pndd
    );
}

function fn_get_czech_holidays() {
    return [
        '01/01/2020' => 0,
        '04/10/2020' => 0,
        '04/13/2020' => 0,
        '05/01/2020' => 0,
        '05/08/2020' => 0,
        '07/06/2020' => 0,
        '09/28/2020' => 0,
        '10/28/2020' => 0,
        '11/17/2020' => 0,
        '12/24/2020' => 0,
        '12/25/2020' => 0,
        '12/26/2020' => 0,
        '01/01/2021' => 0,
        '04/02/2021' => 3,
        '04/05/2021' => 2,
        '05/01/2021' => 0,
        '05/08/2021' => 0,
        '07/05/2021' => 2,
        '07/06/2021' => 1,
        '09/28/2021' => 1,
        '10/28/2021' => 0,
        '11/17/2021' => 1,
        '12/22/2021' => 1,
        '12/23/2021' => 0,
        '12/24/2021' => 0,
        '12/29/2021' => 1,
        '01/01/2022' => 0,
        '01/02/2022' => 0,
        '01/03/2022' => 0,
    ];
}

function fn_get_local_holidays() {
    return [];
}

function fn_get_delivery_date_periods($day, $hour) {
    $delivers_monday = (($day == 3 && $hour >= '0830') || $day == 4 || ($day == 5 && $hour < '0830'));
    $delivers_wednesday = (($day == 5 && $hour >= '0830') || $day == 6 || $day == 7 || ($day == 1 && $hour < '0830'));
    $delivers_friday = (($day == 1 && $hour >= '0830') || $day == 2 || ($day == 3 && $hour < '0830'));

    return array(
        $delivers_monday,
        $delivers_wednesday,
        $delivers_friday,
    );
}

function fn_auto_update_category_image($category_id, $category_data) {
    $client = new AmazonS3Client();

    $name = 'subcategories/' . str_replace('/', '_', $category_data['id_path']) . ' ' . fn_parse_category_name($category_data['category'] . '.jpg');

    try {
        $client->getObject('gj50', $name);
    } catch (S3Exception $e) {
        $pid = db_get_field('SELECT product_id FROM ?:products_categories WHERE category_id = ?i ORDER BY product_id DESC LIMIT 1', $category_id);

        try {
            $client->createObject('gj50', $name,'https://hhstsyoejx.gjirafa.net/gj50/img/' . $pid .'/thumb/0.jpg');
        } catch (InvalidArgumentException $e) {}
    }
}

function fn_get_product_flag($ean, $product_id) {
    $ean = db_get_field('SELECT gtin FROM ?:product_gtins WHERE product_id = ?i', $product_id);

    if (!$ean || $ean == '') {
        return null;
    }

    if (apcu_exists('flags')) {
        $flags = apcu_fetch('flags');
    } else {
        $flags = db_get_array('SELECT * FROM ?:flags');

        apcu_store('flags', $flags, 3600);
    }

    $ean = substr($ean, 0, 3);

    foreach ($flags as $flag) {
        if (strpos($flag['country'], '–') !== false) {
            $range = explode('–', $flag['country']);

            if ($ean >= $range[0] && $ean <= $range[1]) {
                return [
                    'flag' => $flag['flag'],
                    'country' => $flag['country_name']
                ];
            }
        } else {
            if ($ean == $flag['country']) {
                return [
                    'flag' => $flag['flag'],
                    'country' => $flag['country_name']
                ];
            }
        }
    }

    return null;
}

function fn_modify_product_features(&$product) {
    if ($product['flag']['country']) {
        $product['product_features'][1]['subfeatures'][9999999] = array (
            'feature_id' => '9999999',
            'company_id' => Registry::get('runtime.company_id'),
            'feature_type' => 'S',
            'parent_id' => '1',
            'display_on_product' => 'Y',
            'display_on_catalog' => 'Y',
            'display_on_header' => 'N',
            'description' => 'Vendi i origjinës',
            'lang_code' => 'al',
            'prefix' => '',
            'suffix' => '',
            'categories_path' => '',
            'full_description' => NULL,
            'status' => 'A',
            'comparison' => 'Y',
            'position' => '0',
            'group_position' => '0',
            'value' => '',
            'variant_id' => '9999999',
            'value_int' => NULL,
            'variants' =>
                array (
                    9999999 =>
                        array (
                            'variant_id' => '9999999',
                            'variant' => $product['flag']['country'],
                            'description' => NULL,
                            'page_title' => '',
                            'meta_keywords' => '',
                            'meta_description' => '',
                            'lang_code' => 'al',
                            'feature_id' => '12',
                            'url' => '',
                            'position' => '0',
                            'selected' => '9999999',
                            'feature_type' => 'S',
                        ),
                ),
        );
    }
}

function fn_store_session_data($user_id) {
    return;
    $data = [
        'user_id' => $user_id,
        'data' => serialize([
            'cart' => Tygh::$app['session']['cart']['products'],
            'wishlist' => Tygh::$app['session']['wishlist']['products']
        ])
    ];
    db_query('REPLACE INTO ?:sessions_data ?e', $data);
}

function fn_get_session_cookie_value() {
    foreach ($_COOKIE as $name => $value) {
        if (strpos($name, 'sid_customer_') !== false) {
            return $value;
        }
    }
}

function fn_ping_product_view($id, $product_id) {
    $headers = array(
        'headers' => array(
            'ApiKey: cqzW6U6T3YanqWCg',
            'Content-Type: application/json',
        ),
    );

    $body = array(
        'userId' => $id,
        'domain' => 'gjirafa50.com',
        'currentItemId' => $product_id,
        'size' => 0
    );

    Http::post('https://gj50-recommendation.gjirafa.tech/recommend', json_encode($body), $headers);
}

function fn_get_user_recommended_products($id, $html_response = true) {
    $html = '';

//    if (apcu_exists('engine_recommended_products')) {
//        $html = apcu_fetch('engine_recommended_products');
//    } else {
        $headers = array(
            'headers' => array(
                'ApiKey: cqzW6U6T3YanqWCg',
                'Content-Type: application/json',
            ),
        );

        $body = array(
            'userId' => $id,
            'domain' => 'gjirafa50.com',
            'currentItemId' => '0',
            'size' => 10
        );

        $response = Http::post('https://gj50-recommendation.gjirafa.tech/recommend', json_encode($body), $headers);
        $response = json_decode($response, true);
        $product_ids = array_column($response, 'itemId');
        $elastic = new elasticSearch();

        if (!$html_response) {
            return $product_ids;
        }

        $obj = array('where' => [array("field" => "product_id", "values" => $product_ids)], 'limit' => count($product_ids));
        $products = $elastic->sendObject($obj, array(
            'recommended' => true
        ))[0];

        foreach ($response as $product) {
            if (!$products[$product['itemId']]) {
                continue;
            }

            $price = $products[$product['itemId']]['price'];
            $old_price = $products[$product['itemId']]['old_price'];

            if ($old_price <= $price) {
                $old_price = $price;
                $price = 0;
            }

            $product['price'] = $old_price;
            $product['salePrice'] = $price;
            $product['salePercentage'] = round($products[$product['itemId']]['discount']);
            $product['amount'] = $products[$product['itemId']]['amount'];

            Tygh::$app['view']->assign('product', $product);
            $view_html = Tygh::$app['view']->fetch('views/products/single.tpl');
            $html .= preg_replace('/\s+/S', " ", $view_html) . 'delimiter';
        }
//
//        apcu_add('engine_recommended_products', $html, 60);
//    }

    return $html;
}

function fn_display_list_price_discount(&$order_info, $area = 'C') {
    foreach ($order_info['products'] as $key => $product) {
        if ($product['list_price'] > $product['original_price']) {
            $order_info['use_discount'] = true;
            $order_info['products'][$key]['extra']['discount'] += $product['list_price'] - $product['original_price'];
            $order_info['discount'] += ($product['list_price'] - $product['original_price']) * $product['amount'];

            if ($area == 'C') {
                $order_info['products'][$key]['original_price'] = $product['list_price'];
            }
        }
    }
}

function fn_get_user_qbo_credit($order_info) {
    $body = array(
        'PlatformId' => 2,
        'Firstname' => $order_info['b_firstname'],
        'Lastname' => $order_info['b_lastname'],
        'Email' => $order_info['email'],
        'IsCompany' => $order_info['fields'][43] == '' ? false : true,
        'CompanyName' => $order_info['fields'][43] == '' ? '' : $order_info['fields'][43]
    );

    $res = Http::post('https://qboapi.gjirafa.com/api/CustomerCredits', $body);
    $res = json_decode($res);

//    return $res['Result']['CreditValue'];
    return $res->Result[0]->CreditValue;
}

function fn_process_order_totals($request) {
    $payment_total = $request['payment_total'];
    $refund = db_get_field('SELECT order_id FROM ?:orders WHERE order_id = ?i', $request['refund']);
    $gift = $request['gift'];

    if ($payment_total == '') {
        db_query('UPDATE ?:orders SET payment_total = ?i WHERE order_id = ?i', -1, $request['order_id']);
    } else {
        db_query('UPDATE ?:orders SET payment_total = ?i WHERE order_id = ?i', $payment_total, $request['order_id']);
    }

    if ($refund == '') {
        db_query('UPDATE ?:orders SET refund_id = null WHERE order_id = ?i', $request['order_id']);
    } else {
        db_query('UPDATE ?:orders SET refund_id = ?i WHERE order_id = ?i', $refund, $request['order_id']);
        db_query('UPDATE ?:orders SET payment_total = ?i WHERE order_id = ?i', 0, $request['order_id']);
    }

    if ($gift == '') {
        db_query('UPDATE ?:orders SET gift_amount = ?i WHERE order_id = ?i', 0, $request['order_id']);
    } else {
        db_query('UPDATE ?:orders SET gift_amount = ?i WHERE order_id = ?i', $gift, $request['order_id']);

        $total = db_get_field('SELECT total FROM ?:orders WHERE order_id = ?i', $request['order_id']);
        $payment_total_with_gift = $total - $gift;
        db_query('UPDATE ?:orders SET payment_total = ?i WHERE order_id = ?i', $payment_total_with_gift, $request['order_id']);

        db_query('UPDATE ?:orders SET refund_id = null WHERE order_id = ?i', $request['order_id']);
    }

//    if (empty(trim($request['refund']))) {
//        $refund = null;
//        $payment_total = -1;
//    }
//
//    if (!empty(db_get_field('SELECT order_id FROM ?:orders WHERE order_id = ?i', $refund)) || $refund == null) {
//        if ($refund == null) {
//            db_query('UPDATE ?:orders SET refund_id = null WHERE order_id = ?i', $request['order_id']);
//        } else {
//            db_query('UPDATE ?:orders SET refund_id = ?i WHERE order_id = ?i', $refund, $request['order_id']);
//        }
//
//        db_query('UPDATE ?:orders SET payment_total = ?i WHERE order_id = ?i', $payment_total, $request['order_id']);
//        db_query('UPDATE ?:orders SET gift_amount = ?i WHERE order_id = ?i', $payment_total, $request['order_id']);
//    }
//
//    $gift = $request['gift'];
//    if (empty(trim($request['gift']))) {
//        $gift = 0;
//    }
//
//
//    if (empty(trim($request['refund']))) {
//        $total = db_get_field('SELECT total FROM ?:orders WHERE order_id = ?i', $request['order_id']);
//        $payment_total = $total - $request['gift'];
//
//        if ($gift == 0) {
//            $payment_total = -1;
//        }
//
//        db_query('UPDATE ?:orders SET payment_total = ?i WHERE order_id = ?i', $payment_total, $request['order_id']);
//        db_query('UPDATE ?:orders SET gift_amount = ?i WHERE order_id = ?i', $gift, $request['order_id']);
//    }
}

function fn_get_order_items(&$order_info) {
    $order_info['items'] = db_get_array('SELECT * FROM ?:order_details WHERE order_id = ?i', $order_info['order_id']);
}

function fn_get_category_image_link(&$categories_list) {
    foreach ($categories_list as $key => $category) {
        $id_path = str_replace('/', '_', $category['id_path']);
        $name = fn_parse_category_name($category['category']);
        $name = str_replace(' ', '%20', $name);
        $categories_list[$key]['img_url'] = "https://hhstsyoejx.gjirafa.net/gj50/subcategories/{$id_path}%20{$name}.jpg";
    }
}

function fn_get_feature_variants($feature_id) {
    return db_get_array('SELECT variant.variant_id, variant, position FROM ?:product_feature_variants AS variant
                INNER JOIN ?:product_feature_variant_descriptions AS description ON description.variant_id = variant.variant_id
                WHERE feature_id = ?i', $feature_id);
}

function fn_custom_order_delivery_dates($product, $timestamp, $is_product = false, $is_checkout = false) {
    $result = null;
    $local_stock = $is_product ? $product['real_amount'] : $product['in_stock'];

    if ($is_checkout) {
        $local_stock = $product['stock'];
    }

    if (preg_split('#(?<=\d)(?=[a-z])#i', $product['product_code'])[1] == 'at' && $local_stock == 0) {
        $d = date('N', $timestamp);
        $t = date('NHi', $timestamp);

        if ($t <= 41200) {
            if ($d <= 3) {
                $dd = date('m/d/Y', strtotime('this wednesday +1 week', $timestamp));
            } else {
                $dd = date('m/d/Y', strtotime('this wednesday', $timestamp));
            }
        } else {
            $dd = date('m/d/Y', strtotime('this wednesday +1 week', $timestamp));
        }

        $result = array(
            'standard' => $dd,
            'standard_range' => date('m/d/Y', strtotime($dd . ' +' . 1 . ' days')),
            'stock' => 2,
            'timestamp' => time()
        );

        if ($is_product) {
            $result['local'] = $result['standard'];
            $result['local_range'] = $result['standard_range'];
            $result['standard'] = date('m/d/Y', strtotime('+1 day', strtotime($result['standard'])));
            $result['standard_range'] = date('m/d/Y', strtotime('+1 day', strtotime($result['standard_range'])));
        }
    }

    return $result;
}

function fn_get_limited_to_store_products($is_al) {
    if (!$is_al) {
        return null;
    }

    $limited_to_store_products = apcu_fetch('limited_to_store_products');

    if (!apcu_exists('limited_to_store_products') || empty($limited_to_store_products)) {
        $limited_to_store_products = db_get_fields('SELECT product_id FROM ?:products WHERE store_id != ?i', 1);
        apcu_store('limited_to_store_products', $limited_to_store_products, SECONDS_IN_DAY);
    }

    return $limited_to_store_products;
}

function fn_get_checkout_limit() {
    if (fn_isAL()) {
        return AL_CHECKOUT_LIMIT;
    }

    return KS_CHECKOUT_LIMIT;
}

function fn_bank_transfer_order($cart) {
    $limited = false;

    if ($cart['user_data']['fields']['43'] != '' &&
        $cart['user_data']['fields']['51'] != '' &&
        $cart['payment_id'] != 16 &&
        $cart['payment_id'] != 23 &&
        $cart['total'] >= fn_get_checkout_limit()) {
        $limited = true;
    }

    return $limited;
}

function fn_get_order_custom_delivery_dates($product_ids, $timestamp, $cart_products) {
    $delivery_dates = db_get_array('SELECT * FROM ?:delivery_dates WHERE object_id IN(?n) AND object_type = ?s', $product_ids, 'P');

    if (!empty($delivery_dates)) {
        $cd_product_ids = [];
        $cd_cart_products = [];
        foreach ($delivery_dates as $delivery_date) {
            array_push($cd_product_ids, $delivery_date['object_id']);
        }
        $cd_product_ids = array_unique($cd_product_ids);
        foreach ($cart_products as $cart_product) {
            if (in_array($cart_product['product_id'], $cd_product_ids)) {
                array_push($cd_cart_products, $cart_product);
            }
        }
        $has_stock = fn_order_has_stock($cart_products);
        $has_retailer_stock = fn_order_has_retailer_stock($cart_products);

        if ($has_stock && !$has_retailer_stock) {
            $keys = 1;
        } else if (!$has_stock && $has_retailer_stock) {
            $keys = 2;
        } else {
            $keys = 3;
        }

        $periods = [];

        foreach ($delivery_dates as $delivery_date) {
            if ($delivery_date['stock'] == $keys) {
                array_push($periods, $delivery_date['period_in_days']);
            }
        }

        if (!empty($periods)) {
            $days = max($periods);

            return array(
                'standard' => date("m/d/Y", strtotime('+' . $days . ' days', $timestamp)),
                'standard_range' => date("m/d/Y", strtotime('+' . $days + 1 . ' days', $timestamp)),
                'stock' => $keys,
                'timestamp' => time()
            );
        }
    }

    return null;
}

function fn_get_product_custom_delivery_dates($product_data, $has_stock, $has_retailer_stock, $timestamp) {
    if ($product_data['ignore_automatic_delivery_dates'] && !$has_stock) {
        if ($has_stock && !$has_retailer_stock) {
            $days = $product_data['delivery_times'][1];
        } else if ($has_retailer_stock && !$has_stock) {
            $days = $product_data['delivery_times'][2];
        } else {
            $days = $product_data['delivery_times'][3];
        }

        if (!empty($days)) {
            return array(
                'has_stock' => $has_stock,
                'local' => date("m/d/Y", strtotime('+' . $days .' days', $timestamp)),
                'local_range' => date("m/d/Y", strtotime('+' . $days + 1 .' days', $timestamp)),
                'standard' => date("m/d/Y", strtotime('+' . $days + 1 .' days', $timestamp)),
                'standard_range' => date("m/d/Y", strtotime('+' . $days + 2 .' days', $timestamp))
            );
        }
    }

    return null;
}