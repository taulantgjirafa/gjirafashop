<?php

use Tygh\Registry;

class gjirafa_auth_new
{
    private $session_status;
    private $url_redirect;

    public function __construct()
    {

    }

    public function initiate_force_login()
    {
        $urlData = $_SERVER['QUERY_STRING'];
        if($urlData != '')
        {
            $urlData = htmlspecialchars_decode(urldecode($urlData));

            parse_str($urlData, $output);
            $value = $output['v'];

            if($value != null)
            {
                $value = base64_decode($value);
                $userData = json_decode($value,true);

                $this->url_redirect = $userData[0];

                $username = $this->users_info_decoded($userData[1]);
                $username = strrev($username[0]);
                $this->session_login($username);
            }
        }
    }
    public function initiate_force_logout()
    {
        $urlData = $_SERVER['QUERY_STRING'];
        if($urlData != '')
        {
            $urlData = htmlspecialchars_decode(urldecode($urlData));
    
            parse_str($urlData, $output);
            $value = $output['v'];
            if($value != null)
            {
                $value = base64_decode($value);
                $userData = json_decode($value,true);
    
                $this->url_redirect = $userData[0];
            }
            $auth = Tygh::$app['session']['auth'];
            fn_user_logout($auth);
        }
    }
    private function users_info_decoded($data)
    {
        $decoded = base64_decode($data);
        $reverse = strrev($decoded);
        $charAsInt = ord('a');
        $thisWordCodeVerdeeld = str_split($reverse);
        $input = '';
        for ($i = 0; $i < count($thisWordCodeVerdeeld); $i++) {
            $charAsInt = ord($thisWordCodeVerdeeld[$i]);
            $charAsInt += 13;
            $input .= chr($charAsInt);
        }
        $up = explode('|||', $input);
        return $up;
    }

    private function session_login($username){
        $row = db_get_array("SELECT user_id, email, password, salt FROM ?:users WHERE email = ?s", $username);

        if (empty($row)) {
            $firstname = explode("@", $username);
            $user_id = fn_register_user(array($username, 'password', $firstname[0], $firstname[0]));

            $this->fn_sso_login($user_id);

            return;
        }

        $this->fn_sso_login($row[0]['user_id']);
    }
    private function fn_sso_login($user_id){
        if(fn_login_user($user_id) == 1)
        {
            fn_set_notification('N', __('notice'), __('successful_login'));
            $this->session_status = 1;
            return 'True';
        }
        else
        {
            fn_set_notification('E', __('notice'), __('sso_unsuccessful_login'));
            $this->session_status = 0;
            return 'False';
        }
    }

    public function GetRedirection()
    {
        if($this->session_status == 1)
        {
            return $this->url_redirect;
        }
        return '';
    }
}