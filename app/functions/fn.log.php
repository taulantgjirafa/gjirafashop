<?php
/***************************************************************************
 *                                                                          *
 *   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 *                                                                          *
 ****************************************************************************
 * PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
 * "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
 ****************************************************************************/

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use Tygh\Registry;
use Tygh\Settings;
use Tygh\Navigation\LastView;
use Tygh\RabbitMqClient;

if (!defined('BOOTSTRAP')) {
    die('Access denied');
}

fn_define('LOG_MAX_DATA_LENGTH', 10000);

function fn_log_event($type, $action, $data = array())
{
    $object_primary_keys = array(
        'users' => 'user_id',
        'orders' => 'order_id',
        'products' => 'product_id',
        'categories' => 'category_id',
        'promotions' => 'promotion_id'
    );

    $update = false;
    $content = array();

    $actions = Registry::get('settings.Logging.log_type_' . $type);

    $cut_log = Registry::ifGet('log_cut', false);
    Registry::del('log_cut');

    $cut_data = Registry::ifGet('log_cut_data', false);
    Registry::del('log_cut_data');

    $actions['payment_status'] = 'Y';
    $actions['payment_status_callback'] = 'Y';
    $actions['sso'] = 'Y';
    $actions['add'] = 'Y';
    $actions['queued'] = 'Y';
    $actions['update'] = 'Y';
    $actions['create'] = 'Y';
    $actions['render'] = 'Y';
    $actions['delivery_dates'] = 'Y';
    $actions['act_as_user'] = 'Y';

    if (empty($actions) || ($action && !empty($actions) && empty($actions[$action])) || !empty($cut_log)) {
        return false;
    }

    if (!empty(Tygh::$app['session']['auth']['user_id'])) {
        $user_id = Tygh::$app['session']['auth']['user_id'];
    } else {
        $user_id = 0;
    }

    if ($type == 'users' && $action == 'logout' && !empty($data['user_id']) || $type == 'email') {
        $user_id = $data['user_id'];
    }

    if ($user_id) {
        $udata = db_get_row("SELECT firstname, lastname, email FROM ?:users WHERE user_id = ?i", $user_id);
    }

    $event_type = 'N'; // notice

    if (!empty($data['backtrace'])) {
        $_btrace = array();
        $func = '';
        foreach (array_reverse($data['backtrace']) as $v) {
            if (!empty($v['file'])) {
                $v['file'] = fn_get_rel_dir($v['file']);
            }

            if (empty($v['file'])) {
                $func = $v['function'];
                continue;
            } elseif (!empty($func)) {
                $v['function'] = $func;
                $func = '';
            }

            $_btrace[] = array(
                'file' => !empty($v['file']) ? $v['file'] : '',
                'line' => !empty($v['line']) ? $v['line'] : '',
                'function' => $v['function'],
            );
        }

        $data['backtrace'] = serialize($_btrace);
    } else {
        $data['backtrace'] = '';
    }

    if ($type == 'general') {
        if ($action == 'deprecated') {
            $content['deprecated_function'] = $data['function'];
        }
        $content['message'] = $data['message'];
    } elseif ($type == 'orders') {

        $order_status_descr = fn_get_simple_statuses(STATUSES_ORDER, true, true);

        $content = array(
            'order' => '# ' . $data['order_id'],
            'id' => $data['order_id'],
        );

        if (count($data['tags'] > 1)) {
            array_shift($data['tags']);
            $content['tags'] = $data['tags'];
        }

        if ($action == 'status') {
            if(($data['status_from'] != 'N' && $data['status_to'] != 'N' && $_REQUEST['dispatch'] != 'orders_management.save') || $_SESSION['checkout_mode'] == 'checkout'){ // Mos e Log kur osht statusi N , tu u editu porosia , vetem kur te blen per here tpar LOG  N -> O
                $order_id = $data['order_id'];
//                $timestamp = db_get_field("select timestamp from ?:logs where action = 'status' and type = 'orders' and content like '%?i%' ORDER BY log_id DESC limit 1", $order_id);
//                if ($timestamp != '') {
//                    $time_diff = db_get_field("SELECT CONCAT(
//                                            FLOOR(HOUR(TIMEDIFF(CURRENT_TIMESTAMP(), from_unixtime($timestamp))) / 24), ' Ditë ',
//                                            MOD(HOUR(TIMEDIFF(CURRENT_TIMESTAMP(), from_unixtime($timestamp))), 24), ' Orë ',
//                                            MINUTE(TIMEDIFF(CURRENT_TIMESTAMP(), from_unixtime($timestamp))), ' Minuta ',
//                                            SECOND(TIMEDIFF(CURRENT_TIMESTAMP(), from_unixtime($timestamp))), ' Sekonda')");
//
//                    $content['time_diff'] .= $time_diff;
//                }
                $content['status'] = '';
                if (isset($order_status_descr[$data['status_from']])) {
                    $content['status'] = $order_status_descr[$data['status_from']] . ' -> ';
                }
                $content['status'] .= $order_status_descr[$data['status_to']];


                if($data['status_from'] && $data['status_to']){
                    if(!$user_id){
                        $user_id = 0;
                    }
                    $row = array(
                        'user_id' => $user_id,
                        'status_from' => $data['status_from'],
                        'status_to' => $data['status_to'],
                        'order_id' => $data['order_id'],
                        'updated_at' => date("Y-m-d H:i:s")
                    );
                    db_query("INSERT INTO ?:order_logs ?e", $row);
                }
            }
        }

    } elseif ($type == 'products') {

        $product = db_get_field("SELECT product FROM ?:product_descriptions WHERE product_id = ?i AND lang_code = ?s", $data['product_id'], Registry::get('settings.Appearance.backend_default_language'));
        $content = array(
            'user_id' => $_SESSION['auth']['user_id'],
            'product' => $product . ' (#' . $data['product_id'] . ')',
            'id' => $data['product_id'],
            'amount_stock' => $_REQUEST['product_data']['amount'],
            'al_stock' => $_REQUEST['product_data']['al_stock'],
            'czc_stock' => $_REQUEST['product_data']['czc_stock'],
            'czc_retailer_stock' => $_REQUEST['product_data']['czc_retailer_stock'],
            'price' => $_REQUEST['product_data']['price']
        );

        if ($action == 'low_stock') { // log stock - warning
            $event_type = 'W';
        }

    } elseif ($type == 'categories') {

        $category = db_get_field("SELECT category FROM ?:category_descriptions WHERE category_id = ?i AND lang_code = ?s", $data['category_id'], Registry::get('settings.Appearance.backend_default_language'));
        $content = array(
            'category' => $category . ' (#' . $data['category_id'] . ')',
            'id' => $data['category_id'],
        );

    } elseif ($type == 'database') {
        if ($action == 'error') {
            $content = array(
                'error' => $data['error']['message'],
                'query' => $data['error']['query'],
            );
            $event_type = 'E';
        }

    } elseif ($type == 'requests') {
        if (!empty($cut_data)) {
            $data['data'] = preg_replace("/\<(" . implode('|', $cut_data) . ")\>(.*?)\<\/(" . implode('|', $cut_data) . ")\>/s", '<${1}>******</${1}>', $data['data']);
            $data['data'] = preg_replace("/%3C(" . implode('|', $cut_data) . ")%3E(.*?)%3C%2F(" . implode('|', $cut_data) . ")%3E/s", '%3C${1}%3E******%3C%2F${1}%3E', $data['data']);
            $data['data'] = preg_replace("/(" . implode('|', $cut_data) . ")=(.*?)(&)/s", '${1}=******${3}', $data['data']);
        }

        $content = array(
            'url' => $data['url'],
            'request' => (fn_strlen($data['data']) < LOG_MAX_DATA_LENGTH && preg_match('//u', $data['data'])) ? $data['data'] : '',
            'response' => (fn_strlen($data['response']) < LOG_MAX_DATA_LENGTH && preg_match('//u', $data['response'])) ? $data['response'] : '',
        );

    } elseif ($type == 'users') {

        if (!empty($data['time'])) {
            if (empty(Tygh::$app['session']['log']['login_log_id'])) {
                return false;
            }

            $content = db_get_field('SELECT content FROM ?:logs WHERE log_id = ?i', Tygh::$app['session']['log']['login_log_id']);
            $content = unserialize($content);

            $minutes = ceil($data['time'] / 60);

            $hours = floor($minutes / 60);

            if ($hours) {
                $minutes -= $hours * 60;
            }

            if ($hours || $minutes) {
                $content['loggedin_time'] = ($hours ? $hours . ' |hours| ' : '') . ($minutes ? $minutes . ' |minutes|' : '');
            }

            if (!empty($data['timeout']) && $data['timeout']) {
                $content['timeout'] = true;
            }

            $update = Tygh::$app['session']['log']['login_log_id'];

        } else {
            if (!empty($data['user_id'])) {
                $info = db_get_row("SELECT firstname, lastname, email FROM ?:users WHERE user_id = ?i", $data['user_id']);
                $content = array(
                    'user' => $info['firstname'] . ($info['firstname'] && $info['lastname'] ? ' ' : '') . $info['lastname'] . ($info['firstname'] || $info['lastname'] ? '; ' : '') . $info['email'] . ' (#' . $data['user_id'] . ')',
                );
                $content['id'] = $data['user_id'];

                if (count($data['tags']) > 1) {
                    array_shift($data['tags']);
                    $content['tags'] = $data['tags'];
                }

            } elseif (!empty($data['user'])) {
                $content = array(
                    'user' => $data['user'],
                );
            }

            if (in_array($action, array('session', 'failed_login'))) {
                $ip = fn_get_ip();
                $content['ip_address'] = empty($data['ip']) ? $ip['host'] : $data['ip'];
            }
        }

        if ($action == 'failed_login') { // failed login - warning
            $event_type = 'W';
        }

        if ($action == 'sso') {
            $content = $data;
        }
    } elseif ($type == 'promotions') {
        $content = $data;
    } elseif ($type == 'rma') {
        $content = $data;
    } elseif ($type == 'email') {
        $content = $data;
    } elseif ($type == 'pdf') {
        $content = $data;
    } elseif ($type == 'cr_users') {
        $content = $data;
    }

    if ($action == 'delivery_dates') {
        $content = $data;
    } elseif ($action == 'act_as_user') {
        $content = $data;
    }

    fn_set_hook('save_log', $type, $action, $data, $user_id, $content, $event_type, $object_primary_keys);

    $content = serialize($content);

    //if($event_type == 'E' || $event_type == 'W'){
    //	$error_data = '';
    //	$dataerror = '';
    //	try{
    //		$error_data = explode('&', $data['data']);
    //		foreach ($error_data as $key => $value) {
    //			$b = explode('=', $value);
    //			$dataerror = $dataerror . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" . $b[0] . " - " . $b[1] . "<br>";
    //		}
    //	}catch (Exception $e){
    //
    //	}
    //
    //
    //
    //
    //}

    if ($update) {
        db_query('UPDATE ?:logs SET content = ?s WHERE log_id = ?i', $content, $update);
    } else {

        if (Registry::get('runtime.company_id')) {
            $company_id = Registry::get('runtime.company_id');

        } elseif (!empty($object_primary_keys[$type]) && !empty($data[$object_primary_keys[$type]])) {
            $company_id = fn_get_company_id($type, $object_primary_keys[$type], $data[$object_primary_keys[$type]]);

        } else {
            $company_id = 0;
        }

        $row = array(
            'user_id' => $user_id,
            'timestamp' => TIME,
            'type' => $type,
            'action' => $action,
            'event_type' => $event_type,
            'content' => $content,
            'backtrace' => $data['backtrace'],
            'company_id' => $company_id,
        );


        $log_id = db_query("INSERT INTO ?:logs ?e", $row);

        if ($type == 'users' && $action == 'session') {
            Tygh::$app['session']['log']['login_log_id'] = $log_id;
        }

//        if ($type == 'products' && $action == 'update') {
//            $product_data = $_REQUEST['product_data'];
//
//            $product_data['user_id'] = $user_id;
//            $product_data['update_timestamp'] = TIME;
//
////            SEND DATA TO GAZI'S API
//        }
    }


    return true;
}

function fn_send_email_error($params = null, $stringerror = null)
{
    // converted to log errors on file
    $now = date("H:i:s d-m");

    if (!empty($params)) {
        $referer = (isset($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : " ";
        $text = "Error 404 @ Gjirafa50. URL: " . $params['URL'] . " REFERER: " . $referer;

        $error = [
            'message' => $params['URL']
        ];
        fn_log_not_found_error($error);
    }


    error_log($text);
    return;
}

/**
 * Returns store logs
 *
 * @param array $params Search parameters
 * @param int $items_per_page Logs limit
 * @return array Logs with search parameters
 */
function fn_get_logs($params, $items_per_page = 0)
{

    // Init filter
    $params = LastView::instance()->update('logs', $params);

    $default_params = array(
        'page' => 1,
        'items_per_page' => $items_per_page
    );

    $params = array_merge($default_params, $params);

    $sortings = array(
        'timestamp' => array('?:logs.timestamp', '?:logs.log_id'),
        'user' => array('?:users.lastname', '?:users.firstname'),
    );

    $fields = array(
        '?:logs.*',
        '?:users.firstname',
        '?:users.lastname'
    );

    $sorting = db_sort($params, $sortings, 'timestamp', 'desc');

    $join = "LEFT JOIN ?:users USING(user_id)";

    $condition = '';

    if (!empty($params['period']) && $params['period'] != 'A') {
        list($time_from, $time_to) = fn_create_periods($params);

        $condition .= db_quote(" AND (?:logs.timestamp >= ?i AND ?:logs.timestamp <= ?i)", $time_from, $time_to);
    }

    if (!empty($params['q_order_id'])) {
        $condition .= db_quote(" AND action = 'status' and type = 'orders' and content like '%# ?i%'", $params['q_order_id']);
    }

    if (isset($params['q_user']) && fn_string_not_empty($params['q_user'])) {
        $condition .= db_quote(" AND (?:users.lastname LIKE ?l OR ?:users.firstname LIKE ?l)", "%" . trim($params['q_user']) . "%", "%" . trim($params['q_user']) . "%");
    }

    if (!empty($params['q_type'])) {
        $condition .= db_quote(" AND ?:logs.type = ?s", $params['q_type']);
    }

    if (!empty($params['q_action'])) {
        $condition .= db_quote(" AND ?:logs.action = ?s", $params['q_action']);
    }

    if (Registry::get('runtime.company_id')) {
        $condition .= db_quote(" AND ?:logs.company_id = ?i", Registry::get('runtime.company_id'));
    }

    fn_set_hook('admin_get_logs', $params, $condition, $join, $sorting);

    $limit = '';
    if (!empty($params['items_per_page'])) {
        $params['total_items'] = db_get_field("SELECT COUNT(DISTINCT(?:logs.log_id)) FROM ?:logs ?p WHERE 1 ?p", $join, $condition);
        $limit = db_paginate($params['page'], $params['items_per_page'], $params['total_items']);
    }

    $data = db_get_array("SELECT " . join(', ', $fields) . " FROM ?:logs ?p WHERE 1 ?p $sorting $limit", $join, $condition);

    foreach ($data as $k => $v) {

        $data[$k]['backtrace'] = !empty($v['backtrace']) ? unserialize($v['backtrace']) : array();
        $data[$k]['content'] = !empty($v['content']) ? unserialize($v['content']) : array();


    }


    return array($data, $params);
}

/**
 * Gets all available types of logs
 *
 * @return array Log types
 */
function fn_get_log_types()
{
    $types = array();
    $section = Settings::instance()->getSectionByName('Logging');

    $settings = Settings::instance()->getList($section['section_id']);

    foreach ($settings['main'] as $setting_id => $setting_data) {
        $types[$setting_data['name']]['type'] = str_replace('log_type_', '', $setting_data['name']);
        $types[$setting_data['name']]['description'] = $setting_data['description'];
        $types[$setting_data['name']]['actions'] = $setting_data['variants'];
    }

    return $types;
}

function fn_log_fatal_error($error) {
    $error['type'] = 'PHP_FATAL_ERROR';
    fn_send_rabbitmq_message(fn_generate_error_log_body($error), RABBIT_ERRORS_QUEUE_NAME);

    $data = '[' . date('d/m/Y H:i:s') . ' PHP_FATAL_ERROR] ' . $error['message'] . ' on line:' . $error['line'] . PHP_EOL;
    $fp = fopen(DIR_ROOT . '/var/php_fatal_errors_log.txt', 'a');
    fwrite($fp, $data);
}

function fn_log_not_found_error($error) {
    $error['type'] = 'PHP_NOT_FOUND';
    RabbitMqClient::send(fn_generate_error_log_body($error), RABBIT_LOGS_QUEUE_NAME);

    $data = '[' . date('d/m/Y H:i:s') . ' PHP_NOT_FOUND] ' . $error['message'] . PHP_EOL;
    $fp = fopen(DIR_ROOT . '/var/php_fatal_errors_log.txt', 'a');
    fwrite($fp, $data);
}

function fn_generate_error_log_body($error) {
    return [
        'Id' => 19,
        'TrackId' => fn_generate_guid(),
        'Guid' => fn_generate_guid(),
        'Type' => $error['type'],
        'Message' => $error['message'],
        'HostIpAddress' => '',
        'HostMachineName' => '',
        'Url' => '',
        'UrlReferrer' => '',
        'SessionId' => '',
        'UserAgent' => '',
        'UserIpAddress' => '',
        'UserName' => '',
        'DateTime' => date("Y-m-d H:i:s")
    ];
}

function fn_send_rabbitmq_message($body, $queue) {
    $connection = new AMQPStreamConnection(
        RABBIT_HOST,
        RABBIT_PORT,
        RABBIT_USERNAME,
        RABBIT_PASSWORD
    );

    $channel = $connection->channel();

    $msg = new AMQPMessage(
        json_encode($body, JSON_UNESCAPED_SLASHES),
        array('delivery_mode' => 2)
    );

    $channel->basic_publish($msg, '', $queue);
}