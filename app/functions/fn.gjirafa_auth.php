<?php

use Tygh\Registry;

class gjirafa_auth
{

    public function __construct()
    {
        $auth = Tygh::$app['session']['auth'];

        if (isset($_COOKIE['HS'])) {

            if ($auth['user_id'] == 0) {
                $this->session_set_cookie($auth);

                unset($_COOKIE['HS']);
                setcookie('HS', null, -1, '/; samesite=none', '.gjirafa50.com', true, false);

                $headers = apache_request_headers();
                parse_str($headers['Referer'], $output);
                $url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                if ($url) {
                    header("Location:" . $url);
                    exit();
                } else {
                    header("Location: /");
                    exit();
                }

            }
        } elseif (isset($_COOKIE['HU'])) {
            if ($auth['user_id'] != 0) {
                unset($_COOKIE['HU']);
                setcookie('HU', null, -1, '/; samesite=none', '.gjirafa50.com', true, false);

                $url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

//                fn_user_logout($auth, $url);

                $headers = apache_request_headers();
                parse_str($headers['Referer'], $output);
                if ($url) {
                    header("Location:" . $url);
                } else {
                    header("Location: /");
                }
            }
        }
    }

    private function session_set_cookie($auth)
    {
        $up = $this->users_info_decoded();
        $row = db_get_array("SELECT user_id, email, password, salt FROM ?:users WHERE email = ?s", $up[0]);

        if (count($row) == 0) {
            $id = $this->register_user($up);
            $this->fn_sso_login($id);
        } else if (count($row) > 0) {
            $compare_passwords = $this->fn_compare_passwords($row[0]['password'], $row[0]['salt'], $up[1], $up[0]);
            if ($compare_passwords)
                $this->fn_sso_login($row[0]['user_id']);
        }
    }

    private function users_info_decoded()
    {
        $data = $_COOKIE['HS'];
        $decoded = base64_decode($data);
        $reverse = strrev($decoded);
        $charAsInt = ord('a');
        $thisWordCodeVerdeeld = str_split($reverse);
        $input = '';
        for ($i = 0; $i < count($thisWordCodeVerdeeld); $i++) {
            $charAsInt = ord($thisWordCodeVerdeeld[$i]);
            $charAsInt += 13;
            $input .= chr($charAsInt);
        }
        $up = explode('|||', $input);
        return $up;
    }

    private function register_user($request_user_data)
    {
        try {
            if (count($request_user_data) >= 4 && $this->check_user_data_fields($request_user_data)) {
                $user_data['is_root'] = 'N';
                $user_data['salt'] = fn_generate_salt();
                $user_data['password'] = fn_generate_salted_password($request_user_data[1], $user_data['salt']);
                $user_data['lang_code'] = !empty($user_data['lang_code']) ? $user_data['lang_code'] : CART_LANGUAGE;
                $user_data['timestamp'] = TIME;
                $user_data['company_id'] = 1;
                $user_data['email'] = $request_user_data[0];
                $user_data['firstname'] = $request_user_data[2];
                $user_data['lastname'] = $request_user_data[3];
                $user_id = db_query("INSERT INTO ?:users ?e", $user_data);

                if (empty($user_data['user_login'])) { // if we're using email as login or user type does not require login, fill login field
                    db_query("UPDATE ?:users SET user_login = 'user_?i' WHERE user_id = ?i AND user_login = ''", $user_id, $user_id);
                }

                db_query("INSERT INTO ?:user_profiles (user_id, profile_type) values ('$user_id','P')");
                if (!empty(Tygh::$app['session']['cart'])) {
                    fn_save_cart_content(Tygh::$app['session']['cart'], $user_id);
                }
            } else {
                $user_id = '';
            }
        } catch (Exception $e) {
            $user_id = '';
        }

        return $user_id;
    }

    private function check_user_data_fields($params)
    {
        for ($i = 0; $i < count($params); $i++) {
            if ($params[$i] == null && $i != 4) {
                return false;
            }
        }
        return true;
    }

    private function fn_sso_login($user_id)
    {
        if (fn_login_user($user_id) == 1)
            fn_set_notification('N', __('notice'), __('successful_login'));
        else
            fn_set_notification('E', __('notice'), __('sso_unsuccessful_login'));
    }

    private function fn_compare_passwords($db_password, $salt, $request_password, $user_id)
    {

        $salted_request_password = fn_generate_salted_password($request_password, $salt);

        if ($db_password == $salted_request_password)
            return true;
        else
            $this->fn_update_password_on_login($user_id, $request_password);
    }

    private function fn_update_password_on_login($user_id, $password)
    {
        $salt = fn_generate_salt();
        $data = array(
            'password' => fn_generate_salted_password($password, $salt),
            'salt' => $salt
        );

        $a = db_query('UPDATE ?:users SET ?u WHERE email = ?s', $data, $user_id);

        if ($a > 0)
            return true;

        return false;
    }

}