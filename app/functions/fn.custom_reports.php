<?php

use Tygh\Registry;

if (!defined('BOOTSTRAP')) {
    die('Access denied');
}

function fn_get_user_sales_reports($params)
{
    $company_id = Registry::get('runtime.company_id');
    $results_per_page = 20;

    if (isset($params["page"])) {
        $page = $params["page"];
    } else {
        $page = 1;
    };
    $start_from = ($page - 1) * $results_per_page;

    if (isset($params['def_statuses']) and $params['def_statuses'] == 'true') {
        $statuses = (string)'"P","C","O","Y","A","E","G","H","J","K","L","M","Q","R","S"';
    } else {
        if (isset($params['statuses']) && $params['statuses'] != '' || $params['def_statuses'] == 'true') {
            foreach ($params['statuses'] as $status) {
                $statuses[] = "'$status'";
            }
        }
    }

    $fields = array(
        "?:users.user_id",
        "?:users.firstname",
        "?:users.lastname",
        "?:users.email",
        "count(order_id) as sales"
    );
    $joins = array(
        "inner join ?:users on ?:users.user_id = ?:orders.user_id"
    );
    $condition = array();
    $group_by = array(
        "group by ?:orders.user_id"
    );
    $order_by = array(
        "order by sales desc"
    );

    $limit = array(
        "Limit $start_from,$results_per_page"
    );

    $imploded_fields = implode(', ', $fields);
    $imploded_joins = implode(' ', $joins);
    $imploded_conditions = implode(' where ', $condition);
    $imploded_group_by = implode(' ', $group_by);
    $imploded_order_by = implode(' ', $order_by);
    $imploded_limit = implode(' ', $limit);
    if ($params['def_statuses'] != 'false' && isset($params['def_statuses'])) {
        $imploded_statuses = $statuses;
    } else {
        $imploded_statuses = implode(', ', $statuses);
    }
    if (!empty($company_id)) {
        $imploded_conditions .= " where ?:orders.company_id = $company_id";
    } else {
        $imploded_conditions .= " where ?:orders.company_id = 1";
    }
    if (isset($params['statuses']) && $params['statuses'] != '' || $params['def_statuses'] == 'true') {
        $imploded_conditions .= " and ?:orders.status IN($imploded_statuses)";
    }
    if (isset($params['time_from']) && $params['time_from'] != '' || isset($params['time_to']) && $params['time_to'] != '') {
        $time_from = strtotime(date('Y-m-d', strtotime(str_replace('/', '-', $params['time_from']))));
        $time_to = strtotime(date('Y-m-d', strtotime(str_replace('/', '-', $params['time_to']))));
        if ($time_to < 0) {
            $imploded_conditions .= " and ?:orders.timestamp >='$time_from'";
        } else {
            $imploded_conditions .= " and ?:orders.timestamp between '$time_from' and '$time_to'";
        }
    }
    if (isset($params['firstname']) && $params['firstname'] != '') {
        $firstname = (string)$params['firstname'];
        $imploded_conditions .= " and ?:orders.firstname = '$firstname'";
    }
    if (isset($params['lastname']) && $params['lastname'] != '') {
        $last_name = (string)$params['lastname'];
        $imploded_conditions .= " and ?:orders.lastname = '$last_name'";
    }
    if (isset($params['email']) && $params['email'] != '') {
        $email = (string)$params['email'];
        $imploded_conditions .= " and ?:orders.email = '$email'";
    }
    if (isset($params['user_id']) && $params['user_id'] != '') {
        $user_id = (int)$params['user_id'];
        $imploded_conditions .= " and ?:users.user_id = '$user_id'";
    }

    $all_page_data = db_get_array("select " . $imploded_fields . ' from ?:orders ' . $imploded_joins . ' ' . $imploded_conditions . ' ' . $imploded_group_by . ' ' . $imploded_order_by . ' ');
    $result = db_get_array("select " . $imploded_fields . ' from ?:orders ' . $imploded_joins . ' ' . $imploded_conditions . ' ' . $imploded_group_by . ' ' . $imploded_order_by . ' ' . $imploded_limit . '');
    $user_order_details = db_get_array("select order_id from ?:orders where user_id = ?i", $user_id);

    if ($params['export'] == 'true') {
        fn_export_to_xls('Shitjet', $all_page_data, $params, 'time_from', 'time_to');
    }

    $total_pages = count($all_page_data);
    $results = array(
        "total_page" => $total_pages,
        "result" => $result,
        "user_order_details" => $user_order_details
    );
    return $results;
}

function fn_get_detailed_orders($user_id)
{
    $company_id = fn_get_company_id();
    $order_details = db_get_array("select order_id,user_id,timestamp,total from ?:orders where user_id = ?i and company_id = 1 order by order_id desc", $user_id);
    $order_count = db_get_field("select sum(total) as order_count from ?:orders where user_id = ?i and status = 'C'", $user_id);
    $results = array(
        "order_details" => $order_details,
        "order_count" => $order_count
    );
    return $results;
}

function fn_get_product_sales_reports($params)
{

    $company_id = Registry::get('runtime.company_id');

    $results_per_page = 20;

    if (isset($params["page"])) {
        $page = $params["page"];
    } else {
        $page = 1;
    };
    $start_from = ($page - 1) * $results_per_page;

    if (isset($params['def_statuses']) and $params['def_statuses'] == 'true') {
        $statuses = (string)'"P","C","O","Y","A","E","G","H","J","K","L","M","Q","R","S"';
    } else {
        if (isset($params['statuses']) && $params['statuses'] != '') {
            foreach ($params['statuses'] as $status) {
                $statuses[] = "'$status'";
            }
        }
    }

    $fields = array(
        "?:product_descriptions.product_id",
        "product_code",
        "product",
        "sum(?:order_details.amount) as sasia",
        "(price * sum(?:order_details.amount)) as total"
    );
    $joins = array(
        " inner join ?:orders on ?:order_details.order_id = ?:orders.order_id",
        "inner join ?:product_descriptions on ?:order_details.product_id = ?:product_descriptions.product_id",
    );
    $condition = array();
    $group_by = array(
        "group by ?:order_details.product_id"
    );
    $order_by = array(
        " order by sasia desc"
    );

    $limit = array(
        "Limit $start_from,$results_per_page"
    );

    $imploded_fields = implode(', ', $fields);
    $imploded_joins = implode(' ', $joins);
    $imploded_conditions = implode(' where', $condition);
    $imploded_group_by = implode(' ', $group_by);
    $imploded_order_by = implode(' ', $order_by);
    $imploded_limit = implode(' ', $limit);
    if ($params['def_statuses'] != 'false' && isset($params['def_statuses'])) {
        $imploded_statuses = $statuses;
    } else {
        $imploded_statuses = implode(', ', $statuses);
    }
    if (!empty($company_id)) {
        $imploded_conditions .= " where company_id = $company_id";
    } else {
        $imploded_conditions .= " where company_id = 1";
    }
    if (isset($params['statuses']) && $params['statuses'] != '' || $params['def_statuses'] == 'true') {
        $imploded_conditions .= " and status IN($imploded_statuses)";
    }

    if (isset($params['time_from']) && $params['time_from'] != '' || isset($params['time_to']) && $params['time_to'] != '') {

        $time_from = strtotime(date('Y-m-d', strtotime(str_replace('/', '-', $params['time_from']))));
        $time_to = strtotime(date('Y-m-d', strtotime(str_replace('/', '-', $params['time_to']))));
        if ($time_to < 0) {
            $imploded_conditions .= " and timestamp >='$time_from'";
        } else {
            $imploded_conditions .= " and timestamp between '$time_from' and '$time_to'";
        }
    }
    $all_page_data = db_get_array("select " . $imploded_fields . ' from ?:order_details ' . $imploded_joins . $imploded_conditions . ' ' . $imploded_group_by . ' ' . $imploded_order_by . ' ');
    $results = db_get_array("select " . $imploded_fields . ' from ?:order_details ' . $imploded_joins . $imploded_conditions . ' ' . $imploded_group_by . ' ' . $imploded_order_by . ' ' . $imploded_limit . '');

    if ($params['export'] == 'true') {
        fn_export_to_xls('Shitjet', $params, 'time_from', 'time_to');
    }

    $total_pages = count($all_page_data);
    $obj_results = array(
        "total_page" => $total_pages,
        "result" => $results
    );

    return $obj_results;
}

function fn_get_user_stats_report($params)
{
    $company_id = Registry::get('runtime.company_id');
    $results_per_page = 20;

    if (isset($params["page"])) {
        $page = $params["page"];
    } else {
        $page = 1;
    };
    $start_from = ($page - 1) * $results_per_page;

    if (isset($params['def_statuses']) and $params['def_statuses'] == 'true') {
        $statuses = (string)'"P","C","O","Y","A","E","G","H","J","K","L","M","Q","R","S"';
    } else if ($params['def_statuses'] == null && !isset($params['statuses'])) {
        $statuses = (string)'"P","C","O","Y","A","E","G","H","J","K","L","M","Q","R","S"';
    } else {
        if (isset($params['statuses']) && $params['statuses'] != '' || $params['def_statuses'] == 'true') {
            foreach ($params['statuses'] as $status) {
                $statuses[] = "'$status'";
            }
        }
    }


    $fields = array(
        "?:users.user_id",
        "?:users.firstname",
        "?:users.lastname",
        "?:users.email",
        "count(order_id) as sales"
    );
    $joins = array(
        "inner join ?:users on ?:users.user_id = ?:orders.user_id"
    );
    $condition = array();
    $group_by = array(
        "group by ?:orders.user_id"
    );
    $order_by = array(
        "order by sales desc"
    );

    $limit = array(
        "Limit $start_from,$results_per_page"
    );

    $imploded_fields = implode(', ', $fields);
    $imploded_joins = implode(' ', $joins);
    $imploded_conditions = implode(' where ', $condition);
    $imploded_group_by = implode(' ', $group_by);
    $imploded_order_by = implode(' ', $order_by);
    $imploded_limit = implode(' ', $limit);
    if ($params['def_statuses'] != 'false' && isset($params['def_statuses']) || $params['def_statuses'] == null) {
        $imploded_statuses = $statuses;
    } else {
        $imploded_statuses = implode(', ', $statuses);
    }

    if (!empty($company_id)) {
        $imploded_conditions .= " where ?:orders.company_id = $company_id";
    } else {
        $imploded_conditions .= " where ?:orders.company_id = 1";
    }
    if (isset($params['statuses']) && $params['statuses'] != '' || $params['def_statuses'] == 'true') {
        $imploded_conditions .= " and ?:orders.status IN($imploded_statuses)";
    }

    if (isset ($params['period_one_time_from']) && !empty($params['period_one_time_from']) && isset($params['period_two_time_from']) && !empty($params['period_two_time_from'])) {

        $p_one_time_from = strtotime(date('Y-m-d', strtotime(str_replace('/', '-', $params['period_one_time_from']))));
        $p_one_time_to = strtotime(date('Y-m-d', strtotime(str_replace('/', '-', $params['period_one_time_to']))));

        $p_two_time_from = strtotime(date('Y-m-d', strtotime(str_replace('/', '-', $params['period_two_time_from']))));
        $p_two_time_to = strtotime(date('Y-m-d', strtotime(str_replace('/', '-', $params['period_two_time_to']))));

        if ($params['filter'] == 'bp_two_not_one') {
            $imploded_conditions .= " and ?:orders.timestamp between '$p_two_time_from' and '$p_two_time_to' and ?:orders.user_id NOT IN ( select distinct ?:orders.user_id from ?:orders where ?:orders.status IN($imploded_statuses) and ?:orders.timestamp between $p_one_time_from and $p_one_time_to)";
        } elseif ($params['filter'] == 'bp_one_not_two') {
            $imploded_conditions .= " and ?:orders.timestamp between '$p_one_time_from' and '$p_one_time_to' and ?:orders.user_id NOT IN ( select distinct ?:orders.user_id from ?:orders where ?:orders.status IN($imploded_statuses) and ?:orders.timestamp between $p_two_time_from and $p_two_time_to)";

        }

    }

    $all_page_data = db_get_array("select " . $imploded_fields . ' from ?:orders ' . $imploded_joins . ' ' . $imploded_conditions . ' ' . $imploded_group_by . ' ' . $imploded_order_by . ' ');
    $result = db_get_array("select " . $imploded_fields . ' from ?:orders ' . $imploded_joins . ' ' . $imploded_conditions . ' ' . $imploded_group_by . ' ' . $imploded_order_by . ' ' . $imploded_limit . '');


    if ($params['export'] == 'true') {
        fn_export_to_xls('Shitjet', $all_page_data, $params, 'period_one_time_from', 'period_one_time_to');
    }

    $total_pages = count($all_page_data);
    $results = array(
        "total_page" => $total_pages,
        "result" => $result
    );
    return $results;
}

function fn_get_monthly_users($params)
{
    $results_per_page = 20;

    if (isset($params["page"])) {
        $page = $params["page"];
    } else {
        $page = 1;
    };
    $start_from = ($page - 1) * $results_per_page;

    $fields = array(
        "COUNT(*) AS count",
        "MONTH(FROM_UNIXTIME(" . $params['filter'] . ")) AS month",
        "YEAR(FROM_UNIXTIME(" . $params['filter'] . ")) AS year"
    );

    $conditions = array();

    if (isset($params['months']) && trim($params['months'])) {
        array_push($conditions, "MONTH(FROM_UNIXTIME(" . $params['filter'] . ")) = " . $params['months']);
    }

    if (isset($params['years']) && trim($params['years'])) {
        array_push($conditions, 'YEAR(FROM_UNIXTIME(' . $params['filter'] . ')) = ' . $params['years']);
    }

    $group_by = array(
        "GROUP BY MONTH(FROM_UNIXTIME(" . $params['filter'] . ")), YEAR(FROM_UNIXTIME(" . $params['filter'] . "))",
    );

    $order_by = array(
        "ORDER BY YEAR(FROM_UNIXTIME(" . $params['filter'] . ")) DESC, MONTH(FROM_UNIXTIME(" . $params['filter'] . ")) DESC"
    );

    $limit = array(
        "LIMIT $start_from, $results_per_page"
    );

    $imploded_fields = implode(', ', $fields);
    $imploded_joins = implode(' ', $joins);
    if (!empty($conditions)) {
        $imploded_conditions = ' WHERE ' . implode(' AND ', $conditions);
    }
    $imploded_group_by = implode(' ', $group_by);
    $imploded_order_by = implode(' ', $order_by);
    $imploded_limit = implode(' ', $limit);

    $all_page_data = db_get_array("SELECT " . $imploded_fields . ' FROM ?:users ' . $imploded_joins . ' ' . $imploded_conditions . ' ' . $imploded_group_by . ' ' . $imploded_order_by . '');
    $result = db_get_array("SELECT " . $imploded_fields . ' FROM ?:users ' . $imploded_joins . ' ' . $imploded_conditions . ' ' . $imploded_group_by . ' ' . $imploded_order_by . ' ' . $imploded_limit . '');

    if ($params['export'] == 'true') {
        fn_export_to_xls('Perdoruesit per muaj', $all_page_data, $params);
    }

    $total_pages = count($all_page_data);
    $results = array(
        "total_page" => $total_pages,
        "result" => $result
    );

    return $results;
}

function fn_get_monthly_sales($params)
{
    $results_per_page = 20;

    if (isset($params["page"])) {
        $page = $params["page"];
    } else {
        $page = 1;
    };
    $start_from = ($page - 1) * $results_per_page;

    $fields = array(
        "COUNT(a.order_id) AS count",
        "SUM(a.total) AS sum",
        "MONTH(FROM_UNIXTIME(a.timestamp)) AS month",
        "DAY(FROM_UNIXTIME(a.timestamp)) AS day",
        "YEAR(FROM_UNIXTIME(a.timestamp)) AS year"
    );

    $conditions = array(
        "a.status NOT IN ('P','O','F','D','B','I','Y','U','1','N')",
        "a.company_id = 1"
    );

    if (isset($params['months']) && trim($params['months'])) {
        array_push($conditions, "MONTH(FROM_UNIXTIME(a.TIMESTAMP)) = " . $params['months']);
    }

    if (isset($params['years']) && trim($params['years'])) {
        array_push($conditions, 'YEAR(FROM_UNIXTIME(a.TIMESTAMP)) = ' . $params['years']);
    }

    $group_by = array(
        "DAY(FROM_UNIXTIME(a.timestamp))",
        "MONTH(FROM_UNIXTIME(a.timestamp))",
        "YEAR(FROM_UNIXTIME(a.timestamp))"
    );

    $order_by = array(
        "YEAR(FROM_UNIXTIME(a.timestamp)) DESC",
        "MONTH(FROM_UNIXTIME(a.timestamp)) DESC",
        "DAY(FROM_UNIXTIME(a.timestamp))"
    );

    $limit = array(
        "LIMIT $start_from, $results_per_page"
    );

    $imploded_fields = implode(', ', $fields);
    $imploded_joins = implode(' ', $joins);
    if (!empty($conditions)) {
        $imploded_conditions = ' WHERE ' . implode(' AND ', $conditions);
    }
    if (!empty($group_by)) {
        $imploded_group_by = ' GROUP BY ' . implode(', ', $group_by);
    }
    if (!empty($order_by)) {
        $imploded_order_by = ' ORDER BY ' . implode(', ', $order_by);
    }
    $imploded_limit = implode(' ', $limit);

    $all_page_data = db_get_array("SELECT " . $imploded_fields . ' FROM ?:orders AS a ' . $imploded_joins . ' ' . $imploded_conditions . ' ' . $imploded_group_by . ' ' . $imploded_order_by . '');
    $result = db_get_array("SELECT " . $imploded_fields . ' FROM ?:orders AS a ' . $imploded_joins . ' ' . $imploded_conditions . ' ' . $imploded_group_by . ' ' . $imploded_order_by . ' ' . $imploded_limit . '');

    if ($params['export'] == 'true') {
        fn_export_to_xls('Shitjet per muaj', $all_page_data, $params);
    }

    $total_pages = count($all_page_data);
    $results = array(
        "total_page" => $total_pages,
        "result" => $result
    );

    return $results;
}

function fn_get_unique_user_sales($params)
{
    $results_per_page = 20;

    if (isset($params["page"])) {
        $page = $params["page"];
    } else {
        $page = 1;
    };
    $start_from = ($page - 1) * $results_per_page;

    $fields = array(
        "COUNT(order_id) count",
        "COUNT(DISTINCT user_id) AS user",
        "MONTH(FROM_UNIXTIME(timestamp)) AS month",
        "YEAR(FROM_UNIXTIME(a.timestamp)) AS year"
    );

    $conditions = array(
        "status NOT IN ('P','O','F','D','B','I','Y','U','1','N')",
        "company_id = 1"
    );

    if (isset($params['months']) && trim($params['months'])) {
        array_push($conditions, "MONTH(FROM_UNIXTIME(timestamp)) = " . $params['months']);
    }

    if (isset($params['years']) && trim($params['years'])) {
        array_push($conditions, 'YEAR(FROM_UNIXTIME(timestamp)) = ' . $params['years']);
    }

    $group_by = array(
        "MONTH(FROM_UNIXTIME(timestamp))",
        "YEAR(FROM_UNIXTIME(timestamp))"
    );

    $order_by = array(
        "YEAR(FROM_UNIXTIME(timestamp)) DESC",
        "MONTH(FROM_UNIXTIME(timestamp)) DESC"
    );

    $limit = array(
        "LIMIT $start_from, $results_per_page"
    );

    $imploded_fields = implode(', ', $fields);
    $imploded_joins = implode(' ', $joins);
    if (!empty($conditions)) {
        $imploded_conditions = ' WHERE ' . implode(' AND ', $conditions);
    }
    if (!empty($group_by)) {
        $imploded_group_by = ' GROUP BY ' . implode(', ', $group_by);
    }
    if (!empty($order_by)) {
        $imploded_order_by = ' ORDER BY ' . implode(', ', $order_by);
    }
    $imploded_limit = implode(' ', $limit);

    $all_page_data = db_get_array("SELECT " . $imploded_fields . ' FROM ?:orders AS a ' . $imploded_joins . ' ' . $imploded_conditions . ' ' . $imploded_group_by . ' ' . $imploded_order_by . '');
    $result = db_get_array("SELECT " . $imploded_fields . ' FROM ?:orders AS a ' . $imploded_joins . ' ' . $imploded_conditions . ' ' . $imploded_group_by . ' ' . $imploded_order_by . ' ' . $imploded_limit . '');

    if ($params['export'] == 'true') {
        fn_export_to_xls('Shitjet per perdorues unik', $all_page_data, $params);
    }

    $total_pages = count($all_page_data);
    $results = array(
        "total_page" => $total_pages,
        "result" => $result
    );

    return $results;
}

function fn_get_brand_sales($params)
{
    $results_per_page = 20;

    if (isset($params["page"])) {
        $page = $params["page"];
    } else {
        $page = 1;
    };
    $start_from = ($page - 1) * $results_per_page;

    $fields = array(
        "orders.product_id AS id",
        "orders.product_code AS code",
        "REPLACE(?:product_descriptions.product, ',', '') AS description",
        "price",
        "SUM(orders.amount) AS total",
        "price * SUM(orders.amount) AS total_price"
    );

    $joins = array(
        "LEFT JOIN ?:product_descriptions ON orders.product_id = ?:product_descriptions.product_id",
        "LEFT JOIN ?:orders AS gjo ON orders.order_id = gjo.order_id"
    );

    $conditions = array(
        "gjo.status NOT  IN ('P','O','F','D','B','I','Y','U','1','N')",
        "gjo.company_id = 1"
    );

    if (isset($params['brand_name']) && trim($params['brand_name'])) {
        array_push($conditions, "orders.product_id IN (SELECT product_id FROM ?:product_descriptions WHERE product LIKE '%" . $params['brand_name'] . "%')");
    }

    $group_by = array(
        "id"
    );

    $order_by = array(
        "total DESC"
    );

    $limit = array(
        "LIMIT $start_from, $results_per_page"
    );

    $imploded_fields = implode(', ', $fields);
    $imploded_joins = implode(' ', $joins);
    if (!empty($conditions)) {
        $imploded_conditions = ' WHERE ' . implode(' AND ', $conditions);
    }
    if (!empty($group_by)) {
        $imploded_group_by = ' GROUP BY ' . implode(', ', $group_by);
    }
    if (!empty($order_by)) {
        $imploded_order_by = ' ORDER BY ' . implode(', ', $order_by);
    }
    $imploded_limit = implode(' ', $limit);

    $all_page_data = db_get_array("SELECT " . $imploded_fields . ' FROM ?:order_details AS orders ' . $imploded_joins . ' ' . $imploded_conditions . ' ' . $imploded_group_by . ' ' . $imploded_order_by . '');
    $result = db_get_array("SELECT " . $imploded_fields . ' FROM ?:order_details AS orders ' . $imploded_joins . ' ' . $imploded_conditions . ' ' . $imploded_group_by . ' ' . $imploded_order_by . ' ' . $imploded_limit . '');

    if ($params['export'] == 'true') {
        fn_export_to_xls('Shitjet per brende', $all_page_data, $params);
    }

    $total_pages = count($all_page_data);
    $results = array(
        "total_page" => $total_pages,
        "result" => $result
    );

    return $results;
}

function fn_get_recurring_buyers($params)
{
    $results_per_page = 20;

    if (isset($params["page"])) {
        $page = $params["page"];
    } else {
        $page = 1;
    };
    $start_from = ($page - 1) * $results_per_page;

    $fields = array(
        "COUNT(*) AS count",
        "a.month",
        "a.year"
    );

    $from = "(SELECT count(*), user_id as uid, MONTH(FROM_UNIXTIME(timestamp)) as month, YEAR(FROM_UNIXTIME(timestamp)) as YEAR
            FROM ?:orders
            WHERE status NOT IN ('P','O','F','D','B','I','Y','U','1','N')
            AND ?:orders.company_id = 1
            GROUP BY user_id, MONTH(FROM_UNIXTIME(timestamp)), YEAR(FROM_UNIXTIME(TIMESTAMP))
            HAVING COUNT(*) > 1
            ORDER BY YEAR(FROM_UNIXTIME(timestamp)), MONTH(FROM_UNIXTIME(TIMESTAMP)))";

    $joins = array();

    $conditions = array();

    if (isset($params['months']) && trim($params['months'])) {
        array_push($conditions, "a.month = " . $params['months']);
    }

    if (isset($params['years']) && trim($params['years'])) {
        array_push($conditions, "a.year = " . $params['years']);
    }

    $group_by = array(
        "a.year DESC",
        "a.month DESC"
    );

    $order_by = array();

    $limit = array(
        "LIMIT $start_from, $results_per_page"
    );

    $imploded_fields = implode(', ', $fields);
    $imploded_joins = implode(' ', $joins);
    if (!empty($conditions)) {
        $imploded_conditions = ' WHERE ' . implode(' AND ', $conditions);
    }
    if (!empty($group_by)) {
        $imploded_group_by = ' GROUP BY ' . implode(', ', $group_by);
    }
    if (!empty($order_by)) {
        $imploded_order_by = ' ORDER BY ' . implode(', ', $order_by);
    }
    $imploded_limit = implode(' ', $limit);

    $all_page_data = db_get_array("SELECT " . $imploded_fields . ' FROM ' . $from . ' AS a ' . $imploded_joins . ' ' . $imploded_conditions . ' ' . $imploded_group_by . ' ' . $imploded_order_by . '');
    $result = db_get_array("SELECT " . $imploded_fields . ' FROM ' . $from . ' AS a ' . $imploded_joins . ' ' . $imploded_conditions . ' ' . $imploded_group_by . ' ' . $imploded_order_by . ' ' . $imploded_limit . '');

    if ($params['export'] == 'true') {
        fn_export_to_xls('Përdoruesit me më shumë se dy blerje', $all_page_data, $params);
    }

    $total_pages = count($all_page_data);
    $results = array(
        "total_page" => $total_pages,
        "result" => $result
    );

    return $results;
}

function fn_get_product_ratings($params)
{
    $results_per_page = 20;

    if (isset($params["page"])) {
        $page = $params["page"];
    } else {
        $page = 1;
    };
    $start_from = ($page - 1) * $results_per_page;

    $fields = array(
        "a.object_id AS product_id",
        "ROUND(AVG(b.rating_value), 2) AS rating"
    );

    $joins = array(
        "INNER JOIN gjshop_discussion_rating AS b ON a.thread_id = b.thread_id"
    );

    $conditions = array(
        "a.type = 'B' and a.object_type = 'P'",
        "b.rating_value > 0"
    );

    if (isset($params['product_id']) && trim($params['product_id'])) {
        array_push($conditions, "a.object_id = " . $params['product_id']);
    }

    if (isset($params['ratings']) && trim($params['ratings'])) {
        array_push($conditions, "b.rating_value = " . $params['ratings']);
    }

    $group_by = array(
        "product_id"
    );

    $order_by = array();

    $limit = array(
        "LIMIT $start_from, $results_per_page"
    );

    $imploded_fields = implode(', ', $fields);
    $imploded_joins = implode(' ', $joins);
    if (!empty($conditions)) {
        $imploded_conditions = ' WHERE ' . implode(' AND ', $conditions);
    }
    if (!empty($group_by)) {
        $imploded_group_by = ' GROUP BY ' . implode(', ', $group_by);
    }
    if (!empty($order_by)) {
        $imploded_order_by = ' ORDER BY ' . implode(', ', $order_by);
    }
    $imploded_limit = implode(' ', $limit);

    $all_page_data = db_get_array("SELECT " . $imploded_fields . ' FROM ?:discussion AS a ' . $imploded_joins . ' ' . $imploded_conditions . ' ' . $imploded_group_by . ' ' . $imploded_order_by . '');
    $result = db_get_array("SELECT " . $imploded_fields . ' FROM ?:discussion AS a ' . $imploded_joins . ' ' . $imploded_conditions . ' ' . $imploded_group_by . ' ' . $imploded_order_by . ' ' . $imploded_limit . '');

    if ($params['export'] == 'true') {
        fn_export_to_xls('Vleresimet per produkte', $all_page_data, $params);
    }

    $total_pages = count($all_page_data);
    $results = array(
        "total_page" => $total_pages,
        "result" => $result
    );

    return $results;
}

function fn_get_yearly_sales($params)
{
    $results_per_page = 20;

    if (isset($params["page"])) {
        $page = $params["page"];
    } else {
        $page = 1;
    };
    $start_from = ($page - 1) * $results_per_page;

    $fields = array(
        "order_id",
        "firstname",
        "lastname",
        "email",
        "subtotal",
        "subtotal_discount",
        "discount",
        "total",
        "CASE WHEN company_id = 19 THEN 'Shqiperi'
        WHEN company_id = 1 THEN 'Kosove'
        END AS store",
        "DATE_FORMAT(FROM_UNIXTIME(`timestamp`), '%e %b %Y') AS date"
    );

    $joins = array();

    $conditions = array();

    if (isset($params['years']) && trim($params['years'])) {
        array_push($conditions, "YEAR(FROM_UNIXTIME(timestamp)) = " . $params['years']);
    }

    if (isset($params['months']) && trim($params['months'])) {
        array_push($conditions, "MONTH(FROM_UNIXTIME(timestamp)) = " . $params['months']);
    }

    $group_by = array();

    $order_by = array(
        "YEAR(FROM_UNIXTIME(timestamp)) DESC"
    );

    $limit = array(
        "LIMIT $start_from, $results_per_page"
    );

    $imploded_fields = implode(', ', $fields);
    $imploded_joins = implode(' ', $joins);
    if (!empty($conditions)) {
        $imploded_conditions = ' WHERE ' . implode(' AND ', $conditions);
    }
    if (!empty($group_by)) {
        $imploded_group_by = ' GROUP BY ' . implode(', ', $group_by);
    }
    if (!empty($order_by)) {
        $imploded_order_by = ' ORDER BY ' . implode(', ', $order_by);
    }
    $imploded_limit = implode(' ', $limit);

    $all_page_data = db_get_array("SELECT " . $imploded_fields . ' FROM ?:orders ' . $imploded_joins . ' ' . $imploded_conditions . ' ' . $imploded_group_by . ' ' . $imploded_order_by . '');
    $result = db_get_array("SELECT " . $imploded_fields . ' FROM ?:orders ' . $imploded_joins . ' ' . $imploded_conditions . ' ' . $imploded_group_by . ' ' . $imploded_order_by . ' ' . $imploded_limit . '');

    if ($params['export'] == 'true') {
        fn_export_to_xls('Shitjet per vit', $all_page_data, $params);
    }

    $total_pages = count($all_page_data);
    $results = array(
        "total_page" => $total_pages,
        "result" => $result
    );

    return $results;
}

function fn_get_order_details($params)
{
    $results_per_page = 20;

    if (isset($params["page"])) {
        $page = $params["page"];
    } else {
        $page = 1;
    };
    $start_from = ($page - 1) * $results_per_page;

    $fields = array(
        "a.order_id",
        "user_id",
        "subtotal",
        "subtotal_discount",
        "discount",
        "total",
        "c.shipping",
        "b.payment",
        "f.description",
        "FROM_UNIXTIME(timestamp) AS date"
    );

    $from = array(
        "gjshop_orders AS a",
        "gjshop_payment_descriptions AS b",
        "gjshop_shipping_descriptions AS c",
        "gjshop_profile_fields_data AS e",
        "gjshop_profile_field_descriptions AS f"
    );

    $joins = array();

    $conditions = array(
        "a.shipping_ids = c.shipping_id",
        "a.payment_id = b.payment_id",
        "e.field_id = 40",
        "e.object_type = 'O'",
        "a.order_id = e.object_id",
        "e.value = f.object_id",
        "f.object_type = 'V'",
        "a.status NOT IN ('P','O','F','D','B','I','Y','U','1','N')"
    );

    if (isset($params['order_id']) && trim($params['order_id'])) {
        array_push($conditions, "a.order_id = " . $params['order_id']);
    }

    if (isset($params['user_id']) && trim($params['user_id'])) {
        array_push($conditions, "user_id = " . $params['user_id']);
    }

    $group_by = array();

    $order_by = array();

    $limit = array(
        "LIMIT $start_from, $results_per_page"
    );

    $imploded_fields = implode(', ', $fields);
    $imploded_from = ' FROM ' . implode(', ', $from);
    $imploded_joins = implode(' ', $joins);
    if (!empty($conditions)) {
        $imploded_conditions = ' WHERE ' . implode(' AND ', $conditions);
    }
    if (!empty($group_by)) {
        $imploded_group_by = ' GROUP BY ' . implode(', ', $group_by);
    }
    if (!empty($order_by)) {
        $imploded_order_by = ' ORDER BY ' . implode(', ', $order_by);
    }
    $imploded_limit = implode(' ', $limit);

    $all_page_data = db_get_array("SELECT " . $imploded_fields . ' ' . $imploded_from . ' ' . $imploded_joins . ' ' . $imploded_conditions . ' ' . $imploded_group_by . ' ' . $imploded_order_by . '');
    $result = db_get_array("SELECT " . $imploded_fields . ' ' . $imploded_from . ' ' . $imploded_joins . ' ' . $imploded_conditions . ' ' . $imploded_group_by . ' ' . $imploded_order_by . ' ' . $imploded_limit . '');

    if ($params['export'] == 'true') {
        fn_export_to_xls('Detajet e porosive', $all_page_data, $params);
    }

    $total_pages = count($all_page_data);
    $results = array(
        "total_page" => $total_pages,
        "result" => $result
    );

    return $results;
}

function fn_get_warehouse_sales($params)
{
    $results_per_page = 20;

    if (isset($params["page"])) {
        $page = $params["page"];
    } else {
        $page = 1;
    };
    $start_from = ($page - 1) * $results_per_page;

    $fields = array(
        "gjo.order_id",
        "gjp.product_code",
        "gjpd.product",
        "from_unixtime(gjo.timestamp) as date",
        "gjpsh.stock",
        "gjod.price * gjpsh.stock as sum"
    );

    $from = array(
        "gjshop_products AS gjp"
    );

    $joins = array(
        "INNER JOIN gjshop_product_descriptions AS gjpd ON gjp.product_id = gjpd.product_id",
        "INNER JOIN gjshop_product_stock_history AS gjpsh ON gjp.product_id = gjpsh.product_id",
        "INNER JOIN gjshop_orders AS gjo ON gjpsh.order_id = gjo.order_id",
        "INNER JOIN gjshop_order_details AS gjod ON gjo.order_id = gjod.order_id"
    );

    $conditions = array(
        "gjod.product_id = gjpsh.product_id",
        "gjpsh.stock > 0",
        "gjo.company_id = 1"
    );

    if (isset($params['ws_start']) && trim($params['ws_start']) && isset($params['ws_end']) && trim($params['ws_end'])) {
        array_push($conditions, "gjo.timestamp BETWEEN " . $params['ws_start'] . " AND " . $params['ws_end']);
    }

    $group_by = array();

    $order_by = array();

    $limit = array(
        "LIMIT $start_from, $results_per_page"
    );

    $imploded_fields = implode(', ', $fields);
    $imploded_from = ' FROM ' . implode(', ', $from);
    $imploded_joins = implode(' ', $joins);
    if (!empty($conditions)) {
        $imploded_conditions = ' WHERE ' . implode(' AND ', $conditions);
    }
    if (!empty($group_by)) {
        $imploded_group_by = ' GROUP BY ' . implode(', ', $group_by);
    }
    if (!empty($order_by)) {
        $imploded_order_by = ' ORDER BY ' . implode(', ', $order_by);
    }
    $imploded_limit = implode(' ', $limit);

    $all_page_data = db_get_array("SELECT " . $imploded_fields . ' ' . $imploded_from . ' ' . $imploded_joins . ' ' . $imploded_conditions . ' ' . $imploded_group_by . ' ' . $imploded_order_by . '');
    $result = db_get_array("SELECT " . $imploded_fields . ' ' . $imploded_from . ' ' . $imploded_joins . ' ' . $imploded_conditions . ' ' . $imploded_group_by . ' ' . $imploded_order_by . ' ' . $imploded_limit . '');

    if ($params['export'] == 'true') {
        $params['ws_start'] = date('d/m/Y', $params['ws_start']);
        $params['ws_end'] = date('d/m/Y', $params['ws_end']);
        fn_export_to_xls('Shitjet nga depo', $all_page_data, $params, 'ws_start', 'ws_end');
    }

    $total_pages = count($all_page_data);
    $results = array(
        "total_page" => $total_pages,
        "result" => $result
    );

    return $results;
}

function fn_get_monthly_orders($params)
{
    $results_per_page = 20;
    if (isset($params["page"])) {
        $page = $params["page"];
    } else {
        $page = 1;
    };
    $start_from = ($page - 1) * $results_per_page;
    $skip = false;

    if (!$params['from_date'] || !$params['to_date']) {
        $skip = true;
    }

    $query = "SELECT gjod.order_id AS order_id, gjsd.description AS status, gjod.product_id AS product_id, gjp.product AS product_name, gjod.amount AS quantity, gjod.price AS price,
 gjpp.price_without_vat AS tvsh, FROM_UNIXTIME(gjo.timestamp) AS order_date,
 CASE
    WHEN gjshop_product_features_values.feature_id = 14 THEN variant
    ELSE (SELECT variant FROM gjshop_product_feature_variant_descriptions WHERE variant_id IN (SELECT variant_id FROM gjshop_product_features_values WHERE product_id = gjod.product_id AND feature_id = 14) LIMIT 1)
END AS gtin
FROM gjshop_order_details AS gjod
INNER JOIN gjshop_orders AS gjo ON gjo.order_id = gjod.order_id
INNER JOIN gjshop_product_descriptions AS gjp ON gjp.product_id = gjod.product_id
INNER JOIN gjshop_product_prices AS gjpp ON gjpp.product_id = gjod.product_id
INNER JOIN gjshop_statuses AS gjs ON gjs.status = gjo.status
INNER JOIN gjshop_status_descriptions AS gjsd ON gjsd.status_id = gjs.status_id
INNER JOIN gjshop_product_features_values
  ON gjshop_product_features_values.product_id = gjp.product_id
INNER JOIN gjshop_product_feature_variant_descriptions
  ON gjshop_product_features_values.variant_id = gjshop_product_feature_variant_descriptions.variant_id
WHERE gjo.timestamp > " . $params['from_date'] . " AND gjo.timestamp <  " . $params['to_date'] . "
AND gjo.status NOT IN ('N')
AND gjs.type = 'O'
AND gjo.company_id = 1
GROUP BY gjod.order_id, gjod.product_id";

    if ($skip) {
        $all_page_data = [];
        $result = [];
    } else {
        $all_page_data = db_get_array($query);
        $result = db_get_array($query . " LIMIT $start_from, $results_per_page");
    }

    if ($params['export'] == 'true') {
        $params['from_date'] = date('d/m/Y', $params['from_date']);
        $params['to_date'] = date('d/m/Y', $params['to_date']);
        fn_export_to_xls('Shitjet mujore', $all_page_data, $params, 'from_date', 'to_date');
    }

    $total_pages = count($all_page_data);
    $results = array(
        "total_page" => $total_pages,
        "result" => $result
    );

    return $results;
}