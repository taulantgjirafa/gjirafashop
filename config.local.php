<?php

/***************************************************************************
 *                                                                          *
 *   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 *                                                                          *
 ****************************************************************************
 * PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
 * "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
 ****************************************************************************/

if (!defined('BOOTSTRAP')) {
    die('Access denied');
}

/*
 * PHP options
 */

// Log everything, but do not display

ini_set('display_errors', '0');
ini_set('display_startup_errors', false);
ini_set('log_errors', '1');          // We need to log them otherwise this script will be pointless!
ini_set('error_log', DIR_ROOT . '/var/errlog50.txt');   // Full path to a writable file - include the file name
error_reporting(E_ALL ^ E_NOTICE);      // What errors to log - see: http://www.php.net/error_reporting

// Set maximum memory limit
if (PHP_INT_SIZE == 4 && (substr(ini_get('memory_limit'), 0, -1) < "64")) {
    // 32bit PHP
    @ini_set('memory_limit', '128M');
} elseif (PHP_INT_SIZE == 8 && (substr(ini_get('memory_limit'), 0, -1) < "256")) {
    // 64bit PHP
    @ini_set('memory_limit', '512M');
}

@ini_set('memory_limit', '512M');
//@ini_set('memory_limit', '50000M');
if (!defined('CONSOLE')) {
    // Set maximum time limit for script execution.
    @set_time_limit(3600);
}

/*
 * Database connection options
 */

$db_params = array(
    'live' => array(
        'db_host' => '51.195.89.143:3306',
        'db_name' => 'gjirafashop_bf',
        'db_user' => 'root',
        'db_password' => 'mJdaLcYUiAAq'
    ),
    'staging' => array(
        'db_host' => '198.50.153.174:3306',
        'db_name' => 'gjirafashop_bf',
        'db_user' => 'root',
        'db_password' => 'Something123'
    ),
    'local' => array(
        'db_host' => 'localhost:3306',
        'db_name' => 'gjirafashop_bf',
        'db_user' => 'root',
        'db_password' => ''
    )
);



foreach ($db_params[ENVIRONMENT] as $key => $param) {
    $config[$key] = $param;
}

$config['database_backend'] = 'mysqli';

// Database tables prefix
$config['table_prefix'] = 'gjshop_';

/*
 * Script location options
 *
 *  Example:
 *  Your url is http://www.yourcompany.com/store/cart
 *  $config['http_host'] = 'www.yourcompany.com';
 *  $config['http_path'] = '/store/cart';
 *
 *  Your secure url is https://secure.yourcompany.com/secure_dir/cart
 *  $config['https_host'] = 'secure.yourcompany.com';
 *  $config['https_path'] = '/secure_dir/cart';
 *
 */

$http_hosts = array(
    'live' => array(
        'http_host' => 'gjirafa50.com',
        'http_path' => '',
        'https_host' => 'gjirafa50.com',
        'https_path' => ''
    ),
    'staging' => array(
        'http_host' => 'staging.gjirafa50.com',
        'http_path' => '',
        'https_host' => 'staging.gjirafa50.com',
        'https_path' => ''
    ),
    'local' => array(
        'http_host' => 'local.gjirafa50.com',
        'http_path' => '/gjirafa5038',
        'https_host' => 'local.gjirafa50.com',
        'https_path' => '/gjirafa5038',
    )
);

// Host and directory where software is installed on no-secure server
// Host and directory where software is installed on secure server
foreach ($http_hosts[ENVIRONMENT] as $key => $host) {
    $config[$key] = $host;
}

/*
 * Misc options
 */
// Names of index files for the frontend and backend
$config['customer_index'] = 'index.php';
$config['admin_index']    = 'adminGj123.php';


// DEMO mode
$config['demo_mode'] = false;

// Tweaks
$config['tweaks'] = array(
    // Whether to remove any javascript code from description and name of product, category, etc.
    // Auto - false for ULT, true for MVE.
    'sanitize_user_html' => 'auto',
    'anti_csrf' => true, // protect forms from CSRF attacks
    'disable_block_cache' => false, // used to disable block cache
    'disable_localizations' => true, // Disable Localizations functionality
    'disable_dhtml' => false, // Disable Ajax-based pagination and Ajax-based "Add to cart" button
    'dev_js' => false, // set to true to disable js files compilation
    'redirect_to_cart' => false, // Redirect customer to the cart contents page. Used with the "disable_dhtml" setting.
    'api_https_only' => false, // Allows the use the API functionality only by the HTTPS protocol
    'api_allow_customer' => false, // Allow open API for unauthorized customers
    'lazy_thumbnails' => false, // generate image thumbnails on the fly
    'image_resize_lib' => 'auto', // library to resize images - "auto", "gd" or "imagick"
    'products_found_rows_no_cache_limit' => 100, // Max count of SQL found rows without saving to cache
);

// Key for sensitive data encryption
$config['crypt_key'] = 'JI85Mb4TZS';

// Cache backend
// Available backends: file, sqlite, database, redis, xcache, apc, apcu
// To use sqlite cache the "sqlite3" PHP module should be installed
// To use xcache cache the "xcache" PHP module should be installed
// To use apc cache the "apc" PHP module should be m
// To use apcu cache the PHP version should be >= 7.x and the "apcu" PHP module should be installed
$config['cache_backend'] = 'apcu';
$config['cache_redis_server'] = '';
$config['cache_redis_port'] = '';
$config['cache_redis_global_ttl'] = 0; // set this if your cache size reaches Redis server memory size

// Storage backend for sessions. Available backends: database, redis
$config['session_backend'] = 'redis';
//$config['session_redis_server'] = 'rediscache.gjirafa50.com';
$config['session_redis_server'] = '51.195.89.144';
$config['session_redis_server_port'] = '6379';
$config['session_redis_auth_key'] = NULL;
//$config['session_redis_auth_key'] = 'R1yKOLES6797RiIF3Lqc39de9AM7QS+lgKl6c6iJ0kg=';

$config['accounting_api_server'] = 'http://qboapi.gjirafa.com/api';
$config['gjmall_notifier'] = 'https://gjmall.gjirafa.com/Gjirafa/UpdateStock';
$config['dmp_url'] = 'https://gjc.gjirafa.com:998';
$config['elastic_url'] = 'http://50.search.gjirafa.tech';
//$config['cache_apc_global_ttl'] = 0;
//$config['cache_xcache_global_ttl'] = 0;
$config['gateway_params'] = array(
    'token' => '7Kl3YWgxi6nR2bBeaqD1eGaG32vgY0MTxpFcBA4h',
    'from' => 'Gjirafa50',
    'url' => 'https://api.smsapi.com/sms.do'
);

$config['gj50al_cid'] = 19;
$config['rbko_offer'] = false;
$config['rbko_products'] = array();


// Set to unique store prefix if you use the same Redis/Xcache/Apc storage
// for serveral cart installations
$config['store_prefix'] = 'gj50_';

// CDN server backend
$config['cdn_backend'] = 'cloudfront';

// Storage options
$config['storage'] = array(
    'images' => array(
        'prefix' => 'images',
        'dir' => $config['dir']['root'],
        'cdn' => true
    ),
    'downloads' => array(
        'prefix' => 'downloads',
        'secured' => true,
        'dir' => $config['dir']['var']
    ),
    'assets' => array(
        'dir' => &$config['dir']['cache_misc'],
        'prefix' => 'assets',
        'cdn' => true
    ),
    'custom_files' => array(
        'dir' => &$config['dir']['var'],
        'prefix' => 'custom_files'
    )
);

$config['kosgiro'] = array(
    'get_url' => 'https://stg-kosgiro.gjirafa.com/api/PaymentInfo/GetPaymentInfo',
    'update_url' => 'https://stg-kosgiro.gjirafa.com/api/PaymentInfo/UpdatePaymentInfo',
    'image_directory' => 'var/tmp_uploads/invoices/kosgiro/'
);

$config['elastic_services'] = array(
    'http://50.search.gjirafa.tech',
//    'http://50v2.search.gjirafa.tech'
);

$config['sso'] = array(
    'provider_url' => 'https://identity.gjirafa.com',
    'client_id' => 'gjirafa50',
    'client_secret' => '29bxAJnyLPNYm5hA',
    'redirect_url' => 'sso.callback',
    'register_url' => 'https://identity.gjirafa.com/Account/Register',
    'forgot_password_url' => 'https://identity.gjirafa.com/Account/ForgetPassword'
);

define('AWS_KEY', 'GVX94BD2UZQN95PJ8A8R');
define('AWS_SECRET_KEY', '5qBTuWQg3ED5Wi47qw8jNWxdBACmBu4XvuycMm4h');
define('AWS_ENDPOINT', 'http://blob.gjirafa.com');

define('RABBIT_HOST', 'rabbit1.gjirafa.com');
define('RABBIT_PORT', 5672);
define('RABBIT_USERNAME', 'lepuri');
define('RABBIT_PASSWORD', 'gj1rafan3k3mb3telepur1t');
define('RABBIT_LOGS_QUEUE_NAME', 'gjirafa_captain_site_logs_queue');
define('RABBIT_ERRORS_QUEUE_NAME', 'gjirafa_captain_site_errors_queue');
define('RABBIT_SITE_ID', '19');

define('KINGUIN_BASE_URL', 'https://gateway.kinguin.net/esa/api/');
define('KINGUIN_API_KEY', '7844a30cb08d640f4ddf388c7bd5361e');
define('KINGUIN_TEST_BASE_URL', 'https://gateway.sandbox.kinguin.net/esa/api/');
define('KINGUIN_TEST_API_KEY', '18e9510ffaf579e3318a57583cc2d60b');

define('LICENSE_KEY_PRODUCTS_STORE_ID', 123);

define('ORDER_DRIVER_TAGS', array(
    'Latifi',
    'Tahiri',
    'merre vet',
    'Beki',
    'AC',
    'MikMik',
    'SIM',
    'Stafi',
    'KPS',
    'Speed',
    'Tahiri',
    'Intex'
));

define("ESIS_BASE_URI", "https://track.esisatc.com/api/");
define("ESIS_TOKEN", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1cmwiOiJodHRwczovL2dqaXJhZmE1MC5jb20vIiwic2x1ZyI6ImdqaXJhZmFrcyIsImlhdCI6MTYxNDk1ODEwOH0.qBgWe1ttKZmdCVAExbSPRW28324rD1Cc10ZFtgkd1vY");

// Default permissions for newly created files and directories
define('DEFAULT_FILE_PERMISSIONS', 0666);
define('DEFAULT_DIR_PERMISSIONS', 0777);

// Maximum number of files, stored in directory. You may change this parameter straight after a store was installed. And you must not change it when the store has been populated with products already.
define('MAX_FILES_IN_DIR', 1000);

// Developer configuration file
if (file_exists(DIR_ROOT . '/local_conf.php')) {
    include_once(DIR_ROOT . '/local_conf.php');
}

// Enable DEV mode if Product status is not empty (like Beta1, dev, etc.)
if (PRODUCT_STATUS != '' && !defined('DEVELOPMENT')) {
    ini_set('display_errors', 'on');
    ini_set('display_startup_errors', true);

    //define('DEVELOPMENT', true);
}
