{if $tag_type}
    {capture name="add_search_tags"}
        {include file="addons/sd_order_user_tags/views/search_tags/components/search_by_tags.tpl" object=$search input_name="data" allow_save=true type=$tag_type object_id=0}
    {/capture}
    {script src="js/addons/sd_order_user_tags/tags_autocomplete.js"}
    <script type="text/javascript">
    (function(_, $) {
        $(document).ready(function(){
            $('#search_tags').remove();
            {if $show_search_tags_field}
            $('#simple_search').append('{strip}
            <div class="sidebar-field" id="search_tags">
                <label for="elm_name">{__("sd_order_user_tags.tags")}</label>
                <div class="break sd_custom_margin_tags">
                    <ul id="my_search_tags">
                        <input type="hidden" id="object_id" value="0" />
                        <input type="hidden" id="object_type" value={$tag_type} />
                        <input type="hidden" name="search[tags][]" value="" />
                        <input type="hidden" id="object_name" value="search[tags][]" />
                        {foreach from=$search.tags item="tag" name="tags"}<li>{$tag.tag}</li>{/foreach}
                    </ul>
                </div>
            </div>
            {/strip}');
            {/if}
        });
    }(Tygh, Tygh.$));
    </script>
{/if}