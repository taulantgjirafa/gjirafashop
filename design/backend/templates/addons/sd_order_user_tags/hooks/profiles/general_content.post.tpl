{if $user_data.user_id && $show_search_tags_field}
    <div class="control-group {if $allow_save}cm-no-hide-input{/if}">
        <label class="control-label">{__("sd_order_user_tags.tags")}:</label>
        <div class="controls">
            {include file="addons/sd_order_user_tags/views/search_tags/components/object_tags.tpl" object=$user_data input_name="user_data" allow_save=true type="SearchTagTypes::USER"|enum object_id=$user_data.user_id company_id=$user_data.company_id}
        </div>
    </div>
{/if}