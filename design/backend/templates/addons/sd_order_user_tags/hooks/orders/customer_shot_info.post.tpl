{if $show_search_tags_field}
<div class="well orders-right-pane form-horizontal">
<div class="control-group">
    <div class="control-label">
        {include file="common/subheader.tpl" title=__("sd_order_user_tags.tags")}
    </div>
</div>
{include file="addons/sd_order_user_tags/views/search_tags/components/object_tags.tpl" object=$order_info input_name="order_info" allow_save=true type="SearchTagTypes::ORDER"|enum object_id=$order_info.order_id company_id=$order_info.company_id}
</div>
{/if}