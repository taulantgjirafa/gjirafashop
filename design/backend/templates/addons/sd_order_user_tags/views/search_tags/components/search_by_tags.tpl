<div class="sidebar-field" id="search_tags">
    <label for="elm_name">{__("sd_order_user_tags.tags")}</label>
    <div class="break">
        <ul id="my_search_tags">
            <input type="hidden" id="object_id" value={$object_id} />
            <input type="hidden" id="object_type" value={$type} />
            <input type="hidden" name="{$input_name}[tags][]" value="" />
            <input type="hidden" id="object_name" value="{$input_name}[tags][]" />
            {if $company_id}
                <input type="hidden" id="company_id" value={$company_id} />
            {/if}
            {foreach from=$object.tags item="tag" name="tags"}<li>{$tag.tag}</li>{/foreach}
        </ul>
    </div>
</div>