{script src="js/addons/sd_order_user_tags/tags_autocomplete.js"}
<ul id="my_search_tags" {if $update_tag_block}class="sd_short_tag_field"{/if}>
    <input type="hidden" id="object_id" value={$object_id} />
    <input type="hidden" id="object_type" value={$type} />
    <input type="hidden" name="{$input_name}[tags][]" value="" />
    <input type="hidden" id="object_name" value="{$input_name}[tags][]" />
    {if $company_id}
        <input type="hidden" id="company_id" value={$company_id} />
    {/if}
    {foreach from=$object.tags item="tag" name="tags"}<li>{$tag.tag}</li>{/foreach}
</ul>
