<div class="sidebar-row">
    <form action="{""|fn_url}" name="tag_search_form" method="get">
        {capture name="simple_search"}
        <div class="sidebar-field">
            <label>{__("sd_order_user_tags.tag")}</label>
            <input type="text" name="ctag" size="20" value="{$search.ctag}" class="search-input-text" />
        </div>
        <div class="sidebar-field">
            <label for="type">{__("type")}</label>
            <select name="type" id="type">
                <option value="">--</option>
                <option value="{"SearchTagTypes::ORDER"|enum}" {if $search.type == "SearchTagTypes::ORDER"|enum}selected="selected"{/if}>{__("order")}</option>
                <option value="{"SearchTagTypes::USER"|enum}" {if $search.type == "SearchTagTypes::USER"|enum}selected="selected"{/if}>{__("user")}</option>
            </select>
        </div>
        <div class="sidebar-field">
            <label for="status">{__("status")}</label>
            <select name="status" id="status">
                <option value="">--</option>
                <option value="{"SearchTagStatuses::ACTIVE"|enum}" {if $search.status == "SearchTagStatuses::ACTIVE"|enum}selected="selected"{/if}>{__("active")}</option>
                <option value="{"SearchTagStatuses::DISABLE"|enum}" {if $search.status == "SearchTagStatuses::DISABLE"|enum}selected="selected"{/if}>{__("disable")}</option>
            </select>
        </div>
        {/capture}
        {include file="common/advanced_search.tpl" simple_search=$smarty.capture.simple_search dispatch=$dispatch view_type="tags"}
    </form>
</div>
<hr>
