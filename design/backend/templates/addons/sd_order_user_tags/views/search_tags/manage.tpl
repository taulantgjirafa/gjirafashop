{capture name="add_tag"}

<form action="{""|fn_url}" method="post" name="tag_add_var" class="form-horizontal form-edit">
<input type="hidden" name="page" value="{$smarty.request.page}" />

<div class="control-group">
    <label class="control-label cm-required" for="tag">{__("sd_order_user_tags.tag")}:</label>
    <div class="controls">
        <input type="text" name="tag_data[tag]" id="rule_name" value="" class="span9" />
    </div>
</div>
{if "ULTIMATE"|fn_allowed_for}
    {include file="views/companies/components/company_field.tpl"
        name="tag_data[company_id]"
        id="elm_tag_data_`$id`"
        selected=$tag_data.company_id
    }
{/if}
<div class="control-group">
    <label class="control-label" for="type">{__("type")}</label>
    <div class="controls">
        <select name="tag_data[type]" id="type">
            <option value="{"SearchTagTypes::ORDER"|enum}">{__("order")}</option>
            <option value="{"SearchTagTypes::USER"|enum}">{__("user")}</option>
        </select>
    </div>
</div>

<div class="buttons-container">
    {include file="buttons/save_cancel.tpl" but_name="dispatch[search_tags.update]" cancel_action="close"}
</div>
</form>

{/capture}

{capture name="mainbox"}

<form action="{""|fn_url}" method="post" name="tags_form" class="form-horizontal form-edit">

{include file="common/pagination.tpl" save_current_page=true save_current_url=true}

{if $tags}
    <input type="hidden" name="page" value="{$smarty.request.page}" />
    <table class="table table-middle">
        <thead>
        <tr>
            <th width="1%" class="left">
                {include file="common/check_items.tpl" class="cm-no-hide-input"}</th>
            <th>{__("sd_order_user_tags.tag")}</th>
            <th>{__("type")}</th>
            <th width="6%">&nbsp;</th>
            <th width="10%" class="right">{__("status")}</th>
        </tr>
        </thead>
        {foreach from=$tags item=tag key="key"}
            <tr class="cm-row-status-{$tag.status|lower}">
                <td class="left">
                    <input type="checkbox" name="tag_ids[]" value="{$tag.tag_id}" class="cm-item" />
                    <input type="hidden" name="tags_data[{$key}][tag_id]" value="{$tag.tag_id}" class="cm-item" />
                    <input type="hidden" name="tags_data[{$key}][company_id]" value="{$tag.company_id}" class="cm-item" />
                </td>
                <td>
                    <input type="text" name="tags_data[{$key}][tag]" value="{$tag.tag}" class="input-hidden span7" />
                    {include file="views/companies/components/company_name.tpl" object=$tag}
                </td>
                <td>
                    <select name="tags_data[{$key}][type]">
                        <option value="{"SearchTagTypes::ORDER"|enum}" {if $tag.type == "SearchTagTypes::ORDER"|enum}selected="selected"{/if}>{__("order")}</option>
                        <option value="{"SearchTagTypes::USER"|enum}" {if $tag.type == "SearchTagTypes::USER"|enum}selected="selected"{/if}>{__("user")}</option>
                    </select>
                </td>
                <td>
                    {capture name="tools_list"}
                        <li>{btn type="list" class="cm-confirm cm-post" text=__("delete") href="search_tags.delete?tag_id=`$tag.tag_id`"}</li>
                    {/capture}
                    <div class="hidden-tools">
                        {dropdown content=$smarty.capture.tools_list}
                    </div>
                </td>
                <td class="right">
                    {include file="common/select_popup.tpl" id=$tag.tag_id status=$tag.status hidden=false object_id_name="tag_id" table="sd_search_tags" popup_additional_class="dropleft"}
                </td>
            </tr>
        {/foreach}
    </table>
{else}
    <p class="no-items">{__("no_data")}</p>
{/if}
{include file="common/pagination.tpl"}
</form>
{/capture}

{capture name="buttons"}
    {capture name="tools_list"}
        {if $tags}
            <li>{btn type="list" text=__("sd_order_user_tags.activate_selected") dispatch="dispatch[search_tags.approve]" form="tags_form"}</li>
            <li>{btn type="list" text=__("sd_order_user_tags.disable_selected") dispatch="dispatch[search_tags.disapprove]" form="tags_form"}</li>
            <li class="divider"></li>
            <li>{btn type="delete_selected" dispatch="dispatch[search_tags.m_delete]" form="tags_form"}</li>
        {/if}
    {/capture}
    {dropdown content=$smarty.capture.tools_list}
    {if $tags}
        {include file="buttons/save.tpl" but_name="dispatch[search_tags.m_update]" but_role="submit-link" but_target_form="tags_form"}
    {/if}
{/capture}
{capture name="adv_buttons"}
    {include file="common/popupbox.tpl" id="add_tage" text=__("sd_order_user_tags.new_tag") title=__("sd_order_user_tags.add_tag") content=$smarty.capture.add_tag act="general" icon="icon-plus"}
{/capture}

{capture name="sidebar"}
    {include file="addons/sd_order_user_tags/views/search_tags/components/search_form.tpl" dispatch="search_tags.manage"}
{/capture}
{include file="common/mainbox.tpl" title=__("sd_order_user_tags.tags") sidebar=$smarty.capture.sidebar content=$smarty.capture.mainbox buttons=$smarty.capture.buttons adv_buttons=$smarty.capture.adv_buttons}
