<div class="control-group">
    <label class="control-label" for="is_bundle">Bundle:</label>
    <div class="controls">
        <input type="checkbox" id="is_bundle" name="product_data[is_bundle]" {if $is_bundle}checked="checked"{/if}">
    </div>
</div>

<div id="bundle_options" style="{if !$is_bundle}display: none;{/if}">
    <div class="control-group">
        <label class="control-label" for="bundle_product_codes">Bundle products:</label>
        {if $bundle_products}
            {foreach from=$bundle_products key='key' item='bundle_product'}
                <div class="controls bundle-item-input" style="padding-bottom: 5px;">
                    <input type="text" id="bundle_product_codes" name="product_data[bundle_product_data][{$key}][product_code]" value="{$bundle_product.product_code}" placeholder="Product code">
                    <input type="text" id="bundle_product_codes" name="product_data[bundle_product_data][{$key}][amount]" value="{$bundle_product.amount}" placeholder="Amount">
                </div>
            {/foreach}
        {else}
            <div class="controls bundle-item-input" style="padding-bottom: 5px;">
                <input type="text" id="bundle_product_codes" name="product_data[bundle_product_data][1][product_code]" value="" placeholder="Product code">
                <input type="text" id="bundle_product_codes" name="product_data[bundle_product_data][1][amount]" value="" placeholder="Amount">
            </div>
        {/if}
        <div class="controls">
            <input type="submit" value="Add item" class="btn btn-primary" id="bundle_add_item" style="margin-top: 10px;">
        </div>
    </div>
</div>

<script>
    document.getElementById('is_bundle').onclick = function() {
        if (document.getElementById('bundle_options').style.display == 'none') {
            document.getElementById('bundle_options').style.display = 'block';
        } else {
            document.getElementById('bundle_options').style.display = 'none';
        }
    };

    document.getElementById('bundle_add_item').onclick = function(e) {
        e.preventDefault();

        let last_item = $('.bundle-item-input').last().clone();
        let count = parseInt(last_item.find('input').attr('name').match(/\d+/)[0]);

        last_item.find('input:first').attr({ name: 'product_data[bundle_product_data][' + (count + 1) + '][product_code]'});
        last_item.find('input:last').attr({ name: 'product_data[bundle_product_data][' + (count + 1) + '][amount]'});
        last_item.find("input[type='text']").attr({ value: ''}).val('');

        last_item.insertAfter('div.bundle-item-input:last');
    };
</script>