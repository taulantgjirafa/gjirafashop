{capture name="mainbox"}
    {assign var="is_al" value=$al|fn_isAL}
    {capture name="tabsbox"}
        <form id='form' action="{""|fn_url}" method="post" name="promotions_update_form" class="form-horizontal form-edit cm-disable-empty-files" enctype="multipart/form-data">
            <input type="hidden" name="promotion_data[company]" value="{if $al|fn_isAL}19{else}1{/if}">
            <div class="control-group">
                <label for="elm_promotion_name" class="control-label cm-required">Nga shuma fikse:</label>
                <div class="controls">
                    <input type="text" name="discount_value" id="elm_promotion_name" size="25" value="" />
                </div>
            </div>

            {** Form submit section **}
            {capture name="buttons"}
                {include file="buttons/save.tpl" but_role="submit-link" but_name="dispatch[promotions.update.preset]" but_target_form="promotions_update_form" save=$id}
            {/capture}
            {** /Form submit section **}

        </form> {* /product update form *}

    {/capture}
    {include file="common/tabsbox.tpl" content=$smarty.capture.tabsbox group_name=$runtime.controller active_tab=$smarty.request.selected_section track=true}

{/capture}

{include file="common/mainbox.tpl"
title=__("new_promotion")
content=$smarty.capture.mainbox
select_languages=$id
buttons=$smarty.capture.buttons
adv_buttons=$smarty.capture.adv_buttons
}