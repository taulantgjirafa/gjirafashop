<h4 class="subheader hand collapsed" data-toggle="collapse" data-target="#sec_{$item}">
    {__("builder_`$item_data.title`")}
    <span class="exicon-collapse"></span>
</h4>
<div id="sec_{$item}" class="collapse">
    <input type="hidden" name="sections[{$item}][products]" value="{$item_data.item_products}">

    <div class="control-group">
        <label for="product_description_product" class="control-label cm-required">{__('categories')}</label>
        <div class="controls">
            {include file="pickers/categories/picker.tpl" hide_input=$product_data.shared_product company_ids=$product_data.company_id rnd=$rnd data_id="" input_name="sections[{$item}][categories]" radio_input_name="" main_category=$product_data.main_category item_ids=$item_data.item_categories|default:$request_category_id hide_link=true hide_delete_button=true display_input_id="" disable_no_item_text=true view_mode="list" but_meta="btn" show_active_path=true}
        </div>
    </div>

{*    <div>*}
{*        <label for="product_description_product" class="control-label cm-required">{__('products')}</label>*}
{*        <div class="controls">*}
{*            {include file="common/fileuploader.tpl" var_name="sections[{$item}][csv]"}*}
{*        </div>*}
{*    </div>*}

{*    <div class="control-group">*}
{*        <label for="product_description_product" class="control-label cm-required">{__('products')}</label>*}
{*        <div class="controls">*}
{*            <input type="text" name="sections[{$item}][product_codes]" value="{$item_data.item_product_codes}">*}
{*        </div>*}
{*    </div>*}

{*    <div class="control-group">*}
{*        <label for="product_description_product" class="control-label cm-required">{__('products')}</label>*}
{*        <div class="controls">*}
{*            {include file="pickers/products/picker.tpl" input_name="sections[{$item}][products]" type="links" item_ids=$item_data.item_products}*}
{*        </div>*}
{*    </div>*}
</div>