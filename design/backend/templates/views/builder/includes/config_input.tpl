<div class="control-group clone">
    <label for="builder_name_{$item}" class="inline" style="padding-right: 15px;">{__('name')}:
        <input type="text" name="builder[{$item}][title]" id="builder_name_{$item}" value="{$item_data.item}">
    </label>
    <label for="builder_required_{$item}" class="checkbox inline">{__('required')}
        <input type="checkbox" name="builder[{$item}][required]" value="" id="builder_required_{$item}" {if $item_data.required}checked{/if}>
    </label>
    <label for="builder_multiple_{$item}" class="checkbox inline">{__('multiple')}
        <input type="checkbox" name="builder[{$item}][multiple]" value="" id="builder_multiple_{$item}" {if $item_data.multiple}checked{/if}>
    </label>
    <label for="builder_icon_{$item}" class="checkbox inline">{__('icon')}
        <input type="file" name="{$item}" value="" id="builder_icon_{$item}" accept="image/png">
        <img src="https://hhstsyoejx.gjirafa.net/gj50/builder/icons/builder_{$item}.png" style="height: 50px;" alt="">
    </label>
</div>