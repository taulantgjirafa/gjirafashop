{capture name="mainbox"}
    {assign var="is_al" value=$al|fn_isAL}
    {capture name="tabsbox"}
        <form id='form' action="{""|fn_url}" method="post" name="sections_update_form" class="form-horizontal form-edit  cm-disable-empty-files" enctype="multipart/form-data">
            <div class="form-inline">
                {include file="views/builder/includes/config_input.tpl" item=1 item_data=$item_1_data}
                {include file="views/builder/includes/config_input.tpl" item=2 item_data=$item_2_data}
                {include file="views/builder/includes/config_input.tpl" item=3 item_data=$item_3_data}
                {include file="views/builder/includes/config_input.tpl" item=4 item_data=$item_4_data}
                {include file="views/builder/includes/config_input.tpl" item=5 item_data=$item_5_data}
                {include file="views/builder/includes/config_input.tpl" item=6 item_data=$item_6_data}
                {include file="views/builder/includes/config_input.tpl" item=7 item_data=$item_7_data}
                {include file="views/builder/includes/config_input.tpl" item=8 item_data=$item_8_data}
                {include file="views/builder/includes/config_input.tpl" item=9 item_data=$item_9_data}
                {include file="views/builder/includes/config_input.tpl" item=10 item_data=$item_10_data}
            </div>

            {** Form submit section **}
            {capture name="buttons"}
                {include file="buttons/save.tpl" but_role="submit-link" but_name="dispatch[builder.config]" but_target_form="sections_update_form" save=$id}

            {/capture}
            {** /Form submit section **}

        </form> {* /product update form *}

    {/capture}
    {include file="common/tabsbox.tpl" content=$smarty.capture.tabsbox group_name=$runtime.controller active_tab=$smarty.request.selected_section track=true}

{/capture}

{include file="common/mainbox.tpl"
title="Builder configuration"
content=$smarty.capture.mainbox
select_languages=$id
buttons=$smarty.capture.buttons
adv_buttons=$smarty.capture.adv_buttons
}