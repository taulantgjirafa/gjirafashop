{capture name="offer"}
    <div class="row-fluid">
        <div class="span16" id="fp-main">
            <img src="images/offer/bg.jpg" id="fp">
            <div class="fp-content">
                <img src="images/logos/1/gjirafa50.png" width="200" alt="">
                <h1 class="text-primary" style="font-weight: 400;margin-top:40px;"><strong>OFERTË</strong> NGA
                    GJIRAFA<strong>50</strong></h1>
                <h1 class="text-muted" style="font-style: italic;">PËR FURNIZIM ME PAISJE TË IT-SË <BR/> PËR NEVOJA TË
                    <strong style="font-weight:900;"><span contenteditable="true">--------</span></strong></h1>
            </div>
        </div>
    </div>
    <div class="line" style="page-break-after: always;"></div>
    <div>&nbsp;
    </div>
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span16 section about">
                <img src="images/logos/1/gjirafa50.png" width="200" alt="">
                <hr style="width:300px">
                <p contenteditable="true">Gjirafa50 është platformë e shitjes online, e krijuar nga Gjirafa, Inc.<br/><br/>
                    Është dyqan online i specializuar në shitjen e paijsjeve më të fundit të teknologjisë dhe që njëkohësisht janë më të kërkuarat në treg. Në ﬁllimet e saj Gjirafa50, kishte vetëm nga 50 produkte të cilat ishin më të rejat dhe njëkohësisht më të kërkuarat të fushës së teknologjisë.<br/><br/>
                    Tani, Gjirafa50.com është dyqani më i madh online në rajon, që ofron mbi 40,000 produkte të tilla. Te gjitha produktet kane garancion 1 vit nga prodhuesi dhe afati i liferimit eshte  5-9 dite</p>
                <hr style="width:100%">
                <div class="table-info">
                    <div class="list">
                        <div class="list-item">
                            <h4 class="text-light"><span contenteditable="true">Name of CA:</span></h4>
                            <h4><span contenteditable="true">GJIRAFA INC - DEGA NE KOSOVE</span></h4>
                        </div>
                        <div class="list-item">
                            <h4 class="text-light"><span contenteditable="true">Address:</span></h4>
                            <h4><span contenteditable="true">Magjistralja Prishtinë-Ferizaj, Kilometri i 6 (Lapnasellë)</span></h4>
                        </div>
                        <div class="list-item">
                            <h4 class="text-light"><span contenteditable="true">Town:</span></h4>
                            <h4><span contenteditable="true">Prishtinë</span></h4>
                        </div>
                        <div class="list-item">
                            <h4 class="text-light"><span contenteditable="true">Telephone:</span></h4>
                            <h4><span contenteditable="true">+37745101953</span></h4>
                        </div>
                        <div class="list-item">
                            <h4 class="text-light"><span contenteditable="true">E-mail:</span></h4>
                            <h4><span contenteditable="true">contact@gjirafa50.com</span></h4>
                        </div>
                        <div class="list-item">
                            <h4 class="text-light"><span
                                        contenteditable="true">Numri i regjistrimit të kompanisë:</span></h4>
                            <h4><span contenteditable="true">330240630</span></h4>
                        </div>
                        <div class="list-item">
                            <h4 class="text-light"><span contenteditable="true">Numri i TVSH-së:</span></h4>
                            <h4><span contenteditable="true">71075281</span></h4>
                        </div>
                    </div>
                    <div class="list">
                        <div class="list-item">
                            <h4 class="text-light"><span contenteditable="true">Contact Person:</span></h4>
                            <h4><span contenteditable="true">Blerton Krasniqi</span></h4>
                        </div>
                        <div class="list-item">
                            <h4 class="text-light"><span contenteditable="true">E-mail:</span></h4>
                            <h4><span contenteditable="true">Blerton@gjirafa.com</span></h4>
                        </div>
                        <div class="list-item">
                            <h4 class="text-light"><span contenteditable="true">Postal Code:</span></h4>
                            <h4><span contenteditable="true">10000</span></h4>
                        </div>
                        <div class="list-item">
                            <h4 class="text-light"><span contenteditable="true">Region:</span></h4>
                            <h4><span contenteditable="true">Prishtina</span></h4>
                        </div>
                        <div class="list-item">
                            <h4 class="text-light"><span contenteditable="true">URL::</span></h4>
                            <h4><span contenteditable="true">www.gjirafa50.com</span></h4>
                        </div>
                        <div class="list-item">
                            <h4 class="text-light"><span contenteditable="true">NRF:</span></h4>
                            <h4><span contenteditable="true">601144249</span></h4>
                        </div>
                    </div>
                </div>
                <div class="bottomline"></div>
            </div>
            {if !is_null($products)}
                <div class="line" style="page-break-after: always;"></div>
                <div>&nbsp;
                </div>
                <div class="span16 section table-list-wrapper">
                    <table>
                        <thead>
                        <th style="text-align: left;">Produkti</th>
                        <th>Kodi i produktit</th>
                        <th>Sasia</th>
                        <th>Çmimi</th>
                        <th style="text-align: right;"><strong>TOTALI</strong></th>
                        </thead>
                        <tbody id="o_table">
                        {$total = 0}
                        {foreach $products as $product}
                            <tr class="p-row">
                                <td style="text-align: left;"><span contenteditable="true">{$product.product}</span>
                                </td>
                                <td>{$product.product_code}</td>
                                <td><span contenteditable="true" class="o_amount">1</span></td>
                                <td>€<span class="o_price" contenteditable="true">{$product.price}</span></td>
                                <td style="text-align: right;">€<span class="p_total">{$product.price}</span></td>
                            </tr>
                            {$total = $total + $product.price}
                        {/foreach}
                        <tr style="border:0;">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td style="text-align: right;"><span style="font-weight:300;color:#777;">Zbritje: </span>
                                €<span class="o_discount" contenteditable="true">0</span>
                            </td>
                        </tr>
                        <tr style="border:0;">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td style="text-align: right;font-size: 21px;">Total: €<span class="o_total">{$total}</span></td>
                        </tr>
                        </tbody>
                    </table>
                    <script>
                        $('.o_price, .o_amount, .o_discount').on("input", function () {
                            var rows = $('#o_table .p-row');
                            var total = $('.o_total');
                            var discount = parseFloat($('.o_discount').text().trim());
                            var total_num = 0;
                            for(var i = 0; i < rows.length; i++){
                                var product_total = parseFloat($('.o_price', rows[i]).text().trim()) * parseInt($('.o_amount', rows[i]).text().trim());
                                total_num = total_num + (product_total);
                                $('.p_total', rows[i]).text(product_total.toFixed(2));
                            }
                            total.text((total_num - discount).toFixed(2));
                        });
                    </script>
                    <div class="bottomline"></div>
                </div>
                <div class="line" style="page-break-after: always;"></div>
                <div>&nbsp;
                </div>
                {foreach $products as $product}
                    <div class="span16 section product">
                        <div class="half">
                            <h1><span contenteditable="true">{$product.product}</span></h1>
                            <img src="{get_images_from_blob product_id=$product.product_id count=1}" alt="">
                            <div>
                                {for $i = 1; $i < $product.img_count && $i < 4; $i++}
                                    <img style="max-width: 120px;margin-right:10px;max-height:90px;" src="{get_images_url product_id=$product.product_id}{$i}.jpg" alt="">
                                {/for}
                            </div>
                        </div>
                        <div class="half product-details">
                            <h2>DETAJET TEKNIKE</h2>
                            <table>
                                <tbody>
                                {foreach $product.product_features[1].subfeatures as $feature}
                                    {if $feature.variant_id != null}
                                        <tr>
                                            <td><span contenteditable="true">{$feature.description}:</span></td>
                                            <td>
                                                {foreach from=$feature.variants item="var"}
                                                    <span contenteditable="true">{$var.variant}</span>
                                                {/foreach}

                                                <button class="remove btn btn-danger"
                                                        onclick="$(this).parent().parent().remove()">X
                                                </button>
                                            </td>
                                        </tr>
                                    {/if}
                                {/foreach}
                                </tbody>
                            </table>
                        </div>
                        <div class="bottomline"></div>
                    </div>
                    <div class="line" style="page-break-after: always;"></div>
                    <div>&nbsp;
                    </div>
                {/foreach}
                {else}
                <h1 style="text-align: center;color:red;">Nuk u gjet asnje produkt me kete product code</h1>
            {/if}
        </div>
    </div>
    {*{foreach from=$product.product_features[1] item="feature"}*}
    {*{if $feature.feature_type != "ProductFeatures::GROUP"|enum}*}
    {*<div class="ty-product-feature">*}
    {*<span class="ty-product-feature__label">{$feature.description nofilter}{if $feature.full_description|trim}{include file="common/help.tpl" text=$feature.description content=$feature.full_description id=$feature.feature_id show_brackets=false link_text="<span class=\"ty-tooltip-block\"><i class=\"ty-icon-help-circle\"></i></span>" wysiwyg=true}{/if}:</span>*}

    {**}
    {*</div>*}
    {*{/if}*}
    {*{/foreach}*}
{/capture}

{capture name="mainbox"}

    {$smarty.capture.offer nofilter}
{/capture}

<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,900" rel="stylesheet">
{include file="common/mainbox.tpl" title=$page_title content=$smarty.capture.mainbox content_id="manage_orders"}

{literal}
    <style>
        span[contenteditable="true"] {
            border: 1px solid #aaa;
            min-width: 5px;
        }

        @media all {
            .page-break {
                display: none;
            }

            body {
                font-family: 'Roboto', sans-serif !important;
                color: #000;
            }
        }

        .half {
            width: 50%;
            float: left;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            padding: 0 20px;
        }

        .product img {
            max-height: 400px;
        }

        .product .half:nth-child(1) {
            text-align: center;
        }

        .product .half:nth-child(1) img {
            margin-top: 50px;
        }

        .section {
            position: relative !important;
            float: none !important;
            margin: 0 !important;
        }

        .text-light {
            font-weight: lighter;
        }

        hr {
            display: block;
            height: 1px;
            background: transparent;
            width: 100%;
            border: none;
            border-top: solid 2px #e65228;
        }

        h1, h2, h3 {
            line-height: 1.2;
        }

        .row-fluid {
            position: relative;
        }

        .table-info {
            width: 90%;
            margin: 0 auto;
            display: table;
        }

        .table-info .list {
            width: 45%;
            float: left;
            margin-right: 5%;
        }

        .list-item {
            border-bottom: 1px solid #333;
        }

        .list-item h4 {
            margin: 8px 0;
        }

        .fp-content {
            position: absolute;
            bottom: 50px;
            right: 0;
            width: 40%;
        }

        .fp-content .text-primary, .fp-content .text-primary strong {
            color: #e65228 !important;
        }

        .fp-content .text-muted, .fp-content .text-muted strong {
            font-weight: lighter;
            color: #777 !important;
            font-size: 20px;
        }

        .section {
            padding: 100px 100px 0 100px;
            min-hheight: 600px;
            float: none !important;
        }

        .table-list-wrapper {
            height: 700px;
            position: relative;
        }

        .table-list-wrapper table {
            position: absolute;
            left: 0;
            top: 0;
            bottom: 0;
            right: 0;
            margin: auto auto;
            width: 980px;
            text-align: left;
            table-layout: fixed;
        }

        .table-list-wrapper table th:nth-child(1) {
            width: 300px;
        }

        .table-list-wrapper table th {
            color: #e65228 !important;
            font-weight: lighter;
        }

        .table-list-wrapper table tbody td {
            font-weight: bolder;
            text-align: center;
        }

        .table-list-wrapper table tr {
            height: 40px;
            text-align: center;
            border-bottom: 1px solid #222;
            vertical-align: bottom;
        }

        .section.product {
            padding-left: 50px;
            padding-right: 50px;
        }

        .product-details h2 {
            background: #e65228 !important;
            color: #fff !important;
            width: 190px;
            margin: 0 auto;
            display: block;
            font-size: 20px;
            padding: 7px 10px;
            font-weight: lighter;
            text-align: center;
            margin-top: 70px;
        }

        .product-details table {
            width: 100%;
        }

        .product-details table tr {
            border-bottom: 1px solid #333;
            background-color: #eee !important;
        }

        .product-details table th, .product-details table td {
            padding: 10px 10px 10px 40px;
            color: #e65228 !important;
        }

        .product-details table tr td:nth-child(2) {
            text-align: center;
            font-weight: bolder;
        }

        @page {
            margin: 0;
        }

        @media print {

            .remove, #DebugToolbar {
                display: none !important;
            }

            .bottomline {
                position: fixed;
                bottom: 0;
                height: 10px;
                left: 10%;
                background-color: #e65228 !important;
                width: 80%;
            }

            #fp-main .bottomline {
                display: none !important;
            }

            body {
                margin: 0;
                padding: 0;
            }

            html, body {
                height: auto;
            }

            body {
                -webkit-print-color-adjust: exact;
                font-size: 14px;
            }

            .admin-content {
                border: 0;
                -webkit-box-shadow: none;
                -moz-box-shadow: none;
                box-shadow: none;
            }

            .admin-content-wrap .content {
                padding: 0;
            }

            .content-wrap {
                padding-top: 0 !important;
            }

            .section.about {
                padding-top: 50px !important;
            }

            #fp {
                width: auto;
                height: 876px;
                page-break-after: always
            }

            span[contenteditable="true"] {
                border: 0;
            }

            html, body {
                height: 100%;
                min-height: 100%;
            }

            .page-break {
                display: block;
                float: left;
                width: 100%;
                page-break-before: always;
            }

            #header, #actions_panel {
                display: none;
                height: 0;
                overflow: hidden;
            }
        }
    </style>
{/literal}