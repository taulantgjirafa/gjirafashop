{capture name="mainbox"}
    <div style="width:500px;margin:0 auto;padding:5% 0;height:400px;text-align:center;">
        <form method="GET" action="{""|fn_url}">
            <h4 style="font-weight: lighter;">Shkruaj product code per oferte:</h4>
            <input type="text" name="product_code" value="" style="width:500px;" placeholder="Shembull: 123456,987456,321321,456456">
            <button type="submit" name="dispatch[offers.preview]" class="btn btn-primary">KRIJO OFERTEN</button>
        </form>
    </div>
{/capture}

{include file="common/mainbox.tpl" title=$page_title content=$smarty.capture.mainbox content_id="manage_orders"}