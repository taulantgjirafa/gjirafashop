<div id="content_features" class="hidden">

{if $product_features}


{include file="common/pagination.tpl" search=$features_search div_id="product_features_pagination_`$product_id`" current_url="products.get_features?product_id=`$product_id`"|fn_url disable_history=true}

<fieldset>

    <input style="width:345px;" id="search_feature_input" class="col-xs-8" type="text" placeholder="Kërko cilësi.." name="description" value="{$smarty.request.description}"/>
    <a id="search_feature" data-ca-scroll=".cm-pagination-container" class="cm-ajax btn btn-primary" data-href="https://{$smarty.server.SERVER_NAME}/adminGj123.php?dispatch=products.get_features&product_id={$product_id}&page=0" data-ca-page="0" data-ca-target-id="product_features_pagination_{$product_id}">KERKO</a>


    {include file="views/products/components/product_assign_features.tpl" product_features=$product_features}
</fieldset>

{include file="common/pagination.tpl" search=$features_search div_id="product_features_pagination_`$product_id`" current_url="products.get_features?product_id=`$product_id`"|fn_url disable_history=true}

{else}
<p class="no-items">{__("no_items")}</p>
{/if}
</div>

{literal}
    <script>
        $(document).on('keypress keyup', '#search_feature_input', function (e) {
            if (e.keyCode == 13)
                $('#search_feature').click();
            else
                $('#search_feature').attr('href', $('#search_feature').data('href') + '&description=' + $(this).val());
        });
    </script>
{/literal}