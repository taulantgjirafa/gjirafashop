{literal}
    <style>
        [for="period_selects"] { display: none;}
        #period_selects {
            display: none;
        }
    </style>
{/literal}
{capture "mainbox"}
    {capture name="sidebar"}
        <div class="sidebar-row" id="categories_stats">
            <h6>Search</h6>
            <div class="client-sales-search">
                <form action="{""|fn_url}" name="client-sales-search_form" id="example" class="display" cellspacing="0" width="100%" method="GET">
                    <input type="hidden" name="dispatch" value="custom_reports.product_sales">
                    <div class="form-control">
                        <label>Statuset</label>
                        <select class="js-example-basic-multiple" name="statuses[]" multiple="multiple">
                            {assign var="selected_statuses" value=$search_params['statuses']}
                            {foreach $statuses as $status}
                                <option {if $status.status|in_array:$selected_statuses} selected='selected' {/if}value="{$status.status}">{$status.name}</option>
                            {/foreach}
                        </select>
                    </div>
                    <div class="form-control" style="margin-top:10px; margin-bottom: 20px ">
                        <input type="checkbox"  {if $search_params.def_statuses == 'true'}checked{/if} name="def_statuses" value="true" id="def_statuses"> <span class="hint"> Kliko për statuset e parazgjedhura</span>
                    </div>
                    <div class="form-control">
                        {include file="common/period_selector.tpl" period=$period display="form" search=$search_params}
                    </div>
                    <div class="form-control" style="float: right; margin-top: 20px;">
                        {include file="buttons/button.tpl" but_text=__("search") but_onclick=$but_onclick but_href=$but_href but_role=$but_role but_name=$but_name}
                    </div>
                </form>
            </div>
        </div>
    {/capture}
    <table class="table table-middle">
        <thead>
        <tr class="cm-first-sibling">
            <th>Produkt id</th>
            <th>Produkt Kodi</th>
            <th>Produkti</th>
            <th>Sasia</th>
            <th>Shuma Totale</th>
        </tr>
        </thead>
        <tbody>
        {foreach $reports as $report}
            <tr>
                <td>
                    {$report.product_id}
                </td>
                <td>
                    <a href="{"products.update&product_id={$report.product_id}"|fn_url}"> {$report.product_code} </a>
                </td>
                <td>
                    <a href="{"products.update&product_id={$report.product_id}"|fn_url}">  {$report.product} </a>
                </td>
                <td>
                    {$report.sasia}
                </td>
                <td>
                    {$report.total}
                </td>
            </tr>
        {/foreach}
        </tbody>
    </table>
    {if $pages.total !=1}
        <div class="clearfix">
            <div id="pagination-custom"></div>
        </div>
    {/if}
{/capture}
{capture name="buttons"}
    {capture name="tools_list"}

        {assign var="statuses" value="{""|implode:$smarty.get.statuses}"}
        {assign var="time_from" value="&time_from={$smarty.get.time_from}"}
        {assign var="time_to" value="&time_to={$smarty.get.time_to}"}
        <li>{btn type="list" text="Shkarko në Excel" href="custom_reports.product_sales{$time_from}{$time_to}{if $smarty.get.statuses}&statuses[]={$statuses}{elseif $smarty.get.def_statuses}&def_statuses=true{/if}&export=true"}</li>
    {/capture}
    {dropdown content=$smarty.capture.tools_list}
{/capture}
{include file="common/mainbox.tpl" title="Raporte" content=$smarty.capture.mainbox buttons=$smarty.capture.buttons sidebar=$smarty.capture.sidebar}

{literal}
    <script>

        $(document).ready(function() {
            $('.js-example-basic-multiple').select2();
        });

    </script>

{/literal}
<script>

    {ldelim}
        let pages = {$pages.total};
        let active = {$pages.current};
        document.getElementById('pagination-custom').innerHTML = createPagination(pages, active);
        function createPagination(pages, page) {
            {assign var="page_var" value="&page="}
            let url_current = window.location.href +'&page=';
            let str = '<ul>';
            let active;
            let pageCutLow = page - 1;
            let pageCutHigh = page + 1;

            if (page > 1) {
                str += '<li class="page-item previous no"><a href="'+url_current+''+(page-1)+' " onclick="createPagination(pages, '+(page-1)+')">Paraprak</a></li>';
            }
            if (pages < 6) {
                for (let p = 1; p <= pages; p++) {
                    active = page == p ? "active" : "no";
                    str += '<li class="'+active+'"><a href="'+url_current+''+p+'" onclick="createPagination(pages, '+p+')">'+ p +'</a></li>';
                }
            }
            else {
                if (page > 2) {
                    str += '<li class="no page-item"><a href="'+url_current+''+1+'" onclick="createPagination(pages, 1)">1</a></li>';
                    if (page > 3) {
                        str += '<li class="out-of-range"><a onclick="createPagination(pages,'+(page-2)+')">...</a></li>';
                    }
                }
                if (page === 1) {
                    pageCutHigh += 2;
                } else if (page === 2) {
                    pageCutHigh += 1;
                }
                if (page === pages) {
                    pageCutLow -= 2;
                } else if (page === pages-1) {
                    pageCutLow -= 1;
                }
                for (let p = pageCutLow; p <= pageCutHigh; p++) {
                    if (p === 0) {
                        p += 1;
                    }
                    if (p > pages) {
                        continue
                    }
                    active = page == p ? "active" : "no";
                    str += '<li class="page-item '+active+'"><a href="'+url_current+''+p+'" onclick="createPagination(pages, '+p+')">'+ p +'</a></li>';
                }
                if (page < pages-1) {
                    if (page < pages-2) {
                        str += '<li class="out-of-range"><a onclick="createPagination(pages,'+(page+2)+')">...</a></li>';
                    }
                    str += '<li class="page-item no"><a href="'+url_current+''+pages+'" onclick="createPagination(pages, pages)">'+pages+'</a></li>';
                }
            }
            if (page < pages) {
                str += '<li class="page-item next no"><a  href="'+url_current+''+(page+1)+'" onclick="createPagination(pages, '+(page+1)+')">Vijues</a></li>';
            }
            str += '</ul>';
            document.getElementById('pagination-custom').innerHTML = str;
            return str;
        }
        {rdelim}
</script>
