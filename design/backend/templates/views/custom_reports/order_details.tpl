{literal}

    <style>
        #summary {
            background: #666;
            width: 100px;
            color: #fff;
        }

        #summary:hover {
            cursor: pointer;
            color: #fff200;
        }

        #detail {
            width: 300px;
            height: 300px;
            background: #fff200;
            display: none;
        }
        /*[for="period_selects"] { display: none;}*/
        /*#period_selects {*/
            /*display: none;*/
        /*}*/
    </style>

    <script>

        $(document).ready( function(e) {
            $('#summary').hover( function( evt ) {
                $('#detail').toggle();
            });
        });

    </script>

{/literal}
{capture name="mainbox"}
    {capture name="sidebar"}
        <div class="sidebar-row" id="categories_stats">
            <h6>Search</h6>
            <div class="client-sales-search">
                <form action="{""|fn_url}" name="client-sales-search_form" id="example" class="display" cellspacing="0" width="100%" method="GET">
                    <input type="hidden" name="dispatch" value="custom_reports.order_details">
                    <div class="form-control">
                        <label>ID e porosise</label>
                        <input type="text" name="order_id" value="{$search_params['order_id']}">
                    </div>
                    <div class="form-control">
                        <label>ID e perdoruesit</label>
                        <input type="text" name="user_id" value="{$search_params['user_id']}">
                    </div>
                    <div class="form-control" style="float: right; margin-top: 20px;">
                        {include file="buttons/button.tpl" but_text=__("search") but_onclick=$but_onclick but_href=$but_href but_role=$but_role but_name=$but_name}
                    </div>
                    <div class="form-control" style="float: left; margin-top: 20px;">
                        {include file="buttons/button.tpl" but_text=__("reset") but_onclick='resetSearch()' but_href=$but_href but_role=$but_role but_name=$but_name}
                    </div>
                </form>
            </div>
        </div>
    {/capture}
    <table class="table table-middle">
        <thead>
        <tr class="cm-first-sibling">
            <th>ID e porosise</th>
            <th>ID e perdoruesit</th>
            <th>Subtotali</th>
            <th>Subtotali zbritje</th>
            <th>Zbritje</th>
            <th>Totali</th>
            <th>Metoda e shipping</th>
            <th>Metoda e pageses</th>
            <th>Lokacioni</th>
            <th>Data</th>
        </tr>
        </thead>
        <tbody>
        {if !$order_details}
        <tr>
            <td colspan="10" style="text-align: center; color: #e45227; font-size: 14px;">Nuk ka asnjë rezultat!</td>
        </tr>
        {/if}
    {* {$order_details|var_dump} *}

        {foreach $order_details as $row}
            <tr>
                <td>
                    <a href="{"orders.details&order_id={$row.order_id}"|fn_url}">{$row.order_id}</a>
                </td>
                <td>
                    <a href="{"profiles.update&user_id={$row.user_id}"|fn_url}">{$row.user_id}</a>
                </td>
                <td>
                    {$row.subtotal}
                </td>
                <td>
                    {$row.subtotal_discount}
                </td>
                <td>
                    {$row.discount}
                </td>
                <td>
                    {$row.total}
                </td>
                <td>
                    {$row.shipping}
                </td>
                <td>
                    {$row.payment}
                </td>
                <td>
                    {$row.description}
                </td>
                <td>
                    {$row.date}
                </td>
            </tr>
        {/foreach}
        </tbody>
    </table>
    {if $pages.total != 1}
        <div class="clearfix">
            <div id="pagination-custom"></div>
        </div>
    {/if}
{/capture}
{capture name="buttons"}
    {capture name="tools_list"}
        {assign var="order_id" value="&order_id={$smarty.get.order_id}"}
        {assign var="user_id" value="&user_id={$smarty.get.user_id}"}
        <li>{btn type="list" text="Shkarko në Excel" href="custom_reports.order_details{$order_id}{$user_id}&export=true"}</li>
    {/capture}
    {dropdown content=$smarty.capture.tools_list}
{/capture}
{include file="common/mainbox.tpl" title="Detajet e porosive" content=$smarty.capture.mainbox  buttons=$smarty.capture.buttons sidebar=$smarty.capture.sidebar adv_buttons=$smarty.capture.adv_buttons select_languages=true}

<script>
    {literal}
    $(document).ready(function() {
        $('.js-example-basic-multiple').select2();
    });
    {/literal}

    {ldelim}
        var pages = {$pages.total};
        var active = {$pages.current};
        var custom_pagination = document.getElementById('pagination-custom');

        if (custom_pagination) {
            document.getElementById('pagination-custom').innerHTML = createPagination(pages, active);
        }

        function createPagination(pages, page) {
            {assign var="page_var" value="&page="}
            let url_current = window.location.href +'&page=';
            let str = '<ul>';
            let active;
            let pageCutLow = page - 1;
            let pageCutHigh = page + 1;

            if (page > 1) {
                str += '<li class="page-item previous no"><a href="'+url_current+''+(page-1)+' " onclick="createPagination(pages, '+(page-1)+')">Paraprak</a></li>';
            }
            if (pages < 6) {
                for (let p = 1; p <= pages; p++) {
                    active = page == p ? "active" : "no";
                    str += '<li class="'+active+'"><a href="'+url_current+''+p+'" onclick="createPagination(pages, '+p+')">'+ p +'</a></li>';
                }
            }
            else {
                if (page > 2) {
                    str += '<li class="no page-item"><a href="'+url_current+''+1+'" onclick="createPagination(pages, 1)">1</a></li>';
                    if (page > 3) {
                        str += '<li class="out-of-range"><a onclick="createPagination(pages,'+(page-2)+')">...</a></li>';
                    }
                }
                if (page === 1) {
                    pageCutHigh += 2;
                } else if (page === 2) {
                    pageCutHigh += 1;
                }
                if (page === pages) {
                    pageCutLow -= 2;
                } else if (page === pages-1) {
                    pageCutLow -= 1;
                }
                for (let p = pageCutLow; p <= pageCutHigh; p++) {
                    if (p === 0) {
                        p += 1;
                    }
                    if (p > pages) {
                        continue
                    }
                    active = page == p ? "active" : "no";
                    str += '<li class="page-item '+active+'"><a href="'+url_current+''+p+'" onclick="createPagination(pages, '+p+')">'+ p +'</a></li>';
                }
                if (page < pages-1) {
                    if (page < pages-2) {
                        str += '<li class="out-of-range"><a onclick="createPagination(pages,'+(page+2)+')">...</a></li>';
                    }
                    str += '<li class="page-item no"><a href="'+url_current+''+pages+'" onclick="createPagination(pages, pages)">'+pages+'</a></li>';
                }
            }
            if (page < pages) {
                str += '<li class="page-item next no"><a  href="'+url_current+''+(page+1)+'" onclick="createPagination(pages, '+(page+1)+')">Vijues</a></li>';
            }
            str += '</ul>';
            document.getElementById('pagination-custom').innerHTML = str;
            return str;
        }

        function resetSearch() {
            window.location.href = "{'custom_reports.order_details'|fn_url}";
        }
    {rdelim}
</script>
{* {$search_params|var_dump} *}