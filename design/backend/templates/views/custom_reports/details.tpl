<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
{literal}
    <style>
        .sales-card{
            width:300px;
            height: 100px;
            background:rgb(234, 234, 234);
            border-radius: 2%;
            margin: 10px;
        }
        .sales-total{
            float: right;
            font-size: 20px;
            font-weight: 400;
            color: #6d6d6d;
            margin-top: 15%;
            margin-right: 5%;
        }
        .chart-icon{
            font-size: 60px;
            margin: 20px;
        }
        .sales-title-r {
            left: 220px;
            position: absolute;
            font-size: 15px;
            font-weight: 400;
        }
        .sales-title-i {
            left: 550px;
            position: absolute;
            font-size: 15px;
            font-weight: 400;
        }
    </style>
{/literal}
{capture name="mainbox"}
    <div class="cards-container" style="display: flex">
       <div class="sales-card">
           <i class="fas fa-chart-bar chart-icon"></i>
           <div class="sales-total">
               {$user_order_count} euro
           </div>
           <span class="sales-title-r" style="margin-top: 5px">
               Të realizuara
            </span>
       </div>
    <table>
        <thead>
        <tr class="cm-first-sibling">
            <td>Email: {$user_data[0].email}</td>
        </tr>
        <tr class="cm-first-sibling">
            <td>Emri : {$user_data[0].firstname}</td>
        </tr>
        <tr class="cm-first-sibling">
            <td>Mbiemri : {$user_data[0].lastname}</td>
        </tr>
        <tr class="cm-first-sibling">
            <td>Telefoni : {$user_data[0].phone}</td>
        </tr>

        </thead>
    </table>

    </div>
    <table class="table table-middle">
        <thead>
        <tr class="cm-first-sibling">
            <th>Order ID</th>
            <th>Data</th>
            <th>Shuma</th>
        </tr>
        </thead>
        <tbody>
            {foreach $user_order_details as $order}
            <tr>
                <td>
                    <a href="{"orders.details&order_id={$order.order_id}"|fn_url}">{$order.order_id}</a>
                </td>
                <td>
                    {$order.timestamp|date_format}
                </td>
                <td>
                    {$order.total}
                </td>
            </tr>
            {/foreach}
        </tbody>
    </table>

{/capture}

{include file="common/mainbox.tpl" title="Detalet e Shitjes për {$user_data[0].email} #{$user_data[0].user_id}" content=$smarty.capture.mainbox  buttons=$smarty.capture.buttons sidebar=$smarty.capture.sidebar adv_buttons=$smarty.capture.adv_buttons select_languages=true}
