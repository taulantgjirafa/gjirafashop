{capture name="mainbox"}
    {assign var="is_al" value=$al|fn_isAL}
    {capture name="tabsbox"}
        <form id='form' action="{""|fn_url}" method="post" name="attributes_update_form" class="form-horizontal form-edit  cm-disable-empty-files" enctype="multipart/form-data">
            <div class="form-inline">
                <input type="file" name="attributes">
            </div>

            {** Form submit section **}
            {capture name="buttons"}
                {include file="buttons/save.tpl" but_role="submit-link" but_name="dispatch[product_options.attributes]" but_target_form="attributes_update_form" save=$id}

            {/capture}
            {** /Form submit section **}

        </form> {* /product update form *}

    {/capture}
    {include file="common/tabsbox.tpl" content=$smarty.capture.tabsbox group_name=$runtime.controller active_tab=$smarty.request.selected_section track=true}

{/capture}

{include file="common/mainbox.tpl"
title="Product attributes"
content=$smarty.capture.mainbox
select_languages=$id
buttons=$smarty.capture.buttons
adv_buttons=$smarty.capture.adv_buttons
}