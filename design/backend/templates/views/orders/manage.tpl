<style>{literal}.table td {
        padding: 5px 0;
    }{/literal}</style>

{literal}
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css"
          integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
    <style>
        /* Hover tooltips */
        .field-tip {
            position: relative;
            cursor: help;
        }

        .field-tip .tip-content {
            position: absolute;
            top: -22px; /* - top padding */
            right: 9999px;
            width: 200px;
            margin-right: -220px; /* width + left/right padding */
            padding: 10px;
            color: #fff;
            background: #333;
            -webkit-box-shadow: 2px 2px 5px #aaa;
            -moz-box-shadow: 2px 2px 5px #aaa;
            box-shadow: 2px 2px 5px #aaa;
            opacity: 0;
            -webkit-transition: opacity 250ms ease-out;
            -moz-transition: opacity 250ms ease-out;
            -ms-transition: opacity 250ms ease-out;
            -o-transition: opacity 250ms ease-out;
            transition: opacity 250ms ease-out;
            z-index: 999;
        }

        /* <http://css-tricks.com/snippets/css/css-triangle/> */
        .field-tip .tip-content:before {
            content: ' '; /* Must have content to display */
            position: absolute;
            top: 50%;
            left: -16px; /* 2 x border width */
            width: 0;
            height: 0;
            margin-top: -8px; /* - border width */
            border: 8px solid transparent;
            border-right-color: #333;
        }

        .field-tip:hover .tip-content {
            right: -20px;
            opacity: 1;
        }
    </style>

{/literal}
{capture name="mainbox"}

    {if $runtime.mode == "new"}
        <p>{__("text_admin_new_orders")}</p>
    {/if}

    {capture name="sidebar"}
        {include file="common/saved_search.tpl" dispatch="orders.manage" view_type="orders"}
        {include file="views/orders/components/orders_search_form.tpl" dispatch="orders.manage"}
    {/capture}
    <form action="{""|fn_url}" method="post" target="_self" name="orders_list_form">

        {include file="common/pagination.tpl" save_current_page=true save_current_url=true div_id=$smarty.request.content_id}

        {assign var="c_url" value=$config.current_url|fn_query_remove:"sort_by":"sort_order"}
        {assign var="c_icon" value="<i class=\"exicon-`$search.sort_order_rev`\"></i>"}
        {assign var="c_dummy" value="<i class=\"exicon-dummy\"></i>"}

        {assign var="rev" value=$smarty.request.content_id|default:"pagination_contents"}

        {if $incompleted_view}
            {assign var="page_title" value=__("incompleted_orders")}
            {assign var="get_additional_statuses" value=true}
        {else}
            {assign var="page_title" value=__("orders")}
            {assign var="get_additional_statuses" value=false}
        {/if}
        {assign var="order_status_descr" value=$smarty.const.STATUSES_ORDER|fn_get_simple_statuses:$get_additional_statuses:true}
        {assign var="extra_status" value=$config.current_url|escape:"url"}
        {$statuses = []}
        {assign var="order_statuses" value=$smarty.const.STATUSES_ORDER|fn_get_statuses:$statuses:$get_additional_statuses:true}

        {if $orders}
            <table width="100%" class="table table-middle">
                <thead>
                <tr>
                    <th class="left">
                        {include file="common/check_items.tpl" check_statuses=$order_status_descr}
                    </th>
                    <th width="17%"><a class="cm-ajax"
                                       href="{"`$c_url`&sort_by=order_id&sort_order=`$search.sort_order_rev`"|fn_url}"
                                       data-ca-target-id={$rev}>{__("id")}{if $search.sort_by == "order_id"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a>
                    </th>
                    <th width="17%"><a class="cm-ajax"
                                       href="{"`$c_url`&sort_by=status&sort_order=`$search.sort_order_rev`"|fn_url}"
                                       data-ca-target-id={$rev}>{__("status")}{if $search.sort_by == "status"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a>
                    </th>
                    <th width="15%"><a class="cm-ajax"
                                       href="{"`$c_url`&sort_by=date&sort_order=`$search.sort_order_rev`"|fn_url}"
                                       data-ca-target-id={$rev}>{__("date")}{if $search.sort_by == "date"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a>
                    </th>
                    <th width="20%"><a class="cm-ajax"
                                       href="{"`$c_url`&sort_by=customer&sort_order=`$search.sort_order_rev`"|fn_url}"
                                       data-ca-target-id={$rev}>{__("customer")}{if $search.sort_by == "customer"}{$c_icon nofilter}{/if}</a>
                    </th>
                    <th width="15%"><a class="cm-ajax"
                                       href="{"`$c_url`&sort_by=phone&sort_order=`$search.sort_order_rev`"|fn_url}"
                                       data-ca-target-id={$rev}>{__("phone")}{if $search.sort_by == "phone"}{$c_icon nofilter}{/if}</a>
                    </th>

                    {hook name="orders:manage_header"}{/hook}


                    <th width="15%"><a class="cm-ajax"
                                       href="{"`$c_url`&sort_by=phone&sort_order=`$search.sort_order_rev`"|fn_url}"
                                       data-ca-target-id={$rev}>Tags</a></th>
                    <th width="15%"><a class="cm-ajax"
                                       href="{"`$c_url`&sort_by=phone&sort_order=`$search.sort_order_rev`"|fn_url}"
                                       data-ca-target-id={$rev}>Notes</a></th>
                    <th>&nbsp;</th>
                    <th width="10%" class="right"><a
                                class="cm-ajax{if $search.sort_by == "total"} sort-link-{$search.sort_order_rev}{/if}"
                                href="{"`$c_url`&sort_by=total&sort_order=`$search.sort_order_rev`"|fn_url}"
                                data-ca-target-id={$rev}>{__("total")}</a></th>
                    <th width="20%" class="right"><a
                                class="cm-ajax{if $search.sort_by == "markup"} sort-link-{$search.sort_order_rev}{/if}"
                                href="{"`$c_url`&sort_by=markup&sort_order=`$search.sort_order_rev`"|fn_url}"
                                data-ca-target-id={$rev}>Gross margin</a></th>

                </tr>
                </thead>
                <script>var user_ids = [];</script>
                {foreach from=$orders item="o"}
                    {hook name="orders:order_row"}
                        <tr data-uid="{$o.user_id}">
                            <td class="left">
                                <input style="margin: 0 auto;display: block;" type="checkbox" name="order_ids[]" value="{$o.order_id}"
                                       class="cm-item cm-item-status-{$o.status|lower}"/>
                            </td>
                            <td width="10%">
                                <a href="{"orders.details?order_id=`$o.order_id`"|fn_url}"
                                   class="underlined">{__("order")} #{$o.order_id}</a>
                                {if $order_statuses[$o.status].params.appearance_type == "I" && $o.invoice_id}
                                    <p class="small-note">{__("invoice")} #{$o.invoice_id}</p>
                                {elseif $order_statuses[$o.status].params.appearance_type == "C" && $o.credit_memo_id}
                                    <p class="small-note">{__("credit_memo")} #{$o.credit_memo_id}</p>
                                {/if}
                                {include file="views/companies/components/company_name.tpl" object=$o}
                            </td>
                            <td class="center">
                                {if "MULTIVENDOR"|fn_allowed_for}
                                    {assign var="notify_vendor" value=true}
                                {else}
                                    {assign var="notify_vendor" value=false}
                                {/if}

                                {include file="common/select_popup.tpl" suffix="o" order_info=$o id=$o.order_id status=$o.status items_status=$order_status_descr update_controller="orders" notify=true notify_department=true notify_vendor=$notify_vendor send_invoice=true status_target_id="orders_total,`$rev`" extra="&return_url=`$extra_status`" statuses=$order_statuses btn_meta="btn btn-info o-status-`$o.status` btn-small"|lower}
                            </td>
                            <td class="nowrap">{$o.timestamp|date_format:"`$settings.Appearance.date_format`, `$settings.Appearance.time_format`"}</td>
                            <td>
                                {* {if $o.email}<a href="mailto:{$o.email|escape:url}">@</a> {/if} *}
                                {if $o.user_id}<a
                                        href="{"profiles.update?user_id=`$o.user_id`"|fn_url}">{/if}{$o.firstname} {$o.lastname}{if $o.user_id}</a>{/if}
                            </td>
                            <td>{$o.phone}</td>

                            {hook name="orders:manage_data"}{/hook}

                            <td>
                                {if $o.tags != null}
                                    {foreach $o.tags as $tag}
                                        {if $tag != null }
                                            {if $tag.tag_id == 147}
                                                {$o['disable']= 1}
                                            {/if}
                                            <span class="badge badge-Info"
                                                  style="white-space: inherit; margin: 3px">{$tag.tags}</span>
                                        {/if}
                                    {/foreach}
                                {/if}
                            </td>
                            <td style="text-align: center">
                                {foreach $o.notes as $note}
                                    {if $note != ''}
                                        <span class="field-tip">
                   <i class="far fa-user"></i>
                    <span class="tip-content">
                        Shënimet e klientit :
                        <br>
                        {$note}</span>
                </span>
                                    {/if}
                                {/foreach}
                                {foreach $o.details as $staff_notes}
                                    {if $staff_notes != ''}
                                        <span class="field-tip">
                   <i class="fas fa-user"></i>
                    <span class="tip-content">
                        Shënimet për staff :
                        <br>
                        {$staff_notes}</span>
                </span>
                                    {/if}
                                {/foreach}
                            </td>
                            <td width="5%" class="center">
                                {capture name="tools_items"}
                                    <li>{btn type="list" href="orders.details?order_id=`$o.order_id`" text={__("view")}}</li>
                                    {hook name="orders:list_extra_links"}
                                    {if $o.disable != 1}
                                        <li>{btn type="list" href="order_management.edit?order_id=`$o.order_id`" text={__("edit")}}</li>
                                    {/if}
                                    {assign var="current_redirect_url" value=$config.current_url|escape:url}
                                        <li>{btn type="list" href="orders.delete?order_id=`$o.order_id`&redirect_url=`$current_redirect_url`" class="cm-confirm" text={__("delete")} method="POST"}</li>
                                    {/hook}
                                {/capture}
                                <div class="hidden-tools">
                                    {dropdown content=$smarty.capture.tools_items}
                                </div>
                            </td>
                            <td class="right">
                                {include file="common/price.tpl" value=$o.total}
                            </td>
                            <td class="right">
                                % {$o.markup}
                            </td>
                        </tr>
                        <script>user_ids.push({$o.user_id});</script>
                    {/hook}
                {/foreach}
            </table>
        {else}
            <p class="no-items">{__("no_data")}</p>
        {/if}

        {if $orders}
            <div class="statistic clearfix" id="orders_total">
                {hook name="orders:statistic_list"}
                    <table class="pull-right ">
                        <tr>
                            <td class="shift-right">Usera të rikthyer (unik): </td>
                            <td id="total_returned_users"></td>
                        </tr>
                        {if $total_pages > 1 && $search.page != "full_list"}
                            <tr>
                                <td>&nbsp;</td>
                                <td width="100px">{__("for_this_page_orders")}:</td>
                            </tr>
                            <tr>
                                <td>{__("gross_total")}:</td>
                                <td>{include file="common/price.tpl" value=$display_totals.gross_total}</td>
                            </tr>
                            {if !$incompleted_view}
                                <tr>
                                    <td>{__("totally_paid")}:</td>
                                    <td>{include file="common/price.tpl" value=$display_totals.totally_paid}</td>
                                </tr>
                            {/if}
                            <hr/>
                            <tr>
                                <td>{__("for_all_found_orders")}:</td>
                            </tr>
                        {/if}
                        <tr>
                            <td class="shift-right">{__("gross_total")}:</td>
                            <td>{include file="common/price.tpl" value=$totals.gross_total}</td>
                        </tr>
                        {hook name="orders:totals_stats"}
                        {if !$incompleted_view}
                            <tr>
                                <td class="shift-right"><h4>{__("totally_paid")}:</h4></td>
                                <td class="price">{include file="common/price.tpl" value=$totals.totally_paid}</td>
                            </tr>
                        {/if}
                        {/hook}
                        {if $gross_margin}
                            <tr>
                                <td class="shift-right"><h4>Gross Mesatarja:</h4></td>
                                <td class="price">% {$gross_margin|string_format:"%.3f"}</td>
                            </tr>
                        {/if}
                    </table>
                {/hook}
                <!--orders_total--></div>
        {/if}

        {include file="common/pagination.tpl" div_id=$smarty.request.content_id}


        {capture name="adv_buttons"}
            {hook name="orders:manage_tools"}
                {include file="common/tools.tpl" tool_href="order_management.new" prefix="bottom" hide_tools="true" title=__("add_order") icon="icon-plus"}
            {/hook}
        {/capture}

    </form>
{/capture}

{capture name="incomplete_button"}
    {if $incompleted_view}
        <li>{btn type="list" href="orders.manage" text={__("view_all_orders")}}</li>
    {else}
        <li>{btn type="list" href="orders.manage?skip_view=Y&status=`$smarty.const.STATUS_INCOMPLETED_ORDER`" text={__("incompleted_orders")} form="orders_list_form"}</li>
    {/if}
{/capture}

{capture name="buttons"}
    {capture name="tools_list"}
    {if $orders}
    <li>{btn type="list" text={__("bulk_print_invoice")} dispatch="dispatch[orders.bulk_print]" form="orders_list_form" class="cm-new-window"}</li>
<li>{btn type="list" text="{__("bulk_print_pdf")}" dispatch="dispatch[orders.bulk_print..pdf]" form="orders_list_form"}</li>
            <li>{btn type="list" text="{__("bulk_print_packing_slip")}" dispatch="dispatch[orders.packing_slip]" form="orders_list_form" class="cm-new-window"}</li>
    <li>{btn type="list" text={__("view_purchased_products")} dispatch="dispatch[orders.products_range]" form="orders_list_form"}</li>

    <li class="divider"></li>
    <li>{btn type="list" text={__("export_selected")} dispatch="dispatch[orders.export_range]" form="orders_list_form"}</li>

    {$smarty.capture.incomplete_button nofilter}

    {if $orders && !$runtime.company_id}
    <li class="divider"></li>
    <li>{btn type="delete_selected" dispatch="dispatch[orders.m_delete]" form="orders_list_form"}</li>
    {/if}
    {else}
    {$smarty.capture.incomplete_button nofilter}
    {/if}
    {hook name="orders:list_tools"}
    {/hook}
    {/capture}
    {dropdown content=$smarty.capture.tools_list}
    {/capture}

    {include file="common/mainbox.tpl" title=$page_title sidebar=$smarty.capture.sidebar content=$smarty.capture.mainbox buttons=$smarty.capture.buttons adv_buttons=$smarty.capture.adv_buttons content_id="manage_orders"}

    {literal}
    <script>
        var data = Array.from(new Set(user_ids));

        $.ajax({
            url: '/adminGj123.php',
            data: {
                dispatch: "orders.get_user_order_count",
                user_ids: JSON.stringify(data)
            },
            type: 'GET',
            success: function (e) {
                var e = JSON.parse(e);
                $('.table-middle > tbody tr').css('border-left', '2px solid #f67e7e');
                for(var i = 0; i < e.length; i++){
                    $('.table-middle > tbody tr[data-uid="' + e[i].uid + '"]').css('border-left', '2px solid green');
                }
                $('#total_returned_users').text(e.length);
            }
        });
    </script>
{/literal}