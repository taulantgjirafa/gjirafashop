{if $order_info.shipping}
    {foreach from=$order_info.shipping item="shipping" key="shipping_id" name="f_shipp"}
        {if $use_shipments && "shipments.add"|fn_check_view_permissions}
            {capture name="add_new_picker"}
                {include file="views/shipments/components/new_shipment.tpl" group_key=$shipping.group_key}
            {/capture}
            {include file="common/popupbox.tpl" id="add_shipment_`$shipping.group_key`" content=$smarty.capture.add_new_picker text=__("new_shipment") act="hidden"}
        {/if}
    {/foreach}
{else}
    {foreach from=$order_info.product_groups item="group" key="group_id"}
        {if $group.all_free_shipping}
            {if $use_shipments && "shipments.add"|fn_check_view_permissions}
                {capture name="add_new_picker"}
                    {include file="views/shipments/components/new_shipment.tpl" group_key=0}
                {/capture}
                {include file="common/popupbox.tpl" id="add_shipment_0" content=$smarty.capture.add_new_picker text=__("new_shipment") act="hidden"}
            {/if}
        {/if}
    {/foreach}
{/if}

<form action="{""|fn_url}" method="post" name="order_info_form" class="form-horizontal form-edit form-table">
    <input type="hidden" name="order_id" value="{$smarty.request.order_id}"/>
    <input type="hidden" name="order_status" value="{$order_info.status}"/>

    <input type="hidden" name="result_ids" value="content_general"/>
    <input type="hidden" name="selected_section" value="{$smarty.request.selected_section}"/>

    {capture name="mainbox"}
        {capture name="tabsbox"}
            <div id="content_general">
                <div class="row-fluid">
                    <div class="span8">
                        {* Products info *}
                        <table width="100%" class="table table-middle">
                            <thead>
                            <tr>
                                <th width="50%">{__("product")}</th>
                                <th width="10%">{__("price")}</th>
                                <th class="center">{__("list_price")}</th>
                                <th class="center" width="10%">{__("quantity")}</th>
                                {if $order_info.use_discount}
                                    <th width="5%">{__("discount")}</th>
                                {/if}
                                {if $order_info.taxes && $settings.General.tax_calculation != "subtotal"}
                                    <th width="10%">&nbsp;{__("tax")}</th>
                                {/if}
                                <th width="10%" class="right">&nbsp;{__("subtotal")}</th>
                                <th>Çmimi pa tvsh</th>
                            </tr>
                            </thead>
                            {assign var="total_gjflex" value=0}
                            {assign var="bundle_order" value=false}
                            {foreach from=$order_info.products item="oi" key="key"}

                                {if $oi.extra.bundle_order}
                                    {assign var="bundle_order" value=true}
                                    {assign var="bundle_display_subtotal" value=$oi.extra.bundle_display_subtotal}
                                {/if}

                                {assign var="gjflex" value=$oi.base_price|fn_calculate_gjflex}

                                {hook name="orders:items_list_row"}
                                {if !$oi.extra.parent}
                                    {assign var="meta_container" value=intval($oi.product_id/8000+1)}
                                    {if $meta_container gt 5}
                                        {$meta_container=5}
                                    {/if}
                                    {assign var="meta_azure_path" value=("https://hhstsyoejx.gjirafa.net/gj50/img/`$oi.product_id`/thumb/0.jpg")}
                                    <tr>
                                        <td>
                                            <div class="order-product-image">
                                                <img style="max-height: 50px;max-width:100%;margin: 10px 0;"
                                                     src="{$meta_azure_path}" alt="">
                                            </div>
                                            <div class="order-product-info">
                                                {if !$oi.deleted_product}<a
                                                        href="{"products.update?product_id=`$oi.product_id`"|fn_url}">{/if}{$oi.product nofilter}{if !$oi.deleted_product}</a>{/if}
                                                <div class="products-hint">
                                                    {hook name="orders:product_info"}
                                                    {if $oi.product_code}<p>{__("sku")}:{$oi.product_code}</p>{/if}
                                                    {/hook}
                                                    <p>CZC Stock: {$oi.retailer_stock.czc_stock}</p>
                                                    <p>CZC Retailer Stock: {$oi.retailer_stock.czc_retailer_stock}</p>
                                                    <p>Depo Stock: {$oi.stock_wms}</p>
                                                    <p {if $oi.price_alert == true} style="background-color: #e45227; width: 40%; font-weight: bold; padding: 5px; border-radius: 5px;font-size: 12px;color: #fff;" {/if}>
                                                        Çmimi CZC: {$oi.czc_price}</p>
                                                </div>
                                                {if $oi.extra.stock}
                                                    <div class="products-hint">
                                                        Stoku në kohën e porosisë:
                                                        <p>CZC Stock: {$oi.extra.stock.czc_stock}</p>
                                                        <p>CZC Retailer Stock: {$oi.extra.stock.czc_retailer_stock}</p>
                                                        <p>Depo Stock: {$oi.extra.stock.stock}</p>
                                                    </div>
                                                {/if}
                                                {if $oi.product_options}
                                                    <div class="options-info">{include file="common/options_info.tpl" product_options=$oi.product_options}</div>{/if}
                                            </div>
                                        </td>
                                        <td class="left">

                                            {if $oi.extra.exclude_from_calculate}{__("free")}{else}{include file="common/price.tpl" value=$oi.original_price}{/if}
                                            {if $oi.gjflex}
                                                <div>+</div>
                                                <div>
                                                    <strong>{__("gjirafa_flex")}</strong>

                                                    {if $oi.extra.gjflex_fixed && $oi.extra.gjflex_fixed != 0}
                                                        {assign var="gjflex" value=$oi.extra.gjflex_fixed}
                                                    {/if}

                                                    {include file="common/price.tpl" value=$gjflex span_id="product_price_`$key`" class="ty-sub-price"}
                                                </div>
                                            {/if}
                                        </td>
                                        {if $oi.list_price != '0'}
                                            <td class="center">{include file="common/price.tpl" value=$oi.list_price}</td>
                                        {else}
                                            <td class="center">-</td>
                                        {/if}
                                        <td class="center">
                                            &nbsp;{$oi.amount}<br/>
                                            {if !"ULTIMATE:FREE"|fn_allowed_for && $use_shipments && $oi.shipped_amount > 0}
                                                &nbsp;
                                                <span class="muted"><small>({$oi.shipped_amount}&nbsp;{__("shipped")})</small></span>
                                            {/if}
                                        </td>
                                        {if $order_info.use_discount}
                                            <td class="nowrap">
                                                {if $oi.extra.discount|floatval}{include file="common/price.tpl" value=$oi.extra.discount}{else}-{/if}</td>
                                        {/if}
                                        {if $order_info.taxes && $settings.General.tax_calculation != "subtotal"}
                                            <td class="nowrap">
                                                {if $oi.tax_value|floatval}{include file="common/price.tpl" value=$oi.tax_value}{else}-{/if}</td>
                                        {/if}
                                        <td class="right">&nbsp;
                                            {if $oi.gjflex}
                                                {*{$oi.display_subtotal = $oi.display_subtotal + $gjflex}*}
                                                {$total_gjflex = $total_gjflex + ($gjflex  * $oi.amount)}
                                            {/if}

                                            <span>{if $oi.extra.exclude_from_calculate}{__("free")}{else}{include file="common/price.tpl" value=$oi.display_subtotal}{/if}</span>
                                        </td>
                                        {if $oi.price_without_vat}
                                            <td>{include file="common/price.tpl" value=$oi.price_without_vat * $oi.amount}</td>
                                        {else}
                                            <td class="center">-</td>
                                        {/if}
                                    </tr>
                                {/if}
                                {/hook}
                            {/foreach}
                            {hook name="orders:extra_list"}
                            {/hook}
                        </table>

                        <!--{***** Customer note, Staff note & Statistics *****}-->
                        {hook name="orders:totals"}
                            <div class="order-notes statistic">

                                <div class="clearfix">
                                    <table class="pull-right">
                                        <tr class="totals">
                                            <td>&nbsp;</td>
                                            <td width="100px"><h4>{__("totals")}</h4></td>
                                        </tr>

                                        <tr>
                                            <td>{__("subtotal")}:</td>
                                            {*                                            {if $bundle_order}*}
                                            {*                                                <td data-ct-totals="subtotal">{include file="common/price.tpl" value=$bundle_display_subtotal}</td>*}
                                            {*                                            {else}*}
                                            <td data-ct-totals="subtotal">{include file="common/price.tpl" value=$order_info.display_subtotal}</td>
                                            {*                                            {/if}*}
                                        </tr>

                                        {if $total_gjflex != 0}
                                            <tr>
                                                <td>{__("gjirafa_flex")}:</td>
                                                <td data-ct-totals="subtotal">{include file="common/price.tpl" value=$total_gjflex}</td>
                                            </tr>
                                        {/if}

                                        {if $order_info.display_shipping_cost|floatval}
                                            <tr>
                                                <td>{__("shipping_cost")}:</td>
                                                <td data-ct-totals="shipping_cost">{include file="common/price.tpl" value=$order_info.display_shipping_cost}</td>
                                            </tr>
                                        {/if}

                                        {if $order_info.discount|floatval}
                                            <tr>
                                                <td>{__("including_discount")}:</td>
                                                <td data-ct-totals="including_discount">{include file="common/price.tpl" value=$order_info.discount}</td>
                                            </tr>
                                        {/if}

                                        {if $order_info.subtotal_discount|floatval}
                                            <tr>
                                                <td>{__("order_discount")}:</td>
                                                <td data-ct-totals="order_discount">{include file="common/price.tpl" value=$order_info.subtotal_discount}</td>
                                            </tr>
                                        {/if}

                                        {if $order_info.coupons}
                                            {foreach from=$order_info.coupons key="coupon" item="_c"}
                                                <tr>
                                                    <td>{__("discount_coupon")}:</td>
                                                    <td data-ct-totals="discount_coupon">{$coupon}</td>
                                                </tr>
                                            {/foreach}
                                        {/if}

                                        <tr>
                                            <td>Totali pa TVSH:</td>
                                            <td>{include file="common/price.tpl" value={$order_info.total_without_vat}}</td>
                                        </tr>

                                        {if $order_info.taxes}
                                            <tr>
                                                <td>{__("taxes")}:</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            {foreach from=$order_info.taxes item="tax_data"}
                                                <tr>
                                                    <td>&nbsp;<span>&middot;</span>&nbsp;{$tax_data.description}
                                                        &nbsp;{include file="common/modifier.tpl" mod_value=$tax_data.rate_value mod_type=$tax_data.rate_type}{if $tax_data.price_includes_tax == "Y" && ($settings.Appearance.cart_prices_w_taxes != "Y" || $settings.General.tax_calculation == "subtotal")}&nbsp;{__("included")}{/if}{if $tax_data.regnumber}&nbsp;({$tax_data.regnumber}){/if}
                                                    </td>
                                                    <td data-ct-totals="taxes-{$tax_data.description}">{include file="common/price.tpl" value=$tax_data.tax_subtotal}</td>
                                                </tr>
                                            {/foreach}
                                        {/if}

                                        {if $order_info.tax_exempt == "Y"}
                                            <tr>
                                                <td>{__("tax_exempt")}</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                        {/if}

                                        {if $order_info.payment_surcharge|floatval && !$take_surcharge_from_vendor}
                                            <tr>
                                                <td>{$order_info.payment_method.surcharge_title|default:__("payment_surcharge")}
                                                    :
                                                </td>
                                                <td data-ct-totals="payment_surcharge">{include file="common/price.tpl" value=$order_info.payment_surcharge}</td>
                                            </tr>
                                        {/if}

                                        {hook name="orders:totals_content"}
                                        {/hook}

                                        {if $order_info.isset_returns}
                                            {foreach $order_info.returned_products as $returned_prod}
                                                {assign var="returned_sum" value="{$returned_sum + $returned_prod.price}"}
                                            {/foreach}
                                            <tr class="ty-orders-summary__row">
                                                <td>Totali para kthimit
                                                    :&nbsp;
                                                </td>
                                                <td data-ct-orders-summary="summary-surchange">{include file="common/price.tpl" value=$returned_sum + $order_info.total}</td>
                                            </tr>
                                        {/if}
                                        <tr>
                                            <td><h4>{__("total")}:</h4></td>
                                            <td class="price" data-ct-totals="total">{include file="common/price.tpl" value=$order_info.total}</td>
                                        </tr>
                                        {if $order_info.payment_total != -1}
                                            <tr>
                                                <td><h4>Totali i pagesës:</h4></td>
                                                <td class="price" data-ct-totals="total">{include file="common/price.tpl" value=$order_info.payment_total}</td>
                                            </tr>
                                        {/if}
                                    </table>
                                </div>

                                <div class="note clearfix">
                                    <div class="span6">
                                        <label for="notes">{__("customer_notes")}</label>
                                        <textarea class="span12" name="update_order[notes]" id="notes" cols="40"
                                                  rows="5">{$order_info.notes}</textarea>
                                    </div>
                                    <div class="span6">
                                        <label for="details">{__("staff_only_notes")}</label>
                                        <textarea class="span12" name="update_order[details]" id="details" cols="40"
                                                  rows="5">{if !$order_admin_notes}{$order_info.details}{/if}</textarea>
                                    </div>
                                </div>

                                {if $order_admin_notes}
                                    Shënimet e stafit:
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <td align="left" width="20%"
                                                style="text-align: unset; font-weight: bold;">{__('user')}:
                                            </td>
                                            <td align="left" width="60%"
                                                style="text-align: unset; font-weight: bold;">{__('note')}:
                                            </td>
                                            <td align="left" width="20%"
                                                style="text-align: unset; font-weight: bold;">{__('date')}:
                                            </td>
                                        </tr>
                                        </thead>
                                        {foreach from=$order_admin_notes item='note'}
                                            <tbody>
                                            <tr>
                                                <td align="left" width="20%"
                                                    style="text-align: unset;">{$note.user_id|fn_get_user_name}</td>
                                                <td align="left" width="60%"
                                                    style="text-align: unset;">{$note.note}</td>
                                                <td align="left" width="20%" style="text-align: unset;"
                                                ">{$note.created_at}</td>
                                            </tr>
                                            </tbody>
                                        {/foreach}
                                    </table>
                                {/if}

                                {if $order_client_notes}
                                    Shënimet e klientit:
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <td align="left" width="20%"
                                                style="text-align: unset; font-weight: bold;">{__('user')}:
                                            </td>
                                            <td align="left" width="60%"
                                                style="text-align: unset; font-weight: bold;">{__('note')}:
                                            </td>
                                            <td align="left" width="20%"
                                                style="text-align: unset; font-weight: bold;">{__('date')}:
                                            </td>
                                        </tr>
                                        </thead>
                                        {foreach from=$order_client_notes item='note'}
                                            <tbody>
                                            <tr>
                                                <td align="left" width="20%"
                                                    style="text-align: unset;">{$note.user_id|fn_get_user_name}</td>
                                                <td align="left" width="60%"
                                                    style="text-align: unset;">{$note.note}</td>
                                                <td align="left" width="20%" style="text-align: unset;"
                                                ">{$note.created_at}</td>
                                            </tr>
                                            </tbody>
                                        {/foreach}
                                    </table>
                                {/if}

                            </div>
                        {/hook}

                        <!--{***** /Customer note, Staff note & Statistics *****}-->

                        {hook name="orders:staff_only_note"}
                        {/hook}

                    </div>
                    <div class="span4">
                        <div class="well orders-right-pane form-horizontal">

                            {if $reserve_order && $display_reserve_button}
                                <div class="control-group text-center w100">
                                    <div class="control-label"><h4 class="subheader">{__("products")}</h4></div>
                                    <button id="reserve_order" class="btn cm-ajax" name="dispatch[orders.reserve_order]"
                                            style="background-color: #e45227; color: #fff; text-shadow: none; border: none; background-image: none;">
                                        Rezervo automatikisht
                                    </button>
                                </div>
                            {/if}
                            {if $order_info.calculate_without_vat == 'Y'}
                                <div class="control-group text-center w100">
                                    <div class="control-label"><h4 class="subheader">{__("order")}</h4></div>
                                    <a href="{"orders.update_with_vat?order_id={$order_info.order_id}"|fn_url}" class="btn">Llogarit me TVSH</a>
                                </div>
                            {else}
                                <div class="control-group text-center w100">
                                    <div class="control-label"><h4 class="subheader">{__("order")}</h4></div>
                                    <a href="{"orders.update_without_vat?order_id={$order_info.order_id}"|fn_url}" class="btn">Llogarit pa TVSH</a>
                                </div>
                            {/if}
                            {if $display_notify_build_order}
                                <div class="control-group text-center w100">
                                    <div class="control-label"><h4 class="subheader">Build</h4></div>
                                    <button id="reserve_order" class="btn cm-ajax" name="dispatch[orders.notify_build_order]"
                                            style="">
                                        {__('notify_user')}
                                    </button>
                                </div>
                            {/if}

                            <div class="control-group">
                                <div class="control-label"><h4 class="subheader">{__("status")}</h4></div>
                                <div class="controls" style="margin-left: 145px;">
                                    {hook name="orders:order_status"}
                                    {if $order_info.status == $smarty.const.STATUS_INCOMPLETED_ORDER}
                                        {assign var="get_additional_statuses" value=true}
                                    {else}
                                        {assign var="get_additional_statuses" value=false}
                                    {/if}
                                    {assign var="order_status_descr" value=$smarty.const.STATUSES_ORDER|fn_get_simple_statuses:$get_additional_statuses:true}
                                    {assign var="extra_status" value=$config.current_url|escape:"url"}
                                    {if "MULTIVENDOR"|fn_allowed_for}
                                        {assign var="notify_vendor" value=true}
                                    {else}
                                        {assign var="notify_vendor" value=false}
                                    {/if}

                                    {$statuses = []}
                                    {assign var="order_statuses" value=$smarty.const.STATUSES_ORDER|fn_get_statuses:$statuses:$get_additional_statuses:true}
                                        {include file="common/select_popup.tpl" suffix="o" id=$order_info.order_id status=$order_info.status items_status=$order_status_descr update_controller="orders" notify=true send_invoice=true notify_department=true notify_vendor=$notify_vendor status_target_id="content_downloads" extra="&return_url=`$extra_status`" statuses=$order_statuses popup_additional_class="dropleft"}
                                    {/hook}
                                </div>
                            </div>

                            <div class="control-group">
                                <div class="control-label"><h4 class="subheader">{__("payment_status")}</h4></div>
                                <div class="controls" style="margin-left: 145px;">
                                    {assign var="payment_status_descr" value=$smarty.const.STATUSES_PAYMENT|fn_get_simple_statuses}
                                    {assign var="extra_status" value=$config.current_url|escape:"url"}
                                    {assign var="payment_statuses" value=$smarty.const.STATUSES_PAYMENT|fn_get_statuses}
                                    {include file="common/select_popup_general.tpl" suffix="p" id=$order_info.order_id status=$order_info.payment_status items_status=$payment_status_descr update_controller="orders.update_payment_status" status_target_id="content_downloads" extra="&return_url=`$extra_status`" statuses=$payment_statuses popup_additional_class="dropleft"}
                                </div>
                            </div>

                            <div class="control-group shift-top">
                                <div class="control-label">
                                    {include file="common/subheader.tpl" title=__("payment_information")}
                                </div>
                            </div>
                            {hook name="orders:payment_info"}
                                {* Payment info *}
                            {if $order_info.payment_id}
                                <div class="control-group">
                                    <div class="control-label">{__("method")}</div>
                                    <div id="tygh_payment_info" class="controls">{$order_info.payment_method.payment}
                                        &nbsp;{if $order_info.payment_method.description}({$order_info.payment_method.description}){/if}
                                    </div>
                                </div>
                                {if $order_info.payment_info}
                                    {foreach from=$order_info.payment_info item=item key=key}
                                        <div class="control-group">
                                            {if $item && $key != "expiry_year"}
                                                <div class="control-label">
                                                    {if $key == "card_number"}{assign var="cc_exists" value=true}{__("credit_card")}{elseif $key == "expiry_month"}{__("expiry_date")}{else}{__($key)}{/if}
                                                </div>
                                                <div class="controls">
                                                    {if $key == "order_status"}
                                                        {include file="common/status.tpl" status=$item display="view" status_type=""}
                                                    {elseif $key == "reason_text"}
                                                        {$item|nl2br}
                                                    {elseif $key == "expiry_month"}
                                                        {$item}/{$order_info.payment_info.expiry_year}
                                                    {elseif $key == "card_number" || $key == "cvv" || $key == "cvv2"}
                                                        <div class="wrap">{$item}</div>
                                                    {else}
                                                        {$item}
                                                    {/if}
                                                </div>
                                            {/if}
                                        </div>
                                    {/foreach}

                                    {if $cc_exists}
                                        <div class="control-group">
                                            <div class="control-label">
                                                <input type="hidden" name="order_ids[]" value="{$order_info.order_id}"/>
                                                {include file="buttons/button.tpl" but_text=__("remove_cc_info") but_meta="cm-ajax cm-comet" but_name="dispatch[orders.remove_cc_info]"}
                                            </div>
                                        </div>
                                    {/if}
                                {/if}
                            {/if}
                            {/hook}

                            {if $iute_status}
                                <div class="control-group shift-top">
                                    <div class="control-label">
                                        {__("iute_information")}
                                    </div>
                                    <div class="controls">{$iute_status|replace:'_':' '}</div>
                                </div>
                            {/if}

                            {* Shipping info *}
                            {hook name="orders:shipping_info"}
                            {if $order_info.shipping}
                                <div class="control-group shift-top">
                                    <div class="control-label">
                                        {include file="common/subheader.tpl" title=__("shipping_information")}
                                    </div>
                                </div>
                                {assign var="is_group_shippings" value=count($order_info.shipping)>1}

                                {foreach from=$order_info.shipping item="shipping" key="shipping_id" name="f_shipp"}
                                    <div class="control-group">
                                        <span> {$shipping.group_name|default:__("none")}</span>
                                    </div>
                                    <div class="control-group">
                                        <div class="control-label">{__("method")}</div>
                                        <div id="tygh_shipping_info" class="controls">
                                            {$shipping.shipping}
                                        </div>
                                    </div>
                                    {if $shipping.shipment_keys}
                                        {if $use_shipments}
                                            <p>
                                                <strong>{__("track_on_carrier_site")}</strong>
                                            </p>
                                        {/if}
                                        {foreach from=$shipping.shipment_keys item="shipment_key"}
                                            {$shipment = $shipments[$shipment_key]}
                                            {$carrier = ""}
                                            {if $shipment.tracking_number}
                                                <div class="control-group">
                                                    {if $shipment.carrier}
                                                        {$carrier = $shipment.carrier}
                                                    {/if}
                                                    <div class="control-label">
                                                        {if $carrier}
                                                            {__("carrier_`$carrier`")}
                                                        {else}
                                                            {__("tracking_number")}
                                                        {/if}
                                                    </div>
                                                    <div class="controls">
                                                        {if $use_shipments}
                                                            <a class="hand cm-tooltip icon-edit cm-combination tracking-number-edit-link"
                                                               title="{__("edit")}"
                                                               id="sw_tracking_number_{$shipment_key}"></a>
                                                        {/if}
                                                        {if $carrier}
                                                            <a href="{$shipment.tracking_url nofilter}" target="_blank"
                                                               id="on_tracking_number_{$shipment_key}">{if $shipment.tracking_number}{$shipment.tracking_number}{else}&mdash;{/if}</a>
                                                        {else}
                                                            <span id="on_tracking_number_{$shipment_key}">{$shipment.tracking_number}</span>
                                                        {/if}
                                                        <div class="hidden" id="tracking_number_{$shipment_key}">
                                                            <input class="input-small" type="text"
                                                                   name="update_shipping[{$shipping.group_key}][{$shipment.shipment_id}][tracking_number]"
                                                                   size="45" value="{$shipment.tracking_number}"/>
                                                            <input type="hidden"
                                                                   name="update_shipping[{$shipping.group_key}][{$shipment.shipment_id}][shipping_id]"
                                                                   value="{$shipping.shipping_id}"/>
                                                            <input type="hidden"
                                                                   name="update_shipping[{$shipping.group_key}][{$shipment.shipment_id}][carrier]"
                                                                   value="{$shipment.carrier}"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            {/if}
                                        {/foreach}
                                    {/if}

                                    {if $use_shipments}
                                        <div class="clearfix">
                                            {if $shipping.need_shipment}
                                                {if !"ULTIMATE:FREE"|fn_allowed_for}
                                                    {if "shipments.add"|fn_check_view_permissions}
                                                        <div class="pull-left">
                                                            {include file="common/popupbox.tpl" id="add_shipment_`$shipping.group_key`" content="" but_text=__("new_shipment") act="create" but_meta="btn"}
                                                        </div>
                                                    {/if}
                                                {else}
                                                    <div class="pull-left">
                                                        {include file="buttons/button.tpl" but_role="action" but_meta="cm-promo-popup" allow_href=false but_text=__("new_shipment")}
                                                    </div>
                                                {/if}
                                            {/if}
                                            {if !$is_group_shippings}
                                                <a class="pull-right"
                                                   href="{"shipments.manage?order_id=`$order_info.order_id`"|fn_url}">{__("shipments")}
                                                    &nbsp;({$order_info.shipment_ids|count})</a>
                                            {/if}
                                        </div>
                                        {if $is_group_shippings}
                                            <hr>
                                        {/if}
                                    {else}
                                        {if $shipments[$shipping.group_key].shipment_id}
                                            {$shipment_id = $shipments[$shipping.group_key].shipment_id}
                                        {else}
                                            {$shipment_id = 0}
                                        {/if}
                                        <div class="control-group">
                                            <label class="control-label" for="carrier_key">Totali i pagesës</label>
                                            <div class="controls">
                                                <input type="text" class="input-small" name="payment_total" value="{if $order_info.payment_total != -1}{$order_info.payment_total}{/if}">
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label" for="carrier_key">Rimburësim për porosinë</label>
                                            <div class="controls">
                                                <input type="text" class="input-small" name="refund" value="{$order_info.refund_id}">
                                            </div>
                                        </div>
                                        <div class="control-group text-center w100">
                                            <div class="control-label">Shuma dhuratë</div>
                                            <div class="controls">
                                                <input type="text" class="input-small" name="gift" value="{if $order_info.gift_amount != 0}{$order_info.gift_amount}{/if}">
                                            </div>
                                        </div>
                                        <div class="control-group text-center w100">
                                            <div class="control-label">Data e dërgesës (prej)</div>
                                            {include file="common/calendar.tpl" date_id="delivery_date_from" date_name="delivery_date_from" date_val=$delivery_dates.standard_timestamp|default:$smarty.const.TIME start_year=$settings.Company.company_start_year}
                                        </div>
                                        <div class="control-group text-center w100">
                                            <div class="control-label">Data e dërgesës (deri)</div>
                                            {include file="common/calendar.tpl" date_id="delivery_date_to" date_name="delivery_date_to" date_val=$delivery_dates.standard_range_timestamp|default:$smarty.const.TIME start_year=$settings.Company.company_start_year}
                                        </div>
{*                                        <div class="control-group">*}
{*                                            <label class="control-label"*}
{*                                                   for="tracking_number">{__("tracking_number")}</label>*}
{*                                            <div class="controls">*}
{*                                                <input id="tracking_number" class="input-small" type="text"*}
{*                                                       name="update_shipping[{$shipping.group_key}][{$shipment_id}][tracking_number]"*}
{*                                                       size="45"*}
{*                                                       value="{$shipments[$shipping.group_key].tracking_number}"/>*}
{*                                                <input type="hidden"*}
{*                                                       name="update_shipping[{$shipping.group_key}][{$shipment_id}][shipping_id]"*}
{*                                                       value="{$shipping.shipping_id}"/>*}
{*                                            </div>*}
{*                                        </div>*}
{*                                        <div class="control-group">*}
{*                                            <label class="control-label" for="carrier_key">{__("carrier")}</label>*}
{*                                            <div class="controls">*}
{*                                                {include file="common/carriers.tpl" id="carrier_key" meta="input-small" name="update_shipping[`$shipping.group_key`][`$shipment_id`][carrier]" carrier=$shipments[$shipping.group_key].carrier}*}
{*                                            </div>*}
{*                                        </div>*}
                                    {/if}
                                {/foreach}

                                {if $is_group_shippings}
                                    <div class="clearfix">
                                        <a class="pull-right"
                                           href="{"shipments.manage?order_id=`$order_info.order_id`"|fn_url}">{__("shipments")}
                                            &nbsp;({$order_info.shipment_ids|count})</a>
                                    </div>
                                {/if}
                            {else}

                                {foreach from=$order_info.product_groups item="group" key="group_id"}
                                    {if $group.all_free_shipping}

                                        {if $use_shipments}
                                            <div class="clearfix">
                                                {if $order_info.need_shipment}
                                                    {if !"ULTIMATE:FREE"|fn_allowed_for}
                                                        {if "shipments.add"|fn_check_view_permissions}
                                                            <div class="pull-left">
                                                                {include file="common/popupbox.tpl" id="add_shipment_0" content="" but_text=__("new_shipment") act="create" but_meta="btn"}
                                                            </div>
                                                        {/if}
                                                    {else}
                                                        <div class="pull-left">
                                                            {include file="buttons/button.tpl" but_role="action" but_meta="cm-promo-popup" allow_href=false but_text=__("new_shipment")}
                                                        </div>
                                                    {/if}
                                                {/if}

                                                <a class="pull-right"
                                                   href="{"shipments.manage?order_id=`$order_info.order_id`"|fn_url}">{__("shipments")}
                                                    &nbsp;({$order_info.shipment_ids|count})</a>
                                            </div>
                                        {/if}
                                    {/if}
                                {/foreach}
                            {/if}
                            {/hook}
                        </div>
                        {hook name="orders:customer_shot_info"}
                        {/hook}
                    </div>
                </div>
                <!--content_general--></div>
            <div id="content_addons">

                {hook name="orders:customer_info"}
                {/hook}

                <!--content_addons--></div>
            {if $downloads_exist}
                <div id="content_downloads">
                    <input type="hidden" name="order_id" value="{$smarty.request.order_id}"/>
                    <input type="hidden" name="order_status" value="{$order_info.status}"/>

                    {foreach from=$order_info.products item="oi"}
                        {if $oi.extra.is_edp == "Y"}
                            <p><a href="{"products.update?product_id=`$oi.product_id`"|fn_url}">{$oi.product}</a></p>
                            {if $oi.files}
                                <input type="hidden" name="files_exists[]" value="{$oi.product_id}"/>
                                <table cellpadding="5" cellspacing="0" border="0" class="table">
                                    <tr>
                                        <th>{__("filename")}</th>
                                        <th>{__("activation_mode")}</th>
                                        <th>{__("downloads_max_left")}</th>
                                        <th>{__("download_key_expiry")}</th>
                                        <th>{__("active")}</th>
                                    </tr>
                                    {foreach from=$oi.files item="file"}
                                        <tr>
                                            <td>{$file.file_name}</td>
                                            <td>
                                                {if $file.activation_type == "M"}{__("manually")}</label>{elseif $file.activation_type == "I"}{__("immediately")}{else}{__("after_full_payment")}{/if}
                                            </td>
                                            <td>{if $file.max_downloads}{$file.max_downloads} / <input type="text"
                                                                                                       name="edp_downloads[{$file.ekey}][{$file.file_id}]"
                                                                                                       value="{math equation="a-b" a=$file.max_downloads b=$file.downloads|default:0}"
                                                                                                       size="3" />{else}{__("none")}{/if}
                                            </td>
                                            <td>
                                                {if $oi.extra.unlimited_download == 'Y'}
                                                    {__("time_unlimited_download")}
                                                {elseif $file.ekey}
                                                    <p><label>{__("download_key_expiry")}
                                                            : </label><span>{$file.ttl|date_format:"`$settings.Appearance.date_format`, `$settings.Appearance.time_format`"|default:"n/a"}</span>
                                                    </p>
                                                    <p><label>{__("prolongate_download_key")}
                                                            : </label>{include file="common/calendar.tpl" date_id="prolongate_date_`$file.file_id`" date_name="prolongate_data[`$file.ekey`]" date_val=$file.ttl|default:$smarty.const.TIME start_year=$settings.Company.company_start_year}
                                                    </p>
                                                {else}{__("file_doesnt_have_key")}{/if}
                                            </td>
                                            <td>
                                                <select name="activate_files[{$oi.product_id}][{$file.file_id}]">
                                                    <option value="Y"
                                                            {if $file.active == "Y"}selected="selected"{/if}>{__("active")}</option>
                                                    <option value="N"
                                                            {if $file.active != "Y"}selected="selected"{/if}>{__("not_active")}</option>
                                                </select>
                                            </td>
                                        </tr>
                                    {/foreach}
                                </table>
                            {/if}
                        {/if}
                    {/foreach}
                    <!--content_downloads--></div>
            {/if}

            {if $order_info.promotions}
                <div id="content_promotions">
                    {include file="views/orders/components/promotions.tpl" promotions=$order_info.promotions}
                    <!--content_promotions--></div>
            {/if}

            {hook name="orders:tabs_content"}
            {/hook}

            {hook name="orders:tabs_extra"}
            {/hook}

        {/capture}
        {include file="common/tabsbox.tpl" content=$smarty.capture.tabsbox active_tab=$smarty.request.selected_section track=true}

    {/capture}
    {capture name="mainbox_title"}
        {__("order")} #{$order_info.order_id}
        <span class="f-middle">{__("total")}: <span>{include file="common/price.tpl" value=$order_info.total}</span>{if $order_info.company_id} / {$order_info.company_id|fn_get_company_name}{/if}</span>
        <span class="f-small">
    {if $status_settings.appearance_type == "I" && $order_info.doc_ids[$status_settings.appearance_type]}
        ({__("invoice")} #{$order_info.doc_ids[$status_settings.appearance_type]})
    {elseif $status_settings.appearance_type == "C" && $order_info.doc_ids[$status_settings.appearance_type]}
        ({__("credit_memo")} #{$order_info.doc_ids[$status_settings.appearance_type]})
    {/if}
            {assign var="timestamp" value=$order_info.timestamp|date_format:"`$settings.Appearance.date_format`"|escape:url}
            / {$order_info.timestamp|date_format:"`$settings.Appearance.date_format`"},{$order_info.timestamp|date_format:"`$settings.Appearance.time_format`"} /
    </span>
        {if $delivery_dates}
            <span class="f-small"
                  style="color: #6e6e6e;">Data e dërgesës: {$delivery_dates.standard|date_format:$settings.Appearance.date_format|fn_custom_date_format} - {$delivery_dates.standard_range|date_format:$settings.Appearance.date_format}</span>
        {/if}
    {/capture}

    {capture name="sidebar"}
        {* Issuer info *}
        {include file="views/order_management/components/issuer_info.tpl" user_data=$order_info.issuer_data}
        {* Customer info *}
        {include file="views/order_management/components/profiles_info.tpl" user_data=$order_info location="I"}
    {/capture}

    {capture name="buttons"}
        {include file="common/view_tools.tpl" url="orders.details?order_id="}

        {if $status_settings.appearance_type == "C" && $order_info.doc_ids[$status_settings.appearance_type]}
            {assign var="print_order" value=__("print_credit_memo")}
            {assign var="print_pdf_order" value=__("print_pdf_credit_memo")}
        {elseif $status_settings.appearance_type == "O"}
            {assign var="print_order" value=__("print_order_details")}
            {assign var="print_pdf_order" value=__("print_pdf_order_details")}
        {else}
            {assign var="print_order" value=__("print_invoice")}
            {assign var="print_pdf_order" value=__("print_pdf_invoice")}
        {/if}
        {capture name="tools_list"}
            {hook name="orders:details_tools"}
            {if $order_info.payment_id == 28}
                <li>{btn type="list" text='Shtyp faturën KosGiro' href="orders.print_kosgiro_invoice?order_id=`$order_info.order_id`" class="cm-new-window"}</li>
                <li>{btn type="list" text='Shtyp faturën KosGiro (pdf)' href="orders.print_kosgiro_invoice?order_id=`$order_info.order_id`&format=pdf"}</li>
            {/if}
                <li>{btn type="list" text=$print_order href="orders.print_invoice?order_id=`$order_info.order_id`" class="cm-new-window"}</li>
                <li>{btn type="list" text=$print_pdf_order href="orders.print_invoice?order_id=`$order_info.order_id`&format=pdf"}</li>
                <li>{btn type="list" text='Shtyp faturën e shkurtë' href="orders.print_short_invoice?order_id=`$order_info.order_id`" class="cm-new-window"}</li>
                <li>{btn type="list" text='Shtyp faturën e shkurtë (pdf)' href="orders.print_short_invoice?order_id=`$order_info.order_id`&format=pdf"}</li>
                <li>{btn type="list" text=__("print_packing_slip") href="orders.print_packing_slip?order_id=`$order_info.order_id`" class="cm-new-window"}</li>
                <li>{btn type="list" text=__("print_pdf_packing_slip") href="orders.print_packing_slip?order_id=`$order_info.order_id`&format=pdf" class="cm-new-window"}</li>
                <li>{btn type="list" text='Dërgo pro faturë në QuickBooks' href="orders.notify_pro_quickbooks?id=`$order_info.order_id`" class="cm-ajax cm-post"}</li>
                {if $order_info.status == 'K' || $al|fn_isal}
                    <li>{btn type="list" text='Dërgo faturë në QuickBooks' href="orders.notify_quickbooks?id=`$order_info.order_id`" class="cm-ajax cm-post"}</li>
                {/if}
                <li {if $order_info['disable_editing'] == '1'}style="pointer-events:none;opacity:0.6;"{/if}>{btn type="list" text=__("edit_order") href="order_management.edit?order_id=`$order_info.order_id`"}</li>
            {$smarty.capture.adv_tools nofilter}
            {/hook}
        {/capture}
        {dropdown content=$smarty.capture.tools_list}
        <div class="btn-group btn-hover dropleft">
            {include file="buttons/save_changes.tpl" but_meta="cm-no-ajax dropdown-toggle" but_role="submit-link" but_target_form="order_info_form" but_name="dispatch[orders.update_details]" save=true}
            <ul class="dropdown-menu">
                {$notify_customer_status = $order_info['notify_status']}
                {$notify_department_status = $order_info['notify_status']}
                {$notify_vendor_status = false}

                {hook name="orders:notify_checkboxes"}
                    <li><a><input type="checkbox" name="notify_user" id="notify_user" value="Y"/>
                            {__("notify_customer")}</a></li>
                    {*                {if $user_info.user_id|in_array:$user_restrictions}*}
                {if $usergroups.9}
                    <li><a><input type="checkbox" name="create_credit_note" id="create_credit_note" value="1"/>
                            Krijo Kredit Note</a></li>
                    <li><a><input type="checkbox" name="create_order_refund" id="create_order_refund" value="1"/>
                            Krijo Kthim të mjeteve</a></li>
                {/if}
                    <li><a><input type="checkbox" name="notify_department" id="notify_department" value="Y"/>
                            {__("notify_orders_department")}</a></li>
                    <li><a><input type="checkbox" name="gift_card" id="gift_card" value="N"
                                  onclick="{literal}if($(this).val() == 'N'){$(this).val('Y')}else{$(this).val('N')}{/literal}"/>
                            Dergo GiftCard</a></li>
                {if "MULTIVENDOR"|fn_allowed_for}
                    <li>
                        <a><input type="checkbox"
                                  name="notify_vendor" {if $notify_vendor_status == true} checked="checked" {/if}
                                  id="notify_vendor" value="Y"/>
                            {__("notify_vendor")}</a>
                    </li>
                {/if}
                {/hook}
            </ul>
        </div>
    {/capture}

    {include file="common/mainbox.tpl" title=$smarty.capture.mainbox_title content=$smarty.capture.mainbox buttons=$smarty.capture.buttons adv_buttons=$smarty.capture.adv_buttons sidebar=$smarty.capture.sidebar sidebar_position="left"}

</form>

{hook name="orders:detailed_after_content"}
{/hook}