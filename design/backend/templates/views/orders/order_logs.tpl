{capture name="mainbox"}

{capture name="sidebar"}
    <div class="sidebar-row">
        <form action="{""|fn_url}" method="GET">
            <input type="hidden" name="dispatch" value="orders.order_logs">
            <div class="form-control">
                <label>Porosia</label>
                <input type="text" name="order_id" placeholder="#">
            </div>
            <div class="form-control" style="float: right; margin-top: 20px;">
                <button class="btn">{__("search")}</button>
            </div>
            <div class="form-control" style="float: left; margin-top: 20px;">
                <button class="btn" onclick="resetSearch()">{__("reset")}</button>
            </div>
        </form>
    </div>
{/capture}

{include file="common/pagination.tpl"}

{if $logs}

<table class="table">
<thead>
    <tr>
        <th>{__("order")}</th>
        <th>{__("user")}</th>
        <th>{__("time")}</th>
        <th>Detajet</th>
    </tr>
</thead>
<tbody>
{foreach from=$logs item="log"}
<tr>
    <td><a href="{"orders.details&order_id=`$log.order_id`"|fn_url}">#{$log.order_id}</a></td>
    <td><a href="{"profiles.update&user_id=`$log.user_id`"|fn_url}">{$log.firstname} {$log.lastname}</a></td>
    <td>{$log.updated_at}</td>
    <td><a href="{"orders.order_log_details&log_id=`$log.log_id`"|fn_url}" target="_blank">Kliko</a></td>
</tr>
{/foreach}
</tbody>
</table>
{else}
    <p class="no-items">{__("no_data")}</p>
{/if}

{include file="common/pagination.tpl"}
{/capture}

{capture name="buttons"}
    {capture name="tools_list"}
        {hook name="logs:tools"}
        {/hook}
    {/capture}
    {dropdown content=$smarty.capture.tools_list}
{/capture}

{include file="common/mainbox.tpl" title="Order logs" content=$smarty.capture.mainbox buttons=$smarty.capture.buttons sidebar=$smarty.capture.sidebar}

<script>
    function resetSearch() {
        window.location.href = "{'orders.order_logs'|fn_url}";
    }
</script>