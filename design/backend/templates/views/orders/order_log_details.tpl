{capture name="mainbox"}
	{assign var="page_title" value="Order log details (#`$order_id`) (`$updated_at`)"}
	<table class="table table-middle">
		<thead>
			<tr class="cm-first-sibling">
				<th>Atributi</th>
				<th>Vlera paraprake</th>
				<th>Vlera e tanishme</th>
			</tr>
		</thead>
		<tbody>
			{foreach from=$order_edit_log item=log key=key}
			{if $log['attribute'] != 'Status'}
			<tr>
				{if $log.attribute|strstr:'fields'}
					<td>{$fields_map[$log.attribute]}</td>
				{else}
					<td>{$log.attribute}</td>
				{/if}

				{if $log.attribute|strstr:'fields' && !($log.attribute|strstr:'fields (56)' || $log.attribute|strstr:'fields (57)')}
					{foreach from=$profile_fields item=profile_field}
						{if $profile_field.object_id == $log.old_value}
							{assign var="old_value_field" value=$profile_field.description}
						{elseif $profile_field.object_id == $log.new_value}
							{assign var="new_value_field" value=$profile_field.description}
						{/if}
					{/foreach}
					<td>{$old_value_field}</td>
					<td>{$new_value_field}</td>
				{else}
					<td>{$log.old_value}</td>
					<td>{$log.new_value}</td>
				{/if}
			</tr>
			{/if}
			{/foreach}
		</tbody>
	</table>
{/capture}
{capture name="buttons"}
	{capture name="tools_list"}
	{/capture}
	{dropdown content=$smarty.capture.tools_list}
{/capture}
{include file="common/mainbox.tpl" title=$page_title content=$smarty.capture.mainbox  buttons=$smarty.capture.buttons sidebar=$smarty.capture.sidebar adv_buttons=$smarty.capture.adv_buttons select_languages=true}