{capture name="mainbox"}
    {assign var="is_al" value=$al|fn_isAL}
    {capture name="tabsbox"}
        <form id='form' action="{""|fn_url}" method="post" name="sections_update_form" class="form-horizontal form-edit  cm-disable-empty-files" enctype="multipart/form-data">

            <h4 class="subheader   hand" data-toggle="collapse" data-target="#sec_1">
                Seksioni 1
                <span class="exicon-collapse"></span></h4>
            <div id="sec_1" class="collapse in">
                <div class="control-group">
                    <label for="product_description_product" class="control-label cm-required">Kategoria</label>
                    <div class="controls">
                        {include file="pickers/categories/picker.tpl" hide_input=$product_data.shared_product company_ids=$product_data.company_id rnd=$rnd data_id="" input_name="sections[1][id]" radio_input_name="" main_category=$product_data.main_category item_ids=$section_1_data.section_id|default:$request_category_id hide_link=true hide_delete_button=true display_input_id="" disable_no_item_text=true view_mode="list" but_meta="btn" show_active_path=true}
                    </div>
                </div>
                <div class="control-group">
                    <label for="product_description_product" class="control-label cm-required">Produktet</label>
                    <div class="controls">
                        {include file="pickers/products/picker.tpl" input_name="sections[1][products]" type="links" item_ids=$section_1_data.section_products}
                    </div>
                </div>
{*                <div class="control-group">*}
{*                    <label for="product_description_product" class="control-label cm-required">Produktet</label>*}
{*                    <div class="controls">*}
{*                        <input type="text" name="sections[1][products]" value="{$section_1_data.section_products}">*}
{*                    </div>*}
{*                </div>*}
                <div class="control-group">
                    <label for="product_description_product" class="control-label cm-required">Titulli</label>
                    <div class="controls">
                        <input type="text" name="sections[1][title]" value="{$section_1_data.section_title}">
                    </div>
                </div>
                <div class="control-group">
                    <label for="product_description_product" class="control-label cm-required">Pershkrimi</label>
                    <div class="controls">
                        <textarea name="sections[1][description]" id="" cols="30" rows="2">{$section_1_data.section_description}</textarea>
                    </div>
                </div>
            </div>

            <h4 class="subheader   hand" data-toggle="collapse" data-target="#sec_2">
                Seksioni 2
                <span class="exicon-collapse"></span></h4>
            <div id="sec_2" class="collapse in">
                <div class="control-group">
                    <label for="product_description_product" class="control-label cm-required">Kategoria</label>
                    <div class="controls">
                        {include file="pickers/categories/picker.tpl" hide_input=$product_data.shared_product company_ids=$product_data.company_id rnd=$rnd data_id="" input_name="sections[2][id]" radio_input_name="" main_category=$product_data.main_category item_ids=$section_2_data.section_id|default:$request_category_id hide_link=true hide_delete_button=true display_input_id="" disable_no_item_text=true view_mode="list" but_meta="btn" show_active_path=true}
                    </div>
                </div>
                <div class="control-group">
                    <label for="product_description_product" class="control-label cm-required">Titulli</label>
                    <div class="controls">
                        <input type="text" name="sections[2][title]" value="{$section_2_data.section_title}">
                    </div>
                </div>
                <div class="control-group">
                    <label for="product_description_product" class="control-label cm-required">Pershkrimi</label>
                    <div class="controls">
                        <textarea name="sections[2][description]" id="" cols="30" rows="2">{$section_2_data.section_description}</textarea>
                    </div>
                </div>
            </div>

            <h4 class="subheader   hand" data-toggle="collapse" data-target="#sec_3">
                Seksioni 3
                <span class="exicon-collapse"></span></h4>
            <div id="sec_3" class="collapse in">
                <div class="control-group">
                <label for="product_description_product" class="control-label cm-required">Kategoria</label>
                <div class="controls">
                    {include file="pickers/categories/picker.tpl" hide_input=$product_data.shared_product company_ids=$product_data.company_id rnd=$rnd data_id="" input_name="sections[3][id]" radio_input_name="" main_category=$product_data.main_category item_ids=$section_3_data.section_id|default:$request_category_id hide_link=true hide_delete_button=true display_input_id="" disable_no_item_text=true view_mode="list" but_meta="btn" show_active_path=true}
                </div>
            </div>
                <div class="control-group">
                <label for="product_description_product" class="control-label cm-required">Nenkategorite</label>
                <div class="controls">
                    {include file="pickers/categories/picker.tpl" hide_input=$product_data.shared_product company_ids=$product_data.company_id rnd=$rnd data_id="" input_name="sections[3][sub]" radio_input_name="" main_category=$product_data.main_category item_ids=$section_3_data.section_sub|default:$request_category_id hide_link=true hide_delete_button=true display_input_id="" disable_no_item_text=true view_mode="list" but_meta="btn" show_active_path=true}
                </div>
            </div>
            </div>

            <h4 class="subheader   hand" data-toggle="collapse" data-target="#sec_4">
                Seksioni 4
                <span class="exicon-collapse"></span></h4>
            <div id="sec_4" class="collapse in">
                <div class="control-group">
                    <label for="product_description_product" class="control-label cm-required">Kategorite</label>
                    <div class="controls">
                        {include file="pickers/categories/picker.tpl" hide_input=$product_data.shared_product company_ids=$product_data.company_id rnd=$rnd data_id="" input_name="sections[4][id]" radio_input_name="" main_category=$product_data.main_category item_ids=$section_4_data.section_id|default:$request_category_id hide_link=true hide_delete_button=true display_input_id="" disable_no_item_text=true view_mode="list" but_meta="btn" show_active_path=true}
                    </div>
                </div>
            </div>

            {** Form submit section **}
            {capture name="buttons"}
{*                {include file="common/view_tools.tpl" url="products.update?product_id="}*}

{*                {if $id}*}
{*                    {capture name="tools_list"}*}
{*                        {hook name="products:update_tools_list"}*}
{*                        {if $view_uri}*}
{*                            <li>{btn type="list" target="_blank" text=__("preview") href=$view_uri}</li>*}
{*                            <li class="divider"></li>*}
{*                        {/if}*}
{*                            <li>{btn type="list" text=__("clone") href="products.clone?product_id=`$id`" method="POST"}</li>*}
{*                        {if $allow_save}*}
{*                            <li>{btn type="list" text=__("delete") class="cm-confirm" href="products.delete?product_id=`$id`" method="POST"}</li>*}
{*                        {/if}*}
{*                        {/hook}*}
{*                    {/capture}*}
{*                    {dropdown content=$smarty.capture.tools_list}*}
{*                {/if}*}
                {include file="buttons/save.tpl" but_role="submit-link" but_name="dispatch[sections.update]" but_target_form="sections_update_form" save=$id}
{*                {include file="common/tools.tpl" tool_href="products.clear_cache&product_id={$product_data.product_id}" prefix="top" hide_tools="true" title="Clear cache" icon="icon-refresh"}*}

            {/capture}
            {** /Form submit section **}

        </form> {* /product update form *}

    {/capture}
    {include file="common/tabsbox.tpl" content=$smarty.capture.tabsbox group_name=$runtime.controller active_tab=$smarty.request.selected_section track=true}

{/capture}

{include file="common/mainbox.tpl"
title="Sections"
content=$smarty.capture.mainbox
select_languages=$id
buttons=$smarty.capture.buttons
adv_buttons=$smarty.capture.adv_buttons
}