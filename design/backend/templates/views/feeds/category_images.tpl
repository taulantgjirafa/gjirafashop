{capture name="mainbox"}

    {if $category_images != ""}
        <form action="{""|fn_url}" method="POST" >
            <input type="hidden" name="dispatch" value="feeds.category_images">
            <table class="table table-bordered table-condensed table-striped">
                <thead>
                <th>Subcategory id</th>
                <th>Url</th>
                <th></th>
                </thead>
                {assign "i" 1}
                {foreach $category_images as $line}
                    <tr data-row="{$i}">
                            <td data-col="{$i}"><input type="text" value="{$line.category_id}" name="data[{$line.category_id}][category_id]"/></td>
                        <td data-col="{$i}"><input type="text" value="{$line.url}" name="data[{$line.category_id}][url]"/></td>
                        <td>
                            <button onclick="$(this).parent().parent().remove();" class="btn btn-danger remove-button">Remove</button>
                        </td>
                    </tr>
                    {$i = $i +1}
                {/foreach}
            </table>
            <button class="btn btn-default btn-block add-more" type="button" style="margin:0;">+ SHTO</button>
            <br><br>
            <input type="submit" class="btn btn-primary btn-block" value="RUAJ"/>
        </form>
    {else}
        <p class="no-items">Zgjedh file për editim në anën e djathtë!</p>
    {/if}
{/capture}

{include file="common/mainbox.tpl" title="Subcategory Images" content=$smarty.capture.mainbox  buttons=$smarty.capture.buttons sidebar=$smarty.capture.sidebar adv_buttons=$smarty.capture.adv_buttons select_languages=true}

<script>
    {literal}
    $('.add-more').click(function () {
        var $tr = $('.table tr').last().clone();
        $('.table > tbody').append($tr);
        var appended_tr = $('table tr').last();
        var last_column = appended_tr.find('td').last().prev().data('col');
        appended_tr.attr('data-row', appended_tr.data('row') + 1);
        var i = 1;
        appended_tr.find('td').each(function () {
            $(this).find('input').val('').attr('name', 'data[' + (appended_tr.data('row') + 1) + '][' + (parseInt(last_column) + i) + ']');
            $(this).attr('data-row', (appended_tr.data('row') + 1));
            i++;
        });
    });
    {/literal}
</script>