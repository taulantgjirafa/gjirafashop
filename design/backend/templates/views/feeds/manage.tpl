{capture name="mainbox"}

    {capture name="sidebar"}
        <div class="sidebar-row" id="categories_stats">
            <h6>All feeds</h6>
            <ul class="unstyled sidebar-stat">
                {foreach $csv_list as $file}
                    <li><a href="{"feeds.manage"|fn_url}&csv_file={$file}">{$file|replace:"feeds/":""}</a></li>
                {/foreach}
            </ul>
        </div>
    {/capture}
    {if $csv_name != ""}
        <form action="{""|fn_url}" method="post">
            <input type="hidden" name="file_name" value="{$csv_name}">
            <input type="hidden" name="dispatch" value="feeds.manage">
            <table class="table table-bordered table-condensed table-striped">
                {assign "i" 0}
                {assign "j" 0}
                {foreach $csv as $line}
                    <tr data-row="{$i}">
                        {foreach $line as $column}
                            <td data-col="{$j}"><input type="text" value="{$column}" name="data[{$i}][{$j}]"/></td>
                            {$j = $j +1}
                        {/foreach}
                        <td>
                            <button onclick="$(this).parent().parent().remove();"
                                    class="btn btn-danger remove-button">Remove
                            </button>
                        </td>
                    </tr>
                    {$i = $i +1}
                {/foreach}
            </table>
            <button class="btn btn-default btn-block add-more" type="button" style="margin:0;">+ SHTO</button>
            <br><br>
            <input type="submit" class="btn btn-primary btn-block" value="RUAJ"/>
        </form>
        {else}
        <p class="no-items">Zgjedh file për editim në anën e djathtë!</p>
    {/if}
{/capture}

{include file="common/mainbox.tpl" title="Feeds" content=$smarty.capture.mainbox  buttons=$smarty.capture.buttons sidebar=$smarty.capture.sidebar adv_buttons=$smarty.capture.adv_buttons select_languages=true}

<script>
    {literal}
    $('.add-more').click(function () {
        var $tr = $('.table tr').last().clone();
        $('.table > tbody').append($tr);
        var appended_tr = $('table tr').last();
        var last_column = appended_tr.find('td').last().prev().data('col');
        appended_tr.attr('data-row', appended_tr.data('row') + 1);
        var i = 1;
        appended_tr.find('td').each(function () {
            $(this).find('input').val('').attr('name', 'data[' + (appended_tr.data('row') + 1) + '][' + (parseInt(last_column) + i) + ']');
            $(this).attr('data-row', (appended_tr.data('row') + 1));
            i++;
        });
    });
    {/literal}
</script>