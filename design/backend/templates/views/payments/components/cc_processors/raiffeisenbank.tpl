<div class="control-group">
    <label class="control-label" for="merchant_id">Merchant ID:</label>
    <div class="controls">
        <input type="text" name="payment_data[processor_params][merchant_id]" id="merchant_id"
               value="{$processor_params.merchant_id}" size="60">
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="terminal_id">Terminal ID:</label>
    <div class="controls">
        <input type="text" name="payment_data[processor_params][terminal_id]" id="terminal_id"
               value="{$processor_params.terminal_id}" size="60">
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="ew_include_cvn">Signature:</label>
    <div class="controls"><input type="hidden" name="payment_data[processor_params][signature]" value="false">
        <input type="text" name="payment_data[processor_params][signature]" id="signature"
               value="{$processor_params.signature}">
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="currency_id">Currency ID:</label>
    <div class="controls">
        <select name="payment_data[processor_params][currency_id]" id="currency_id">
            <option value="978" {if $processor_params.currency_id == "978"}selected="selected"{/if}>Euro</option>
            <option value="008" {if $processor_params.currency_id == "008"}selected="selected"{/if}>Lek</option>
        </select>
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="terminal_url">Terminal URL:</label>
    <div class="controls">
        <input type="text" name="payment_data[processor_params][terminal_url]" id="terminal_url" value="{$processor_params.terminal_url}">
    </div>
</div>