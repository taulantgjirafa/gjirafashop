{strip}
    {*{assign var="container" value=intval($product.product_id/8000+1)}*}
    {*{if $container gt 5}*}
        {*{$container=5}*}
    {*{/if}*}

{assign var="image_data" value=$image|fn_image_to_display:$image_width:$image_height}

{if $image || $href}
    {if $smarty.get.dispatch == 'products.manage'}
        <a href="{"products.update&product_id={$image}"|fn_url}" {if !$href}target="_blank"{/if}>
        {else}
    <a href="https://hhstsyoejx.gjirafa.net/gj50/banners/{$image_name}" {if !$href}target="_blank"{/if}>
        {/if}
{/if}
{if $image_data.image_path}
    <img {if $image_id}id="image_{$image_id}"{/if} data-src="{$image_data.image_path}" width="{$image_data.width}" height="{$image_data.height}" alt="{$image_data.alt}" {if $image_data.generate_image}class="spinner lazyload" data-ca-image-path="{$image_data.image_path}"{/if} title="{$image_data.alt}" />
{else}
    {if $smarty.get.dispatch == 'products.manage'}
        {assign var="azure_path" value={get_images_from_blob product_id=$product.product_id count=1 type="large"}}
        <img {if $image_id}id="image_{$image_id}"{/if} class="lazy-load__img" data-src="{$azure_path}" />
        {else}
    <img {if $image_id}id="image_{$image_id}"{/if} class="lazy-load__img" data-src="https://hhstsyoejx.gjirafa.net/gj50/banners/{$image_name}" />
    {/if}
        {*<div class="no-image" style="width: {$image_width|default:$image_height}px; height: {$image_height|default:$image_width}px;"><i class="glyph-image" title="{__("no_image")}"></i></div>*}
{/if}
{if $image || $href}</a>{/if}

{/strip}