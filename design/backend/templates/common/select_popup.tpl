{$notify_customer_status = true}
{$notify_department_status = true}
{$notify_vendor_status = true}

{if $non_editable || $display == "text"}
    <span class="view-status">
        {if $status == "A"}
            {__("active")}
        {elseif $status == "H"}
            {__("hidden")}
        {elseif $status == "D"}
            {__("disabled")}
        {elseif $status == "P"}
            {__("pending")}
        {elseif $status == "N"}
            {__("new")}
        {/if}
    </span>
{else}
    {assign var="prefix" value=$prefix|default:"select"}
    {assign var="btn_meta" value=$btn_meta|default:"btn-text"}
    {assign var="popup_additional_class" value=$popup_additional_class}
    <div class="cm-popup-box dropleft {if !$hide_for_vendor}dropdown{/if} {$popup_additional_class}">
        {if !$hide_for_vendor}
        <a {if $id}id="sw_{$prefix}_{$id}_wrap"{/if}  class="{if $btn_meta}{$btn_meta}{/if}  btn dropdown-toggle{if $id} cm-combination{/if}" data-toggle="dropdown">
            {/if}

            {if $items_status}
                {$items_status.$status|default:$default_status_text}
            {else}
                {if $status == "A"}
                    {__("active")}
                {elseif $status == "D"}
                    {__("disabled")}
                {elseif $status == "H"}
                    {__("hidden")}
                {elseif $status == "P"}
                    {__("pending")}
                {elseif $status == "N"}
                    {__("new")}
                {/if}
            {/if}
            {if !$hide_for_vendor}
            <span class="caret"></span>
        </a>
        {/if}
        {if $id && !$hide_for_vendor}
            {assign var="_update_controller" value=$update_controller|default:"tools"}
            {if $table && $object_id_name}{capture name="_extra"}&table={$table}&id_name={$object_id_name}{/capture}{/if}
            <ul class="dropdown-menu">

                {if !$items_status}
                    {assign var="items_status" value=$status|fn_get_default_statuses:$hidden}
                    {assign var="extra_params" value="&table=`$table`&id_name=`$object_id_name`"}
                {else}
                    {assign var="extra_params" value="`$smarty.capture._extra``$extra`"}
                {/if}
                {if $st_return_url}
                    {$return_url = $st_return_url|escape:url}
                    {$extra_params = "`$extra_params`&redirect_url=`$return_url`"}
                {/if}
                {if $items_status}
                    {foreach from=$items_status item="val" key="st"}
                        {$disabled = ($st == 'K' || $st == 'H') || ($status == 'K' && ($st != 'L' && $st != 'E' && $st != 'D' && $st != 'B')) || ($status == 'E' && ($st == 'L' || $st == 3))}
                        {$restricted = ($status == 'L' || $status == 'H' || $status == 3) && ($st == 'E')}
                        {$other = $status != 'E' && $st == 'C'}

                        {if $al|fn_isAL || $auth.user_id == 1 || $status == '1'}
                            {$disabled = false}
                            {$restricted = false}
                            {$other = false}
                        {/if}

                        <li {if ($status == $st || $disabled) && !$restricted || $other}class="disabled"{/if}><a class="{if $confirm}cm-confirm {/if}status-link-{$st|lower} {if ($status == $st || $disabled) && !$restricted || $other}active{else}cm-ajax cm-post{if $ajax_full_render} cm-ajax-full-render{/if}{/if} {if $status_meta}{$status_meta}{/if}"{if $status_target_id} data-ca-target-id="{$status_target_id}"{/if} href="{"`$_update_controller`.update_status?id=`$id`&status=`$st``$extra_params``$dynamic_object`"|fn_url}" onclick="return fn_check_object_status(this, '{$st|lower}', '{if $statuses}{$statuses[$st].params.color|default:''}{/if}') ,    fn_get_status('{$st|lower}');"   {if $st_result_ids}data-ca-target-id="{$st_result_ids}"{/if} data-ca-event="ce.update_object_status_callback" {if ($status == $st || $disabled) && !$restricted || $other}{* style="pointer-events: none;" *}{/if}>{$val}</a></li>
                    {/foreach}
                {/if}

                {capture name="list_items"}
                    {hook name="select_popup:notify_checkboxes"}
                    {assign var="notify_status" value="{$order_info['notify_status']}"}

                    {if $notify}
                        <li class="divider"></li>

                        {*{if $notify_customer_status == true} checked="checked" {/if}*}
                        <li><a><label for="{$prefix}_{$id}_notify">
                                    <input type="checkbox" class="notify_user_drop" {if $notify_status}checked{/if} name="__notify_user" id="{$prefix}_{$id}_notify" value="N"  onclick="Tygh.$('input[name=__notify_user]').prop('checked', this.checked);" />
                                    {$notify_text|default:__("notify_customer")}</label></a>
                        </li>
                        <li><a><label for="{$prefix}_{$id}_pro_notify_quickbooks">
                                    <input type="checkbox" class="notify_user_drop" name="__notify_pro_quickbooks" id="{$prefix}_{$id}_notify" value="N"  onclick="Tygh.$('input[name=__notify_pro_quickbooks]').prop('checked', this.checked);" />
                                    Dergo pro fature ne Quickbooks</label></a>
                        </li>
                        <li><a><label for="{$prefix}_{$id}_pro_notify_user_with_sms">
                                    <input type="checkbox" class="notify_user_drop" name="__notify_user_with_sms" id="{$prefix}_{$id}_pro_notify_user_with_sms" value="N"  onclick="Tygh.$('input[name=__notify_user_with_sms]').prop('checked', this.checked);" />
                                    Njofto përdoruesin me SMS</label></a>
                        </li>
                    {/if}
                    {if ($send_invoice && $status == 'K') || $al|fn_isal}
                        <li><a><label for="{$prefix}_{$id}_notify_quickbooks">
                                    <input type="checkbox" name="__notify_quickbooks" id="{$prefix}_{$id}_notify_quickbooks" value="N" onclick="Tygh.$('input[name=__notify_quickbooks]').prop('checked', this.checked);" /> Dergo faturen ne Quickbooks</label></a>
                        </li>
                    {/if}
                    {if $notify_department}
                        {*{if $notify_department_status == true} checked="checked" {/if}*}
                        <li><a><label for="{$prefix}_{$id}_notify_department">
                                    <input type="checkbox" name="__notify_department" checked id="{$prefix}_{$id}_notify_department" value="N"  onclick="Tygh.$('input[name=__notify_department]').prop('checked', this.checked);" />
                                    {__("notify_orders_department")}</label></a>
                        </li>
                    {/if}
                    {if "MULTIVENDOR"|fn_allowed_for && $notify_vendor}
                        {*{if $notify_vendor_status == true} checked="checked" {/if}*}
                        <li><a><label for="{$prefix}_{$id}_notify_vendor">
                                    <input type="checkbox" name="__notify_vendor" id="{$prefix}_{$id}_notify_vendor" value="N"  onclick="Tygh.$('input[name=__notify_vendor]').prop('checked', this.checked);" />
                                    {__("notify_vendor")}</label></a>
                        </li>
                    {/if}
                    {/hook}

                {/capture}

                {if $smarty.capture.list_items|trim}
                    {$smarty.capture.list_items nofilter}
                {/if}
            </ul>
            {if !$smarty.capture.avail_box}
                {script src="js/tygh/select_popup.js"}
                {capture name="avail_box"}Y{/capture}
            {/if}
        {/if}
    </div>
{/if}
{literal}
    <script>
        function fn_get_status(status){
            var notify_statuses = [ "c", "b" , "i" ,"e","j","r","v"];
            if (notify_statuses.indexOf(status) > -1) {
                $('.notify_user_drop').prop('checked', true);
            }
            else{
                $('.notify_user_drop').prop('checked', false);
            }
        }

        function send_order_status_analytics(order_id){
            return;
            // Refund an entire transaction.
            ga('ec:setAction', 'refund', {
                'id': order_id    // Transaction ID is only required field for full refund.
            });
        }
    </script>
   {/literal}