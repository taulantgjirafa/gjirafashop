<!DOCTYPE html>
<html dir="{$language_direction}">
<head>
{literal}
<style type="text/css" media="print">
.main-table {
    background-color: #ffffff !important;
}
.main-table td {
    vertical-align: top;
}
</style>
<style type="text/css" media="screen,print">
    .xdebug-error{
        display: none !important;
        visibility: hidden !important;
    }
body,p,div,td {
    color: #000000;
    font: 10.5px Arial;
}
body {
    padding: 0;
    margin: 0;
}
p, li {
    line-height: 1.3;
}
h2 {
    font-size: 1.3em;
    font-weight: bolder;
}
a, a:link, a:visited, a:hover, a:active {
    color: #000000;
    text-decoration: underline;
}
a:hover {
    text-decoration: none;
}
    .container{
        width:1000px;
        margin:0 auto;
        display: block;
        padding:20px;
        max-width:90%;
    }
    .text-center{
        text-align: center;
    }
    .half-container{
        width:500px;
        border:1px solid #000;
        display: inline-block;
        box-sizing: border-box;
        margin-top: 15px;
        vertical-align: top;
        min-height:150px;
        max-width:50%;
    }
.half-container:nth-child(3){
    border-left:0;
}
.half-container h3 {
    margin: 0;
    border-bottom: 1px solid #000;
    padding: 10px;
}
.half-container p {
    margin: 0;
    padding: 10px;
    min-height: 60px;
}
    .half-container.n{
        min-height: 125px;
    }
    .half-container.n p{
        min-height: 20px;
    }
    .text-bold{
        font-weight:bolder;
    }
</style>
{/literal}
</head>
<body>
    <div class="container">
        <div style="width: 602px; margin: 0 auto; padding: 100px 0 25px 0;">
        {$extra nofilter}
        </div>
    </div>
</body>
</html>