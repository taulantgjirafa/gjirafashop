{* {assign "is_al" $al|fn_isAL} *}

<style>
    * {
        font-size: 22px;
    }

    .text-left {
        text-align: left;
    }
    .text-center{
        text-align:center;
    }

    .text-right {
        text-align: right;
    }
    body, html{
        margin:0 !important;
        padding:0 !important;
    }
    .main-div{
        margin:0 20px;
        font-size:24px;
        line-height:1.4;
        box-sizing:border-box;
        margin-bottom:0;
        font-family:sans-serif;
        bo
    }
    .smtitle{
        color:#fff;
        background:#000;
        width:100%;
        padding:0 5px;
        font-weight:600;
        margin-bottom:5px;
    }
    .half-flet {
        width: 45%;
        display: inline-block;
        text-align:left;
    }
    .boxes{
        margin:15px 0;
        padding:10px 0;
    }
    .f-flet{
        margin-right:2%;
        font-weight:700;
    }
    .shenime{
        border:1px solid #000;
        margin-top:5px;
        font-style:italic;
        padding:10px;
        font-size:20px;
    }
    .image{
        margin:10px 0;
    }
    .image img{
        width:80%;
        height:70px;
        display:block;
        margin:0 auto;
    }
    .ids{
        border-top:2px solid #000;
        border-bottom:2px solid #000;
    }
    .total-pay{
        border-top:2px solid #000;
        margin-top:10px;
        padding-top:10px;
    }
    h1,h2,h3,h4,h5,h6{
        margin:0;
    }
    .bold {
        font-weight: bold;
    }
    .table-container {
        display: inline-flex;
        width: 100%;
    }
</style>

<div class="main-div">
    <div class="send boxes">
        <div class="smtitle" style="text-align: center;">{$order_info.display_order_tag|capitalize}{if $package_size != false} - {$package_size}{/if}</div>
        <div class="smtitle">Dërgesa</div>
        <table width="100%" style="border-collapse: collapse;">
            <tbody>
                <tr>
                    <td width="40%" class="bold">Emri:</td>
                    <td width="60%">{$order_info.s_firstname}</td>
                </tr>
                <tr>
                    <td width="40%" class="bold">Mbiemri:</td>
                    <td width="60%">{$order_info.s_lastname}</td>
                </tr>
                <tr>
                    <td width="40%" valign="top" class="bold">Adresa:</td>
                    <td width="60%">{$order_info.s_address}{if $order_info.s_address_2}, {$order_info.s_address_2}{/if}</td>
                </tr>
                <tr>
                    <td width="40%" class="bold">Qyteti:</td>
                    <td width="60%">{$city}</td>
                </tr>
                <tr>
                    <td width="40%" class="bold">Shteti:</td>
                    <td width="60%">{$country}</td>
                </tr>
                <tr>
                    <td width="40%" valign="top" class="bold">Telefoni:</td>
                    <td width="60%">{$order_info.s_phone}{if $order_info.fields.56}, {$order_info.fields.56}{/if}</td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="porosia boxes">
        <div class="smtitle">Porosia</div>
        <table width="100%" style="border-collapse: collapse;">
            <tbody>
                <tr>
                    <td width="40%" class="bold">Data e porosisë:</td>
                    <td width="60%">{$order_info.timestamp|date_format:"`$settings.Appearance.date_format`"|replace:'/':'.'}</td>
                </tr>
                <tr>
                    <td width="40%" class="bold">Data e dërgesës:</td>
                    <td width="60%">{$order_info.delivery_dates.date_from|date_format:"`$settings.Appearance.date_format`"|replace:'/':'.'}{if $order_info.delivery_dates.date_from != $order_info.delivery_dates.date_to} - {$order_info.delivery_dates.date_to|date_format:"`$settings.Appearance.date_format`"|replace:'/':'.'}{/if}</td>
                </tr>
                <tr>
                    <td width="40%" valign="top" class="bold">Mënyra e pagesës:</td>
                    <td width="60%">{$order_info.payment_method.payment}</td>
                </tr>
                <tr>
                    <td width="40%" valign="top" class="bold">Mënyra e transportit:</td>
                    <td width="60%">{$order_info.shipping[0].shipping}</td>
                </tr>
            </tbody>
        </table>
            <div>
            <div class="bold">Shënime:</div>
                {if $order_info.notes}
                <div class="shenime">{$order_info.notes}</div>
                {/if}
            </div>
        </div>
    <div class="ids boxes">
        <h5 class="text-center">ID e porosisë</h5>
        <div class="image">                                
            <img src="https://gj.al/ImageGenerator?text={$order_info.order_id}&key=hug897r8w3-0rnjmwfoslfklsehf0834wy0r9iw=-dalkidja90d-2d64wa32wjnczz" />               
        </div>
        <h3 class="text-center">#{$order_info.order_id}</h3>
    </div>
    <div>
        <table width="40%" style="border-collapse: collapse; float: left;">
            {if $order_info.company_id == 19}
                <tbody>
                    <tr>
                        <td class="bold">Gjirafa50.al</td>
                    </tr>
                    <tr>
                        <td>Tirane, Vore, Tirana Business Park, Rruga Rinas</td>
                    </tr>
                    <tr>
                        <td>Tirana, Shqipëri</td>
                    </tr>
                    <tr>
                        <td>Tel: +355688030303</td>
                    </tr>
                </tbody>
            {else}
                <tbody>
                    <tr>
                        <td class="bold">Gjirafa50.com</td>
                    </tr>
                    <tr>
                        <td>Magjistralja Prishtinë-Ferizaj, Kilometri i 6 (Lapnasellë)</td>
                    </tr>
                    <tr>
                        <td>Prishtinë, 10000</td>
                    </tr>
                    <tr>
                        <td>Kosovë</td>
                    </tr>
                    <tr>
                        <td>Tel: +38345101953</td>
                    </tr>
                </tbody>
            {/if}
        </table>
        <table width="60%" style="border-collapse: collapse; float: left;">
            <tbody>
                <tr>
                    <td class="bold">Nëntotali:</td>
                    <td>{if $is_al}{$order_info.subtotal|number_format} Lekë{else}{number_format((float)$order_info.subtotal, 2, '.', '')} €{/if}</td>
                </tr>
                <tr>
                    <td class="bold">Flex:</td>
                    <td>{if $is_al}{$order_info.gjflex_total|number_format} Lekë{else}{number_format((float)$order_info.gjflex_total, 2, '.', '')} €{/if}</td>
                </tr>
                <tr>
                    <td class="bold">Transporti:</td>
                    <td>{if $is_al}{$order_info.shipping_cost|number_format} Lekë{else}{$order_info.shipping_cost} €{/if}</td>
                </tr>
                <tr style="font-size: 26px;">
                    <td class="bold" style="border-top: 2px solid black;">Totali i porosisë:</td>
                    <td class="bold" style="border-top: 2px solid black;">{if $is_al}{$order_info.total|number_format} Lekë{else}{$order_info.total} €{/if}</td>
                </tr>
                {if $order_info.payment_total != -1}
                    <tr style="font-size: 26px;">
                        <td class="bold">Totali i pagesës:</td>
                        <td class="bold">{if $is_al}{$order_info.payment_total|number_format} Lekë{else}{$order_info.payment_total} €{/if}</td>
                    </tr>
                {/if}
            </tbody>
        </table>
    </div>
</div>