<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml"
      xmlns:o="urn:schemas-microsoft-com:office:office">
<head><title></title>  <!--[if !mso]><!-- -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">  <!--<![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style type="text/css">  #outlook a {
            padding: 0;
        }

        .ReadMsgBody {
            width: 100%;
        }

        .ExternalClass {
            width: 100%;
        }

        .ExternalClass * {
            line-height: 100%;
        }

        body {
            margin: 0;
            padding: 0;
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
        }

        table, td {
            border-collapse: collapse;
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        img {
            border: 0;
            height: auto;
            line-height: 100%;
            outline: none;
            text-decoration: none;
            -ms-interpolation-mode: bicubic;
        }
        .rating-stars img {
            margin-left: 5px;
            margin-right: 5px;
            width: 15px;
            height: 15px;
            display: inline-block;
        }
        p {
            display: block;
            margin: 13px 0;
        }
    </style><!--[if !mso]><!-->
    <style type="text/css">  @media only screen and (max-width: 480px) {
            @-ms-viewport {
                width: 320px;
            }    @viewport {
                width: 320px;
            }
        }</style><!--<![endif]--><!--[if mso]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml><![endif]--><!--[if lte mso 11]>
    <style type="text/css">  .outlook-group-fix {
        width: 100% !important;
    }</style><![endif]--><!--[if !mso]><!-->
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet" type="text/css">
    <style type="text/css">        @import url(https://fonts.googleapis.com/css?family=Roboto);
        @import url(https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700);    </style>  <!--<![endif]-->
    <style type="text/css">  @media only screen and (min-width: 480px) {
            .mj-column-per-100 {
                width: 100% !important;
            }

            .mj-column-per-40 {
                width: 40% !important;
            }

            .mj-column-per-60 {
                width: 60% !important;
            }
        }</style>
</head>
<body style="background: #E2E2E2;">

<div class="mj-container" style="background-color:#E2E2E2;"><!--[if mso | IE]>
    <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center"
           style="width:600px;">
        <tr>
            <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">      <![endif]-->
    <div style="margin:0px auto;max-width:600px;background:#222;">
        <table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;background:#222;"
               align="center" border="0">
            <tbody>
            <tr>
                <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:0px 0px 0px 0px;">
                    <!--[if mso | IE]>
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="vertical-align:top;width:600px;">      <![endif]-->
                    <div class="mj-column-per-100 outlook-group-fix"
                         style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
                        <table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
                            <tbody>
                            <tr>
                                <td style="word-wrap:break-word;font-size:0px;padding:19px 19px 19px 19px;"
                                    align="center">
                                    <table role="presentation" cellpadding="0" cellspacing="0"
                                           style="border-collapse:collapse;border-spacing:0px;" align="center"
                                           border="0">
                                        <tbody>
                                        <tr>
                                            <td style="width:150px;"><img alt="" title="" height="auto"
                                                                          src="https://storage.googleapis.com/topolio2241/gjirafa50.png"
                                                                          style="border:none;border-radius:0px;display:block;font-size:13px;outline:none;text-decoration:none;width:100%;height:auto;"
                                                                          width="150"></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--[if mso | IE]>      </td></tr></table>      <![endif]--></td>
            </tr>
            </tbody>
        </table>
    </div>
    <!--[if mso | IE]>      </td></tr></table>      <![endif]-->      <!--[if mso | IE]>
    <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center"
           style="width:600px;">
        <tr>
            <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">      <![endif]-->
    <div style="margin:0px auto;max-width:600px;background:#222;">
        <table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;background:#222;"
               align="center" border="0">
            <tbody>
            <tr>
                <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:10px 0px 10px 0px;">
                    <!--[if mso | IE]>
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="vertical-align:top;width:600px;">      <![endif]-->
                    <div class="mj-column-per-100 outlook-group-fix"
                         style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
                        <table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
                            <tbody>
                            <tr>
                                <td style="word-wrap:break-word;font-size:0px;padding:0px 20px 0px 20px;"
                                    align="center">
                                    <div style="cursor:auto;color:#FFFFFF;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:center;">
                                        <p><span style="font-size:14px;"><strong>FALEMINDERIT P&#xCB;R BLERJEN {$order_info.firstname|upper}!</strong></span>
                                        </p></div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--[if mso | IE]>      </td></tr></table>      <![endif]--></td>
            </tr>
            </tbody>
        </table>
    </div>
    <!--[if mso | IE]>      </td></tr></table>      <![endif]-->      <!--[if mso | IE]>
    <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center"
           style="width:600px;">
        <tr>
            <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">      <![endif]-->
    <div style="margin:0px auto;max-width:600px;background:#FFFFFF;">
        <table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;background:#FFFFFF;"
               align="center" border="0">
            <tbody>
            <tr>
                <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:9px 0px 9px 0px;">
                    <!--[if mso | IE]>
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="vertical-align:top;width:600px;">      <![endif]-->
                    <div class="mj-column-per-100 outlook-group-fix"
                         style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
                        <table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
                            <tbody>
                            <tr>
                                <td style="word-wrap:break-word;font-size:0px;padding:0px 24px 0px 24px;"
                                    align="center">
                                    <div style="cursor:auto;color:#757575;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:center;">
                                        <p><span style="font-size:14px;">Si po ju duket produkti q&#xEB; keni marr&#xEB;?</span>
                                        </p>
                                        <p><span style="font-size:14px;">N&#xEB;se ju p&#xEB;lqen, mund ta vler&#xEB;soni at&#xEB; duke klikuar butonin vler&#xEB;so m&#xEB; posht&#xEB; dhe t&#xEB; p&#xEB;rfitoni kupon me vler&#xEB; 5% n&#xEB; blerjen e radh&#xEB;s <b>Kuponi: 5yje</b></span>
                                        </p></div>
                                </td>
                            </tr>
                            <tr>
                                <td style="word-wrap:break-word;font-size:0px;padding:0px 20px 0px 20px;"
                                    align="center">
                                    <div style="cursor:auto;color:#000000;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:22px;text-align:center;">
                                        {if $order_info.company_id != '1'}
                                        <p><span style="font-size:12px;"><strong><a href="https://gjirafa50.al/index.php?dispatch=pages.add-discussion&order_id={$order_info.order_id}&e_key={$e_key}" style="background: #e65228; border-radius: 5px; padding: 10px; text-decoration: none"><span style="color:#ffffff;">VLER&#xCB;SO</span></a></strong></span>
                                            {else}
                                        <p><span style="font-size:12px;"><strong><a href="https://gjirafa50.com/index.php?dispatch=pages.add-discussion&order_id={$order_info.order_id}&e_key={$e_key}" style="background: #e65228; border-radius: 5px; padding: 10px; text-decoration: none"><span style="color:#ffffff;">VLER&#xCB;SO</span></a></strong></span>
                                            {/if}
                                        </p></div>
                                </td>
                            </tr>
                            <tr>
                                <td style="word-wrap:break-word;font-size:0px;padding:0px 20px 0px 20px;"
                                    align="center">
                                    <div style="margin-top: 10px; margin-bottom: 10px;  cursor:auto;color:#000000;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:22px;text-align:center;" class="rating-stars">
                                        <img src="https://gjstatic.blob.core.windows.net/fix/50_email_template/star.png" />
                                        <img src="https://gjstatic.blob.core.windows.net/fix/50_email_template/star.png" />
                                        <img src="https://gjstatic.blob.core.windows.net/fix/50_email_template/star.png" />
                                        <img src="https://gjstatic.blob.core.windows.net/fix/50_email_template/star.png" />
                                        <img src="https://gjstatic.blob.core.windows.net/fix/50_email_template/star.png" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <p><b>*Vlera e porosisë duhet të jetë më e lartë se 5 EUR.</b></p>
                                </td>
                            </tr>
                            <tr>
                                <td style="word-wrap:break-word;font-size:0px;padding:10px 25px;padding-top:10px;padding-bottom:10px;">
                                    <p style="font-size:1px;margin:0px auto;border-top:1px solid #ddd;width:100%;"></p>
                                    <!--[if mso | IE]>
                                    <table role="presentation" align="center" border="0" cellpadding="0" cellspacing="0"
                                           style="font-size:1px;margin:0px auto;border-top:1px solid #ddd;width:100%;"
                                           width="600">
                                        <tr>
                                            <td style="height:0;line-height:0;"> </td>
                                        </tr>
                                    </table><![endif]--></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--[if mso | IE]>      </td></tr></table>      <![endif]--></td>
            </tr>
            </tbody>
        </table>
    </div>
    <!--[if mso | IE]>      </td></tr></table>      <![endif]-->      <!--[if mso | IE]>

    <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center"
           style="width:600px;">
        <tr>
            <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">      <![endif]-->
    <div style="margin:0px auto;max-width:600px;background:#FFFFFF;">
        <table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;background:#FFFFFF;"
               align="center" border="0">
            <tbody>
            {foreach $order_info.products as $oi}
                {*{assign var="container" value=intval($product.product_id/8000+1)}*}
                {*{if $container gt 5}*}
                {*{$container=5}*}
                {*{/if}*}
                {assign var="meta_azure_path" value={get_images_from_blob product_id=$oi.product_id count=1}}
                <tr>
                    <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:0px 0px 0px 0px;">
                        <!--[if mso | IE]>
                        <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="vertical-align:top;width:240px;">      <![endif]-->
                        <div class="mj-column-per-40 outlook-group-fix"
                             style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
                            <table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tbody>
                                <tr>
                                    <td style="word-wrap:break-word;font-size:0px;padding:0px 0px 0px 0px;" align="center">
                                        <table role="presentation" cellpadding="0" cellspacing="0"
                                               style="border-collapse:collapse;border-spacing:0px;" align="center"
                                               border="0">
                                            <tbody>
                                            <tr>
                                                <td style="width:100px;height: 100px;background-image: url('{$meta_azure_path}');background-position: center;background-size: contain;background-repeat: no-repeat;"></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <!--[if mso | IE]>      </td>
                        <td style="vertical-align:top;width:360px;">      <![endif]-->
                        <div class="mj-column-per-60 outlook-group-fix"
                             style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
                            <table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tbody>
                                <tr>
                                    <td style="word-wrap:break-word;font-size:0px;padding:0px 20px 0px 20px;" align="left">
                                        <div style="cursor:auto;color:#757575;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:left;">
                                            <p><strong><span style="font-size:14px;">{$oi.product}</span></strong>
                                            </p></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="word-wrap:break-word;font-size:0px;padding:0px 20px 0px 20px;" align="left">
                                        <div style="cursor:auto;color:#757575;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:left;">
                                            <p><span style="font-size:12px;">{$oi.desciption}</span></p></div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <!--[if mso | IE]>      </td></tr></table>      <![endif]-->
                    </td>
                </tr>
                <div style="margin:0px auto;max-width:600px;background:#FFFFFF;"><table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;background:#FFFFFF;" align="center" border="0"><tbody><tr><td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:0px 0px 0px 0px;"><!--[if mso | IE]>      <table role="presentation" border="0" cellpadding="0" cellspacing="0">        <tr>          <td style="vertical-align:top;width:600px;">      <![endif]--><div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-wrap:break-word;font-size:0px;padding:10px 25px;padding-top:10px;padding-bottom:10px;padding-right:25px;padding-left:25px;"><p style="font-size:1px;margin:0px auto;border-top:1px solid #ddd;width:100%;"></p><!--[if mso | IE]><table role="presentation" align="center" border="0" cellpadding="0" cellspacing="0" style="font-size:1px;margin:0px auto;border-top:1px solid #ddd;width:100%;" width="600"><tr><td style="height:0;line-height:0;">&nbsp;</td></tr></table><![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>      </td></tr></table>      <![endif]--></td></tr></tbody></table></div>
            {/foreach}
            </tbody>
        </table>
    </div>

    <!--[if mso | IE]>      </td></tr></table>      <![endif]-->      <!--[if mso | IE]>

    <br>
    <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center"
           style="width:600px;">
        <tr>
            <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">      <![endif]-->
    <div style="margin:0px auto;max-width:600px;background:#222;">
        <table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;background:#222;"
               align="center" border="0">
            <tbody>
            <tr>
                <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:9px 0px 9px 0px;">
                    <!--[if mso | IE]>
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="vertical-align:top;width:600px;">      <![endif]-->
                    <div class="mj-column-per-100 outlook-group-fix"
                         style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
                        <table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
                            <tbody>
                            <tr>
                                <td style="word-wrap:break-word;font-size:0px;padding:0px 20px 0px 20px;"
                                    align="center">
                                    <div style="cursor:auto;color:#fff;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:center;">
                                        <p><span style="font-size:12px;">E vler&#xEB;sojm&#xEB; &#xE7;do sugjerim q&#xEB; </span><span
                                                    style="font-size:12px;">mund t&#xEB; kemi nga ana juaj.</span></p>
                                        <p><span style="font-size:12px;">E g&#xEB;zofshit, Gjirafa50</span></p>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="word-wrap:break-word;font-size:0px;padding:10px 25px;" align="center">
                                    <div><!--[if mso | IE]>
                                        <table role="presentation" border="0" cellpadding="0" cellspacing="0"
                                               align="undefined">
                                            <tr>
                                                <td>      <![endif]-->
                                        <table role="presentation" cellpadding="0" cellspacing="0"
                                               style="float:none;display:inline-table;" align="center" border="0">
                                            <tbody>
                                            <tr>
                                                <td style="padding:4px;vertical-align:middle;">
                                                    <table role="presentation" cellpadding="0" cellspacing="0"
                                                           style="background:none;border-radius:3px;width:35px;"
                                                           border="0">
                                                        <tbody>
                                                        <tr>
                                                            <td style="vertical-align:middle;width:35px;height:35px;padding: 5px;"><a
                                                                        href="https://www.facebook.com/Gjirafa50/"><img
                                                                            alt="facebook" height="35"
                                                                            src="https://s3-eu-west-1.amazonaws.com/ecomail-assets/editor/social-icos/outlinedbw/facebook.png"
                                                                            style="display:block;border-radius:3px;"
                                                                            width="35"></a>
                                                            </td>
                                                            <td style="vertical-align:middle;width:35px;height:35px;padding: 5px;"><a
                                                                        href="https://www.instagram.com/gjirafa50"><img
                                                                            alt="facebook" height="35"
                                                                            src="https://s3-eu-west-1.amazonaws.com/ecomail-assets/editor/social-icos/outlinedbw/instagram.png"
                                                                            style="display:block;border-radius:3px;"
                                                                            width="35"></a>
                                                            </td>
                                                            <td style="vertical-align:middle;width:35px;height:35px;padding: 5px;"><a
                                                                        href="https://twitter.com/gjirafa50"><img
                                                                            alt="facebook" height="35"
                                                                            src="https://s3-eu-west-1.amazonaws.com/ecomail-assets/editor/social-icos/outlinedbw/twitter.png"
                                                                            style="display:block;border-radius:3px;"
                                                                            width="35"></a>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td style="padding:4px 4px 4px 0;vertical-align:middle;"><a
                                                            href="https://www.facebook.com/Gjirafa50/"
                                                            style="text-decoration:none;text-align:left;display:block;color:#333333;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:22px;border-radius:3px;"></a>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <!--[if mso | IE]>      </td></tr></table>      <![endif]--></div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--[if mso | IE]>      </td></tr></table>      <![endif]--></td>
            </tr>
            </tbody>
        </table>
    </div>
    <!--[if mso | IE]>      </td></tr></table>      <![endif]--></div>
</body>
</html>