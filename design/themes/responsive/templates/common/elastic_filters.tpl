{assign var="is_al" value=$al|fn_isAL}
{if $search.features_hash != ""}
    {assign var=feature_selected value="_"|explode:$search.features_hash}
    {assign var=features_ids value=[]}

    {foreach from=$feature_selected item=feature_item}
        {assign var=feature_variants value="-"|explode:$feature_item}
        {assign var=variant_ids value=[]}

        {for $i=1 to ($feature_variants|@count - 1)}
            {append var=variant_ids value=$feature_variants[$i] index=($i-1)}
        {/for}

        {if $feature_variants|count gt 0}
            {append var=features_ids value=$variant_ids index=$feature_variants[0]}
        {/if}
    {/foreach}

{/if}

{if $elastic_filters}
    <div class="span4" style="margin:0;">
        <p id="toggle-filter" class="hidden-desktop">
            <svg viewBox="0 0 49.913 49.913" xmlns="http://www.w3.org/2000/svg"><path d="M2.606 8.187c-.022-.014-.042.012-.027.033l17.37 23.77c.004.006.012.01.019.01h9.976a.023.023 0 0 0 .019-.01l17.37-23.77c.015-.021-.005-.048-.027-.034" fill="#e45227"></path><path d="M29.957 40l-9.895 9.895a.061.061 0 0 1-.105-.043V32h10v8z" fill="#d7461b"></path><ellipse cx="24.957" cy="6" rx="24" ry="6" fill="#d7461b"></ellipse></svg>
        </p>
        <div class="ty-sidebox" id="filter-scrollbar" style="max-height: 5020px;  overflow-x: auto;">
            {if $smarty.const.IS_MOBILE}
            <p class="ty-sidebox__title cm-combination " id="sw_sidebox_29">

                {*<span class="ty-sidebox__title-wrapper hidden-phone">Filterat e produkteve</span>*}
                <span class="ty-sidebox__title-wrapper visible-phone filter-title">Filterat e produkteve <span><i class="ty-icon-cancel"></i></span></span>


            </p>
            {/if}
            <div class="ty-sidebox__body" id="sidebox_29">
                <div id="elastic-filters">
                    <div id="es-search-form" action="/index.php?dispatch={$url}" type="GET">
                        <input type="hidden" id="is_al" name="is_al" value="{$is_al}">
                        {if $url =='products.search'}
                        <input type="hidden" value="{$smarty.request.q}" name="query_value" id="es_q">
                        {else}
                        <input type="hidden" value="{$category_data.category_id}" name="query_value" id="es_q">
                        {/if}
                        <div class="active-filters">
                        </div>
                        <div class="ty-product-filters__wrapper">
                            <div class="ty-product-filters__block">
                                <div id="sw_content_29_1265"
                                     class="ty-product-filters__switch cm-combination-filter_29_1265 open cm-save-state cm-ss-reverse">
                                    <span class="ty-product-filters__title">Çmimi</span>
                                    <i class="ty-product-filters__switch-down ty-icon-down-open hidden-phone"></i>
                                    <i class="ty-product-filters__switch-right ty-icon-up-open hidden-phone"></i>
                                </div>
                                <ul class="ty-product-filters " id="content_29_1265" >
                                    <li>{include file="blocks/product_filters/components/product_filter_slider.tpl" filter=$price_filter}</li>
                                </ul>
                            </div>
                            {foreach from=$elastic_filters key="header" item=filter}
                                {assign var=feature_header value="§§"|explode:$header}
                                <div class="ty-product-filters__block grid" >
                                    <div id="sw_content_29_{$feature_header[0]}"
                                         class="ty-product-filters__switch cm-combination-filter_29_{$feature_header[0]} cm-save-state cm-ss-reverse">
                                        <span class="ty-product-filters__title">{$feature_header[1]}</span>
                                        <i class="ty-product-filters__switch-down ty-icon-down-open hidden-phone"></i>
                                        <i class="ty-product-filters__switch-right ty-icon-up-open hidden-phone"></i>
                                    </div>
                                    <div class="ty-product-filters " id="content_29_{$feature_header[0]}" style="display: none">
                                        <div class="search-header">
                                            <i class="ty-icon-search search-filter-icon"></i>
                                            <input type="text" placeholder="Kërko" class="search-box search-texti" data-searchFor="ranges_29_{$feature_header[0]}">
                                            <span class="list-count" id="listCount-ranges_29_{$feature_header[0]}"></span>
                                        </div>
                                        <ul id="ranges_29_{$feature_header[0]}"
                                            class="ty-product-filters__variants cm-filter-table"
                                            data-ca-input-id="elm_search_29_{$feature_header[0]}"
                                            data-ca-clear-id="elm_search_clear_29_{$feature_header[0]}"
                                            data-ca-empty-id="elm_search_empty_29_{$feature_header[0]}">
                                            {assign var="feature_header_my" value="ranges_29_{$feature_header[0]}"}
                                            {foreach from=$filter item=feature}
                                                <li class="cm-product-filters-checkbox-container ty-product-filters__group my_list in">
                                                    {assign var=feature_list value="§§"|explode:$feature}
                                                    {if isset($features_ids[$feature_header[0]]) && $features_ids[$feature_header[0]]}
                                                        {if in_array($feature_list[0], $features_ids[$feature_header[0]])}
                                                            <label class="lbl"><input class="cm-product-filters-checkbox checkItems"
                                                                          type="checkbox"
                                                                          name="product_filters[{$feature_header[0]}]"
                                                                          data-ca-filter-id="{$feature_header[0]}"
                                                                          value="{$feature_list[0]}"
                                                                          checked="checked"
                                                                                      id="elm_checkbox_29_{$feature_header[0]}_{$feature_list[0]}"> <span>{$feature_list[1]}</span>
                                                                <span class="checkmark"></span>
                                                            </label>
                                                        {/if}
                                                        <script>document.getElementById('content_29_{$feature_header[0]}').style.display='block';</script>
                                                    {else}
                                                        <label class="lbl"><input class="cm-product-filters-checkbox"
                                                                      type="checkbox"
                                                                      name="product_filters[{$feature_header[0]}]"
                                                                      data-ca-filter-id="{$feature_header[0]}"
                                                                      value="{$feature_list[0]}"
                                                                      id="elm_checkbox_29_{$feature_header[0]}_{$feature_list[0]}">{$feature_list[1]}
                                                            <span class="checkmark"></span>
                                                        </label>
                                                    {/if}
                                                </li>

                                            {/foreach}
                                            <span class="empty-item">Nuk ka rezultate</span>
                                        </ul>

                                    </div>
                                </div>
                            {/foreach}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>{literal}

        for (var filtersList = $(".ty-product-filters").not("#content_29_1265"), i = 0; i < filtersList.length; i++) filtersList.eq(i).find("li").length < "6" &&  ($(".search-texti").eq(i).parent().addClass("hidden")), $(".list-count").eq(i).text(filtersList.eq(i).find("li").length + " filterë");
        $(".search-texti").keyup(function() {
            var i = $(this).data("searchfor"),
                t = $(this).val(),
                e = $("#" + i),
                s = t.replace(/ /g, "'):containsi('");
            $("li", e).not(":containsi('" + s + "')").each(function(i) {
                $(this).addClass("hiding out").removeClass("in"), setTimeout(function() {
                    $(".out").addClass("hidden")
                }, 300)
            }), $("li:containsi('" + s + "')", e).each(function(i) {
                $(this).removeClass("hidden out").addClass("in"), setTimeout(function() {
                    $(".in", e).removeClass("hiding")
                }, 1)
            });
            var n = $(".in", e).length;
            if ($("#listCount-" + i).text(n + " filterë"), 0 == $(this).val().length) {
                $("li", e).removeClass("hidden out hiding").addClass("in");
                n = $(".in", e).length;
                $("#listCount-" + i).text(n + " filterë"), $(".empty-item").hide()
            }
            "0" == n ? (e.addClass("empty"), $(".empty-item").show()) : (e.removeClass("empty"), $(".empty-item").hide())
        });

        if($(window).width() < 769){
            $('#toggle-filter').click(function() {
                $('body').toggleClass('filter-open')
            });
            $('.ty-product-filters__switch').click(function(event){
                if($(this).hasClass('open')){
                    event.stopPropagation();
                }
                $('#filter-scrollbar').scrollTop(0,0);
                $('.ty-product-filters__switch').not(this).removeClass('open');
                $('.ty-product-filters').not($(this).next()).hide();

            });
            $('.ty-sidebox__title.cm-combination').click(function(){
                $('body').removeClass('filter-open')
            });
            $('.ty-product-filters').hide();
            $('#content_29_1265').show();

            setTimeout(function(){
                if($('.filter-item').length){
                    $('.active-filters').show();
                }
            });
        }
        else{
            $('.active-filters').prependTo('.ty-sort-container');
        }

        var checkitems = $(".checkItems:checked");
        for(var i = 0; i < checkitems.length; i++)
        {
            function getSecondPart(str) {
                return str.split('elm_checkbox_')[1];
            }

            var filterName = getSecondPart(checkitems[i].id);
            filterName = filterName.split('_', 2);
            filterName = filterName.join('_');
            filterName = $('#sw_content_' + filterName + ' span').text();

            var filterId = $($('.checkItems:checked')[i]).attr('id');
            var filterItm = $($(".checkItems:checked")[i]).next().html();
            var appended = "<span id='"+ filterId+"' class='filter-item'>" + "<span style='font-size: 12px; color: #777;'>" + filterName + "</span><br><span>" +filterItm + "</span> <i class='ty-icon-cancel'></i></span>";
            $('.active-filters').append(appended);
            $('.active-filters-out').append(appended);
                $("span#"+filterId).click(function(event){
                    var thisCL = $(this).attr('id');
                    console.log(thisCL)
                     $("input#"+thisCL).click();
                    event.preventDefault();
                });
        }

        $('.ty-product-filters__group').click(function () {
            $(window).scrollTop(0)
        })
        setTimeout(function(){
            if($('.filter-item').length){
                $('.active-filters-out').show();
            }
        });
        {/literal}</script>
{/if}
