{if !$auth.user_id}
    <style>{literal}#ssoPopup, .ssoPopup_wrapper {
            top: 0;
            right: 0;
            left: 0;
            bottom: 0
        }

        .ssoPopup_wrapper {
            max-height: 80%;
            height: 100%;
            max-width: 100%;
            width: 450px;
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            margin: auto;
            max-height: 420px;
        }

        #ssoPopup {
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            background: rgba(0, 0, 0, 0.5);
            z-index: 9999999;
            opacity: 0;
            pointer-events: none;
            user-select: none;
            visibility: hidden;
            display: none;
        }

        #ssoPopup.active {
            opacity: 1;
            pointer-events: all;
            visibility: visible;
            display: block;
        }

        #ssoPopup.active #ssoPopup_iframe {
            opacity: 1;
            pointer-events: all;
        }

        #ssoPopup_iframe {
            opacity: 0;
            -webkit-transition: opacity .3s ease;
            transition: opacity .3s ease;
            height: 100%;
            width: 100%;
            border: 0;
            overflow: auto;
            pointer-events: all;
            cursor: pointer;
            z-index: 9999;
        }

        .ssoPopup_close {
            position: absolute;
            z-index: 99;
            right: 15px;
            top: 15px;
            text-align: center;
            cursor: pointer;
            line-height: 0;
        }

        .ssoPopup_close svg {
            width: 15px;
            height: 15px;
            fill: #9f9f9f;
        }

        .sso-container {
            height: 100%;
            width: 100%;
            padding: 0px;
            background: rgba(255,255,255, .95);
            border-radius: 6px;
            padding: 0 10px;
            /*padding-top: 30px;*/
        }

        @media (max-width: 768px) {
            .ssoPopup_wrapper {
                max-width: 90%;
                margin: auto
            }

            .ssoPopup_close {
                right: 8px;
                top: 7px
            }
        }

        .spinner-loader {
            width: 40px;
            height: 40px;
            border-radius: 50%;
            border-left: 7px solid rgba(230, 126, 34, .32);
            border-bottom: 7px solid rgba(230, 126, 34, .32);
            border-right: 7px solid rgba(230, 126, 34, .32);
            border-top: 7px solid #e67e22;
            -webkit-animation: spinner .7s linear infinite;
            -moz-animation: spinner .7s linear infinite;
            -o-animation: spinner .7s linear infinite;
            animation: spinner .7s linear infinite;
            z-index: 9;
            position: absolute;
            top: 0;
            right: 0;
            left: 0;
            bottom: 0;
            margin: auto
        }

        @-webkit-keyframes spinner {
            0% {
                -webkit-transform: rotate(-360deg);
                -moz-transform: rotate(-360deg);
                -ms-transform: rotate(-360deg);
                -o-transform: rotate(-360deg);
                transform: rotate(-360deg)
            }
        }

        @-moz-keyframes spinner {
            0% {
                -webkit-transform: rotate(-360deg);
                -moz-transform: rotate(-360deg);
                -ms-transform: rotate(-360deg);
                -o-transform: rotate(-360deg);
                transform: rotate(-360deg)
            }
        }

        @keyframes spinner {
            0% {
                -webkit-transform: rotate(-360deg);
                -moz-transform: rotate(-360deg);
                -ms-transform: rotate(-360deg);
                -o-transform: rotate(-360deg);
                transform: rotate(-360deg)
            }
        }{/literal}</style>
    <div id="ssoPopup">
        <div class="ssoPopup_wrapper">
            <span class="ssoPopup_close">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 212.982 212.982"><path
                            d="M131.804 106.491l75.936-75.936c6.99-6.99 6.99-18.323 0-25.312-6.99-6.99-18.322-6.99-25.312 0L106.491 81.18 30.554 5.242c-6.99-6.99-18.322-6.99-25.312 0-6.989 6.99-6.989 18.323 0 25.312l75.937 75.936-75.937 75.937c-6.989 6.99-6.989 18.323 0 25.312 6.99 6.99 18.322 6.99 25.312 0l75.937-75.937 75.937 75.937c6.989 6.99 18.322 6.99 25.312 0 6.99-6.99 6.99-18.322 0-25.312l-75.936-75.936z"
                            fill-rule="evenodd" clip-rule="evenodd"/>
                </svg>
             </span>
            <div class="sso-container bbox"></div>
        </div>
    </div>
    <script>
        {literal}
        $('.ty-account-info__title, .login-gjirafa, #user-area a, #gift-wrapper').click(function () {
            sso.init();
            return false;
        })


        var sso = {
            iframe: document.createElement('iframe'),
            el: document.querySelector("#ssoPopup"),
            callback: function ($callback) {
                $callback;
                window.location.reload();
            },
            eventAdded: false,
            eventListeners: function () {
                this.eventAdded = true;
                var $this = this,
                    close = document.querySelector(".ssoPopup_close");

                close.addEventListener("click", function () {
                    $this.el.classList.remove("active");
                    $("#ssoPopup_iframe").remove();
                });


                $(document).on('click', '#ssoPopup.active', function (event) {
                    if (!$(event.target).closest(".ssoPopup_wrapper, ssoPopup_wrapper*").length) {
                        $this.el.classList.remove("active");
                        $("#ssoPopup_iframe").remove();
                    }
                });


                var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent",
                    eventer = window[eventMethod],
                    messageEvent = eventMethod === "attachEvent" ? "onmessage" : "message";
                eventer(messageEvent, function (e) {
                    var message = e.data || e.message

                    if (typeof message != "string")
                        return;

                    if (message === "popupTrue")
                        $this.callback();
                    else if (e.data.indexOf("LoginOtherFb") > -1) {
                        var link = e.data.replace("#rtu", encodeURIComponent(window.location.href));
                        window.location.href = link;
                    }
                });
            },
            init: function ($callback) {
                var is_safari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);
                var baseUrl = document.getElementById("sso_base_url").value;

                if (is_safari) {
                    window.location = baseUrl + "/Llogaria/Kycu?returnUrl=" + encodeURIComponent(window.location.href) + "&sid=" + sid;
                    return;
                }

                var element = document.getElementById('ssoPopup_iframe');
                if (!(typeof (element) != 'undefined' && element != null)) {
                    this.iframe = document.createElement('iframe');
                    this.iframe.id = "ssoPopup_iframe";
                    this.iframe.src = baseUrl + "/Llogaria/KycuPartial?returnUrl=" + encodeURIComponent(window.location.href);
                    document.querySelector(".sso-container").appendChild(this.iframe);
                    this.el.classList.add("active");

                    if (this.eventAdded)
                        return;

                    this.eventListeners();
                }
            }
        }
        window.addEventListener('message', function(event) {
            if(event.data.event_id === 'ssoEvent'){
                window.location.replace(event.data.obj.returnUrl);
            }
        });
        {/literal}
    </script>
{/if}