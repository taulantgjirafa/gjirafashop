<input type="hidden" name="logged-in-user-email" value="{$smarty.session.auth.email}">

{*{literal}*}
{*<script>WebFontConfig = {classes: !1, google: {families: ["Roboto:300,400,500,700:latin"]}}, function (e) {*}
{*var o = e.createElement("script"), t = e.scripts[0];*}
{*o.src = "https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js", o.async = !0, t.parentNode.insertBefore(o, t)*}
{*}(document);</script>*}
{*{/literal}*}

{*<!-- Start of gjirafa-support Zendesk Widget script -->*}
{*<script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=0830c254-756f-47bd-b7fd-4503f77f2581"> </script>*}
{*<!-- End of gjirafa-support Zendesk Widget script -->*}

<script>
    var sid = `{fn_get_session_cookie_value()}`;
</script>

<script src="https://wchat.freshchat.com/js/widget.js"></script>

{if !$smarty.const.DEVELOPMENT}
    {if $al|fn_isal}
    {literal}
        <!-- Global site tag (gtag.js) - Google Ads: 381755593 -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=AW-381755593"></script>
        <script>
            window.dataLayer = window.dataLayer || [];

            function gtag() {
                dataLayer.push(arguments);
            }

            gtag('js', new Date());

            gtag('config', 'AW-381755593');
        </script>
    {/literal}
    {else}
    {literal}
        <!-- Global site tag (gtag.js) - Google Ads: 622454293 -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=AW-622454293"></script>
        <script>
            window.dataLayer = window.dataLayer || [];

            function gtag() {
                dataLayer.push(arguments);
            }

            gtag('js', new Date());

            gtag('config', 'AW-622454293');
        </script>
    {/literal}
    {/if}
{/if}

{if !$config.tweaks.dev_js}
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" data-no-defer></script>
    <script data-no-defer> if (!window.jQuery) {
            document.write('{script src="js/lib/jquery/jquery.min.js" no-defer=true escape=true}');
        }</script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js" data-no-defer></script>
    <script data-no-defer> if (!window.jQuery.ui) {
            document.write('{script src="js/lib/jqueryui/jquery-ui.custom.min.js" no-defer=true escape=true}');
        } </script>
{/if}
{scripts}

{if $config.tweaks.dev_js}
    {script src="js/lib/jquery/jquery.min.js"}
    {script src="js/lib/jqueryui/jquery-ui.custom.min.js"}
{/if}
{script src="js/tygh/recommendations.js"}

<script src="https://snippets.freshchat.com/js/fc-pre-chat-form-v2.min.js"></script>
{script src="js/tygh/freshchat-preform.js"}
{if $al|fn_isal}
    {script src="js/tygh/freshchat-al.js"}
{else}
    {script src="js/tygh/freshchat.js"}
{/if}
{script src="js/tygh/lazysizes.min.js"}
{script src="js/lib/modernizr/modernizr.custom.js"}
{*{script src="js/tygh/macy.min.js"}*}
{script src="js/tygh/core.js"}
{script src="js/tygh/snapbackCache.js"}
{script src="js/tygh/infinite_scroll.js"}
{script src="js/tygh/grid_animations.js"}
{script src="js/tygh/ajax.js"}
{script src="js/tygh/history.js"}
{script src="js/lib/autonumeric/autoNumeric.js"}
{script src="js/lib/appear/jquery.appear-1.1.1.js"}
{script src="js/lib/inputmask/jquery.inputmask.min.js"}
{script src="js/lib/autocomplete/jquery.autocomplete.js"}
{script src="js/tygh/sidenav.min.js"}
{script src="js/tygh/ssoreqs.js"}

{if $smarty.session.auth.user_id}
    <script>
        loadAsyncIframe(ssourl + 'cookie/verifyAccount', 'verifyAcc');
    </script>
{/if}

{if !$runtime.customization_mode.live_editor}
    {script src="js/lib/tools/tooltip.min.js"}
{/if}

{script src="js/tygh/editors/`$settings.Appearance.default_wysiwyg_editor`.editor.js"}

{script src="js/tygh/responsive.js"}

{if $runtime.customization_mode.live_editor}
    {script src="js/lib/autosize/jquery.autosize.js"}
    {script src="js/tygh/live_editor_mode.js"}
{/if}
{if !$smarty.const.DEVELOPMENT}
    {script src="js/tygh/sha256.min.js"}
    {script src="js/addons/google_analytics/track.js"}
{/if}
    <script type="text/javascript">
        {*        {literal}null == $.cookie.get("gj50__gjci") && $("html").hasClass("homepage") && $.ajax({*}
        {*            url: "https://bisko.gjirafa.com/gjci",*}
        {*            type: "GET",*}
        {*            xhrFields: {withCredentials: !0},*}
        {*            success: function (e) {*}
        {*                var s = new Date;*}
        {*                s.setHours(s.getHours() + 1), $.cookie.set("gj50__gjci", e, s), $.ajax({*}
        {*                    url: "index.php?dispatch=products.is_products",*}
        {*                    success: function (e) {*}
        {*                        if (window.innerWidth < 769 && !$(".product-content").length) {*}
        {*                            $('.grid-list').first().append(e);*}
        {*                        } else $(".grid-list").append(e)*}
        {*                    }*}
        {*                })*}
        {*            }*}
        {*        });*}
        {*        {/literal}*}
        (function (_, $) {
            _.tr({
                cannot_buy: '{__("cannot_buy")|escape:"javascript"}',
                no_products_selected: '{__("no_products_selected")|escape:"javascript"}',
                error_no_items_selected: '{__("error_no_items_selected")|escape:"javascript"}',
                delete_confirmation: '{__("delete_confirmation")|escape:"javascript"}',
                text_out_of_stock: '{__("text_out_of_stock")|escape:"javascript"}',
                items: '{__("items")|escape:"javascript"}',
                text_required_group_product: '{__("text_required_group_product")|escape:"javascript"}',
                save: '{__("save")|escape:"javascript"}',
                close: '{__("close")|escape:"javascript"}',
                notice: '{__("notice")|escape:"javascript"}',
                warning: '{__("warning")|escape:"javascript"}',
                error: '{__("error")|escape:"javascript"}',
                empty: '{__("empty")|escape:"javascript"}',
                text_are_you_sure_to_proceed: '{__("text_are_you_sure_to_proceed")|escape:"javascript"}',
                text_invalid_url: '{__("text_invalid_url")|escape:"javascript"}',
                error_validator_email: '{__("error_validator_email")|escape:"javascript"}',
                error_validator_phone: '{__("error_validator_phone")|escape:"javascript"}',
                error_validator_integer: '{__("error_validator_integer")|escape:"javascript"}',
                error_validator_multiple: '{__("error_validator_multiple")|escape:"javascript"}',
                error_validator_password: '{__("error_validator_password")|escape:"javascript"}',
                error_validator_required: '{__("error_validator_required")|escape:"javascript"}',
                error_validator_zipcode: '{__("error_validator_zipcode")|escape:"javascript"}',
                error_validator_message: '{__("error_validator_message")|escape:"javascript"}',
                text_page_loading: '{__("text_page_loading")|escape:"javascript"}',
                error_ajax: '{__("error_ajax")|escape:"javascript"}',
                text_changes_not_saved: '{__("text_changes_not_saved")|escape:"javascript"}',
                text_data_changed: '{__("text_data_changed")|escape:"javascript"}',
                placing_order: '{__("placing_order")|escape:"javascript"}',
                file_browser: '{__("file_browser")|escape:"javascript"}',
                browse: '{__("browse")|escape:"javascript"}',
                more: '{__("more")|escape:"javascript"}',
                text_no_products_found: '{__("text_no_products_found")|escape:"javascript"}',
                cookie_is_disabled: '{__("cookie_is_disabled")|escape:"javascript"}'
            });
            $.extend(_, {
                index_script: '{$config.customer_index|escape:javascript nofilter}',
                changes_warning: /*'{$settings.Appearance.changes_warning|escape:javascript nofilter}'*/'N',
                currencies: {
                    'primary': {
                        'decimals_separator': '{$currencies.$primary_currency.decimals_separator|escape:javascript nofilter}',
                        'thousands_separator': '{$currencies.$primary_currency.thousands_separator|escape:javascript nofilter}',
                        'decimals': '{$currencies.$primary_currency.decimals|escape:javascript nofilter}'
                    },
                    'secondary': {
                        'decimals_separator': '{$currencies.$secondary_currency.decimals_separator|escape:javascript nofilter}',
                        'thousands_separator': '{$currencies.$secondary_currency.thousands_separator|escape:javascript nofilter}',
                        'decimals': '{$currencies.$secondary_currency.decimals|escape:javascript nofilter}',
                        'coefficient': '{$currencies.$secondary_currency.coefficient}'
                    }
                },
                default_editor: '{$settings.Appearance.default_wysiwyg_editor}',
                default_previewer: '{$settings.Appearance.default_image_previewer}',
                current_path: '{$config.current_path|escape:javascript nofilter}',
                current_location: '{$config.current_location|escape:javascript nofilter}',
                images_dir: '{$images_dir}',
                notice_displaying_time: {if $settings.Appearance.notice_displaying_time}{$settings.Appearance.notice_displaying_time}{else}0{/if},
                cart_language: '{$smarty.const.CART_LANGUAGE}',
                language_direction: '{$language_direction}',
                default_language: '{$smarty.const.DEFAULT_LANGUAGE}',
                cart_prices_w_taxes: {if ($settings.Appearance.cart_prices_w_taxes == 'Y')}true{else}false{/if},
                theme_name: '{$settings.theme_name|escape:javascript nofilter}',
                regexp: [],
                current_url: '{$config.current_url|fn_url|escape:javascript nofilter}',
                current_host: '{$config.current_host|escape:javascript nofilter}',
                init_context: '{$smarty.request.init_context|escape:javascript nofilter}'
            }); {if $live_editor_objects} $.extend(_, {
                live_editor_mode: true,
                live_editor_objects: {$live_editor_objects|json_encode nofilter} }); {/if} {if !$smarty.request.init_context} $(document).ready(function () {
                $.runCart('C');
            }); {/if} {if $config.tweaks.anti_csrf} _.security_hash = '{""|fn_generate_security_hash}'; {/if} }
        (Tygh, Tygh.$));
    </script>
{hook name="index:scripts"}
{/hook}

<script>
    //Freshdesk
    window.addEventListener ?
        window.addEventListener("load", initiateCall, !1) :
        window.attachEvent("load", initiateCall, !1);
    window.fcWidget.setExternalId(`{$smarty.session.auth.user_id}`);
    // To set user name
    window.fcWidget.user.setFirstName(`{$smarty.session.auth.firstname}`);
    // To set user email
    window.fcWidget.user.setEmail(`{$smarty.session.auth.email}`);
    // To set user properties
    window.fcWidget.user.setProperties({
        plan: "Estate",                 // meta property 1
        status: "Active"                // meta property 2
    });
</script>
{/scripts}
