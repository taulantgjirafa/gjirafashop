{strip}

{if $smarty.cookies.currency == 'EUR' && $al|fn_isAL}
    {if !$currency}
        {$value = ($value / $smarty.const.CURRENCY_CONVERSION_RATE)}
        {$primary_currency = $smarty.cookies.currency}
    {else}
        {$primary_currency = $currency}
    {/if}
{/if}

{if $settings.General.alternative_currency == "use_selected_and_alternative"}
    {$value|format_price:$currencies.$primary_currency:$span_id:$class:false:$live_editor_name:$live_editor_phrase nofilter}
    {if $secondary_currency != $primary_currency}
        {if $class}<span class="{$class}">{/if}
        (
        {if $class}</span>{/if}
        {$value|format_price:$currencies.$secondary_currency:$span_id:$class:true:$is_integer:$live_editor_name:$live_editor_phrase nofilter}
        {if $class}<span class="{$class}">{/if}
        )
        {if $class}</span>{/if}
    {/if}
{else}
    {if $order_info}
        {if $order_info.company_id != 1}
            {$primary_currency = 'ALL'}

            {if $smarty.cookies.currency == 'EUR' && $al|fn_isAL}
                {$primary_currency = $smarty.cookies.currency}
            {/if}
        {/if}
    {/if}

    {if $currency}
        {$primary_currency = $currency}
    {/if}

    {$value|format_price:$currencies.$primary_currency:$span_id:$class:true:$live_editor_name:$live_editor_phrase nofilter}
{/if}
{/strip}