<div class="load_more_category" style="text-align: center;">
    {if $button_visible}
        <a href="" class="ty-btn__primary ty-btn" id="load_more_infinite">{$button_text}</a>
    {/if}
    <div id="is_loader"></div>
    <div id="no_results" style="display:none;text-align:center;padding:30px 0px;font-size:14px;">
        <span>Fundi i rezultateve</span>
    </div>
</div>