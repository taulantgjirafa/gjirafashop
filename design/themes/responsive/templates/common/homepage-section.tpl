{assign var="is_al" value=$al|fn_isAL}
{assign var="currency_symbol" value=fn_get_company_currency_symbol()}

<div class="hs-section {if $msi}hs-msi{/if}">
    <div class="hs-title flexing flex-h-between flex-v-center">
        <h2>{$name}</h2>
        <h4><a href="{if $msi}{"pages.msi-landing"|fn_url}{else}{"categories.view&category_id=2161"|fn_url}{/if}{if $msi}?utm_content=homepage_msi_section{else}?utm_content=homepage_recommended_section{/if}">Shiko të gjitha</a></h4>
    </div>
    <div class="hs-wrapper">
        <div class="arrows">
            <span class="hs-left" style="visibility: hidden;">
                <svg><use xlink:href="#svg-left"></use></svg>
            </span>
            <span class="hs-right">
                <svg><use xlink:href="#svg-right"></use></svg>
            </span>
        </div>
        <div class="hs-scroll">
            {foreach from=$section_products item=product}

            {if $smarty.session.auth.vip && !$is_al && $product.vip_price != null}
                {if $product.old_price < $product.vip_price}
                    {$product.old_price = $product.price}
                {/if}
                {if $product.price > $product.vip_price}
                    {$product.price = $product.vip_price}
                {/if}
            {/if}

            <div class="hs-item" {if $is_al && !$product.price_al} style="display: none;" {/if} style="position: relative;">

                {if $is_al}
                    {if $product.old_price_al}
                        {$product.discount = $product.old_price_al}
                        {$product.discount_prc_al = (((($product.old_price_al|floatval) - $product.price_al) / $product.old_price_al|floatval) * 100)|string_format:"%.0f"}
                    {/if}
                    {if isset($product.old_price_al) && $product.old_price_al > $product.price_al}
                        {$show_discount_label = true}
                        {if $product.old_price_al - $product.price_al > 125}
                            {$discount_greater_than = true}
                        {/if}
                    {else}
                        {$show_discount_label = false}
                    {/if}
                {else}
                    {if $product.old_price && !$product.list_price}
                        {$product.discount = $product.old_price}
                        {$product.discount_prc = (((($product.old_price) - $product.price) / $product.old_price) * 100)|string_format:"%.0f"}
                    {elseif $product.list_price && $product.list_price > $product.price}
                        {$product.discount = $product.list_price}
                        {$product.discount_prc = (((($product.list_price) - $product.price) / $product.list_price) * 100)|string_format:"%.0f"}
                        {$product.old_price = $product.list_price}
                    {/if}
                    {if $product.old_price > $product.price }
                        {$show_discount_label = true}
                    {/if}
                {/if}

                {if $show_discount_label  && $product.discount_prc > 0}
                <span class="ty-discount-label price-drop_label">
                    <span class="ty-discount-label__item">
                        <span class="ty-discount-label__value">
                        {* -{if $product.discount_prc}{$product.discount_prc}{else}{$product.list_discount_prc|string_format:"%d"}{/if}% *}
                        -{if $product.discount_prc}{$product.discount_prc}{/if}%
                        </span>
                    </span>
                </span>
                {/if}

                <a href="{"products.view&product_id={$product.product_id}"|fn_url}{if $msi}?utm_content=homepage_msi_section{else}?utm_content=homepage_recommended_section{/if}" style="text-decoration: none;" class="hs-link">
                    <div class="hs-image flexing flex-h-center flex-v-center">
                        <img src="https://hhstsyoejx.gjirafa.net/gj50/img/{$product.product_id}/thumb/0.jpg" alt="">
                    </div>
                    <div class="hs-in-content">
                        <h3>{$product.product}</h3>
                        <h2>
                            {if $is_al}
                                {include file="common/price.tpl" value=$product.price_al}
                            {else}
                                {include file="common/price.tpl" value=$product.price}
                            {/if}
                        {if $product.old_price && $product.old_price > $product.price && !$is_al}
                            <span class="text-muted discount">
                                {include file="common/price.tpl" value=$product.old_price}
                            </span>
                        {elseif $product.old_price_al && $product.old_price_al > $product.price_al && $is_al}
                            <span class="text-muted discount">
                                {include file="common/price.tpl" value=$product.old_price_al}
                            </span>
                        {/if}
                        </h2>
                    </div>
                </a>
            </div>
            {/foreach}
            {if $msi}
            <div class="hs-item msi-item">
                <a href="{"pages.msi-landing"|fn_url}?utm_content=homepage_msi_section">
                    <div class="hs-image">
                        <img src="images/dragonlogo.png" alt="">
                    </div>
                    <div class="hs-in-content">
                        <h3>Shiko të gjitha produktet MSI</h3>
                    </div>
                </a>
            </div>
            {else}
            {$recommended_category = 2161}
            {if $is_al}
                {$recommended_category = 3233}
            {/if}
            <div class="hs-item">
                <a href="{"categories.view&category_id={$recommended_category}"|fn_url}?utm_content=homepage_recommended_section">
                    <div class="hs-image">
                        <img src="images/icons/recommended_product.svg" alt="" style="width: 150px; margin: 20px 10px 10px 10px; animation: dragon linear 4s infinite; will-change: transform;">
                    </div>
                    <div class="hs-in-content">
                        <h3 style="margin-top: 44px; text-transform: uppercase; text-align: center; font-size: 12px;">Shiko të gjitha produktet e rekomanduara</h3>
                    </div>
                </a>
            </div>
            {/if}
        </div>
    </div>
</div>