<div class="ty-product-coming-soon">
    {* {assign var="date" value=$avail_date|date_format:$settings.Appearance.date_format} *}
    {* {if $add_to_cart == "N"}{__("product_coming_soon", ["[avail_date]" => $date])}{else}{__("product_coming_soon_add", ["[avail_date]" => $date])} <span style="background: #ffc052; color: #fff; padding: 5px;">{$date}</span>{/if} *}
    {__('product_coming_soon_avail')}: <span class="ty-product-coming-soon__date">{$avail_date|date_format}</span>
    <div class="ty-product-coming-soon__timer" id="coming_soon_timer"></div>
    {* {if $product.amount > 0}
        <p>😃 {__('product_coming_soon_stock', ['[stock]' => $product.amount])}</p>
    {/if} *}
</div>

<script>
    var futureDate = {$avail_date} * 1000;
	var iteration = 0;
    var timer = setInterval(comingSoonTimer, 1000);

    function comingSoonTimer() {
        iteration++;

        var now = new Date().getTime();
        var distance = futureDate - now;
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        if (distance > 0) {
            document.getElementById('coming_soon_timer').innerHTML = '<div style="margin: 0 3px;"><span class="cs-timer-el">' + days + '</span><div class="cs-timer-label">Ditë</div></div> : <div style="margin: 0 3px;"><span class="cs-timer-el">' + hours + '</span><div class="cs-timer-label">Orë</div></div> : <div style="margin: 0 3px;"><span class="cs-timer-el">' + minutes + '</span><div class="cs-timer-label">Min.</div></div> : <div style="margin: 0 3px;"><span class="cs-timer-el">' + seconds + '</span><div class="cs-timer-label">Sek.</div></div>';
        } else {
            clearInterval(timer);
        }
    }

    document.getElementById('coming_soon_timer').classList.add('ty-product-coming-soon__active');
</script>