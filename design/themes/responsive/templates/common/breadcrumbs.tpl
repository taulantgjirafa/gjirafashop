<div id="breadcrumbs_{$block.block_id}">

{if $breadcrumbs && $breadcrumbs|@sizeof > 1}
	{*<a id="back-btn-breadcrumb" href="/"><i class="ty-icon-left-open-thin"></i> Kthehu tek ballina</a>
	*}
	{include file="common/view_tools.tpl"}

    <div class="ty-breadcrumbs clearfix">
        {strip}
            {foreach from=$breadcrumbs item="bc" name="bcn" key="key"}
                {if $key != "0"}
                    <span class="ty-breadcrumbs__slash">/</span>
                {/if}
                {if $bc.link}
                    <a href="{$bc.link|fn_url}" class="ty-breadcrumbs__a{if $additional_class} {$additional_class}{/if} gjanout-category"{if $bc.nofollow} rel="nofollow"{/if}>{$bc.title|strip_tags|escape:"html" nofilter}</a>
                {else}
                    <span class="ty-breadcrumbs__current gjanout-category">{$bc.title|strip_tags|escape:"html" nofilter}</span>
                {/if}
            {/foreach}
            {include file="common/view_tools.tpl"}
        {/strip}
    </div>
    <script type="application/ld+json">{ "@context": "http://schema.org", "@type": "BreadcrumbList", "itemListElement": [ {foreach from=$breadcrumbs item="bc" name="bcn" key="key"} { "@type": "ListItem", "position": {$key}, "name": "{$bc.title|strip_tags|escape:"html" nofilter}", "item": "{$bc.link|fn_url}" }{if !$smarty.foreach.bcn.last},{/if} {/foreach} ] } </script>
{/if}
    <!--breadcrumbs_{$block.block_id}--></div>