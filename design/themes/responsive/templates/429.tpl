<!DOCTYPE html>
<html>
<head>
    <title>Gjirafa50 - Too many requests</title>
    <style type="text/css">
    .ty-exception {
        font: normal 13px Arial, Tahoma, Helvetica, sans-serif;
        position: absolute;
        top: 10%;
        left: 50%;
        margin: 0 0 0 -350px;
        width: 960px;
    }
    .ty-exception h2 {
        padding-top: 25px;
    }
    .ty-exception p {
        margin: 0;
        padding: 0px 0px 30px 0px;
        color: #808080;
        font-size: 110%;
    }
    .ty-exception-code {
        float: left;
        display: inline-block;
        margin-right: 30px;
        padding: 50px 70px 90px;
        line-height: 70px;
        background-image: url(images/appicon/50-144px.png);
        background-repeat: no-repeat;

    }
  </style>
</head>
<body>
    <div class="ty-exception">
        <span class="ty-exception-code"></span>
    <h2>Keni bërë shumë kërkesa, ju lutem provoni më vonë</h2>
    <p>Kodi: 429</p>
    </div>
</body>
</html>