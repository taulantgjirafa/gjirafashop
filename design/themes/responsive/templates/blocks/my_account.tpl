{** block-description:my_account **}
{*<span class="ty-account-info__title-txt" {live_edit name="block:name:{$block.block_id}"}>{$title}</span>*}
{include file="common/svgicons.tpl"}
{capture name="title"}
    <a class="ty-account-info__title" href="{"profiles.update"|fn_url}">
        {*<i class="ty-icon-user"></i>&nbsp;*}
        <span class="user-header">
            <svg><use xlink:href="#svg-user"></use></svg>
        </span>
        {if $auth.user_id}
            {if $user_info.firstname || $user_info.lastname}
                <span class="ty-account-info__title-txt">{$user_info.firstname} {$user_info.lastname}</span>
            {else}
                <span class="ty-account-info__title-txt">{$user_info.email}</span>
            {/if}
            <i class="ty-icon-down-micro ty-account-info__user-arrow"></i>
        {elseif $user_data.firstname || $user_data.lastname}
            <span class="ty-account-info__title-txt">{$user_data.firstname} {$user_data.lastname}</span>
            <i class="ty-icon-down-micro ty-account-info__user-arrow"></i>
        {elseif $user_data.email}
            <span class="ty-account-info__title-txt">{$user_data.email}</span>
            <i class="ty-icon-down-micro ty-account-info__user-arrow"></i>
        {else}
            <span>{__("sign_in")}</span>
        {/if}


    </a>
{/capture}

<div id="account_info_{$block.snapping_id}">
    {assign var="return_current_url" value=$config.current_full_url|addslashes|escape:url}
    <ul class="ty-account-info">
        {hook name="profiles:my_account_menu"}
        {if $auth.user_id}
            <li class="ty-account-info__item ty-dropdown-box__item responsive-useracc" style="background:transparent;">
                {__('signed_in_as')},
                {if $user_info.firstname || $user_info.lastname}
                    <span>{$user_info.firstname} {$user_info.lastname}</span>
                {else}
                    <span>{$user_info.email}</span>
                {/if}
            </li>
            <li class="ty-account-info__item ty-dropdown-box__item"><a class="ty-account-info__a underlined"
                                                                       href="{"profiles.update"|fn_url}"
                                                                       rel="nofollow">{__("profile_details")}</a></li>
            {if $settings.General.enable_edp == "Y"}
                <li class="ty-account-info__item ty-dropdown-box__item"><a class="ty-account-info__a underlined"
                                                                           href="{"orders.downloads"|fn_url}"
                                                                           rel="nofollow">{__("downloads")}</a></li>
            {/if}
        {/if}
            <li class="ty-account-info__item ty-dropdown-box__item"><a class="ty-account-info__a underlined"
                                                                       href="{"orders.search"|fn_url}"
                                                                       rel="nofollow">{__("orders")}</a></li>
        {if $settings.General.enable_compare_products == 'Y'}
            {assign var="compared_products" value=""|fn_get_comparison_products}
            <li class="ty-account-info__item ty-dropdown-box__item"><a class="ty-account-info__a underlined"
                                                                       href="{"product_features.compare"|fn_url}"
                                                                       rel="nofollow">{__("view_comparison_list")}{if $compared_products} ({$compared_products|count}){/if}</a>
            </li>
        {/if}
        {/hook}
    </ul>

    {if $settings.Appearance.display_track_orders == 'Y'}
        <div class="ty-account-info__orders updates-wrapper track-orders" id="track_orders_block_{$block.snapping_id}">
            <form action="{""|fn_url}" method="POST" class="cm-ajax cm-post cm-ajax-full-render"
                  name="track_order_quick">
                <input type="hidden" name="result_ids" value="track_orders_block_*"/>
                <input type="hidden" name="return_url"
                       value="{$smarty.request.return_url|default:$config.current_url}"/>

                <div class="ty-account-info__orders-txt">{__("track_my_order")}</div>

                <div class="ty-account-info__orders-input ty-control-group ty-input-append">
                    <label for="track_order_item{$block.snapping_id}"
                           class="cm-required hidden">{__("track_my_order")}</label>
                    <input type="text" size="20" class="ty-input-text cm-hint" id="track_order_item{$block.snapping_id}"
                           name="track_data" value="{__("order_id")}{if !$auth.user_id}/{__("email")}{/if}"/>
                    {include file="buttons/go.tpl" but_name="orders.track_request" alt=__("go")}
                    {include file="common/image_verification.tpl" option="track_orders" align="left" sidebox=true}
                </div>
            </form>
            <!--track_orders_block_{$block.snapping_id}--></div>
    {/if}

    <div class="ty-account-info__buttons buttons-container">
{*        {if $smarty.server.REQUEST_URI|strpos:"?"}*}
{*            {assign var="redirect_url" value=$smarty.server.REQUEST_URI|strpos:"?"}*}
{*            {$redirect_url = $smarty.server.REQUEST_URI|substr:0:$redirect_url}*}
{*        {else}*}
{*            {assign var="redirect_url" value=$smarty.server.REQUEST_URI}*}
{*        {/if}*}
{*        {if $auth.user_id}*}
{*            <a href="{"sso.logout?redirect={$smarty.server.QUERY_STRING}"|fn_url}"*}
{*               rel="nofollow" class="ty-btn ty-btn__primary">{__("sign_out")}</a>*}
{*        {else}*}
{*            <a href="{$config.sso.provider_url}"*}
{*               class="ty-btn ty-btn__primary" rel="nofollow">{__("sign_in")}</a>*}
{*            <a href="{$config.sso.register_url}"*}
{*               class="ty-btn ty-btn__secondary">{__("register")}</a>*}
{*        {/if}*}

            {if $auth.user_id}
                <a href="{"auth.logout?redirect_url=`$return_current_url`"|fn_url}" rel="nofollow" class="ty-btn ty-btn__primary">{__("sign_out")}</a>
            {else}
                <a href="{if $runtime.controller == "auth" && $runtime.mode == "login_form"}{$config.current_url|fn_url}{else}{"auth.login_form?return_url=`$return_current_url`"|fn_url}{/if}" {if $settings.Security.secure_storefront != "partial"} data-ca-target-id="login_block{$block.snapping_id}" class="cm-dialog-opener cm-dialog-auto-size ty-btn ty-btn__secondary"{else} class="ty-btn ty-btn__primary"{/if} rel="nofollow">{__("sign_in")}</a><a href="{"profiles.add"|fn_url}" rel="nofollow" class="ty-btn ty-btn__primary">{__("register")}</a>
                {if $settings.Security.secure_storefront != "partial"}
                    <div  id="login_block{$block.snapping_id}" class="hidden" title="{__("sign_in")}">
                        <div class="ty-login-popup">
                            {include file="views/auth/login_form.tpl" style="popup" id="popup`$block.snapping_id`"}
                    </div>
                    </div>
                {/if}
            {/if}
    </div>


    <!--account_info_{$block.snapping_id}--></div>
