{script src="js/tygh/exceptions.js"}

{hook name="products:view_main_info"}
{hook name="index:head_scripts"}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.5.0/css/flag-icon.min.css">
{/hook}

{if $product}
    {assign var="obj_id" value=$product.product_id}
    {assign var="form_open" value="form_open_`$obj_id`"}
    {assign var="old_price" value="old_price_`$obj_id`"}
    {assign var="price" value="price_`$obj_id`"}
    {assign var="clean_price" value="clean_price_`$obj_id`"}
    {assign var="list_discount" value="list_discount_`$obj_id`"}
    {assign var="discount_label" value="discount_label_`$obj_id`"}
    {assign var="disable_flex_categories" value=','|explode:{__('disable_flex')}}
    {assign var="check_category" value=$disable_flex_categories|array_intersect:$product.category_ids}
    {include file="common/product_data.tpl" product=$product but_role="big" but_text=__("add_to_cart")}
    {include file="views/products/components/product_bigpicture_right.tpl" product=$product}
    {assign var="price_info_section" value="price_info_section_`$obj_id`"}
    {assign var="add_to_cart_section" value="add_to_cart_section_`$obj_id`"}
    {assign var="how_to_video_section" value="how_to_video_`$obj_id`"}
    {assign var="sticky_add_to_cart_section" value="sticky_add_to_cart_`$obj_id`"}
    {assign var="vip_member" value="13"|in_array:$smarty.session.auth.usergroup_ids}
    {*{if count($check_category) > 0}*}
    {*{$product.full_description = "`$product.full_description` <br/><br/> <strong>Shënim: Ky produkt nuk ka garancion, por reklamacioni është valid për 24h.</strong>"}*}
    {*{/if}*}
    <div class="ty-product-bigpicture clearfix {if $vip_member}vip_member{/if}">

        <div class="ty-product-bigpicture__left product-wrapper">
            <div class="span6 product-image">
                {hook name="products:image_wrap"}
                {if !$no_images}
                    <div class="ty-product-bigpicture__img {if $product.image_pairs|@count < 1} ty-product-bigpicture__no-thumbs{/if} cm-reload-{$product.product_id} {if $settings.Appearance.thumbnails_gallery == "Y"}ty-product-bigpicture__as-gallery{else}ty-product-bigpicture__as-thumbs{/if}"
                         id="product_images_{$product.product_id}_update">


                        {include file="views/products/components/product_images.tpl" product=$product show_detailed_link="Y" thumbnails_size=55 }

                        {if $product.amount == 0}
                            <div class="sold-out">
                                <h1>E SHITUR</h1>
                            </div>
                        {/if}

                    </div>
                {/if}
                {/hook}
            </div>
            <div class="span6 product-details">
                {hook name="products:main_info_title"}
                {if !$hide_title}
                    <h1 class="ty-product-block-title" {live_edit name="product:product:{$product.product_id}"}>{$product.product nofilter}</h1>
                {/if}
                {/hook}

                {if $smarty.const.IS_MOBILE}
                    <div id="gjrecommend">
                        {if $vip_member}
                            <div class="bbox crown_wrapper"><img src="images/icons/crown.png" class="crown">VIP</div>
                        {elseif 991|in_array:$product.category_ids && !$is_AL}
                            <img src="images/icons/outlet.svg" style="width: 103px;margin-top: 10px;"
                                 alt="Ulje çmimi për shkak të rregullave të reja doganore!">
                        {elseif (($product.score * 1) == 5 || $product.score > 95)}
                            <img src="images/icons/recommended_product.svg" style="width: 103px;" alt="Gjirafa50 rekomandon këtë produkt!">
                        {/if}

                    </div>
                {/if}
                {if $smarty.const.IS_MOBILE && $smarty.capture.$price_info_section|trim}{$smarty.capture.$price_info_section nofilter}{/if}



                <div class="product_details-specs {if $product.amount == 0}  out-of-stock {/if}">
                    <div class="ty-product-feature"> <span class="ty-product-feature__label bbox">Kodi i produktit:</span><div class="ty-product-feature__value bbox">{$product.product_code}</div> </div>
                    {foreach from=$product.header_features key=i item=product_feature name=feature}
                        {if $smarty.foreach.feature.index == 5}
                            {break}
                        {/if}
                        <div class="ty-product-feature"> <span class="ty-product-feature__label bbox" data-feature="{$product_feature.description}">{$product_feature.description}:</span><div class="ty-product-feature__value bbox">{if $product_feature.variant == "Po"}
                                    <i class="ty-compare-checkbox__icon ty-icon-ok"></i>
                                {else}
                                    {$product_feature.variant}
                                {/if}</div> </div>
                    {/foreach}
                    <ul>
                        {if $product.header_features}
                            <li class="product_details-link"><a class="cm-external-click" data-ca-scroll="features_list"
                                                                data-ca-external-click-id="features">Shiko të gjitha
                                    specifikat</a></li>
                        {/if}
{*                        <div class="yg-plus">*}
{*                            <h3>Plus, you get it from us</h3>*}
{*                            <ul>*}
{*                                <li>*}
{*                                    <span class="yg-img">*}
{*                                        <img  src="https://hhstsyoejx.gjirafa.net/gj50/img/56357/thumb/0.jpg" />*}
{*                                    </span>*}
{*                                    <span>Service readiness - Improved PC and NTB service for FREE</span>*}
{*                                </li>*}
{*                                   <li>*}
{*                                    <span class="yg-img">*}
{*                                        <img  src="https://hhstsyoejx.gjirafa.net/gj50/img/56357/thumb/0.jpg" />*}
{*                                    </span>*}
{*                                    <span>Service readiness - Improved PC and NTB service for FREE</span>*}
{*                                </li>*}
{*                                   <li>*}
{*                                    <span class="yg-img">*}
{*                                        <img  src="https://hhstsyoejx.gjirafa.net/gj50/img/56357/thumb/0.jpg" />*}
{*                                    </span>*}
{*                                    <span>Service readiness - Improved PC and NTB service for FREE</span>*}
{*                                </li>*}
{*                            </ul>*}
{*                        </div>*}
                        <li class="separator"></li>
                        <li>
                            <span class="ty-product-feature__label bbox">Çmimi i transportit:</span><strong class="product-details_primary">{*<img src="images/delivery-truck.svg" alt="">*}
                                {if $a|fn_isAL}
{*                                    200 Lekë*}
                                    {include file="common/price.tpl" value=200}
                                {else}
                                    FALAS
                                {/if}</strong>
                        </li>
                        {*{if !$a|fn_isAL}*}
                        {*<li><span class="ty-product-feature__label bbox">Kthimet:</span><strong class="product-details_primary">FALAS*</strong></li>*}
                        {*{/if}*}
                        {*<li>*}
                        {*<span class="ty-product-feature__label bbox">Koha e arritjes:</span><strong class="product-details_primary">{$product|fn_get_shipping_days:$product} DITË</strong>*}
                        {*</li>*}
                        <li><span  class="ty-product-feature__label bbox">Mundësitë e pagesave:</span><br>
                            <ul class="product-details_payments">
                                <li><img src="images/payment/payments_all.png" alt="Mundësitë e pagesave"></li>
                            </ul>
                        </li>
                    </ul>

                    {if $smarty.const.IS_MOBILE && $smarty.capture.$add_to_cart_section|trim}{$smarty.capture.$add_to_cart_section nofilter}{/if}

                    {$smarty.capture.$how_to_video_section nofilter}

                </div>

                {if $smarty.const.IS_MOBILE}
                    {hook name="products:extra_actions"}
                    {/hook}
                {/if}
            </div>
        </div>
        {if !$smarty.const.IS_MOBILE && ($smarty.const.DEVICE_TYPE != 'mobile' || $smarty.const.DEVICE_TYPE != 'tablet')}
        <div class="ty-product-bigpicture__right">
            {if $vip_member}
                <div class="bbox crown_wrapper">
                    <img src="images/icons/crown.png" class="crown">
                    <div>
                        Çmim VIP
                        <i class="ty-icon-help-circle ty-hand tooltip-top" style="font-size: 17px;position: relative;top: 4px;"></i>
                        <div class="tltip bbox">Dhurata jonë për ju! VIP çmimet janë ekskluzive të cilat mund të i shihni vetëm ju.</div>
                    </div>
                </div>
            {/if}
            <div class="product-data {if $product.amount == 0}  out-of-stock {/if}" >

                {if !$smarty.const.IS_MOBILE && ($smarty.const.DEVICE_TYPE != 'mobile' || $smarty.const.DEVICE_TYPE != 'tablet')}
                    <div id="gjrecommend">
                        {if 991|in_array:$product.category_ids && !$is_AL}
                            <img src="images/icons/outlet.svg" style="width: 103px;margin: 10px 10px;"
                                 alt="[OUTLET] PRODUKTI NUK ËSHTË I PËRDORUR, POR VJEN ME PAKO TË HAPUR">
                        {elseif (($product.score * 1) == 5 || $product.score > 95)}
                            <img src="images/icons/recommended_product.svg" style="width: 103px;" alt="Gjirafa50 rekomandon këtë produkt!">
                        {/if}
                    </div>
                {/if}

                <div class="{if $smarty.capture.$old_price|trim || $smarty.capture.$clean_price|trim || $smarty.capture.$list_discount|trim}prices-container {/if}price-wrap">
                    {if $smarty.capture.$price_info_section|trim}{$smarty.capture.$price_info_section nofilter}{/if}

                    {if $smarty.capture.$add_to_cart_section|trim}{$smarty.capture.$add_to_cart_section nofilter}{/if}
                </div>

                {hook name="products:extra_actions"}
                {/hook}

                {/if}

                {if !$smarty.const.IS_MOBILE && ($smarty.const.DEVICE_TYPE != 'mobile' || $smarty.const.DEVICE_TYPE != 'tablet')}
                <div class="ty-product-bigpicture__sidebar-bottom">
                    {include file="views/products/components/product_share_buttons.tpl" view='bigpicture' product_id=$product.product_id}
                </div>

                {hook name="products:product_detail_bottom"}
                {/hook}

                {if $show_product_tabs}
                    {include file="views/tabs/components/product_popup_tabs.tpl"}
                    {$smarty.capture.popupsbox_content nofilter}
                {/if}
            </div>
        </div>
        {/if}
        <div class="clearfix"></div>

        {if $show_product_tabs}
            <div class="ty-product_all-details all_details-wrapper">
                <ul class="product-tabs">
                    <li class="tab-det-btns desc active">{__('description')}</li>
                    <li class="tab-det-btns details">{__('details')|capitalize}</li>
                    <li class="tab-det-btns review">{__('discussion_title_category')}</li>
                    <li class="tab-det-btns similar">{__('related_products')}</li>
                </ul>
                <div class="tab-wrapper">
                    {include file="views/tabs/components/product_tabs.tpl"}

                    {if $blocks.$tabs_block_id.properties.wrapper}
                        {include file=$blocks.$tabs_block_id.properties.wrapper content=$smarty.capture.tabsbox_content title=$blocks.$tabs_block_id.description}
                    {else}
                        {$smarty.capture.tabsbox_content nofilter}
                    {/if}
                </div>
            </div>
        {/if}
    </div>


    {include file="views/products/components/recommended.tpl" recommendations=$recommendations}
    {if $recently_viewed_products}
        {include file="views/products/components/recently_viewed_products.tpl" products=$recently_viewed_products}
    {/if}
{/if}

{/hook}


<input type="hidden" class="product_id" value="{$product.product_id}">
<input type="hidden" class="gja_view_data" value="{$gj_analytics_views|json_encode}">
<input type="hidden" class="gja_view_date" value="{$gj_analytics_views['timestamp']}">
{if $smarty.capture.hide_form_changed == "Y"}
    {assign var="hide_form" value=$smarty.capture.orig_val_hide_form}
{/if}

{$smarty.capture.$sticky_add_to_cart_section nofilter}

{literal}
    <script>
        $('#customs_sale').length && $('#c_sale_val').text($('#content_description #customs_sale').val() + '%') && $('#begamer').show();
    </script>
    <script>
        var howToOpen = false;
        $('.product_howto').click(function () {
            $('.product_howto-video').addClass('show');
            $('.product_howto-video-inner').append(' <iframe id="howto-iframe" src="https://video.gjirafa.com/embed/si-te-porosisim-permes-kompjuterit-ne-gjirafa50" frameborder="0" allowfullscreen></iframe>')
            $('html').addClass('dialog-is-open');
            howToOpen = true;
        })
        $('body').click(function (e) {
            if (howToOpen && !$(e.target).closest('.product_howto-video iframe').length && !$(e.target).closest('.product_howto-play').length) {
                $('html').removeClass('dialog-is-open');
                $('.product_howto-video').removeClass('show');
                $('#howto-iframe').remove();
            }
        });
    </script>
{/literal}

<script>
if(window.outerWidth > 768){
    var descriptionBtn = document.querySelector('.desc');
    var detailsBtn = document.querySelector('.details');
    var reviewBtn = document.querySelector('.review');
    var similarBtn = document.querySelector('.similar');


    var description = document.querySelectorAll('#content_features .span6')[0];
    var review = document.querySelectorAll('#content_features .span6')[1];
    var details = document.querySelectorAll('#content_features .span6')[2];

    var allDetBtn = document.querySelectorAll('.tab-det-btns');

    review.style.display = "none";
    details.style.display = "none";

    $(".similar").click(function() {
        $([document.documentElement, document.body]).animate({
            scrollTop: $(".more-products").offset().top
        }, 1000);
    });

    descriptionBtn.onclick = function(){
        review.style.display = "none";
        details.style.display = "none";
        description.style.display = "block";
        for(var i = 0; i < allDetBtn.length; i++) {
            allDetBtn[i].classList.remove('active');
        }
        this.classList.add('active')
    }
    reviewBtn.onclick = function(){
        review.style.display = "block";
        details.style.display = "none";
        description.style.display = "none";
        for(var i = 0; i < allDetBtn.length; i++) {
            allDetBtn[i].classList.remove('active');
        }
        this.classList.add('active')
    }
    detailsBtn.onclick = function(){
        review.style.display = "none";
        details.style.display = "block";
        description.style.display = "none";
        for(var i = 0; i < allDetBtn.length; i++) {
            allDetBtn[i].classList.remove('active');
        }
        this.classList.add('active')
    }
}
else{
    var mobDetButtons = document.querySelectorAll('#discussion, #description, #features');
}

    document.getElementById('buy_now_form').onsubmit = function(e) {
        e.preventDefault();

        let amount = document.getElementsByClassName('cm-amount')[0].value;

        if (amount > 1) {
            document.getElementById('buy_now_amount').setAttribute('value', amount);
        }

        {if count($check_category) == 0}
        let gjflexyes = document.querySelector('input[id^="gjflexyes_"]').checked;
        let gjflexno = document.querySelector('input[id^="gjflexno_"]').checked;

        if (gjflexyes === false && gjflexno === false ) {
            document.getElementById('with_gjflex_product_view').classList.add('flash');
            document.getElementById('with_gjflex_product_view').style.border = '2px solid #d7461b';

            {literal}
            setTimeout(function() {document.getElementById('with_gjflex_product_view').classList.remove('flash');}, 500);
            {/literal}
        } else {
            if (gjflexyes === true) {
                document.getElementById('buy_now_gjflex').setAttribute('value', 1);
            } else if(gjflexyes === false) {
                document.getElementById('buy_now_gjflex').setAttribute('value', 0);
            }

            gjdmp.tr("Checkout", {
                step: 'viewproduct',
                id: `{$product.product_id}`,
                user_id: $('#store_user').val(),
                name: `{$product.product}`,
                value: `{$product.price}`,
                flex: gjflexyes ? 'flex' : 'noflex',
                currency: CURRENCY
            });

            document.getElementById('ajax_overlay').style.display = 'block';
            document.getElementById('ajax_loading_box').style.display = 'block';
            this.submit();
        }
        {else}
        document.getElementById('ajax_overlay').style.display = 'block';
        document.getElementById('ajax_loading_box').style.display = 'block';
        this.submit();
        {/if}
    };

    // Tracking variables
    const product_id = `{$product.product_id}`;
    // const user_id = $('#store_user').val();
    const name = `{$product.product}`;
    const price = `{$product.price}`;
    const sku = `{$ean}`;

    var more_images_clicked = false;
    //Product more images - click
    $(document).on('click', '.ty-product-thumbnails .owl-item, .mfp-arrow', function(e) {
        if (!more_images_clicked) {
            gjdmp.tr("MoreImages", {
                id: product_id,
                user_id: user_id,
                name: name,
                type: 'product',
                currency: CURRENCY,
            });
        }

        more_images_clicked = true;
        // e.preventDefault();
    });

    //Product arrive - click
    $(document).on('click', '.product-delivery-time', function(e) {
        gjdmp.tr("ViewContent", {
            type: "product-arrive",
            id: product_id,
            user_id: user_id,
            name: name,
            value: price,
            sku: sku
        });
    });

    //How to order - click
    $(document).on('click', '.product_howto-play', function(e) {
        gjdmp.tr("ViewContent", {
            type: "how-to-order",
            id: product_id,
            user_id: user_id,
            name: name,
            value: price,
            sku: sku
        });
    });

    //Review added - click
    $(document).on('click', 'button[name ="dispatch[discussion.add]"]', function (e) {
        gjdmp.tr("Rate", {
            id: product_id,
            user_id: user_id,
            name: name,
            value: price,
            sku: sku,
            rating: $('.ty-rating__check:checked').val()
        });
    })
</script>

<script>
    {literal}
        $('.cm-external-click').click(function(){
            $([document.documentElement, document.body]).animate({
                scrollTop: $(".product-tabs").offset().top
            }, 500);
            $('.details').click()
        })
    {/literal}
</script>

{capture name="mainbox_title"}{assign var="details_page" value=true}{/capture}

<img style="display:none;" src="/index.php?dispatch=products.pv_gjanalytics&gja_view_data={$gj_analytics_views|json_encode}" alt="">