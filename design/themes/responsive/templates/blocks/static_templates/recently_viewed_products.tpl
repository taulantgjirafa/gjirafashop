{nocache}
{assign var="products" value=$p|fn_get_recently_viewed_products}

{include file="views/products/components/recently_viewed_products.tpl" products=$products}
{/nocache}