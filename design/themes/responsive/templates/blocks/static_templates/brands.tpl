<div class="brands-list ">
    <div class="clearfix brands-list_inner">

        <div class="brands-list__item">
            <a href="{"products.search&q=apple&utm_source=Gjirafa50&utm_medium=footer&utm_campaign=Brands%20in%20footer"|fn_url}">
                <img src="https://hhstsyoejx.gjirafa.net/gj50/brand_images/apple.png" alt="Blej produkte Apple në Gjirafa50" style="opacity: .6;">
            </a>
        </div>

        <div class="brands-list__item">
            <a href="{"pages.samsung-main"|fn_url}">
                <img src="https://hhstsyoejx.gjirafa.net/gj50/brand_images/samsung.png" alt="Blej produkte Samsung në Gjirafa50" style="opacity: .6;">
            </a>
        </div>

        <div class="brands-list__item">
            <a href="{"products.search&q=msi&utm_source=Gjirafa50&utm_medium=footer&utm_campaign=Brands+in+footer"|fn_url}">
                <img src="https://hhstsyoejx.gjirafa.net/gj50/brand_images/msi.png" alt="Blej produkte MSI në Gjirafa50">
            </a>
        </div>

        <div class="brands-list__item">
            <a href="{"products.search&q=steelseries&utm_source=Gjirafa50&utm_medium=footer&utm_campaign=Brands+in+footer"|fn_url}">
                <img src="https://hhstsyoejx.gjirafa.net/gj50/brand_images/steelseries.png" alt="Blej produkte SteelSeries në Gjirafa50" style="opacity: .6;">
            </a>
        </div>

        <div class="brands-list__item">
            <a href="{"products.search&q=lenovo&utm_source=Gjirafa50&utm_medium=footer&utm_campaign=Brands+in+footer"|fn_url}">
                <img src="https://hhstsyoejx.gjirafa.net/gj50/brand_images/lenovo.png" alt="Blej produkte Lenovo në Gjirafa50">
            </a>
        </div>

        <div class="brands-list__item">
            <a href="{"products.search&q=dji&utm_source=Gjirafa50&utm_medium=footer&utm_campaign=Brands+in+footer"|fn_url}">
                <img src="https://hhstsyoejx.gjirafa.net/gj50/brand_images/dji.png" alt="Blej produkte DJI në Gjirafa50">
            </a>
        </div>

    </div>
</div>