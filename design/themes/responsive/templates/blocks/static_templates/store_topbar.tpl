<style>
	.safe-b{
		background:#333;
		color:#fff;
		box-sizing:border-box;
		padding:6px 10px;
		font-weight:600;
	}
	.safe-b svg{
		width:30px;
		height:30px;
		margin-right:10px;
	}

    .topbar-order-search {
        all: unset;
        cursor: pointer;
        color: #fff;
    }

    .topbar-order-search:hover {
        color: #F04E25;
        text-decoration: none;
    }
</style>
{if $smarty.const.IS_MOBILE}
    {if $al|fn_isAL}
        <div class="safe-b flexing flex-h-center flex-v-center">
            <span style="">
                <form method="post" action="{"pages.change_currency"|fn_url}">
                    <input type="hidden" name="redirect_url" value="{$config.current_url}" />

                    <label for="currency" style="padding-right: 5px;">Valuta</label>
                    <select name="currency" id="currency" onchange="this.form.submit()" style="border-radius: 5px; background: #333; color: #fff;">
                        <option value="ALL" {if $smarty.cookies.currency == 'ALL'}selected{/if} style="background: #333; color: #fff;">Lekë</option>
                        <option value="EUR" {if $smarty.cookies.currency == 'EUR'}selected{/if} style="background: #333; color: #fff;">Euro</option>
                    </select>
                </form>
            </span>
        </div>
    {/if}
{/if}
<div class="safe-b flexing flex-h-center flex-v-center">

    {if !$smarty.const.IS_MOBILE}
        {if $al|fn_isAL}
            <span style="position: absolute; left: 15px;">
                <form method="post" action="{"pages.change_currency"|fn_url}">
                    <input type="hidden" name="redirect_url" value="{$config.current_url}" />

                    <label for="currency" style="padding-right: 5px;">Valuta</label>
                    <select name="currency" id="currency" onchange="this.form.submit()" style="border-radius: 5px; background: #333; color: #fff;">
                        <option value="ALL" {if $smarty.cookies.currency == 'ALL'}selected{/if} style="background: #333; color: #fff;">Lekë</option>
                        <option value="EUR" {if $smarty.cookies.currency == 'EUR'}selected{/if} style="background: #333; color: #fff;">Euro</option>
                    </select>
                </form>
            </span>
        {/if}
    {/if}

    <span>

        {if $active_orders > 0}
         <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 500 500" style="enable-background:new 0 0 500 500;" xml:space="preserve"> <style type="text/css"> .st0 { fill:#FFFFFF; } .st1 { fill:#F04E25; } </style> <g> <circle class="st0" cx="250" cy="250" r="239"/> <g> <path class="st1" d="M202.9,376c15,24.7,51.2,11.9,79.1-8c9.9-7.1,0.3-22.5-10.5-16.6c-4.7,2.6-9.5,4.4-14.7,5.3 c-4.8,0.8-9.1-3.1-8.7-8c4.5-53.6,24.6-103.4,44.1-153.4c0.7-1.8-0.8-3.9-2.6-3.4l-79,20.4c-0.8,0.2-1.3,0.8-1.4,1.6l-0.7,5.5 c-0.1,1,0.6,2,1.6,2.1l15.4,2.7c1.6,0,2.8,1.5,2.4,3.1C215.4,276.4,185.2,346.9,202.9,376z"/> <path class="st1" d="M268.6,178.1c18.7,0,33.9-15.1,33.9-33.8c0-18.7-15.1-33.9-33.8-33.9c-18.7,0-33.9,15.1-33.9,33.8 C234.8,162.8,249.9,178,268.6,178.1z"/> </g> </g> </svg>
        {else}
         <svg viewBox="0 0 30 30"> <style type="text/css"> .st0 { fill: #E05626; } .st1 { fill: #FFFFFF; } </style> <g><rect x="9.7" y="8" class="st0" width="10.9" height="12.3"></rect><g> <path class="st1" d="M22.4,15.6c0-0.9,0-1.7,0-2.6c0-0.4,0-0.8,0-1.1l0-1v-0.3c0-1,0-2.1,0-3.2c-1.3-0.3-2.7-0.7-4-1 c-1.1-0.3-2.2-0.5-3.2-0.8c-0.1,0-0.2,0-0.2,0c-1.1,0.3-2.1,0.5-3.2,0.8c-1.3,0.3-2.7,0.7-4,1l0,0.9c0,2.4,0,4.9,0,7.4 c0,1.7,0.5,3.2,1.6,4.6c1.4,1.7,3.2,3,5.7,4.1c2.2-0.9,3.9-2.1,5.2-3.5C21.7,19.3,22.4,17.5,22.4,15.6z M19.8,12.1 c-0.7,0.8-1.3,1.7-2,2.5c-1.1,1.4-2.1,2.7-3.2,4.1c-0.6,0.8-1.1,0.8-1.6,0c-0.9-1.3-1.9-2.6-2.8-3.9c-0.3-0.4-0.3-0.9,0-1.2 c0.3-0.3,0.8-0.3,1.2,0.1c0.7,0.6,1.4,1.2,2.1,1.8c0.1,0.1,0.2,0.2,0.5,0.4c0.1-0.1,0.1-0.3,0.3-0.4c1.4-1.5,2.9-2.9,4.4-4.4 c0.2-0.2,0.4-0.3,0.6-0.4c0.3-0.1,0.6,0,0.8,0.4C20.1,11.5,20,11.8,19.8,12.1z"></path> <path class="st1" d="M27.3,15.5L27.3,15.5c0-0.5,0-1.1,0-1.6h0l0-4c0-1.3,0-2.7,0-4c0-1.6-0.9-2.8-2.5-3.2 c-1.5-0.4-3.1-0.8-4.6-1.1l-0.7-0.2c-1.1-0.3-2.3-0.6-3.5-0.9c-0.7-0.2-1.4-0.2-2.1,0c-1.3,0.3-2.5,0.6-3.7,0.9 C8.8,1.8,7,2.2,5.2,2.7C3.7,3,2.7,4.3,2.7,5.9l0,1.4c0,2.8,0,5.6,0,8.4c0,2.9,0.9,5.5,2.7,7.7c2,2.4,4.6,4.3,8.2,5.9 c0.4,0.2,0.8,0.3,1.3,0.3c0.5,0,0.9-0.1,1.4-0.3c3-1.3,5.4-2.9,7.4-5C26.2,21.8,27.4,18.8,27.3,15.5z M21.7,22.5L21.7,22.5 c-1.7,1.8-3.9,3.3-6.7,4.5c-3.2-1.5-5.6-3.1-7.3-5.3c-1.4-1.7-2.1-3.7-2.1-5.9c0-3.3,0-6.6,0-9.8l0-0.9C7.3,4.8,9,4.4,10.7,4 c1.4-0.3,2.8-0.7,4.3-1.1c0,0,0.1,0,0.2,0c1.4,0.3,2.8,0.7,4.2,1c1.7,0.4,3.4,0.9,5.1,1.3c0,1.3,0,2.7,0,4c0,0,0,0,0,0.1l0,1.6 c0,0.5,0,1,0,1.5c0,1.1,0,2.3,0,3.4C24.5,18.3,23.6,20.5,21.7,22.5z"></path></g></g> </svg>
        {/if}

    {if $al|fn_isAL}
        {$country = 'Shqipëri'}
    {else}
        {$country = 'Kosovë'}
    {/if}

    </span>
    {if $active_orders > 0 && $smarty.request.dispatch != 'checkout.complete'}
        {if $smarty.session.auth.latest_order.order_id && $smarty.session.auth.latest_order.company_id == 1 && $settings.General.display_delivery == 'Y' && !$al|fn_isAL}
            {$delivery_dates = $delivery_dates|unserialize}

            {if !$delivery_dates}
                {$delivery_dates = $smarty.session.auth.latest_order.order_id|fn_insert_order_delivery_dates}
            {/if}

            {if $delivery_dates}
                <span>
                    <a href="{"orders.details&order_id={$smarty.session.auth.latest_order.order_id}"|fn_url}" class="topbar-order-search">
                        Porosia juaj me numër #{$smarty.session.auth.latest_order.order_id} do të arrijë me datë <br>
                        {$delivery_dates.standard|date_format:$settings.Appearance.date_format|fn_custom_date_format} - {$delivery_dates.standard_range|date_format:$settings.Appearance.date_format}
                    </a>
                </span>
            {else}
                <span><a href="{"orders.search"|fn_url}" class="topbar-order-search">Për të parë statusin e porosisë tuaj, <br>ju lutem klikoni këtu.</a></span>
            {/if}
        {else}
            <span><a href="{"orders.search"|fn_url}" class="topbar-order-search">Për të parë statusin e porosisë tuaj, <br>ju lutem klikoni këtu.</a></span>
        {/if}
    {else}
        <span>
            Mbështetu tek ne. Dërgesa 100% të sigurta, kudo në {$country}.
        </span>
    {/if}
</div>