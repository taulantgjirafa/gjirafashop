{assign var="dod" value=$dod|fn_get_deal_of_the_day nocache}

{assign var="is_al" value=$al|fn_isAL}
{if $dod != false}
    {assign var="item" value=$dod.deal}

    {assign var="product" value=$dod.product}
    {*{assign var="container" value=intval($product.product_id/8000+1)}*}
    {*{if $container gt 5}*}
        {*{$container=5}*}
    {*{/if}*}
    {assign var="azure_path" value={get_images_from_blob product_id=$product.product_id count=1}}
    <div id="deal-of-the-day" class="ty-column4" style="display: none;"
         onclick="window.location.href='{"products.view&product_id={$product.product_id}"|fn_url}?t=deal_of_the_day'">

        {if $item.zone == 'catalog'}
            <div class="dod-img bbox">
                <a href="{"products.view&product_id={$product.product_id}"|fn_url}?t=deal_of_the_day">
                    <img src="{$azure_path}" alt="{$product.product}">
                </a>
            </div>
            <div class="dod-content bbox">
                <a href="{"products.view&product_id={$product.product_id}"|fn_url}?t=deal_of_the_day">
                    <span style="font-weight: bold; font-size: 18px; display: block;" class="no-mg">{$item.name}</span>
                    <span style="font-weight: bold" class="dod-title no-mg">{$product.product}</span>
                    <div class="dod-desc no-mg">{$product.description|strip_tags|truncate:350:"...":true}</div>
                    <div class="dod-price-wrapper">
                        <h2 class="dod-price nprice" {if $is_al}style="width: auto"{/if}><span>{$product.price - $item.new_price}{if $is_al} Lekë{else}€{/if}</span></h2>

                        {if $product.old_price != 0 && $product.old_price != null && $product.old_price_al !=null}
                            <h2 class="dod-price ribbon oprice"><span>{if $is_al && $product.old_price_al != 0 && $product.old_price_al != null}{$product.old_price_al} Lekë {else}{$product.old_price} €{/if}</span></h2>
                            {else}
                            <span style="text-decoration: none; font-weight: bold" class="dod-price ribbon oprice"><span style="font-size: 17px;">për vetëm</span></span>
                        {/if}

                    </div>
                </a>
            </div>
            {*<h1>Vetem edhe {$product.amount}</h1>*}
            {foreach $item.bonuses as $bonus}
                {if $bonus.discount_bonus == 'by_percentage'}
                    <h4 class="offer-badge bbox"><span>Zbritje</span> {$bonus.discount_value}%</h4>
                {elseif $bonus.discount_bonus  == 'to_percentage'}
                    <h4 class="offer-badge bbox"><span>Zbritje</span> {$bonus.discount_value}%</h4>
                {/if}
            {/foreach}
            <div class="dod-countdown bbox">
                <a href="{"products.view&product_id={$product.product_id}"|fn_url}?t=deal_of_the_day">
                    <img src="images/icons/countdown.png" alt="countdown">
                    <div id="countdown"></div>
                </a>
                <a href="{"products.view&product_id={$product.product_id}"|fn_url}?t=deal_of_the_day" class="dod-buynow">BLEJ
                    TANI</a>
                {include file="views/products/components/product_share_buttons.tpl" product_id=$product.product_id}
            </div>
        {/if}
    </div>
    <script>
        {literal}

        {/literal}
    </script>
{/if}