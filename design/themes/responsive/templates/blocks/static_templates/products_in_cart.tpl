{assign var="cart_products" value=$smarty.session.cart.products|array_reverse:true}

<style>
    .go-to-cart,
    .go-to-cart span {
        margin-right: 15px;
    }

    @media all and (max-width: 768px) {
        .go-to-cart,
        .go-to-cart span {
            margin-right: 0;
        }
    }
</style>

{if $auth.user_id == 0}
    <div class="homepage-zbritje flexing">
        <div class="homepage-zbritje__title-wrapper flexing">
            <i class="ty-icon-user"></i>
            <div class="">
                <h2 class="homepage-zbritje__title">Mirësevini në Gjirafa50</h2>
                <div class="">
                    <a href="#!" class="popup_login_button">Kyçu</a>
                    <a href="#!" class="popup_login_button">Regjistrohu</a>
                </div>
            </div>
        </div>
        <div class="homepage-zbritje__right">
            <p>Për të pasur eksperiencë më të mirë</p>
            <p>Zbritje 5% në blerjen e parë</p>
            <p><a href="#!" class="popup_login_button">Kycu</a> apo <a href="#!" class="popup_login_button">Regjistrohu</a></p>
            <svg id="Capa_1" enable-background="new 0 0 512 512" viewBox="0 0 512 512"  xmlns="http://www.w3.org/2000/svg"><g><path d="m509 186-26.852-35.802-14.93 104.512c-1.367 9.574-5.888 18.614-12.729 25.454l-183.847 183.848c-16.036 16.038-37.382 24.871-60.104 24.871-6.791 0-13.454-.806-19.894-2.34 9.78 15.29 26.897 25.457 46.356 25.457h220c30.327 0 55-24.673 55-55v-262c0-3.245-1.053-6.403-3-9z"/><circle cx="309.533" cy="234.203" r="15"/><circle cx="139.827" cy="234.203" r="15"/><path d="m249.429 442.799 183.848-183.848c2.295-2.295 3.784-5.272 4.243-8.485l21.214-148.493c.667-4.674-.904-9.39-4.243-12.728l-31.82-31.82-42.426 42.427 10.607 10.607c5.858 5.857 5.858 15.355 0 21.213-2.929 2.929-6.768 4.394-10.606 4.394s-7.678-1.465-10.606-4.394l-42.427-42.427c-5.858-5.857-5.858-15.355 0-21.213 5.857-5.857 15.355-5.857 21.213 0l10.607 10.607 42.426-42.427-31.82-31.82c-3.338-3.339-8.054-4.905-12.728-4.243l-148.494 21.214c-3.213.459-6.19 1.948-8.485 4.243l-183.848 183.848c-21.445 21.444-21.445 56.338 0 77.782l155.563 155.563c10.722 10.723 24.807 16.083 38.891 16.083s28.169-5.36 38.891-16.083zm60.104-253.596c24.813 0 45 20.187 45 45s-20.187 45-45 45-45-20.187-45-45 20.187-45 45-45zm-169.706 90c-24.813 0-45-20.187-45-45s20.187-45 45-45 45 20.187 45 45-20.186 45-45 45zm49.5 76.07c-1.572 0-3.172-.249-4.745-.773-7.859-2.62-12.106-11.114-9.487-18.974l70.71-212.133c2.62-7.859 11.115-12.106 18.974-9.487 7.859 2.62 12.106 11.114 9.487 18.974l-70.71 212.133c-2.096 6.284-7.949 10.26-14.229 10.26z"/><path d="m454.489 25.606c5.858-5.857 5.858-15.355 0-21.213-5.857-5.857-15.355-5.857-21.213 0l-31.819 31.82 21.213 21.213z"/></g></svg>
        </div>
    </div>
{/if}

{if $cart_products}
<div class="home-forgot-cart home-cart-items hbox">
    <div class="flexing hfti">
        <h3 class="flexing flex-v-center"><svg><use xlink:href="#svg-cart"></use></svg> Ke harruar diçka?</h3>
        <a href="{"checkout.cart"|fn_url}" class="btn btn-primary go-to-cart">SHIKO SHPORTËN <span><svg><use xlink:href="#svg-right"></use></svg></span>
        </a>
    </div>
    <div class="homecart-wrapper">
        {foreach from=$cart_products item=product key=key name=name}
        <a href="{"products.view&product_id=`$product.product_id`"|fn_url}">
            <div class="homepage-cart-item">
                <div class="home-cart-img">
                    <div
                        style="background-image:url('https://hhstsyoejx.gjirafa.net/gj50/img/{$product.product_id}/thumb/0.jpg');">
                    </div>
                </div>
                <!-- <div class="divider"></div> -->
                <div>
                    <h4 style="min-height: 30px; margin-bottom: 15px;">{$product.product|truncate:40}</h4>
                    {if $al|fn_isAL}
                    <h3 style="font-size: 20px;">{$product.price|number_format} Lekë</h3>
                    {else}
                    <h3 style="font-size: 20px;">{number_format((float)$product.price, 2, '.', '')} €</h3>
                    {/if}
                </div>
            </div>
        </a>
        {/foreach}
    </div>
</div>
{/if}

<script>
    {if $auth.user_id == 0}
        document.querySelectorAll('.popup_login_button').forEach(item => {
            item.addEventListener('click', function(){
                document.getElementsByClassName('ty-account-info__title')[0].click();
            })
        });
    {/if}
</script>