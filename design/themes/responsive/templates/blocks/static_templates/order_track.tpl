<div class="tracking-orders">
    <div class="track-progress">
        <div class="flexing flex-h-between">
            <span class="track-">E Hapur</span>
            <span class="track-">E verifikuar</span>
            <span class="track-">Dërgesa Gati</span>
            <span class="track-">E nisur</span>
            <span class="track-">E Realizuar</span>
        </div>
        <span class="track-bar">
            <span></span>
        </span>
    </div>
    <div class="tracking-products  ">
        <div class="tp-first-prod flexing flex-v-center">

            <div class="tp-desc flexing flex-h-between flex-grow">
                <div class="sub-desc">
                    <div class="tp-img">
                        <img src="https://hhstsyoejx.gjirafa.net/gj50/img/93068/thumb/0.jpg" alt="">
                    </div>
                    <div>
                        <h4 class="mb0 mb0 max-rows mr2">Tablet Apple iPad Pro Wi-Fi, 11" 2018, 512GB, i argjendtë</h4>
                        <p class="text-muted"><strong>Qmimi: </strong> 64.50€</p>
                        <span class="text-muted p0"><strong>Sasia:</strong> 1</span>
                    </div>
                </div>
                <div class="flex-grow sub-desc">
                    <strong>Kodi produktit:</strong>
                    <p>1911msi</p>
                </div>
                <div class="flex-grow sub-desc">
                    <strong>Nentotali:</strong>
                    <p>64.50€</p>
                </div>
            </div>
        </div>
    </div>
    <button>Shiko te gjitha porosite</button>
</div>