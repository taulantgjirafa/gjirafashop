<div class="homepage-content">
    <div class="row-fluid">
        <div class="span4 ty-left" id="footer-logo">
            <img src="images/logos/logo.svg" alt="Gjirafa50" style="height: 40px;">
        </div>
        <div class="span4 footer-links first">
            <ul class="ty-right">
                <li class="heading">Llogaria</li>
                {if $auth.user_id == 0}
                <li><a class="text-muted ty-block" onclick="sso.init()" ">Kyçu</a></li>
{*                <li><a class="text-muted ty-block" href="{$config.sso.register_url}">Regjistrohu</a></li>*}
{*                <li><a class="text-muted ty-block" href="{$config.sso.forgot_password_url}">Keni harruar fjalëkalimin?</a></li>*}
                <li><a class="text-muted ty-block" href="https://sso.gjirafa.com/Llogaria/Regjistrohu">Regjistrohu</a></li>
                <li><a class="text-muted ty-block" href="https://sso.gjirafa.com/Llogaria/ResetoFjalekalimin">Keni harruar fjalëkalimin?</a></li>
                {else}
                    <li><a class="text-muted ty-block" href="{"checkout.cart"|fn_url}">Shporta ime</a></li>
                    <li><a class="text-muted ty-block" href="{"orders.search"|fn_url}">Porositë</a></li>
                    <li><a class="text-muted ty-block" href="{"wishlist.view"|fn_url}">Lista e dëshirave</a></li>
                    <li><a class="text-muted ty-block" href="{"profiles.update"|fn_url}">Të dhënat e profilit</a></li>
                {/if}
            </ul>
        </div>
        <div class="span4 footer-links">
            <ul class="ty-right">
                <li class="heading">Kujdesi ndaj klientëve</li>
                <li><a class="text-muted ty-block" href="{"pages.customer-care"|fn_url}#50">Për Gjirafa 50</a></li>
                <li><a class="text-muted ty-block" href="{"pages.customer-care"|fn_url}#pagesat">Pagesat</a></li>
                <li><a class="text-muted ty-block" href="{"pages.customer-care"|fn_url}#teknike">Çështje teknike</a></li>
                <li><a class="text-muted ty-block" href="{"pages.customer-care"|fn_url}#transport">Transporti</a></li>
                <li><a class="text-muted ty-block" href="{"pages.customer-care"|fn_url}#produktet">Produktet/porositë</a></li>
                <li><a class="text-muted ty-block" href="{"pages.customer-care"|fn_url}#flex">GjirafaFLEX</a></li>
            </ul>
        </div>
        <div class="span4 footer-links third">
                <ul class="ty-right">
                <li class="heading">Kontakt</li>
                {if $al|fn_isAL}
                    <li><a class="text-muted ty-block" href="tel:+355688030303">Shqipëri: {$settings.Company.company_phone}</a></li>
                {else}
                    <li><a class="text-muted ty-block" href="tel:+38345101953">Kosovë: {$settings.Company.company_phone}<br>{$settings.Company.company_phone_2}</a></li>
                {/if}
                <li><a class="text-muted ty-block" href="mailto:contact@gjirafa50.com">contact@gjirafa50.com</a></li>
                <li><a class="text-muted ty-block" href="https://gjirafa.biz/gjirafa50-1" style="line-height: 1.4;" target="_blank">{$settings.Company.company_address}<br/> {$settings.Company.company_city}, {$settings.Company.company_state}</a></li>
{*                {if !$al|fn_isAL}*}
{*                    <li><a class="text-muted ty-block" style="line-height: 1.4;">Për çështje të kthimeve dhe riparimeve:<br />044228188</a></li>*}
{*                {/if}*}
                <li><a class="text-muted ty-block" style="line-height: 1.4;">Për kërkesa të ofertave ju lutemi drejtohuni në:<br />b2b@gjirafa50.com</a></li>
            </ul>
        </div>
    </div>
    <hr style="margin: 35px 0;">
    <div class="row-fluid" id="footer-info">
        <div class="span10 flex-center-row justify-left">
            <p class="text-muted">
                Pagesat bëhen përmes:
            </p>
            {if $al|fn_isAL}
                <img style="width: 550px;max-width: 100%;margin-left: 20px;" id="payments-all-footer" src="images/payment/payments_all_al.png" alt="">
            {else}
                <img style="width: 550px;max-width: 100%;margin-left: 20px;" id="payments-all-footer" src="images/payment/payments_all.png" alt="">
            {/if}
        </div>
        <div class="span6 flex-center-row justify-end">
            <p class="text-muted">Rri i lidhur me Gjirafa50</p>
            <div class="footer-share-icons">
                <a href="https://www.facebook.com/Gjirafa50/" target="_blank"><img alt="Gjirafa50 në Facebook" src="images/social/fb_ico.png"></a>
                <a href="https://www.twitter.com/gjirafa50" target="_blank"><img alt="Gjirafa50 në Twitter" src="images/social/twitter_ico.png"></a>
                <a href="https://www.instagram.com/gjirafa50/" target="_blank"><img alt="Gjirafa50 në Instagram" src="images/social/insta_ico.png"></a>
{*                <a href="https://www.snapchat.com/add/gjirafa50" target="_blank"><img alt="Gjirafa50 në Snapchat" src="images/social/snap_ico.png"></a>*}
            </div>
        </div>
    </div>
    <hr style="margin: 35px 0;">
    <div class="row">
        <div class="span16">
            <p style="
    font-weight: 300;
    font-size: 13px;
    color: #777;
">Mundësuar nga <a href="https://gjirafa.com" style="color:#777;">Gjirafa, Inc.</a> - Të gjitha të drejtat e rezervuara</p>
            <p style="margin-top:10px;"><a href="{'pages.terms-and-conditions'|fn_url}">Termet dhe kushtet</a> <span style="padding:0 10px;">|</span> <a href="https://gjirafa.com/Top/Terms#Privacy">Politika e Privatësisë</a></p>
        </div>
    </div>
    <div class="small-footer pt-4 pb-4">
        <div class="container">
            <div class="small-footer_container">
                <div class="footer-logos d-flex justify-content-between align-items-center">
                    <div class="footer-logos_item pl-2 pr-2">
                        <a href="https://gjirafa.com/?utm_source=gjirafa50&amp;utm_medium=footer" target="_blank" title="Gjirafa">
                            <img src="https://gjstatic.blob.core.windows.net/fix/footer-logos/gjirafa.png" alt="Gjirafa - Kërko në gjuhën tënde.">
                        </a>
                    </div>
                    <div class="footer-logos_item pl-2 pr-2">
                        <a href="https://gjirafa50.com/?utm_source=gjirafa50&amp;utm_medium=footer" target="_blank" title="Gjirafa50">
                            <img src="https://gjstatic.blob.core.windows.net/fix/footer-logos/gjirafa50.png" alt="Gjirafa50 - Dyqani online më i madh për teknologji në Kosovë dhe rajon.">
                        </a>
                    </div>
                    <div class="footer-logos_item pl-2 pr-2">
                        <a href="https://gjirafamall.com/?utm_source=gjirafa50&amp;utm_medium=footer" target="_blank" title="GjirafaMall">
                            <img src="https://gjstatic.blob.core.windows.net/fix/footer-logos/mall.png" alt="GjirafaMall - Qendra e vetme tregtare online në Kosovë.">
                        </a>
                    </div>
                    <div class="footer-logos_item pl-2 pr-2">
                        <a href="https://video.gjirafa.com/?utm_source=gjirafa50&amp;utm_medium=footer" title="GjirafaVideo">
                            <img src="https://gjstatic.blob.core.windows.net/fix/footer-logos/video.png" alt="GjirafaVideo - Video platforma me përmbajtje ekskluzive">
                        </a>
                    </div>
                    <div class="footer-logos_item pl-2 pr-2">
                        <a href="https://gjirafa.biz/?utm_source=gjirafa50&amp;utm_medium=footer" target="_blank" title="Gjirafa.biz">
                            <img src="https://gjstatic.blob.core.windows.net/fix/footer-logos/pikBiz.png" alt="Gjirafa.biz - Të gjitha bizneset dhe pikat e interesit.">
                        </a>
                    </div>
                    <div class="footer-logos_item pl-2 pr-2">
                        <a href="https://client.gjirafa.com/landing?utm_source=gjirafa50&utm_medium=footer" target="_blank" title="Gjirafa AdNetwork">
                            <img src="https://gjstatic.blob.core.windows.net/fix/footer-logos/aNet.png" alt="Gjirafa AdNetwork - Zgjidhja më efektive dhe më e thjeshtë e reklamimit në Internet.">
                        </a>
                    </div>
                    <div class="footer-logos_item pl-2 pr-2">
                        <a href="https://gjirafalab.com/?utm_source=gjirafa50&amp;utm_medium=footer" target="_blank" title="GjirafaLab">
                            <img src="https://gjstatic.blob.core.windows.net/fix/footer-logos/lab.png" alt="Gjirafa Lab - Become part of the founder community.">
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <style>
            .d-flex {
                display: -webkit-box !important;
                display: -ms-flexbox !important;
                display: flex !important;
            }
            .justify-content-between {
                -webkit-box-pack: justify !important;
                -ms-flex-pack: justify !important;
                justify-content: space-between !important;
            }
            .align-items-center {
                -webkit-box-align: center !important;
                -ms-flex-align: center !important;
                align-items: center !important;
            }
            .pl-2 {
                padding-left: 0.5rem !important;
            }
            .pr-2 {
                padding-right: 0.5rem !important;
            }
            .pb-4 {
                padding-bottom: 1.5rem !important;
            }
            .pt-4 {
                padding-top: 1.5rem !important;
            }
            .small-footer {
                background: #f3f3f3;
            }
            .small-footer_container {
                overflow-x: auto;
                overflow-y: hidden;
                margin: 0 -15px;
            }
            .footer-logos {
                -ms-flex-wrap: nowrap;
                flex-wrap: nowrap;
                padding-left: 15px;
            }
            .footer-logos_item a {
                display:block;
            }
            .small-footer .container {
                width: 100%;
                padding-right: 15px;
                padding-left: 15px;
                margin-right: auto;
                margin-left: auto;
                box-sizing:border-box;
            }
            @media(min-width: 576px) {
                .small-footer .container {
                    max-width:540px;
                }
            }
            @media(min-width: 768px) {
                .small-footer .container {
                    max-width:720px;
                }
            }
            @media(min-width: 992px) {
                .small-footer .container {
                    max-width:960px;
                }
            }
            @media(min-width: 1200px) {
                .small-footer .container {
                    max-width:1200px;
                }
            }
            .footer-logos_item img {
                max-height: 20px;
                width: auto;
                max-width: 200px;
                display: block;
                opacity: 0.5;
                -webkit-filter: grayscale(100%);
                filter: grayscale(100%);
                -webkit-transition: all .15s ease-in-out;
                -o-transition: all .15s ease-in-out;
                transition: all .15s ease-in-out;
            }
            @media(max-width: 991.98px) {
                .footer-logos:after {
                    content:"";
                    -webkit-box-flex: 0;
                    -ms-flex: 0 0 10px;
                    flex: 0 0 15px;
                    height: 10px;
                }

                .footer-logos_item:first-child {
                    padding-left: 0 !important;
                }
            }
            @media(min-width: 768px) {
                .small-footer_container {
                    margin:0 !important;
                }
                .footer-logos {
                    padding-left: 0;
                }
                .footer-logos_item img {
                    max-width: 100%;
                    max-height: 25px;
                }
                .footer-logos_item:hover img {
                    opacity: 1;
                    -webkit-filter: grayscale(0);
                    filter: grayscale(0);
                }
            }
        </style>
    </div>
</div>