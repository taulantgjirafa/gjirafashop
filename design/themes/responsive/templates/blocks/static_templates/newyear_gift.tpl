{assign var="product" value=$dod|get_newyear_gift nocache}
<link rel="stylesheet" href="https://hhstsyoejx.gjirafa.net/gj50/newyear_gift.css">
<div class="span16" id="gift-wrapper">
    {if !$auth.user_id}
        <h1 class="gift_login">Kyçu për ta parë dhuratën me ofertën e sotme për ty!</h1>
        {elseif $product.gift_options.saw_it == 0}
        <h1 class="gift_login">
            Zbulo dhuratën me ofertën e sotme për ty!
            {if $product.gift_options.sold_out}
                <span style="display: block;font-size: 14px;font-weight: 700;margin-top: 5px;">Na vjen keq por dhurata e kaluar eshte shitur!</span>
            {/if}
        </h1>
        {elseif $product.gift_options.saw_it == 1}
        <h1 class="gift_login">Kjo është oferta e ditës së sotme për ty!<span style="display: block;font-size: 14px;color: #777;">Kthehu nesër për ofertën tjetër!</span></h1>
    {/if}
        <div id="merrywrap" class="merrywrap {if $product.gift_options.saw_it == 1}step-2{/if}">

            <div class="giftbox" {if !$auth.user_id}style="pointer-events: none !important;"{/if} {if $product.gift_options.saw_it == 1}style="display:none;"{/if}>
                <div class="cover">
                    <div></div>
                </div>
                <div class="box"></div>
            </div>
            <div class="icons">
                {if $product != false}
                    <a href="/index.php?dispatch=products.view&product_id={$product.product_id}" style="text-decoration: none;">
                        <div class="product">
                            <input type="hidden" id="gift_prod_code" value="{$product.product_code}">
                            <img class="img" src="{get_images_from_blob product_id=$product.product_id count=1}"/>
                            <h3>{$product.product}</h3>
                            <h1><span class="old-price">{$product.price}€</span> {$product.new_price}€</h1>
                            <span class="ty-btn ty-btn__primary">BLEJ TANI!</span>
                        </div>
                    </a>
                {/if}

            </div>
        </div>
</div>
{if $product != false}
    <h5 id="prod-desc" {if $product.gift_options.saw_it == 1}style="display:block;"{/if}>Produktin me çmim {$product.new_price}€ do ta merrni pasi që të konfirmohet porosia juaj!</h5>
{/if}
{if $product.gift_options.saw_it == 1}
    {literal}
        <script> if($('.mobile-header').is(":visible")) { $('.product-masonry').addClass('gift'); }</script>
        {/literal}
    {/if}
{literal}
    <script>
        window.onload=function(){
            var merrywrap=document.getElementById("merrywrap");
            var box=merrywrap.getElementsByClassName("giftbox")[0];
            var step=1;
            var stepMinutes=[2000,2000,0,0];
            function init(){
                box.addEventListener("click",openBox,false);
            }
            function stepClass(step){
                merrywrap.className='merrywrap';
                merrywrap.className='merrywrap step-'+step;
            }
            function openBox(){
                if(step===1){
                    box.removeEventListener("click",openBox,false);
                    var exdate=new Date();
                    exdate.setHours(23);
                    exdate.setMinutes(59);
                    exdate.setSeconds(59);
                    document.cookie = "gift_prod_code=" + document.getElementById('gift_prod_code').value  + '; expires=' + exdate.toUTCString() +'; path=/';
                }
                stepClass(step);
                if(step===2){
                    document.getElementById('gift-wrapper').className += ' step-2';
                    if($('.mobile-header').is(":visible"))
                        $('.product-masonry').addClass('gift');
                        document.getElementById('prod-desc').style.display = 'block';
                    return;
                }
                setTimeout(openBox,stepMinutes[step-1]);
                step++;
            }

            init();

        }
    </script>

{/literal}