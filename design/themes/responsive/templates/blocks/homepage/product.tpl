{assign var="obj_id" value=$product.product_id}
{assign var="obj_id_prefix" value="`$obj_prefix``$product.product_id`"}
<div class="hs-item">
    <a href="{"products.view&product_id={$product.product_id}"|fn_url}" class="anchor-unset">
        <div class="hs-item-inner">
            {if $product.amount > 0}
            <span class="grid-list-icons on-sale">
                {if $al|fn_isal}
                    <img src="https://gjirafa50.com/images/icons/upTo48-1.png" alt="">
                {else}
                    <img src="https://gjirafa50.com/images/icons/upTo48.png" alt="">
                {/if}
             </span>
            {/if}
            {if $product.discount}
                <span class="hr-percentage">
                    -{$product.discount|string_format:"%d"}%
                </span>
            {/if}
            <div class="hs-img">
                <div style="background-image: url(https://hhstsyoejx.gjirafa.net/gj50/img/{$product.product_id}/thumb/0.jpg);"></div>
            
            </div>
            <div class="hs-content">
                <p class="text-muted hidden-phone hs-p-title">{$product.product|truncate:55:"..."}</p>
                <div class="hs-price flexing flex-h-between flex-v-center">
                    <div>
                        <b style="font-size:18px;">{include file="common/price.tpl" value=$product.price}</b>
                        {if $product.discount}<span class="hs-old-price">{include file="common/price.tpl" value=$product.old_price}</span>{/if}
                    </div>
                    {include file="addons/discussion/views/discussion/components/stars.tpl" stars=$product.rating|fn_get_discussion_rating}
                </div>
                <div class="hs-btns">
                    <span class="hs-add-cart">
                        <form action="{""|fn_url}" method="post" name="product_form_{$obj_prefix}{$obj_id}" enctype="multipart/form-data" class="cm-disable-empty-files cm-ajax cm-ajax-full-render cm-ajax-status-middle">
                            <input type="hidden" name="result_ids" value="cart_status*,wish_list*,checkout*,account_info*"/>
                            <input type="hidden" name="redirect_url" value="{$redirect_url|default:$config.current_url}"/>
                            <input type="hidden" name="product_data[{$product.product_id}][product_id]" value="{$product.product_id}"/>

                            <button id="button_cart_{$product.product_id}" type="submit" name="dispatch[checkout.add..{$product.product_id}]">
                                <svg><use xlink:href="#svg-cart"></use></svg>
                            </button>
                        </form>
                    </span>
                    <span class="add-to-wish in-wish-list">
                        <form action="{""|fn_url}" method="post" name="product_form_{$obj_prefix}{$obj_id}" enctype="multipart/form-data" class="cm-disable-empty-files cm-ajax cm-ajax-full-render cm-ajax-status-middle">
                            <input type="hidden" name="result_ids" value="cart_status*,wish_list*,checkout*,account_info*"/>
                            <input type="hidden" name="redirect_url" value="{$redirect_url|default:$config.current_url}"/>
                            <input type="hidden" name="product_data[{$product.product_id}][product_id]" value="{$product.product_id}"/>

                            <button id="button_cart_{$product.product_id}" class="button wishlist-btn" type="submit" name="dispatch[wishlist.add..{$product.product_id}]">
                                <svg><use xlink:href="#svg-heart"></use></svg>
                            </button>
                        </form>
                    </span>
                </div>
            </div>
        </div>
    </a>
</div>