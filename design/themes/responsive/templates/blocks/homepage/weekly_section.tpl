<div class="weekly-section">
    <div class="hbox">
        <div class=" flexing flex-h-between">
            <div>
                <h2>{$products.extra.title}</h2>
                <p class="text-muted">{$products.extra.description}</p>
            </div>
            <div class="more-from-this">
                <a href="{"categories.view&category_id={$products.extra.category_id}"|fn_url}">{__('more')}</a>
                <svg>
                    <use xlink:href="#svg-right"></use>
                </svg>
            </div>
        </div>
        <div class="ws-item-wrapper flexing flex-h-between ">
            {$random_counter = 1}
            {foreach from=$products item="product" key="key"}
            <span style="display: none;">
            {$random_counter++}
            </span>
            {if $key != 'extra'}
            {$product_id = $product.product_id|strval}
            {if $product_id|in_array:$products.extra.products}
            <div class="ws-item">
                <a href="{"products.view&product_id={$product.product_id}"|fn_url}" class="anchor-unset">
                <div class="ws-inner-item flexing cp">
                    <div class="ws-img">
                        <div
                            style="background-image: url(https://hhstsyoejx.gjirafa.net/gj50/img/{$product.product_id}/thumb/0.jpg);">
                        </div>
                    </div>
                    <div>
                        <h3 class=" text-light max-rows mr3">{$product.product}</h3>
                        <div class="prd-left-bar">
                            {$amount = $product.amount + $product.czc_stock + $product.czc_retailer_stock}
                            {if $amount > 10}
                            {$amount = $random_counter}
                            {/if}

                            {$amount_percentage = ($amount / 50) * 100}
                            {if $amount}
                                <div class="ws-progress-bar">
                                    <span style="width:{$amount_percentage}%;"></span>
                                </div>
                                <h6 class="text-muted text-light">{$amount} të mbetura</h6>
                            {/if}
                        </div>
                    </div>
                </div>
                </a>
            </div>
            {/if}
            {/if}
            {/foreach}
        </div>
    </div>
    <div class="flexing flex-h-center ws-other-prods b-product-container">
        <div class="b-arrows hidden">
            <button class="arrow-top">
            </button>
            <button class="arrow-bottom">
            </button>
        </div>
        <div class="bp-wrapper flexing">
            <span style="display: none;">{$products|shuffle}</span>
            <div class="wop-item bp ">
                <div class="weekly-slide-container bp-item-slider td_1">
                    {foreach from=$products item="product" key="key"}
                    {if $key != 'extra'}
                    <div class="w-slide-item bp-items td_{$key} {if $key == 2} active {/if}" onclick="location.href='{"products.view&product_id={$product.product_id}"|fn_url}';">
                        <img src="https://hhstsyoejx.gjirafa.net/gj50/img/{$product.product_id}/thumb/0.jpg" alt="">
                    </div>
                    {/if}
                    {/foreach}
                </div>
            </div>
            <span style="display: none;">{$products|shuffle}</span>
            <div class="wop-item bp ">
                <div class="weekly-slide-container bp-item-slider td_2">
                    {foreach from=$products item="product" key="key"}
                    {if $key != 'extra'}
                    <div class="w-slide-item bp-items td_{$key} {if $key == 2} active {/if}" onclick="location.href='{"products.view&product_id={$product.product_id}"|fn_url}';">
                        <img src="https://hhstsyoejx.gjirafa.net/gj50/img/{$product.product_id}/thumb/0.jpg" alt="">
                    </div>
                    {/if}
                    {/foreach}
                </div>
            </div>
            <span style="display: none;">{$products|shuffle}</span>
            <div class="wop-item bp ">
                <div class="weekly-slide-container bp-item-slider td_3">
                    {foreach from=$products item="product" key="key"}
                    {if $key != 'extra'}
                    <div class="w-slide-item bp-items td_{$key} {if $key == 2} active {/if}" onclick="location.href='{"products.view&product_id={$product.product_id}"|fn_url}';">
                        <img src="https://hhstsyoejx.gjirafa.net/gj50/img/{$product.product_id}/thumb/0.jpg" alt="">
                    </div>
                    {/if}
                    {/foreach}
                </div>
            </div>
        </div>


    </div>
    <div class="ws-bg"></div>
</div>

<script>

    if (window.outerWidth > 767) {
        var wsBanner = document.querySelector('.ws-bg'),
            leftSpace = wsBanner.offsetLeft;
            console.log(leftSpace)

        wsBanner.style.marginLeft = '-' + leftSpace + 'px';
        wsBanner.style.width = 'calc(100% + ' + leftSpace * 2 + 'px)';
    }


    const slide = document.querySelectorAll('.bp-item-slider'),
        arrowsSlide = document.querySelectorAll('.b-arrows button'),
        bannerContainer = document.querySelector('.bp-wrapper');
    var slideInterval,
        slideIntervalTimer = 3000;


    for (var j = 0; j < arrowsSlide.length; j++) {
        arrowsSlide[j].onclick = function () {
            const arrows = this;
            var activeSlide = document.querySelectorAll('.bp-items.active');
            slideAlone(arrows, activeSlide);
        };
    }

    bannerContainer.addEventListener('mouseover', function () {
        clearInterval(slideInterval);
    });
    setTimeout(function () {
        bannerContainer.addEventListener('mouseleave', function () {
            slideInterval = setInterval(autoSlide, slideIntervalTimer);
        });
    }, 1000);
    slideInterval = setInterval(autoSlide, slideIntervalTimer);

    function slideAlone(arrows, activeSlide) {
        for (let i = 0; i < slide.length; i++) {
            let lastSlideChildNum = slide[i].children.length - 1;
            firstSlideItem = slide[i].children[0],
                lastSlideItem = slide[i].children[lastSlideChildNum];
            if (arrows.getAttribute('class') == 'arrow-bottom') {
                activeSlide[i].classList.remove('active');
                activeSlide[i].nextElementSibling.classList.add('active');
                slide[i].appendChild(firstSlideItem);
            } else if (arrows.getAttribute('class') == 'arrow-top') {
                activeSlide[i].classList.remove('active');
                activeSlide[i].previousElementSibling.classList.add('active');
                slide[i].insertBefore(lastSlideItem, slide[i].children[0]);
            }
        }
    }

    function autoSlide() {
        document.querySelector('.arrow-bottom').click();
    };
</script>