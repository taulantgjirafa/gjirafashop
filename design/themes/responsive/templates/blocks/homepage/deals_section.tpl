<div class="hbox">
    <div class="mb10 flexing flex-h-between flex-v-center">
        <div>
            <h2>{$products.extra.title}</h2>
            <p class="text-muted hidden-phone">{$products.extra.description}</p>
        </div>
        <div class="more-from-this">
            <a href="{"categories.view&category_id={$products.extra.category_id}"|fn_url}">{__('more')}</a>
            <svg><use xlink:href="#svg-right"></use></svg>
        </div>
    </div>
    <div class="slide-section-container home-section-wrapper ">
        <div class="slide-arrows">
            <span class="h-slide-left">
                    <svg><use xlink:href="#svg-left"></use></svg>
                </span>
            <span class="h-slide-right">
                    <svg><use xlink:href="#svg-right"></use></svg>
                </span>
        </div>
        <div class="slide-section-wrapper">
            <div class="home-prods-slide flexing flex-h-between" data-index="0">
                {assign var=counter value=1}
                {foreach from=$products item="product" key="key"}
                    {if $key != 'extra' && $counter <= 20}
                        {include file="blocks/homepage/product.tpl" product=$product}
                        <span style="display: none;">{$counter++}</span>
                    {/if}
                {/foreach}
            </div>
        </div>
    </div>
</div>