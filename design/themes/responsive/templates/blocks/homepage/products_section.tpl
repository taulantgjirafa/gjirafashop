<div class="home-section-wrapper">
    <div class="mb10 flexing flex-h-between flex-v-center">
        <div class="ml10">
            <h2>Të gjitha produktet</h2>
            <p class="text-muted">
                Të rekomanduara për ju
            </p>
        </div>

    </div>
    <div class="flexing flex-wrap grid-list">
{*        {foreach from=$products item="product"}*}
{*            {include file="blocks/homepage/product.tpl" product=$product}*}
{*        {/foreach}*}
        {include file="blocks/homepage/all_products.tpl" products=$products}
    </div>
</div>