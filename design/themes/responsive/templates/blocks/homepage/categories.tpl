<div>
    <div class="homepage-off-categories">
        {foreach from=$products.extra.categories item="category"}
            <a href="{"categories.view&category_id={$category.category_id}"|fn_url}" class="anchor-unset">
            <div class="ho-cat">
                <div class="hc-icon">
                    <img src="https://hhstsyoejx.gjirafa.net/gj50/categories/icons/{$category.id_path|replace:'/':'_'}%20{$category.category|replace:'/':'_'|lower|replace:'ë':'e'}.png" alt="">
                </div>
                <h3>{$category.category}</h3>
            </div>
            </a>
        {/foreach}
{*        <div class="ho-cat">*}
{*            <div class="hc-icon">*}
{*                <svg><use xlink:href="#svg-bag"></use></svg>*}
{*            </div>*}
{*            <h3>Qant</h3>*}
{*        </div>*}
{*        <div class="ho-cat">*}
{*            <div class="hc-icon">*}
{*                <svg><use xlink:href="#svg-case"></use></svg>*}
{*            </div>*}
{*            <h3>Case</h3>*}
{*        </div>*}
{*        <div class="ho-cat">*}
{*            <div class="hc-icon">*}
{*                <svg><use xlink:href="#svg-usb"></use></svg>*}
{*            </div>*}
{*            <h3>USB</h3>*}
{*        </div>*}
{*        <div class="ho-cat">*}
{*            <div class="hc-icon">*}
{*                <svg><use xlink:href="#svg-pbank"></use></svg>*}
{*            </div>*}
{*            <h3>Power Bank</h3>*}
{*        </div>*}
{*        <div class="ho-cat">*}
{*            <div class="hc-icon">*}
{*                <svg><use xlink:href="#svg-drone"></use></svg>*}
{*            </div>*}
{*            <h3>Drone</h3>*}
{*        </div>*}
{*        <div class="ho-cat">*}
{*            <div class="hc-icon">*}
{*                <svg><use xlink:href="#svg-swatch"></use></svg>*}
{*            </div>*}
{*            <h3>Ore e menqur</h3>*}
{*        </div>*}
{*        <div class="ho-cat">*}
{*            <div class="hc-icon">*}
{*                <svg><use xlink:href="#svg-vr"></use></svg>*}
{*            </div>*}
{*            <h3>VR</h3>*}
{*        </div>*}
    </div>
</div>