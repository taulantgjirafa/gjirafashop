{$find = ['ë', ' ']}
{$replace = ['e', '%20']}

<div>
    <div class="hbox transparent-box">
        <div class="mb10 flexing flex-h-between flex-v-center">
            <div>
                <h2>{$products.extra.category.category}</h2>
                <p class="text-muted hidden-phone">
                    Nënkategoritë
                </p>
            </div>
            <div class="more-from-this">
                <a href="{"categories.view&category_id={$products.extra.category.category_id}"|fn_url}">{__('more')}</a>
                <svg><use xlink:href="#svg-right"></use></svg>
            </div>
        </div>
        <div class="flexing flex-h-between ss-img-container">
            <div class="special-main-img-wrapper">
                <div class="ss-img w100 h100">
                    <span style="background-image: url(https://hhstsyoejx.gjirafa.net/gj50/categories/headers/{$products.extra.category.id_path|replace:'/':'_'}%20{$products.extra.category.category|replace:'/':'_'|lower|replace:$find:$replace}.jpg);" onclick="location.href='{"categories.view&category_id={$products.extra.category.category_id}"|fn_url}';"></span>
                </div>
            </div>
            <div class="special-off-img-wrapper flexing">
                <div class="w50">
                    {$subcat1 = $products.extra.subcategories.0}
                    <a href="{"categories.view&category_id={$subcat1.category_id}"|fn_url}" class="anchor-unset">
                        <div class="ss-img">
                            <span style="background-image: url(https://hhstsyoejx.gjirafa.net/gj50/categories/headers/{$subcat1.id_path|replace:'/':'_'}%20{$subcat1.category|replace:'/':'_'|lower|replace:$find:$replace}.jpg);"></span>
                            <div class="ss-hover">
                                <h3>{$subcat1.category}</h3>
                                <p>Shiko më shumë nga kjo kategori</p>
                            </div>
                        </div>
                    </a>
                    {$subcat2 = $products.extra.subcategories.1}
                    <a href="{"categories.view&category_id={$subcat2.category_id}"|fn_url}" class="anchor-unset">
                        <div class="ss-img">
                            <span style="background-image: url(https://hhstsyoejx.gjirafa.net/gj50/categories/headers/{$subcat2.id_path|replace:'/':'_'}%20{$subcat2.category|replace:'/':'_'|lower|replace:$find:$replace}.jpg);"></span>
                            <div class="ss-hover">
                                <h3>{$subcat2.category}</h3>
                                <p>Shiko më shumë nga kjo kategori</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="w50">
                    {$subcat3 = $products.extra.subcategories.2}
                    <a href="{"categories.view&category_id={$subcat3.category_id}"|fn_url}" class="anchor-unset">
                        <div class="ss-img">
                            <span style="background-image: url(https://hhstsyoejx.gjirafa.net/gj50/categories/headers/{$subcat3.id_path|replace:'/':'_'}%20{$subcat3.category|replace:'/':'_'|lower|replace:$find:$replace}.jpg);"></span>
                            <div class="ss-hover">
                                <h3>{$subcat3.category}</h3>
                                <p>Shiko më shumë nga kjo kategori</p>
                            </div>
                        </div>
                    </a>
                    {$subcat4 = $products.extra.subcategories.3}
                    <a href="{"categories.view&category_id={$subcat4.category_id}"|fn_url}" class="anchor-unset">
                        <div class="ss-img">
                            <span style="background-image: url(https://hhstsyoejx.gjirafa.net/gj50/categories/headers/{$subcat4.id_path|replace:'/':'_'}%20{$subcat4.category|replace:'/':'_'|lower|replace:$find:$replace}.jpg);"></span>
                            <div class="ss-hover">
                                <h3>{$subcat4.category}</h3>
                                <p>Shiko më shumë nga kjo kategori</p>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>