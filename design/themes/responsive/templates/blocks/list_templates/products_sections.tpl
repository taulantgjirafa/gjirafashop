<main class="new-h">
    <section>
        {include file="blocks/homepage/weekly_section.tpl" products=$products.1}
    </section>
    <section>
        {include file="blocks/homepage/deals_section.tpl" products=$products.2}
    </section>
    <section>
        {include file="blocks/homepage/category_section.tpl" products=$products.3}
    </section>
{*    <section>*}
{*        {include file="blocks/homepage/deals_section.tpl"}*}
{*    </section>*}
{*    <section>*}
{*        {include file="blocks/homepage/featured_section.tpl"}*}
{*    </section>*}
    <section>
        {include file="blocks/homepage/categories.tpl" products=$products.4}
    </section>
    <section class="all-prods">
        {include file="blocks/homepage/products_section.tpl" products=$products.manual}
    </section>
</main>
<style>
    .tygh-content {
        background: #f5f5f5;
    }

    .anchor-unset {
        all: unset;
    }

    .anchor-unset:hover {
        text-decoration: none;
    }

    section {
        margin-top: 30px;
    }
</style>

<script>
    if (window.outerWidth > 767) {
        var arrows = document.querySelectorAll(
            ".slide-arrows .h-slide-right, .h-slide-left"
        );

        arrows.forEach(function(el) {
            el.onclick = function() {
                var slideWrapper = el.parentElement.nextElementSibling.querySelector(
                        ".home-prods-slide"
                    ),
                    slideItemWidth = document.querySelector(".hs-item").offsetWidth,
                    slideItems = slideWrapper.querySelectorAll(".hs-item"),
                    slideIndex = slideWrapper.getAttribute("data-index");

                if (el.classList.contains("h-slide-right")) {
                    if (slideIndex < slideItems.length - 5) {
                        slideIndex++;
                        slideWrapper.setAttribute("data-index", slideIndex);
                        slider.right(slideWrapper, slideItemWidth, slideIndex);
                    }
                } else {
                    if (slideIndex >= 1) {
                        slideIndex--;
                        slideWrapper.setAttribute("data-index", slideIndex);
                        slider.left(slideWrapper, slideItemWidth, slideIndex);
                    }
                }
            };
        });

        var slider = {
            left: function left(slideEl, slideWidth, index) {
                slideEl.style.transform =
                    "translate3d(-" + slideWidth * index + "px,0,0)";
            },
            right: function right(slideEl, slideWidth, index) {
                slideEl.style.transform =
                    "translate3d(-" + slideWidth * index + "px,0,0)";
            }
        };
    }
</script>