{assign var="is_al" value=$al|fn_isAL}
<div id="backdrop"></div>
<div id="sidenav" style="transform: translate3d(-680px, 0px, 0px);position: fixed;">

    <div id="user-area">
        {if $auth.user_id}
            {if $user_info.firstname || $user_info.lastname}
                <span class="ty-account-info__title-txt">{$user_info.firstname} {$user_info.lastname}</span>
            {else}
                <span class="ty-account-info__title-txt">{$user_info.email}</span>
            {/if}
        {else}
            <a class="rplef login-gjirafa" href="https://sso.gjirafa.com">
                <p>Kycu apo regjistrohu</p>
            </a>
        {/if}
        <span class="user-line"></span>
        <a class="logo-ico" href="/"><img src="https://hhstsyoejx.gjirafa.net/gj50/icons/0.svg"/></a>
    </div>
    <ul class="m-menu-list">

        <li><a class="rplef" href="/"><img class="ico sm" src="https://hhstsyoejx.gjirafa.net/gj50/icons/home.svg"/>Ballina</a></li>
        <li><a class="rplef" href="/index.php?dispatch=checkout.cart"><img class="ico sm" src="https://hhstsyoejx.gjirafa.net/gj50/icons/shporta.svg"/>Shporta</a></li>
        <li><a class="rplef" href="/index.php?dispatch=wishlist.view"><img class="ico sm" src="https://hhstsyoejx.gjirafa.net/gj50/icons/wish_list.svg"/>Lista e dëshirave</a></li>
        <li class="divider"></li>
        {foreach $categories|fn_get_mobile_main_categories as $category}
            <li {if $category.category_id == 2161}class="new-category" style="right: 0;"{/if}>
                <a class="rplef" href="{"categories.view&category_id={$category.category_id}"|fn_url}">
                    {if $category.category_id == 3433}
                        <img class="ico" src="https://hhstsyoejx.gjirafa.net/gj50/img/500000/img/7.jpg"/>
                    {else}
                        <img class="ico" src="https://hhstsyoejx.gjirafa.net/gj50/icons/{$category.category_id}.svg"/>
                    {/if}
                    {$category.category}
                </a>
            </li>
        {/foreach}

        <li class="divider"></li>
        <li><a href="{"pages.customer-care"|fn_url}">Kujdesi ndaj klientëve</a></li>
        <li id="contact-mob">
            <h6>Kontakt:</h6>
            <a class="email rplef" href="mailto:{"email"|fn_get_contact_info}">{"email"|fn_get_contact_info}</a>
            <a class="rplef" href="tel:{"number"|fn_get_contact_info}">{"number"|fn_get_contact_info}</a>
        </li>
    </ul>
</div>