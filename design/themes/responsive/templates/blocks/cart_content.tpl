{assign var="dropdown_id" value=$block.snapping_id}
{assign var="r_url" value=$config.current_url|escape:url}
{hook name="checkout:cart_content"}
    <div class="ty-dropdown-box ty-header-cart" id="cart_status_{$dropdown_id}">
        <div id="sw_dropdown_{$dropdown_id}" class="ty-dropdown-box__title cm-combination">
        <a href="{"checkout.cart"|fn_url}" id="cart_content_dropdown">
            {hook name="checkout:dropdown_title"}
                <div id="cart-icon">
                    {*<img src="images/cart-white.svg" alt="cartico"/>*}
                    <svg><use xlink:href="#svg-cart"></use></svg>
                </div>

{*                {if count($smarty.session.cart.products)}*}
                {if $smarty.session.cart.amount}

                    {*<i class="ty-minicart__icon ty-icon-basket filled"></i>*}

                    <span class="ty-minicart-title ty-hand">
                        <strong class="cart-count">

{*                            {count($smarty.session.cart.products)}*}
                            {$smarty.session.cart.amount}

                        </strong>
                        {* &nbsp;{__("items")} {__("for")}&nbsp;{include file="common/price.tpl" value=$smarty.session.cart.total}*}
                    </span>
                    <i class="ty-icon-down-micro"></i>
                {else}
                    {*<i class="ty-minicart__icon ty-icon-basket empty"></i>*}

                    <span class="ty-minicart-title empty-cart ty-hand">0</span>
                    <i class="ty-icon-down-micro"></i>
                {/if}
            {/hook}
        </a>
        </div>
        <div id="dropdown_{$dropdown_id}" class="cm-popup-box ty-dropdown-box__content hidden">
            {hook name="checkout:minicart"}
                <div class="cm-cart-content {if $block.properties.products_links_type == "thumb"}cm-cart-content-thumb{/if} {if $block.properties.display_delete_icons == "Y"}cm-cart-content-delete{/if}">
                        <div class="ty-cart-items">
                            {if $smarty.session.cart.amount}
                                <ul class="ty-cart-items__list">
                                    {hook name="index:cart_status"}
                                        {assign var="_cart_products" value=$smarty.session.cart.products|array_reverse:true}
                                        {foreach from=$_cart_products key="key" item="product" name="cart_products"}
                                            {assign var="gjflex" value=$product.base_price|fn_calculate_gjflex}
                                            {hook name="checkout:minicart_product"}
                                            {if !$product.extra.parent}
                                                <li class="ty-cart-items__list-item">
                                                    <input type="hidden" value="{$product.product_id}" class="ty-cart-items__list-item-pid">
                                                    {hook name="checkout:minicart_product_info"}
                                                    {if $block.properties.products_links_type == "thumb"}
                                                        <div class="ty-cart-items__list-item-image">
                                                            {$product.main_pair =["detailed" => ["object_type" => "product", "image_path" => {get_images_from_blob product_id=$product.product_id count=1} , "alt" => ""]]}
                                                            {include file="common/image.tpl" image_width="40" image_height="40" images=$product.main_pair no_ids=true}
                                                        </div>
                                                    {/if}
                                                    <div class="ty-cart-items__list-item-desc">
                                                        <a href="{"products.view?product_id=`$product.product_id`"|fn_url}">{$product.product_id|fn_get_product_name nofilter}</a>
                                                    <p>
                                                        <span>{$product.amount}</span><span>&nbsp;x&nbsp;</span>{include file="common/price.tpl" value=$product.price span_id="price_`$key`_`$dropdown_id`" class="none"}
                                                        {if $product.gjflex}
                                                            <span>
                                                                +
                                                                 <strong>GjirafaFLEX </strong>
                                                                {include file="common/price.tpl" value=($gjflex * $product.amount) + $product.additional_gjflex span_id="product_price_`$key`" class="ty-sub-price"}
                                                            </span>
                                                        {/if}
                                                        {if ($product.list_price && $product.list_price > $product.price) || $product.display_price > $product.price && !$product.gjflex}
                                                            {if $product.list_price && $product.list_price > $product.price}
                                                            <span  style="color:darkred;" class="ty-strike"> {include file="common/price.tpl" value=$product.list_price }</span>
                                                            {else}
                                                            <span  style="color:darkred;" class="ty-strike"> {include file="common/price.tpl" value=$product.display_price }</span>
                                                            {/if}
                                                        {/if}
                                                    </p>
                                                    </div>
                                                    {if $block.properties.display_delete_icons == "Y"}
                                                        <div class="ty-cart-items__list-item-tools cm-cart-item-delete">
                                                            {if (!$runtime.checkout || $force_items_deletion) && !$product.extra.exclude_from_calculate}
                                                                {include file="buttons/button.tpl" but_href="checkout.delete.from_status?cart_id=`$key`&redirect_url=`$r_url`" but_meta="cm-ajax cm-ajax-full-render" but_target_id="cart_status*" but_role="delete" but_name="delete_cart_item"}
                                                            {/if}
                                                        </div>
                                                    {/if}
                                                    {/hook}
                                                </li>
                                            {/if}
                                            {/hook}
                                        {/foreach}
                                    {/hook}
                                </ul>
                            {else}
                                <div class="ty-cart-items__empty ty-center">{__("cart_is_empty")}</div>
                            {/if}
                        </div>

                        {if $block.properties.display_bottom_buttons == "Y"}
                        <div class="cm-cart-buttons ty-cart-content__buttons buttons-container{if $smarty.session.cart.amount} full-cart{else} hidden{/if}">
                            <a href="{"checkout.cart"|fn_url}" style="display:block;" rel="nofollow" class="ty-btn ty-btn__secondary">{__("view_cart")}</a>
                            {*<div class="ty-float-left">*}
                                {**}
                            {*</div>*}
                            {if $settings.General.checkout_redirect != "Y"}
                            <div class="ty-float-right">
                                <a href="{"checkout.checkout"|fn_url}" rel="nofollow" class="ty-btn ty-btn__primary">{__("checkout")}</a>
                            </div>
                            {/if}
                        </div>
                        {/if}
                    {if {__("before_order_message")} != ''}
{*                        <div>*}
{*                            <p class="before_order_message">{__("before_order_message")}</p>*}
{*                        </div>*}
                    {/if}
                </div>
            {/hook}
        </div>
    <!--cart_status_{$dropdown_id}--></div>
{/hook}

<script>
    var product_ids = [];

    {foreach from=$smarty.session.cart.products item='product'}
        product_ids.push(`{$product.product_id}`);
    {/foreach}

    $(document).on('click', '#cart_content_dropdown', function(e) {
        gjdmp.tr("CartView", {
            id: 0,
            user_id: $('#store_user').val(),
            items_count: `{$smarty.session.cart.amount}`,
            value: `{$smarty.session.cart.subtotal}`,
            list: product_ids
        });
    });
</script>