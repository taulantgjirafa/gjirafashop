<div id="checkout_info_products_{$block.snapping_id}">
    <ul class="ty-order-products__list order-product-list">
        {hook name="block_checkout:cart_items"}
        {foreach from=$cart_products key="key" item="product" name="cart_products"}
            {hook name="block_checkout:cart_products"}
            {if !$cart.products.$key.extra.parent}
                <li class="ty-order-products__item">
                    <div class="ty-cart-items__list-item-image">
                        {$product.main_pair =["detailed" => ["object_type" => "product", "image_path" => {get_images_from_blob product_id=$product.product_id count=1} , "alt" => ""]]}
                        {include file="common/image.tpl" image_width="40" image_height="40" images=$product.main_pair no_ids=true}
                    </div>
                    <a href="{"products.view?product_id=`$product.product_id`"|fn_url}" class="ty-order-products__a">{$product.product nofilter}</a>
                    {if !$product.exclude_from_calculate}
                        {include file="buttons/button.tpl" but_href="checkout.delete?cart_id=`$key`&redirect_mode=`$runtime.mode`" but_meta="ty-order-products__item-delete delete" but_target_id="cart_status*" but_role="delete" but_name="delete_cart_item"}
                    {/if}
                    <div class="ty-order-products__price">
                        {if $cart.products.$key.list_price && $cart.products.$key.list_price > $product.display_price}
                            {$product.display_price = $cart.products.$key.list_price}
                        {/if}
                        {$product.amount}&nbsp;x&nbsp;{include file="common/price.tpl" value=$product.display_price} <strong>{if $cart.products.$key.gjflex} + GjirafaFLEX {/if}</strong>
                    </div>
{*                    {if $product.avail_since != 0 && ($product.avail_since > $smarty.const.TIME)}*}
{*                        <div class="ty-order-products__avail-since">{__('product_coming_soon_date')} <span>{$product.avail_since|date_format}</span></div>*}
{*                    {/if}*}
                    <input type="hidden" class="p_ids" value="{$product.product_id}">
                    {include file="common/options_info.tpl" product_options=$product.product_options no_block=true}
                    {hook name="block_checkout:product_extra"}{/hook}
                </li>
            {/if}
            {/hook}
        {/foreach}
            <span class="product_id" style="display: none;">{$product['product_id']}</span>
        {/hook}
    </ul>
    <!--checkout_info_products_{$block.snapping_id}--></div>