{if $completed_steps.step_two}
    <div class="ty-order-info">
        {assign var="profile_fields" value="I"|fn_get_profile_fields}
        
        {if $profile_fields.B}
            <p class="ty-order-info__title">Lloji i porosisë:</p>

            <ul class="ty-order-info__profile-field clearfix">
                <li class="ty-order-info__profile-field-item">
                    <a href="{"checkout.checkout&edit_step=step_two&from_step=step_three"|fn_url}" class="cm-ajax" title="Kliko për të ndryshuar llojin e porosisë" data-ca-target-id="checkout_*"><span>{if $business_order}Porosi nga biznesi{else}Porosi individuale{/if}</span>
                    </a>
                    <span style="position: relative;">
                        <i class="ty-icon-help-circle ty-hand tooltip-top" style="font-size: 17px;position: relative;top: 4px;"></i>
                        <div class="tltip bbox order-tooltip" style="">{__("order_type")}</div>
                    </span>
                </li>
            </ul>

            <hr class="shipping-adress__delim">

            <p class="ty-order-info__title">{__("billing_address")}:</p>

            <ul id="tygh_billing_adress" class="ty-order-info__profile-field clearfix">
                {foreach from=$profile_fields.B item="field"}
                    {assign var="value" value=$cart.user_data|fn_get_profile_field_value:$field}
                    {if $value}
                        <li class="ty-order-info__profile-field-item {$field.field_name|replace:"_":"-"}">{$value}</li>
                    {/if}
                {/foreach}
            </ul>

            <hr class="shipping-adress__delim" />
        {/if}

        {if $profile_fields.S}
            <p class="ty-order-info__title">{__("shipping_address")}:</p>
            <ul id="tygh_shipping_adress" class="ty-order-info__profile-field clearfix">
                {foreach from=$profile_fields.S item="field"}
                    {assign var="value" value=$cart.user_data|fn_get_profile_field_value:$field}
                    {if $value}
                        <li class="ty-order-info__profile-field-item {$field.field_name|replace:"_":"-"}">{$value}</li>
                    {/if}
                {/foreach}
            </ul>
            <hr class="shipping-adress__delim" />
        {/if}

        {if !$cart.shipping_failed && !empty($cart.chosen_shipping) && $cart.shipping_required}
            <p style="font-weight: bold">{__("shipping_method")}:</p>
            <ul id="tygh_shipping_method">
                {foreach from=$cart.chosen_shipping key="group_key" item="shipping_id"}
                    <li>{$product_groups[$group_key].shippings[$shipping_id].shipping}</li>
                {/foreach}
            </ul>
        {/if}
    </div>
{/if}
{assign var="block_wrap" value="checkout_order_info_`$block.snapping_id`_wrap" scope="parent"}
