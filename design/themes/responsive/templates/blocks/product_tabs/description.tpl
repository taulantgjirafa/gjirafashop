{** block-description:description **}

{if $product.full_description}
    <div {live_edit name="product:full_description:{$product.product_id}"}>{$product.full_description nofilter}</div>
{else if $product.short_description}
    <div {live_edit name="product:short_description:{$product.product_id}"}>{$product.short_description nofilter}</div>
{/if}
{literal} 
<script>
$('#content_description ul').prev().prev().addClass('expandIco');
$('#content_description p.expandIco').click(function(){
//if($(this).next()[0].tagName == "HR")
$(this).toggleClass('active').next().next().toggle();
});

</script>
{/literal}