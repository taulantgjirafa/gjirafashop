{script src="js/lib/jqueryuitouch/jquery.ui.touch-punch.min.js"}
{assign var="is_al" value=$al|fn_isAL}
{$min = $filter.min}
{$max = $filter.max}
{$left = $filter.left|default:$min}
{$right = $filter.right|default:$max}

{if $max - $min <= $filter.round_to}
    {$max = $min + $filter.round_to}
    {$disable_slider = true}
{/if}

{assign var="filter_uid" value=$filter.filter_uid}
{assign var="filter_id" value=$filter.filter_uid}
{if $filter_id == ""}
    {$filter_id = $filter.filter_id}
{/if}
{if $is_al}
    {$filter.prefix = 'Lekë'}
{/if}
<div id="content_29_{$filter_id}"
     class="cm-product-filters-checkbox-container ty-price-slider {if $collapse}hidden{/if} {$extra_class}">
    <div class="ty-price-range">
        <div>
        <span>
            {$filter.prefix nofilter}
            {$filter.suffix nofilter}
        </span>

            <input type="text" class="ty-price-slider__input-text" id="slider_{$filter_uid}_left" name="left_{$filter_uid}" value="{$left}"{if $disable_slider} disabled="disabled"{/if} />

        </div>
        <span>–&nbsp;</span>
        <div>
        <span>
             {$filter.prefix nofilter}
            {$filter.suffix nofilter}
        </span>

            <input type="text" class="ty-price-slider__input-text" id="slider_{$filter_uid}_right" name="right_{$filter_uid}" value="{$right}"{if $disable_slider} disabled="disabled"{/if} />

        </div>
    </div>
    <div id="slider_{$filter_uid}" class="ty-range-slider cm-range-slider">
        <ul class="ty-range-slider__wrapper">
            <li class="ty-range-slider__item" style="left: 0%;"><span
                        class="ty-range-slider__num">{$filter.prefix nofilter}{$min}{$filter.suffix nofilter}</span>
            </li>
            <li class="ty-range-slider__item" style="left: 100%;"><span
                        class="ty-range-slider__num">{$filter.prefix nofilter}{$max}{$filter.suffix nofilter}</span>
            </li>
        </ul>
    </div>

    <input id="elm_checkbox_slider_{$filter_uid}" data-ca-filter-id="{if $filter_id == ''}1171{else}{$filter_id}{/if}"
           class="cm-product-filters-checkbox hidden" type="checkbox" name="product_filters[{$filter.filter_id}]"
           value=""/>
    <input type="hidden" id="filter_id" value="{$filter_id}"/>
    <input type="hidden" id="price_min_val" value="{$min}"/>
    <input type="hidden" id="price_max_val" value="{$max}"/>

    {if $right == $left}
        {$right = $left + $filter.round_to}
    {/if}

    {* Slider params *}
    <script data-no-defer> var price_filter_params = {ldelim} disabled: {$disable_slider|default:"false"}, left: {$left}, right: {$right}, min: {$min}, max: {$min}, step: {$filter.round_to}, extra: "{$filter.extra}" {rdelim} </script>
    {* /Slider params *}
</div>
