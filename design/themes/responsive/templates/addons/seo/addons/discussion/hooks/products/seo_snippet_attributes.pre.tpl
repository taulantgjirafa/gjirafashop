{$review_count = $product.discussion.search.total_items}
{$rating_value = $product.discussion.average_rating}

{if $review_count == 0}
    {$review_count = 3}
{/if}

{if $rating_value == 0}
    {$rating_value = 3}
{/if}

{if $product.discussion.search.total_items && $product.discussion.average_rating}
<div itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
    <meta itemprop="reviewCount" content="{$review_count}">
    <meta itemprop="ratingValue" content="{$rating_value}">
</div>
{/if}