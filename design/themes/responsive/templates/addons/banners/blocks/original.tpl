{** block-description:original **}

{foreach from=$items item="banner" key="key"}
    <style>
        #progressBar {
            bottom: 27px !important;
        }
    </style>
    {if $banner.type == "G"}
    <div class="ty-banner__image-wrapper">
        {if $banner.url != ""}<a href="{$banner.url|fn_url}" {if $banner.target == "B"}target="_blank"{/if}>{/if}
            {assign var="azure_path" value=("https://hhstsyoejx.gjirafa.net/gj50/banners/`$banner.image_path`")}
            {$banner.main_pair =["detailed" => ["object_type" => "product", "image_path" => $azure_path , "alt" => $banner.banner]]}
        {include file="common/banner-image.tpl" images=$banner.main_pair image_auto_size=true}
        {if $banner.url != ""}</a>{/if}
    </div>
    {else}
        <div class="ty-wysiwyg-content">
            {$banner.description nofilter}
        </div>
    {/if}
{/foreach}

