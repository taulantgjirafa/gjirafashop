{assign var="cr_is_al" value=$c|fn_isAL}
{if $cr_is_al}
    {assign var="cr_is_al" value="true"}
{else}
    {assign var="cr_is_al" value="false"}
{/if}

{assign var="disable_flex_categories" value=','|explode:{__('disable_flex')}}
{assign var="check_category" value=$disable_flex_categories|array_intersect:$product.category_ids}

<div id="{$id}" style="box-sizing: border-box;">

    <form name="call_requests_form{if !$product}_main{/if}" id="form_{$id}" action="{""|fn_url}" method="post"
          class="cr-form">
        <input type="hidden" name="result_ids" value="{$id}"/>
        <input type="hidden" name="return_url" value="{$config.current_url}"/>
        <input type="hidden" name="obj_prefix" value="{$obj_prefix}"/>
        {if $cr_auth}
            <input type="hidden" name="cr_product_data[email]" value="{$cr_user_info.email}">
        {/if}
        <input type="hidden" name="cr_product_data[amount]" value="1">
        <input type="hidden" name="cr_product_data[gjflex]" value="false">
        <input type="hidden" name="cr_product_data[fields][37]" value="{if $al|fn_isAL}115{else}2{/if}">
        {if $product}
            <input type="hidden" name="cr_product_data[product_id]" value="{$product.product_id}"/>
            <div class="ty-cr-product-info-container flexing flex-v-center flex-h-center" style="max-width: 100%;">
                <div class="ty-cr-product-info-image">
                    {assign var="azure_path" value="{get_images_from_blob product_id=$product.product_id count=1}"}
                    {$product.main_pair =["detailed" => ["object_type" => "product", "image_path" => $azure_path , "alt" => $product.product]]}
                    {include file="common/image.tpl" images=$product.main_pair}
                </div>

                <div class="ty-cr-product-info-header" style="padding:0 10px;">
                    <h4 class="mb10 max-rows mr2">{$product.product}</h4>
                    <div class="flexing flex-h-between flex-wrap">
                        <strong class="mb5"
                                id="cr_price">{include file="common/price.tpl" value=$product.price}</strong> |
                        <strong class="mb5" id="cr_amount"></strong> |
                        <strong class="mb5" id="cr_gjflex"></strong>
                    </div>
                </div>
            </div>
        {/if}
        <hr style="margin-top:15px;">
        <div class="mb20 mt10">
            <h4>
                Ju jeni duke blerë si:
            </h4>
            <label class="mr15">
                <input type="radio" name="buy_type" checked data-buy-type="individ"/>
                Individ
            </label>
            <label>
                <input type="radio" name="buy_type" data-buy-type="business"/>
                Biznes
            </label>
        </div>
        {if $cr_auth}
            <div class="flexing flex-h-between main_ip_con">
                <div class="ty-control-group">
                    <label class="ty-control-group__title  cm-required cm-trim"
                           for="cr_product_data_{$id}_name">{__("first_name")}</label>
                    <input id="cr_product_data_{$id}_name" size="50" class="ty-input-text-full" type="text"
                           name="cr_product_data[name]" value="{$cr_user_info.s_firstname}"/>
                </div>

                <div class="ty-control-group">
                    <label class="ty-control-group__title  cm-required cm-trim"
                           for="cr_product_data_{$id}_lastname">{__("last_name")}</label>
                    <input id="cr_product_data_{$id}_lastname" size="50" class="ty-input-text-full" type="text"
                           name="cr_product_data[lastname]" value="{$cr_user_info.s_lastname}"/>
                </div>

                <div class="ty-control-group">
                    <label for="cr_product_data_{$id}_phone"
                           class="ty-control-group__title cm-cr-mask-phone-lbl cm-required cm-trim">{__( "phone")}</label>
                    <input id="cr_product_data_{$id}_phone" class="ty-input-text-full cm-cr-mask-phone" size="50"
                           type="text" name="cr_product_data[phone]" value="{$cr_user_info.s_phone}"/>
                    <span id="spnPhoneStatus"></span>
                </div>

                <div class="ty-control-group">
                    <label for="cr_product_data_{$id}_email"
                           class="ty-control-group__title cm-required cm-trim cm-email">{__("email")}</label>
                    <input id="cr_product_data_{$id}_email" class="ty-input-text-full" size="50" type="text"
                           name="cr_product_data[email]" value="{$cr_user_info.email}" disabled/>
                </div>

                <div class="ty-control-group">
                    <label for="cr_product_data_{$id}_address" class="ty-control-group__title cm-required cm-trim">{__(
                        "address")}</label>
                    <input id="cr_product_data_{$id}_address" class="ty-input-text-full" size="50" type="text"
                           name="cr_product_data[address]" value="{$cr_user_info.s_address}"/>
                </div>

                <div class="ty-control-group">
                    <label for="cr_product_data_{$id}_city" class="ty-control-group__title cm-required cm-trim">{__(
                        "city")}</label>
                    <select name="cr_product_data[fields][{if $al|fn_isAL}54{else}40{/if}]"
                            id="cr_product_data_{$id}_city"
                            class="ty-profile-field__select">
                        {foreach from=$cities.values key='key' item='city'}
                            <option value="{$key}" {if $cr_city_id == $key}selected{/if}>{$city}</option>
                        {/foreach}
                    </select>
                </div>

                <div class="ty-control-group hidden ty-cr-toggle">
                    <label for="cr_product_data_{$id}_company_name" class="ty-control-group__title" id="test1">Emri i
                        kompanisë</label>
                    <input id="cr_product_data_{$id}_company_name" class="ty-input-text-full" size="50" type="text"
                           name="cr_product_data[fields][43]"/>
                </div>

                <div class="ty-control-group hidden ty-cr-toggle">
                    <label for="cr_product_data_{$id}_fiscal_number" class="ty-control-group__title" id="test2">Numri
                        fiskal</label>
                    <input id="cr_product_data_{$id}_fiscal_number" class="ty-input-text-full" size="50" type="text"
                           name="cr_product_data[fields][51]"/>
                </div>
            </div>
            <div class="ty-control-group">
                <label for="cr_product_data_{$id}_shipping"
                       class="mb5 ty-control-group__title cm-required cm-trim">{__("shipping")}</label>
                {foreach from=$shippings key='key' item='shipping'}
                    <div class="mt15" id="cr_shipping_container_{$shipping.shipping_id}">
                        <input type="radio" name="cr_product_data[shipping_id]" value="{$shipping.shipping_id}"
                               id="cr_shipping_{$key}" {if $key == 0}checked{/if}>
                        <label for="cr_shipping_{$key}">{$shipping.shipping}<span style="font-weight: 400;"> (Data e arritjes: {$delivery_dates.standard_range|date_format:$settings.Appearance.date_format})</span></label>
                    </div>
                {/foreach}
            </div>
            <div class="ty-control-group  mt20">
                <label class="cm-required cm-trim mb15" style="display: block;">Pagesa <span class="text-muted"
                                                                                             style="font-size:12px; font-weight:400;">(Zgjedhni njërën nga metodat e pagesës)</span></label>
                <div class="pm">
                    {assign var=pcounter1 value=0}
                    {foreach from=$payments item='payment'}
                        {assign var=pcounter1 value=$pcounter1 + 1}
                        <label class="flexing flex-h-between flex-v-center pm_item">
                            <input type="radio" name="cr_product_data[payment_id]"
                                   value="{$payment.payment_id}"
                                    {if $product.license_key_product}
                                        checked
                                    {else}
                                        {if $pcounter1 == 1}checked{/if}
                                    {/if}
                            />
                            <div class="flexing flex-h-between flex-v-center pm_inner">
                                <div>
                                    <strong>{$payment.payment}</strong>
                                </div>
                                <div>
                                    <img src="https://hhstsyoejx.gjirafa.net/gj50/payments/{$payment.payment_id}.png"
                                         alt="">
                                </div>
                            </div>
                        </label>
                    {/foreach}
                </div>
            </div>
        {else}
            <div class="flexing flex-h-between main_ip_con">
                <div class="ty-control-group">
                    <label class="ty-control-group__title  cm-required cm-trim"
                           for="cr_product_data_{$id}_name">{__("name")}</label>
                    <input id="cr_product_data_{$id}_name" size="50" class="ty-input-text-full" type="text"
                           name="cr_product_data[name]" value="{$cr_product_data.name}"/>
                </div>

                <div class="ty-control-group">
                    <label class="ty-control-group__title  cm-required cm-trim"
                           for="cr_product_data_{$id}_lastname">{__("last_name")}</label>
                    <input id="cr_product_data_{$id}_lastname" size="50" class="ty-input-text-full" type="text"
                           name="cr_product_data[lastname]" value="{$cr_product_data.lastname}"/>
                </div>

                <div class="ty-control-group">
                    <label for="cr_product_data_{$id}_phone"
                           class="ty-control-group__title cm-cr-mask-phone-lbl cm-required cm-trim">{__( "phone")}</label>
                    <input id="cr_product_data_{$id}_phone" class="ty-input-text-full cm-cr-mask-phone" size="50"
                           type="text" name="cr_product_data[phone]" value="{$cr_product_data.phone}"/>
                    <span id="spnPhoneStatus"></span>
                </div>

                <div class="ty-control-group">
                    <label for="cr_product_data_{$id}_email"
                           class="ty-control-group__title cm-required cm-trim cm-email">{__("email")}</label>
                    <input id="cr_product_data_{$id}_email" class="ty-input-text-full" size="50" type="text"
                           name="cr_product_data[email]" value="{$cr_product_data.email}"/>
                </div>

                <div class="ty-control-group">
                    <label for="cr_product_data_{$id}_address" class="ty-control-group__title cm-required cm-trim">{__(
                        "address")}</label>
                    <input id="cr_product_data_{$id}_address" class="ty-input-text-full" size="50" type="text"
                           name="cr_product_data[address]" value="{$cr_user_info.s_address}"/>
                </div>

                <div class="ty-control-group">
                    <label for="cr_product_data_{$id}_city" class="ty-control-group__title cm-required cm-trim">{__(
                        "city")}</label>
                    <select name="cr_product_data[fields][{if $al|fn_isAL}54{else}40{/if}]"
                            id="cr_product_data_{$id}_city"
                            class="ty-profile-field__select">
                        {foreach from=$cities.values key='key' item='city'}
                            <option value="{$key}">{$city}</option>
                        {/foreach}
                    </select>
                </div>

                <div class="ty-control-group hidden ty-cr-toggle">
                    <label for="cr_product_data_{$id}_company_name" class="ty-control-group__title" id="test1">Emri i
                        kompanisë</label>
                    <input id="cr_product_data_{$id}_company_name" class="ty-input-text-full" size="50" type="text"
                           name="cr_product_data[fields][43]"/>
                </div>

                <div class="ty-control-group hidden ty-cr-toggle">
                    <label for="cr_product_data_{$id}_fiscal_number" class="ty-control-group__title" id="test2">Numri
                        fiskal</label>
                    <input id="cr_product_data_{$id}_fiscal_number" class="ty-input-text-full" size="50" type="text"
                           name="cr_product_data[fields][51]"/>
                </div>
            </div>
            <div class="ty-control-group">
                <label style="padding-bottom: 0;" for="cr_product_data_{$id}_shipping"
                       class="ty-control-group__title cm-required cm-trim">{__("shipping")}</label>
                {foreach from=$shippings key='key' item='shipping'}
                    <div class="mt15">
                        <input type="radio" name="cr_product_data[shipping_id]" value="{$shipping.shipping_id}"
                               id="cr_shipping_{$key}" {if $key == 0}checked{/if}>
                        <label for="cr_shipping_{$key}">{$shipping.shipping}<span style="font-weight: 400;"> (Data e arritjes: {$delivery_dates.standard_range|date_format:$settings.Appearance.date_format})</span></label>
                    </div>
                {/foreach}
            </div>
            <div class="ty-control-group  mt20">
                <label class="cm-required cm-trim mb15" style="display: block;">Pagesa <span class="text-muted"
                                                                                             style="font-size: 12px; font-weight: 400;">(Zgjedhni njërën nga metodat e pagesës)</span></label>
                <div class="pm">
                    {assign var=pcounter2 value=0}
                    {foreach from=$payments item='payment'}
                        {assign var=pcounter2 value=$pcounter2 + 1}
                        <label class="flexing flex-h-between flex-v-center pm_item">
                            <input type="radio" name="cr_product_data[payment_id]"
                                   value="{$payment.payment_id}" {if $pcounter2 == 1}checked{/if} />
                            <div class="flexing flex-h-between flex-v-center pm_inner">
                                <div>
                                    <strong>{$payment.payment}</strong>
                                </div>
                                <div>
                                    <img src="https://hhstsyoejx.gjirafa.net/gj50/payments/{$payment.payment_id}.png"
                                         alt="">
                                </div>
                            </div>
                        </label>
                    {/foreach}
                </div>
            </div>
        {/if}

        {include file="common/image_verification.tpl" option="call_request"}

        <div class="ty-control-group">
            <label class="ty-control-group__title"
                   for="cr_product_data_{$id}_coupon_code">{__("promo_code_or_certificate")}</label>
            <input id="cr_product_data_{$id}_coupon_code" size="50" class="ty-input-text-full" type="text"
                   name="cr_product_data[coupon_code]" value=""/>
        </div>

        <div class="ty-control-group">
            <div class="cm-field-container">
                {strip}
                    <label for="cr_product_data_{$id}_accept_terms" class="cm-required cm-trim cm-check-agreement">
                        <input style="margin-right: 7px;" type="checkbox" id="cr_product_data_{$id}_accept_terms"
                               name="cr_product_data[terms]" class="cm-agreement checkbox"/>
                        {capture name="terms_link"}
                            <a target="_blank" href="{'pages.terms-and-conditions'|fn_url}">
                                {__("checkout_terms_n_conditions_name")}
                            </a>
                        {/capture}
                        {__("checkout_terms_n_conditions", ["[terms_href]" => $smarty.capture.terms_link])}
                    </label>
                {/strip}

                <div class="hidden" id="terms_and_conditions_">
                    {__("terms_and_conditions_content")}
                </div>
            </div>
        </div>

        <div class="buttons-container">
            {include file="buttons/button.tpl" but_name="dispatch[call_requests.request]" but_text=__("buy_now") but_role="submit" but_meta="ty-btn__primary ty-btn__big cm-form-dialog-closer ty-btn w100 pt10 pb10 one-click-buy-order"}
        </div>
    </form>

    <!--{$id}-->
</div>

<style>
    .ui-dialog.ui-widget.ui-widget-content.ui-corner-all.ui-front {
        height: auto !important;
    }

    .object-container {
        height: calc(100vh - 30px) !important;
    }

    .ty-cr-product-info-header {
        width: 100% !important;
        max-width: 260px !important;
    }

    * {
        box-sizing: border-box;
    }

    .ty-cr-product-info-image img {
        height: 100px;
    }

    .main_ip_con {
        flex-wrap: wrap;
    }

    .main_ip_con .ty-control-group {
        width: calc(50% - 10px);
    }

    .buttons-container {
        position: sticky !important;
        margin: 0 !important;
        background: #fff !important;
        padding: 15px 0;
        bottom: 0 !important;
    }

    @media screen and (min-width: 768px) {
        .pm_item {
            min-height: 102px;
        }
    }

    .pm {
        display: flex;
        flex-wrap: wrap;
    }

    .pm_item {
        margin-bottom: 10px;
        width: calc(33% - 10px);
        box-sizing: border-box;
        position: relative;
        margin: 5px;
    }

    .pm_item input {
        position: absolute;
        pointer-events: none;
        opacity: 0;
    }

    .pm_item input:checked ~ .pm_inner {
        background: #fafafa;
        border: 2px solid #e65228;
    }

    .pm_inner {
        border: 2px solid #ddd;
        text-align: center;
        border-radius: 4px;
        cursor: pointer;
        padding: 10px;
        box-sizing: border-box;
        width: 100%;
        transition: border-color .3s ease, background .3s ease;
        flex-direction: column;
        height: 100%;
    }

    .pm_inner strong {
        margin-bottom: 10px;
        display: block;
    }

    .pm_item:hover {
        border-color: #999;
    }

    .pm_item img {
        max-height: 38px;
    }

    label {
        font-weight: 700 !important;
    }

    input {
        border-radius: 4px !important;
    }

    .object-container {
        width: 700px;
        /*height: auto !important;*/
    }

    /*.ui-icon-closethick

    {*/
            /*    top: -10px !important;*/
            /*    right: 0;*/
            /*    width: 30px !important;*/
            /*    height: 30px !important;*/
            /*    text-align: center;*/
            /*    line-height: 30px !important;*/
            /*    border-radius: 100%;*/
            /*    background: #fff !important;*/
            /*    box-shadow: 0 1px 5px -2px #000000;*/
            /*}  */
    .dialog-is-open {
        overflow: hidden !important;
    }

    @media screen and (max-width: 768px) {
        .mt20 {
            margin-top: 20px !important;
        }

        .dialog-is-open .zopim {
            display: none !important;
        }

        .buttons-container {
            margin: 0 -15px !important;
            padding: 10px 15px;
        }

        .ty-cr-product-info-image img {
            height: 50px;
        }

        hr {
            margin-top: 10px;
            margin-bottom: 0;
        }

        .pm_shipping label {
            margin-bottom: 0;
        }

        .pm_shipping .mr20 {
            display: block;
            margin-top: 5px;
        }

        /*.ui-icon-closethick

    {*/
                /*    top:0  !important;*/
                /*}*/
        .object-container {
            width: 100%;
            height: calc(100vh) !important;
            padding: 15px;
            padding-bottom: 11px;
        }

        .pm {
            white-space: nowrap;
            overflow-y: hidden;
            overflow-x: auto;
            flex-wrap: nowrap;
            margin-left: -10px;
            margin-right: -10px;
        }

        .pm_item {
            white-space: normal;
            width: 210px;
            margin-right: 10px;
            flex: none;
        }

        .pm_item:first-child {
            margin-left: 10px;
        }

        .ty-control-group {
            box-shadow: none;
            padding: 0;
            margin: 10px 0;
        }
    }

    .ty-profile-field__select {
        border-color: #ddd;
        border-radius: 4px;
        height: 32px;
    }
</style>
<script>
    var cr_product_price = Math.max({$product.price}, {$product.base_price}, {$product.list_price});
    var cr_final_product_price = {$product.price};
    var cr_is_al = {$cr_is_al};

    cr_get_product_data();

    document.getElementById('cr_ocb').addEventListener('click', function () {
        cr_get_product_data();
    });

    function cr_get_product_data() {
        let amount = document.getElementsByClassName('cm-amount')[0].value;
        let gjflex = false;

        {*if ('{count($check_category)}' === '0' && '{$product.license_key_product}' === '0') {*}
        if ('{count($check_category)}' === '0') {
            gjflex = document.querySelector('input[id^="gjflexyes_"]').checked;
        }

        if (amount > 1) {
            document.getElementById('cr_price').innerHTML = amount + ' x ' + cr_final_product_price + (cr_is_al ? ' Lekë' : ' €');

            if (amount > {$product.real_amount}) {
                if ($('#cr_shipping_container_7').length > 0) {
                    document.getElementById('cr_shipping_container_7').remove();
                    document.querySelectorAll('[id^="cr_shipping_container_"]')[0].firstElementChild.checked = true;
                }
            }
        }

        document.getElementById('cr_amount').innerHTML = amount + ((amount == 1) ? ' artikull' : ' artikuj');
        document.getElementById('cr_gjflex').innerHTML = gjflex ? ('GjirafaFLEX: ' + cr_calculate_gjflex(cr_product_price)) : 'GjirafaFLEX: Jo';
        document.getElementsByName('cr_product_data[amount]')[0].value = amount;
        document.getElementsByName('cr_product_data[gjflex]')[0].value = gjflex;
    }

    function cr_calculate_gjflex(price) {
        let multiplier = 1;
        let final_price = 0;

        if (cr_is_al) {
            multiplier = 130;
        }

        price_limit_1 = 10 * multiplier;
        price_limit_2 = 218 * multiplier;
        return_price = 1 * multiplier;
        formula_num_2 = 40 * multiplier;
        extra = 0.50 * multiplier;
        formula_num_1 = 410 * multiplier;

        if (price < price_limit_1) {
            return return_price;
        } else if (price > price_limit_1 && price < price_limit_2) {
            final_price = parseInt(price * (((formula_num_1 - price) / formula_num_2) / 100)) + extra;
            final_price = cr_is_al ? final_price.toLocaleString() : final_price;
            return final_price + (cr_is_al ? ' Lekë' : '0 €');
        } else {
            final_price = parseInt(price * 0.048) + extra;
            final_price = cr_is_al ? final_price.toLocaleString() : final_price;
            return final_price + (cr_is_al ? ' Lekë' : '0 €');
        }
    }

    $('input[name="buy_type"]').click(function () {
        var radio = $(this);

        if (radio.data('buy-type') === 'business') {
            $('.ty-cr-toggle').removeClass('hidden');
            $('.ty-cr-toggle label').addClass('cm-required cm-trim');
        } else {
            $('.ty-cr-toggle').addClass('hidden');
            $('.ty-cr-toggle label').removeClass('cm-required cm-trim');
        }
    });
</script>