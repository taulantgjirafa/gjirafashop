{if $is_wishlist}
<div class="ty-twishlist-item">
    <a href="{"wishlist.delete?product_id=`$product.product_id`"|fn_url}" class="cm-ajax cm-ajax-full-render ty-twishlist-item__remove ty-remove" data-ca-target-id="wishlist_ajax_container" rel="nofollow" rev="wishlist_ajax_container" title="{__("remove")}"><span class="ty-twishlist-item__txt ty-remove__txt">{__("remove")}</span><i class="ty-remove__icon ty-icon-cancel-circle"></i></a>
</div>
{/if}