{if $product._score == null && $product.price != 0 && !$a|fn_isAL && $block.type == 'main'}
{*    {assign var="starcard_six_only" value=41|in_array:$product.category_ids}*}
    {assign var="starcard_six_only" value=false}
        <span class="starcard">
            <hr>
            <p class="no-margin">
                Paguaj me <strong>Raiffeisen Bonus Kartelë</strong> <a href="https://www.raiffeisen-kosovo.com/cards/apply/apply-bonus-card.php" class="ty-btn" style="    color: #000;padding: 2px 5px;background: transparent;text-decoration: underline;font-weight: bold;font-size: 12px;" target="_blank">[APLIKO KËTU]</a>dhe <strong>TEB Starcard</strong> deri në {if $starcard_six_only} 6 këste pa kamatë për vetëm {($product.price/6)|string_format:"%.2f"}€ {else} 12 këste pa kamatë për vetëm {($product.price/12)|string_format:"%.2f"}€ {/if} në muaj &nbsp;<span class="ty-tooltip-block"><a class="cm-tooltip" style="text-decoration: none;" title='Për të vazhduar me përzgjedhjen e mënyrës së pagesës, ju lutem klikoni  "Shto në shportë". Pastaj tek mënyrat e pagesës klikoni "Paguaj me këste (Gold Bonus dhe Starcard)"'><i style="font-size: 19px;" class="ty-icon-help-circle"></i></a></span>
            </p>
        </span>
{/if}