{if $product.amount != 0}
    {$show_one_click_buy = $addons.call_requests.buy_now_with_one_click == "Y" && $product.amount && (!$smarty.session.auth.user_id != 0 || ($smarty.session.auth.user_id && ($smarty.session.auth.order_count == null || $smarty.session.auth.order_count == 0))) && ($product.avail_since < $smarty.const.TIME)}
    {$show_buy_now = $settings.General.buy_now == 'Y' && $product.avail_since < $smarty.const.TIME}

    {if $show_one_click_buy}
        <div class="mt15" id="cr_ocb">
            <div class="one_click_buy">
                {include file="common/popupbox.tpl"
                href="call_requests.request?product_id={$product.product_id}&obj_prefix={$obj_prefix}"
                link_text=__("call_requests.buy_now_with_one_click")
                text=__("call_requests.buy_now_with_one_click")
                id="call_request_{$obj_prefix}{$product.product_id}" link_meta="" content=""
                }
            </div>
        </div>
    {/if}

    {if $show_buy_now && !$show_one_click_buy}
        <div class="ty-btn__buy-now" style="width: calc(70% + 70px);">
            <form action="{"checkout.add..{$product.product_id}"|fn_url}" method="post" class="pd-buttons flexing flex-v-center" id="buy_now_form">
                <input type="hidden" name="result_ids" value="cart_status*,wish_list*,checkout*,account_info*"/>
                <input type="hidden" name="redirect_url" value="{'checkout.checkout'|fn_url}"/>
                <input type="hidden" name="product_data[{$obj_id}][product_id]" value="{$obj_id}"/>
                <input type="hidden" name="product_data[{$obj_id}][amount]" value="1" id="buy_now_amount"/>
                <input type="hidden" name="product_data[{$obj_id}][gjflex]" value="" id="buy_now_gjflex"/>

                <button class="ty-btn__primary ty-btn__big ty-btn__add-to-cart ty-btn ty-btn__buy-now-button" type="submit" name="dispatch[checkout.add..{$product.product_id}]">
                    {__('buy_now')}
                </button>
            </form>
        </div>
    {/if}

    {if $settings.General.call_to_order == 'Y'}
        <div class="ty-btn__call-now" style="width: calc(70% + 70px); position: relative;">
            {assign var="phone_number" value=($al|fn_isAL) ? '+355 68 803 0303' : '+383 45 101 953'}
            <a href="tel:{$phone_number}" class="ty-btn btn-click-to-call ty-btn__call-now-button tooltip-top">{__('call_to_order')}: {$phone_number}</a>
{*            <div class="tltip bbox call-to-order-tooltip" style="">{__("call_to_order_help")}</div>*}
        </div>
    {/if}
{/if}

{* {if ($product.avail_since > $smarty.const.TIME)}
    {include file="common/coming_soon_notice.tpl" avail_date=$product.avail_since add_to_cart=$product.out_of_stock_actions}
{/if} *}