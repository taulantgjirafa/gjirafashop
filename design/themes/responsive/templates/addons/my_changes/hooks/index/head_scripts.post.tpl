{if !$smarty.const.DEVELOPMENT}
    {literal}
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script data-no-defer async src="https://www.googletagmanager.com/gtag/js?id=UA-106862121-1"></script>
        <noscript>
            <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5D9JVBD"
                    height="0" width="0" style="display:none;visibility:hidden"></iframe>
        </noscript>
    {/literal}
{/if}

{literal}<script type="application/ld+json" data-no-defer>{"@context":"http://schema.org","@type":"Organization","url":"http://www.gjirafa50.com","logo":"https://gjirafa50.com/images/appicon/50-112px.png","contactPoint":[{"@type":"ContactPoint","telephone":"+38345101953","contactType":"customerservice","areaServed":"XK"},{"@type":"ContactPoint","telephone":"+355688030303","contactType":"customerservice","areaServed":"AL"}]}</script>{/literal}