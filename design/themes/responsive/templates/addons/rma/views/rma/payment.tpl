<div class="ty-center" style="
    padding: 80px 0;
    position: relative;
">
    <div class="arrow-box">
        <div class="arrow-round"></div>
    </div>
    <img style="padding-bottom: 30px;" src="images/order-stepper/B.svg" alt="" width="100">
    <h1 style="padding-top: 30px;">Faleminderit per pagesen!</h1>
    <h3 style="padding-bottom: 25px;">Produktin tuaj do vi korrieri ta marre tek adresa juaj qe e keni dhene</h3>
    {include file="buttons/continue_shopping.tpl" but_role="text" but_meta="ty-checkout-complete__button-vmid" but_href=$continue_url|fn_url}
</div>

<script>
    $(document).ready(function() {
        $('.arrow-round').css({
            'transform': 'rotate(-1turn)',
            'opacity': '1',
            'transition': '1s cubic-bezier(0.645, 0.045, 0.355, 1)'
        });
    });

    document.title = "Konfirmimi i kthimit";
</script>