<style type="text/css">
    .disabled {
        pointer-events: none;
    }

    @media (max-width: 768px) {
        .ty-table__responsive-header {
            width: 15%;
        }

        .reason-select {
            width: 80%;
        }

        .reason-field-container textarea {
            width: 100%;
        }
    }
</style>

<div class="ty-rma-register">
    <form action="{""|fn_url}" method="post" name="return_registration_form">
    <input name="order_id" type="hidden" value="{$smarty.request.order_id}" />
    <input name="user_id" type="hidden" value="{$order_info.user_id}" />
    <input name="dispatch[rma.add_return]" type="hidden">

{*    {if $actions}*}
{*        <div class="ty-rma-register__actions">*}
{*            <strong>{__("what_you_would_like_to_do")}:</strong>*}
{*            <select class="ty-rma-register__action-select" name="action">*}
{*                {foreach from=$actions item="action" key="action_id"}*}
{*                    <option value="{$action_id}">{$action.property}</option>*}
{*                {/foreach}*}
{*            </select>*}
{*        </div>*}
{*    {/if}*}

    <table class="ty-table ty-rma-register__table">
        <thead>
            <tr>
{*                <th class="ty-center" style="text-align: center;"><input type="checkbox" name="check_all" value="Y" title="{__("check_uncheck_all")}" class="checkbox cm-check-items" /></th>*}
                <th>{__("product")}</th>
                <th class="ty-right">{__("price")}</th>
                <th>{__("quantity")}</th>
                <th>{__("reason")}</th>
{*                <th>{__("status")}</th>*}
                <th>{__("rma_return_action")}</th>
            </tr>
        </thead>
    <tbody>
    {foreach from=$order_info.products item="oi" key="key"}
        <tr class="rma-row">
{*            <td class="ty-center ty-rma-register-id">*}
{*                <input type="checkbox" name="returns[{$oi.cart_id}][chosen]" id="delete_checkbox" value="Y" class="checkbox cm-item" />*}
                <input type="hidden" name="returns[{$oi.cart_id}][product_id]" value="{$oi.product_id}" />
                <input type="hidden" name="returns[{$oi.cart_id}][product_code]" value="{$oi.product_code}" /></td>
{*            </td>*}
            <td class="ty-left"><a href="{"products.view?product_id=`$oi.product_id`"|fn_url}">{$oi.product nofilter}</a>
                {if $oi.product_options}
                    {include file="common/options_info.tpl" product_options=$oi.product_options}
                {/if}
            </td>
            <td class="ty-right ty-nowrap">
                {if $oi.extra.exclude_from_calculate}{__("free")}{else}{include file="common/price.tpl" value=$oi.price}{/if}
            </td>
            <td class="ty-center">
                <input type="hidden" name="returns[{$oi.cart_id}][available_amount]" value="{$oi.amount}" />
                <select name="returns[{$oi.cart_id}][amount]">
                    <option value="0">0</option>
                    {section name=$key loop=$oi.amount+1 start="1" step="1"}
                        <option value="{$smarty.section.$key.index}">{$smarty.section.$key.index}</option>
                    {/section}
                </select>
            </td>
            <td class="ty-center">
                {if $reasons}
                    <select name="returns[{$oi.cart_id}][reason]" class="reason-select">
                    {foreach from=$reasons item="reason" key="reason_id"}
                        <option value="{$reason_id}">{$reason.property}</option>
                    {/foreach}
                    </select>
                    <div class="reason-field-container" style="display: none;">
                        <textarea name="returns[{$oi.cart_id}][reason_description]" cols="29" rows="4" maxlength="500" class="reason-field" placeholder="Ju lutem shënoni arsyen..." style="/*display: none;*/ resize: none; margin: 0 auto; margin-top: 10px;"></textarea>
                        <label for="reason-field" class="reason-field-label" hidden>Fusha <b>Arsye</b> është e detyrueshme.</label>
                    </div>
                {/if}
            </td>
{*            <td class="ty-left">*}
{*                {if $statuses}*}
{*                <fieldset class="status-array" style="border: none; padding: 0.35em 0.625em;" name="returns[{$oi.cart_id}][fieldset]">*}
{*                    {foreach from=$statuses item="status" key="status_id"}*}
{*                    <div>*}
{*                        <label *}{* for="{$status_id}" *}{*>*}
{*                            <input type="checkbox" id="{$status_id}" name="returns[{$oi.cart_id}][status][]" value="{$status_id}" name="{$status.property}">*}
{*                            <span>*}
{*                                {$status.property}*}
{*                            </span>*}
{*                        </label>*}
{*                    </div>*}
{*                    {/foreach}*}
{*                </fieldset>*}
{*                <span class="help-inline status-error" style="display: none;"><p>Ju lutem zgjedhni së paku një opsion.</p></span>*}
{*                {/if}*}
{*            </td>*}
            <td class="ty-center">
                <select class="ty-rma-register__action-select" name="returns[{$oi.cart_id}][action]">
                    {foreach from=$actions item="action" key="action_id"}
                        <option value="{$action_id}">{$action.property}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
    {foreachelse}
        <tr class="ty-table__no-items">
            <td colspan="6"><p class="ty-no-items">{__("no_items")}</p></td>
        </tr>
    {/foreach}
    </tbody>
    </table>


    <p>
        Kthimet e produkteve të blera tek ne bëhen në dy mënyra:<br/>
        - Duke kthyer produktin dhe marrë një tjetër (me atë shume të parave, duke shtuar më shumë, apo duke u rimbursuar për pjesën e mbetur të shumës më anë të kredisë në platformën përkatëse)<br/>
        - Duke kthyer produktin dhe duke u rimbursuar me kredi në platformën përkatëse.<br/>
        *Ju jeni pergjegjës për të e sjellur produktin tek zyret tona në mënyrë që të bëhet kthimi i tij.<br/>

    </p>
    <div class="ty-rma-register__comments">
        <strong class="ty-rma-register__comments-title">{__("type_comment")}</strong>
        <textarea name="comment" cols="3" rows="4" class="ty-rma-register__comments-textarea" style="padding: 5px 7px;"></textarea>
    </div>
    <div class="ty-rma-register__buttons buttons-container">
        {include file="buttons/button.tpl" but_id="create_return_submit" but_text=__("rma_return") but_name="dispatch[rma.add_return]" but_meta="ty-btn__secondary cm-process-items"}
    </div>

    </form>
</div>

{capture name="mainbox_title"}{__("return_registration")}{/capture}

{literal}
<script type="text/javascript">
    $('select.reason-select').on('change', function() {
        var value = $(this).children('option:selected').val();

        if(value == 13) {
            $(this).next('.reason-field-container').show();
            $(this).next('.reason-field-container').addClass('open-ta');
        } else {
            $(this).next('.reason-field-container').hide();
            $(this).next('.reason-field-container').find('.reason-field').css('border', '1px solid #c6c6c6');
            $(this).next('.reason-field-container').find('.reason-field-label').hide();
            $(this).next('.reason-field-container').val('');
            $(this).next('.reason-field-container').removeClass('open-ta');
        }
    });

    // activate row
    $('.ty-rma-register-id input:first-child').on('change', function() {
        let siblings = $(this).parents('.ty-rma-register-id').siblings();

        if ($(this).is(':checked')) {
            siblings.removeClass('disabled');
        } else {
            siblings.addClass('disabled');
        }
    });

    // activate all
    $('input[name="check_all"]').on('change', function() {
        let disabledTableRows = $('.rma-row').find('.disabled');
        let tableRows = $('.rma-row').find('td:not(:first)');

        if ($(this).is(':checked')) {
            disabledTableRows.removeClass('disabled');
        } else {
            tableRows.addClass('disabled');
        }
    });

    $('form[name="return_registration_form"]').submit(function(e) {
        var form = $(this);
        var checkedFields = 0;
		var validFields = 0;

        form.find('tbody tr').each(function() {
            if($(this).find('td:first').find('#delete_checkbox').is(':checked')) {
                checkedFields++;

                var isChecked = $(this).find('.status-array :checkbox:checked').length > 0;

                if(!isChecked || ($(this).find('.reason-field-container').hasClass('open-ta') && !$.trim($(this).find('.reason-field').val()))) {
                    e.preventDefault();

                    if(!isChecked) {
                        $(this).find('.status-array').css('border', '2px solid #b94a48');
                        $(this).find('.status-error').show();

                        $('html, body').animate({
                            scrollTop: form.offset().top
                        }, 500);
                    }

                    if($(this).find('.reason-field-container').hasClass('open-ta') && !$.trim($(this).find('.reason-field').val())) {
                        $(this).find('.reason-field-container.open-ta .reason-field').css('border', '1px solid #b94a48');
                        $(this).find('.reason-field-container .reason-field-label').css({'display': 'block', 'color': '#b94a48'});

                        $('html, body').animate({
                            scrollTop: form.offset().top
                        }, 500);
                    }

                    if(isChecked && ($(this).find('.reason-field-container').hasClass('open-ta') && !$.trim($(this).find('.reason-field').val()))) {
						$(this).find('.status-array').css('border', 'none');
                        $(this).find('.status-error').hide();

                        $('html, body').animate({
                            scrollTop: form.offset().top
                        }, 500);
                    }

                    if (!isChecked && ($(this).find('.reason-field-container').hasClass('open-ta') && $.trim($(this).find('.reason-field').val()))) {
						$(this).find('.reason-field-container.open-ta .reason-field').css('border', '1px solid #c6c6c6');
                        $(this).find('.reason-field-label').hide();

                        $('html, body').animate({
                            scrollTop: form.offset().top
                        }, 500);
                    }
                } else {
                    validFields++;

                    if(isChecked) {
                        $(this).find('.status-array').css('border', 'none');
                        $(this).find('.status-error').hide();
                    }

                    if($.trim($(this).find('.reason-field-container.open-ta .reason-field').val())) {
                        $(this).find('.reason-field-container.open-ta .reason-field').css('border', '1px solid #c6c6c6');
                        $(this).find('.reason-field-label').hide();
                    }
                }
            }
        });

        if(checkedFields == validFields) {
            $('#ajax_overlay, #ajax_loading_box').show();
			form.off('submit').submit();
        }
    });
</script>
{/literal}