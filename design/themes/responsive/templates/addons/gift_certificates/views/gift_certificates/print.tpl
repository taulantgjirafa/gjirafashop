<div class="card-wrapper w50">
    <div id="card">
        <span class="c-background">
        <span class="colored {$gift_cert_data.design}"></span>
        </span>
        <div class="c-header">
            <img src="https://i.imgur.com/T3BaRBd.png" />
            <h1 class="c-amount">
                <span class="c-coupon-text">Kupon me vlerë</span>
                <span class="c-priceWrapper"><span>{$gift_cert_data.amount}€</span></span>
            </h1>
        </div>
        <div class="c-body ">
            <h1 class="c-name "><span class="c-dummy">Nga:</span> <b class="gift_cert_sender" data-placeholder="">{$gift_cert_data.sender}</b></h1>
            <h1 class="gift_cert_message" data-placeholder="Mesazhi juaj do të shfaqet këtu...">{$gift_cert_data.message}</h1>
        </div>
        <div class="c-footer">
            <h3>Kodi kuponit: <span class="c-code">{$gift_cert_data.gift_cert_code}</span></h4>
        </div>
    </div>
</div>