{include file="common/price.tpl" value=$addons.gift_certificates.max_amount assign="max_amount"}
{include file="common/price.tpl" value=$addons.gift_certificates.min_amount assign="min_amount"}
{assign var="text_gift_cert_amount_alert" value=__("text_gift_cert_amount_alert", ["[min]" => $min_amount, "[max]" => $max_amount])}

{* <script type="text/javascript">
(function(_, $) {

    var max_amount = '{$addons.gift_certificates.max_amount|escape:javascript nofilter}';
    var min_amount = '{$addons.gift_certificates.min_amount|escape:javascript nofilter}';
    var send_via = '{$gift_cert_data.send_via|default:"E"}';

    $(document).ready(function() {

        $.ceFormValidator('registerValidator', {
            class_name: 'cm-gc-validate-amount',
            message: '',
            func: function(id) {
                var max = parseInt((parseFloat(max_amount) / parseFloat(_.currencies.secondary.coefficient))*100)/100;
                var min = parseInt((parseFloat(min_amount) / parseFloat(_.currencies.secondary.coefficient))*100)/100;

                var amount = parseFloat($('#' + id).val());
                if ((amount <= max) && (amount >= min)) {
                    return true;
                }

                return false;
            }
        }); 
        
        $('#' + (send_via == 'E' ? 'post' : 'email') + '_block').switchAvailability(true, true);

        $(_.doc).on('click', 'input[name="gift_cert_data[send_via]"]', function() {
            $('#email_block').switchAvailability($(this).val() == 'P', true);
            $('#post_block').switchAvailability($(this).val() == 'E', true);
        });
    });
}(Tygh, Tygh.$));
</script> *}

{* {include file="views/profiles/components/profiles_scripts.tpl"} *}

<style type="text/css">
    label.cm-required:after {
        display: none;
    }

    #gift_cert_amount_error_message {
        display: none;
    }

    .help-inline {
        display: none !important;
    }
</style>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">

<main class="flexing gift-wrapper">
    {include file="addons/gift_certificates/views/gift_certificates/card.tpl"}
    <div class="form-wrapper w50">
        <div class="gc-form mt20">
            {* <div class="chose-gcm">
                <h3>1. Zgjedh metoden e kuponit digjitale apo fizike</h3>

                <div class="flexing">
                    <div data-name="digitalMethod" class="gift-method active">
                        <img src="https://cdn0.iconfinder.com/data/icons/shopping-and-commerce-4-4/48/161-512.png" />
                        <div>
                            <h4 class="mb0">Kupon digjitale</h4>
                            <p class="text-muted">Kuponi digjital dergohet ne mënyrë digjitale me e-mail</p>
                        </div>
                    </div>
                    <div data-name="cardMethod" class="gift-method">
                        <img src="https://cdn3.iconfinder.com/data/icons/flat-design-hands-icons/128/40-512.png" />
                        <div>
                            <h4 class="mb0">Kupon kartel</h4>
                            <p class="text-muted">Kuponi fizik në form te kartës </p>
                        </div>
                    </div>
                </div>
            </div> *}
            <h3>Plotesoni fushat</h3>
            <form method="post" target="_self" name="gift_certificates_form">
                <input type="hidden" name="gift_cert_data[amount]" id="amount_input"
                       value="{if $runtime.mode == 'update'}{$gift_cert_data.amount}{else}20{/if}">
                <input type="hidden" name="gift_cert_data[send_via]" value="E">
                <input type="hidden" name="gift_cert_data[template]" id="gift_cert_template" value="default.tpl">
                <input type="hidden" name="gift_cert_data[design]" id="gift_cert_design"
                       value="{if $runtime.mode == 'update'}{$gift_cert_data.design}{else}design1{/if}">
                {if $runtime.mode == "update"}
                    <input type="hidden" name="gift_cert_id" value="{$gift_cert_id}"/>
                    <input type="hidden" name="type" value="{$type}"/>
                {/if}

                <ul id="sending-methods" class="digitalMethod">
                    <li>
                        <div class="form-title">
                            <h4>Shuma</h4>
                        </div>
                        <div class="input-content">
                            <div class="gc-price-tags flexing flex-v-center">
                                <div class="flexing">
                                    <span data-amount="10">10€</span>
                                    <span data-amount="20"
                                          {if $runtime.mode == 'add'}class="active-gc-price"{/if}>20€</span>
                                    <span data-amount="50">50€</span>
                                    <span data-amount="100">100€</span>
                                    <span data-amount="200">200€</span>
                                    <span data-amount="500">500€</span>
                                </div>
                                <div class="gc-price-input">
                                    <label for="gift_cert_amount" class="{if $runtime.mode == 'update'}cm-required{/if}"
                                           id="amount_label"></label>
                                    <input {* name="gift_cert_data[amount]" *} id="gift_cert_amount" type="text"
                                                                               placeholder="Vlerë tjetër"
                                                                               value="{$gift_cert_data.amount}"/>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="form-title">
                            <h4>{__("for")|capitalize}</h4>
                        </div>
                        <div class="input-content">
                            <label for="gift_cert_recipient_name" class="cm-required" id=""></label>
                            <input name="gift_cert_data[recipient]" class="card-text" id="gift_cert_recipient_name"
                                   type="text" placeholder="Personi per te cilin eshte dhurata"
                                   value="{$gift_cert_data.recipient}"/>
                        </div>
                    </li>
                    <li>
                        <div class="form-title">
                            <h4>{__("from")|capitalize}</h4>
                        </div>
                        <div class="input-content">
                            <label for="gift_cert_sender" class="{if $runtime.mode == 'add'}cm-required{/if}"
                                   id="sender_label"></label>
                            <input name="gift_cert_data[sender]"
                                   class="card-text {if $runtime.mode == 'update' && $gift_cert_data.sender == ''}disabled{/if}"
                                   id="gift_cert_sender" type="text" placeholder="Personi që dërgon dhuratën"
                                   value="{$gift_cert_data.sender}"/>
                            <div class="anonim inputCheckBox">
                                <input name="{* gift_cert_data[] *}" id="anonymous" type="checkbox"
                                       {if $runtime.mode == 'update' && $gift_cert_data.sender == ''}checked{/if} />
                                <label for="anonymous">Anonim</label>
                            </div>
                        </div>
                    </li>
                    <li class="digitalEle">
                        <div class="form-title">
                            <h4>{__("email")}</h4>
                        </div>
                        <div class="input-content">
                            <label for="gift_cert_recipient" class="cm-email cm-required"></label>
                            <input name="gift_cert_data[email]" id="gift_cert_recipient" type="text"
                                   placeholder="Emaili i personit qe e pranon dhuraten"
                                   value="{$gift_cert_data.email}"/>
                        </div>
                    </li>
                    <li class="cardEle">
                        <div class="form-title">
                            <h4>Tek</h4>
                        </div>
                        <div class="input-content flexing">
                            <input name="{* gift_cert_data[] *}" type="text" id="c-adress"
                                   placeholder="Adresa e transportit"/>
                            <div class="merreVet inputCheckBox">
                                <input name="{* gift_cert_data[] *}" id="mrvet-id" type="checkbox"/>
                                <label for="mrvet-id">Marr tek zyret e Gjirafes</label>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="form-title">
                            <h4>{__("message")|capitalize}</h4>
                        </div>
                        <div class="input-content">
                            <label for="gift_cert_message" class="cm-required"></label>
                            <textarea maxlength="500" class="card-text" id="gift_cert_message"
                                      name="gift_cert_data[message]" style="min-height:100px;"
                                      placeholder="Mesazhi juaj do të shfaqet këtu...">{$gift_cert_data.message}</textarea>
                            <span class="count">Gjithsej shkronja: 500</span>
                        </div>
                    </li>
                    <li>
                        <div class="form-title">
                            <h4>{__("quantity")}</h4>
                        </div>
                        <div class="input-content">
                            <label for="gift_cert_stock" class="cm-integer cm-required"></label>
                            <input name="gift_cert_data[stock]" id="gift_cert_stock" type="text" placeholder="Sasia"
                                   value="{if $gift_cert_data.stock}{$gift_cert_data.stock}{else}1{/if}"/>
                        </div>
                    </li>
                    <li class="hidden-phone">
                        <div class="form-title">
                            <h4>Ngjyra e kartës</h4>
                        </div>
                        <div class="input-content card-colors">

                            {include file="addons/gift_certificates/views/gift_certificates/components/designs.tpl"}

                        </div>
                    </li>

                    <li>
                        <div class="form-title">
                            <h4>Data e dërgimit</h4>
                        </div>
                        <div class="input-content">
                            <label for="gift_cert_message" class="cm-required"></label>
                            <input id="gift_card_date_picker" type="date" name="gift_cert_data[send_date]"
                                   style="border: 1px solid #c6c6c6;"
                                   {if !$gift_cert_data.send_date}class="disabled"{/if}
                                   value="{$gift_cert_data.send_date}">
                            <div class="enable-date inputCheckBox">
                                <input id="enable_send_date" type="checkbox"
                                       {if $gift_cert_data.send_date}checked{/if} />
                                <label for="enable_send_date">Cakto datën e dërgimit</label>
                            </div>
                        </div>
                    </li>

                    <li class="gift-method-txt">
                        <p class="text-muted digitalEle"><b>Keni zgjedhur kupon digjital. Dhurata do ti shkoje personit
                                ne email me daten e caktuar pasi behet pagesa.</b></p>
                        <p class="text-muted cardEle"><b>Keni zgjedhur kupon fizik. Dhuraten mund ta merni ne zyret tona
                                apo transport ne adresen e caktuar.</b></p>
                    </li>

                    <li class="mt20">
                        {if $runtime.mode == 'add'}
                            <button class="button coupon" name="dispatch[gift_certificates.add]">Blej kuponin</button>
                        {else}
                            <button class="button coupon" name="dispatch[gift_certificates.update]">Blej kuponin
                            </button>
                        {/if}
                    </li>
                </ul>
            </form>
        </div>

    </div>
</main>

<script>
    var allColors = document.querySelectorAll('.card-colors span'),
        card = document.querySelector('#card'),
        allPriceTags = document.querySelectorAll('.gc-price-tags span'),
        priceWrapper = document.querySelector('.c-priceWrapper'),
        inputTexts = document.querySelectorAll('.card-text'),
        colorBackgroundWrapper = document.querySelector('.c-background'),
        anonymous = document.querySelector('#anonymous'),
        enableSendDate = document.querySelector('#enable_send_date'),
        merreID = document.querySelector('#mrvet-id'),
        anonimButton = document.querySelector('.anonim'),
        enableSendDateButton = document.querySelector('.enable-date'),
        giftMethod = document.querySelectorAll('.gift-method'),
        personInput = document.querySelector('#gift_cert_sender'),
        msgInput = document.querySelector('#gift_cert_message'),
        merreVet = document.querySelector('.merreVet'),
        priceInput = document.querySelector('#gift_cert_amount');

    priceInput.onkeyup = function () {
        inputPrcValue = parseFloat(this.value);
        setTimeout(function () {
            if (inputPrcValue < 5 || inputPrcValue > 1500 || isNaN(inputPrcValue)) {
                document.querySelector('.c-priceWrapper').classList.add('wrongAnimated');
                setTimeout(function () {
                    document.querySelector('.c-priceWrapper').classList.remove('wrongAnimated');
                }, 1000);
                // var currentPriceValue = document.querySelector('.gc-price-tags .active-gc-price').innerHTML;
                // document.querySelector('.c-priceWrapper span').innerHTML = '€';
                priceInput.value = null;
                // document.querySelector('#amount_label').classList.add('cm-required');
                document.querySelector('.gc-price-tags').setAttribute('style', 'pointer-events:all; opacity: 1');
            } else {
                var sel = document.querySelector('.gc-price-tags .active-gc-price') !== null;
                if (sel) {
                    document.querySelector('.gc-price-tags .active-gc-price').classList.remove('active-gc-price');
                }
                document.querySelector('#amount_input').setAttribute('value', inputPrcValue);
                document.querySelector('.c-priceWrapper span').innerHTML = inputPrcValue + '€';
            }
        }, 500);
        // if (inputPrcValue > 0) document.querySelector('.gc-price-tags > div').setAttribute('style', 'pointer-events:none; opacity: .5');
        // else document.querySelector('.gc-price-tags').setAttribute('style', 'pointer-events:all; opacity: 1');

        if (inputPrcValue == '') {
            document.querySelector('#amount_label').classList.add('cm-required');
        }
    };

    for (var i = 0; i < giftMethod.length; i++) {
        giftMethod[i].addEventListener('click', function () {
            currentMethod = this.getAttribute('data-name');
            document.querySelector('#sending-methods').className = currentMethod;
            card.className = currentMethod;
            for (var j = 0; j < giftMethod.length; j++) {
                giftMethod[j].classList.remove('active-gc-price');
            }
            this.classList.add('active-gc-price');

            document.querySelector('#gift_cert_amount').value = '';

            amountInputValue = this.dataset.amount;

            document.querySelector('#amount_input').setAttribute('value', amountInputValue);

            document.querySelector('#amount_label').classList.remove('cm-required');
        });
    }

    for (var i = 0; i < allColors.length; i++) {
        allColors[i].addEventListener('click', function () {
            var allBackgrounds = document.querySelectorAll('.c-background span'),
                newColor = document.createElement("SPAN");
            for (var j = 0; j < allColors.length; j++) {
                allColors[j].classList.remove('active');
                setTimeout(function () {
                    for (var j = 0; j < allBackgrounds.length; j++) {
                        allBackgrounds[j].remove();
                    }
                }, 300);
            }
            this.classList.add('active');

            designValue = this.dataset.design;

            document.querySelector('#gift_cert_design').setAttribute('value', designValue);

            colorBackgroundWrapper.appendChild(newColor);
            setTimeout(function () {
                newColor.classList.add('colored');
                newColor.classList.add(designValue);
            });
        });
    }

    for (var i = 0; i < allPriceTags.length; i++) {
        allPriceTags[i].addEventListener('click', function () {
            var thisPrice = this.innerText,
                currentPrice = document.querySelectorAll('.c-priceWrapper span');
            for (var k = 0; k < allPriceTags.length; k++) {
                allPriceTags[k].classList.remove('active-gc-price');
            }
            this.classList.add('active-gc-price');

            document.querySelector('#gift_cert_amount').value = '';

            amountInputValue = this.dataset.amount;

            document.querySelector('#amount_input').setAttribute('value', amountInputValue);

            document.querySelector('#amount_label').classList.remove('cm-required');
            for (var j = 0; j < currentPrice.length; j++) {
                currentPrice[j].classList.add('hidden-price');
                for (var j = 0; j < currentPrice.length; j++) {
                    currentPrice[j].remove();
                }
            }
            var newPrice = document.createElement("SPAN");
            newPrice.innerHTML = thisPrice;
            newPrice.setAttribute('class', 'newAdded-price');
            priceWrapper.appendChild(newPrice);
            newPrice.classList.remove('newAdded-price');
        });
    }

    for (var i = 0; i < inputTexts.length; i++) {
        inputTexts[i].onkeyup = function () {
            var classId = this.getAttribute('id'),
                personContent = this.value,
                charLength = personContent.length,
                isFocused = (document.activeElement === msgInput);

            document.querySelector('.' + classId).innerHTML = personContent;
            document.querySelector('.' + classId).classList.remove('opacity-active');
            document.querySelector('.c-dummy').classList.remove('opacity-active');

            if (isFocused) {
                document.querySelector('.count').innerHTML = "Gjithsej shkronja: " + (500 - this.value.length);
            }

            if (charLength == 0) {
                var personPlaceholder = document.querySelector('.' + classId).getAttribute('data-placeholder');
                document.querySelector('.' + classId).innerHTML = personPlaceholder;
                document.querySelector('.' + classId).classList.add('opacity-active');
            }
            if (personInput.value.length == 0) {
                document.querySelector('.c-dummy').classList.add('opacity-active');
            }
        };
    }
    //     msgInput.onkeyup = function () {
    //   document.querySelector('.count').innerHTML = "Gjithsej shkronja: " + (100 - this.value.length);
    // };

    anonimButton.onclick = function () {
        document.getElementById('gift_cert_sender').classList.add('disabled');
        document.querySelector('.c-name').classList.add('cname-hidden');
        // setTimeout(function(){
        // document.querySelector('.c-name').style.display = "none";
        // },400);
        if (anonymous.checked == false) {
            document.getElementById('gift_cert_sender').classList.remove('disabled');
            // document.querySelector('.c-name').style.display = "block";
            document.querySelector('.c-name').classList.remove('cname-hidden');
            document.querySelector('#sender_label').classList.add('cm-required');
        } else {
            document.querySelector('#sender_label').classList.remove('cm-required');
        }

    };

    enableSendDateButton.onclick = function () {
        document.getElementById('gift_card_date_picker').classList.remove('disabled');
        // document.querySelector('.c-name').classList.add('cname-hidden');
        // setTimeout(function(){
        // document.querySelector('.c-name').style.display = "none";
        // },400);
        if (enableSendDate.checked == false) {
            document.getElementById('gift_card_date_picker').classList.add('disabled');
            // document.querySelector('.c-name').style.display = "block";
            // document.querySelector('.c-name').classList.remove('cname-hidden');
            // document.querySelector('#sender_label').classList.add('cm-required');
        } else {
            // document.querySelector('#sender_label').classList.remove('cm-required');
        }

    };

    merreVet.onclick = function () {
        document.getElementById('c-adress').classList.add('disabled');
        if (merreID.checked == false) {
            document.getElementById('c-adress').classList.remove('disabled');
        }
    };
</script>

<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>

<script>
    // init datepicker
    flatpickr("#gift_card_date_picker", {
        minDate: "today"
    });

    // get design on edit mode
    {ldelim}
        let mode = `{$runtime.mode}`;

        if (mode == 'update') {
            let design = `{$gift_cert_data.design}`;
            let userAgent = `{$user_agent.type}`;

            if (userAgent == 'phone' || userAgent == 'tablet') {
                var designContainer = document.querySelector('.hidden-desktop.card-colors [data-design=\'' + design + '\']');
            } else if (userAgent == 'desktop') {
                var designContainer = document.querySelector('.input-content.card-colors [data-design=\'' + design + '\']');
            }

            designContainer.classList.add('active');
        }
        {rdelim}
</script>

{* {capture name="mainbox_title"}{if $runtime.mode == "add"}{__("purchase_gift_certificate")}{else}{__("gift_certificate")}{/if}{/capture} *}