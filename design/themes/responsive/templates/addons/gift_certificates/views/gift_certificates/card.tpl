<div class="card-wrapper w50" style="border: 1px solid #ddd; border-radius: 5px; box-shadow: 0px 12px 8px -5px rgb(0 0 0 / 5%)">
    <div class="card-colors hidden-desktop">

        {include file="addons/gift_certificates/views/gift_certificates/components/designs.tpl"}

    </div>
    <div id="card">
        <span class="c-background">
            <span class="colored {if $runtime.mode == 'add'}{$add_design}{else}{$gift_cert_data.design}{/if}" style="border-radius-top-left: 5px; border-radius-top-right: 5px;"></span>
        </span>
    </div>
{*    <div class="c-header">*}
{*        <img src="images/logos/gj50logoWhite.png" />*}
{*    </div>*}
    <div class="c-body pt20 pb20" style="padding-left: 20px; border-bottom: 1px solid #ddd;">
        <p class="c-name {if $runtime.mode == 'update' && $gift_cert_data.sender == ''}cname-hidden{/if}" style="font-size: 16px;">
            <span class="c-dummy {if $runtime.mode == 'add'}opacity-active{/if}">{__("from")|capitalize}:</span>
            <b class="gift_cert_sender {if $runtime.mode == 'add'}opacity-active{/if}" data-placeholder="">{$gift_cert_data.sender}</b>
        </p>
        <p class="gift_cert_message mb0 {if $runtime.mode == 'add'}opacity-active{/if}" data-placeholder="Mesazhi juaj do të shfaqet këtu..." style="font-size: 18px;">{if $runtime.mode == 'update'}{$gift_cert_data.message}{else}Mesazhi juaj do të shfaqet këtu...{/if}</p>
    </div>
    <div class="c-footer pt20 pb20" style="padding-left: 20px;">
{*        <h3>Kodi kuponit: <span class="c-code">******</span></h4>*}
        <p class="c-amount mb0" style="font-size: 24px; font-weight: bold">
{*            <span class="c-coupon-text">Kupon me vlerë</span>*}
            <span class="c-priceWrapper"><span>{if $runtime.mode == 'update'}{$gift_cert_data.amount}€{else}20€{/if}</span></span>
        </p>
    </div>
</div>