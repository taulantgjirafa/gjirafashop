{* <style type="text/css">
	html {
	  box-sizing: border-box;
	}

	*,
	*:after,
	*:before {
	  box-sizing: inherit;
	}

	body {
	  background-color: #222;
	  color: #e65228;
	  font-family: 'Lato', sans-serif;
	  overflow: hidden;
	}

	.present {
	  height: 200px;
	  left: 0;
	  margin: 0 auto;
	  -webkit-perspective: 600px;
	          perspective: 600px;
	  position: absolute;
	  right: 0;
	  top: 50%;
	  -webkit-transform: translateY(-50%);
	          transform: translateY(-50%);
	  width: 200px;
	}

	.name {
	  font-size: 1em;
	  font-weight: 100;
	  position: absolute;
	  top: -50%;
	  right: -15%;
	  font-family: Montserrat;
	  line-height: 2;
	  width: 400px;
	  opacity: 0;
	  transform: scale(0);
	  z-index: 9;
	}

	.instruction {
	  bottom: -100px;
	  left: 0;
	  opacity: 1;
	  position: absolute;
	  text-align: center;
	  transition: opacity .5s;
	  width: 100%;
	}

	.rotate-container {
	  -webkit-animation: none;
	          animation: none;
	  height: 100%;
	  -webkit-transform: rotateY(45deg);
	          transform: rotateY(45deg);
	  -webkit-transform-style: preserve-3d;
	          transform-style: preserve-3d;
	}

	@-webkit-keyframes present-rotate {
	  0% {
	    -webkit-transform: rotateY(0);
	            transform: rotateY(0);
	  }
	  100% {
	    -webkit-transform: rotateY(360deg);
	            transform: rotateY(360deg);
	  }
	}

	@keyframes present-rotate {
	  0% {
	    -webkit-transform: rotateY(0);
	            transform: rotateY(0);
	  }
	  100% {
	    -webkit-transform: rotateY(360deg);
	            transform: rotateY(360deg);
	  }
	}
	.bottom,
	.front,
	.left,
	.back,
	.right {
	  background-color: #e65228;
	  border: 1px solid #8a3118;
	  height: 100%;
	  left: 0;
	  position: absolute;
	  top: 0;
	  width: 100%;
	}

	.bottom {
	  -webkit-transform: translateY(50%) rotateX(90deg);
	          transform: translateY(50%) rotateX(90deg);
	}

	.front,
	.left,
	.back,
	.right {
	  transition: -webkit-transform .5s;
	  transition: transform .5s;
	  transition: transform .5s, -webkit-transform .5s;
	  -webkit-transform-origin: bottom;
	          transform-origin: bottom;
	  -webkit-transform-style: preserve-3d;
	          transform-style: preserve-3d;
	}

	.front:after,
	.left:after,
	.back:after,
	.right:after,
	.lid-top:after,
	.lid-front:after,
	.lid-left:after,
	.lid-back:after,
	.lid-right:after {
	  background-color: #fff;
	  box-shadow: 0 0 5px rgba(0, 0, 0, 0.2);
	  content: '';
	  height: 100%;
	  left: calc(50% - 10px);
	  position: absolute;
	  -webkit-transform: translateZ(0.1px);
	          transform: translateZ(0.1px);
	  width: 20px;
	}

	.lid-top:before {
	  background-color: #fff;
	  box-shadow: 0 0 5px rgba(0, 0, 0, 0.2);
	  content: '';
	  height: 20px;
	  left: 0;
	  position: absolute;
	  top: calc(50% - 10px);
	  -webkit-transform: translateZ(0.1px);
	          transform: translateZ(0.1px);
	  width: 100%;
	}

	.left {
	  -webkit-transform: translateX(-50%) rotateY(-90deg);
	          transform: translateX(-50%) rotateY(-90deg);
	}

	.back {
	  -webkit-transform: translateZ(-100px) rotateY(180deg) rotateX(0);
	          transform: translateZ(-100px) rotateY(180deg) rotateX(0);
	}

	.right {
	  -webkit-transform: translateX(50%) rotateY(90deg);
	          transform: translateX(50%) rotateY(90deg);
	}

	.front {
	  -webkit-transform: translateZ(100px);
	          transform: translateZ(100px);
	}

	.lid {
	  -webkit-animation: lid-animation 3.5s 1s infinite;
	          animation: lid-animation 3.5s 1s infinite;
	  -webkit-transform: translate3d(0, 0, 0);
	          transform: translate3d(0, 0, 0);
	  -webkit-transform-style: preserve-3d;
	          transform-style: preserve-3d;
	  transition: -webkit-transform .5s;
	  transition: transform .5s;
	  transition: transform .5s, -webkit-transform .5s;
	}

	@-webkit-keyframes lid-animation {
	  0% {
	    -webkit-transform: translate3d(0, 0, 0) rotateX(0);
	            transform: translate3d(0, 0, 0) rotateX(0);
	  }
	  5% {
	    -webkit-transform: translate3d(0, -10px, -5px) rotateX(5deg);
	            transform: translate3d(0, -10px, -5px) rotateX(5deg);
	  }
	  10% {
	    -webkit-transform: translate3d(0, -10px, 5px) rotateX(-5deg);
	            transform: translate3d(0, -10px, 5px) rotateX(-5deg);
	  }
	  15% {
	    -webkit-transform: translate3d(0, -10px, -5px) rotateX(5deg);
	            transform: translate3d(0, -10px, -5px) rotateX(5deg);
	  }
	  20% {
	    -webkit-transform: translate3d(0, -10px, 5px) rotateX(-5deg);
	            transform: translate3d(0, -10px, 5px) rotateX(-5deg);
	  }
	  25% {
	    -webkit-transform: translate3d(0, -10px, -5px) rotateX(5deg);
	            transform: translate3d(0, -10px, -5px) rotateX(5deg);
	  }
	  30% {
	    -webkit-transform: translate3d(0, 0, 0) rotateX(0);
	            transform: translate3d(0, 0, 0) rotateX(0);
	  }
	}

	@keyframes lid-animation {
	  0% {
	    -webkit-transform: translate3d(0, 0, 0) rotateX(0);
	            transform: translate3d(0, 0, 0) rotateX(0);
	  }
	  5% {
	    -webkit-transform: translate3d(0, -10px, -5px) rotateX(5deg);
	            transform: translate3d(0, -10px, -5px) rotateX(5deg);
	  }
	  10% {
	    -webkit-transform: translate3d(0, -10px, 5px) rotateX(-5deg);
	            transform: translate3d(0, -10px, 5px) rotateX(-5deg);
	  }
	  15% {
	    -webkit-transform: translate3d(0, -10px, -5px) rotateX(5deg);
	            transform: translate3d(0, -10px, -5px) rotateX(5deg);
	  }
	  20% {
	    -webkit-transform: translate3d(0, -10px, 5px) rotateX(-5deg);
	            transform: translate3d(0, -10px, 5px) rotateX(-5deg);
	  }
	  25% {
	    -webkit-transform: translate3d(0, -10px, -5px) rotateX(5deg);
	            transform: translate3d(0, -10px, -5px) rotateX(5deg);
	  }
	  30% {
	    -webkit-transform: translate3d(0, 0, 0) rotateX(0);
	            transform: translate3d(0, 0, 0) rotateX(0);
	  }
	}
	.lid-top,
	.lid-left,
	.lid-back,
	.lid-right,
	.lid-front {
	  background-color: #e65228;
	  border: 1px solid #8a3118;
	  left: -5px;
	  opacity: 1;
	  position: absolute;
	  top: 0;
	  width: 210px;
	}

	.lid-top {
	  height: 210px;
	  top: -5px;
	  -webkit-transform: translateY(-50%) rotateX(90deg);
	          transform: translateY(-50%) rotateX(90deg);
	  -webkit-transform-style: preserve-3d;
	          transform-style: preserve-3d;
	}

	.lid-left,
	.lid-back,
	.lid-right,
	.lid-front {
	  height: 40px;
	  top: -5px;
	  -webkit-transform-style: preserve-3d;
	          transform-style: preserve-3d;
	}

	.lid-left {
	  -webkit-transform: translateX(-50%) rotateY(-90deg);
	          transform: translateX(-50%) rotateY(-90deg);
	}

	.lid-back {
	  -webkit-transform: translateZ(-105px) rotateY(180deg);
	          transform: translateZ(-105px) rotateY(180deg);
	}

	.lid-right {
	  -webkit-transform: translateX(50%) rotateY(90deg);
	          transform: translateX(50%) rotateY(90deg);
	}

	.lid-front {
	  -webkit-transform: translateZ(105px);
	          transform: translateZ(105px);
	}

	.present:hover .lid {
	  -webkit-animation: none;
	          animation: none;
	  -webkit-transform: translate3d(0, -40px, -10px) rotateX(10deg);
	          transform: translate3d(0, -40px, -10px) rotateX(10deg);
	}

	.present.open .name {
	  -webkit-transform: translate3d(0, -50%, 0) rotateY(1080deg) rotateX(0deg);
	          transform: translate3d(0, -50%, 0) rotateY(1080deg) rotateX(0deg);
	  opacity: 1;
	  transform: scale(0.9);
	  transition: .5s;
	}
	.present.open .instruction {
	  opacity: 0;
	}
	.present.open .left {
	  -webkit-transform: translateX(-50%) rotateY(-90deg) rotateX(-90deg);
	          transform: translateX(-50%) rotateY(-90deg) rotateX(-90deg);
	}
	.present.open .back {
	  -webkit-transform: translateZ(-100px) rotateY(180deg) rotateX(-90deg);
	          transform: translateZ(-100px) rotateY(180deg) rotateX(-90deg);
	}
	.present.open .right {
	  -webkit-transform: translateX(50%) rotateY(90deg) rotateX(-90deg);
	          transform: translateX(50%) rotateY(90deg) rotateX(-90deg);
	}
	.present.open .front {
	  -webkit-transform: translateZ(100px) rotateX(-90deg);
	          transform: translateZ(100px) rotateX(-90deg);
	}
	.present.open .lid {
	  -webkit-animation: none;
	          animation: none;
	  -webkit-transform: translate3d(0, -120px, -120px) rotateX(50deg);
	          transform: translate3d(0, -120px, -120px) rotateX(50deg);
	}

	canvas {
	  height: 100%;
	  left: 0;
	  position: fixed;
	  top: 0;
	  width: 100%;
	}

</style>

<div id="claim_gc_container" style="height: 650px;">
	<div class="present">
		<div class="name">
			{include file="addons/gift_certificates/views/gift_certificates/print.tpl"}
		</div>
		
		<div class="rotate-container">
			<div class="bottom"></div>
			<div class="front"></div>
			<div class="left"></div>
			<div class="back"></div>
			<div class="right"></div>
			
			<div class="lid">
				<div class="lid-top"></div>
				<div class="lid-front"></div>
				<div class="lid-left"></div>
				<div class="lid-back"></div>
				<div class="lid-right"></div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	const present = document.querySelector('.present');
	present.onclick = () => present.classList.toggle('open');




	(function () {
	  'use strict';

	  const canvas = document.querySelector('canvas');
	  const ctx = canvas.getContext('2d');

	  let width, height, lastNow;
	  let snowflakes;
	  let maxSnowflakes = 100;

	  function init() {
	    snowflakes = [];
	    resize();
	    render(lastNow = performance.now());
	  }

	  function render(now) {
	    requestAnimationFrame(render);

	    const elapsed = now - lastNow;
	    lastNow = now;

	    ctx.clearRect(0, 0, width, height);
	    if (snowflakes.length < maxSnowflakes)
	    snowflakes.push(new Snowflake());

	    ctx.fillStyle = ctx.strokeStyle = 'rgba(255, 255, 255, .5)';

	    snowflakes.forEach(snowflake => snowflake.update(elapsed, now));
	  }

	  function pause() {
	    cancelAnimationFrame(render);
	  }
	  function resume() {
	    lastNow = performance.now();
	    requestAnimationFrame(render);
	  }


	  class Snowflake {
	    constructor() {
	      this.spawn();
	    }

	    spawn(anyY = false) {
	      this.x = rand(0, width);
	      this.y = anyY === true ?
	      rand(-50, height + 50) :
	      rand(-50, -10);
	      this.xVel = rand(-.05, .05);
	      this.yVel = rand(.02, .1);
	      this.angle = rand(0, Math.PI * 2);
	      this.angleVel = rand(-.001, .001);
	      this.size = rand(7, 12);
	      this.sizeOsc = rand(.01, .5);
	    }

	    update(elapsed, now) {
	      const xForce = rand(-.001, .001);

	      if (Math.abs(this.xVel + xForce) < .075) {
	        this.xVel += xForce;
	      }

	      this.x += this.xVel * elapsed;
	      this.y += this.yVel * elapsed;
	      this.angle += this.xVel * 0.05 * elapsed; //this.angleVel * elapsed

	      if (
	      this.y - this.size > height ||
	      this.x + this.size < 0 ||
	      this.x - this.size > width)
	      {
	        this.spawn();
	      }

	      this.render();
	    }

	    render() {
	      ctx.save();
	      const { x, y, angle, size } = this;
	      ctx.beginPath();
	      ctx.arc(x, y, size * 0.2, 0, Math.PI * 2, false);
	      ctx.fill();
	      ctx.restore();
	    }}


	  // Utils
	  const rand = (min, max) => min + Math.random() * (max - min);

	  function resize() {
	    width = canvas.width = window.innerWidth;
	    height = canvas.height = window.innerHeight;
	    maxSnowflakes = Math.max(width / 10, 100);
	  }

	  window.addEventListener('resize', resize);
	  window.addEventListener('blur', pause);
	  window.addEventListener('focus', resume);
	  init();

	})();
</script> *}



<div style="display: flex; justify-content: center; padding-top: 35px;">
{include file="addons/gift_certificates/views/gift_certificates/print.tpl"}
</div>