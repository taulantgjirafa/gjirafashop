.chose-gcm {
  margin-bottom: 20px;
  border-bottom: 1px solid #eee;
  padding-bottom: 25px;
  margin-top: 20px;
}
.digitalMethod .digitalEle {
  display: flex;
}
.digitalMethod .cardEle {
  display: none;
}
.cardMethod .cardEle {
  display: flex;
}
.cardMethod .digitalEle {
  display: none;
}
.coupon {
  background: #e25526;
  padding: 17px;
  border: 0;
  color: #fff;
  width: 100%;
  font-weight: 600;
  border-radius: 4px;
}
.gift-method {
  display: inline-block;
  box-shadow: 0px 2px 6px -2px rgba(0, 0, 0, 0.4392156862745098);
  margin: 0 10px;
  width: 50%;
  border-radius: 4px;
  transition: box-shadow 0.3s ease, border-color 0.3s ease;
  border: 2px solid transparent;
  cursor: pointer;
}
.gift-method p {
  padding-bottom: 0;
}
.gift-method.active {
  border-color: #e25526;
  box-shadow: 0px 2px 6px -2px #e25526;
}
.gift-method img {
  width: 100px;
  display: block;
  margin: 10px auto 10px auto;
}
.gift-method > div {
  border-top: 1px solid #eee;
  padding: 10px;
}
.card-wrapper {
  margin: auto 45px auto 0;
}
.card-wrapper #card {
  transition: border-radius 0.4s ease;
  width: 580px;
  height: 330px;
  border-radius: 25px;
  box-shadow: 0 26px 20px -11px rgba(0, 0, 0, 0.24);
  padding: 30px;
  box-sizing: border-box;
  position: relative;
  overflow: hidden;
  color: #fff;
}
.card-wrapper #card.cardMethod {
  border-radius: 4px;
}
.card-wrapper #card:before {
  position: absolute;
  content: "";
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  background-image: url("https://i.imgur.com/MZa2frP.png");
  background-repeat: no-repeat;
  background-position: right top;
}
.card-wrapper #card .c-background span {
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  background: linear-gradient(to top, #ca471b, #ca471b);
  z-index: -1;
  transition: opacity .3s ease;
  opacity: 0;
}
.card-wrapper #card .c-background span.colored {
  opacity: 1;
}
.card-wrapper #card h1,
.card-wrapper #card h2,
.card-wrapper #card h3,
.card-wrapper #card h4,
.card-wrapper #card h5 {
  color: #fff;
  font-weight: 400;
  margin: 0;
}
.card-wrapper #card .c-header img {
  width: 130px;
}
.card-wrapper #card .c-header .c-amount {
  text-align: right;
  float: right;
  font-size: 37px;
  margin: 0;
  line-height: 1;
}
.card-wrapper #card .c-header .c-amount .c-coupon-text {
  display: block;
  font-size: 14px;
  margin-top: -8px;
  margin-bottom: 2px;
  text-transform: uppercase;
}
.card-wrapper #card .c-header .c-amount .c-priceWrapper {
  position: relative;
}
.card-wrapper #card .c-header .c-amount .c-priceWrapper.wrongAnimated span {
  animation: wrong .4s ease;
}
.card-wrapper #card .c-header .c-amount .c-priceWrapper > span {
  display: block;
  transition: transform .4s ease, opacity .4s ease, left .3s ease;
  opacity: 1;
  transform: translate3d(0, 0, 0);
  position: absolute;
  right: 0;
}
.card-wrapper #card .c-header .c-amount .c-priceWrapper > span.hidden-price {
  transform: translate3d(100px, 0, 0);
  opacity: 0;
}
.card-wrapper #card .c-header .c-amount .c-priceWrapper > span.newAdded-price {
  transform: translate3d(-100px, 0, 0);
  opacity: 0;
}
.card-wrapper #card .c-body {
  margin: 40px 0;
}
.card-wrapper #card .c-body .c-name {
  position: absolute;
  transition: opacity .3s ease, transform .4s ease;
}
.card-wrapper #card .c-body .c-name > b {
  display: inline-block;
}
.card-wrapper #card .c-body .c-name.cname-hidden {
  opacity: 0;
  transform: translate3d(-30px, 0, 0);
}
.card-wrapper #card .c-body .c-name b {
  font-size: 27px;
  text-transform: capitalize;
  transition: opacity .3s ease, transform .3s ease;
  transform-origin: left;
  transform: scale(1);
}
.card-wrapper #card .c-body .c-msg,
.card-wrapper #card .c-body .gift_cert_message {
  padding-top: 35px;
  word-break: break-word;
  transition: opacity .3s ease, transform .3s ease;
  transform-origin: left;
  transform: scale(1);
}
.card-wrapper #card .c-body .opacity-active {
  opacity: 0.7;
  font-weight: 400;
  transform: scale(0.9) !important;
}
.card-wrapper #card .c-footer {
  float: right;
  position: absolute;
  bottom: 40px;
  right: 30px;
}
.card-wrapper #card .c-footer .c-code {
  font-size: 18px;
}
.card-wrapper #card:after {
  content: "";
  position: absolute;
  bottom: 0px;
  left: 0;
  width: 100%;
  height: 13px;
  background: rgba(0, 0, 0, 0.11);
}
.card-colors {
  font-size: 0;
}
.card-colors span {
  width: 30px;
  height: 30px;
  margin-right: 7px;
  margin-top: 3px;
  margin-bottom: 3px;
  border-radius: 100%;
  display: inline-block;
  transition: transform 0.3s cubic-bezier(0.18, 0.89, 0.32, 1.28), box-shadow 0.3s ease, margin 0.4s ease;
  cursor: pointer;
}
.card-colors span:hover {
  transform: scale(1.2);
}
.card-colors span.active {
  transform: scale(1.3);
  box-shadow: 0px 12px 8px -5px rgba(0, 0, 0, 0.46);
}
.gc-form ul li {
  display: flex;
  margin-bottom: 20px;
  padding: 0;
  align-items: center;
}
.gc-form ul li.gift-method-txt p {
  padding: 0;
  height: 35px;
}
.gc-form ul li .form-title {
  text-align: right;
  width: 105px;
  margin-right: 15px;
}
.gc-form ul li .input-content {
  width: 100%;
  position: relative;
}
.gc-form ul li .input-content .inputCheckBox {
  position: absolute;
  right: 1px;
  top: 1px;
  display: flex;
  align-items: center;
  background: #e8e8e8;
  height: 38px;
  border-bottom-right-radius: 4px;
  border-top-right-radius: 4px;
  color: #777;
  font-weight: 600;
  user-select: none;
}
.gc-form ul li .input-content .inputCheckBox label {
  height: 100%;
  display: flex;
  padding: 0 10px;
  align-items: center;
  cursor: pointer;
}
.gc-form ul li .input-content .inputCheckBox input {
  width: 15px;
  height: 15px;
  min-height: 15px;
  margin: 0;
  margin-left: 15px;
}
.gc-form ul li .input-content .count {
  position: absolute;
  background: #fff;
  font-weight: 600;
  color: #888;
  padding: 10px;
  bottom: 1px;
  right: 1px;
  border-bottom-right-radius: 4px;
}
.gc-form ul li h4 {
  margin: 0;
}
.gc-form ul li input,
.gc-form ul li textarea {
  /*border: 1px solid #d0d0d0;*/

  outline: none;
  border-radius: 4px;
  min-height: 40px;
  width: 100%;
  max-width: 100%;
  resize: none;
  transition: background .35s ease;
  padding: 4px 8px;
  height: 32px;
  box-sizing: border-box;
  font-size: 14px;
}
.gc-form ul li input.disabled,
.gc-form ul li textarea.disabled {
  background: #e8e8e8;
  pointer-events: none;
}
.gc-form ul li .gc-price-tags span {
  padding: 10px 9px;
  border-radius: 4px;
  background: #f7f7f7;
  margin-right: 5px;
  font-size: 16px;
  transition: background .4s ease, color .4s ease;
  cursor: pointer;
  margin-top: 3px;
  margin-bottom: 3px;
  user-select: none;
  display: inline-block;
}
.gc-form ul li .gc-price-tags span:hover {
  background: #ddd;
}
.gc-form ul li .gc-price-tags span.active-gc-price {
  background: #e25526;
  color: #fff;
}
.gc-form ul li .gc-price-input {
  width: 100%;
}
@keyframes wrong {
  0% {
    transform: translate3d(-15px, 0, 0);
  }
  25% {
    transform: translate3d(0, 0, 0);
  }
  55% {
    transform: translate3d(-15px, 0, 0);
  }
}
@media screen and (max-width: 1280px) {
  .card-wrapper {
    width: 60%;
    margin-left: -20px;
    margin-right: 15px;
  }
}
@media screen and (max-width: 768px) {
  .gc-price-tags {
    display: flex;
    justify-content: space-between;
  }
  .card-colors {
    white-space: nowrap;
    overflow-x: auto;
    margin: 0 -10px;
    padding: 0 10px;
  }
  .card-colors span {
    width: 65px;
    height: 40px;
    margin: 10px 10px 10px 0;
    border-radius: 12px;
    box-sizing: border-box;
    transform: none !important;
    transition: none !important;
  }
  .gift-wrapper {
    flex-direction: column;
    align-items: center;
  }
  .gift-method > div {
    border: 0;
  }
  .gift-method img {
    width: 60px;
    display: none;
  }
  .gc-form ul li {
    flex-direction: column;
  }
  .gc-form ul li .form-title {
    text-align: left;
    width: 100%;
    margin: 5px 0 10px 0;
  }
  .card-wrapper {
    width: 100%;
    margin: 0;
    padding: 10px;
    box-sizing: border-box;
  }
  .card-wrapper #card {
    box-shadow: 0 16px 7px -11px rgba(0, 0, 0, 0.24);
    height: 230px;
    padding: 20px;
  }
  .card-wrapper #card:before {
    left: 70px;
    top: -13px;
  }
  .card-wrapper #card .c-header img {
    width: 100px;
  }
  .card-wrapper #card .c-header .c-amount {
    font-size: 27px;
  }
  .card-wrapper #card .c-header .c-amount .c-coupon-text {
    font-size: 12px;
    margin-top: -3px;
  }
  .card-wrapper #card .c-body {
    margin: 20px 0;
  }
  .card-wrapper #card .c-body .c-name b {
    font-size: 23px;
  }
  .card-wrapper #card .c-body .c-msg,
  .card-wrapper #card .c-body .gift_cert_message {
    padding-top: 30px;
  }
  .card-wrapper #card .c-footer {
    bottom: 25px;
  }
  .form-wrapper {
    width: 100%;
    box-sizing: border-box;
    padding: 0 10px 10px;
  }
  .chose-gcm {
    margin-top: 15px;
  }
  .wishlist-quick-view {
    width: 100% !important;
    margin-top: 0 !important;
  }
  .wishlist-single-container {
    height: auto;
    position: relative;
  }
  .wishlist-price-container {
    padding-bottom: 40px;
  }
  .wishlist-description-container {
    display: none;
  }
}
.card-wrapper {
  margin: 0 !important;
}

.design1 {
  background:linear-gradient(to top, #ca471b, #ca471b);
}
.design2 {
  background:linear-gradient(to bottom, #f7bb97, #dd5e89);
}
.design3 {
  background:linear-gradient(to bottom, #2c3e50, #fd746c);
}
.design4 {
  background:linear-gradient(to bottom, #642B73, #C6426E);
}
.design5 {
  background:linear-gradient(to bottom, #eb3349, #f45c43);
}
.design6 {
  background:linear-gradient(to bottom, #2c3e50, #3498db);
}
.design7 {
  background:linear-gradient(to bottom, #dce35b, #45b649);
}
.design8 {
  background:linear-gradient(to bottom, #ff5f6d, #ffc371);
}
.design9 {
  background:linear-gradient(to bottom, #464646, #0e0e0e);
}
.design10 {
  background:linear-gradient(to bottom, #0e0e0e, #e15726);
}
.design11 {
  background:linear-gradient(to bottom, #ffd89b, #19547b);
}
.design12 {
  background:linear-gradient(to bottom, #808080, #3fada8);
}
