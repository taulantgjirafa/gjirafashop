{if strpos($smarty.server.QUERY_STRING, "products.view") == false}
<div class="scroll-top-button rounded-m__box bbox">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 86.002 86.002"><path d="M80.093 64.999a3.488 3.488 0 0 0 4.896 0 3.405 3.405 0 0 0 0-4.847L45.445 21.004a3.483 3.483 0 0 0-4.892 0L1.013 60.152a3.405 3.405 0 0 0 0 4.847 3.486 3.486 0 0 0 4.895 0l37.091-35.703 37.094 35.703z" /></svg>
</div>
{/if}
{literal}<script>
    var scrollTopButton=$(".scroll-top-button")[0];
    $(window).scroll(function(){
        $(window).scrollTop()>500 ? $(".scroll-top-button").css("transform","translateX(0)") : $(".scroll-top-button").css("transform","translateX(200px)")
    }), $(".scroll-top-button").click(function(){return $("html, body").animate({scrollTop:0},800),!1});

</script>{/literal}