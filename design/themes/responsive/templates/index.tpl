{*  To modify and rearrange content blocks in your storefront pages
    or change the page structure, use the layout editor under Design->Layouts
    in your admin panel.

    There, you can:

    * modify the page layout
    * make it fluid or static
    * set the number of columns
    * add, remove, and move blocks
    * change block templates and types and more.

    You only need to edit a .tpl file to create a new template
    or modify an existing one; often, this is not the case.

    Basic layouting concepts:

    This theme uses the Twitter Bootstrap 2.3 CSS framework.

    A layout consists of four containers (CSS class .container):
    TOP PANEL, HEADER, CONTENT, and FOOTER.

    Containers are partitioned with fixed-width grids (CSS classes .span1, .span2, etc.).

    Content blocks live inside grids. You can drag'n'drop blocks
    from grid to grid in the layout editor.

    A block represents a certain content type (e.g. products)
    and uses a certain template to display it (e.g. list with thumbnails).
*}
<!DOCTYPE html>
<html class="notranslate" translate="no" {hook name="index:html_tag"}{/hook} lang="sq" dir="{$language_direction}">
{*lang="{$smarty.const.CART_LANGUAGE}"*}
<head>
    <meta name="google" content="notranslate"/>
    <link rel="manifest" href="manifest.json">
    {capture name="page_title"}
        {hook name="index:title"}
        {if $page_title}
            {$page_title}
        {else}
            {if $location_data.name == "Categories" || $location_data.name == "Products"}
                Blej online
            {/if}
            {foreach from=$breadcrumbs item=i name="bkt"}
                {if $smarty.foreach.bkt.last}
                    {$i.title|strip_tags}
                {/if}
                {*{if !$smarty.foreach.bkt.first}{$i.title|strip_tags}{if !$smarty.foreach.bkt.last} :: {/if}{/if}*}
            {/foreach}
            {if $location_data.name != "Categories" || $location_data.name == "Products"}
                {if !$skip_page_title && $location_data.title}{if $breadcrumbs|count > 1} - {/if}{$location_data.title}{/if}
            {/if}
            {if $location_data.name == "Categories" || $location_data.name == "Products"}
                - Gjirafa50
            {/if}
        {/if}
        {/hook}
    {/capture}
    <title>{$smarty.capture.page_title|strip|trim nofilter}</title>
    <meta property="og:image" content="design\themes\responsive\media\images"/>
    {include file="meta.tpl"}
    <link href="images/logos/2/favico.png" rel="shortcut icon" type="image/png"/>
    {include file="common/styles.tpl" include_dropdown=true}
    {if "DEVELOPMENT"|defined && $smarty.const.DEVELOPMENT == true}
        <!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/es5-shim/4.1.9/es5-shim.min.js"></script><![endif]-->
    {/if}
    {hook name="index:head_scripts"}{/hook}

</head>

<body class="store_{$smarty.const.STORE} {$smarty.const.DEVICE_TYPE}_device">
<input id="sso_base_url" value="https://sso.gjirafa.com" hidden>
<input id="store_user" value="{$smarty.session.auth.user_id}" hidden>
{assign var="user_email_domain" value="@"|explode:$smarty.session.cart.user_data.user_login}
<input type="hidden" id="d_mode"
       value="{if $user_email_domain != null && $user_email_domain[1] != null && $user_email_domain[1] == 'gjirafa.com' && $smarty.const.DEVELOPMENT}0{else}1{/if}">

{assign var="is_al" value=$al|fn_isAL}
{if $is_al}
    <span class="store" style="display: none">AL</span>
{else}
    <span class="store" style="display: none">KS</span>
{/if}

{hook name="index:body"}
{assign var="user_agent" value=$smarty.server.HTTP_USER_AGENT}

{if preg_match('!(tablet|pad|mobile|phone|symbian|android|ipod|ios|blackberry|webos)!i', $user_agent)}
    {include file="blocks/menu/responsive_menu.tpl"}
{/if}


{if isset($runtime.customization_mode) && $runtime.customization_mode.design}
    {include file="common/toolbar.tpl" title=__("on_site_template_editing") href="customization.disable_mode?type=design"}
{/if}
{if isset($runtime.customization_mode) && $runtime.customization_mode.live_editor}
    {include file="common/toolbar.tpl" title=__("on_site_live_editing") href="customization.disable_mode?type=live_editor"}
{/if}
{if "THEMES_PANEL"|defined && !$runtime.customization_mode.live_editor}
    {include file="demo_theme_selector.tpl"}
{/if}
    <div class="ty-tygh {if $runtime.customization_mode.theme_editor}te-mode{/if} {if $runtime.customization_mode.live_editor || $runtime.customization_mode.design || $smarty.const.THEMES_PANEL}ty-top-panel-padding{/if}"
         id="tygh_container">

        {include file="common/notification.tpl"}

        <div class="ty-helper-container" id="tygh_main_container">
            {hook name="index:content"}
                {* NOTE:
                   render_location - call a Smarty function that builds a page structure according to the layout (see Design->Layouts).
                   The function renders a template and generates blocks depending on 'dispatch'.
               *}
            {render_location}

            {/hook}
            <!--tygh_main_container--></div>

        <!--tygh_container--></div>
    {*        <button id="chatInit"><span class="img"><img alt="Bisedoni me ne!" src="images/icons/chatIcon.jpg"></span>Kontakto këtu</button>*}
{hook name="index:footer"}{/hook}
    <script data-no-defer src="https://gjstatic.blob.core.windows.net/fix/dmp.js"></script>
    {include file="common/scripts.tpl"}
    {include file="common/loading_box.tpl"}

{if isset($runtime.customization_mode) && $runtime.customization_mode.design}
    {include file="backend:common/template_editor.tpl"}
{/if}
{if isset($runtime.customization_mode) && $runtime.customization_mode.theme_editor}
    {include file="backend:common/theme_editor.tpl"}
{/if}
{/hook}
{include file="buttons/back_to_top.tpl"}
{include file="common/login_popup.tpl"}
</body>
</html>
