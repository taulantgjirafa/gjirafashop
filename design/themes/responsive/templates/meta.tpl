{hook name="index:meta"}
{assign "is_al" $a|fn_isAL}
{if $display_base_href}
    <base href="{$config.current_location}/"/>
{/if}
    <meta name="theme-color" content="#222222">
    <meta name="msapplication-navbutton-color" content="#222222">
    <meta name="apple-mobile-web-app-status-bar-style" content="#222222">
    <meta http-equiv="Content-Type" content="text/html; charset={$smarty.const.CHARSET}" data-ca-mode="{$store_trigger}"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
    {foreach $breadcrumbs as $breadcrump}
        {$bread_crumbs[] = $breadcrump.title}
    {/foreach}
    {if $bread_crumbs == null}
        <meta name="title" content="Home">
        {$bread_crumbs = 'Home'}
        {else}
        <meta name="title" content="{' | '|implode:$bread_crumbs|strip}">
    {/if}
{if $product}
    {*{assign var="meta_container" value=intval($product.product_id/8000+1)}*}
    {*{if $meta_container gt 5}*}
        {*{$meta_container=5}*}
    {*{/if}*}
    {assign var="meta_azure_path" value={get_images_from_blob product_id=$product.product_id count=1}}
    <meta itemprop="name" content="{$product.product}">
    <meta itemprop="description" content="{$product.full_description|strip_tags}">
    <meta itemprop="image" content="{$meta_azure_path}">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="{$product.product}">
    <meta name="twitter:description" content="{$product.full_description}">
    <meta name="twitter:image:src" content="{$meta_azure_path}">
    <meta property="og:title" content="{$product.product}"/>
    <meta property="og:url" content="{$config.current_location}{$smarty.server.REQUEST_URI|substr:0:-1}"/>
    <meta property="og:image" content="{$meta_azure_path}"/>
    <meta property="og:image:width" content="200" />
    <meta property="og:image:height" content="200" />
    <meta property="og:description" content="{$product.full_description|strip_tags}"/>
    {elseif !is_null($category_data)}
    <meta itemprop="name" content="Blej online {$category_data.category}">
    <meta itemprop="description" content="Blej online në Gjirafa50 {$category_data.category} origjinal të teknologjisë së fundit me çmim të volitshëm dhe garancion nga prodhuesi. Pagesa të sigurta online si dhe me para në dorë. Transport i sigurtë dhe falas. Porosit tani, prano produktin në shtëpi.">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Blej online {$category_data.category}">
    <meta name="twitter:description" content="Blej online në Gjirafa50 {$category_data.category} origjinal të teknologjisë së fundit me çmim të volitshëm dhe garancion nga prodhuesi. Pagesa të sigurta online si dhe me para në dorë. Transport i sigurtë dhe falas. Porosit tani, prano produktin në shtëpi.">
    <meta property="og:title" content="Blej online {$category_data.category}"/>
    <meta property="og:url" content="{$config.current_location}{$smarty.server.REQUEST_URI|substr:0:-1}"/>
    <meta property="og:image:width" content="200" />
    <meta property="og:image:height" content="200" />
    <meta property="og:description" content="{$product.full_description|strip_tags}"/>
{/if}
    {if $is_al && !$product}
    <meta property="og:image" content="https://gjirafa50.com/images/logos/1/og-image-al.png"/>
{else}
    <meta property="og:image" content="https://gjirafa50.com/images/logos/1/og-image.jpg"/>
{/if}

    {hook name="index:meta_description"}
        <meta name="description" content="{$meta_description|html_entity_decode:$smarty.const.ENT_COMPAT:"UTF-8"|default:$location_data.meta_description}"/>
    {/hook}
    {if $product && $product.header_features}
        {foreach $product.header_features as $features}
           {$features_str[] = "{$features.description} : {$features.variant}"}
        {/foreach}
        <meta name="keywords" content="{', '|implode:$features_str|strip}"/>
    {elseif $category_data}
        <meta name="keywords" content="{$category_brands_meta|implode:","}"/>
    {else}
    <meta name="keywords" content="{$meta_keywords|default:$location_data.meta_keywords}"/>
    {/if}
{/hook}
{$location_data.custom_html nofilter}
