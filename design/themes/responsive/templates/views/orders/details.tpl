<div class="ty-orders-detail">
    {* {assign var="remaining" value=$order_info|fn_get_order_complete_remaining_days}
    {if $remaining}
    <h1><span class="ty-product-feature__label bbox">Koha e arritjes:</span><strong
            class="product-details_primary">Përafërsisht {$remaining} ditë</strong></h1>
    {/if} *}

    {if $order_info.company_id != 1}
        {assign var="currency" value="ALL"}
    {else}
        {assign var="currency" value="EUR"}
    {/if}

    {if $order_info}
    <div style="background: #fff;">
        {capture name="order_actions"}
        {if $view_only != "Y"}
        <div class="ty-orders__actions">
            {hook name="orders:details_tools"}
            {assign var="print_order" value=__("print_invoice")}
            {assign var="print_pdf_order" value=__("print_pdf_invoice")}

            {if $status_settings.appearance_type == "C" && $order_info.doc_ids[$status_settings.appearance_type]}
            {assign var="print_order" value=__("print_credit_memo")}
            {assign var="print_pdf_order" value=__("print_pdf_credit_memo")}
            {elseif $status_settings.appearance_type == "O"}
            {assign var="print_order" value=__("print_order_details")}
            {assign var="print_pdf_order" value=__("print_pdf_order_details")}
            {/if}

            {include file="buttons/button.tpl" but_role="text" but_text=$print_order
            but_href="orders.print_invoice?order_id=`$order_info.order_id`" but_meta="cm-new-window ty-btn__text"
            but_icon="ty-icon-print orders-print__icon"}

            {include file="buttons/button.tpl" but_role="text" but_meta="orders-print__pdf ty-btn__text cm-no-ajax"
            but_text=$print_pdf_order but_href="orders.print_invoice?order_id=`$order_info.order_id`&format=pdf"
            but_icon="ty-icon-doc-text orders-print__icon"}

            {if $order_info.payment_id == 28}
            {include file="buttons/button.tpl" but_role="text" but_text='Shtyp faturën KosGiro'
            but_href="orders.print_kosgiro_invoice?order_id=`$order_info.order_id`" but_meta="cm-new-window ty-btn__text"
            but_icon="ty-icon-print orders-print__icon"}

            {include file="buttons/button.tpl" but_role="text" but_meta="orders-print__pdf ty-btn__text cm-no-ajax"
            but_text='Shtyp faturën KosGiro (pdf)'
            but_href="orders.print_kosgiro_invoice?order_id=`$order_info.order_id`&format=pdf"
            but_icon="ty-icon-doc-text orders-print__icon"}
            {/if}
            {/hook}

            <div class="ty-orders__actions-right">
                {if $view_only != "Y"}
                {hook name="orders:details_bullets"}
                {/hook}
                {/if}

                {include file="buttons/button.tpl" but_meta="ty-btn__text" but_role="text" but_text=__("re_order")
                but_href="orders.reorder?order_id=`$order_info.order_id`" but_icon="ty-orders__actions-icon ty-icon-cw"}
            </div>

        </div>
        {/if}
        {/capture}
    </div>
    {capture name="tabsbox"}

    <div id="content_general" class="{if $selected_section && $selected_section != " general"}hidden{/if}">
        {* {$refused_statuses = ['F','D','B','I','X']} *}
        {$refused_statuses = ['F','D','I','X']}
        {if $order_info.status|in_array:$refused_statuses} {assign var="refused" value="true"}{/if}
        <div class="order-steps">
            {foreach $tracking_steps as $step}
            <div class="{if $step.active}active{/if} {if $refused}cancel{/if} step {if $step.current}current{/if}">
                <span>
                    <img src="images/order-stepper/{if $refused}cancel{else}{$step.status}{/if}.svg" />
                </span>
                <div class="st-text">
                    <h3>{$step.name}</h3>
                    <p>{if $step.active}{$step.status_desc}{else}{$step.status_desc_inactive}{/if}</p>
                </div>
            </div>
            {/foreach}
        </div>
        <div style="margin-top: 30px;">
        {if $order_info.status == 'L' && $tracking_steps[3].is_aproved_by_client == false}
            <div style="padding-top: 10px;" class="review-button">
                <a style="color: white; text-decoration: none;" href="{"orders.update_order_status&order_id={$order_info.order_id}&status='C'"|fn_url}"><svg
                        xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1"
                        id="Capa_1" x="0px" y="0px" viewBox="0 0 288.579 288.579"
                        style="vertical-align: middle;height: 15px;" xml:space="preserve" width="24px" height="24px">
                        <g>
                            <path
                                d="M283.127,57.184l-22.871-22.131c-7.101-6.874-18.438-6.683-25.311,0.424L113.442,161.085   c-6.88,7.107-19.404,8.879-27.985,3.962l-42.824-24.542c-8.568-4.917-19.512-1.951-24.428,6.629l-15.83,27.615   c-4.917,8.58-1.951,19.518,6.623,24.434c0,0,103.889,59.46,103.931,59.376c0.048-0.084,137.25-141.57,170.617-176.058   C290.419,75.389,290.228,64.052,283.127,57.184z"
                                fill="#FFDA44"></path>
                        </g>
                        <g style="margin-top: 9px;"> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                    </svg> Kam pranuar porosinë</a>

            </div>
            <p style="text-align: center;margin-top: 10px;">{__('i_received_the_product')}</p>
        {/if}
        {if $order_info.status == 'C' || $tracking_steps[3].is_aproved_by_client == true}
            {assign var='hash' value="email_rating_security_hash_654sdf546fe.{$order_info.email}"|md5}
            <div class="review-button">
                <a style="color: white; text-decoration: none;" href="{"pages.add-discussion&order_id={$order_info.order_id}&e_key={$hash}"|fn_url}"><svg baseProfile="tiny"
                        height="24px" id="Layer_1" version="1.2" viewBox="0 0 24 24" width="24px" xml:space="preserve"
                        xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                        style=" margin-bottom: -6px; margin-right: 2px; margin-left: -2px;">
                        <g>
                            <g>
                                <path
                                    d="M9.362,9.158c0,0-3.16,0.35-5.268,0.584c-0.19,0.023-0.358,0.15-0.421,0.343s0,0.394,0.14,0.521    c1.566,1.429,3.919,3.569,3.919,3.569c-0.002,0-0.646,3.113-1.074,5.19c-0.036,0.188,0.032,0.387,0.196,0.506    c0.163,0.119,0.373,0.121,0.538,0.028c1.844-1.048,4.606-2.624,4.606-2.624s2.763,1.576,4.604,2.625    c0.168,0.092,0.378,0.09,0.541-0.029c0.164-0.119,0.232-0.318,0.195-0.505c-0.428-2.078-1.071-5.191-1.071-5.191    s2.353-2.14,3.919-3.566c0.14-0.131,0.202-0.332,0.14-0.524s-0.23-0.319-0.42-0.341c-2.108-0.236-5.269-0.586-5.269-0.586    s-1.31-2.898-2.183-4.83c-0.082-0.173-0.254-0.294-0.456-0.294s-0.375,0.122-0.453,0.294C10.671,6.26,9.362,9.158,9.362,9.158z"
                                    style="
            fill: gold;
        "></path>
                            </g>
                        </g>
                    </svg> Vlerëso produktin</a>
            </div>
            {if $tracking_steps[3].is_aproved_by_client == true}
            <p style="text-align: center">Ju keni konfirmuar pranimin e porosisë</p>
            {/if}
        {/if}
        </div>
        {if $without_customer != "Y"}
        {* Customer info *}
        <div class="orders-customer">
            {if $tracking_steps == null}
            {include file="views/profiles/components/profiles_info.tpl" user_data=$order_info location="I"}
            {/if}
        </div>
        {* /Customer info *}
        {/if}


        {capture name="group"}

        {include file="common/subheader.tpl" title=__("products_information")}
        <table class="ty-orders-detail__table ty-table">
            {hook name="orders:items_list_header"}
            <thead>
                <tr>
                    <th class="ty-orders-detail__table-product">{__("product")}</th>
                    <th class="ty-orders-detail__table-price">{__("price")}</th>
                    <th class="ty-orders-detail__table-quantity">{__("quantity")}</th>
                    {if $order_info.use_discount}
                    <th class="ty-orders-detail__table-discount">{__("discount")}</th>
                    {/if}
                    {if $order_info.taxes && $settings.General.tax_calculation != "subtotal"}
                    <th class="ty-orders-detail__table-tax">{__("tax")}</th>
                    {/if}
                    <th class="ty-orders-detail__table-subtotal">{__("subtotal")}</th>
                </tr>
            </thead>
            {/hook}
            {assign var="total_gjflex" value=0}

            {foreach from=$order_info.products item="product" key="key"}
            {hook name="orders:items_list_row"}
            {if !$product.extra.parent}
            {*{assign var="container" value=intval($product['product_id']/8000+1)}*}
            {*{if $container gt 5}*}
            {*{$container=5}*}
            {*{/if}*}
            <tr class="ty-valign-top">
                <td>
                    <div class="thumbnail-wrapper">
                        <span class="order_thumbnail">
                            <img class="thumbnail-img"
                                src="{get_images_from_blob product_id=$product.product_id count=1}">
                        </span>
                    </div>
                    {if $product.is_accessible}<a href="{"products.view?product_id=`$product.product_id`"|fn_url}">{/if}
                        <h3 class="no-margin">{$product.product nofilter}</h3>
                        {if $product.is_accessible}</a>{/if}

                    {if $product.extra.is_edp == "Y"}
                    <div class="ty-right"><a href="{"orders.order_downloads?order_id=`$order_info.order_id`"|fn_url}">{__("download")}</a></div>
                    {/if}
                    {if $product.product_code}
                    <div class="ty-orders-detail__table-code">
                        <p>{__("sku")}
                            :&nbsp;{$product.product_code}</p>
                    </div>
                    {/if}
                    {hook name="orders:product_info"}
                    {if $product.product_options}{include file="common/options_info.tpl"
                    product_options=$product.product_options inline_option=true}{/if}
                    {/hook}
                </td>

                {assign var="gjflex" value=$product.base_price|fn_calculate_gjflex}

                {if $product.extra.gjflex_fixed && $product.extra.gjflex_fixed != 0}
                    {assign var="gjflex" value=$product.extra.gjflex_fixed|round:2}
                {/if}

                <td class="ty-right">
                    {if $product.extra.exclude_from_calculate}{__("free")}{else}
{*                        {$product.original_price} {$currency_symbol}*}
                        {if $product.list_price && $product.list_price > $product.original_price}
                            {include file="common/price.tpl" value=$product.list_price}
                        {else}
                            {include file="common/price.tpl" value=$product.original_price}
                        {/if}
                    {/if}
                    {if $product.gjflex}
                    <div>+</div>
                    <div>
                        <strong>GjirafaFLEX</strong>
{*                        {$gjflex} {$currency_symbol}*}
                        {include file="common/price.tpl" value=$gjflex currency=$currency}
                    </div>
                    {/if}
                </td>
                <td class="ty-center">&nbsp;{$product.amount}</td>
                {if $order_info.use_discount}
                <td class="ty-right">
                    {if $product.extra.discount|floatval}
{*                        {$product.extra.discount} {$currency_symbol}*}
                        {include file="common/price.tpl" value=$product.extra.discount currency=$currency}
                    {else}-{/if}
                </td>
                {/if}

                {if $order_info.taxes && $settings.General.tax_calculation != "subtotal"}
                <td class="ty-center">
                    {if $product.tax_value|floatval}
{*                        {$product.tax_value} {$currency_symbol}*}
                        {include file="common/price.tpl" value=$product.tax_value currency=$currency}
                    {else}-{/if}
                </td>
                {/if}

                <td class="ty-right">
                    {if $product.gjflex}
                        {$total_gjflex = $total_gjflex + ($gjflex * $product.amount)}
                    {/if}
                    {if $product.extra.exclude_from_calculate}{__("free")}{else}
                        {include file="common/price.tpl" value=$product.display_subtotal}
                    {/if}
                </td>
            </tr>
            {/if}
            {/hook}
            {/foreach}

            {hook name="orders:extra_list"}
            {assign var="colsp" value=5}
            {if $order_info.use_discount}{assign var="colsp" value=$colsp+1}{/if}
            {if $order_info.taxes && $settings.General.tax_calculation != "subtotal"}{assign var="colsp"
            value=$colsp+1}{/if}
            {/hook}

        </table>
        {*Customer notes*}
        {if $order_info.notes}
        <div class="ty-orders-notes">
            {include file="common/subheader.tpl" title=__("customer_notes")}
            <div class="ty-orders-notes__body">
                <span class="ty-caret"><span class="ty-caret-outer"></span><span class="ty-caret-inner"></span></span>
                {$order_info.notes}
            </div>
        </div>
        {/if}
        {*/Customer notes*}
        <div class="ty-orders-summary clearfix">
            {include file="common/subheader.tpl" title=__("summary")}
            <div class="ty-orders-summary__right">
                {hook name="orders:info"}{/hook}
            </div>
            <div class="ty-orders-summary__wrapper">
                <table class="ty-orders-summary__table">
                    {hook name="orders:totals"}
                    {if $order_info.payment_id}
                    <tr class="ty-orders-summary__row">
                        <td>{__("payment_method")}:</td>
                        <td style="width: 57%" data-ct-orders-summary="summary-payment">
                            {hook name="orders:totals_payment"}
                            {$order_info.payment_method.payment} {if
                            $order_info.payment_method.description}({$order_info.payment_method.description}){/if}
                            {/hook}
                        </td>
                    </tr>
                    {/if}

                    {if $order_info.shipping}
                    <tr class="ty-orders-summary__row">
                        <td>{__("shipping_method")}:</td>
                        <td data-ct-orders-summary="summary-ship">
                            {hook name="orders:totals_shipping"}
                            {if $use_shipments}
                            <ul>
                                {foreach from=$order_info.shipping item="shipping_method"}
                                <li>{if $shipping_method.shipping} {$shipping_method.shipping} {else} – {/if}</li>
                                {/foreach}
                            </ul>
                            {else}
                            {foreach from=$order_info.shipping item="shipping" name="f_shipp"}
                            {if $shipments[$shipping.group_key].tracking_number}
                            {include file="common/carriers.tpl" carrier=$shipments[$shipping.group_key].carrier
                            tracking_number=$shipments[$shipping.group_key].tracking_number}
                            {strip}
                            {$shipping.shipping}&nbsp;
                            ({__("tracking_number")}:&nbsp;
                            {if $smarty.capture.carrier_url|trim != ""}
                            <a {if $smarty.capture.carrier_url|strpos:"://"}target="_blank" {/if}
                                href="{$smarty.capture.carrier_url nofilter}">
                                {/if}
                                {$shipments[$shipping.group_key].tracking_number}
                                {if $smarty.capture.carrier_url|trim != ""}
                            </a>
                            {/if})
                            {/strip}

                            {$smarty.capture.carrier_info nofilter}
                            {else}
                            {$shipping.shipping}
                            {/if}
                            {if !$smarty.foreach.f_shipp.last}<br>{/if}
                            {/foreach}
                            {/if}
                            {/hook}
                        </td>
                    </tr>
                    {/if}
                    <tr class="ty-orders-summary__row">
                        <td>{__("subtotal")}:&nbsp;</td>
                        <td data-ct-orders-summary="summary-subtotal">
{*                            {$order_info.display_subtotal} {$currency_symbol}*}
                            {include file="common/price.tpl" value=$order_info.display_subtotal currency=$currency}
                        </td>
                    </tr>
                    {if $total_gjflex != 0}
                    <tr class="ty-orders-summary__row">
                        <td>{__("gjirafa_flex")}:&nbsp;</td>
                        <td data-ct-orders-summary="summary-subtotal">
{*                            {$total_gjflex} {$currency_symbol}*}
                            {include file="common/price.tpl" value=$total_gjflex currency=$currency}
                        </td>
                    </tr>
                    {/if}
                    {if $order_info.display_shipping_cost|floatval}
                    <tr class="ty-orders-summary__row">
                        <td>{__("shipping_cost")}:&nbsp;</td>
                        <td data-ct-orders-summary="summary-shipcost">
{*                            {$order_info.display_shipping_cost} {$currency_symbol}*}
                            {include file="common/price.tpl" value=$order_info.display_shipping_cost currency=$currency}
                        </td>
                    </tr>
                    {/if}

                    {if $order_info.discount|floatval}
                    <tr class="ty-orders-summary__row">
                        <td class="ty-strong">{__("including_discount")}:</td>
                        <td class="ty-nowrap" data-ct-orders-summary="summary-discount">
{*                            {$order_info.discount} {$currency_symbol}*}
                            {include file="common/price.tpl" value=$order_info.discount currency=$currency}
                        </td>
                    </tr>
                    {/if}

                    {if $order_info.subtotal_discount|floatval}
                    <tr class="ty-orders-summary__row">
                        <td class="ty-strong">{__("order_discount")}:</td>
                        <td class="ty-nowrap" data-ct-orders-summary="summary-sub-discount">
{*                            {$order_info.subtotal_discount} {$currency_symbol}*}
                            {include file="common/price.tpl" value=$order_info.subtotal_discount currency=$currency}
                        </td>
                    </tr>
                    {/if}

                    {if $order_info.coupons}
                    {foreach from=$order_info.coupons item="coupon" key="key"}
                    <tr class="ty-orders-summary__row">
                        <td class="ty-nowrap">{__("coupon")}:</td>
                        <td data-ct-orders-summary="summary-coupons">{$key}</td>
                    </tr>
                    {/foreach}
                    {/if}

                    {if $order_info.taxes}
                    <tr class="taxes">
                        <td><strong>{__("taxes")}:</strong></td>
                        <td>&nbsp;</td>
                    </tr>
                    {foreach from=$order_info.taxes item=tax_data}
                    <tr class="ty-orders-summary__row">
                        <td class="ty-orders-summary__taxes-description">
                            {$tax_data.description}
                            {include file="common/modifier.tpl" mod_value=$tax_data.rate_value
                            mod_type=$tax_data.rate_type}
                            {if $tax_data.price_includes_tax == "Y" && ($settings.Appearance.cart_prices_w_taxes != "Y"
                            || $settings.General.tax_calculation == "subtotal")}
                            {__("included")}
                            {/if}
                            {if $tax_data.regnumber}
                            ({$tax_data.regnumber})
                            {/if}
                        </td>
                        <td class="ty-orders-summary__taxes-description" data-ct-orders-summary="summary-tax-sub">
{*                            {$tax_data.tax_subtotal} {$currency_symbol}*}
                            {include file="common/price.tpl" value=$tax_data.tax_subtotal currency=$currency}
                        </td>
                    </tr>
                    {/foreach}
                    {/if}
                    {if $order_info.tax_exempt == "Y"}
                    <tr class="ty-orders-summary__row">
                        <td>{__("tax_exempt")}</td>
                        <td>&nbsp;</td>
                    <tr>
                        {/if}

                        {if $order_info.payment_surcharge|floatval && !$take_surcharge_from_vendor}
                    <tr class="ty-orders-summary__row">
                        <td>{$order_info.payment_method.surcharge_title|default:__("payment_surcharge")}
                            :&nbsp;
                        </td>
                        <td data-ct-orders-summary="summary-surchange">
{*                            {$order_info.payment_surcharge} {$currency_symbol}*}
                            {include file="common/price.tpl" value=$order_info.payment_surcharge currency=$currency}
                        </td>
                    </tr>
                    {/if}
                    {hook name="orders:order_total"}
                    <tr class="ty-orders-summary__row">
                        <td class="ty-orders-summary__total">{__("total")}:&nbsp;</td>
                        <td class="ty-orders-summary__total" data-ct-orders-summary="summary-total">
{*                            {$order_info.total} {$currency_symbol}*}
                            {include file="common/price.tpl" value=$order_info.total currency=$currency}
                        </td>
                    </tr>
                    {/hook}
                    {/hook}
                </table>
            </div>
        </div>

        {if $order_info.promotions}
            <div style="margin-top: 30px; background: #fff; border-radius: 7px; box-shadow: 1px 2px 6px -3px rgba(0, 0, 0, 0.28); padding: 15px;">
                <h3>Promovimet e porosisë</h3>
                {include file="views/orders/components/promotions.tpl" promotions=$order_info.promotions}
            </div>
        {/if}

        {if $view_only != "Y"}
        <div class="ty-orders-repay">
            {hook name="orders:repay"}
            {if $settings.Checkout.repay == "Y" && $payment_methods}
            {include file="views/orders/components/order_repay.tpl"}
            {/if}
            {/hook}
        </div>
        {/if}

        {/capture}
        <div class="ty-orders-detail__products orders-product">
            {include file="common/group.tpl" content=$smarty.capture.group}
        </div>
    </div>
    <!-- main order info -->

    {if !"ULTIMATE:FREE"|fn_allowed_for}
    {if $use_shipments}
    <div id="content_shipment_info" class="ty-orders-shipment {if $selected_section != " shipment_info"}hidden{/if}">
        {foreach from=$shipments item="shipment"}
        {include file="common/subheader.tpl" title="{__("shipment")} #`$shipment.shipment_id`"}
        <div class="ty-orders-shipment__info">
            {hook name="orders:shipment_info"}
            <p>{$shipment.shipping}</p>
            {if $shipment.tracking_number || $shipment.carrier}
            {include file="common/carriers.tpl" carrier=$shipment.carrier tracking_number=$shipment.tracking_number
            shipment_id=$shipment.shipment_id}
            <p>
                {strip}
                {if $shipment.carrier}
                {__("carrier")}: {$smarty.capture.carrier_name nofilter}
                {/if}
                {if $shipment.carrier && $shipment.tracking_number}
                (
                {/if}
                {if $shipment.tracking_number}
                {__("tracking_number")}:
                {if $smarty.capture.carrier_url|trim != ""}
                <a {if $smarty.capture.carrier_url|strpos:"://"}target="_blank" {/if}
                    href="{$smarty.capture.carrier_url nofilter}">
                    {/if}
                    {$shipment.tracking_number}
                    {if $smarty.capture.carrier_url|trim != ""}
                </a>
                {/if}
                {/if}
                {if $shipment.carrier && $shipment.tracking_number}
                )
                {/if}
                {/strip}
            </p>
            {$smarty.capture.carrier_info nofilter}
            {/if}
            {/hook}
        </div>
        <table class="ty-orders-shipment__table ty-table">
            <thead>
                <tr>
                    <th style="width: 90%">{__("product")}</th>
                    <th>{__("quantity")}</th>
                </tr>
            </thead>
            {foreach from=$shipment.products key="product_hash" item="amount"}
            {if $order_info.products.$product_hash}
            {assign var="product" value=$order_info.products.$product_hash}
            <tr style="vertical-align: top;">
                <td>{if $product.is_accessible}<a href="{"products.view?product_id=`$product.product_id`"|fn_url}"
                        class="product-title">{/if}{$product.product nofilter}{if $product.is_accessible}</a>{/if}
                    {if $product.extra.is_edp == "Y"}
                    <div class="ty-right"><a href="{"orders.order_downloads?order_id=`$order_info.order_id`"|fn_url}">[{__("download")}
                            ]</a></div>
                    {/if}
                    {if $product.product_code}
                    <p>{__("sku")}: {$product.product_code}</p>
                    {/if}
                    {if $product.product_options}{include file="common/options_info.tpl"
                    product_options=$product.product_options inline_option=true}{/if}
                </td>
                <td class="ty-center">{$amount}</td>
            </tr>
            {/if}
            {/foreach}
        </table>
        {if $shipment.comments}
        <div class="ty-orders-shipment-notes__info">
            <h4 class="ty-orders-shipment-notes__header">{__("comments")}: </h4>
            <div class="ty-orders-shipment-notes__body">
                <span class="caret"> <span class="ty-caret-outer"></span> <span class="ty-caret-inner"></span></span>
                {$shipment.comments}
            </div>
        </div>
        {/if}

        {foreachelse}
        <p class="ty-no-items">{__("text_no_shipments_found")}</p>
        {/foreach}
    </div>
    {/if}
    {/if}

    {hook name="orders:tabs"}
    {/hook}

    {/capture}
    {include file="common/tabsbox.tpl" top_order_actions=$smarty.capture.order_actions content=$smarty.capture.tabsbox
    active_tab=$smarty.request.selected_section}

    {/if}
</div>

{hook name="orders:details"}
{/hook}

{capture name="mainbox_title"}
{__("order")}&nbsp;#{$order_info.order_id}
<em class="ty-date">({$order_info.timestamp|date_format:"`$settings.Appearance.date_format`, `$settings.Appearance.time_format`"})</em>
<em class="ty-status">{__("status")}: {include file="common/status.tpl" status=$order_info.status display="view" name="update_order[status]"}</em>
{if $settings.General.display_delivery == 'Y' && !$al|fn_isAL && $delivery_dates}
    <em class="ty-date">(Data e dërgesës: {$delivery_dates.standard|date_format:$settings.Appearance.date_format|fn_custom_date_format} - {$delivery_dates.standard_range|date_format:$settings.Appearance.date_format})</em>
{/if}
{*{if !$delivery_dates && $order_info.no_delivery_date}*}
{if !$delivery_dates}
    <em class="ty-date">(Data e dërgesës: E papërcaktuar)</em>
{/if}
{/capture}


{literal}
<style>
    .tygh-content {
        background: #f1f1f1;
    }

    .ty-mainbox-title,
    .ty-orders__actions,
    .ty-tabs,
    .order-steps,
    .ty-table,
    .ty-orders-summary__wrapper {
        background: #fff;
        padding: 15px;
        border-radius: 7px;
        box-shadow: 1px 2px 6px -3px rgba(0, 0, 0, 0.28);
        margin: 0;
        border: 0;
        margin-top: 20px;
    }

    .ty-tabs__content {
        padding: 0;
    }

    .ty-mainbox-title {
        position: relative;
    }

    .ui-widget-content {
        background: transparent;
    }


    .ty-orders__actions {
        border-bottom-left-radius: 0;
        border-bottom-right-radius: 0;
    }

    .ty-tabs {
        border-top-left-radius: 0;
        border-top-right-radius: 0;
        margin-top: 0;
        padding: 10px 0 0 0;
        border-top: 1px solid #f7f7f7;
    }

    .ty-tabs__item {
        background: transparent;
        border: 0;
        position: relative;
    }

    .ty-tabs__item.active:after {
        content: "";
        position: absolute;
        bottom: 0;
        left: 0;
        width: 100%;
        height: 3px;
        border-top-right-radius: 4px;
        border-top-left-radius: 4px;
        background: #e65228;
    }

    .ty-subheader {
        display: none;
    }

    .ty-table {
        overflow: hidden;
        padding: 0;
    }

    .ty-table th {
        background: transparent;
    }

    .ty-orders-summary,
    .ty-orders-detail__products {
        margin-top: 0;
    }

    .order_thumbnail {
        width: 100px;
        height: 100px;
        float: left;
        margin-right: 20px;
    }

    .thumbnail-img {
        max-height: 100px;
        width: 100px;
        object-fit: contain;
    }

    @media (max-width: 768px) {
        .ty-table__responsive-header {
            width: 100%;
        }
    }
</style>
{/literal}