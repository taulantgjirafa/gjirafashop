{capture name="section"}
{include file="views/orders/components/orders_search_form.tpl"}
{/capture}
{include file="common/section.tpl" section_title=__("search_options") section_content=$smarty.capture.section
class="ty-search-form"}

{assign var="c_url" value=$config.current_url|fn_query_remove:"sort_by":"sort_order"}
{if $search.sort_order == "asc"}
{assign var="sort_sign" value="<i class=\"ty-icon-down-dir\"></i>"}
{else}
{assign var="sort_sign" value="<i class=\"ty-icon-up-dir\"></i>"}
{/if}
{if !$config.tweaks.disable_dhtml}
{assign var="ajax_class" value="cm-ajax"}

{/if}

{include file="common/pagination.tpl"}

{foreach from=$orders item="order"}
    {$order_data = $order.order_id|fn_get_order_info}
    {$tracker = $order_data|fn_get_tracking_steps}
    <div class="tracking-orders" style="background: #fff;
    margin-bottom: -10px;
    border-radius: 0;
    border: 1px solid #ecf0f1;">
        {if $tracker}
            <div class="track-progress">
            <div class="flexing flex-h-between">
                {foreach from=$tracker item="track" key="key"}
                {if $track.current == true}
                    {if $key == 0}
                        {$percentage = 1}
                    {elseif $key == $tracker|count - 1}
                        {$percentage = 100}
                    {elseif $key > 0}
                        {$percentage = ($key / ($tracker|count - 1)) * 100}
                    {/if}
                {/if}
                <span class="track-">{$track.name}</span>
                {/foreach}
            </div>
            <span class="track-bar">
                <span style="width: {$percentage}% !important;"></span>
            </span>
        </div>
        {/if}
        {foreach from=$order_data.products item="product"}
            <div class="tracking-products" style="max-width:100%; border:1px solid #eee;">
            <div class="tp-first-prod flexing flex-v-center">

                <div class="tp-desc flexing flex-h-between flex-grow">
                    <div class="sub-desc">
                        <div class="tp-img">
                            <img src="https://hhstsyoejx.gjirafa.net/gj50/img/{$product.product_id}/thumb/0.jpg" alt="">
                        </div>
                        <div>
                            <h4 class="mb0 mb0 max-rows mr2">{$product.product}</h4>
                            <p class="text-muted"><strong>{__('price')}: </strong> {include file="common/price.tpl" value=$product.price}</p>
                            <span class="text-muted p0"><strong>{__('amount')}:</strong> {$product.amount}</span>
                        </div>
                    </div>
                    {if !$smarty.const.IS_MOBILE}
                        <div class="flex-grow sub-desc">
                            <strong>{__('product_code')}:</strong>
                            <p>{$product.product_code}</p>
                        </div>
                    {/if}
                    <div class="flex-grow sub-desc">
                        <strong>{__('subtotal')}:</strong>
                        {if $order_data.company_id != 1}
                            <p>{include file="common/price.tpl" value=$product.price currency='ALL'}</p>
                        {else}
                            <p>{include file="common/price.tpl" value=$product.price currency='EUR'}</p>
                        {/if}
                    </div>
                </div>
            </div>
        </div>
        {/foreach}
    </div>
    {break}
{/foreach}

<table class="ty-table ty-orders-search">
    <thead>
        <tr>
            <th><a class="{$ajax_class}" href="{"`$c_url`&sort_by=order_id&sort_order=`$search.sort_order_rev`"|fn_url}"
                    data-ca-target-id="pagination_contents">{__("id")}</a>{if $search.sort_by == "order_id"}{$sort_sign
                nofilter}{/if}</th>
            <th><a class="{$ajax_class}" href="{"`$c_url`&sort_by=status&sort_order=`$search.sort_order_rev`"|fn_url}"
                    data-ca-target-id="pagination_contents">{__("status")}</a>{if $search.sort_by ==
                "status"}{$sort_sign nofilter}{/if}</th>
            <th><a class="{$ajax_class}" href="{"`$c_url`&sort_by=customer&sort_order=`$search.sort_order_rev`"|fn_url}"
                    data-ca-target-id="pagination_contents">{__("customer")}</a>{if $search.sort_by ==
                "customer"}{$sort_sign nofilter}{/if}</th>
            <th><a class="{$ajax_class}" href="{"`$c_url`&sort_by=date&sort_order=`$search.sort_order_rev`"|fn_url}"
                    data-ca-target-id="pagination_contents">{__("date")}</a>{if $search.sort_by == "date"}{$sort_sign
                nofilter}{/if}</th>

            {hook name="orders:manage_header"}{/hook}

            {if $settings.General.display_delivery == 'Y' && !$al|fn_isAL}
                <th>Data e dërgesës</th>
            {/if}

            <th><a class="{$ajax_class}" href="{"`$c_url`&sort_by=total&sort_order=`$search.sort_order_rev`"|fn_url}"
                    data-ca-target-id="pagination_contents">{__("total")}</a>{if $search.sort_by == "total"}{$sort_sign
                nofilter}{/if}</th>
            <th width="15%"></th>
        </tr>
    </thead>
    {foreach from=$orders item="o"}
    <tr class="header">
        <td class="ty-orders-search__item"><a href="{"orders.details?order_id=`$o.order_id`"|fn_url}"><strong>#{$o.order_id}</strong></a></td>
        <td class="ty-orders-search__item">{include file="common/status.tpl" status=$o.status display="view"}</td>
        <td class="ty-orders-search__item">
            <ul class="ty-orders-search__user-info">
                <li class="ty-orders-search__user-name">{$o.firstname} {$o.lastname}</li>
{*                <li class="ty-orders-search__user-mail"><a href="mailto:{$o.email|escape:url}">{$o.email}</a></li>*}
            </ul>
        </td>
        <td class="ty-orders-search__item"><a href="{"orders.details?order_id=`$o.order_id`"|fn_url}">{$o.timestamp|date_format:$settings.Appearance.date_format:"`$settings.Appearance.date_format`,`$settings.Appearance.time_format`"}</a></td>

        {hook name="orders:manage_data"}{/hook}

        {if $settings.General.display_delivery == 'Y' && !$al|fn_isAL}
            <td class="ty-orders-search__item">
                {if $o.delivery_dates}
                    {$o.delivery_dates.standard|date_format:$settings.Appearance.date_format|fn_custom_date_format} - {$o.delivery_dates.standard_range|date_format:$settings.Appearance.date_format}
                {else}
                    E papërcaktuar
                {/if}
            </td>
        {/if}

        {if ($o.company_id)==$company_al}
        <td class="ty-orders-search__item">{$o.total} Lekë</td>
        {else}
        <td class="ty-orders-search__item">{$o.total} Euro</td>
        {/if}
        {if $o.products}
            <td>
                <button class="ty-btn__secondary ty-btn order-products-btn">Shiko produktet</button>
            </td>
        {else}
            <td></td>
        {/if}
    </tr>
    <tr style="display: none;" class="collaspable">
        <td colspan="7">
            <p><strong>Produktet në porosi:</strong></p>
            <div class="td-images">
            {foreach from=$o.products item="order_product"}
                <a href="{"products.view&product_id={$order_product.product_id}"|fn_url}"><img src="https://hhstsyoejx.gjirafa.net/gj50/img/{$order_product.product_id}/thumb/0.jpg" alt="" title="{$order_product.product}"></a>
            {/foreach}
            </div>
        </td>
    </tr>
    {foreachelse}
    <tr class="ty-table__no-items">
        <td colspan="7">
            <p class="ty-no-items">{__("text_no_orders")}</p>
        </td>
    </tr>
    {/foreach}
</table>

{include file="common/pagination.tpl"}

{capture name="mainbox_title"}{__("orders")}{/capture}

<script>
    var targetOffset = $(".ty-section").offset().top;
    $('.ty-section').css({ "position": "absolute", "height": "calc(100vh - " + targetOffset + "px)" });

    var $w = $(window).scroll(function () {
        if ($w.scrollTop() > targetOffset) {
            $('.ty-section').css({ "position": "fixed", "height": "100%" });

        } else {
            $('.ty-section').css({ "position": "absolute", "height": "calc(100vh - " + targetOffset + "px)" });
        }
    });
    if ($('.ty-status-info label input').is(':checked')) {
        $('.ty-status-info label input:checked').parent().addClass('active-label')
    }
    $('.ty-status-info label input').click(function () {
        if ($('.ty-status-info label input').is(':checked')) {
            $('.ty-status-info label input:checked').parent().addClass('active-label')
        }
        // $(this).closest('.cm-processed-form').submit();
    })

</script>
<style>
    .tygh-content {
        background: #f1f1f1;
    }

    @media screen and (max-width:768px) {
        .tygh-content {
            padding: 10px !important;
        }

        .tracking-orders {
            border-radius: 7px !important;
            margin-bottom: 20px !important;
            border: 0 !important;
            box-shadow: 1px 2px 6px -3px rgba(0, 0, 0, 0.28);
        }

        .tracking-products {
            margin-bottom: 0 !important;
        }

        .ty-table {
            margin: 0;
            border-radius: 7px;
            background: #fff;
            box-shadow: 1px 2px 6px -3px rgba(0, 0, 0, 0.28);
            border: 0;
            overflow: hidden;
        }

        .ty-table tr {
            border: 0;
        }

        .ty-table tbody tr:nth-child(2n) {
            border-top: 1px solid #eee;
            border-bottom: 1px solid #eee;
        }

        .order-products-btn {
            width: 100%;
        }
    }

    @media screen and (min-width:768px) {
        .cm-combination.cm-save-state.cm-ss-reverse {
            display: none;
        }

        .ty-period__wrapper {
            padding: 0;
            width: 100%;
        }


        .ty-calendar__block {
            width: 30%;
        }

        @media screen and (min-width: 768px) {
            .container-fluid.content-grid {
                padding-left: 280px;
            }
        }

        .ty-control-group>select,
        .ty-control-group>input,
        .ty-control-group>.ty-calendar__block,
        .ty-control-group__title {
            margin-left: 25px;
        }




        .ty-mainbox-title {
            display: none;
        }

        .ty-mainbox-container {
            margin-top: 20px;
        }

        .ty-table,
        .tracking-orders {
            border: 0 !important;
            border-radius: 7px !important;
            overflow: hidden;
            box-shadow: 1px 2px 6px -3px rgba(0, 0, 0, 0.28);
        }

        .ty-table th {
            background: #fff;
            border: 0;
        }

        .ty-table tbody {
            margin-top: 10px;
        }

        .ty-table tbody tr:nth-child(odd) {
            background: #f6f6f6;
        }

        .ty-table tbody tr:nth-child(2n) {
            background: #fff;
        }

        .ty-table td,
        .ty-table .table td {
            border: 0;
        }
    }
    .ty-table tbody tr:nth-child(4n),
    .ty-table tbody tr:nth-child(4n-1) {
        background: #fff !important;
    }
    .ty-table tbody tr:nth-child(4n-2),
    .ty-table tbody tr:nth-child(4n-3) {
        background: #f6f6f6 !important;
    }
    .ty-table tbody tr {
        border-bottom: 1px solid #eee;
    }
    .collaspable .ty-table__responsive-header {
        display: none;
    }
    .td-images {
        display: block;
        white-space: nowrap;
        overflow-x: auto;
    }
    .td-images img {
        max-height: 50px;
        padding: 10px;
        border-radius: 15px;
        box-shadow: 0px 0px 5px #eee;
        background: white;
        margin: 5px 5px 5px 0;
    }
</style>

<script>
    $('body').on('click', '.order-products-btn', function() {
        $(this).closest('tr').nextUntil('tr.header').slideToggle(100);
    });
</script>