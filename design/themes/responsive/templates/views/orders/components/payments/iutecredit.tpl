<script type="text/javascript">
	{literal}

	var iute_opened = false;

	$('body').on('click', '#place_order_tab1', function(e) {
		let payment_id = $('input[name="payment_id"]').val();
		let terms_checked = $('#id_accept_termstab1').is(':checked');
		let total = $('.ty-checkout-summary__total-sum span').text();

		if (payment_id == 27 && terms_checked && !iute_opened) {
			e.preventDefault();

			if (total < 120 || total > 1200) {

				$.ceNotification('show', {
					type: 'W',
					title: 'Njoftim',
					message: 'Totali i porosisë duhet të jetë brenda limitit 120€ deri në 1200€ për aplikim të kredisë.'
				});

				return;
			}

			$('#dark_overlay').show();
			$('#dark_overlay').animate({opacity: 1}, 200);
			$('#iute_text_overlay').show();
			$('#iute_text_overlay').animate({opacity: 1}, 200);

			setTimeout(function() {
				var iute_popup = popupCenter('https://iutecredit.org/application/#utm_source=gjirafa50', 'IuteCredit', '900', '600');

				if (iute_popup) {
					iute_opened = true;

					var iute_interval = setInterval(function() {
						if (iute_popup.closed) {
							clearInterval(iute_interval);

							$('#dark_overlay').animate({opacity: 0}, 200);
							$('#dark_overlay').hide(100);
							$('#iute_text_overlay').animate({opacity: 0}, 200);
							$('#iute_text_overlay').hide(100);

							$('#place_order_tab1').click();
						}
					}, 500);
				} else {
					$('.iute-popup-failed').show();
				}
			}, 2000);
		}
	});

	function popupCenter(url, title, w, h) {
		var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : window.screenX;
		var dualScreenTop = window.screenTop != undefined ? window.screenTop : window.screenY;

		var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
		var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

		var systemZoom = width / window.screen.availWidth;
		var left = (width - w) / 2 / systemZoom + dualScreenLeft
		var top = (height - h) / 2 / systemZoom + dualScreenTop
		var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w / systemZoom + ', height=' + h / systemZoom + ', top=' + top + ', left=' + left);

		if (window.focus) {
			newWindow.focus();
		}

		return newWindow;
	}

	{/literal}
</script>