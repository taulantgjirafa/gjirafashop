{script src="js/tygh/exceptions.js"}
{script src="js/tygh/checkout.js"}
{script src="js/tygh/recommended_slider.js"}

{*<div class="confirm-msg-check" style="">*}
{*    <p><b>Për shkak të disa problemeve teknike, në përfundim të porosisë procesi do të ju vonojë më shumë se zakonisht, mirëpo porosia juaj kompletohet pa ndonjë problem. Ju faleminderit për mirëkutimin tuaj.</b></p>*}
{*</div>*}

<div class="confirm-msg-check" style="">
    <p><b>Ju njoftojmë se klientët e rregullt me më shumë se një blerje në Gjirafa50, nuk do të telefonohen për konfirmim të porosisë.</b></p>
    <p>Përndryshe, të gjitha detajet e porosisë do i pranoni përmes email-it, për secilin status që kalon porosia juaj.</p>
</div>

{*{foreach from=$cart_products item="product"}*}
{*    {if $product.limited_to_payment_method_id}*}
{*        <div class="confirm-msg-check" style="">*}
{*            <p><b>Ju njoftojmë se klientët e rregullt me më shumë se një blerje në Gjirafa50, nuk do të telefonohen për konfirmim të porosisë.</b></p>*}
{*            <p>Përndryshe, të gjitha detajet e porosisë do i pranoni përmes email-it, për secilin status që kalon porosia juaj.</p>*}
{*        </div>*}
{*    {/if}*}
{*{/foreach}*}

{if $settings.General.display_delivery == 'Y' && $pdd.pdd && !$al|fn_isAL}
    <div class="checkout-notification">
        <p>
            {if $pdd.pdd|count eq 1}Produkti{else}Produktet{/if}
            {foreach from=$pdd.pdd item='spdd'}
                <b>{$spdd}{if $pdd.pdd|count neq 1}, {/if}</b>
            {/foreach}
            {if $pdd.pdd|count eq 1}ka{else}kanë{/if} kohë të liferimit 3 ditë më të gjatë se produktet tjera në shportë. Nëse dëshironi që produktet të ju mbërrijnë më herët, ju këshillojmë që të bëni porosi të veçantë për produktet më poshtë:
            {*            {if $pdd.pndd|count eq 1}produkti{else}produktet{/if}*}
            {*            {foreach from=$pdd.pndd item='spdd'}*}
            {*            <b>{$spdd}{if $pdd.pndd|count neq 1}, {/if}</b>{/foreach}. Nëse dëshironi që {if $pdd.pndd|count eq 1}produkti{else}produktet{/if}*}
            {*            {foreach from=$pdd.pndd item='spdd'}*}
            {*                <b>{$spdd}{if $pdd.pndd|count neq 1}, {/if}</b>*}
            {*            {/foreach}*}
            {*            të ju mbërrijnë më herët, ju këshillojmë që të bëni porosi të veçantë për {if $pdd.pdd|count eq 1}produktin{else}produktet{/if}*}
            {*            {foreach from=$pdd.pdd item='spdd' key='key'}*}
            {*            <b>{$spdd}{if $pdd.pdd|count neq 1 && $key < ($pdd.pdd|count - 1)}, {/if}</b>*}
            {*            {/foreach}.*}
        </p>
        {foreach from=$pdd.pdd item='spdd' key='key'}
            <div class="flexing align-items-center" style="border-top: 1px solid #eee;padding-top: 5px;margin-top: 5px;">
                <img src="https://hhstsyoejx.gjirafa.net/gj50/img/{$key}/thumb/0.jpg" style="max-width: 40px; margin-right: 10px;">
                <p style="font-weight: 600;">{$spdd}</p>
            </div>
        {/foreach}
    </div>
{/if}

{$smarty.capture.checkout_error_content nofilter}
{include file="views/checkout/components/checkout_steps.tpl"}

{if ($also_bought_products != null)}
    <div class="cart-recommended__wrapper">
        {include file="views/checkout/components/related_products.tpl"}
    </div>
{/if}

{capture name="mainbox_title"}<span class="ty-checkout__title">{__("secure_checkout")}&nbsp;<i class="ty-checkout__title-icon ty-icon-lock"></i></span>{/capture}

<style>
.confirm-msg-check{
    background: #fcfcfc;
    padding: 10px 14px;
    border: 2px solid #e45227;
    border-radius: 5px;
    margin-bottom: 15px;
}
.confirm-msg-check p{
    font-size:15px;
}

.checkout-notification {
    background: #fcfcfc;
    padding: 10px 14px;
    border: 2px solid #acacac;
    border-radius: 5px;
    margin-bottom: 15px;
}
</style>