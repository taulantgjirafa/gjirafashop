<style type="text/css">
	p {
		font-size: 16px;
	}
</style>

<div style="text-align: center; padding-bottom: 30px;">
	<img src="images/payment/iutecredit-logo.svg">
	<div style="margin: 20px 0;">
		<p>Pas <em id="iute_timer" style="color: #e65228; font-weight: bold;">5</em> sekondash do të hapet dritarja për aplikim në IuteCredit!</p>
		<p>Pasi që të përfundoni aplikimin, kthehuni në këtë faqe dhe vazhdoni porosinë.</p>
	</div>
	<div id="window_open" style="display: none;">
		<a href="https://iutecredit.org/application/#utm_source=gjirafa50" target="_blank" style="margin: 0 auto; text-transform: uppercase;" class="ty-btn ty-btn__primary"><b>Hap faqen e aplikimit</b></a>
		<p style="font-size: 14px; color: #e45227;">*Nëse nuk është hapur dritarja e aplikimit, ju lutem klikoni butonin.</p>
	</div>
	<button id="iute_close" style="display: none; margin: 0 auto; text-transform: uppercase;" class="ty-btn ty-btn__primary"><b>Kam përfunduar aplikimin</b></button>
	<div id="is_loader" class="is_loader active" style="margin: 0px auto;"></div>
</div>

<script>
	setTimeout(function() {
  		document.getElementById('is_loader').style.display = 'none';

		let tab = window.open('https://iutecredit.org/application/#utm_source=gjirafa50', '_blank');

		if (tab) {
			tab.focus();
			document.getElementById('iute_close').style.display = 'block';
		} else {
			document.getElementById('window_open').style.display = 'block';
		}
	}, 6000);

	var seconds = 4;

	iuteInterval = setInterval(function() {
		document.getElementById('iute_timer').textContent = seconds;
		seconds -= 1;

		if (seconds < 0) {
			clearInterval(iuteInterval);
		}
	}, 1000);

	document.getElementById('window_open').addEventListener('click', function() {
		document.getElementById('window_open').style.display = 'none';
		document.getElementById('iute_close').style.display = 'block';
	});

	document.getElementById('iute_close').addEventListener('click', function() {
		let notificationContainer = $('.cm-notification-content-extended:visible');
		$.ceNotification('close', notificationContainer, false);
	});
</script>