<style type="text/css">
	.no-spin::-webkit-inner-spin-button {
		-webkit-appearance: none;
	}

	.iute-submit-inactive {
	    pointer-events: none;
		opacity: .6;
	}

	.iute-loan-data {
		display: flex;
		flex-direction: row;
	}

	.iute-loan-data .form-row-container,
	.iute-form-fields .form-row-container .ty-control-group{
		padding-left: 10px;
		padding-right: 10px;
	}

	.cm-notification-content {
		top: 0;
		right: 0;
		left: 0;
		width: 100%;
		margin: 0;
		border: 0;
		overflow: auto;
		height: 100%;
	}

	.form-row-container {
		display: flex;
		justify-content: space-between;
	}

	.form-row-container .ty-control-group {
		width: 50%;
	}

	.form-row-container .ty-control-group .range-slider__value {
		height: 28px;
		width: 100%;
	}

	.iute-loan-data-row {
		display: flex;
		justify-content: space-between;
		padding: 9px 0;
		align-items: center;
		font-weight: bold;
	}

	.iute-loan-data-row span:last-child {
		color: #e65228;
	}

	.loan-data-spinner {
		width: 13px;
	}

	input[type=range] {
		height: 20px;
		background:#fafafa;
		-webkit-appearance: none;
		margin: 10px 0;
		width: 100%;
	}
	input[type=range]:focus {
		outline: none;
	}
	input[type=range]::-webkit-slider-runnable-track {
		width: 100%;
		height: 5px;
		cursor: pointer;
		animate: 0.2s;
		box-shadow: 0px 0px 0px #000000;
		background: #e65228;
		border-radius: 1px;
		border: 0px solid #000000;
	}
	input[type=range]::-webkit-slider-thumb {
		box-shadow: 0px 0px 0px #000000;
		border: 1px solid #e65228;
		height: 18px;
		width: 18px;
		border-radius: 25px;
		background: #ffbba8;
		cursor: pointer;
		-webkit-appearance: none;
		margin-top: -7px;
	}
	input[type=range]:focus::-webkit-slider-runnable-track {
		background: #e65228;
	}
	input[type=range]::-moz-range-track {
		width: 100%;
		height: 5px;
		cursor: pointer;
		animate: 0.2s;
		box-shadow: 0px 0px 0px #000000;
		background: #e65228;
		border-radius: 1px;
		border: 0px solid #000000;
	}
	input[type=range]::-moz-range-thumb {
		box-shadow: 0px 0px 0px #000000;
		border: 1px solid #e65228;
		height: 18px;
		width: 18px;
		border-radius: 25px;
		background: #ffbba8;
		cursor: pointer;
	}
	input[type=range]::-ms-track {
		width: 100%;
		height: 5px;
		cursor: pointer;
		animate: 0.2s;
		background: transparent;
		border-color: transparent;
		color: transparent;
	}
	input[type=range]::-ms-fill-lower {
		background: #e65228;
		border: 0px solid #000000;
		border-radius: 2px;
		box-shadow: 0px 0px 0px #000000;
	}
	input[type=range]::-ms-fill-upper {
		background: #e65228;
		border: 0px solid #000000;
		border-radius: 2px;
		box-shadow: 0px 0px 0px #000000;
	}
	input[type=range]::-ms-thumb {
		margin-top: 1px;
		box-shadow: 0px 0px 0px #000000;
		border: 1px solid #e65228;
		height: 18px;
		width: 18px;
		border-radius: 25px;
		background: #ffbba8;
		cursor: pointer;
	}
	input[type=range]:focus::-ms-fill-lower {
		background: #e65228;
	}
	input[type=range]:focus::-ms-fill-upper {
		background: #e65228;
	}
	.iute-container{
		background: #fafafa;
		box-sizing: border-box;
		border-radius: 4px;
		box-shadow: 0 3px 10px -3px rgba(0,0,0,0.2);
		padding: 10px;
		margin-bottom: 25px;
	}
	.ty-control-group input{
		height: 40px;
		border:1px solid #ddd;
		border-radius: 4px;
	}

	@media screen and (max-width: 768px) {
		.container{
			padding: 10px !important;

		}
		.form-row-container{
			width: 100% !important;
		}
		.iute-loan-data{
			flex-direction: column;
		}
		.iute-loan-data .form-row-container{
			padding:0;
		}
	}
</style>

<div style="text-align: center;">
	<img src="images/payment/iutecredit-logo.svg">
	<h1>Apliko për kredi në IuteCredit</h1>
</div>
<div class="container" style="padding: 0 30px 30px 30px; position: relative;">

    <div class="iute-container">
		<div class="form-row-container" style="width: 60%; padding:10px; box-sizing: border-box; margin:0 auto; ">
			<input style="width: 90%; padding-right: 20px;" id="periodInput" class="range-slider__range form-control-range" name="loan_period" type="range" min="1" value="1" max="12">
			<span class="flexing flex-v-center">
			<input style="margin-right: 5px; padding: 10px 5px 10px 20px; text-align: center; border: 1px solid #ddd; border-radius: 4px;" id="months" class="range-slider__value" type="number" value="1" min="1" max="5"><span> muaj</span>
			</span>
		</div>
		<div class="iute-loan-data">
			<div class="form-row-container" style="width: 50%;">
				<div style="width: 100%;">
					<div class="iute-loan-data-row">
						<span>SHUMA E KREDISË</span>
                        <span id="loanAmount" class="loan-amount"><img class="loan-data-spinner" src="images/spinner.gif"></span>
					</div>
					<div class="iute-loan-data-row">
						<span>PAGESA MUJORE NGA</span>
						<span id="monthlyPayment" class="monthly-repayment"><img class="loan-data-spinner" src="images/spinner.gif"></span>
					</div>
					<div class="iute-loan-data-row">
						<span>KOSTO ADMINISTRATIVE</span>
						<span id="adminFee" class="admin-fee"><img class="loan-data-spinner" src="images/spinner.gif"></span>
					</div>
					<div class="iute-loan-data-row">
						<span>SHUMA TOTALE E KREDISË</span>
						<span id="totalCost" class="total-cost"><img class="loan-data-spinner" src="images/spinner.gif"></span>
					</div>
				</div>
			</div>
			<div class="form-row-container" style="width: 50%;">
				<div style="width: 100%;">
					<div class="iute-loan-data-row">
						<span>PERIUDHA E KREDISË</span>
                        <span id="loanPeriod" class="loan-period"><img class="loan-data-spinner" src="images/spinner.gif"></span>
                    </div>
					<div class="iute-loan-data-row">
						<span>KOMISIONI</span>
						<span id="commissionFee" class="comm-fee"><img class="loan-data-spinner" src="images/spinner.gif"></span>
					</div>
					<div class="iute-loan-data-row">
						<span>KOSTO E INTERESIT</span>
						<span id="interestCost" class="interest-cost"><img class="loan-data-spinner" src="images/spinner.gif"></span>
					</div>
					<div class="iute-loan-data-row">
						<span>NORMA EFEKTIVE E INTERESIT</span>
						<span id="apr" class="apr"><img class="loan-data-spinner" src="images/spinner.gif"></span>
					</div>
				</div>
			</div>
		</div>
	</div>

	<form method="POST" action="{""|fn_url}" id="iute_form">
        <input id="amount" class="range-slider__value form-control" type="hidden" value="{$total}">

		<h1 style="padding: 0;">Informatat Personale</h1>
		<div class="iute-form-fields" style="margin: 0 -10px;">
			<div class="form-row-container">
				<div class="ty-control-group">
					<label for="firstname" class="ty-control-group__title">Emri</label>
					<input id="firstname" type="text" name="" style="width: 100%;" disabled value="{$user_data.b_firstname}">
				</div>
				<div class="ty-control-group">
					<label for="lastname" class="ty-control-group__title">Mbiemri</label>
					<input id="lastname" type="text" name="" style="width: 100%;" disabled value="{$user_data.b_lastname}">
				</div>
			</div>
			<div class="form-row-container">
				<div class="ty-control-group">
					<label for="phone" class="ty-control-group__title">Nr. Telefonit</label>
					<input id="phone" type="text" name="" style="width: 100%;" disabled value="{$user_data.b_phone}">
				</div>
				<div class="ty-control-group">
					<label for="email" class="ty-control-group__title">E-mail</label>
					<input id="email" type="text" name="" style="width: 100%;" disabled value="{$user_data.email}">
				</div>
			</div>
			<div class="form-row-container">
				<div class="ty-control-group">
					<label for="birthday" class="cm-required ty-control-group__title">Ditëlindja</label>
					{* <input type="date" value="" id="birthday" class="no-spin" name="birthday" style="width: 100%; background: none; height: 32px; padding-left: 5px; border: 1px solid #c3c3c3; width: 99%;"> *}
					<input type="text" value="" id="birthday" name="birthday" style="width: 100%;" placeholder="dd.mm.yyyy">
					<div style="padding-top: 5px;"><label id="birthday_error" style="display: none; color: #b94a48;">Fusha <b>Ditëlindja</b> është e detyrueshme dhe duhet të jetë në formatin <b>dd.mm.yyyy</b>.</label></div>
				</div>
				<div class="ty-control-group">
					<label for="personal_number" class="cm-required ty-control-group__title">Numri personal</label>
					<input id="personal_number" type="text" name="personal_number" style="width: 100%;" value="{$smarty.session.auth.iutecredit.personal_number}">
					<div style="padding-top: 5px;"><label id="personal_number_error" style="display: none; color: #b94a48;">Fusha <b>Numri personal</b> është e detyrueshme dhe duhet të përmbajë 10 shirfa.</label></div>
				</div>
			</div>
		</div>
		<div>
			<div class="ty-control-group product-subscribe" style="text-align: left; margin-bottom: 0;">
				<input type="checkbox" name="terms_and_conditions" id="terms_and_conditions" {if $smarty.session.auth.iutecredit.terms}checked{/if}>
				<label for="terms_and_conditions">
					<span><img src="images/stock.svg" alt=""></span>
					Pajtohem me <a href="https://iutecredit.org/rreth-nesh/kushtet/" style="color: #e45227; font-weight: bold;" target="_blank">Termet dhe kushtet</a>
				</label>
			</div>
			<div style="padding-top: 5px;"><label id="terms_and_conditions_error" style="display: none; color: #b94a48;">Fusha <b>Termet dhe kushtet</b> është e detyrueshme.</label></div>
			<div class="ty-control-group" style="text-align: right;">
				<span id="iute_success" style="display: none; float: left; font-weight: bold; color: #e65228; margin-top: 10px;">Të dhënat u ruajtën me sukses!</span>
	            {include file="buttons/button.tpl" but_meta="ty-btn__primary" but_name="dispatch[checkout.validate_iute]" but_text="Apliko" but_id="iute_submit"}
			</div>
		</div>
	</form>

<div>

<div id="ajax_overlay" class="ty-ajax-overlay" style="display: none;"></div>
<div id="ajax_loading_box" class="ty-ajax-loading-box" style="display: none;"></div>

<script>
	{ldelim}
		if (`{$smarty.session.auth.iutecredit.birthday}`) {
			// let sessionBirthday = ((`{$smarty.session.auth.iutecredit.birthday}`).split('.').reverse()).join('-');
			let sessionBirthday = `{$smarty.session.auth.iutecredit.birthday}`;
			$('#birthday').val(sessionBirthday);
		}

		var amount = `{$total}`;
	{rdelim}

	{literal}

		$(document).on('click', 'button#iute_submit', function(e) {
			e.preventDefault();

			let pn = $('input#personal_number').val();
			let birthday = $('input#birthday').val();
			let period = $('input#months').val();

			let validPn = pn && validatePersonalNumber(pn);
			let validTerms = $('input[name="terms_and_conditions"]:checked').length > 0;
			// let validBirthday = birthday && ((birthday.split('-')).reverse()).join('.');
			let validBirthday = birthday && validateBirthday(birthday);

			if (validPn) {
	    		$('label#personal_number_error').hide();
			} else {
				$('label#personal_number_error').show();
			}

			if (validTerms) {
				$('label#terms_and_conditions_error').hide();
			} else {
				$('label#terms_and_conditions_error').show();
			}

			if (validBirthday) {
				$('label#birthday_error').hide();
			} else {
				$('label#birthday_error').show();
			}

			if (validPn && validTerms && validBirthday) {

				$('#ajax_overlay').show();
	        	$('#ajax_loading_box').show();

				let url = fn_url('checkout.validate_iute');

		        $.ajax({
		            url: url,
		            data: {is_ajax: 1, amount: amount, period: period, personal_number: pn, valid_terms: validTerms, birthday: birthday},
		            success: function (response) {
		            	if (JSON.stringify(response.notifications).indexOf('invalid_personal_number') != -1) {
	        				$('label#personal_number_error').show();
						} else if (JSON.stringify(response.notifications).indexOf('invalid_terms') != -1) {
							$('label#terms_and_conditions_error').show();
						} else if (JSON.stringify(response.notifications).indexOf('valid_request') != -1) {
	        				$('label#personal_number_error').hide();
							$('.iute-instructions').html('<h3>Informatat e aplikimit të kredisë</h3><p>Shuma e kredisë: ' + amount + ' €</p><p>Periudha e kredisë: ' + period + ' muaj</p><p>Ditëlindja: ' + birthday + '</p><p>Numri personal: ' + pn + '</p>');
							$('#iute_success').fadeIn(200);
							$('#iute_submit').addClass('iute-submit-inactive');

							setTimeout(function() {
								let notificationContainer = $('.cm-notification-content-extended:visible');
								$.ceNotification('close', notificationContainer, false);
								$('#iute_success').fadeOut(300);
								$('#iute_submit').removeClass('iute-submit-inactive');
							}, 1000);
		            	}

		                $('#ajax_overlay').hide();
	        			$('#ajax_loading_box').hide();
		            }
		        });
			}
		});

		function validatePersonalNumber(pn) {
			if (!pn.match(/^[0-9]+$/) || pn.length != 10) {
				return false;
			}

			return true;
		}

		function validateBirthday(birthday) {
			if (birthday.match(/^\s*(3[01]|[12][0-9]|0?[1-9])\.(1[012]|0?[1-9])\.((?:19|20)\d{2})\s*$/g)) {
				return true;
			}

			return false;
		}

	{/literal}
</script>

<script src="js/lib/iutecredit/calculator.js"></script>
<script>

	var calculator = new EshopCalculator();
	var country = 'XK';
	var currency = '€';
	var apiUrl = 'http://test.iute.tripledev.ee:7101';
	var agentId = 542;
	calculator.initWithLiveAgentInformation(country, currency, apiUrl, agentId);

	var translations = {
		"no.active.agent": "Translation for agent error",
		"internal.error": "Translation for internal error",
		"service.unavailable": "Translation for service down error"
	};

</script>