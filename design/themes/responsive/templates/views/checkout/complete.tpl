<style>
	.iute-active{
		pointer-events: all !important;
		opacity: 1 !important;
	}
</style>
<div class="complete-page">
    <div class="ty-checkout-complete__order-success {if $order_info.products}span11{else}span16{/if} complete-page__left">
        {if !empty($smarty.session.anc)}
            <img style="display:none" height="1" width="1" alt=""
                 src="{$smarty.session.anc_url}{$smarty.session.anc}&order_id={$order_info.order_id}"/>
        {/if}

        {if $order_info.status|in_array:["F", "D", "B", "I"]}
            <p>{__('order_was_not_placed')}</p>
        {else}
            <p>{__("text_order_placed_successfully")}</p>
            {if $iute_order}
                <p id="iute_timer_container" style="transition:opacity .4s ease; pointer-events:none; opacity:0; color: #333; font-size: 18px;">Një agjent nga IuteCredit do të ju kontaktojë për <span id="iute_timer" style="color: #e65228;"></span></p>
                {* <div id="window_open" style="display: block; padding-top: 25px;">
                    <a href="https://iutecredit.org/application/#utm_source=gjirafa50" target="_blank" style="margin: 0 auto; text-transform: uppercase;" class="ty-btn ty-btn__primary"><b>Hap faqen e aplikimit</b></a>
                    <p style="font-size: 14px; color: #e45227;">*Nëse nuk është hapur dritarja e aplikimit të IuteCredit, ju lutem klikoni butonin.</p>
                </div> *}
            {/if}
        {/if}

        {if $order_info.payment_id == 28}
            {include file="buttons/button.tpl" but_role="text" but_text='Shtyp faturën KosGiro' but_href="orders.print_kosgiro_invoice?order_id=`$order_info.order_id`" but_meta="cm-new-window ty-btn__text" but_icon="ty-icon-print orders-print__icon"}

            {include file="buttons/button.tpl" but_role="text" but_meta="orders-print__pdf ty-btn__text cm-no-ajax" but_text='Shtyp faturën KosGiro (pdf)' but_href="orders.print_kosgiro_invoice?order_id=`$order_info.order_id`&format=pdf" but_icon="ty-icon-doc-text orders-print__icon"}
        {/if}

        {foreach $order_info.products as $product}
            {if $product.ga_category_name == 'Bonu Gamer'}
                <div class="ty-center" style="font-weight: 900;margin: 10px 0 50px 0;font-size: 15px;">
                    Tani jeni pjese e lojës shpërblyese BONU GAMER! Fituesi zgjidhet me 31 mars, me LIVE VIDEO në <a
                            href="https://www.facebook.com/Gjirafa50/" target="_blank"
                            style="font-size: 15px;text-decoration: underline;font-weight: 900;">Facebook të
                        gjirafa50.com</a>.<br/>
                    <a href="/index.php?dispatch=pages.begamer"><img
                                src="https://hhstsyoejx.gjirafa.net/gj50/landings/begamer/logo_i.png"
                                style="max-width: 200px;margin-top: 20px;" alt=""></a>
                </div>
                {break}
            {/if}
        {/foreach}

        {$refused_statuses = ['F','D','B','I','X']}
        {if $order_info.status|in_array:$refused_statuses} {assign var="refused" value="true"}{/if}
        <div class="order-steps">
            {foreach $tracking_steps as $step}
                <div class="{if $step.active}active{/if} {if $refused}cancel{/if} step {if $step.current}current{/if}">
                                            <span>
                                                <img src="images/order-stepper/{if $refused}cancel{else}{$step.status}{/if}.svg"/>
                                            </span>
                    <div class="st-text">
                        <h3>{$step.name}</h3>
                        <p>{if $step.active}{$step.status_desc}{else}{$step.status_desc_inactive}{/if}</p>
                    </div>
                </div>
            {/foreach}
        </div>

        {if $order_info && $atuh.user_id != 0}
            {if $order_info.child_ids}
                <div class="ty-center">
                    <a class="ty-btn ty-btn__primary"
                       href="{"orders.search?period=A&order_id=`$order_info.child_ids`"|fn_url}">{__("order_details")}</a>
                </div>
            {else}
                <div class="ty-center">
                    <a class="ty-btn ty-btn__primary"
                       href="{"orders.details?order_id=`$order_info.order_id`"|fn_url}">{__("order_details")}</a>
                </div>
            {/if}
        {/if}
        {if ($also_bought_products != null)}
            <div class="cart-recommended__wrapper">
                {include file="views/checkout/components/related_products.tpl"}
            </div>
        {/if}

    </div>


    {if $order_info.products}
        <div class="span5 complete-page__right">
            <div class="share-checkout-container">
                <h3>Shpërndaje!</h3>
                <div class="row-fluid">
                    <div class="column-checkout share-p-wrapper">
                        {foreach $order_info.products as $product}

                            {assign var="url" value="https://gjirafa50.com/index.php?dispatch=products.view&product_id={$product.product_id}"|escape:'url'}
                            <div class="card-product-checkout bbox share-box-item clear-fix">
                                <a href={"products.view&product_id={$product.product_id}"|fn_url} target="_blank" class="share-box-item-container">
                                    <div class="image-container">
                                        <img src="{get_images_from_blob product_id=$product.product_id count=1}"
                                             alt="Avatar">
                                    </div>
                                    <div class="">

                                        <h4><b>{$product.product|truncate:50}</b></h4>
                                        <p>
                                           Kodi i produktit: {$product.product_code}
                                        </p>

                                        <h3>
                                            {if !$is_AL}
{*                                                {$product.price}€*}
                                                {include file="common/price.tpl" value=$product.price}
                                            {/if}
                                        </h3>

                                    </div>
                                </a>
                                <a class="resp-sharing-button__link"
                                   href="https://facebook.com/sharer/sharer.php?u={$url}"
                                   target="_blank" aria-label="">
                                    <div class="resp-sharing-button resp-sharing-button--facebook resp-sharing-button--small">
                                        <div aria-hidden="true"
                                             class="resp-sharing-button__icon resp-sharing-button__icon--solid">
                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                                                <path d="M18.77 7.46H14.5v-1.9c0-.9.6-1.1 1-1.1h3V.5h-4.33C10.24.5 9.5 3.44 9.5 5.32v2.15h-3v4h3v12h5v-12h3.85l.42-4z"></path>
                                            </svg>
                                        </div>
                                    </div>
                                </a>

                                <a class="resp-sharing-button__link"
                                   href="https://twitter.com/intent/tweet/?text=Blej%20online%20TANI!&amp;url={$url}"
                                   target="_blank" aria-label="">
                                    <div class="resp-sharing-button resp-sharing-button--twitter resp-sharing-button--small">
                                        <div aria-hidden="true"
                                             class="resp-sharing-button__icon resp-sharing-button__icon--solid">
                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                                                <path d="M23.44 4.83c-.8.37-1.5.38-2.22.02.93-.56.98-.96 1.32-2.02-.88.52-1.86.9-2.9 1.1-.82-.88-2-1.43-3.3-1.43-2.5 0-4.55 2.04-4.55 4.54 0 .36.03.7.1 1.04-3.77-.2-7.12-2-9.36-4.75-.4.67-.6 1.45-.6 2.3 0 1.56.8 2.95 2 3.77-.74-.03-1.44-.23-2.05-.57v.06c0 2.2 1.56 4.03 3.64 4.44-.67.2-1.37.2-2.06.08.58 1.8 2.26 3.12 4.25 3.16C5.78 18.1 3.37 18.74 1 18.46c2 1.3 4.4 2.04 6.97 2.04 8.35 0 12.92-6.92 12.92-12.93 0-.2 0-.4-.02-.6.9-.63 1.96-1.22 2.56-2.14z"></path>
                                            </svg>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        {/foreach}
                    </div>
                </div>
            </div>
            {if $auth.user_id != 0}
                <div class="ty-checkout-complete__buttons buttons-container {if !$order_info || !$settings.Checkout.allow_create_account_after_order == "Y" || $auth.user_id} ty-mt-s{/if}">
                    {hook name="checkout:complete_button"}
                        <div class="cart-total__buttons-small clearfix">
                            <div class="span8">
                                {if $order_info}
                                    {if $order_info.child_ids}
                                        {include file="buttons/button.tpl" but_meta="ty-btn__secondary" but_text=__("order_details") but_href="orders.search?period=A&order_id=`$order_info.child_ids`"}
                                    {else}
                                        {include file="buttons/button.tpl" but_text=__("order_details") but_meta="ty-btn__secondary" but_href="orders.details?order_id=`$order_info.order_id`"}
                                    {/if}
                                {/if}
                            </div>
                            <div class="span8">
                                {include file="buttons/button.tpl" but_meta="ty-btn__secondary" but_text=__("view_orders") but_href="orders.search"}
                            </div>


                        </div>
                        <div class="cart-total__buttons-big">
                            {include file="buttons/continue_shopping.tpl" but_role="text" but_meta="ty-checkout-complete__button-vmid" but_href=$continue_url|fn_url}
                        </div>
                    {/hook}
                </div>
            {/if}
        </div>
    {/if}
</div>
{if $order_info && $settings.Checkout.allow_create_account_after_order == "Y" && !$auth.user_id}
    <div class="ty-checkout-complete__create-account">
        <h3 class="ty-subheader">{__("create_account")}</h3>
        <div class="ty-login">
            <form name="order_register_form" action="{""|fn_url}" method="post">
                <input type="hidden" name="order_id" value="{$order_info.order_id}"/>

                <div class="ty-control-group">
                    <label for="password1"
                           class="ty-control-group__label ty-login__filed-label cm-required cm-password">{__("password")}</label>
                    <input type="password" id="password1" name="user_data[password1]" size="32" maxlength="32" value=""
                           class="cm-autocomplete-off ty-login__input cm-focus"/>
                </div>

                <div class="ty-control-group">
                    <label for="password2"
                           class="ty-control-group__label ty-login__filed-label cm-required cm-password">{__("confirm_password")}</label>
                    <input type="password" id="password2" name="user_data[password2]" size="32" maxlength="32" value=""
                           class="cm-autocomplete-off ty-login__input"/>
                </div>

                <div class="buttons-container clearfix">
                    <p>{include file="buttons/button.tpl" but_name="dispatch[checkout.create_profile]" but_text=__("create")}</p>
                </div>
            </form>
        </div>
    </div>
    <div class="ty-checkout-complete__login-info">
        {hook name="checkout:payment_instruction"}
        {if $order_info.payment_method.instructions}
            <div class="ty-login-info">
                <h4 class="ty-subheader">{__("payment_instructions")}</h4>
                <div class="ty-wysiwyg-content">
                    {$order_info.payment_method.instructions nofilter}
                </div>
            </div>
        {/if}
        {/hook}
    </div>
{else}
    {* <div class="ty-checkout-complete__login-info ty-checkout-complete_width_full">
        {hook name="checkout:payment_instruction"}
            {if $order_info.payment_method.instructions}
                <h4 class="ty-subheader">{__("payment_instructions")}</h4>
                <div class="ty-wysiwyg-content">
                    <br>
                    {$order_info.payment_method.instructions nofilter}
                </div>
            {/if}
        {/hook}
    </div>
	*}
{/if}

{* place any code you wish to display on this page right after the order has been placed *}
{hook name="checkout:order_confirmation"}
{/hook}

{if $esis_url}
    <img src="{$esis_url}" width="1" height="1" >
{/if}

{*{if !$smarty.const.DEVELOPMENT && $order_info.tracked == false}*}
{if !$smarty.const.DEVELOPMENT}
    {assign "lek_val" "133"}

    {assign var="currency" value="EUR"}
    {if $order_info.company_id == 19}
        {$currency = "ALL"}
    {/if}

    {foreach $order_info.products as $product}
        <script>
            let currency = '{$currency}';

            ga('ec:addProduct', {
                'id': '{$product.product_id}',
                'name': '{$product.product}',
                'price': '{if $order_info.secondary_currency == 'ALL'}{math equation="x/y" x=$product.original_price y=$lek_val format="%.2f"}{else}{$product.original_price}{/if}',
                'quantity': {$product.amount}
            });

            gjdmp.tr("Purchase", {
                id: '{$product.product_id}',
                order_id: '{$order_info.order_id}',
                user_id: $('#store_user').val(),
                type: 'product',
                currency: currency,
                quantity: '{$product.amount}',
                value: '{$product.base_price}',
                payment: '{$order_info.payment_method.payment}',
                method: '{$order_info.shipping.0.shipping}',
                flex: '{$product.gjflex}' ? 'flex' : 'noflex',
                discount: '{$product.discount}',
                coupon: '{$coupon}'
            });
        </script>
    {/foreach}
    <script>

        fbq('trackCustom', 'Thank You Page Payment', {
            title: 'Purchase Completed',
            order_id: '{$order_info.order_id}',
            value: '{if $order_info.secondary_currency == 'ALL'}{math equation="x/y" x=$order_info.total y=$lek_val format="%.2f"}{else}{$order_info.total}{/if}',
            currency: 'EUR',
        });
    </script>
{literal}
    <script>(function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12&appId=113237396053647&autoLogAppEvents=1';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
{/literal}
{/if}
{script src="js/tygh/recommended_slider.js"}

{if $iute_order}
<script>
	var countDownDate;

	if (localStorage.getItem('iute_countdown_' + {$order_info.order_id}) === null) {
		countDownDate = new Date().getTime() + 30 * 60000;
		localStorage.setItem('iute_countdown_' + {$order_info.order_id}, countDownDate);
	} else {
		countDownDate = localStorage.getItem('iute_countdown_' + {$order_info.order_id});
	}

	var iteration = 0;

    var timer = setInterval(iuteTimer, 1000);

    function iuteTimer() {
        iteration++;

        var now = new Date().getTime();
        var distance = countDownDate - now;
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        if (distance > 0) {
            document.getElementById('iute_timer').textContent = minutes + ' minuta e ' + seconds + ' sekonda.';

            if (iteration == 1) {
                document.getElementById('iute_timer_container').classList.add('iute-active');
            }

        } else {
            clearInterval(timer);
            document.getElementById('iute_timer_container').classList.remove('iute-active');
        }
    }

    // let tab = window.open('https://iutecredit.org/application/#utm_source=gjirafa50', '_blank');

    // if (tab) {
    //     tab.focus();
    // } else {
    //     document.getElementById('window_open').style.display = 'block';
    // }

</script>
{/if}