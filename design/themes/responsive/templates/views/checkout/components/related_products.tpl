<style type="text/css">
    .slidItem > h4 {
        font-weight: normal;
        border: none;
        color: #757575;
        padding: 0 0 5px 0;
    }

    .checkout-also-bought-price h3 {
        margin: 0 0 3px 0;
    }
</style>

<div class="checkout-also-bought-wrapper">
    <h4 class="">Blerësit gjithashtu blenë</h4>
    <div class="row-fluid">
        <div class="sliderWrapper">
            <span class="left">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 477.175 477.175"><path
                            d="M145.188 238.575l215.5-215.5c5.3-5.3 5.3-13.8 0-19.1s-13.8-5.3-19.1 0l-225.1 225.1c-5.3 5.3-5.3 13.8 0 19.1l225.1 225c2.6 2.6 6.1 4 9.5 4s6.9-1.3 9.5-4c5.3-5.3 5.3-13.8 0-19.1l-215.4-215.5z"/></svg>
            </span>
            <span class="right">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 477.175 477.175"><path
                            d="M360.731 229.075l-225.1-225.1c-5.3-5.3-13.8-5.3-19.1 0s-5.3 13.8 0 19.1l215.5 215.5-215.5 215.5c-5.3 5.3-5.3 13.8 0 19.1 2.6 2.6 6.1 4 9.5 4 3.4 0 6.9-1.3 9.5-4l225.1-225.1c5.3-5.2 5.3-13.8.1-19z"/></svg>
            </span>
            <div class="innerWrapper">
                {assign "is_al" $a|fn_isAL}
                {foreach $also_bought_products as $product}
                    <div class="slidItem">
                    <span class="imgHolder">
                        <a href="{"products.view&product_id={$product.product_id}"|fn_url}"><img src="{get_images_from_blob product_id=$product.product_id count=1}"></a>
                    </span>
                        <h4 style="white-space: pre-wrap;"><a href="{"products.view&product_id={$product.product_id}"|fn_url}">{$product.product}</a></h4>
                        <div class="social">
                            <div class="checkout-also-bought-price">
                                <h3>
                                    {if $is_al}
{*                                        {$product.price_al|string_format:"%d"} Lekë *}
                                        {include file="common/price.tpl" value=$product.price_al}
                                    {else}
{*                                        {$product.price} &euro;*}
                                        {include file="common/price.tpl" value=$product.price}
                                    {/if}
                                </h3>
                                <div class="cart-recommended__button">
                                    <form action="{""|fn_url}" method="post" class="cm-ajax">
                                        <input type="hidden" name="result_ids" value="cart_status*,wish_list*,checkout*,account_info*"/>
                                        <input type="hidden" name="redirect_url" value="{$redirect_url|default:$config.current_url}"/>
                                        <input type="hidden" name="product_data[{$product.product_id}][product_id]" value="{$product.product_id}"/>

                                        <button class="ty-btn__primary ty-btn__big ty-btn__add-to-cart ty-btn" type="submit" name="dispatch[checkout.add..{$product.product_id}]">Shto në shportë
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                {/foreach}
            </div>
        </div>
    </div>
</div>