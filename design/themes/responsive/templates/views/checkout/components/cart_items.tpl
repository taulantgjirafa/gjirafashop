{capture name="cartbox"}
    {if $runtime.mode == "checkout"}
        {if $cart.coupons|floatval}<input type="hidden" name="c_id" value=""/>{/if}
        {hook name="checkout:form_data"}
        {/hook}
    {/if}
    <div id="cart_items">
        <table class="ty-cart-content ty-table">
            {assign var="prods" value=false}
            <thead>
            <tr>
                <th class="ty-cart-content__title ty-left">{__("product")}</th>
                <th class="ty-cart-content__title ty-left">&nbsp;</th>
                <th class="ty-cart-content__title ty-right">{__("unit_price")}</th>
                <th class="ty-cart-content__title quantity-cell">{__("quantity")}</th>
                <th class="ty-cart-content__title ty-right">{__("total_price")}</th>
                <th>
                    <div class="ty-cart-remove">
                        <a href="{"checkout.clear_cart"|fn_url}" class="ty-twishlist-item__remove ty-remove cm-tooltip"
                           title="Fshij Shportën">
                            <i style="top: 0; color:#e45830" class="ty-remove__icon ty-icon-cancel"></i>
                            <span class="ty-twishlist-item__txt ty-remove-txt">Fshij Shportën</span>
                        </a>
                    </div>
                </th>
            </tr>
            </thead>
            <tbody>
            {if $cart_products}
                {assign var="show_flex_first" value=0}
                {foreach from=$cart_products item="product" key="key" name="cart_products"}
                    {if !$product.hidden}
                    <input type="hidden" name="cart_products[{$key}][bundle_id]" value="{$product.bundle_id}">
                    <input type="hidden" name="cart_products[{$key}][bundle_parent]" value="{$product.bundle_parent}">
                    {if $product.list_price && $product.list_price > $product.price}
                        {$product.calculated_price = $product.list_price}
                    {else}
                        {$product.calculated_price = $product.base_price}
                    {/if}

                    {assign var="obj_id" value=$product.object_id|default:$key}
                    {hook name="checkout:items_list"}
                    {if !$cart.products.$key.extra.parent}
                        {assign var="gjflex" value=$product.base_price|fn_calculate_gjflex}

{*                        {if $product.bundle_parent}*}
{*                            {assign var="bundle_gjflex" value=0}*}

{*                            {foreach from=$product.bundle_data item=bundle_item}*}
{*                                {$bundle_gjflex = $bundle_gjflex + $bundle_item.base_price|fn_calculate_gjflex}*}
{*                            {/foreach}*}

{*                            {assign var="gjflex" value=$bundle_gjflex}*}
{*                        {else}*}
{*                            {assign var="gjflex" value=$product.base_price|fn_calculate_gjflex}*}
{*                        {/if}*}

                        <tr class="cart-item">
                            <td class="ty-cart-content__product-elem ty-cart-content__image-block">
                                {if $runtime.mode == "cart" || $show_images}
                                    <div class="ty-cart-content__image ty-center cm-reload-{$obj_id}"
                                         id="product_image_update_{$obj_id}">
                                        {hook name="checkout:product_icon"}
                                            <a href="{"products.view?product_id=`$product.product_id`"|fn_url}">
                                                {*{assign var="container" value=intval($product.product_id/8000+1)}*}
                                                {*{if $container gt 5}*}
                                                    {*{$container=5}*}
                                                {*{/if}*}
                                                {assign var="azure_path" value={get_images_from_blob product_id=$product.product_id count=1}}
                                                {$product.main_pair =["detailed" => ["object_type" => "product", "image_path" => $azure_path , "alt" => ""]]}
                                                {include file="common/image.tpl" obj_id=$key images=$product.main_pair image_width=$settings.Thumbnails.product_cart_thumbnail_width image_height=$settings.Thumbnails.product_cart_thumbnail_height}</a>
                                        {/hook}
                                        <!--product_image_update_{$obj_id}--></div>
                                {/if}
                            </td>
                            <td class="ty-cart-content__product-elem ty-cart-content__description" style="width: 50%;">
                                {strip}
                                    <a href="{"products.view?product_id=`$product.product_id`"|fn_url}"
                                       class="ty-cart-content__product-title">
                                        {$product.product nofilter}
                                    </a>
                                    {*{if !$product.exclude_from_calculate}*}
                                    {*<a class="{$ajax_class} ty-cart-content__product-delete ty-delete-big" href="{"checkout.delete?cart_id=`$key`&redirect_mode=`$runtime.mode`"|fn_url}" data-ca-target-id="cart_items,checkout_totals,cart_status*,checkout_steps,checkout_cart" title="{__("remove")}">&nbsp;<i class="ty-delete-big__icon ty-icon-cancel-circle"></i>*}
                                    {*</a>*}
                                    {*{/if}*}
                                {/strip}
                                <div class="ty-cart-content__sku ty-sku cm-hidden-wrapper{if !$product.product_code} hidden{/if}"
                                     id="sku_{$key}">
                                    {__("sku")}: <span class="cm-reload-{$obj_id}"
                                                       id="product_code_update_{$obj_id}">{$product.product_code}<!--product_code_update_{$obj_id}--></span>
                                </div>
{*                                {if $product.avail_since != 0 && ($product.avail_since > $smarty.const.TIME)}*}
{*                                    <div class="ty-cart-content__avail-since">{__('product_coming_soon_date')} <span>{$product.avail_since|date_format}</span></div>*}
{*                                {/if}*}
                                {if $product.product_options}
                                    <div class="cm-reload-{$obj_id} ty-cart-content__options"
                                         id="options_update_{$obj_id}">
                                        {include file="views/products/components/product_options.tpl" product_options=$product.product_options product=$product name="cart_products" id=$key location="cart" disable_ids=$disable_ids form_name="checkout_form"}
                                        <!--options_update_{$obj_id}--></div>
                                {/if}
                                <input type="hidden" class="product_id" value="{$product.product_id}">

                                {assign var="disable_flex_categories" value=','|explode:{__('disable_flex')}}
                                {assign var="currency_symbol" value=fn_get_company_currency_symbol()}
                                {assign var="check_category" value=$disable_flex_categories|array_intersect:$cart.products.$key.category_ids}

                                {nocache}
                                <div class="{if $product.is_edp != 'Y'}with_gjflex{/if} {if count($check_category) > 0 || $product.license_key_product} active {/if} {if $show_flex_first != 0}hiddenct{/if} {if $cart.products.$key.gjflex !== null}active{/if}"
                                     {if $cart.products.$key.gjflex === null}data-select="null"{/if}>

                                    {if count($check_category) > 0 || $product.is_edp == 'Y' || $product.license_key_product}
                                    <p>GjirafaFLEX nuk është e disponueshme për këtë produkt.</p>
                                    <div class="content">
                                        <p><a href="https://m.me/Gjirafa50">Kontakto për më shumë informata.</a></p>
                                    </div>
                                    {else}
                                    <div class="cart__gjflex-option gjflex-cart-item">
                                        <div class="gjflex-option active">
                                            <img src="images/icons/flex.png" alt="GjirafaFLEX">
                                            <div class="content">
                                                <strong class="ty-hand"
                                                        onclick="{literal}$(this).next().slideToggle();{/literal}"
                                                        style="display:block;margin: 0 0 8px 0;">Rri shlirë me
                                                    GjirafaFlex
                                                    <i class="ty-icon-help-circle"></i>: </strong>
                                                <div {if $show_flex_first}style="display:none;"{/if}>
                                                    <p>
{*                                                        GjirafaFLEX është shërbim shtesë që ne e ofrojmë si zgjidhje të*}
{*                                                        shpejtë*}
{*                                                        nëse keni defekt me produktin brenda periudhës 1 vjeçare të*}
{*                                                        garancionit.*}
{*                                                        Do të thotë, ose merrni produkt të ri ose merrni kredit në*}
{*                                                        Gjirafa50 për*}
{*                                                        vlerën e produktit. Pa pasur nevojë për pritje tek serviset e*}
{*                                                        autorizuara, GjirafaFLEX ju mundëson zgjidhje aty për aty.*}
                                                        {__('gjflex_description')}
                                                        <br>
                                                    </p>
                                                    <p class="text-muted">
{*                                                        *Defektet përfshijnë vetëm ato fabrike, dhe jo*}
{*                                                        dëmet*}
{*                                                        fizike që mund të i shkaktohen produktit.*}
                                                        {__('gjflex_disclaimer')}
                                                    </p>
                                                </div>
                                                <label for="gjflexyes_{$key}">
                                                    <input type="radio"
                                                           onclick="$('#button_cart').click();$('#ajax_overlay, #ajax_loading_box').show();"
                                                           name="cart_products[{$key}][gjflex]" id="gjflexyes_{$key}"
                                                           value="1"
                                                           {if $cart.products.$key.gjflex === true}checked="checked"{/if}><strong>Po
                                                        {assign "isAL" "{fn_isAL()}"}
                                                        {if !$isAL}
{*                                                                (+{(($gjflex * $cart.products.$key.amount) + $product.additional_gjflex)|number_format:2:".":","}*}
                                                            (+
                                                            {include file="common/price.tpl" value=(($gjflex * $cart.products.$key.amount) + $product.additional_gjflex)}
                                                            )
                                                        {else}
{*                                                                (+{(($gjflex * $cart.products.$key.amount) + $product.additional_gjflex)|number_format:0:".":","}*}
                                                            (+
                                                            {include file="common/price.tpl" value=(($gjflex * $cart.products.$key.amount) + $product.additional_gjflex)}
                                                            )
                                                        {/if}
{*                                                        {$currency_symbol})*}
                                                    </strong>
                                                </label>
                                                <label for="gjflexno_{$key}" style="margin-left:15px;">
                                                    <input type="radio" class="gjflexno"
                                                           onclick="$('#button_cart').click();$('#ajax_overlay, #ajax_loading_box').show();"
                                                           name="cart_products[{$key}][gjflex]" id="gjflexno_{$key}"
                                                           value="0"
                                                           {if $cart.products.$key.gjflex === false}checked="checked"{/if}>
                                                    <strong>Jo</strong>
                                                </label>

                                            </div>
                                        </div>
                                        {*<strong class="find-gjFlex" style="display:block;margin: 0 0 8px 0;">GjirafaFLEX*}
                                        {*<i class="ty-icon-help-circle"></i>: </strong>*}

                                        {*<label for="gjflexno_{$key}">*}
                                        {*<input type="radio" class="gjflexno"*}
                                        {*onclick="$('#button_cart').click();$('#ajax_overlay, #ajax_loading_box').show();"*}
                                        {*name="cart_products[{$key}][gjflex]" id="gjflexno_{$key}" value="0"*}
                                        {*{if $cart.products.$key.gjflex === false}checked="checked"{/if}>*}
                                        {*<strong>Jo</strong>*}
                                        {*</label>*}
                                        {/if}
                                    </div>
                                </div>
                                {/nocache}

                                {assign var="name" value="product_options_$key"}
                                {capture name=$name}
                                    {capture name="product_info_update"}
                                        {hook name="checkout:product_info"}
                                        {if $product.exclude_from_calculate}
                                            <strong><span class="price">{__("free")}</span></strong>
                                        {elseif $product.discount|floatval || ($product.taxes && $settings.General.tax_calculation != "subtotal")}
                                            {if $product.discount|floatval}
                                                {assign var="price_info_title" value=__("discount")}
                                            {else}
                                                {assign var="price_info_title" value=__("taxes")}
                                            {/if}
                                            <p><a data-ca-target-id="discount_{$key}"
                                                  class="cm-dialog-opener cm-dialog-auto-size"
                                                  rel="nofollow">{$price_info_title}</a></p>
                                            <div class="ty-group-block hidden" id="discount_{$key}"
                                                 title="{$price_info_title}">
                                                <table class="ty-cart-content__more-info ty-table" style="border: none;">
                                                    <thead>
                                                    <tr>
                                                        <th style="padding-top: 12px;" class="ty-cart-content__more-info-title">{__("price")}</th>
                                                        <th style="padding-top: 12px;" class="ty-cart-content__more-info-title">{__("quantity")}</th>
                                                        {if $product.discount|floatval}
                                                            <th style="padding-top: 12px;" class="ty-cart-content__more-info-title">{__("discount")}</th>{/if}
                                                        {if $product.taxes && $settings.General.tax_calculation != "subtotal"}
                                                            <th style="padding-top: 12px;">{__("tax")}</th>{/if}
                                                        <th style="padding-top: 12px;" class="ty-cart-content__more-info-title">{__("subtotal")}</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr style="border: none;">
                                                        <td>{include file="common/price.tpl" value=$product.calculated_price span_id="original_price_`$key`" class="none"}</td>
                                                        <td class="ty-center">{$product.amount}</td>
                                                        {if $product.discount|floatval}
                                                            <td>{include file="common/price.tpl" value=$product.discount span_id="discount_subtotal_`$key`" class="none"}</td>{/if}
                                                        {if $product.taxes && $settings.General.tax_calculation != "subtotal"}
                                                            <td>{include file="common/price.tpl" value=$product.tax_summary.total span_id="tax_subtotal_`$key`" class="none"}</td>{/if}
                                                        <td>{include file="common/price.tpl" span_id="product_subtotal_2_`$key`" value=$product.price class="none"}</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        {/if}
                                            {include file="views/companies/components/product_company_data.tpl" company_name=$product.company_name company_id=$product.company_id}
                                        {/hook}
                                    {/capture}

                                    {if $smarty.capture.product_info_update|trim}
                                        <div class="cm-reload-{$obj_id}" id="product_info_update_{$obj_id}">
                                            {$smarty.capture.product_info_update nofilter}
                                            <!--product_info_update_{$obj_id}--></div>
                                    {/if}
                                {/capture}

                                {if $smarty.capture.$name|trim}
                                    <div id="options_{$key}" class="ty-product-options ty-group-block">
                                        <div class="ty-group-block__arrow">
                                            <span class="ty-caret-info"><span class="ty-caret-outer"></span><span
                                                        class="ty-caret-inner"></span></span>
                                        </div>
                                        {* {$smarty.capture.$name nofilter} *}
                                        {if $product.discount|floatval}
                                        <div style="display: flex; justify-content: space-between; padding-bottom: 7px;">
                                            <span>{__("discount")}:</span>
                                            <span style="color: #e65228; font-weight: bold;">
                                                -{include file="common/price.tpl" value=$product.discount span_id="discount_subtotal_`$key`" class="none"}
                                            </span>
                                        </div>
                                        {/if}
                                        {if $product.taxes && $settings.General.tax_calculation != "subtotal"}
                                        <div style="display: flex; justify-content: space-between; padding-bottom: 7px;">
                                            <span>{__("tax")}:</span>
                                            <span style="font-weight: bold;">
                                                {include file="common/price.tpl" value=$product.tax_summary.total span_id="tax_subtotal_`$key`" class="none"}
                                            </span>
                                        </div>
                                        {/if}
                                        <div style="display: flex; justify-content: space-between;">
                                            <span>{__("subtotal")}:</span>
                                            <span style="font-weight: bold;">
                                                {include file="common/price.tpl" span_id="product_subtotal_2_`$key`" value=$product.price class="none"}
                                            </span>
                                        </div>
                                    </div>
                                {/if}
                            </td>

                            <td class="ty-cart-content__product-elem ty-cart-content__price cm-reload-{$obj_id}  mobile-dn"
                                id="price_display_update_{$obj_id}">

                                {include file="common/price.tpl" value=$product.calculated_price span_id="product_price_`$key`" class="ty-sub-price"}
                                <!--price_display_update_{$obj_id}-->
                                {if $cart.products.$key.gjflex}
                                    <div>+</div>
                                    <div>
                                        <strong>GjirafaFLEX</strong>
                                        {include file="common/price.tpl" value=($gjflex * $cart.products.$key.amount) + $product.additional_gjflex span_id="product_price_`$key`" class="ty-sub-price"}
                                    </div>
                                {/if}
                            </td>
                            {$rbko_products = [42931, 37983, 35717, 30180, 27028, 26575, 24556, 23259, 22977, 22861, 20244, 19902, 19410, 17980, 16394, 14296, 14001, 12869, 11654, 11617, 10612, 9954, 8588, 8430, 3412, 2310, 1653, 673, 41581, 43409, 43573, 44113, 44195, 44211, 44756, 60398, 58318, 59397, 60335, 60333, 57752, 60300, 57638, 49479, 60266, 50246, 50278, 52370, 54249, 58651, 56132, 55737, 61117, 63690, 64982, 65059, 70540, 71621, 72393, 72580, 77169, 73778, 74612, 74910, 75050, 75135, 75288, 75591, 76424, 76447, 76534, 77003, 77142, 78062, 78491, 78503, 78748, 81761, 79277, 79353, 79390, 83748, 80105, 80143, 80406, 80586, 80741, 81647, 81656, 86090, 83479, 83629, 83630, 84157, 85123, 85312, 85854, 86618, 86620, 86797, 86905, 86920, 86947, 87014, 87015]}

                            <td {if $cart.products.$key.product_id|in_array:$rbko_products}style="opacity: .5; pointer-events: none; cursor: default" {/if}
                                class="ty-cart-content__product-elem ty-cart-content__qty {if $product.is_edp == "Y" || $product.exclude_from_calculate} quantity-disabled{/if}">
                                {if $use_ajax == true && $cart.amount != 1}
                                    {assign var="ajax_class" value="cm-ajax"}
                                {/if}

                                <div class="quantity cm-reload-{$obj_id}{if $settings.Appearance.quantity_changer == "Y"} changer{/if}"
                                     id="quantity_update_{$obj_id}">
                                    <input type="hidden" name="cart_products[{$key}][product_id]"
                                           value="{$product.product_id}"/>
                                    {if $product.exclude_from_calculate}<input type="hidden"
                                                                               name="cart_products[{$key}][extra][exclude_from_calculate]"
                                                                               value="{$product.exclude_from_calculate}" />{/if}
                                    <label for="amount_{$key}"></label>
                                    {if $product.is_edp == "Y" || $product.exclude_from_calculate}
                                        {$product.amount}
                                        <input type="hidden" name="cart_products[{$key}][amount]" value="{$product.amount}" />
                                    {else}
                                        {if $settings.Appearance.quantity_changer == "Y"}
                                            <div class="ty-center ty-value-changer cm-value-changer">
                                            <a class="cm-decrease ty-value-changer__decrease">&minus;</a>
                                        {/if}
                                        <input type="text" size="3" id="amount_{$key}"
                                               name="cart_products[{$key}][amount]" value="{$product.amount}"
                                               data-ca-min-qty="1"
                                               class="ty-value-changer__input cm-amount"{if $product.qty_step > 1} data-ca-step="{$product.qty_step}"{/if} />
                                        {if $settings.Appearance.quantity_changer == "Y"}
                                            <a class="cm-increase ty-value-changer__increase">&#43;</a>
                                            </div>
                                        {/if}
                                    {/if}
                                    {if $product.is_edp == "Y" || $product.exclude_from_calculate}
                                        <input type="hidden" name="cart_products[{$key}][amount]"
                                               value="{$product.amount}"/>
                                    {/if}
                                    {if $product.is_edp == "Y"}
                                        <input type="hidden" name="cart_products[{$key}][is_edp]" value="Y"/>
                                    {/if}
                                    <!--quantity_update_{$obj_id}--></div>
                            </td>
                            <td class="ty-cart-content__product-elem ty-cart-content__price cm-reload-{$obj_id}"
                                id="price_subtotal_update_{$obj_id}">
                                <span style="font-size: 13px;    font-weight: bold; margin-right: 5px; display: none;"
                                      class="mobile-dib"> Çmimi total:</span>
                                {include file="common/price.tpl" value=$product.display_subtotal span_id="product_subtotal_`$key`" class="price"}
                                {if $product.zero_price_action == "A"}
                                    <input type="hidden" name="cart_products[{$key}][price]"
                                           value="{$product.base_price}"/>
                                {/if}
                                <!--price_subtotal_update_{$obj_id}--></td>
                            <td class="ty-cart-remove">
                                <div class="ty-table__responsive-content">
                                    <a href="{"checkout.remove?product_id=`$product.product_id`&redirect_mode=cart"|fn_url}"
                                       class="ty-twishlist-item__remove cm-tooltip ty-remove" title="Largo">
                                        <i class="ty-remove__icon ty-icon-cancel"></i>
                                        <span class="ty-twishlist-item__txt ty-remove-txt">{__("remove")}</span>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        {*{if $recommendations[$product.product_code]['related_code'] != null}*}
                        {*<tr>*}
                        {*<td colspan="6" style="background-color: white;">*}

                        {*</td>*}
                        {*</tr>*}
                        {*{/if}*}
                    {/if}
                    {$show_flex_first = 1}
                    {/hook}

                    {if $product.bundle_data != null}
                        <tr style="padding: 0;">
                            <td colspan="6" style="padding: 0;">
                                <div class="ty-cart-bundle">
                                    <div class="ty-cart-bundle__title">
                                        <p>Produktet e përfshira me <b>{$product.product}</b></p>
                                    </div>
                                    <div class="ty-cart-bundle__content">
                                        {foreach from=$product.bundle_data item=bundle_data}
                                            <div class="ty-cart-bundle__product">
                                                <div class="d-flex">
                                                    {if $bundle_data.bundle_amount}
                                                        <p class="pr-2">{$bundle_data.bundle_amount} × </p>
                                                    {else}
                                                        <p class="pr-2">{$product.amount * $bundle_data.bundle_item_amount} × </p>
                                                    {/if}
                                                    <p><a href="{"products.view&product_id=`$bundle_data.product_id`"|fn_url}">{$bundle_data.product}</a></p>
                                                </div>
                                                <div class="d-flex">
                                                    <p>{$bundle_data.price} {if $al|fn_isAL}Lekë{else}€{/if}</p>
                                                </div>
                                            </div>
                                        {/foreach}
                                    </div>
                                </div>
                            </td>
                        </tr>
                    {/if}

                    {/if}
                {/foreach}
            {/if}

            {hook name="checkout:extra_list"}
            {/hook}
            </tbody>
        </table>
        <!--cart_items--></div>
{/capture}
{include file="common/mainbox_cart.tpl" title=__("cart_items") content=$smarty.capture.cartbox}


<script>
    {*$(document).on('click', '.find-gjFlex', function () {*}
    {*$('html, body').animate({scrollTop: $('.gjflex-option').offset().top - 60}, 1000);*}
    {*setTimeout(function () {*}
    {*$('.gjflex-option').removeClass('highlight');*}
    {*}, 1000)*}
    {*});*}
</script>