{assign var="ajax_form" value="cm-ajax"}

{include file="views/profiles/components/profiles_scripts.tpl"}

{assign var="delivery_time_o" value=" 5 deri 8 ditë"}
{if !$store|fn_isAL}
    {$delivery_dates = 0|fn_get_order_delivery_dates:$cart.products:$cart.user_data:$cart_products}
    {foreach from=$cart.products item="product"}
        {if $settings.General.display_delivery == 'Y' && !$al|fn_isAL}
            {if !$delivery_dates}
                {$delivery_time_o = ", data e dërgesës: E papërcaktuar"}
            {else}
                {$delivery_time_o = ", data e dërgesës: `$delivery_dates.standard|date_format:$settings.Appearance.date_format|fn_custom_date_format` - `$delivery_dates.standard_range|date_format:$settings.Appearance.date_format`"}
            {/if}
        {else}
            {if $product.amount gt $product.stock_amount}
                {if $product.amount gt $product.czc_stock}
                    {$delivery_time_o = " 8 deri 12 ditë"}
                {/if}
            {/if}
        {/if}
    {/foreach}
{/if}

<div class="checkout-steps cm-save-fields clearfix" id="checkout_steps">
    {$number_of_step = 0}
    {if $display_steps.step_one || $cart.edit_step == "step_one"}
        {$edit = $cart.edit_step == "step_one"}
        {$number_of_step = $number_of_step + 1}
        {include file="views/checkout/components/steps/step_one.tpl" step="one" complete=$completed_steps.step_one edit=$edit but_text=__("continue")}
    {/if}

    {if $display_steps.step_two}
        {$edit = $cart.edit_step == "step_two"}
        {$number_of_step = $number_of_step + 1}
        {include file="views/checkout/components/steps/step_two.tpl" step="two" complete=$completed_steps.step_two edit=$edit but_text=__("continue")}
    {/if}

    {if $display_steps.step_three || $cart.edit_step == "step_three"}
        {$edit = $cart.edit_step == "step_three"}
        {$number_of_step = $number_of_step + 1}
        {include file="views/checkout/components/steps/step_three.tpl" step="three" complete=$completed_steps.step_three edit=$edit but_text=__("continue")}
    {/if}

    {if $display_steps.step_four || $cart.edit_step == "step_four"}
        {$edit = $cart.edit_step == "step_four"}
        {$number_of_step = $number_of_step + 1}
        {include file="views/checkout/components/steps/step_four.tpl" step="four" edit=$edit complete=$completed_steps.step_four}
    {/if}

<script>
    var steps = {
        step_one: 'llogaria',
        step_two: 'faturimi-dhe-transporti',
        step_three: 'menyrat-e-transportit',
        step_four: 'opsionet-e-faturimit',
    };

    var steps_tracking = {
        step_one: 'account',
        step_two: 'address',
        step_three: 'transport',
        step_four: 'payment',
    };

    if (!(/*@cc_on!@*/false || !!document.documentMode)) {
        window.location.hash = steps[`{$smarty.session.edit_step}`];
    }

    gjdmp.tr("Checkout", {
        step: steps_tracking[`{$smarty.session.edit_step}`],
        id: 0,
        user_id: $('#store_user').val(),
    });
</script>

<!--checkout_steps--></div>