<style>
    .checkout-notification {
        background: #fcfcfc;
        padding: 10px 14px;
        border: 2px solid #acacac;
        border-radius: 5px;
        margin-bottom: 15px;
    }
</style>

{assign var="result_ids" value="cart_items,checkout_totals,checkout_steps,cart_status*,checkout_cart"}
<div class="span11 cart-left">
    <form name="checkout_form" class="cm-check-changes " action="{""|fn_url}" method="post"
          enctype="multipart/form-data">
        <input type="hidden" name="redirect_mode" value="cart"/>
        <input type="hidden" name="result_ids" value="{$result_ids}"/>

        <h1 class="ty-mainbox-title">{__("cart_contents")}</h1>

        {if $settings.General.display_delivery == 'Y' && $pdd.pdd && !$al|fn_isAL}
            <div class="checkout-notification">
                <p>
                    {if $pdd.pdd|count eq 1}Produkti{else}Produktet{/if}
                    {foreach from=$pdd.pdd item='spdd'}
                        <b>{$spdd}{if $pdd.pdd|count neq 1}, {/if}</b>
                    {/foreach}
                    {if $pdd.pdd|count eq 1}ka{else}kanë{/if} kohë të liferimit 3 ditë më të gjatë se produktet tjera në shportë. Nëse dëshironi që produktet të ju mbërrijnë më herët, ju këshillojmë që të bëni porosi të veçantë për produktet më poshtë:
                    {*            {if $pdd.pndd|count eq 1}produkti{else}produktet{/if}*}
                    {*            {foreach from=$pdd.pndd item='spdd'}*}
                    {*            <b>{$spdd}{if $pdd.pndd|count neq 1}, {/if}</b>{/foreach}. Nëse dëshironi që {if $pdd.pndd|count eq 1}produkti{else}produktet{/if}*}
                    {*            {foreach from=$pdd.pndd item='spdd'}*}
                    {*                <b>{$spdd}{if $pdd.pndd|count neq 1}, {/if}</b>*}
                    {*            {/foreach}*}
                    {*            të ju mbërrijnë më herët, ju këshillojmë që të bëni porosi të veçantë për {if $pdd.pdd|count eq 1}produktin{else}produktet{/if}*}
                    {*            {foreach from=$pdd.pdd item='spdd' key='key'}*}
                    {*            <b>{$spdd}{if $pdd.pdd|count neq 1 && $key < ($pdd.pdd|count - 1)}, {/if}</b>*}
                    {*            {/foreach}.*}
                </p>
                {foreach from=$pdd.pdd item='spdd' key='key'}
                    <div class="flexing align-items-center" style="border-top: 1px solid #eee;padding-top: 5px;margin-top: 5px;">
                        <img src="https://hhstsyoejx.gjirafa.net/gj50/img/{$key}/thumb/0.jpg" style="max-width: 40px; margin-right: 10px;">
                        <p style="font-weight: 600;">{$spdd}</p>
                    </div>
                {/foreach}
            </div>
        {/if}

        <div class="buttons-container ty-cart-content__top-buttons clearfix" style="display: none !important;">
            <div class="ty-float-left ty-cart-content__left-buttons">

                {include file="buttons/continue_shopping.tpl" but_href="/index.php" }
                {include file="buttons/clear_cart.tpl" but_href="checkout.clear" but_role="text" but_meta="cm-confirm ty-cart-content__clear-button"}
            </div>
            <div class="ty-float-right ty-cart-content__right-buttons">
                {include file="buttons/update_cart.tpl" but_id="button_cart" but_name="dispatch[checkout.update]"}
                {if $payment_methods}
                    {assign var="m_name" value="checkout"}
                    {assign var="link_href" value="checkout.checkout"}
                    {include file="buttons/proceed_to_checkout.tpl" but_href=$link_href but_meta="__checkflex_proceed"}
                {/if}
            </div>
        </div>


        {include file="views/checkout/components/cart_items.tpl" disable_ids="button_cart"}

    </form>
    <div class="cart-recommended__wrapper" style="display:none;">
{*        {if $recommendations|@count gt 0}*}
        {if !empty($recommendations)}
            {include file="views/products/components/recommended.tpl" recommendations=$recommendations is_cart =true}
        {/if}
    </div>
{*        {if ($also_bought_products != null)}*}
        {if !empty($also_bought_products)}
            <div class="cart-recommended__wrapper">
                {include file="views/checkout/components/related_products.tpl"}
            </div>
        {/if}
</div>

<div class="span5 cart-right">

    <div class="cart__gjflex-option">
        <div class="gjflex-option active">
            <img src="images/icons/flex.png" alt="GjirafaFLEX">
            <div class="content">
                <p style="font-weight: bold" class="ty-hand">Rri shlirë
                    me GjirafaFlex
                    {*<i class="ty-icon-help-circle"></i>*}
                </p>
                <p>
{*                    GjirafaFLEX është shërbim shtesë që ne e ofrojmë si zgjidhje të shpejtë*}
{*                    nëse keni defekt me produktin brenda periudhës 1 vjeçare të garancionit.*}
{*                    Do të thotë, ose merrni produkt të ri ose merrni kredit në Gjirafa50 për*}
{*                    vlerën e produktit. Pa pasur nevojë për pritje tek serviset e*}
{*                    autorizuara, GjirafaFLEX ju mundëson zgjidhje aty për aty.*}
                    {__('gjflex_description')}
                    <br>
                </p>
                <p class="text-muted">
{*                    *Defektet përfshijnë vetëm ato fabrike, dhe jo dëmet*}
{*                    fizike që mund të i shkaktohen produktit.*}
                    {__('gjflex_disclaimer')}
                </p>
            </div>


        </div>
    </div>
    <div class="cart__payment-methods">
        <p>Mundësitë e pagesave</p>
        {if $al|fn_isAL}
            <img src="images/llogot_pagese_al.png" alt="">
        {else}
            <img src="images/llogot_pagese.png" alt="">
        {/if}
    </div>
    <div class="cart-total__box">
        {include file="views/checkout/components/checkout_totals.tpl" location="cart"}

        <div class="buttons-container ty-cart-content__bottom-buttons clearfix">
            <div class="cart-total__buttons-small clearfix">
                <div class="span8">
                    {include file="buttons/continue_shopping.tpl" but_href="/index.php" }
                </div>
                <div class="span8">
                    {include file="buttons/update_cart.tpl" but_external_click_id="button_cart" but_meta="cm-external-click"}
                </div>


            </div>
            {if $auth.user_id}
                <div class="cart-total__buttons-big">
{*                    {if $payment_methods}*}
                        {assign var="m_name" value="checkout"}

                        {assign var="link_href" value="checkout.checkout"}
                        {include file="buttons/proceed_to_checkout.tpl" but_href=$link_href but_meta="__checkflex_proceed"}
{*                    {/if}*}
                </div>
            {else}

                <div class="cart-total__buttons-big">
                    <a onclick="sso.init();" class="ty-btn ty-btn__primary __checkflex_proceed ">Vazhdo te pagesa</a>
                </div>

            {/if}
            {if {__("before_order_message")} != ''}
                {*            <div style="margin-top: 10px;">*}
                {*                    <p class="before_order_message">{__("before_order_message")}</p>*}
                {*            </div>*}
            {/if}
        </div>
    </div>
</div>
<style>
    .help-checkout{
        position: fixed;
        background: #fff;
        z-index: 1;
        padding: 15px;
        box-sizing: border-box;
        border-bottom-left-radius: 14px;
        border-top-left-radius: 14px;
        box-shadow: 0 2px 5px -1px rgba(0,0,0,0.3);
        right: 0;
        top: 40%;
        max-width:200px;
        transition:transform .4s ease, opacity .4s ease;
        transform:translate3d(210px,0,0);
        opacity:0;
        pointer-events:none;
    }
    .help-checkout.active{
        opacity:1;
        transform:translate3d(0,0,0);
        pointer-events:all;
    }
    .help-checkout .float-right{
        float:right;
    }
    .help-checkout .float-right a,span.cancel-help{
        padding: 3px 13px;
        font-weight: bold;
        color: #888;
        display: inline-block;
        text-decoration:none;
        cursor:pointer;
    }
    .help-checkout .float-right a:hover, span.cancel-help:hover{
        background:#f9f9f9;
    }

</style>
<div class="help-checkout">
    <h3>A keni nevojë për ndihmë rreth procesit të blerjes?</h3>
    <div class="float-right">
        <span class="cancel-help">ANULO</span>
        <a class="goto-help" style="color:#e65228;">PO</a>
    </div>
</div>
<div class="product_howto-video">
    <div class="product_howto-video-inner">
        <div class="product_howto-ratio"></div>
        <div class="product_howto-close">
            <div class="close-x"></div>
            <div class="close-y"></div>
        </div>
    </div>
</div>

<script>
    var howToOpen = false;

    $('.goto-help').click(function () {
        document.querySelector('.help-checkout').classList.remove('active');
        $('.product_howto-video').addClass('show');
        $('.product_howto-video-inner').append('<iframe id="howto-iframe" src="https://video.gjirafa.com/embed/si-te-porosisim-permes-kompjuterit-ne-gjirafa50" frameborder="0" allowfullscreen></iframe>')
        $('html').addClass('dialog-is-open');
        howToOpen = true;
    });

    $('.product_howto-close').click(function (e) {
        if (howToOpen && !$(e.target).closest('.product_howto-video iframe').length && !$(e.target).closest('.product_howto-play').length) {
            $('html').removeClass('dialog-is-open');
            $('.product_howto-video').removeClass('show');
            $('#howto-iframe').remove();
        }
    });
</script>

{*{if $checkout_add_buttons}*}
{*<div class="ty-cart-content__payment-methods payment-methods" id="payment-methods">*}
{*<span class="ty-cart-content__payment-methods-title payment-metgods-or">{__("or_use")}</span>*}
{*<table class="ty-cart-content__payment-methods-block">*}
{*<tr>*}
{*{foreach from=$checkout_add_buttons item="checkout_add_button"}*}
{*<td class="ty-cart-content__payment-methods-item">{$checkout_add_button nofilter}</td>*}
{*{/foreach}*}
{*</tr>*}
{*</table>*}
{*<!--payment-methods--></div>*}
{*{/if}*}
{literal}
    <script>
        setTimeout(function() {
            document.querySelector('.help-checkout').classList.add('active');
        }, 80000);

        document.querySelector('.cancel-help').onclick = function() {
            document.querySelector('.help-checkout').classList.remove('active');
        };

        $(document).ready(function () {
            if ($('.with_gjflex').length != $('.with_gjflex.active').length) {
                var items = $('[class^="cart_item-"]');
                var itemArr = [];
                for (var i = 0; i < items.length; i++) {
                    var a = {
                        "id": $("[class^='product_id-']", items[i]).val(),
                        "name": $("[class^='name-']", items[i]).val(),
                        "quantity": $("[class^='amount-']", items[i]).val(),
                        "price": $("[class^='price-']", items[i]).val(),
                    }
                    itemArr.push(a);
                }
                $('.__checkflex_proceed').click(function () {
                    if ($('.with_gjflex').length != $('.with_gjflex.active').length) {
                        var $flashed = $('.with_gjflex:not(.active)');
                        $flashed.css('border', '2px solid #e65228');
                        $flashed.addClass('flash');
                        var $th = 0;
                        if (window.innerWidth < 768)
                            $th = 105;
                        else
                            $th = 50;
                        if ($(window).scrollTop() > $flashed.first().offset().top - $th)
                            $('html, body').animate({
                                scrollTop: $flashed.first().offset().top - $th
                            });
                        return false;
                    }
                });

            }
        });

        if ($('.__checkflex_proceed').css('position') == "fixed") {
            $(window).scroll(function () {
                var scrollTop = $(this).scrollTop() + $(window).height();
                setTimeout(function () {
                    if (scrollTop - 50 > $('[data-ca-external-click-id="button_cart"]').offset().top)
                        $('.__checkflex_proceed').css({
                            'position': 'relative',
                            'text-align': 'center'
                        });
                    else {
                        $('.__checkflex_proceed').css({
                            'position': 'fixed',
                            'text-align': 'right'
                        });
                    }

                }, 300)
            });
        }
    </script>
{/literal}