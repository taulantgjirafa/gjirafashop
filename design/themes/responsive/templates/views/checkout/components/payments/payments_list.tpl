<div class="ty-other-pay clearfix">
    <ul class="ty-payments-list">
        {assign var="raiffeisen_products" value=','|explode:{__('rbko_only_products')}}
        {assign var="cash_only_products" value=','|explode:{__('cash_only_products')}}

        {assign var="disable_cash" value=false}
        {assign var="raiffeisen" value=false}
        {assign var="cash_only" value=false}
        {if $disable_cash_payment == true}
            {$disable_cash = true}
            {else}
            {$disable_cash = false}
        {/if}
        {if $rbko_offer == true || is_array($raiffeisen_products) || is_array($cash_only_products)}
            {foreach from=$cart_products item="product"}
                {if $product.product_id|in_array:$raiffeisen_products}
                    {$raiffeisen = true}
                    {$cash_only = false}
                    {break}
                {elseif $product.product_id|in_array:$cash_only_products}
                    {$cash_only = true}
                {/if}
            {/foreach}
        {/if}

        {hook name="checkout:payment_method"}
        {assign var="is_al" value=$al|fn_isAL}
        {assign var="counter" value=1}
        {foreach from=$payments item="payment"}
            {*{if $cash_only && $payment.payment_id == 15 || $raiffeisen && $payment.payment_id == 13 || (!$cash_only && !$raiffeisen)}*}

            {if $is_al}
                {if $cart.has_license_key_product && $payment.payment_id != 22}
                    {continue}
                {/if}
                {if $cart.total > 1000000 && $payment.payment_id == 24}
                    {continue}
                {/if}
            {else}
                {if $cart.has_license_key_product && $payment.payment_id != 13}
                    {continue}
                {/if}
                {if $cart.total > 10000 && $payment.payment_id == 15}
                    {continue}
                {/if}
            {/if}


            {if $cash_only && ($payment.payment_id == 15 ||  $payment.payment_id == 16 ||  $payment.payment_id == 21) || $raiffeisen && $payment.payment_id == 26 || $disable_cash && ($payment.payment_id == 15 ||  $payment.payment_id == 16 ||  $payment.payment_id == 21) || (!$cash_only && !$raiffeisen )  }
                {if !$raiffeisen && $payment.payment_id == 26}{continue}{/if}
                {if ($disable_cash && $payment.payment_id == 15)}{continue}{/if}
                {if ($disable_cash && $payment.payment_id == 24)}{continue}{/if}
                {if $payment_id == $payment.payment_id}
                    {$instructions = $payment.instructions}
                {/if}

                {if ($is_al && ($payment.payment_id != "22" && $payment.payment_id != "23" && $payment.payment_id != "24")) || (!$is_al && ($payment.payment_id == "22" || $payment.payment_id == "23" || $payment.payment_id == "24"))}
                    {continue}
                {/if}
                <li class="ty-payments-list__item">
                    <input id="payment_{$payment.payment_id}" class="ty-payments-list__checkbox cm-select-payment"
                           type="radio" name="payment_id" value="{$payment.payment_id}" data-ca-url="{$url}"
                           data-ca-result-ids="{$result_ids}"
                           {if $payment_id == $payment.payment_id || ($bank_transfer_only && $payment.payment_id == 16) || $cart.has_license_key_product || $payments|count == 1}checked="checked"{/if}
{*                           {if $counter == 1}checked="checked"{/if}*}
                           {if $payment.disabled}disabled{/if}

                    />

                    <div class="ty-payments-list__item-group">
                        <label for="payment_{$payment.payment_id}" class="ty-payments-list__item-title">

{*                            {include file="common/image.tpl" obj_id=$payment.payment_id images=$payment.image class="ty-payments-list__image"}*}

                            {$payment_image = "https://hhstsyoejx.gjirafa.net/gj50/payments/`$payment.payment_id`.png"}
                            <div class="ty-payments-list__image">
                                <img src="{$payment_image}" alt="" class="ty-pict ty-payments-list__image cm-image lazyloaded" id="det_img_{$payment.payment_id}">
                            </div>

                            {$payment.payment}
                            <div class="ty-payments-list__description">
                                {$payment.description}
                            </div>
                        </label>
                    </div>
                </li>
                {if $bank_transfer_only && $payment.payment_id != 16}
                    {* <div style="background: #fff; opacity: 0.5; width: 100%; height: 77px;"></div> *}
                {/if}
                {if $payment_id == $payment.payment_id}
                    {if $payment.template && $payment.template != "cc_outside.tpl"}
                        <div>
                            {include file=$payment.template}
                        </div>
                    {/if}
                {/if}

            {/if}
            {assign var="counter" value=$counter+1}
        {/foreach}
        {/hook}
    </ul>
    <div class="ty-payments-list__instruction">
        {if $instructions}
            <h3>Informata shtesë:</h3>
            {$instructions nofilter}
            <div class="iute-instructions"></div>
        {/if}
        {if $prepaid || $cart.prepaid}
            <h3>Parapagimi i porosisë:</h3>
            <p>Për këtë porosi duhet të bëhet parapagim 10% i shumës totale. Për më shumë informata do të telefonoheni prej stafit tonë.</p>
            <input type="hidden" name="prepaid" value="true">
        {/if}
    </div>
</div>