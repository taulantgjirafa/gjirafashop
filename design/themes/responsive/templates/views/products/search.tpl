{*{$search.features_hash|var_dump}*}
{if $db_switch != '1'}{literal}<style>.ty-pagination__bottom{display: none;}</style>{/literal}
    <span style="display: none" class="db_switch">0</span>
{else}
    <span style="display: none" class="db_switch">1</span>
{/if}
{if $search.features_hash != ""}
    {assign var=feature_selected value="_"|explode:$search.features_hash}
    {assign var=features_ids value=[]}

    {foreach from=$feature_selected item=feature_item}
        {assign var=feature_variants value="-"|explode:$feature_item}
        {assign var=variant_ids value=[]}

        {for $i=1 to ($feature_variants|@count - 1)}
            {append var=variant_ids value=$feature_variants[$i] index=($i-1)}
        {/for}
        {*{$variant_ids|var_dump}*}
        {if $feature_variants|count gt 0}
            {append var=features_ids value=$variant_ids index=$feature_variants[0]}
        {/if}
    {/foreach}
{/if}

{include file="common/elastic_filters.tpl" elastic_filters=$search.feature_filters url="products.search"}

<div class="{if $search.feature_filters}span12{else}span16{/if}" {if !$search.feature_filters}style="margin:0;"{/if}>
    {*{if $search['subcategory_filters']}*}
    {*<ul class="subcategories clearfix">*}
        {*<div class="subcategories-content">*}
            {*{foreach $search['subcategory_filters'] as $subcategory}*}
            {*<li class="ty-subcategories__item">*}
                {*<a href="{"categories.view?category_id=`$subcategory.category_id`"|fn_url}">*}
                    {*<img src="https://hhstsyoejx.gjirafa.net/gj50/category_img/{$category.category_id}.png" class="ty-subcategories-img"/>*}
                    {*<span>{$subcategory.category} ({$subcategory.count})</span>*}
                {*</a>*}
            {*</li>*}
            {*{/foreach}*}
        {*</div>*}
    {*</ul>*}
    {*{/if}*}
    <div id="products_search_{$block.block_id}">
        {if $products}
            {assign var="title_extra" value="{__("products_found")}: `$search.total_items`"}
            {assign var="layouts" value=""|fn_get_products_views:false:0}

            {if $layouts.$selected_layout.template}
                {include file="`$layouts.$selected_layout.template`" columns=$settings.Appearance.columns_in_products_list show_qty=true}

            {/if}

            {if $search.total_items > $search.items_per_page && !$search_limited}
                {include file="common/load_more_products.tpl" button_visible=true button_text="SHFAQ MË SHUMË PRODUKTE"}
            {/if}
        {else}
            {hook name="products:search_results_no_matching_found"}
                <div style="padding: 20px 10px;">
                    <h3>{__("text_no_matching_products_found")}</h3>
                    <ul style="padding-left: 20px;">
                        <li style="list-style: disc inside;">Kontrolloni për gabime gjatë shkrimit te fjalëve</li>
                        <li style="list-style: disc inside;">Provoni një fjalë tjetër</li>
                        <li style="list-style: disc inside;">Provoni më pak shkronja</li>
                        <li style="list-style: disc inside;">Provoni fjali më gjenerale</li>
                        {if !empty($did_you_mean)} <li style="list-style: disc inside;">A mos menduat: <a style="text-decoration: underline;font-weight: bolder;    font-size: 14px;" href={"products.search&q={$did_you_mean}"|fn_url}>{$did_you_mean} </a></li>{/if}
                    </ul>
                </div>
            {/hook}
        {/if}
    </div>
    <!--products_search_{$block.block_id}-->
</div>



{hook name="products:search_results_mainbox_title"}

{capture name="mainbox_title"}<span class="ty-mainbox-title__left">{__("search_results")}</span><span
    class="ty-mainbox-title__right"
    id="products_search_total_found_{$block.block_id}">{$title_extra nofilter}<!--products_search_total_found_{$block.block_id}--></span>{/capture}
{/hook}

{if !$smarty.const.DEVELOPMENT}
<script>
    var url_string = window.location.href
    var url = new URL(url_string);
    var query = url.searchParams.get('q');
    var product_ids = `{$result_product_ids}`;

    bisko.tr('Search', {
        id: 1,
        query: query,
        type: 'product',
        url: 'https://gjirafa50.com/search?q=' + query + '&rel=' + '{$rand_elastic_url}',
        result: product_ids,
        quantity: `{$search.total_items}`
    });
</script>
{/if}

<script>
    var url_string = window.location.href
    var url = new URL(url_string);
    var rel = {$smarty.cookies.rel};

    {* url.searchParams.set('rel', '{$rand_elastic_url}');
    window.history.pushState('', '', url); *}

    let products = document.querySelectorAll(".ty-grid-list__item--container");

    for (let i = 0; i < products.length; i++) {
        let getAllHref = products[i].getElementsByTagName("a");

        for (let j = 0; j < getAllHref.length; j++) {
            if (getAllHref[j].getAttribute("href") != null) {
                // getAllHref[j].href += "#rel=" + rel;
            }
        }
    }
</script>