{if $products}
{*    {include file="blocks/products/products_multicolumns.tpl" columns=5 items=$products show_qty=true}*}

    {if $block.properties.hide_add_to_cart_button == "Y"}
        {assign var="_show_add_to_cart" value=false}
    {else}
        {assign var="_show_add_to_cart" value=true}
    {/if}

    {assign var="col" value=5}
    {if $block.properties.number_of_columns != null}
        {$col = $block.properties.number_of_columns}
    {/if}

    {include file="blocks/list_templates/grid_list.tpl"
    products=$products
    columns=$col
    form_prefix="block_manager"
    no_sorting="Y"
    no_pagination="Y"
    no_ids="Y"
    obj_prefix="`$block.block_id`000"
    item_number=$block.properties.item_number
    show_name=true
    show_old_price=true
    show_price=true
    show_rating=true
    show_clean_price=true
    show_list_discount=true
    show_add_to_cart=$_show_add_to_cart
    show_add_to_wishlist=true
    but_role="action"
    show_discount_label=true}
{/if}

{*{if $products}{if $settings.Appearance.enable_quick_view == 'Y'}{$quick_nav_ids = $products|fn_fields_from_multi_level:"product_id":"product_id"}{/if}*}
{*{foreach from=$products item="product"}*}
{*<div class="ty-column5">*}
{*{if $product}*}
{*{assign var="obj_id" value=$product.product_id}*}
{*{assign var="obj_id_prefix" value="`$obj_prefix``$product.product_id`"}*}
{*{append var="product" value=$product.rating index="average_rating"}*}
{*{append var="product" value="R" index="discussion_type"}*}
{*{append var="product" value="2867" index="discussion_thread_id"}*}
{*{assign var="rating" value="rating_$obj_id"}*}
{*{include file="common/product_data.tpl" product=$product}*}
{*<div class="ty-grid-list__item ty-quick-view-button__wrapper">*}
{*{assign var="form_open" value="form_open_`$obj_id`"}*}
{*{$smarty.capture.$form_open nofilter}*}
{*<div class="ty-grid-list__image">*}
{*{include file="views/products/components/product_icon.tpl" product=$product show_gallery=true}*}

{*{if $product.amount == null && $product.czc_stock == null && $product.czc_retailer_stock == null}*}
{*<div class="sold-out"></div>*}
{*{/if}*}
{*{assign var="discount_label" value="discount_label_`$obj_prefix``$obj_id`"}*}
{*{assign var="is_al" value=$al|fn_isAL}*}
{*{if !$is_al}*}
{*{$smarty.capture.$discount_label nofilter}*}
{*{/if}*}

{*</div>*}
{*<div class="ty-grid-list__item-name">*}
{*{if $item_number == "Y"}*}
{*<span class="item-number">{$cur_number}.&nbsp;</span>*}
{*{math equation="num + 1" num=$cur_number assign="cur_number"}*}
{*{/if}*}

{*<a href="/index.php?dispatch=products.view&product_id={$obj_id}" class="product-title"*}
{*title="{$product.product}">{$product.product}</a>*}
{*</div>*}

{*{assign var="show_rating" value=true}*}



{*<div class="ty-grid-list__price ">*}
{*<span class="cm-reload-23000665" id="old_price_update_23000665">*}
{*</span>*}
{*<span class="cm-reload-23000665 ty-price-update" id="price_update_23000665">*}
{*<input type="hidden" name="appearance[show_price_values]" value="1">*}
{*<input type="hidden" name="appearance[show_price]" value="1">*}
{*<span class="ty-price" id="line_discounted_price_23000665">*}
{*<span id="sec_discounted_price_23000665" class="ty-price-num">*}
{*{strip}*}
{*{if $smarty.session.settings.company_id.value == 1}*}
{*<span class="ty-price-num">€</span>*}
{*{$product.price|number_format:2}*}
{*{else}*}
{*{$product.price|number_format}*}
{*<span class="ty-price-num">Lekë</span>*}
{*{/if}*}
{*{/strip}*}
{*</span>*}
{*{if $smarty.session.settings.company_id.value != 1}*}

{*{/if}*}
{*</span>*}
{*</span>*}
{*{assign var="old_price" value="old_price_`$obj_id`"}*}
{*{if $product.old_price}*}
{*{if $product.old_price && $product.old_price > $product.price || $product.old_price_al && $product.old_price_al > $product.price_al}*}
{*<span class="cm-reload-{$obj_id}" id="old_price_update_{$obj_id}">*}
{*<span class="ty-list-price ty-nowrap"  id="line_old_price_{$obj_id} ">*}
{*<span class="ty-strike">*}
{*<span {if $is_al}style="font-size: 10px !important;"{/if} id="sec_old_price_{$obj_id}" class="ty-list-price ty-nowrap">*}
{*{if $is_al && $product.old_price_al}*}
{*{$product.old_price_al|number_format:0:".":","} Lekë*}
{*{elseif $product.old_price}*}
{*{$product.old_price} €*}
{*{/if}*}
{*</span>*}
{*</span>*}
{*</span>*}
{*</span>*}
{*{/if}*}
{*{/if}*}
{*<h6 class="vat-included-text text-muted ty-inline-block">Përfshirë TVSH-në</h6>*}
{*</div>*}
{*{if $smarty.capture.$rating}*}
{*<div class="grid-list__rating">*}
{*{$smarty.capture.$rating nofilter}*}
{*</div>*}
{*{/if}*}
{*<div class="ty-grid-list__control">*}
{*<div class="cm-reload-{$obj_prefix}{$obj_id} {$add_to_cart_class}"*}
{*id="add_to_cart_update_{$obj_prefix}{$obj_id}">*}
{*<input type="hidden" name="appearance[show_add_to_cart]" value="{$show_add_to_cart}"/>*}
{*<input type="hidden" name="appearance[show_list_buttons]" value="{$show_list_buttons}"/>*}
{*<input type="hidden" name="appearance[but_role]" value="{$but_role}"/>*}
{*<input type="hidden" name="appearance[quick_view]" value="{$quick_view}"/>*}
{*{include file="buttons/add_to_cart.tpl" but_id="button_cart_`$obj_prefix``$obj_id`" but_name="dispatch[checkout.add..`$obj_id`]" but_role=$but_role block_width=$block_width obj_id=$obj_id product=$product but_meta=$add_to_cart_meta}*}
{*</div>*}
{*{if $settings.Appearance.enable_quick_view == 'Y'}*}
{*{include file="views/products/components/quick_view_link.tpl" quick_nav_ids=$quick_nav_ids}*}
{*{/if}*}
{*{append var='product' value=5 index='amount'}*}
{*</div>*}
{*{assign var="form_close" value="form_close_`$obj_id`"}*}
{*{$smarty.capture.$form_close nofilter}*}
{*<div class="grid-list-icons">*}
{*{assign "is_AL" $a|fn_isAL}*}

{*{if ($product.amount gt 0 && !$is_AL) || ($product.amount_al gt 0 && $is_AL)}*}
{*<img src="images/icons/upto48.svg"/>*}
{*class="one-day-shipping"*}
{*{/if}*}
{*{if 989|in_array:$product.category_ids}*}
{*<img src="images/icons/new-icon.svg" alt="Produkt i sapo ardhur!">*}
{*{/if}*}
{*{if 2061|in_array:$product.category_ids && !$is_AL}*}
{*<img src="images/icons/icon_dogana.svg" alt="Ulje çmimi për shkak të rregullave të reja doganore!!">*}
{*{/if}*}
{*</div>*}
{*</div>*}
{*{/if}</div>{/foreach}{/if}{capture name="mainbox_title"}{$title}{/capture}*}