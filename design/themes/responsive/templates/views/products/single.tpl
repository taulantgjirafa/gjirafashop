<div class="ty-column5 ty-grid-list__item--container rph-{$product.itemId}">
    {$product.product = $product.title}
    {$product.product_id = $product.itemId}
    {$product.main_pair = $product.imageUrl}
    {$redirect_url = "index.php?dispatch=checkout.cart"}
    {$obj_prefix = "729000"}
    {$product.rec_hash = "?rec-{$product.requestId}"}
    {assign var="obj_id" value=$product.product_id}
    {assign var="obj_id_prefix" value="`$obj_prefix``$product.product_id`"}
    {append var="product" value=$product.rating index="average_rating"}
    {append var="product" value="R" index="discussion_type"}
    {append var="product" value=$product.product_id index="discussion_thread_id"}
    {assign var="rating" value="rating_$obj_id"}
    {assign var="image_count" value="1"}
    {assign var="sold_out" value=false}
    {assign var="show_add_to_wishlist" value=true}
    {assign var="show_name" value=true}
    {assign var="show_price_values" value=true}
    {assign var="show_old_price" value=true}
    {assign var="show_price" value=true}
    {assign var="form_meta" value="cm-processed-form"}
    {assign var="show_discount_label" value=true}

    {if $smarty.session.auth.vip && $elastic && !$is_al && $product.vip_price != null}
        {if $product.old_price < $product.vip_price}
            {$product.old_price = $product.price}
        {/if}
        {if $product.price > $product.vip_price}
            {$product.price = $product.vip_price}
        {/if}
    {/if}

    {if $product.salePrice != 0 && $product.salePrice < $product.price}
        {$product.base_price = $product.price}
        {$product.price = $product.salePrice}
        {$product.discount_prc = $product.salePercentage}
        {assign var="discount" value=true}
    {/if}

    {include file="common/product_data.tpl" product=$product}

    {if $product.amount === '0'}
        {$sold_out = true}
    {/if}

    <div class="ty-grid-list__item ty-quick-view-button__wrapper make3D recommended"
         data-id="{$product.product_id}" data-img-count="{$image_count}" data-slide="0" data-init="0">
        <input type="hidden" name="position" value="{$smarty.foreach.sproducts.iteration}" />
        {assign var="form_open" value="form_open_`$obj_id`"}
        {$smarty.capture.$form_open nofilter}
        {hook name="products:product_multicolumns_list"}
            <div class="ty-grid-list__item-all-images">
                <div class="ty-grid-list__image">
                    {include file="views/products/components/product_icon.tpl" product=$product show_gallery=true}
                    {if $sold_out}
                        <div class="sold-out"><h4>E SHITUR</h4></div>
                    {/if}


                </div>
                <div class="ty-grid-list__back-images">
                    <div class="ty-grid-list__carousel">
                        <div class="ty-grid-list__img-to-change">
                            <img src="about:blank">
                        </div>

                        <div class="ty-grid-list__carousel--arrows">
                            <div class="ty-grid-list__carousel--arrow-prev">
                                <div class="y"></div>
                                <div class="x"></div>
                            </div>
                            <div class="ty-grid-list__carousel--arrow-next">
                                <div class="y"></div>
                                <div class="x"></div>
                            </div>
                        </div>
                        <div class="ty-grid-list__buy-now">
                            <a title="Blej tani {$product.product}" href="{"products.view&product_id={$product.product_id}"|fn_url}">BLEJ TANI</a>
                        </div>
                    </div>
                    <div class="ty-grid-list__carousel--close">
                        <div class="cy"></div>
                        <div class="cx"></div>
                    </div>
                </div>

                {if !$sold_out}
                    {if $image_count > 1}
                        <div class="grid-list__item-buttons">
                            <a title="Shiko galerinë për {$product.product}" class="grid-list__item-buttons--button view_gallery ty-button">
                                <p>Shiko galerinë</p>
                                <img src="images/gallery.svg" alt="Shiko galerinë">
                            </a>
                        </div>
                    {/if}
                    <div class="grid-list-icons on-sale">
                        {if $product.limited_to_payment_method_id != 0}
                            <img src="images/pcb-tag-small.png" alt="" style="position: absolute; top: 210px; left: 170px;">
                        {/if}
                        {if 991|in_array:$product.category_ids && !$is_AL}
                            <img src="images/icons/outlet.svg"
                                 alt="Ulje çmimi për shkak të rregullave të reja doganore!!">
                        {/if}
                        {assign "is_AL" $a|fn_isAL}
                        {if ($product.amount gt 0 && !$is_AL) || ($product.amount_al gt 0 && $is_AL)}
                            {if $is_AL}
                                <img src="images/icons/upTo48-1.png"/>
                            {else}
                                <img src="images/icons/upTo48.png"/>
                            {/if}
                            {*class="one-day-shipping"*}
                        {/if}

                        {if 989|in_array:$product.category_ids}
                            <img src="images/icons/new-icon.svg" alt="Produkt i sapo ardhur!">
                        {/if}
                        {if 2061|in_array:$product.category_ids && !$is_AL}
                            <img src="images/icons/icon_dogana.svg"
                                 alt="Ulje çmimi për shkak të rregullave të reja doganore!!">
                        {/if}
                    </div>
                {/if}
                {if !$smarty.session.auth.vip}
                    {assign var="discount_label" value="discount_label_`$obj_prefix``$obj_id`"}

                    {* {if !$is_al} *}
                    {$smarty.capture.$discount_label nofilter}
                    {* {/if} *}
                {else}
                    {assign var="discount_label" value="discount_label_`$obj_prefix``$obj_id`"}
                    {$smarty.capture.$discount_label nofilter}

                    {*                                        {if $product.vip_price && !$product.list_price}*}
                    <img src="images/icons/crown.png" class="crown" title="Dhurata jonë për ju! VIP çmimet janë ekskluzive të cilat mund të i shihni vetëm ju." alt="Çmim VIP"/>
                    {*                                        {/if}*}
                {/if}

            </div>
            {*{if $smarty.capture.$rating}*}
            {*<div class="grid-list__rating">*}
            {*{$smarty.capture.$rating nofilter}*}
            {*</div>*}
            {*{/if}*}
            <div class="ty-grid-list__item-details">
                <div class="ty-grid-list__item-name">
                    {if $item_number == "Y"}
                        <span class="item-number">{$cur_number}.&nbsp;</span>
                        {math equation="num + 1" num=$cur_number assign="cur_number"}
                    {/if}

                    {assign var="name" value="name_$obj_id"}
                    {$smarty.capture.$name nofilter}
                </div>
                <div class="ty-grid-list__price {if $product.price == 0}ty-grid-list__no-price{/if}">

                    {assign var="price" value="price_`$obj_id`"}
                    {$smarty.capture.$price nofilter}

                    {assign var="clean_price" value="clean_price_`$obj_id`"}
                    {$smarty.capture.$clean_price nofilter}

                    {assign var="list_discount" value="list_discount_`$obj_id`"}
                    {$smarty.capture.$list_discount nofilter}

                    {assign var="old_price" value="old_price_`$obj_id`"}
                    {if $a|fn_isAL == 'al'}
                        {assign var="is_al" value=true}
                    {/if}
                    {assign var="list_discount" value="list_discount_`$obj_id`"}
                    {$smarty.capture.$list_discount nofilter}
                    {assign var="old_price" value="old_price_`$obj_id`"}
                    {*{if $smarty.const.STORE == 'al'}*}
                    {*{assign var="is_al" value=true}*}
                    {*{/if}*}

                    {* {if $smarty.capture.$old_price|trim}
                        {if $product.old_price && $product.old_price > $product.price || $product.old_price_al && $product.old_price_al > $product.price_al}
                            <span class="cm-reload-{$obj_id}"
                                  id="old_price_update_{$obj_id}">
                                <span class="ty-list-price ty-nowrap"
                                      id="line_old_price_{$obj_id} ">
                                    <span class="ty-strike">
                                       <span {if $is_al}style="font-size: 10px !important;"{/if}
                                             id="sec_old_price_{$obj_id}"
                                             class="ty-list-price ty-nowrap">
                                        {if $is_al && $product.old_price_al}
                                            {$product.old_price_al|number_format:0:".":","} Lekë
                                        {elseif $product.old_price}
                                            {$product.old_price} €
                                        {/if}
                                       </span>
                                    </span>
                                </span>
                            </span>
                        {/if}
                    {/if} *}
{*                    {if ($product.old_price && $product.price < $product.old_price) ||*}
{*                    ($product.list_price && $product.price < $product.list_price) ||*}
{*                    ($product.old_price_al && $product.price_al < $product.old_price_al) ||*}
{*                    ($product.list_price_al && $product.price_al < $product.list_price_al)}*}
{*                        {if $smarty.capture.$old_price|trim}{$smarty.capture.$old_price nofilter}{/if}*}
{*                    {/if}*}

                    {if $discount}
                        <span class="ty-list-price ty-nowrap" id="line_old_price_{$obj_prefix}{$obj_id}">
                            <span class="ty-strike">{include file="common/price.tpl" value=$product.base_price span_id="old_price_`$obj_prefix``$obj_id`" class="ty-list-price ty-nowrap"}</span>
                        </span>
                    {/if}

                    <p class="vat-included-text text-muted ty-inline-block">
                        Përfshirë TVSH-në
                    </p>
                </div>

                {if $smarty.capture.$rating}
                    <div class="grid-list__rating">
                        {$smarty.capture.$rating nofilter}
                    </div>
                {/if}

            </div>
            <div class="ty-grid-list__control clearfix ty-flex-horizontal ">
                <div class="ty-control__button ty-control__button--details" {if !$show_add_to_wishlist && !$is_wishlist}style="width: 60%;"{elseif $is_wishlist}style="width: 100%;"{/if}>
                    {if $settings.Appearance.enable_quick_view == 'Y'}
                        {include file="views/products/components/quick_view_link.tpl" quick_nav_ids=$quick_nav_ids}
                    {/if}
                </div>

                {*{if $show_add_to_cart}*}
                {*{$product.amount = ($product.amount + $product.czc_stock + $product.czc_retailer_stock)}*}
                {*<div class="button-container">*}
                {*{assign var="add_to_cart" value="add_to_cart_`$obj_id`"}*}
                {*{$smarty.capture.$add_to_cart nofilter}*}
                {*</div>*}
                {*{/if}*}

                {*{include file="buttons/add_to_cart.tpl" but_id="button_cart_`$obj_prefix``$obj_id`" but_name="dispatch[checkout.add..`$obj_id`]" but_role=$but_role block_width=$block_width obj_id=$obj_id product=$product but_meta=$add_to_cart_meta}*}

                {if $product.avail_since < $smarty.const.TIME}
                    <div class="ty-control__button ty-control__button--wishlist" {if !$show_add_to_wishlist}style="width: 100%;"{/if}>
                        <button class="ty-btn__primary ty-btn__add-to-cart rcbtn "
                                type="submit" name="dispatch[checkout.add..{$product.product_id}]">
                            <img src="images/cart.svg" alt="Shto në shportë {$product.product}"/>
                        </button>
                    </div>

{*                    <input type="hidden" name="full_render" value="Y" />*}
{*                    <input type="hidden" name="is_ajax" value="1" />*}

                    {*<div class="cm-reload-{$product.product_id}" id="add_to_cart_update_{$product.product_id}">*}
                    {*<input type="hidden" name="appearance[show_add_to_cart]" value="1">*}
                    {*<input type="hidden" name="appearance[show_list_buttons]" value="1">*}
                    {*<input type="hidden" name="appearance[but_role]" value="big">*}
                    {*<input type="hidden" name="appearance[quick_view]" value="">*}

                    {*<button id="button_cart_{$product.product_id}" class="ty-btn__primary ty-btn__big ty-btn__add-to-cart cm-form-dialog-closer ty-btn" type="submit" name="dispatch[checkout.add..{$product.product_id}]">Shto në shportë</button>*}

                    {*<!--add_to_cart_update_54249--></div>*}

                    {if $show_add_to_wishlist}
                        <div class="ty-control__button ty-control__button--wishlist">
                            <button id="button_wishlist"
                                    class="ty-btn__primary  ty-btn__add-to-wishlist" name="dispatch[wishlist.add..{$product.product_id}]"
                                    type="submit">
                                <img src="images/wishlist.svg" alt="Shto në listën e dëshirave {$product.product}"/>
                            </button>
                        </div>
                    {/if}
                {/if}

            </div>
            <div class="ty-grid-list__price">

            </div>
        {/hook}
        {assign var="form_close" value="form_close_`$obj_id`"}
        {$smarty.capture.$form_close nofilter}

    </div>
</div>

<script>
    gjdmp.tr('Impression', {
        id: '{$product.product_id}',
        name: '{$product.product}',
        type: 'product',
        value: '{$product.requestId}',
        method: 'recommendation-engine'
    });
</script>