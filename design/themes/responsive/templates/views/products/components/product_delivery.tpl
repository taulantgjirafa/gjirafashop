<style>
    @media screen and (max-width: 570px) {
        .cm-notification-content.cm-notification-content-extended.notification-content-extended {
            top: 0 !important;
            margin-top: 10px;
        }

        .cm-notification-content > h1 {
            margin-bottom: 0;
        }

        .ty-product-notification__body {
            padding-top: 0;
        }

        .product-delivery__disclaimer {
            display: none;
        }
    }

    .cm-notification-content > h1 {
        padding-top: 40px;
    }

    .notification-content-extended h1 {
        white-space: normal;
    }

    @media screen and (min-width: 769px) {
        .notification-content-extended {
            width: 500px;
            margin: -40px 0 0 -250px;
        }
    }

    .product-delivery__row {
        border-bottom: 1px solid #ececec;
        padding: 10px 0;
    }

    .product-delivery__row .bold {
        font-weight: bold;
    }

    .product-delivery__title {
        font-size: 16px;
    }

    .product-delivery__label {
        color: #e65228;
    }

    .product-delivery__info {
        background-color: #e8f3fa;
        color: #20648b;
        padding: 10px 15px;
        font-weight: normal;
        justify-content: flex-start;
    }

    .product-delivery__notice {
        background-color: #fdf0ee;
        color: #D94D07;
        padding: 10px 15px;
        font-weight: normal;
        justify-content: flex-start;
    }

    .product-delivery__disclaimer {
        color: #777;
    }
</style>

<div class="ty-product-notification__body">
    <div class="product-delivery__row">
        <p class="product-delivery__title">Koha e arritjes së produktit</p>
    </div>
    <div class="product-delivery__notice safe-b flexing flex-h-center flex-v-center">
        <span>
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 500 500" style="enable-background:new 0 0 500 500;" xml:space="preserve"> <style type="text/css"> .st0 { fill:#FFFFFF; } .st1 { fill:#e65228; } </style> <g> <circle class="st0" cx="250" cy="250" r="239"/> <g> <path class="st1" d="M202.9,376c15,24.7,51.2,11.9,79.1-8c9.9-7.1,0.3-22.5-10.5-16.6c-4.7,2.6-9.5,4.4-14.7,5.3 c-4.8,0.8-9.1-3.1-8.7-8c4.5-53.6,24.6-103.4,44.1-153.4c0.7-1.8-0.8-3.9-2.6-3.4l-79,20.4c-0.8,0.2-1.3,0.8-1.4,1.6l-0.7,5.5 c-0.1,1,0.6,2,1.6,2.1l15.4,2.7c1.6,0,2.8,1.5,2.4,3.1C215.4,276.4,185.2,346.9,202.9,376z"/> <path class="st1" d="M268.6,178.1c18.7,0,33.9-15.1,33.9-33.8c0-18.7-15.1-33.9-33.8-33.9c-18.7,0-33.9,15.1-33.9,33.8 C234.8,162.8,249.9,178,268.6,178.1z"/> </g> </g> </svg>
        </span>
        <span>
            Koha e arritjes së produktit nënkupton periudhën prej kur bëhet verifikimi i porosisë suaj, dhe njoftimit për verifikim që ju e pranoni përmes email-it apo SMS-it.
        </span>
    </div>
    <div class="product-delivery__row flexing flex-h-between">
        <p>Kosovë, Prishtinë</p>
        <p class="bold">
            {if $delivery_dates == null}
                E papërcaktuar
            {else}
                {if $delivery_dates.has_stock}
                    {*                <span class="product-delivery__label">SOT</span>*}
                    {$delivery_dates.local|date_format:$settings.Appearance.date_format}
                {else}
                    {$delivery_dates.local|date_format:$settings.Appearance.date_format} - {$delivery_dates.local_range|date_format:$settings.Appearance.date_format}
                {/if}
            {/if}
        </p>
    </div>
    <div class="product-delivery__row flexing flex-h-between">
        <p>Kosovë, të tjera</p>
        <p class="bold">
            {if $delivery_dates == null}
                E papërcaktuar
            {else}
                {if $delivery_dates.has_stock}
    {*                <span class="product-delivery__label">NESËR</span>*}
                    {$delivery_dates.standard|date_format:$settings.Appearance.date_format} - {$delivery_dates.standard_range|date_format:$settings.Appearance.date_format}
                {else}
                    {$delivery_dates.standard|date_format:$settings.Appearance.date_format} - {$delivery_dates.standard_range|date_format:$settings.Appearance.date_format}
                {/if}
            {/if}
        </p>
    </div>
    <div class="product-delivery__info safe-b flexing flex-h-center flex-v-center">
        <span>
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 500 500" style="enable-background:new 0 0 500 500;" xml:space="preserve"> <style type="text/css"> .ts0 { fill:#FFFFFF; } .ts1 { fill:#3a8cbb; } </style> <g> <circle class="ts0" cx="250" cy="250" r="239"/> <g> <path class="ts1" d="M202.9,376c15,24.7,51.2,11.9,79.1-8c9.9-7.1,0.3-22.5-10.5-16.6c-4.7,2.6-9.5,4.4-14.7,5.3 c-4.8,0.8-9.1-3.1-8.7-8c4.5-53.6,24.6-103.4,44.1-153.4c0.7-1.8-0.8-3.9-2.6-3.4l-79,20.4c-0.8,0.2-1.3,0.8-1.4,1.6l-0.7,5.5 c-0.1,1,0.6,2,1.6,2.1l15.4,2.7c1.6,0,2.8,1.5,2.4,3.1C215.4,276.4,185.2,346.9,202.9,376z"/> <path class="ts1" d="M268.6,178.1c18.7,0,33.9-15.1,33.9-33.8c0-18.7-15.1-33.9-33.8-33.9c-18.7,0-33.9,15.1-33.9,33.8 C234.8,162.8,249.9,178,268.6,178.1z"/> </g> </g> </svg>
        </span>
        <span>
            Nëse porosia bëhet tani, produkti arrin sipas afatit kohor të vendosur më lartë. Ju do të njoftoheni në vazhdimësi përmes emailit rreth vendndodhjes së porosisë suaj, duke përfshirë momentin kur produkti arrin në depon tonë, dhe momentin kur niset në dërgesë për tek ju.
{*            Datat e transportit janë të vlefshme nëse e porositni artikullin tani,<br>*}
{*            {$smarty.now|date_format:$settings.Appearance.date_format:"`$settings.Appearance.date_format`, `$settings.Appearance.time_format`"}*}
{*            dhe për sasi 1.*}
        </span>
    </div>
    <p class="product-delivery__disclaimer pt10">*Në 99% të rasteve, produktet arrijnë sipas parashikimit të vendosur më lartë. Ju lusim të keni parasysh që festat ndërkombëtare ndikojnë që liferimi të shtyhet për rreth 2 ditë.</p>
</div>
