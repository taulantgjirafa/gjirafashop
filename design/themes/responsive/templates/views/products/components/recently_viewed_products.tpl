{assign var="currency_symbol" value=fn_get_company_currency_symbol()}

{* {if $products}
<div class="home-forgot-cart" style="background: none;">
    <div class="flexing hfti">
        <h3 class="flexing flex-v-center">Të shikuara</h3>
    </div>
    <div class="homecart-wrapper">
        {foreach from=$products item=product key=key name=name}
        <a href="{"products.view&product_id=`$product.product_id`"|fn_url}">
            <div class="homepage-cart-item">
                <div class="home-cart-img" >
                    <div style="background-image:url('https://hhstsyoejx.gjirafa.net/gj50/img/{$product.product_id}/thumb/0.jpg');"></div>
                </div>
                <div class="divider"></div>
                <div>
                    <h4 style="min-height: 30px;">{$product.product|truncate:40}</h4>
                    {if $al|fn_isAL}
                    <h3>{$product.price|number_format} Lekë</h3>
                    {else}
                    <h3>{number_format((float)$product.price, 2, '.', '')} €</h3>
                    {/if}
                </div>
            </div>
        </a>
        {/foreach}
    </div>
</div>
{/if} *}

{assign var="is_al" value=$al|fn_isAL}

<div class="hs-section recently-viewed">
    <div class="hs-title flexing flex-h-between flex-v-center">
        <h1>Të shikuara</h1>
    </div>
    <div class="hs-wrapper">
        <div class="arrows">
            <span class="hs-left" style="visibility: hidden;">
                <svg><use xlink:href="#svg-left"></use></svg>
            </span>
            <span class="hs-right">
                <svg><use xlink:href="#svg-right"></use></svg>
            </span>
        </div>
        <div class="hs-scroll">
            {foreach from=$products item=product}

            {if $smarty.session.auth.vip && !$is_al && $product.vip_price != null}
                {if $product.old_price < $product.vip_price}
                    {$product.old_price = $product.price}
                {/if}
                {if $product.price > $product.vip_price}
                    {$product.price = $product.vip_price}
                {/if}
            {/if}

            <div class="hs-item" {if $is_al && !$product.price_al} style="display: none;" {/if} style="position: relative; border: 1px solid #ddd;">

                {if $is_al}
                    {if $product.old_price_al}
                        {$product.discount = $product.old_price_al}
                        {$product.discount_prc_al = (((($product.old_price_al|floatval) - $product.price_al) / $product.old_price_al|floatval) * 100)|string_format:"%.0f"}
                    {/if}
                    {if isset($product.old_price_al) && $product.old_price_al > $product.price_al}
                        {$show_discount_label = true}
                        {if $product.old_price_al - $product.price_al > 125}
                            {$discount_greater_than = true}
                        {/if}
                    {else}
                        {$show_discount_label = false}
                    {/if}
                {else}
                    {if $product.old_price && !$product.list_price}
                        {$product.discount = $product.old_price}
                        {$product.discount_prc = (((($product.old_price) - $product.price) / $product.old_price) * 100)|string_format:"%.0f"}
                    {elseif $product.list_price && $product.list_price > $product.price}
                        {$product.discount = $product.list_price}
                        {$product.discount_prc = (((($product.list_price) - $product.price) / $product.list_price) * 100)|string_format:"%.0f"}
                        {$product.old_price = $product.list_price}
                    {/if}
                    {if $product.old_price > $product.price }
                        {$show_discount_label = true}
                    {/if}
                {/if}

                {if $show_discount_label  && $product.discount_prc > 0}
                <span class="ty-discount-label price-drop_label">
                    <span class="ty-discount-label__item">
                        <span class="ty-discount-label__value">
                        -{if $product.discount_prc}{$product.discount_prc}{/if}%
                        </span>
                    </span>
                </span>
                {/if}

                <a href="{"products.view&product_id={$product.product_id}"|fn_url}{if $msi}?utm_content=homepage_msi_section{else}?utm_content=homepage_recommended_section{/if}" style="text-decoration: none;" class="hs-link">
                    <div class="hs-image flexing flex-h-center flex-v-center">
                        <img src="https://hhstsyoejx.gjirafa.net/gj50/img/{$product.product_id}/thumb/0.jpg" alt="">
                    </div>
                    <div class="hs-in-content">
                        <h3>{$product.product}</h3>
                        <h2>
                            {if $is_al}
{*                                {$product.price_al|number_format} {$currency_symbol}*}
                                {include file="common/price.tpl" value=$product.price_al}
                            {else}
{*                                {number_format((float)$product.price, 2, '.', '')} {$currency_symbol}*}
                                {include file="common/price.tpl" value=$product.price}
                            {/if}
                        {if $product.old_price && $product.old_price > $product.price && !$is_al}
                            <span class="text-muted discount">
{*                                {number_format((float)$product.old_price, 2, '.', '')} {$currency_symbol}*}
                                {include file="common/price.tpl" value=$product.old_price}
                            </span>
                        {elseif $product.old_price_al && $product.old_price_al > $product.price_al && $is_al}
                            <span class="text-muted discount">
{*                                {$product.old_price_al} {$currency_symbol}*}
                                {include file="common/price.tpl" value=$product.old_price_al}
                            </span>
                        {/if}
                        </h2>
                    </div>
                </a>
            </div>
            {/foreach}
        </div>
    </div>
</div>