{hook name="products:notification_items"}
    {if $added_products}
        {foreach from=$added_products item=product key="key"}
            {hook name="products:notification_product"}
            <div class="ty-product-notification__item clearfix">
                {*{assign var="container" value=intval($product.product_id/8000+1)}*}
                {*{if $container gt 5}*}
                    {*{$container=5}*}
                {*{/if}*}
                {assign var="azure_path" value={get_images_from_blob product_id=$product.product_id count=1}}
                {$product.main_pair =["detailed" => ["object_type" => "product", "image_path" => $azure_path , "alt" => ""]]}
                {include file="common/image.tpl" image_width="50" image_height="50" images=$product.main_pair no_ids=true class="ty-product-notification__image"}
                <div class="ty-product-notification__content clearfix">
                    <a href="{"products.view?product_id=`$product.product_id`"|fn_url}" class="ty-product-notification__product-name">{$product.product_id|fn_get_product_name nofilter}</a>
                    {if !($settings.General.allow_anonymous_shopping == "hide_price_and_add_to_cart" && !$auth.user_id)}
                        {if !$smarty.request.dispatch|strstr:"wishlist"}
                            <div class="ty-product-notification__price">
                                {if !$hide_amount}
                                    <span class="none">{$product.amount}</span>&nbsp;x&nbsp;{include file="common/price.tpl" value=$product.price span_id="price_`$key`" class="none"}
                                {/if}
                            </div>
                        {/if}
                    {/if}
                    {if $product.product_option_data}
                    {include file="common/options_info.tpl" product_options=$product.product_option_data}
                    {/if}
                </div>
            </div>
            {/hook}
            {assign var="added" value=$key}
        {/foreach}
        {if $wishlist_view != true}
            <div class="other-products-cart" style="overflow:visible;">
        {foreach from=$smarty.session.cart.products item=product key="key"}
            {if $product@iteration == 1 && $key != $added}
                <hr>
                <h3 class="ty-product-notification__sub-header">Produktet tjera në shportë</h3>
            {/if}
            {if $key == $added && $product.amount == 1}
                {continue}
            {/if}
            <div class="ty-product-notification__item clearfix">
                {assign var="container" value=intval($product.product_id/8000+1)}
                {if $container gt 5}
                    {$container=5}
                {/if}
                {assign var="azure_path" value=("https://hhstsyoejx.gjirafa.net/gj50/img/`$product.product_id`/thumb/0.jpg")}
                {$product.main_pair =["detailed" => ["object_type" => "product", "image_path" => $azure_path , "alt" => ""]]}
                {include file="common/image.tpl" image_width="50" image_height="50" images=$product.main_pair no_ids=true class="ty-product-notification__image"}
                <div class="ty-product-notification__content clearfix">
                    <a href="{"products.view?product_id=`$product.product_id`"|fn_url}" class="ty-product-notification__product-name">{$product.product_id|fn_get_product_name nofilter}</a>
                    {if !($settings.General.allow_anonymous_shopping == "hide_price_and_add_to_cart" && !$auth.user_id)}
                        <div class="ty-product-notification__price">
                            {if !$hide_amount}
                                <span class="none">{$product.amount}</span>&nbsp;x&nbsp;{include file="common/price.tpl" value=$product.display_price span_id="price_`$key`" class="none"}
                            {/if}
                        </div>
                    {/if}
                    {if $product.product_option_data}
                        {include file="common/options_info.tpl" product_options=$product.product_option_data}
                    {/if}
                </div>
            </div>
        {/foreach}
            </div>
    {else}
    {$empty_text}
    {/if}
    {/if}
{/hook}
