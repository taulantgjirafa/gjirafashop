<div class="product-price_labels">
    {if $product.discount}
        <span class="price_labels labels-zbritje">
                                   {$product.discount_prc}% Zbritje
                        </span>
    {/if}
    {if 991|in_array:$product.category_ids}
        <span class="price_labels labels-outlet">Outlet</span>
    {/if}
    {if 989|in_array:$product.category_ids}
        <span class="price_labels labels-risi">Risi</span>
    {/if}
    {if $product.amount == 5}
        <div class="price_labels labels-lowstock"><span>Vetëm 5</span>
        </div>
    {elseif $product.amount == 4}
        <div class="price_labels labels-lowstock"><span>Vetëm 4</span>
        </div>
    {elseif $product.amount == 3}
        <div class="lprice_labels labels-lowstock"><span>Vetëm 3</span>
        </div>
    {elseif $product.amount == 2}
        <div class="price_labels labels-lowstock"><span>Vetëm 2</span>
        </div>
    {elseif $product.amount == 1}
        <div class="price_labels labels-lowstock"><span>Vetëm 1</span>
        </div>
    {/if}
    {if $product.flag}
        <span class="price_labels flags flag-icon flag-icon-{$product.flag.flag}" title="{$product.flag.country}" style="border: none !important;"></span>
    {/if}
</div>

