{capture name="price_info_section_`$obj_id`"}
    {assign var="old_price" value="old_price_`$obj_id`"}
    {assign var="price" value="price_`$obj_id`"}
    {assign var="clean_price" value="clean_price_`$obj_id`"}
    {assign var="list_discount" value="list_discount_`$obj_id`"}
    {assign var="discount_label" value="discount_label_`$obj_id`"}
    {assign var="currency_symbol" value=fn_get_company_currency_symbol()}

    {if $smarty.capture.$price|trim}
        {if $product.best_price_guaranteed && $smarty.const.IS_MOBILE}
            <div style="display: flex; text-align: left; padding: 10px;">
                <div style="width: 12%; justify-content: center; align-items: center; display: flex; padding-right: 8px;">
                    <img src="https://hhstsyoejx.gjirafa.net/gj50/bestpricebadge.png" width="350" alt="Best price badge"></div>
                <div style="width: 85%;">
                    <h3 class="mb0">{__("best_price_guaranteed")}</h3>
                    <p style="color: #a1a1a1;">{__("best_price_guaranteed_text")}</p>
                </div>
            </div>
        {/if}
        <div class="product-price_inner">
            {if $smarty.const.IS_MOBILE}
                <div class="price-drop_productpage">
                    {if $smarty.capture.$old_price|trim || $smarty.capture.$clean_price|trim || $smarty.capture.$list_discount|trim}
                        <div class="ty-product-bigpicture__prices">
                          <div class="price_inner--amount">
{*                                <span>*}
{*                                    {if !$a|fn_isAL}{$product.price|string_format:"%.2f"} {$currency_symbol}{else}{$product.price|number_format} {$currency_symbol}{/if}*}
{*                                </span>*}
                                {include file="common/price.tpl" value=$product.price}
                {*{if $product.base_price > $product.price}*}
                    {*<span class="price_inner--sale">*}
                                      {*{if $smarty.capture.$old_price|trim}{$product.base_price|string_format:"%.2f"}{/if}*}
                        {*{if $smarty.capture.$old_price|trim}{$smarty.capture.$old_price nofilter}{/if}*}
                                    {*</span>*}
                {*{/if}*}

                             {if $smarty.capture.$old_price|trim && ($product.price < $product.base_price || ($product.list_price && $product.list_price > 0))}
                                <div class="old-price" style="display: inline-block;">
                                    {$smarty.capture.$old_price nofilter}
                                </div>
                            {/if}

                         </div>

                            <input type="hidden" value="10" id="customs_sale">
                            {assign var="discount_label" value="discount_label_`$obj_prefix``$obj_id`"}

                            {*LABEL PER ZBRITJEEEEEEEEEEEEEEE *}
                            {*{$smarty.capture.$discount_label nofilter}*}

                            {if $product.discount}
                                <div class="discount">
                                    {$smarty.capture.$clean_price nofilter}
                                    {$smarty.capture.$list_discount nofilter}
                                </div>
                            {/if}
                        </div>
                    {/if}
                       <small>* Përfshirë TVSH-në</small>
                       {if $al|fn_isal}
                           <small>* Çmimet e shfaqura janë me lekë të reja.</small>
                       {/if}
                    {* <small id="without-vat" class="hidden" style="margin: 20px 0 0 2px;color: #999;display: inline-block;font-size: 10px;right: auto;position: relative;display: block;">{$price_without_vat}</small> *}
                    <small id="without-vat" class="hidden" style="margin: 20px 0 0 2px;color: #999;display: inline-block;font-size: 10px;right: auto;position: relative;display: block;">{if $product.price_without_vat && !fn_isAL()}{$product.price_without_vat} € pa TVSH{/if}</small>
                </div>
            {else}
                <div class="price-drop_productpage">
                    {if $smarty.capture.$old_price|trim || $smarty.capture.$clean_price|trim || $smarty.capture.$list_discount|trim}
                        <div class="ty-product-bigpicture__prices">
                            {if $smarty.capture.$old_price|trim && ($product.price < $product.base_price || ($product.list_price && $product.list_price > 0))}
                                <div class="old-price">
                                    {$smarty.capture.$old_price nofilter}
                                </div>
                            {/if}
                            <input type="hidden" value="10" id="customs_sale">
                            {assign var="discount_label" value="discount_label_`$obj_prefix``$obj_id`"}

                            {*LABEL PER ZBRITJEEEEEEEEEEEEEEE *}
                            {*{$smarty.capture.$discount_label nofilter}*}

                            {if $product.discount}
                                <div class="discount">
                                    {$smarty.capture.$clean_price nofilter}
                                    {$smarty.capture.$list_discount nofilter}
                                </div>
                            {/if}
                        </div>
                    {/if}
            <span class="price_inner--amount">
{*               {if !$a|fn_isAL}{$product.price|string_format:"%.2f"} {$currency_symbol}{else}{$product.price|number_format} {$currency_symbol}{/if}*}
               {include file="common/price.tpl" value=$product.price}
                {* {if $product.base_price > $product.price} *}
                    {* <span class="price_inner--sale"> *}
                                      {* {if $smarty.capture.$old_price|trim}{$product.base_price|string_format:"%.2f"}{/if} *}
                        {*{if $smarty.capture.$old_price|trim}{$smarty.capture.$old_price nofilter}{/if}*}
                                    {* </span> *}
                {* {/if} *}
                            </span>
                    <small>* Përfshirë TVSH-në</small>
                    {if $al|fn_isal}
                        <small>* Çmimet e shfaqura janë me lekë të reja.</small>
                    {/if}
                    {* <small id="without-vat" class="hidden" style="margin-top: 5px;color: #999;">{$price_without_vat}</small> *}
                    <small id="without-vat" class="hidden" style="margin-top: 5px;color: #999;">
                        {if $product.price_without_vat && !fn_isAL()}
{*                            {$product.price_without_vat} {$currency_symbol} pa TVSH*}
                            {include file="common/price.tpl" value=$product.price_without_vat} pa TVSH
                        {/if}
                    </small>
                </div>
            {/if}
        </div>
        {*<div class="ty-product-block__price-actual">*}
        {*{$smarty.capture.$price nofilter}*}
        {*</div>*}
    {/if}
    {include file="views/products/components/product_labels.tpl" product=$product}
    <div class="product-price_info">
        {if $product.amount gt 0}
            <div class="product-price_info--status text-left">
                {* <img src="images/stock.svg" alt=""> *}
                Disponueshmëria:
                <br>
                <strong>{if $product.real_amount + $product.czc_stock + $product.czc_retailer_stock > 10}Më shumë se {/if} {$product.amount} {if $product.amount == 1}artikull{else}artikuj{/if}</strong>
            </div>
        {else}
            <div class="product-price_info--status text-left">
                0 artikuj në stok <span style="font-size: 16px;" class="h2">😢</span>


            </div>
        {/if}
        {if $smarty.session.settings.company_id.value == 1 && $product.real_amount gt 0}
            <div class="product-price_info--status product-price_info--delivery text-right">
                <img src="images/delivery-truck.svg" alt="">
                Transporti:
                <br>
                <strong>
                    {if $al|fn_isal}
                        48 orë
                    {else}
                        24 orë
                    {/if}
                </strong>
            </div>
        {elseif $product.al_stock && $smarty.session.settings.company_id.value == 19}
            <div class="product-price_info--status product-price_info--delivery text-right">
                <img src="images/delivery-truck.svg" alt="">
                Transporti:
                <br>
                <strong>
                    {if $al|fn_isal}
                        48 orë
                    {else}
                        24 orë
                    {/if}
                </strong>
            </div>
        {/if}
    </div>
    {if $settings.General.display_delivery == 'Y' && $product.amount gt 0 && !$al|fn_isAL && !$product.license_key_product}
        {if $product.avail_since < $smarty.const.TIME}
            {if $product.no_delivery_date}
                <p>Koha e dërgesës: <strong>E papërcaktuar</strong></p>
            {else}
                <div class="product-price_info--delivery" style="color: #e65228; text-decoration: underline; padding-bottom: 15px; text-align: center;">
{*                    <a href="{"products.delivery_dates&product_id={$product.product_id}"|fn_url}" class="cm-ajax product-delivery-time">Kur arrinë produkti?</a>*}
                    <a href="{"products.delivery_dates&product_id={$product.product_id}"|fn_url}" class="cm-dialog-opener cm-dialog-auto-size" data-ca-view-id="{$product.product_id}" data-ca-target-id="product_delivery_dates" data-ca-dialog-title="{__("product_delivery_dates")}">Kur arrinë produkti?</a>
                </div>
            {/if}
        {/if}
    {/if}
{/capture}
{if $no_capture}
    {assign var="capture_name" value="price_info_mobile_`$obj_id`"}
    {$smarty.capture.$capture_name nofilter}
{/if}


{capture name="add_to_cart_section_`$obj_id`"}
    {$smarty.capture.$form_open nofilter}
    {assign var="form_open" value="form_open_`$obj_id`"}
    {$smarty.capture.$form_open nofilter}

    {include file="views/products/components/product_installaments.tpl" product=$product}



    {if $capture_options_vs_qty}{capture name="product_options"}{$smarty.capture.product_options nofilter}{/if}
    <div class="ty-product-block__field-group">
        {*{assign var="product_amount" value="product_amount_`$obj_id`"}*}
        {*{$smarty.capture.$product_amount nofilter}*}

        {assign var="qty" value="qty_`$obj_id`"}
        {$smarty.capture.$qty nofilter}

        {assign var="min_qty" value="min_qty_`$obj_id`"}
        {$smarty.capture.$min_qty nofilter}

    </div>
    {if $product.amount gt 0 && ($product.avail_since < $smarty.const.TIME)}
        <div class="with_gjflex" id="with_gjflex_product_view">
{*            <input type="hidden" name="product_data[{$product.product_id}][gjflex]" value="">*}
{*            <input type="checkbox"*}
{*                   name="product_data[{$product.product_id}][gjflex]" id="gjflexyes_{$product.product_id}"*}
{*                    {if $cart.products.$key.gjflex === true} checked="checked"{/if} value="1">*}


            {*<h1>KATASTROF IDEJA</h1>*}
            {*<input class="styled-checkbox gjflex_checkbox" id="styled-checkbox-1" type="checkbox"*}
            {*value="1"*}
            {*name="with_gjflex_{$product.product_id}">*}

            {if count($check_category) > 0 || $product.license_key_product}
                <p class="text-muted">Gjirafa FLEX nuk është e disponueshme për këtë produkt.</p>
            {else}
                {if $product.is_edp != 'Y'}
{*                    <label for="gjflexyes_{$product.product_id}">*}
{*                        <span><img src="images/stock.svg" alt=""></span>*}
{*                        E dua me GjirafaFLEX*}
{*                    </label>*}

                    <div class="with_gjflex--inner">
                        <div onclick="$(this).next().slideToggle();">E dua me GjirafaFLEX <i class="ty-icon-help-circle"></i></div>
                        <div style="display:none;">
                            <p>
                                GjirafaFLEX është shërbim shtesë që ne e ofrojmë si zgjidhje të
                                shpejtë
                                nëse keni defekt me produktin brenda periudhës 1 vjeçare të
                                garancionit.
                                Do të thotë, ose merrni produkt të ri ose merrni kredit në
                                Gjirafa50 për
                                vlerën e produktit. Pa pasur nevojë për pritje tek serviset e
                                autorizuara, GjirafaFLEX ju mundëson zgjidhje aty për aty.

                                <br>
                            </p>
                            <p class="text-muted">*Defektet përfshijnë vetëm ato fabrike, dhe jo
                                dëmet
                                fizike që mund të i shkaktohen produktit.</p>
                        </div>
                    </div>

                    <label for="gjflexyes_{$product.product_id}">
                        <input type="radio"
                               name="product_data[{$product.product_id}][gjflex]" id="gjflexyes_{$product.product_id}"
                               value="1">
                        <strong>Po</strong>
                    </label>

                    <label for="gjflexno_{$product.product_id}" style="margin-left: 20px;">
                        <input type="radio" class="gjflexno"
                               name="product_data[{$product.product_id}][gjflex]" id="gjflexno_{$product.product_id}"
                               value="0">
                        <strong>Jo</strong>
                    </label>

{*                    <span class="gjflex-button-glow button-glow-left" style="display: none;"></span>*}
{*                    <span class="gjflex-button-glow button-glow-right" style="display: none;"></span>*}

                {/if}
            {/if}
        </div>
    {/if}
    {if $capture_options_vs_qty}{/capture}{/if}

{assign var="product_edp" value="product_edp_`$obj_id`"}
{$smarty.capture.$product_edp nofilter}

{if $capture_buttons}{capture name="buttons"}{/if}
<div class="ty-product-block__button">
    {if $show_details_button}
        {include file="buttons/button.tpl" but_href="products.view?product_id=`$product.product_id`" but_text=__("view_details") but_role="submit"}
    {/if}

    {assign var="add_to_cart" value="add_to_cart_`$obj_id`"}
    {$smarty.capture.$add_to_cart nofilter}

    {assign var="list_buttons" value="list_buttons_`$obj_id`"}
    {$smarty.capture.$list_buttons nofilter}

</div>
{if $capture_buttons}{/capture}{/if}



{assign var="form_close" value="form_close_`$obj_id`"}
{$smarty.capture.$form_close nofilter}
{/capture}


{if $no_capture}
    {assign var="capture_name" value="add_to_cart_mobile_`$obj_id`"}
    {$smarty.capture.$capture_name nofilter}
{/if}


{capture name="how_to_video_`$obj_id`"}
    {if !$smarty.const.IS_MOBILE}
        <div class="product_howto">
            <h3>
                Si të porosisim në <b>Gjirafa50</b>?
            </h3>
            <div class="product_howto-play">
                <img src="images/howtoimg.jpg" class="product_howto-bg" alt="">
            </div>
        </div>
        <div class="product_howto-video">
            <div class="product_howto-video-inner">
                <div class="product_howto-ratio"></div>
                <div class="product_howto-close">
                    <div class="close-x"></div>
                    <div class="close-y"></div>
                </div>
            </div>
        </div>
    {/if}
{/capture}
{if $no_capture}
    {assign var="capture_name" value="how_to_video_`$obj_id`"}
    {$smarty.capture.$capture_name nofilter}
{/if}


{capture name="sticky_add_to_cart_`$obj_id`"}
    {if $product.amount gt 0}
        <div class="sticky__add-to-cart hidden-desktop">
            <div class="container bbox">
                {*<div class="img"*}
                     {*style="background-image:url('{get_images_from_blob product_id=$product.product_id count=1}');"></div>*}
                {*<div class="ty-float-left">*}
                    {*<h3 class="text-light no-margin">{$product.product}</h3>*}
                    {*<h2 class="no-margin"><b>{$currency_symbol}{$product.price}</b></h2>*}
                {*</div>*}

                {if $settings.General.call_to_order == 'Y'}
                    {assign var="phone_number" value=($al|fn_isAL) ? '+355 68 803 0303' : '+383 45 101 953'}
                    <a href="tel:{$phone_number}" class="ty-call-to-order ty-btn ty-btn__primary ty-float-right" style="padding: 0;"></a>
                {/if}

                <button onclick="$('.ty-product-block__button .ty-add-to-wish').click();return false;"
                   class=" ty-add-to-wish ty-btn ty-btn__primary ty-float-right"></button>
                <button onclick="$('.ty-product-block__button .ty-btn__add-to-cart').click();return false;"
                   class=" ty-btn ty-btn__primary ty-float-right"><svg><use xlink:href="#svg-cart"></use></svg></button>


            </div>
        </div>
    {/if}
{/capture}
{if $no_capture}
    {assign var="capture_name" value="sticky_add_to_cart_`$obj_id`"}
    {$smarty.capture.$capture_name nofilter}
{/if}

{if $smarty.server.REQUEST_URI|strstr:"?def"}
    <script>
        gjdmp.tr('Click', {
            id: '{$product.product_id}',
            name: '{$product.product}',
            type: 'product',
            value: '',
            method: 'default'
        });
    </script>
{/if}

{if $smarty.server.REQUEST_URI|strstr:"?rec"}
    <script>
        gjdmp.tr('Click', {
            id: '{$product.product_id}',
            name: '{$product.product}',
            type: 'product',
            value: window.location.href.slice(window.location.href.indexOf('?rec-') + '?rec-'.length),
            method: 'recommendation-engine'
        });
    </script>
{/if}

<script>
    $(document).ready(function() {
        $.ajax({
            url: 'index.php?dispatch=pages.product_view&bisko_sid=' + biskoSid + '&product_id={$product.product_id}',
            type: "get",
            success: function () {
            },
            error: function () {
            },
            complete: function () {
            },
        });
    });
</script>