{assign var="someVar" value=","|explode:$product.product}
{assign var="is_AL" value=$a|fn_isAL}
{if !empty($recommendations)}
    {if $is_cart == true}
        <div class="checkout-also-bought-wrapper cart-recommended">
            <h4>Disa nga produktet që mund të ju interesojnë</h4>
            <div>
                <div class="sliderWrapper">
                    <span class="left"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 477.175 477.175"><path
                                d="M145.188 238.575l215.5-215.5c5.3-5.3 5.3-13.8 0-19.1s-13.8-5.3-19.1 0l-225.1 225.1c-5.3 5.3-5.3 13.8 0 19.1l225.1 225c2.6 2.6 6.1 4 9.5 4s6.9-1.3 9.5-4c5.3-5.3 5.3-13.8 0-19.1l-215.4-215.5z"/></svg></span>
                    <span class="right"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 477.175 477.175"> <path
                                    d="M360.731 229.075l-225.1-225.1c-5.3-5.3-13.8-5.3-19.1 0s-5.3 13.8 0 19.1l215.5 215.5-215.5 215.5c-5.3 5.3-5.3 13.8 0 19.1 2.6 2.6 6.1 4 9.5 4 3.4 0 6.9-1.3 9.5-4l225.1-225.1c5.3-5.2 5.3-13.8.1-19z"/></svg></span>
                    <div class="innerWrapper">
                        {assign "slider" true}
                        {foreach $recommendations as $recommended}
                            {foreach $recommended.related_code  as $related}
{*                                {if ($related.czc_stock || $related.czc_retailer_stock || $related.amount) > 0 }*}
                                    {assign var="obj_id" value=$related.product_id}
                                    {assign var="obj_id_prefix" value="`$obj_prefix``$related.product_id`"}
                                    {include file="common/product_data.tpl" product=$related}
                                    {include file="views/products/components/product_bigpicture_right.tpl" product=$related}
                                    {assign var="url" value="https://gjirafa50.com/index.php?dispatch=products.view&product_id={$related.product_id}"|escape:'url'}
                                    <div class="slidItem">
                                  <span class="imgHolder">
                                           <a href="{"products.view&product_id={$related.product_id}"|fn_url}">
                                    <img src="{get_images_from_blob product_id=$related.product_id count=1}"></a>
                                   </span>

                                        <h4 style="white-space: pre-wrap;"><a href="{"products.view&product_id={$related.product_id}"|fn_url}">{$related.product}</a></h4>
                                        <div class="checkout-also-bought-price cart-recommended__price">
                                            <h3>
                                                {if !$is_AL}
{*                                                    {$related.price|string_format:"%.2f"} €*}
                                                    {include file="common/price.tpl" value=$related.price}
                                                {else}
{*                                                    {$related.price_al|number_format} Lekë*}
                                                    {include file="common/price.tpl" value=$related.price_al}
                                                {/if}
                                            </h3>
                                        </div>
                                        <div class="cart-recommended__button">
                                            {assign var="form_open" value="form_open_`$obj_id`"}
                                            {$smarty.capture.$form_open nofilter}
                                            <button id="button_cart_{$obj_id}"
                                                    class="ty-btn__primary ty-btn__big ty-btn__add-to-cart cm-form-dialog-closer ty-btn"
                                                    type="submit" name="dispatch[checkout.add..{$obj_id}]">Shto në
                                                shportë
                                            </button>
                                            {assign var="form_close" value="form_close_`$obj_id`"}
                                            {$smarty.capture.$form_close nofilter}
                                        </div>
                                    </div>
{*                                {else}*}
{*                                    {if count($recommended) == 1}*}
{*                                        {$slider = false}*}
{*                                    {/if}*}
{*                                {/if}*}
                            {/foreach}
                        {/foreach}
                    </div>
                </div>
            </div>
        </div>
        <script>
            var clicked = true;
            $('.ty-btn__add-to-cart').click(function () {
                clicked = false;
                $.ceEvent('on', 'ce.ajaxdone', function (data) {
                    if (!clicked)
                        $('#button_cart').click();
                    clicked = true;
                });
            });
        </script>
    {else}
        <h3 class="more-products">Bashkangjitja {$someVar[0]} edhe ndonjë aksesorë</h3>
        <div class="accordion bbox clearfix recommended-products-accordion">
            {foreach $recommendations as $recommend}
                {assign var="products_in_stock" value=0}

                {foreach $recommend.products as $product}
                    {if $product.total_stock > 0}
                        {$products_in_stock = $products_in_stock + 1}
                    {/if}
                {/foreach}

                {if $products_in_stock > 0}
                    <div class="accordion-header bbox {if $recommend@iteration == 1} active {/if}">{$recommend.category}</div>
                    <div class="accordion-content bbox {if $recommend@iteration != 1} not-loaded {/if} {if $recommend@iteration == 1} active {/if}">
                        <div class="accordion-content-inner">
                            {foreach $recommend.products as $product}
                                {if ($product.czc_stock || $product.czc_retailer_stock || $product.amount) > 0 }
                                    {assign var="obj_id" value=$product.product_id}
                                    {assign var="obj_id_prefix" value="`$obj_prefix``$product.product_id`"}
                                    {include file="common/product_data.tpl" product=$product}
                                    <div class="span2 ty-center">
                                        {assign var="form_open" value="form_open_`$obj_id`"}
                                        {$smarty.capture.$form_open nofilter}
                                        <a href="/index.php?dispatch=products.view&product_id={$product.product_id}&utm_source=web&utm_campaign=relatedaccessories">
                                            <div class="img">
                                                <img {if $recommend@iteration != 1} data-src="{get_images_from_blob product_id=$product.product_id count=1}" src="//:0" {else}src="{get_images_from_blob product_id=$product.product_id count=1}" {/if}
                                                        alt="{$product.product}"></div>
                                            <h5 style="white-space: normal;margin: 5px 0;overflow: hidden;text-overflow: ellipsis;display: -webkit-box;-webkit-box-orient: vertical; -webkit-line-clamp: 2;line-height: 15px;max-height: 30px;">{$product.product}</h5>
                                            <h3>
                                                {if !$is_AL}
{*                                                    {$product.price|string_format:"%.2f"} €*}
                                                    {include file="common/price.tpl" value=$product.price}
                                                {else}
{*                                                    {$product.price_al|number_format} Lekë*}
                                                    {include file="common/price.tpl" value=$product.price_al}
                                                {/if}
                                            </h3>
                                        </a>
                                        {* {if $smarty.capture.buttons_product}{$smarty.capture.buttons_product nofilter}{/if} *}
                                        {assign var="form_open" value="form_open_`$obj_id`"}
                                        {$smarty.capture.$form_open nofilter}
                                        <button id="button_cart_{$obj_id}"
                                                class="ty-btn__primary ty-btn__big ty-btn__add-to-cart cm-form-dialog-closer ty-btn"
                                                type="submit" name="dispatch[checkout.add..{$obj_id}]">Shto në
                                            shportë
                                        </button>
                                        {assign var="form_close" value="form_close_`$obj_id`"}
                                        {$smarty.capture.$form_close nofilter}
                                        {assign var="form_close" value="form_close_`$obj_id`"}
                                        {$smarty.capture.$form_close nofilter}
                                    </div>
                                {/if}
                            {/foreach}
                        </div>
                    </div>
                {/if}
            {/foreach}
        </div>
    {/if}
{/if}
{literal}
    <script>
        $(".accordion").on("click", ".accordion-header", function () {
            var $next = $(this).next();
            if ($next.hasClass('not-loaded')) {
                $next.removeClass('not-loaded').find('img').each(function () {
                    $(this).attr('src', $(this).data('src'));
                });
            }
            $(this).toggleClass("active").next().toggleClass("active");
        });
    </script>
{/literal}
{if $slider}
    {script src="js/tygh/recommended_slider.js"}
{/if}