{assign var="th_size" value=$thumbnails_size|default:35}

{if $product.main_pair.icon || $product.main_pair.detailed}
    {assign var="image_pair_var" value=$product.main_pair}
{elseif $product.option_image_pairs}
    {assign var="image_pair_var" value=$product.option_image_pairs|reset}
{/if}

{*{assign var="container" value=intval($product.product_id/8000+1)}*}
{*{if $container gt 5}*}
    {*{$container=5}*}
{*{/if}*}

{if $image_pair_var.image_id}
    {assign var="image_id" value=$image_pair_var.image_id}
{else}
    {assign var="image_id" value=$image_pair_var.detailed_id}
{/if}

{if !$preview_id}
    {assign var="preview_id" value=$product.product_id|uniqid}
{/if}
{*{include file="common/svgicons.tpl"}*}
{*<div class="product-header flexing flex-h-between flex-v-center ">*}
    {*<div class="flexing flex-h-between flex-v-center">*}
        {*<span class="inc back">*}
            {*<svg><use xlink:href="#svg-back"></use></svg>*}
        {*</span>*}
        {*<img src="/gjirafa5038/images/logos/logo.svg" alt="">*}
    {*</div>*}
    {*<div>*}
        {*<span class="inc home">*}
            {*<a href="/">*}
                {*<svg><use xlink:href="#svg-home"></use></svg>*}
            {*</a>*}
        {*</span>*}
        {*<span class="inc more-o">*}
            {*<svg><use xlink:href="#svg-user"></use></svg>*}
        {*</span>*}
        {*<span class="inc cart">*}
            {*<svg><use xlink:href="#svg-cart"></use></svg>*}
        {*</span>*}

    {*</div>*}
{*</div>*}
<div class="ty-product-img cm-preview-wrapper peppermint peppermint-inactive"  id="peppermint">
<figure>
    {assign var="azure_path" value={get_images_from_blob product_id=$product.product_id count=1 type="large"}}

    {$image_pair_var =["detailed" => ["object_type" => "product", "image_path" => $azure_path , "alt" => $product.product]]}
{include file="common/image.tpl" obj_id="`$preview_id`_0" images=$image_pair_var link_class="cm-image-previewer" image_width=$image_width image_height=$image_height image_id="preview[product_images_`$preview_id`]"}
</figure>
{*{foreach from=$product.image_pairs item="image_pair"}*}
    {assign var="azure_path_icons" value={get_images_from_blob product_id=$product.product_id count=($product.img_count) type="large"}}

    {for $index=1 to $product.img_count-1}
        <figure>
    {*{if $image_pair}*}
        {*{if $image_pair.image_id}*}
            {*{assign var="img_id" value=$image_pair.image_id}*}
        {*{else}*}
            {*{assign var="img_id" value=$image_pair.detailed_id}*}
        {*{/if}*}
        {$image_pair =["detailed" => ["object_type" => "product", "image_path" => $product_imgs[$index] , "alt" => $product.product]]}

        {include file="common/image.tpl" images=$image_pair link_class="cm-image-previewer hidden" obj_id="`$preview_id`_`$index`" image_width=$image_width image_height=$image_height image_id="preview[product_images_`$preview_id`]"}
    {*{/if}*}
        </figure>
    {/for}
{*{/foreach}*}
</div>
<a onclick="$('.ty-add-to-wish').click();return false;" class="fav-product flexing flex-v-center flex-h-center">
    <svg viewBox="0 0 23.217 23.217">
        <path d="M11.608,21.997c-22.647-12.354-6.268-27.713,0-17.369C17.877-5.716,34.257,9.643,11.608,21.997z"/>
    </svg>
</a>
{if $product.img_count > 1}
    {if $settings.Appearance.thumbnails_gallery == "Y"}
        <input type="hidden" name="no_cache" value="1" />
        {strip}
        <div class="ty-center ty-product-bigpicture-thumbnails_gallery hidden-phone">
            <div class="cm-image-gallery-wrapper ty-thumbnails_gallery ty-inline-block">
                {strip}
                <div class="ty-product-thumbnails owl-carousel cm-image-gallery" id="images_preview_{$preview_id}">
                    {if $image_pair_var}
                        <div class="cm-item-gallery ty-float-left">
                            {assign var="azure_path" value={get_images_from_blob product_id=$product.product_id count=1}}
                            {$image_pair_var =["detailed" => ["object_type" => "product", "image_path" => $azure_path , "alt" => ""]]}
                            <a data-ca-gallery-large-id="det_img_link_{$preview_id}_0" class="cm-gallery-item cm-thumbnails-mini active ty-product-thumbnails__item" style="width: {$th_size}px">
                           {include file="common/image.tpl" images=$image_pair_var image_width=$th_size image_height=$th_size show_detailed_link=false obj_id="`$preview_id`_0_mini"}
                            </a>
                        </div>
                    {/if}
                    {if $product.img_count}
                        {assign var="azure_path_icons" value={get_images_from_blob product_id=$product.product_id count=$product.img_count}}
                        {for $index=1 to $product.img_count-1}
                            {if $image_pair}
                                <div class="cm-item-gallery ty-float-left">
                                    {if $image_pair.image_id}
                                        {assign var="img_id" value=$image_pair.image_id}
                                    {else}
                                        {assign var="img_id" value=$image_pair.detailed_id}
                                    {/if}

                                    {$image_pair =["detailed" => ["object_type" => "product", "image_path" => $product_imgs[$index] , "alt" => ""]]}
                                    <a data-ca-gallery-large-id="det_img_link_{$preview_id}_{$index}" class="cm-gallery-item cm-thumbnails-mini ty-product-thumbnails__item" style="width: {$th_size}px">
                                    {include file="common/image.tpl" images=$image_pair image_width=$th_size image_height=$th_size show_detailed_link=false obj_id="`$preview_id`_`$index`_mini"}
                                    </a>
                                </div>
                            {/if}
                        {/for}
                    {/if}
                </div>
                {/strip}
            </div>
        </div>
        {/strip}
    {else}
        <div class="ty-product-thumbnails ty-center cm-image-gallery" id="images_preview_{$preview_id}" style="width: {$image_width}px;">
            {strip}
                {if $image_pair_var}
                <a data-ca-gallery-large-id="det_img_link_{$preview_id}_{$image_id}" class="cm-thumbnails-mini active ty-product-thumbnails__item">
                    {include file="common/image.tpl" images=$image_pair_var image_width=$th_size image_height=$th_size show_detailed_link=false obj_id="`$preview_id`_`$image_id`_mini"}
                </a>
                {/if}

                {if $product.image_pairs}
                    {foreach from=$product.image_pairs item="image_pair"}
                        {if $image_pair}
                                {if $image_pair.image_id == 0}
                                    {assign var="img_id" value=$image_pair.detailed_id}
                                {else}
                                    {assign var="img_id" value=$image_pair.image_id}
                                {/if}
                                <a data-ca-gallery-large-id="det_img_link_{$preview_id}_{$img_id}" class="cm-thumbnails-mini ty-product-thumbnails__item">
                                {include file="common/image.tpl" images=$image_pair image_width=$th_size image_height=$th_size show_detailed_link=false obj_id="`$preview_id`_`$img_id`_mini"}
                                </a>
                        {/if}
                    {/foreach}
                {/if}
            {/strip}
        </div>
    {/if}
{/if}
{*{/if}*}


{include file="common/previewer.tpl"}
{script src="js/tygh/product_image_gallery.js"}
{script src="js/lib/Peppermint.js"}
<link rel="stylesheet" href="var/themes_repository/responsive/css/lib/peppermint.css">
<style>{literal} @media all and (max-width:769px){
        .ty-dropdown-box__title{
            border:0;
            width:60px;
        }

        .mobile-header.scroll .ty-header-cart, .mobile-header.scroll button#mobile-menu-toggle{
            transform: none !important;
        }
        .tygh-top-panel{
            background: transparent;
            height:auto;
            top:0;
        }

        .mobile-header{
            transition:background .3s ease;
        }
        .mobile-header .user-header, #cart-icon, .back-button{
            width: 35px !important;
            height: 35px !important;
            display:  flex !important;
            align-items: center;
            justify-content: center;
            padding: 9px;
            box-sizing: border-box;
            background: #fff;
            border-radius: 100%;
            margin-top: -7px;
            top: auto !important;
            float: right;
            transition:background .3s ease, box-shadow .3s ease;
        }
        .mobile-header .user-header:hover, #cart-icon:hover, .back-button:hover{
            background: #ddd;
        }
        .ty-dropdown-box__title.open, .ty-dropdown-box__title:hover{
            background: transparent !important;
            outline:0;
            border:0;
            box-shadow:none;
        }
        .user-header svg, .back-button svg{
            width: 100%;
            height:100%;
        }
        .back-button{
            margin:10px 15px;
            float:left;
        }
        .mobile-header{
            background: transparent;
            box-shadow: none;

        }
        .mobile-header svg{
            fill:#5d5d5d !important;
            transition: fill .3s ease;
        }
        .ty-search-block, #mobile-menu-toggle, .product_howto{
            display: none !important;
        }
        .active-scroll{
            background: #fff;
            box-shadow: 0 0 5px 0 rgba(0,0,0,0.3);
        }
        /*.active-scroll .user-header, .active-scroll #cart-icon, .active-scroll .back-button{*/
            /*background: transparent;*/
        /*}*/
        /*.active-scroll svg{*/
            /*fill: #808080 !important;*/
        /*}*/

      } {/literal}</style>

{if $smarty.request.dispatch == 'products.view'}
    <style>
        {literal}
        @media all and (max-width: 768px) {
            .mobile-menu-toggle {
                display: none !important;
            }

            .ty-search-block {
                display: block !important;
            }

            .mobile-header {
                background: #fff !important;
                transition: none;
            }

            .mobile-header.scroll .cm-processed-form {
                width: 100% !important;
            }

            .tygh-content {
                padding-top: 100px !important;
            }

            .mobile-header #search_input {
                background: #e8e8e8 !important;
                color: #000 !important;
            }
        }
        {/literal}
    </style>
{/if}

<script>{literal}
    if($(window).width() < 769) {
        $('.peppermint').Peppermint({
            dots: true
        });
        $('.back-button').click(function () {
            window.history.back();
        });

        var imgheight = $('#peppermint').height();
        $(window).scroll(function () {
            var scroll = $(window).scrollTop();

            if (scroll >= imgheight - 70) {
                $('.mobile-header').addClass('active-scroll');
            } else {
                $('.mobile-header').removeClass('active-scroll');
            }
        });
        var targetOffset = $(".ty-product-block__button").offset().top;

        var $w = $(window).scroll(function(){
            if ( $w.scrollTop() > targetOffset ) {
                $('.sticky__add-to-cart').addClass('active')
            } else {
                $('.sticky__add-to-cart').removeClass('active');
            }
        });
    }
    {/literal}</script>

{hook name="products:product_images"}{/hook}