{if $product.best_price_guaranteed && !$smarty.const.IS_MOBILE}
    <div style="display: flex; text-align: left; padding: 10px; box-shadow: 0 4px 20px -6px #aaa;">
        <div style="width: 12%; justify-content: center; align-items: center; display: flex; padding-right: 8px;">
            <img src="https://hhstsyoejx.gjirafa.net/gj50/bestpricebadge.png" width="350" alt="Best price badge"></div>
        <div style="width: 85%;">
            <h3 class="mb0">{__("best_price_guaranteed")}</h3>
            <p style="color: #a1a1a1;">{__("best_price_guaranteed_text")}</p>
        </div>
    </div>
{/if}
{if $product.limited_to_payment_method_id != 0}
    <div>
        <img src="images/pcb-banner.jpg" alt="">
    </div>
{else}
    {if $product._score == null && $product.price != 0 && !$a|fn_isAL}
        {assign var="starcard_six_only" value=41|in_array:$product.category_ids}
        <div class="product-banka">
            Paguaj me{if !$smarty.const.IS_MOBILE}:
            <div class="product-bank_wrapper">
                        <span class="bank">
                            <img src="images/payment/rbko.png" alt="">
{*                            <a href="https://www.raiffeisen-kosovo.com/shq/individe/produktet-dhe-sherbimet/kartelat/bonus-kartela?utm_source=gjirafa50&utm_medium=apliko-bonus-kartela"><small>[apliko këtu]</small></a>*}
                        </span>
                {assign var="raiffeisen_products" value=','|explode:{__('rbko_only_products')}}
                {assign var="raiffeisen" value=false}
                {if $rbko_offer == true || is_array($raiffeisen_products)}
                    {if $product.product_id|in_array:$raiffeisen_products}
                        {$raiffeisen = true}
                    {/if}
                {/if}
                {if !$raiffeisen}
                    <span class="bank">
                            <img src="images/payment/teb.png" class="block">
                            <a href="" style="pointer-events: none"><small>&nbsp;</small></a>
                        </span>
                {/if}
            </div>
            {else}
            <strong>Raiffeisen Bonus Kartelë</strong>
            {*        <a href="https://www.raiffeisen-kosovo.com/shq/individe/produktet-dhe-sherbimet/kartelat/bonus-kartela?utm_source=gjirafa50&utm_medium=apliko-bonus-kartela" class="ty-btn ty-btn__text" style="color: #000;padding: 0 5px;background: transparent;text-decoration: underline;font-weight: bold;font-size: 12px;" target="_blank">[APLIKO KËTU]</a>*}
            dhe <strong>TEB Starcard</strong>
            {/if}
            deri në
            {if $starcard_six_only}
                6 këste
                pa kamatë për vetëm
                <strong>
                    {($product.price/6)|string_format:"%.2f"} € në muaj
                </strong>
            {else}
                12 këste
                pa kamatë për vetëm
                <strong>
                    {($product.price/12)|string_format:"%.2f"} € në muaj
                </strong>
            {/if}

        </div>
    {/if}
{/if}