<div style="display: none">
    <svg>
        <symbol id="tv" viewBox="0 0 512 512">
            <path
                    d="M512,391V61H0v330h141v30h-40v30h40h230h40v-30h-40v-30H512z M341,421H171v-30h170V421z M30,361V91h452v270H30z"
            />
        </symbol>
        <symbol id="headset" viewBox="0 0 47 47">
            <path
                    d="M40.508,25.355h-0.311v-1.58c0.738-0.428,1.238-1.216,1.238-2.13c0-9.888-8.045-17.934-17.935-17.934   c-9.889,0-17.935,8.046-17.935,17.935c0,0.914,0.5,1.702,1.237,2.131v1.58h-0.31C2.913,25.357,0,28.685,0,32.775v2.477   c0,4.092,2.913,7.42,6.493,7.42h2.89l0.256,0.256c0.241,0.241,0.558,0.362,0.874,0.362c0.317,0,0.634-0.121,0.875-0.362   c0.285-0.285,0.382-0.673,0.33-1.044c0.214-0.302,0.342-0.669,0.342-1.067V27.21c0-1.025-0.831-1.855-1.855-1.855H9.277v-1.58   c0.736-0.428,1.236-1.216,1.236-2.13c0-7.161,5.826-12.987,12.987-12.987s12.986,5.826,12.986,12.987c0,0.914,0.5,1.702,1.236,2.13   v1.58h-0.928c-1.023,0-1.855,0.83-1.855,1.855v13.605c0,0.397,0.129,0.766,0.342,1.066c-0.051,0.371,0.046,0.758,0.33,1.044   c0.242,0.241,0.559,0.362,0.875,0.362s0.634-0.121,0.875-0.362l0.256-0.256h2.89c3.58,0,6.493-3.328,6.493-7.42v-2.476   C47.001,28.685,44.088,25.355,40.508,25.355z"
            />
        </symbol>
        <symbol id="keyboard" viewBox="0 0 512 512">
            <path
                    d="M481.596,90.907H30.404C13.353,90.907,0,104.261,0,121.311v269.378c0,16.765,13.639,30.404,30.404,30.404h451.192    c16.765,0,30.404-13.639,30.404-30.404V121.311C512,104.546,498.361,90.907,481.596,90.907z M475.515,384.608H36.485V127.392    h439.031V384.608z"
            />

            <rect x="161.14" y="310.423" width="189.72" height="36.485"/>

            <rect x="73.577" y="163.876" width="71.145" height="36.485"/>

            <rect x="171.477" y="163.876" width="71.145" height="36.485"/>

            <rect x="269.378" y="163.876" width="71.145" height="36.485"/>

            <rect x="367.278" y="163.876" width="71.139" height="36.485"/>

            <rect x="73.577" y="240.494" width="71.145" height="36.485"/>

            <rect x="171.477" y="240.494" width="71.145" height="36.485"/>

            <rect x="269.378" y="240.494" width="71.145" height="36.485"/>

            <rect x="367.278" y="240.494" width="71.139" height="36.485"/>
        </symbol>
        <symbol id="mouse" viewBox="0 0 512 512">
            <path
                    d="M256,0C156.595,0,75.726,82.14,75.726,183.099v145.807C75.726,429.865,156.595,512,256,512    c99.399,0,180.274-81.886,180.274-182.534V183.099C436.274,82.14,355.399,0,256,0z M402.366,329.466    c0,81.954-65.656,148.627-146.366,148.627c-80.705,0-146.366-66.927-146.366-149.192V183.099    c0-82.265,65.661-149.192,146.366-149.192c80.711,0,146.366,66.927,146.366,149.192V329.466z"
            />
            <path
                    d="M256,140.15c-9.364,0-16.954,7.59-16.954,16.954v59.338c0,9.364,7.59,16.954,16.954,16.954    c9.364,0,16.954-7.59,16.954-16.954v-59.338C272.954,147.74,265.364,140.15,256,140.15z"
            />
        </symbol>
    </svg>
</div>

<div class="pbl_banner">
    <div class="pbl_img">
        <div class="container">
            <h4>Nderto tani</h4>
            <h1>Gaming PC sipas deshires</h1>
            <h2>Zgjedh te gjitha pjeset vete</h2>
            <h3>Zgjedh te gjitha pjeset vete</h3>
            <a href="{"builder.create"|fn_url}">NDERTO</a>
        </div>
    </div>
</div>

{if $sets}
    <div class="pre_build pt40 pb40">
        <div class="container flexing">
            {foreach from=$sets item='set'}
                <div class="pre_item">
                    <div class="pri_img">
                        <div class="flexing">
                            {foreach from=$set.images item='images' key='key'}
                                {if $key == 0}
                                    <div id="left" style="width: 25%; display: flex; flex-direction: column;">
                                        {foreach from=$images item='image'}
                                            <img style="all: unset; width: 80%; padding: 10px;" src="https://hhstsyoejx.gjirafa.net/gj50/img/{$image}/thumb/0.jpg"/>
                                        {/foreach}
                                    </div>
                                {/if}
                                {if $key == 1}
                                    <div id="mid" style="width: 50%;">
                                        {foreach from=$images item='image'}
                                            <img style="all: unset; width: 100%;" src="https://hhstsyoejx.gjirafa.net/gj50/img/{$image}/thumb/0.jpg"/>
                                        {/foreach}
                                    </div>
                                {/if}
                                {if $key == 2}
                                    <div id="right" style="width: 25%; display: flex; flex-direction: column;">
                                        {foreach from=$images item='image'}
                                            <img style="all: unset; width: 80%; padding: 10px;" src="https://hhstsyoejx.gjirafa.net/gj50/img/{$image}/thumb/0.jpg"/>
                                        {/foreach}
                                    </div>
                                {/if}
                            {/foreach}
                        </div>
                    </div>
                    <div class="pri_content">
                        <h3>{$set.name}</h3>
                        <ul class="text-muted mb10">
                            {foreach from=$set.features item="feature"}
                                <li>
                                    <strong>{$feature.feature}:</strong>
                                    <span>{$feature.value}</span>
                                </li>
                            {/foreach}
                        </ul>
                        {if $set.accessories}
                            <div class="pri_acces">
                                <h4>Aksesorë tjerë:</h4>
                                {foreach from=$set.accessories item='accessory' key='key'}
                                    <svg>
                                        <use xlink:href="#{$key}"></use>
                                    </svg>
                                {/foreach}
                            </div>
                        {/if}
                        <a href="{"builder.create&load={$set.id}"|fn_url}">Customize this pc</a>
                    </div>
                </div>
            {/foreach}
        </div>
    </div>
{/if}

<style>
    body,
    p,
    div,
    li,
    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
        color: inherit;
    }

    * {
        box-sizing: border-box;
    }

    .container-fluid {
        width: 100%;
        max-width: 100%;
        padding: 0;
    }

    .tygh-content {
        background: #020202;
    }

    .tygh-content > div {
        padding-bottom: 0;
    }

    /* .ty-menu__item .ty-menu__item-link {
      color: #fff;
      border-right: 1px solid #1f1f1f;
    }
    .header-grid .row-fluid:nth-child(2) {
      background: #111;
      border-top: 1px solid #1f1f1f;
      border-bottom: 1px solid #1f1f1f;
    } */
    .scroll-top-button {
        display: none !important;
    }

    @media screen and (max-width: 768px) {
        .container-fluid.content-grid {
            padding: 0 !important;
        }

        body {
            overflow-x: hidden;
        }
    }
</style>
