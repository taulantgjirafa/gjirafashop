<div class="result_control">
    <div class="res_search">
        <form class="cm-ajax" action="{""|fn_url}" name="" id="builder_search_form">
            <input type="hidden" name="result_ids" value="builder_container" />
            <input type="hidden" name="item" value="{$item}" />
            {if $filter.sort}
                <input type="hidden" name="filter[sort]" value="{$filter.sort|http_build_query}" />
            {/if}

            <input id="builder_search_box" autocomplete="off" placeholder="{__('search_products')}" type="text" name="filter[search]"
                value="{$filter.search}">
            <!-- <input id="builder_search" type="submit" name="dispatch[builder.create]" value="Search" /> -->
            <i class="ty-icon-search hidden-desktop"></i>
            <button id="builder_search" type="submit" class="hidden-phone" name="dispatch[builder.create]">
                <i class="ty-icon-search"></i>
            </button>
        </form>
    </div>
    <div class="res_tabs">
        {if $filter}
        {if $filter.sort.field != 'price'}
        {$active_filter = $filter.sort.field}
        {elseif $filter.sort.field == 'price'}
        {$active_filter = $filter.sort.order}
        {/if}
        {else}
        {$active_filter = 'popularity'}
        {/if}

        <p class="mobile_pre_active hidden-desktop"><span>{__("builder_filter_{$active_filter}")}</span> <i
                class="ty-icon-down-micro"></i></p>
        <ul class="flexing">
            <li class="{if $active_filter == 'popularity'}active{/if}"><a class="cm-ajax builder-filter" data-ca-target-id="builder_container" href="{"builder.create&item={$item}&filter[sort][field]=popularity&filter[search]={$filter.search}"|fn_url}">{__('builder_filter_popularity')}</a>
            </li>

            <li class="{if $active_filter == 'asc'}active{/if}"><a class="cm-ajax builder-filter" data-ca-target-id="builder_container" href="{"builder.create&item={$item}&filter[sort][field]=price&filter[sort][order]=asc&filter[search]={$filter.search}"|fn_url}">{__('builder_filter_asc')}</a>
            </li>

            <li class="{if $active_filter == 'desc'}active{/if}"><a class="cm-ajax builder-filter" data-ca-target-id="builder_container" href="{"builder.create&item={$item}&filter[sort][field]=price&filter[sort][order]=desc&filter[search]={$filter.search}"|fn_url}">{__('builder_filter_desc')}</a>
            </li>

            <li class="{if $active_filter == 'bestsellers'}active{/if}"><a class="cm-ajax builder-filter" data-ca-target-id="builder_container" href="{"builder.create&item={$item}&filter[sort][field]=bestsellers&filter[search]={$filter.search}"|fn_url}">{__('builder_filter_bestsellers')}</a>
            </li>
        </ul>
    </div>
</div>

<script>
    $(document).on('click', '.builder-filter', function (e) {
        let href = e.target.getAttribute('href');
        window.history.pushState('', '', href);
    });

    $(document).on('click', '#builder_search', function (e) {
        let params = $('#builder_search_form').serialize();
        params = params.replace('result_ids=builder_container', '');
        window.history.pushState('', '', window.location.href + params);
    });
</script>