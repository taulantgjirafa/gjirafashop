<script>
    $(document).ajaxComplete(function () {
        document.querySelector('body').classList.remove('build_list_active');
        var allFt = document.querySelectorAll('.build_extra_ft li').length,
            activeFt = document.querySelectorAll('.build_extra_ft .active').length,
            result = activeFt / allFt,
            newnumber = parseInt(result.toFixed(2).replace(".", ""), 10);

        // document.querySelector('.fp_bar span').style.width = newnumber + '%';
        var message;

        if (newnumber > 1) {
            message = "You're almost there!";
            // document.querySelector('.fp_msg').innerHTML = message;
        }
        if (newnumber > 99) {
            message = "You did it you have the perfect build!";
            // document.querySelector('.fp_msg').innerHTML = message;
        }

        if (window.outerWidth < 768) {
            var tabs = document.querySelectorAll('.res_tabs li');
            var filterMobileBtn = document.querySelector('.mobile_pre_active');

            document.querySelector('.pb_parts_overlay').onclick = function () {
                document.querySelector('body').classList.add('build_list_active');
            
            };
            document.querySelector('.build_overlay').onclick = function () {
                document.querySelector('body').classList.remove('build_list_active');
            };

            for (var i = 0; i < tabs.length; i++) {
                tabs[i].onclick = function () {
                    this.parentElement.prepend(this);
                    var activeText = this.textContent;
                    filterMobileBtn.querySelector('span').textContent = activeText;
                    this.parentElement.classList.remove('active_filter');
                }
            }
            filterMobileBtn.onclick = function () {
                document.querySelector('.res_tabs ul').classList.toggle('active_filter');
            };


            var ignoreClickOnMeElement = document.querySelector('.res_search');

            document.addEventListener('click', function (event) {
                var isClickInsideElement = ignoreClickOnMeElement.contains(event.target);
                var searchBtn = document.querySelector('.res_search #builder_search_box');
                if (!isClickInsideElement) {
                    searchBtn.parentElement.classList.remove('active_search');
                    document.querySelector('#builder_search_box').blur();
                } else {
                    searchBtn.parentElement.classList.add('active_search');
                    document.querySelector('#builder_search_box').focus();
                }
            });
        }
    });
</script>

<script>
    var allFt = document.querySelectorAll('.build_extra_ft li').length,
        activeFt = document.querySelectorAll('.build_extra_ft .active').length,
        result = activeFt / allFt,
        newnumber = parseInt(result.toFixed(2).replace(".", ""), 10);

    // document.querySelector('.fp_bar span').style.width = newnumber + '%';
    var message;

    if (newnumber > 1) {
        message = "You're almost there!";
        // document.querySelector('.fp_msg').innerHTML = message;
    }
    if (newnumber > 99) {
        message = "You did it you have the perfect build!";
        // document.querySelector('.fp_msg').innerHTML = message;
    }

    if (window.outerWidth < 768) {
        var tabs = document.querySelectorAll('.res_tabs li');
        var filterMobileBtn = document.querySelector('.mobile_pre_active');

        document.querySelector('.pb_parts_overlay').onclick = function () {
            document.querySelector('body').classList.add('build_list_active');
          
        };
        document.querySelector('.build_overlay').onclick = function () {
            document.querySelector('body').classList.remove('build_list_active');
        };

        for (var i = 0; i < tabs.length; i++) {
            tabs[i].onclick = function () {
                this.parentElement.prepend(this);
                var activeText = this.textContent;
                filterMobileBtn.querySelector('span').textContent = activeText;
                this.parentElement.classList.remove('active_filter');
            }
        }
        filterMobileBtn.onclick = function () {
            document.querySelector('.res_tabs ul').classList.toggle('active_filter');
        };


        var ignoreClickOnMeElement = document.querySelector('.res_search');

        document.addEventListener('click', function (event) {
            var isClickInsideElement = ignoreClickOnMeElement.contains(event.target);
            var searchBtn = document.querySelector('.res_search #builder_search_box');
            if (!isClickInsideElement) {
                searchBtn.parentElement.classList.remove('active_search');
                document.querySelector('#builder_search_box').blur();
            } else {
                searchBtn.parentElement.classList.add('active_search');
                document.querySelector('#builder_search_box').focus();
            }
        });
    }
</script>