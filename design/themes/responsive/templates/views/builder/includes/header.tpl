<div class="pb_subhead">
    <div class="flexing">
        {if $selected_items|count}
            <a class="cm-ajax flexing flex-v-center mr15" data-ca-target-id="builder_container"
               href="{"builder.create&reset=true"|fn_url}">
                <img class="subhead_icons"
                     src="https://hhstsyoejx.gjirafa.net/storage/gjmall/banners/1hMall/f55572f5d19943dc945bba6ac5cac275_reduced-restart-icon-32261.png"
                     alt="Reseto"/>
                Reseto
            </a>
        {/if}

        {if !$order_disabled && $smarty.session.auth.user_id && $can_save_build}
            <a data-ca-target-id="save_build_dialog" class="cm-dialog-opener flex-v-center cm-dialog-auto-size"
               href="{"builder.create&save=true"|fn_url}">
                <svg class="subhead_icons" xmlns="http://www.w3.org/2000/svg" id="Capa_1"
                     enable-background="new 0 0 512.007 512.007" height="512" viewBox="0 0 512.007 512.007" width="512">
                    <g>
                        <path d="m142 143.003h139v-143c-49.379 0-107.674 0-154 0v128c0 8.271 6.729 15 15 15z"/>
                        <path d="m345 128.003v-128c-9.97 0-21.461 0-34 0v143h19c8.271 0 15-6.729 15-15z"/>
                        <path d="m127 512.003h218v-161h-218z"/>
                        <path d="m511.927 126.537c-.279-2.828-1.38-5.667-3.315-8.027-.747-.913 6.893 6.786-114.006-114.113-2.882-2.882-6.794-4.396-10.612-4.394-.789 0-3.925 0-8.995 0v128c0 24.813-20.187 45-45 45-14.028 0-186.064 0-188 0-24.813 0-45-20.187-45-45v-128c-29.589 0-49.82 0-52 0-24.813 0-45 20.187-45 45v422c0 24.813 20.187 45 45 45h52c0-10.815 0-201.797 0-210 0-24.813 20.187-45 45-45h188c24.813 0 45 20.187 45 45v210h92c24.813 0 45-20.187 45-45 .001-364.186.041-339.316-.072-340.466z"/>
                        <path d="m330 287.003h-188c-8.271 0-15 6.729-15 15v19h218v-19c0-8.271-6.729-15-15-15z"/>
                    </g>
                </svg>
                <span>Ruaj</span>
            </a>
        {/if}
    </div>
    <div class="flexing flex-v-center bp_price">
        <h4>
            {include file="common/price.tpl" value=$total}
        </h4>
        <form action="{"builder.create"|fn_url}" method="post">
            <input type="submit" value="{__('place_order')|upper}" {if $order_disabled}class="disabled" title="Plotësoni fushat e nevojshme!" disabled{/if}>
        </form>
    </div>
</div>