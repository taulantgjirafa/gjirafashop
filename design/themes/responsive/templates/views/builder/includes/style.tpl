<style>
    .tygh-content {
        background: #111;
    }

    /* .ty-menu__item .ty-menu__item-link {
        color:#fff;
        border-right:1px solid #1f1f1f;
    }
    .header-grid .row-fluid:nth-child(2) {
        background: #111;
        border-top: 1px solid #1f1f1f;
    border-bottom: 1px solid #1f1f1f;
    } */
    .scroll-top-button {
        display: none !important;
    }

    @media screen and (max-width: 768px) {
        .container-fluid.content-grid {
            padding: 0 !important;
        }

        body {
            overflow-x: hidden;
        }
    }

</style>