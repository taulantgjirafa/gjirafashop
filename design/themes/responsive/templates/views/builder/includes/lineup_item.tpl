<div class="flexing flex-h-between chosen_item">
    <div class="flexing flex-v-center">
        <img src="https://hhstsyoejx.gjirafa.net/gj50/img/{$product.product_id}/thumb/0.jpg" alt="">
        <span>
            <p class="max-rows mr2" style="line-height:15px;">{$product.product}</p>
            <h4>
                {$product.amount} × {include file="common/price.tpl" value=$product.price}
            </h4>
        </span>
    </div>
    <a class="cm-ajax remove_item" data-ca-target-id="builder_container" href="{"builder.create&item={$item}&remove=true&product_id={$product.product_id}&{$filter_query}"|fn_url}" ><i class="ty-icon-cancel "></i></a>
</div>
