{assign var="obj_id" value=$product.product_id}
{assign var="obj_id_prefix" value="`$obj_prefix``$product.product_id`"}

{if $multiple}
    {foreach from=$selected_item item='single_item'}
        {if $single_item.product_id == $product.product_id}
            {$selected = true}
        {/if}
    {/foreach}
{else}
    {if $selected_item.product_id == $product.product_id}
        {$selected = true}
    {/if}
{/if}

<div class="hs-item {if $selected}selected{/if}">
    <a href="{"products.view&product_id={$product.product_id}"|fn_url}" class="anchor-unset">
        <div class="hs-item-inner">
{*            {if $product.amount > 0}*}
{*                <span class="grid-list-icons on-sale">*}
{*                    <img src="https://gjirafa50.com/images/icons/upTo48.png" alt="">*}
{*             </span>*}
{*            {/if}*}
            {if $product.discount}
                <span class="hr-percentage">
                    -{$product.discount|string_format:"%d"}%
                </span>
            {/if}
            <div class="hs-img">
                <div style="background-image: url(https://hhstsyoejx.gjirafa.net/gj50/img/{$product.product_id}/thumb/0.jpg);"></div>

            </div>
            <div class="hs-content">
                <p class="text-muted hidden-phone hs-p-title">{$product.product|truncate:55:"..."}</p>
                <div class="hs-price flexing flex-h-between flex-v-center">
                    <div>
                        <b style="font-size:18px;">{include file="common/price.tpl" value=$product.price}</b>
                        {if $product.discount}<span class="hs-old-price">{include file="common/price.tpl" value=$product.old_price}</span>{/if}
                    </div>
                    {include file="addons/discussion/views/discussion/components/stars.tpl" stars=$product.rating|fn_get_discussion_rating}
                </div>
                <div class="hs-btns">
                    <span class="hs-add-cart d-flex align-items-center" style="width: unset;">
{*                        {if $multiple}*}
                            {if $selected}
                                ZGJEDHUR
                            {else}
                                <div class="ty-center ty-value-changer cm-value-changer value-changer__{$product.product_id}" style="background: none; border: none; display: flex; justify-content: flex-end; align-items: center;">
                                    <a class="cm-decrease ty-value-changer__decrease" style="color: #c6c6c6; background: none; padding: 0;">&minus;</a>
                                    <input type="text" size="3"
                                           name="" value="1"
                                           data-ca-min-qty="1"
                                           class="ty-value-changer__input cm-amount" style="background: none; color: #c6c6c6; width: 35px; border: 1px solid #c6c6c6;" disabled />
                                    <a class="cm-increase ty-value-changer__increase" style="color: #c6c6c6; background: none; padding: 0;">+</a>
                                </div>

                                <form class="cm-ajax" action="{""|fn_url}" method="get" name="builder_add_item_{$product.product_id}">
                                    <input type="hidden" name="result_ids" value="builder_container" />
                                    <input type="hidden" name="item" value="{$item}">
                                    <input type="hidden" name="selected_item" value="{$item}">
                                    <input type="hidden" name="product[product_id]" value="{$product.product_id}">
                                    <input type="hidden" name="product[product]" value="{$product.product}">
                                    <input type="hidden" name="product[price]" value="{$product.price}">
                                    <input type="hidden" name="product[amount]" value="1">
                                    <input type="hidden" name="filter_query" value="{$filter_query}">

                                    <input type="submit" name="dispatch[builder.create]" value="ZGJEDH" style="all: unset; padding: 10px 15px;" />
                                </form>

{*                                <a class="cm-ajax" data-ca-target-id="builder_container" href="{"builder.create&item={$item}&selected_item={$item}&product[product_id]={$product.product_id}&product[product]={$product.product}&product[price]={$product.price}&product[amount]=1{$filter_query}"|fn_url}" id="value-changer__{$product.product_id}">*}
{*                                    ZGJEDH*}
{*                                </a>*}
                            {/if}
{*                        {else}*}
{*                            {if $selected}*}
{*                            ZGJEDHUR*}
{*                            {else}*}
{*                                <a class="cm-ajax" data-ca-target-id="builder_container" href="{"builder.create&item={$item}&selected_item={$item}&product[product_id]={$product.product_id}&product[product]={$product.product}&product[price]={$product.price}&{$filter_query}"|fn_url}">*}
{*                                    ZGJEDH*}
{*                                </a>*}
{*                            {/if}*}
{*                        {/if}*}
                    </span>
                </div>
            </div>
        </div>
    </a>
</div>

<script>
    $(document).ajaxComplete(function () {
        $('.value-changer__{$product.product_id} .cm-decrease').on('click', function() {
            $('[name="builder_add_item_{$product.product_id}"]').find('[name="product[amount]"]').val(function(i, old_amount) {
                let value = 1;

                if (old_amount !== '' && old_amount > 1) {
                    value = parseInt(old_amount) - 1;
                    $('.value-changer__{$product.product_id} input').val(value + 1);
                }

                return value;
            });
        });

        $('.value-changer__{$product.product_id} .cm-increase').on('click', function() {
            $('[name="builder_add_item_{$product.product_id}"]').find('[name="product[amount]"]').val(function(i, old_amount) {
                let value = parseInt(old_amount) + 1;

                $('.value-changer__{$product.product_id} input').val = value - 1;

                return value;
            });
        });
    });
</script>

<script>
    $('.value-changer__{$product.product_id} .cm-decrease').on('click', function() {
       $('[name="builder_add_item_{$product.product_id}"]').find('[name="product[amount]"]').val(function(i, old_amount) {
           let value = 1;

           if (old_amount !== '' && old_amount > 1) {
               value = parseInt(old_amount) - 1;
               $('.value-changer__{$product.product_id} input').val(value + 1);
           }

           return value;
       });
    });

    $('.value-changer__{$product.product_id} .cm-increase').on('click', function() {
        $('[name="builder_add_item_{$product.product_id}"]').find('[name="product[amount]"]').val(function(i, old_amount) {
            let value = parseInt(old_amount) + 1;

            $('.value-changer__{$product.product_id} input').val = value - 1;

            return value;
        });
    });
</script>