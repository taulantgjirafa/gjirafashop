<div>

    <form class="cm-ajax" action="{""|fn_url}" name="" id="builder_search_form">
        <input type="hidden" name="result_ids" value="builder_container" />
        <input type="hidden" name="item" value="{$item}" />
        {if $filter.sort}
            <input type="hidden" name="filter[sort]" value="{$filter.sort|http_build_query}" />
        {/if}

        <input type="text" name="filter[search]" value="{$filter.search}">

        <input id="builder_search" type="submit" name="dispatch[builder.create]" value="Search" />
    </form>

    <a class="cm-ajax bangem" data-ca-target-id="builder_container" href="{"builder.create&item={$item}&filter[sort][field]=popularity&filter[search]={$filter.search}"|fn_url}">Recommended</a>

    <a class="cm-ajax bangem" data-ca-target-id="builder_container" href="{"builder.create&item={$item}&filter[sort][field]=price&filter[sort][order]=asc&filter[search]={$filter.search}"|fn_url}">Asc</a>

    <a class="cm-ajax bangem" data-ca-target-id="builder_container" href="{"builder.create&item={$item}&filter[sort][field]=price&filter[sort][order]=desc&filter[search]={$filter.search}"|fn_url}">Desc</a>

    <a class="cm-ajax bangem" data-ca-target-id="builder_container" href="{"builder.create&item={$item}&filter[sort][field]=bestsellers&filter[search]={$filter.search}"|fn_url}">Bestsellers</a>

</div>

<script>
    $(document).on('click', '.bangem', function(e) {
        let href = e.target.getAttribute('href');
        window.history.pushState('', '', href);
    });

    $(document).on('click', '#builder_search', function (e) {
        let params = $('#builder_search_form').serialize();
        params = params.replace('result_ids=builder_container', '');
        window.history.pushState('', '', window.location.href + params);
    });
</script>