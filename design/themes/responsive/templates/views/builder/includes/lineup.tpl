<ul class="build_parts_list">
    {foreach from=$items_map item='item_map' key='key'}
        <li class="{if $item == $key}builder-step-active{/if}"
            style="{if $selected_items.$key && $smarty.const.IS_MOBILE} padding-top:0; padding-bottom:0; {/if}">
            <a class="cm-ajax" data-ca-target-id="builder_container"
               href="{"builder.create&item={$key}"|fn_url}">
                {if !$selected_items.$key}
                    <img src="https://hhstsyoejx.gjirafa.net/gj50/builder/icons/builder_{$key}.png" alt="">
                {/if}

                <div>
                    <h3 class="pb_part_title">
                        {$key}. {__("builder_`$item_map.item`")}
                    </h3>
                    {if $item_map.required && !$selected_items.$key}
                        <p class="bp_required" style="color:#999;">E nevojshme</p>
                    {/if}

                    {if $selected_items.$key}
                        {if $item_map.multiple}
                            {foreach from=$selected_items.$key item='selected_product'}
                                {include file="views/builder/includes/lineup_item.tpl" product=$selected_product item=$key}
                            {/foreach}
                        {else}
                            {include file="views/builder/includes/lineup_item.tpl" product=$selected_items.$key item=$key}
                        {/if}
                    {/if}
                </div>

            </a>
        </li>
    {/foreach}
</ul>