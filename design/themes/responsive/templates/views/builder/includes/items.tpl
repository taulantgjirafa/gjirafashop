{$multiple = false}
{if $items_map.$item.multiple == true}
    {$multiple = true}
{/if}
<div class="home-section-wrapper">
    <div class="flexing flex-wrap grid-list">
        {if $products}
            {foreach from=$products item="product"}
                {include file="views/builder/includes/item.tpl" product=$product item=$item selected_item=$selected_item multiple=$multiple filter_query=$filter_query}
            {/foreach}
        {/if}
    </div>
</div>