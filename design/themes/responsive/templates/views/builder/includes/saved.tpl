{if $saved_builds}
    <div style="padding: 10px;" class="build_extra_ft">
        <h3>SAVED BUILDS</h3>
        {foreach from=$saved_builds item='saved_build'}
            <div class="flexing flex-h-between">
                <a class="cm-ajax" data-ca-target-id="builder_container"
                   href="{"builder.create&load={$saved_build.builder_set_id}"|fn_url}">{$saved_build.name}</a>
                <div>
                    <a data-ca-target-id="share_build_dialog" class="cm-dialog-opener cm-dialog-auto-size"
                       href="{"builder.share&build={$saved_build.builder_set_id}"|fn_url}" title="Share">
                        <i class="ty-icon-popup"></i>
                    </a>
                    <a class="cm-ajax remove_item" data-ca-target-id="builder_container"
                       href="{"builder.create&remove=true&build={$saved_build.builder_set_id}"|fn_url}" title="Remove">
                        <i class="ty-icon-cancel"></i>
                    </a>
                </div>
            </div>
        {/foreach}
    </div>
{/if}