<div class="build_extra_ft">
    <h3>{__('accessories')}</h3>
    <ul>
        {foreach from=$accessories_map item='accessory' key='key'}
            <li class="{if $accessory|in_array:$accessories}active{/if}">
                <svg><use xlink:href="#{$key}"></use></svg>
                <span>{$accessory}</span>
            </li>
        {/foreach}
    </ul>
    <div class="feature_progress">
        <div class="fp_bar">
            <span style="width: {$progress}%;"></span>
        </div>
        {if $progress < 100}
            <div class="fp_msg">You're almost there!</div>
        {else}
            <div class="fp_msg">You got your complete PC!</div>
        {/if}
    </div>
</div>