{include file='views/builder/includes/style.tpl'}
{include file='views/builder/includes/icons.tpl'}

<span class="build_overlay"></span>

<div class="" id="builder_container">
    {include file='views/builder/includes/header.tpl'}

    <div class="flexing flex-row-reverse">
        <div class="pb_parts">
            <span class="pb_parts_overlay"></span>
            <!-- <div>
                <a class="cm-ajax {if !$item}builder-step-active{/if}" data-ca-target-id="builder_container" href="{"builder.create"|fn_url}" >
                    <p>Konfigurimi</p>
                </a>
            </div> -->

            {include file='views/builder/includes/lineup.tpl'}

            {include file='views/builder/includes/features.tpl'}

            {include file='views/builder/includes/saved.tpl' saved_builds=$saved_builds}

        </div>

        <div id="builder_result">
            {include file='views/builder/includes/search.tpl'}
            {if !$products}

            {else}
                {include file='views/builder/includes/items.tpl' products=$products selected_item=$selected_items.$item items_map=$items_map item=$item filter_query=$filter_query}
            {/if}

            {if $item && $has_more}
                <div style="text-align: center;">
                    <form class="cm-ajax" action="{""|fn_url}" method="get" name="builder_add_item_{$product.product_id}">
                        <input type="hidden" name="result_ids" value="builder_container" />
                        <input type="hidden" name="item" value="{$item}">
                        <input type="hidden" name="offset" value="{$offset}">
                        <input type="hidden" name="filter_query" value="{$filter_query}">

                        <input type="submit" name="dispatch[builder.create]" value="SHFAQ MË SHUMË PRODUKTE" class="ty-btn__primary ty-btn" style="box-shadow: none; margin: 0 0 20px 0; border-radius: 10px;" />
                    </form>

{*                    <a class="cm-ajax ty-btn__primary ty-btn" data-ca-target-id="builder_result" href="{"builder.create&item={$item}&offset={$offset}"|fn_url}" style="box-shadow: none; margin: 0 0 20px 0;">SHFAQ MË SHUMË PRODUKTE</a>*}
                </div>
            {/if}
            <!--builder_result--></div>
    </div>
    <!--builder_container--></div>

<div id="save_build_dialog" class="hidden">
    {include file="views/builder/notifications/save_set.tpl"}
</div>

{include file='views/builder/includes/script.tpl'}

<style>
@media screen and (max-width: 768px) and (min-width: 500px) {
    .tygh-content {
        padding: 0 !important;
    }
}
</style>