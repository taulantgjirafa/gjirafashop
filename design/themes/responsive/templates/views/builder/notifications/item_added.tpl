<style>
    @media screen and (max-width: 570px) {
        .cm-notification-content.cm-notification-content-extended.notification-content-extended {
            top: 0 !important;
            margin-top: 10px;
        }

        .cm-notification-content > h1 {
            margin-bottom: 0;
        }

        .ty-product-notification__body {
            padding-top: 0;
        }

        .product-builder__disclaimer {
            display: none;
        }
    }

    .cm-notification-content > h1 {
        padding-top: 40px;
        color:#fff;
    }

    .notification-content-extended h1 {
        white-space: normal;
        text-align: center;
    }

    @media screen and (min-width: 769px) {
        .notification-content-extended {
            width: 500px;
            margin: -40px 0 0 -250px;
        }
    }
    .notification-content-extended {
    color: #fff;
    background-color: #000;
    border: 2px solid #535353;
    border-radius: 10px;
    }

    .product-builder__row {
        padding: 10px 0;
        display: flex;
        flex-direction: column;
        align-items: center;
    }

    .product-builder__row img {
        max-width: 80px;
        padding-bottom: 15px;
        border-radius: 10px;
    }

    .product-builder__row .bold {
        font-weight: bold;
    }

    .product-builder__title {
        font-size: 16px;
    }

    .product-builder__label {
        color: #e65228;
    }

    .product-builder__info {
        background-color: #e8f3fa;
        color: #20648b;
        padding: 10px 15px;
        font-weight: normal;
        justify-content: flex-start;
    }

    .product-builder__notice {
        background-color: #fdf0ee;
        color: #D94D07;
        padding: 10px 15px;
        font-weight: normal;
        justify-content: flex-start;
    }

    .product-builder__disclaimer {
        color: #777;
    }
    .product-builder__continue {
        background: #e65228;
    margin-top: 20px;
    border-radius: 10px;
    padding: 10px 15px;
    color: #fff !important;
    font-weight: 700;
    text-decoration: none !important;
    min-width: 120px;
    text-align: center;
    }
</style>

<div class="ty-product-notification__body">
    <div class="product-builder__row">
        <img src="https://hhstsyoejx.gjirafa.net/gj50/img/{$product.product_id}/thumb/0.jpg" alt="">
        <p class="product-builder__title">{$added_product.product}</p>
        {if $item}
            <a class="cm-ajax product-builder__continue" data-ca-target-id="builder_container" href="{"builder.create&item={$item}"|fn_url}">Vazhdo</a>
        {/if}
        {if $multiple}
            <a href="#" class="product-builder__continue">Shto tjeter</a>
        {/if}
    </div>
</div>

<script>
    $(document).on('click', 'body .product-builder__continue', function() {
        $('.cm-notification-close').trigger('click');
    });
</script>