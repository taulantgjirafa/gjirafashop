<div style="padding: 0 20px;">
{*    <a href="">{$build_share_url}</a>*}
    <input type="text" id="builder_share_link" value="{$build_share_url}" disabled class="ty-input-text" style="width: 90%; border-radius: 5px;">
    <a onclick="copyToClipBoard()" style="font-size: 16px; padding-left: 10px;" title="Copy"><i class="ty-icon-docs"></i></a>
</div>

<script>
    function copyToClipBoard() {
        var copyText = document.getElementById("builder_share_link");

        copyText.select();
        copyText.setSelectionRange(0, 99999);

        document.execCommand("copy");

        // alert("Copied the text: " + copyText.value);
    }
</script>