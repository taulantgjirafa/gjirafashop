{if $banners && ($banners.category_banner_left || $banners.category_banner_left)}
    <div class="side_banners">
        {if $banners.category_banner_left}
            <div class="left_banner" style="background-image: url(https://hhstsyoejx.gjirafa.net/gj50/categories/banners/{$banners.category_banner_left.image_path});">
                {if $banners.category_banner_left.position != 0}
                    <a href="{"products.view&product_id={$banners.category_banner_left.position}"|fn_url}"></a>
                {/if}
            </div>
        {/if}
        {if $banners.category_banner_left}
            <div class="right_banner" style="background-image: url(https://hhstsyoejx.gjirafa.net/gj50/categories/banners/{$banners.category_banner_right.image_path});">
                {if $banners.category_banner_left.position != 0}
                    <a href="{"products.view&product_id={$banners.category_banner_right.position}"|fn_url}"></a>
                {/if}
            </div>
        {/if}
    </div>
    <style>
        .side_banners {
            position: relative;
            width: 100%;
            top: 80px;
        }

        .right_banner, .left_banner {
            position: absolute;
            height: 600px;
            width: 170px;
            background-size: cover;
        }
        .right_banner a, .left_banner a {
            position: absolute;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
        }

        .left_banner {
            left: -170px;
            background-position: top left;
        }

        .right_banner {
            right: -170px;
            background-position: left top;
        }

        @media screen and (max-width:1024px) {
            .side_banners {
                display:none;
            }
        }
    </style>
{/if}

{if $recommended_subcats}
    {assign var="category_recommended" value=in_array($category_data.category_id, $recommended_subcats)}
{/if}

{if !$category_recommended}
    <div class="ty-mainbox-title__category clearfix">
        <div class="span16">
            <h1 class="ty-mainbox-title">
                <span class="ty-mainbox-title__left">{$category_data.category}</span>
            </h1>
            {if !$smarty.const.IS_MOBILE}
                <h1 class="ty-mainbox-title__shadow">{$category_data.category}</h1>
            {/if}
        </div>
    </div>
{/if}

{if $category_recommended}
    <style>
        .breadcrumbs-grid {
            display: none !important;
        }

        .ty-holiday-gift-banner {
            display: none !important;
        }
    </style>
    <div class="rec-banner">
        <img class="hidden-phone" src="images/recommended.jpg" alt="">
        <img class="hidden-desktop" src="images/recommended-mobile.jpg" alt="">
    </div>
{/if}

{if $category_data.category_id != 989 && $category_data.category_id != 1976}
    {include file="common/elastic_filters.tpl" elastic_filters=$search.feature_filters}
{/if}
{if $db_switch != '1'}
{literal}
    <style>
        .ty-pagination__bottom {
            display: none;
        }
    </style>
{/literal}
    <span style="display: none" class="db_switch">0</span>
{else}
    <span style="display: none" class="db_switch">1</span>
{/if}
<div class="{if $search.feature_filters && $category_data.category_id != 989 && $category_data.category_id != 1976}span12{else}span16{/if}" {if
!$search.feature_filters}style="margin-left: 0px !important;" {/if}>

    {hook name="categories:view"}
        <div id="category_products_{$block.block_id}">
            <input type="hidden" id="category_id" value="{$category_data.category_id}">
            {if $category_data.description || $runtime.customization_mode.live_editor}
                <div class="ty-wysiwyg-content ty-mb-s" {live_edit name="category:description:{$category_data.category_id}" }>
                    {$category_data.description nofilter}</div>
            {/if}

            {if $subcategories}
                {math equation="ceil(n/c)" assign="rows" n=$subcategories|count c=$columns|default:"2"}
                {split data=$subcategories size=$rows assign="splitted_subcategories"}
                {*<button type="button" class="show-subcategories">Shfaq nënkategoritë <i
                        class="ty-icon-down-micro "></i></button>*}
                <div class="subcategories-wrapper {if $category_recommended}sliderOuterWrapper{/if}">
                    {if $category_recommended}
                    <div class="justSlides">
                        <div class="wrapperToSlide">
                            {/if}
                            {if !$category_recommended}
                            <ul class="subcategories clearfix">
                                {/if}
                                {foreach from=$splitted_subcategories item="ssubcateg"}
                                    {if $category_recommended}
                                        <ul class="subcategories clearfix">
                                    {/if}
                                    {foreach from=$ssubcateg item=category name="ssubcateg"}
                                        {if $category}
                                            <li class="ty-subcategories__item bbox rounded-m__box">
                                                <input type="hidden" value="{$category.category_id}">
                                                <a href="{"categories.view?category_id=`$category.category_id`"|fn_url}">
                                                    <img data-src="{$category.image}"
                                                         class="ty-subcategories-img lazyload"
                                                         alt="{$category.category}"/>
                                                    {*{if $category.main_pair}*}
                                                    {*{include file="common/image.tpl"*}
                                                    {*show_detailed_link=false*}
                                                    {*images=$category.main_pair*}
                                                    {*no_ids=true*}
                                                    {*image_id="category_image"*}
                                                    {*image_width=$settings.Thumbnails.category_lists_thumbnail_width*}
                                                    {*image_height=$settings.Thumbnails.category_lists_thumbnail_height*}
                                                    {*class="ty-subcategories-img"*}
                                                    {*alt=$category.category*}
                                                    {*}*}
                                                    {*{/if}*}
                                                    <span {live_edit name="category:category:{$category.category_id}"
                                                    }>{$category.category}</span>
                                                </a>
                                            </li>
                                        {/if}
                                    {/foreach}
                                    {if $category_recommended}
                                        </ul>
                                    {/if}
                                {/foreach}
                                {if !$category_recommended}
                            </ul>
                            {/if}
                            {if $category_recommended}
                        </div>
                    </div>
                    <p id="leftSliderArrow" class="sliderArrows" style="display: none;"><img
                                src="images/icons/arrow.svg"
                                width="22px" style="transform: rotate(270deg);"></p>
                    <p id="rightSliderArrow" class="sliderArrows"><img src="images/icons/arrow.svg" width="22px"
                                                                       style="transform: rotate(90deg);"></p>
                    {/if}
                </div>
            {/if}

            {if $banners && $banners.category_banner_main}
                <div class="mb-3">
                    <img class="hidden-desktop" src="https://hhstsyoejx.gjirafa.net/gj50/categories/banners/{$banners.category_banner_main.0.image_path}" style="width:100%;">
                    <img class="hidden-phone" src="https://hhstsyoejx.gjirafa.net/gj50/categories/banners/{$banners.category_banner_main.1.image_path}" style="width:100%;">
                </div>
            {/if}

            <div class="active-filters-out"></div>

            {if $category_recommended_products}
                <div class="recomended_subcategories">
                    <h3>Produktet e rekomanduara</h3>
                    <ul class="rs-tags">
                        {if $category_recommended_products.products_bought}
                            <li class="tab-det-btns bought active">Më të shiturat</li>
                        {/if}
                        {if $category_recommended_products.products_viewed}
                            <li class="tab-det-btns viewed
                            {if $category_recommended_products.products_viewed && !$category_recommended_products.products_bought}
                                active
                            {/if}
                        ">Më të shikuarat
                            </li>
                        {/if}
                        {if $category_recommended_products.products_rated}
                            <li class="tab-det-btns rated
                            {if $category_recommended_products.products_rated && !$category_recommended_products.products_bought}
                                active
                            {/if}
                        ">Më të vlerësuarat
                            </li>
                        {/if}
                    </ul>
                    <ul class="rs-wrapper" id="products_bought" {if !$category_recommended_products.products_bought}
                        style="display: none;" {/if}>
                        {foreach from=$category_recommended_products.products_bought item='pitem'}
                            <li>
                                <a class="flexing flex-h-between flex-v-center" href="{"products.view&product_id={$pitem.product_id}"|fn_url}">
                                    <div class="rs-img">
                                        <img src="https://hhstsyoejx.gjirafa.net/gj50/img/{$pitem.product_id}/thumb/0.jpg"/>
                                    </div>
                                    <div class="rs-content">
                                        <h4 class="mb0">{$pitem.product}</h4>
                                        <p class="max-rows mr1" style="padding:0;">{$pitem.description|strip_tags}</p>
                                    </div>
                                    <div>
                                        <h3 class="mb0">
                                            {include file="common/price.tpl" value=$pitem.price}
                                        </h3>
                                    </div>
                                </a>
                            </li>
                        {/foreach}
                        <li class="more_item_btn " style="display:none;"><strong>Shiko më shumë produkte <i
                                        class="ty-icon-down-open"></i></strong>
                        </li>
                    </ul>
                    <ul class="rs-wrapper" id="products_viewed" {if $category_recommended_products.products_bought ||
                    !$category_recommended_products.products_viewed} style="display: none;" {/if}>
                        {foreach from=$category_recommended_products.products_viewed item='pitem'}
                            <li>
                                <a class="flexing flex-h-between flex-v-center" href="{"products.view&product_id={$pitem.product_id}"|fn_url}">
                                    <div class="rs-img">
                                        <img src="https://hhstsyoejx.gjirafa.net/gj50/img/{$pitem.product_id}/thumb/0.jpg"/>
                                    </div>
                                    <div class="rs-content">
                                        <h4 class="mb0">{$pitem.product}</h4>
                                        <p class="max-rows mr1" style="padding:0;">{$pitem.description|strip_tags}</p>
                                    </div>
                                    <div>
                                        <h3 class="mb0">
                                            {include file="common/price.tpl" value=$pitem.price}
                                        </h3>
                                    </div>
                                </a>
                            </li>
                        {/foreach}
                        <li class="more_item_btn " style="display:none;"><strong>Shiko më shumë produkte <i
                                        class="ty-icon-down-open"></i></strong>
                        </li>
                    </ul>
                    <ul class="rs-wrapper" id="products_rated" {if $category_recommended_products.products_bought ||
                    $category_recommended_products.products_viewed} style="display: none;" {/if}>
                        {foreach from=$category_recommended_products.products_rated item='pitem'}
                            <li>
                                <a class="flexing flex-h-between flex-v-center" href="{"products.view&product_id={$pitem.product_id}"|fn_url}">
                                    <div class="rs-img">
                                        <img src="https://hhstsyoejx.gjirafa.net/gj50/img/{$pitem.product_id}/thumb/0.jpg"/>
                                    </div>
                                    <div class="rs-content">
                                        <h4 class="mb0">{$pitem.product}</h4>
                                        <p class="max-rows mr1" style="padding:0;">{$pitem.description|strip_tags}</p>
                                    </div>
                                    <div>
                                        <h3 class="mb0">
                                            {include file="common/price.tpl" value=$pitem.price}
                                        </h3>
                                    </div>
                                </a>
                            </li>
                        {/foreach}
                        <li class="more_item_btn " style="display:none;"><strong>Shiko më shumë produkte <i
                                        class="ty-icon-down-open"></i></strong>
                        </li>
                    </ul>
                </div>
            {/if}

            <div id="products_search_">
                <div id="__didumean"></div>
                {if $products}
                    {assign var="title_extra" value="{__("products_found")}: `$search.total_items`"}
                    {assign var="layouts" value=""|fn_get_products_views:false:0}

                    {if $layouts.$selected_layout.template}
                        {include file="`$layouts.$selected_layout.template`" columns=$settings.Appearance.columns_in_products_list
                        show_qty=true show_add_to_wishlist=true}
                    {/if}
                {else}
                    {*{hook name="products:search_results_no_matching_found"}*}
                    {*<p class="ty-no-items">{__("text_no_matching_products_found")}</p>*}
                    {*{/hook}*}
                {/if}
            </div>
            <!--products_search_{$block.block_id}-->
        </div>
        {include file="common/load_more_products.tpl" button_visible=true button_text="SHFAQ MË SHUMË PRODUKTE"}
    {/hook}
    {hook name="products:search_results_mainbox_title"}
        {*{capture name="mainbox_title"}<span class="ty-mainbox-title__left">{$category_data['name']|ucfirst}</span>
        {/capture}*}
        {*<span class="ty-mainbox-title__right" *} {*id="products_search_total_found_{$block.block_id}">{$title_extra
        nofilter}
        <!--products_search_total_found_{$block.block_id}-->*}
        {*</span>*}

    {/hook}

    {*{literal}*}
    {*
    <script>*}
    {* $(".cm-product-filters-checkbox").each(function (e) {*}
    {* if($(this).is(":checked")) {*}
            {* $(this).closest(".ty-product-filters").show();*}
    {* $(this).closest(".ty-product-filters__switch").addClass("open");*}
    {*}*}
    {*});*}
    {*</script>*}
    {*{/literal}*}

    <img style="display:none;"/>

</div>

{if $category_recommended}
    <script type="text/javascript" src="js/tygh/category_slider.js"></script>
{/if}
<style>
    @media screen and (max-width: 768px) {
        .rs-wrapper li:not(:first-child) {
            display: none;
        }

        .rs-wrapper.activeItems li {
            display: block;
        }

        .more_item_btn {
            text-align: center;
            color: #e65228;
            padding: 13px !important;
            display: block !important;
        }
    }
</style>

<script>
    // Category recommended products - tabs

    var openListBtn = document.querySelectorAll('.more_item_btn');

    for (var i = 0; i < openListBtn.length; i++) {
        openListBtn[i].onclick = function () {
            this.parentNode.classList.toggle('activeItems')
        }
    }

    var boughtBtn = document.querySelector('.bought');
    var viewedBtn = document.querySelector('.viewed');
    var ratedBtn = document.querySelector('.rated');


    var description = document.querySelector('#products_bought');
    var details = document.querySelector('#products_viewed');
    var review = document.querySelector('#products_rated');

    var allDetBtn = document.querySelectorAll('.tab-det-btns');

    if (boughtBtn) {
        boughtBtn.onclick = function () {
            review.style.display = "none";
            details.style.display = "none";
            description.style.display = "block";
            for (var i = 0; i < allDetBtn.length; i++) {
                allDetBtn[i].classList.remove('active');
            }
            this.classList.add('active')
        }
    }

    if (ratedBtn) {
        ratedBtn.onclick = function () {
            review.style.display = "block";
            details.style.display = "none";
            description.style.display = "none";
            for (var i = 0; i < allDetBtn.length; i++) {
                allDetBtn[i].classList.remove('active');
            }
            this.classList.add('active')
        }
    }

    if (viewedBtn) {
        viewedBtn.onclick = function () {
            review.style.display = "none";
            details.style.display = "block";
            description.style.display = "none";
            for (var i = 0; i < allDetBtn.length; i++) {
                allDetBtn[i].classList.remove('active');
            }
            this.classList.add('active')
        }
    }

    var url_string = window.location.href;
    var url = new URL(url_string);
    var rel = {$smarty.cookies.rel};

    url.searchParams.set('rel', rel);
    window.history.pushState('', '', url);

    let products = document.querySelectorAll(".ty-grid-list__item--container");

    for (let i = 0; i < products.length; i++) {
        let getAllHref = products[i].getElementsByTagName("a");

        for (let j = 0; j < getAllHref.length; j++) {
            if (getAllHref[j].getAttribute("href") != null) {
                // getAllHref[j].href += "#rel=" + rel;
            }
        }
    }

    gjdmp.tr("ViewContent", {
        id: 0,
        type: "Category",
        category: $('.ty-breadcrumbs__current.gjanout-category').text(),
        user_id: $('#store_user').val(),
        result: `{$products|@array_keys|implode:','}`
    });
</script>