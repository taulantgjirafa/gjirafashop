{literal}
    <style>
        .ty-product-feature__label {
            width: 50%;
            font-weight: 500;
        }

        .ty-product-feature__value {
            width: 50%;
        }

        .ty-product-feature__value {
            margin-left: 0px;
        }
    </style>
{/literal}
{if ($smarty.request.dispatch == 'products.view' && !$smarty.const.IS_MOBILE) or  $smarty.const.DEVICE_TYPE == "tablet"}
    <div id="content_features" class="ty-product_details row-fluid">
        <div class="span6">
            <div class="ty-product_details-content">
                <p style="font-weight: bold" class="ty-subheader">Përshkrimi i produktit</p>
                <div class="ty-product_details-description">
                    {foreach from=$tabs item="tab" key="tab_id"}
                        {if $tab.tab_id == '1' || $tab.tab_id == '111'}
                            {include file=$tab.template product_tab_id=$tab.html_id}
                        {/if}
                    {/foreach}
                </div>
            </div>
        </div>
        <div class="span6">
            <p style="font-weight: bold" class="ty-subheader">Vlerësimet</p>
            <div class="ty-wysiwyg-content content-discussion" style="display: block;">
                {foreach from=$tabs item="tab" key="tab_id"}
                    {assign var="product_details_in_tab" value=$product_details_in_tab|default:$settings.Appearance.product_details_in_tab}
                    {if $tab.tab_id == '7' || $tab.tab_id == '114'}
                        {include file=$tab.template product_tab_id=$tab.html_id}
                    {/if}
                {/foreach}
                {*<div class="discussion-block" id="content_discussion_block">*}
                {*<div id="posts_list_60738">*}

                {*<div class="ty-pagination-container cm-pagination-container"*}
                {*id="pagination_contents_comments_60738">*}
                {*<div class="ty-discussion-post__content ty-mb-l">*}
                {*<span class="ty-discussion-post__author">Festina</span>*}
                {*<div class="clearfix ty-discussion-post__rating">*}
                {*<span class="ty-nowrap ty-stars">*}
                {*<i class="ty-stars__icon ty-icon-star"></i>*}
                {*<i class="ty-stars__icon ty-icon-star"></i>*}
                {*<i class="ty-stars__icon ty-icon-star"></i>*}
                {*<i class="ty-stars__icon ty-icon-star"></i>*}
                {*<i class="ty-stars__icon ty-icon-star"></i>*}
                {*</span>*}
                {*</div>*}
                {*<div class="ty-discussion-post " id="post_133033">*}
                {*<div class="ty-discussion-post__message">Televizori Philips ka ekran LED 32” (80cm),*}
                {*me*}
                {*kualitet HD dhe rezolucion 1366 x 768 piksel. Ky televizor posedon teknologjinë*}
                {*për*}
                {*pamje Digital Crystal Clear dhe teknologjitë për zërim Incredible Surround,*}
                {*Clear*}
                {*Sound, Smart Sound dhe Auto Volume të cilat mundësojnë shfaqjen e imazheve me*}
                {*kualitet superior dhe prodhimin e zërit të qartë, të detajuar e kumbues. Veçori*}
                {*t*}
                {*</div>*}
                {*<span class="ty-caret-bottom"><span class="ty-caret-outer"></span>*}
                {*<span class="ty-caret-inner"></span></span>*}
                {*</div>*}
                {*</div>*}
                {*<div class="ty-discussion-post__content ty-mb-l">*}
                {*<span class="ty-discussion-post__author">Diella Aliu</span>*}
                {*<div class="clearfix ty-discussion-post__rating">*}
                {*<span class="ty-nowrap ty-stars">*}
                {*<i class="ty-stars__icon ty-icon-star"></i>*}
                {*<i class="ty-stars__icon ty-icon-star"></i>*}
                {*<i class="ty-stars__icon ty-icon-star"></i>*}
                {*<i class="ty-stars__icon ty-icon-star"></i>*}
                {*<i class="ty-stars__icon ty-icon-star-empty"></i>*}
                {*</span>*}
                {*</div>*}
                {*</div>*}

                {*NO REVIEWS*}
                {*<div class="ty-discussion-post__content ty-mb-l">*}
                {*<p class="ty-discussion-post__no-content">Asnjë postim i gjetur</p>*}
                {*</div>*}
                {*</div>*}
                {*</div>*}
                {*<div class="ty-discussion-post__buttons buttons-container">*}
                {*<a class="ty-btn cm-dialog-opener cm-dialog-auto-size ty-btn__primary " rel="nofollow"*}
                {*data-ca-target-id="new_post_dialog_60738">Shkruaj koment</a>*}
                {*</div>*}
                {*</div>*}
            </div>
        </div>
        <div class="span6">
            <div class="ty-product_details-content" id="features_list">
                {foreach from=$tabs item="tab" key="tab_id"}
                    {if $tab.tab_id == '2' || $tab.tab_id == '112'}
                        {include file=$tab.template product_tab_id=$tab.html_id}
                    {/if}
                {/foreach}
            </div>
        </div>
    </div>
{else}
    {assign var="product_details_in_tab" value=$product_details_in_tab|default:$settings.Appearance.product_details_in_tab}
    {capture name="tabsbox"}
        {foreach from=$tabs item="tab" key="tab_id"}
            {if $tab.show_in_popup != "Y" && $tab.status == "A"}
                {assign var="tab_content_capture" value="tab_content_capture_`$tab_id`"}

                {capture name=$tab_content_capture}
                    {if $tab.tab_type == 'B'}
                        {render_block block_id=$tab.block_id dispatch="products.view" use_cache=false parse_js=false}
                    {elseif $tab.tab_type == 'T'}
                        {include file=$tab.template product_tab_id=$tab.html_id}
                    {/if}
                {/capture}

                {if $smarty.capture.$tab_content_capture|trim}
                    {if $product_details_in_tab == "N"}
                        <h3 class="tab-list-title" id="{$tab.html_id}">{$tab.name}</h3>
                    {/if}
                {/if}
                <div id="content_{$tab.html_id}" class="ty-wysiwyg-content content-{$tab.html_id}">
                    {$smarty.capture.$tab_content_capture nofilter}
                </div>
            {/if}
        {/foreach}
    {/capture}

    {capture name="tabsbox_content"}
        {if $product_details_in_tab == "Y"}
            {include file="common/tabsbox.tpl" content=$smarty.capture.tabsbox }
        {else}
            {$smarty.capture.tabsbox nofilter}
        {/if}
    {/capture}
{/if}