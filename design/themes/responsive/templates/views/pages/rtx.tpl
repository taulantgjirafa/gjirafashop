<main class="rtx3090">
    <div class="rtx_preorder">
        <h3>GeForce RTX™ 3080 & 3090</h3>
        <a href="https://gjirafa50.com/pjese-per-kompjutere/kartela-grafike/rtx30series">REZERVO TANI</a>
    </div>
    <section>
        <div class="rtx_img msi1"></div>
        <div class="banner_content">
            <h1>GAMINGU QË E NJEH DHE I BESON</h1>
            <h3>
                Seria e fundit ikonike e MSI GAMING, edhe njëherë sjell performancë,
                efikasitet, zhurmë të ulët, dhe estetikë të jashtëzakonshme, veçori këto
                të cilat gamerët hardcore i njohin dhe ju besojnë. Tani edhe ti mund ti
                shijosh të gjitha lojët tua favorite me një kartelë të fuqishme grafike,
                e cila qëndron e ftohtë dhe e qetë. Ashtu si ty të pëlqen.
            </h3>
            <h2>Thjeshtë, eksperienca më e Mirë GAMING që mund ta kesh.</h2>
        </div>
    </section>
    <section>
        <div class="rtx_img msi2">
            <div class="img_content">
                <h1>NDEZI DRITAT</h1>
                <h3>
                    Është koha që ti vendosësh ngjyrat e ekipit tënd. Sinkronizo ndriçimin
                    me pajisjet tjera elektronike, për të sjell spektakël dritash, dhe për
                    ta ngritur atë në tjetër nivel.
                </h3>
                <a href="https://gjirafa50.com/pjese-per-kompjutere/kartela-grafike/rtx30series" class="pre_order_button"
                >REZERVO TANI</a
                >
            </div>
        </div>
    </section>
    <section>
        <div class="slider_container">
            <div class="slider_wrapper">
                <div class="slider_mover">
                    <div class="slider_item">
                        <div
                                class="slider_image"
                                style="
                background-image: url(https://hhstsyoejx.gjirafa.net/storage/gjmall/banners/1hMall/a2f06d3ac3154d5fa3eee40809419aa3_reduced-c2.jpg);
              "
                        >
                            <div class="slider_content">
                <span class="container">
                  <h1 style="color: #76b900">SERIA GEFORCE RTX™ 30</h1>
                  <h2>TEKNOLOGJIA E FUNDIT</h2>
                  <h4>
                    Seria e kartelave grafike GeForce RTX™ 30 sjell performancën
                    e fundit për gamerë dhe krijues. Të fuqizuara nga
                    arkitektura RTX e gjeneratës së dytë të NVIDIAs, me bërthama
                    RT të reja, bërthama Tensor, dhe multiprocesorë streaming
                    për grafikën më realistike dhe veçoritë më të reja të AI.
                  </h4>
                  <a
                          href="https://gjirafa50.com/pjese-per-kompjutere/kartela-grafike/rtx30series"
                          class="pre_order_button"
                  >REZERVO TANI</a
                  >
                </span>
                            </div>
                        </div>
                    </div>
                    <div class="slider_item">
                        <div
                                class="slider_image"
                                style="
                background-image: url(https://hhstsyoejx.gjirafa.net/storage/gjmall/banners/1hMall/dda056cf5ec74313a600eff1d320861b_reduced-c3_rev.jpg);
              "
                        >
                            <div
                                    class="slider_content"
                                    style="text-align: right; justify-content: flex-end"
                            >
                <span class="container">
                  <h1>RTX. IT'S ON.</h1>
                  <h2 style="color: #76b900">RAY TRACING DHE AI</h2>
                  <h4>
                    Përjeto sot fuqinë më të madhe si kurrë më parë, me
                    saktësinë vizuale të ray tracing në kohë reale, dhe
                    performancën e fundit të AI, të fuqizuar nga DLSS. RTX.
                    Është On.
                  </h4>
                  <a
                          href="https://gjirafa50.com/pjese-per-kompjutere/kartela-grafike/rtx30series"
                          class="pre_order_button"
                  >REZERVO TANI</a
                  >
                </span>
                            </div>
                        </div>
                    </div>
                    <div class="slider_item">
                        <div
                                class="slider_image"
                                style="
                background-image: url(https://hhstsyoejx.gjirafa.net/storage/gjmall/banners/1hMall/f3bfe6914f9540b3b54d41a6ae9216f5_reduced-c1.jpg);
              "
                        >
                            <div class="slider_content">
                <span class="container">
                  <div class="pb10">
                    <h4>2ND GENERATION</h4>
                    <h3>RT CORES</h3>
                    <h4>2X THROUGHPUT</h4>
                  </div>
                  <div class="pb10">
                    <h4>3RD GENERATION</h4>
                    <h3>TENSOR CORES</h3>
                    <h4>UP TO 2X THROUGHPUT</h4>
                  </div>
                  <div class="pb10">
                    <h4>NEW</h4>
                    <h3>SM</h3>
                    <h4>2X FP32 THROUGHPUT</h4>
                  </div>
                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<style>
    .container-fluid {
        width: 100%;
        max-width: 100%;
        padding: 0;
    }
    section {
        margin-top: 0;
    }
    .tygh-content {
        background: #000;
    }
    h1 {
        font-size: 34px;
    }
    @media screen and (max-width: 768px) {
        h1 {
            font-size: 29px;
        }
    }
</style>

<script>
    if (window.outerWidth > 800) {
        document.onscroll = function () {
            var scrollNum = window.pageYOffset / 7;
            document.querySelector(".banner_content").style.backgroundPosition =
                "center " + scrollNum + "px";
        };
    }
</script>
