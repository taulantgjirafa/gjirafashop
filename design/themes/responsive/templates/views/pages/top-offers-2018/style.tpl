{literal}<style>body, html {
        font-family: Roboto, sans-serif
    }

    .container-fluid {
        max-width: 100%;
        padding: 0
    }

    .ty-menu__item .ty-menu__item-link {
        background: 0 0;
        color: #eee;
        border-color: #222
    }

    img.stockimage {
        position: absolute;
        left: 10px;
        top: 10px
    }

    .discount-label {
        position: absolute;
        bottom: 0;
        left: 0;
        width: 100%;
        padding: 20px 10px 15px;
        background: #2d2729;
        color: #fff;
        font-weight: 800;
        font-size: 20px;
        box-sizing: border-box;
        -webkit-clip-path: polygon(0 16%, 100% 0, 100% 100%, 0 100%);
        clip-path: polygon(0 16%, 100% 0, 100% 100%, 0 100%)
    }

    #bf-countdown, .discount-label span {
        font-weight: 300
    }

    .tygh-content {
        background-color: #2b2b2b;
    }

    #bf-heading{
        font-size: 60px;
        font-weight: 900 !important;
        margin-top: -70px;
    }

    .free-p{
        line-height: 1;
        background: #e65228;
        margin: 10px 0 0 0;
        color: #fff;
        padding: 10px 0;
        -webkit-clip-path: polygon(0 12%, 100% 0, 100% 100%, 0 92%);
        clip-path: polygon(0 12%, 100% 0, 100% 100%, 0 92%);
        height: 40px;
    }

    @media screen and (max-width: 768px) {
        .content {
            padding: 0 10px 15px
        }

        #bf-heading{
            font-size: 40px;
            margin-top: -70px;
            letter-spacing: -1px;
            font-weight:300 !important;
        }

        .tygh-content .container {
            padding: 0
        }

        .ty-grid-list__item--container {
            margin-right: -2px !important
        }

        .ty-grid-list__image {
            min-height: 240px
        }

        .tygh-content {
            background-size: contain
        }
    }

    .ty-discount-label {
        background: #111
    }

    .product-masonry.grid-list {
        position: relative
    }



    .ty-grid-list__item-details {
        padding: 10px
    }


    .ty-grid-list__item--container {
        text-align: center;
        background: #fff;
        border: 1px solid #eee;
        box-sizing: border-box
    }

    .ty-grid-list__image img {
        position: absolute;
        top: 50%;
        transform: translatey(-50%);
        left: 0;
        right: 0;
        margin: auto;
        max-width: 80%;
        max-height: 80%
    }

    #bf-countdown {
        font-size: 25px;
        text-align: center;
        margin-bottom: 20px;
        margin-top: -50px;
        color: #822e44
    }

    div#bf-countdown span {
        font-weight: 800;
        font-size: 30px;
        color: #fff
    }

    h3.title {
        height: 38px;
        overflow: hidden;
        margin: 0;
        font-weight: 400
    }

    span.o-price {
        text-decoration: line-through;
        font-size: 18px;
        opacity: .5;
        letter-spacing: -.3px;
        font-weight: 400
    }

    h2.price {
        font-weight: bolder;
        font-size: 30px;
        line-height: 1;
        letter-spacing: -2.5px
    }

    #rbko-title {
        position: relative;
        font-weight: 400;
        text-align: center;
        margin-bottom: 30px
    }

    #rbko {
        display: block;
        margin: 0 auto 25px auto;
    }

    #bf-heading {
        margin-bottom: 50px;
        color: #fff;
        font-weight: 300;
    }

    .product-masonry.grid-list {
        padding: 50px 0;
    }

    .product-masonry.grid-list:before {
        content: '';
        position: absolute;
        left: -50%;
        top: 0;
        width: 200%;
        height: 100%;
        display: block
    }

    #oneplusone:before{
        background:#222;
    }

    #discount10:before{
        background: #09caf5;
    }

    #upto70:before{
        background:#2b2b2b;
    }

    #begamer:before{
        background:#191919;
    }

    #stock:before{
        background:#c03c27;
    }


    #buildpc:before{
        background:#c81c1c;
    }

    #feboffer:before{
        background:#231f20;
    }

    #laptopweekend:before{
        background:#75daa0;
    }

    #outlet:before{
        background:#e65228;
    }
.span16{
    margin:0  !important;
}


</style>{/literal}<style>
.bf-cover {ldelim}background-image: url(https://hhstsyoejx.gjirafa.net/gj50/landings/{$key}/cover.jpg);font-family: 'Pirulen Rg';{rdelim}
    .product.small .save-percentage, .product.medium .save-percentage {ldelim}background:#{$landing_config.percentage_bg_color};{rdelim}
    .product.small .save-percentage span, .product.medium .save-percentage span {ldelim}color:#{$landing_config.percentage_color};{rdelim}
    .product.small .bnow, .product.medium .bnow {ldelim}background:#{$landing_config.buynow_color};font-family: 'Pirulen Rg';{rdelim}
    .bf-cover, .bf-bottom {ldelim}background-color: #{$landing_config.bg_color};{rdelim}
    #bf-countdown span {ldelim}color: #{$landing_config.header_color};{rdelim}
    .bf-cover, #bf-vid{ldelim}background-size:cover;{rdelim}
    .bf-cover{ldelim}display: flex;flex-direction: column;justify-content: center;align-items: center;text-align: center;cursor:normal;{rdelim}
    .header-grid .row-fluid:nth-child(2){ldelim} background: #191818;{rdelim}
    .ty-menu__item .ty-menu__item-link {ldelim}background: #0000;{rdelim}</style>