<link rel="stylesheet" href="https://hhstsyoejx.gjirafa.net/gj50/gjirafa50_teb/teb.css">

<div class="teb-wrapper">
    <div class="teb-background">

        <div class="container-fluid">
            <div class="teb-intro">
                <div class="teb-logo">
                    <img src="https://hhstsyoejx.gjirafa.net/gj50/gjirafa50_teb/teb-logo-bnp.svg" alt="TEB Bank">
                </div>
                <p>
                    Kartela <b>Starcard</b>
                    <br>
                    <small>Një kartelë, shumë përfitime!</small>
                </p>
            </div>

            <div class="teb-topProduct" >
                <div class="topProduct-wrapper">
                    <div class="topProduct-inner">
                        {assign var="main_prod_stock" value=($products[204121]['amount'] + $products[204121]['czc_stock'] + $products[204121]['czc_retailer_stock'])}
                        {if $main_prod_stock == 0}
                            <span class="teb-product-soldout" style="background-size: 150px;"></span>
                        {else}
                            <div class="topProduct-count">
                                <span class="count-inner">
                                    Vetëm
                                    <br>
                                    <strong>{$main_prod_stock}</strong>
                                </span>
                            </div>
                        {/if}
                        <div class="teb-product-inner">
                            <h1 class="topProduct-title">
                                Produkti i ditës
                            </h1>

                            <a href="https://gjirafa50.com/index.php?dispatch=products.view&product_id={$products[204121]['product_id']}">
                                <div class="teb-product-container clearfix" style="min-height: auto;">
                                    <div class="span16">

                                        <img style="max-width: 270px;" class="ty-pict cm-image" src="https://hhstsyoejx.gjirafa.net/gj50/img/{$products[204121]['product_id']}/img/0.jpg">
                                        <h3>{$products[204121]['product']} + {$products[192641]['product']}</h3>

                                    </div>
                                </div>
                                <div class="teb-product-buy clearfix">
                                    <div class="teb-product-price">
                                        <b>{$products[204121]['price']} €</b>
                                    </div>
                                    <div class="teb-product-button">
                                        <button onclick="window.location.href='/index.php?dispatch=products.view&product_id={$products[204121]['product_id']}'">Blej TANI</button>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>


    <div class="teb-divider"></div>

    <div class="container-fluid teb-wrapper-inner">
        <div class="teb-container">
            <div class="teb-headline">
                <h1 style="font-weight: lighter;color:#555;">
                    100 produkte në ofertë 1+1 si dhe 2 këste falas për blerjet me 12 këste me kredit kartelë të TEB. Gjeje kombinimin tuaj të preferuar!
                </h1>
            </div>

            <div class="grid-list">
                {assign var="iteration" value=1}
                {foreach $list as $product}<div class="teb-product-item ty-column2" {if $iteration%2}data-aos="fade-right" data-aos-delay="260" {else}data-aos="fade-left" data-aos-delay="0"{/if} data-aos-duration="300" data-aos-easing="ease" data-aos-once="true">
                    <div class="teb-product-inner ">
                        {assign var="stock" value=($products[$product[0]]['amount'] + $products[$product[0]]['czc_stock'] + $products[$product[0]]['czc_retailer_stock'])}
                        {if $stock == 0}
                            <span class="teb-product-soldout" style="background-size: 150px;border-radius:0;"></span>
                        {else}
                            <div class="topProduct-count">
                                <span class="count-inner">
                                    Vetëm
                                    <br>
                                    <b>{$stock}</b>
                                </span>
                            </div>
                        {/if}

                        {*{assign var="container" value=intval($products[$product[0]]['product_id']/8000+1)}*}
                        {*{if $container gt 5}*}
                            {*{$container=5}*}
                        {*{/if}*}
                        {assign var="azure_path" value={get_images_from_blob product_id=$product.product_id count=1}}

                        <a class="no-decoration" href="https://gjirafa50.com/index.php?dispatch=products.view&product_id={$products[$product[0]]['product_id']}">
                            <div class="teb-product-container clearfix">
                                <div class="teb-product-left">
                                    <div class="m-img" style="background-image:url({$azure_path})"></div>
                                    <h3>{$products[$product[0]]['product']}</h3>
                                </div>

                                <span class="teb-product-plus">+</span>

                                {assign var="azure_path" value={get_images_from_blob product_id=$products[$product[1]]['product_id'] count=1}}

                                <div class="teb-product-right">
                                    <div class="m-img m-img-s" style="background-image:url({$azure_path})"></div>
                                    <h4>{$products[$product[1]]['product']}</h4>
                                </div>
                            </div>

                            <div class="teb-product-buy clearfix">
                                <div class="teb-product-price">
                                    <b>{$products[$product[0]]['price']}€</b>
                                </div>
                                <div class="teb-product-button">
                                    <button onclick="window.location.href='/index.php?dispatch=products.view&product_id={$products[$product[0]]['product_id']}'">Blej tani</button>
                                </div>
                            </div>
                        </a>
                    </div>
                    </div>{$iteration = $iteration + 1}{/foreach}

            </div>
        </div>
    </div>

</div>


<script src="https://cdnjs.cloudflare.com/ajax/libs/aos/2.2.0/aos.js"></script>
<script>AOS.init();</script>