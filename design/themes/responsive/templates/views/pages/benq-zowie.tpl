<link rel="stylesheet" href="var/themes_repository/responsive/css/lib/peppermint.css">
{include file="views/pages/landing/svg-icons.tpl"}
<main class="">
<div class="msi-bg bqz-bg">
    <div class="hidden-phone">
        <img id="zowie" class="bqz-banners" src="images/pages/zowie-banner.jpg" />
        <img id="benq"  class="bqz-banners" src="images/pages/benq-banner.jpg" />
    </div>
</div>
<div class="bqz-header">
    <div class="flexing h100">
        <div class="flex-grow w50 zowie flexing flex-end flex-column bnqz">
            <img class="hidden-desktop" src="images/zowie.png" />
            <div class="bqz-categoreis">
                <ul>
                    {foreach from=$zowie_categories item=category key=key}
                    <a href="{"pages.benq-zowie&kategoria=`$key`-zowie"|fn_url}">
                        <li {if $active_category == $key && $brand == 'zowie'}class="zowie-active"{/if}>
                            <svg><use xlink:href="#{$key}"></use></svg>
                            <span>{$category}</span>
                        </li>
                    </a>
                    {/foreach}
                <ul>
            </div>
        </div>
        <div class="flex-grow w50 benq flexing  flex-column bnqz" >
        <img class="hidden-desktop" style="width:70px; position:relative; top: 4px;" src="images/benq.png" />
            <div class="bqz-categoreis">
                <ul>
                    {foreach from=$benq_categories item=category key=key}
                    <a href="{"pages.benq-zowie&kategoria=`$key`-benq"|fn_url}">
                        <li {if $active_category == $key && $brand == 'benq'}class="benq-active"{/if}>
                            <svg><use xlink:href="#{$key}"></use></svg>
                            <span>{$category}</span>
                        </li>
                    </a>
                    {/foreach}
                <ul>
            </div>
        </div>
    </div>
     <div class="flexing flex-h-between bnqz-buttons">
        <div class="flex-grow zowie-button {if $brand == 'zowie'}active{/if}"><a href="{"pages.benq-zowie&brendi=zowie"|fn_url}"></a>Zowie</div>
        <div class="flex-grow benq-button {if $brand == 'benq'}active{/if}"><a href="{"pages.benq-zowie&brendi=benq"|fn_url}"></a>Benq</div>
        <span class="nipp {if $brand == 'zowie'}nippleft{else}nippright{/if}" style="{if $brand == 'zowie'}right: 50%; left: 0;{else}right: 0; left: 50%;{/if}"></span>
    </div>
</div>

<div class="pd-wrapper ss-wrapper">
{include file="views/pages/landing/products.tpl"}
</div>

</main>

<script type="text/javascript">
    function loadedContent() {
        $('.peppermint').css('opacity', '1');
        $('.msi-loader').css('display', 'none');
    }

    function goto(url) {
        window.location = url;
    }
</script>
<script type="text/javascript" onload="loadedContent();" src="js/lib/Peppermint.js?ver=4.3.9"></script>
<script>
var zowieBtn = document.querySelector('.zowie-button'),
    benqBtn = document.querySelector('.benq-button'),
    switcher = document.querySelector('.nipp');
    
    zowieBtn.addEventListener('click', function(){
        switcher.style.left = "0";
        setTimeout(function(){
            switcher.style.right = "50%";
            switcher.classList.remove('nippright');
            switcher.classList.add('nippleft');
        }, 0);
     
    });
       benqBtn.addEventListener('click', function(){
           switcher.style.right = "0";
        setTimeout(function(){
            switcher.style.left = "50%";
            switcher.classList.remove('nippleft');
            switcher.classList.add('nippright');
        }, 0);
        
    });

    $('.peppermint').Peppermint({
        dots: true,
        speed: 500,
        slideshowInterval: 5000,
        stopSlideshowAfterInteraction: true,
        onSetup: function(n) {

        }
    });
    $('body').on('click', '.right-arr', function(){
        var slider = $(this).parent().parent().find('.peppermint');
        slider.data('Peppermint').next();
    });
    $('body').on('click', '.left-arr', function(){
        var slider = $(this).parent().parent().find('.peppermint');
        slider.data('Peppermint').prev();
    });
    $('body').on('click', '.expandDet', function(){
        $(this).prev().toggleClass('detOpen');
        $(this).toggleClass('rotSvg');
    });

    document.title = "Blej online produkte BenQ dhe Zowie - Gjirafa50";
</script>

<style>
    .tygh-content {
        background: #000;
    }

    .load_more_category {
        background: #000;
    }

    #load_more_infinite {
        box-shadow: 0 4px 10px -4px #000;
    }

    .sold-out-msi {
        position: absolute;
        width: 100%;
        height: 100%;
        background-color: rgba(255,255,255,0.5);
    }

    .sold-out-msi h4 {
        border-radius: 3px;
        box-shadow: 1px 1px 2px 0 #bebebe;
        color: #111;
        width: auto;
        transform: none;
        font-weight: bold;
        background: none;
        border: none;
        box-shadow: none;position: absolute;
        top: 45%;
        left: 40%;
    }

    .msi-loader {
        background: url(design/themes/responsive/media/images/icons/loader.svg) no-repeat;
        background-size: contain;
        width: 50px;
        height: 50px;
        position: absolute;
        top: 40%;
        left: 40%;
    }

    .pd-wrapper .pd-image .peppermint-dots {
        margin-top: 35px;
    }

    .pd-grid .grid-single.empty {
        background: #000;
        border: none;
    }

    .zowie-active {
        border: 2px solid #c10033 !important;
    }

    .benq-active {
        border: 2px solid #6e288b !important;
    }

    #empty_category_container {
      background-color: #000;
      color: white;
      text-align: center;
      padding: 100px 0;
    }

    @media screen and (max-width: 768px) {
        .ss-wrapper {
            border-radius: 0;
            padding-top: 0;
        }

        .bqz-header {
            margin-bottom: -5px;
        }

        .tygh-content {
            padding-top: 0px;
        }

        .bqz-categoreis {
            width: 90%;
        }
    }
</style>