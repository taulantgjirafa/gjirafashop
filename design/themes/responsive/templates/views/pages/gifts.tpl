<main class="flexing gift-wrapper">
    <div class="card-wrapper w50">
        <div class="card-colors hidden-desktop">
            <span class="active" style="background:linear-gradient(to top, #ca471b, #ffa081);"></span>
            <span style="background:linear-gradient(to bottom, #f7bb97, #dd5e89);"></span>
            <span style="background:linear-gradient(to bottom, #2c3e50, #fd746c);"></span>
            <span style="background:linear-gradient(to bottom, #00dbde, #fc00ff);"></span>
            <span style="background:linear-gradient(to bottom, #eb3349, #f45c43);"></span>
            <span style="background:linear-gradient(to bottom, #2c3e50, #3498db);"></span>
            <span style="background:linear-gradient(to bottom, #dce35b, #45b649);"></span>
            <span style="background:linear-gradient(to bottom, #ff5f6d, #ffc371);"></span>
            <span style="background:linear-gradient(to bottom, #464646, #0e0e0e);"></span>
            <span style="background:linear-gradient(to bottom, #0e0e0e, #e15726);"></span>
            <span style="background:linear-gradient(to bottom, #ffd89b, #19547b);"></span>
            <span style="background:linear-gradient(to bottom, #808080, #3fada8);"></span>
        </div>
        <div id="card">
            <span class="c-background">
            <span class="colored"></span>
            </span>
            <div class="c-header">
                <img src="images/logos/gj50logoWhite.svg" />
                <h1 class="c-amount">
                    <span class="c-coupon-text">Kupon me vlerë</span>
                    <span class="c-priceWrapper"><span class="active-price">20€</span></span>
                </h1>
            </div>
            <div class="c-body ">
                <h1 class="c-name "><span class="c-dummy opacity-active">Nga:</span> <b class="c-person opacity-active" data-placeholder="Personi">Personi</b></h1>
                <h1 class="c-msg opacity-active" data-placeholder="Mesazhi juaj do të shfaqet këtu...">Mesazhi juaj do të shfaqet këtu...</h1>
            </div>
            <div class="c-footer">
                <h3>Kodi kuponit: <span class="c-code">******</span></h4>
            </div>
        </div>
    </div>
    <div class="form-wrapper w50">
        <div class="gc-form mt20">
            
            <div class="chose-gcm">
                <h3>1. Zgjedh metoden e kuponit digjitale apo fizike</h3>

                <div class="flexing">
                    <div data-name="digitalMethod" class="gift-method active">
                        <img src="https://cdn0.iconfinder.com/data/icons/shopping-and-commerce-4-4/48/161-512.png" />
                        <div>
                            <h4 class="mb0">Kupon digjitale</h4>
                            <p class="text-muted">Kuponi digjital dergohet ne mënyrë digjitale me e-mail</p>
                        </div>
                    </div>
                    <div data-name="cardMethod" class="gift-method">
                        <img src="https://cdn3.iconfinder.com/data/icons/flat-design-hands-icons/128/40-512.png" />
                        <div>
                            <h4 class="mb0">Kupon kartel</h4>
                            <p class="text-muted">Kuponi fizik në form te kartës </p>
                        </div>
                    </div>
                </div>
            </div>
            
            <h3>Plotsoni fushat e vijushme me posht</h3>
            <ul id="sending-methods" class="digitalMethod">
                <li>
                    <div class="form-title">
                        <h4>Shuma e paras</h4>
                    </div>
                    <div class="input-content">
                        <div class="gc-price-tags flexing flex-v-center">
                            <span class="active">10€</span>
                            <span>20€</span>
                            <span>50€</span>
                            <span>100€</span>
                            <span>200€</span>
                            <span>500€</span>
                            <div class="gc-price-input">
                                <input id="priceInput" type="number" min="5" max="1500" step="any" placeholder="5€ deri 1500€" />
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="form-title">
                        <h4>Nga</h4>
                    </div>
                    <div class="input-content">
                        <input class="card-text" id="c-person" type="text" placeholder="Personi që dërgon dhuratën" />
                        <div class="anonim inputCheckBox">
                            <input id="anonymous" type="checkbox" />
                            <label for="anonymous">Anonim?</label>
                        </div>
                    </div>
                </li>
                <li class="digitalEle">
                    <div class="form-title">
                        <h4>Tek</h4>
                    </div>
                    <div class="input-content">
                        <input type="text" placeholder="Emaili i personit" />
                    </div>
                </li>
                <li class="cardEle">
                    <div class="form-title">
                        <h4>Tek</h4>
                    </div>
                    <div class="input-content flexing">
                        <input type="text" id="c-adress" placeholder="Adresa e transportit" />
                        <div class="merreVet inputCheckBox">
                            <input id="mrvet-id" type="checkbox" />
                            <label for="mrvet-id">Marr tek zyret e Gjirafes</label>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="form-title">
                        <h4>Mesazhi</h4>
                    </div>
                    <div class="input-content">
                        <textarea maxlength="100" class="card-text" id="c-msg" style="min-height:100px;" placeholder="Mesazhi juaj"></textarea>
                        <span class="count">Gjithsej shkronja: 100</span>
                    </div>
                </li>
                <li class="hidden-phone">
                    <div class="form-title">
                        <h4>Ngjyra e kartës</h4>
                    </div>
                    <div class="input-content card-colors">
                        <span class="active" style="background:linear-gradient(to top, #ca471b, #ffa081);"></span>
                        <span style="background:linear-gradient(to bottom, #f7bb97, #dd5e89);"></span>
                        <span style="background:linear-gradient(to bottom, #2c3e50, #fd746c);"></span>
                        <span style="background:linear-gradient(to bottom, #00dbde, #fc00ff);"></span>
                        <span style="background:linear-gradient(to bottom, #eb3349, #f45c43);"></span>
                        <span style="background:linear-gradient(to bottom, #2c3e50, #3498db);"></span>
                        <span style="background:linear-gradient(to bottom, #dce35b, #45b649);"></span>
                        <span style="background:linear-gradient(to bottom, #ff5f6d, #ffc371);"></span>
                        <span style="background:linear-gradient(to bottom, #464646, #0e0e0e);"></span>
                        <span style="background:linear-gradient(to bottom, #0e0e0e, #e15726);"></span>
                        <span style="background:linear-gradient(to bottom, #ffd89b, #19547b);"></span>
                        <span style="background:linear-gradient(to bottom, #808080, #3fada8);"></span>
                    </div>
                </li>


                <li class="gift-method-txt">
                    <p class="text-muted digitalEle"><b>Keni zgjedhur kupon digjital. Dhurata do ti shkon personit ne email me daten e caktuar pasi behet pagesa.</b></p>
                    <p class="text-muted cardEle"><b>Keni zgjedhur kupon fizik. Dhuraten mund ta merni ne zyret tona apo transport ne adresen e caktuar.</b></p>
                </li>

                <li class="mt20">
                    <button class="button coupon">Blej kuponin</button>
                </li>
            </ul>
        </div>

    </div>
</main>
<script>
</script>
<script>
    var allColors = document.querySelectorAll('.card-colors span'),
        card = document.querySelector('#card'),
        allPriceTags = document.querySelectorAll('.gc-price-tags span'),
        priceWrapper = document.querySelector('.c-priceWrapper'),
        inputTexts = document.querySelectorAll('.card-text'),
        colorBackgroundWrapper = document.querySelector('.c-background'),
        anonymous = document.querySelector('#anonymous'),
        merreID = document.querySelector('#mrvet-id'),
        anonimButton = document.querySelector('.anonim'),
        giftMethod = document.querySelectorAll('.gift-method'),
        personInput = document.querySelector('#c-person'),
        msgInput = document.querySelector('#c-msg'),
        merreVet = document.querySelector('.merreVet'),
        priceInput = document.querySelector('#priceInput');



    priceInput.onkeyup = function() {
        inputPrcValue = this.value;
        setTimeout(function() {
            if (inputPrcValue < 5 || inputPrcValue > 1500) {
                document.querySelector('.c-priceWrapper').classList.add('wrongAnimated');
                setTimeout(function() {
                    document.querySelector('.c-priceWrapper').classList.remove('wrongAnimated');
                }, 1000);
                var currentPriceValue = document.querySelector('.gc-price-tags .active').innerHTML;
                document.querySelector('.c-priceWrapper span').innerHTML = currentPriceValue;
                priceInput.value = null;
                document.querySelector('.gc-price-tags').setAttribute('style', 'pointer-events:all; opacity: 1');
            }

        }, 2000);
        if (inputPrcValue > 0) document.querySelector('.gc-price-tags').setAttribute('style', 'pointer-events:none; opacity: .5');
        else document.querySelector('.gc-price-tags').setAttribute('style', 'pointer-events:all; opacity: 1');

        document.querySelector('.c-priceWrapper span').innerHTML = inputPrcValue + '€';

    };

    for (var i = 0; i < giftMethod.length; i++) {
        giftMethod[i].addEventListener('click', function() {
            currentMethod = this.getAttribute('data-name');
            document.querySelector('#sending-methods').className = currentMethod;
            card.className = currentMethod;
            for (var j = 0; j < giftMethod.length; j++) {
                giftMethod[j].classList.remove('active');
            }
            this.classList.add('active');
        });
    }

    for (var i = 0; i < allColors.length; i++) {
        allColors[i].addEventListener('click', function() {
            var allBackgrounds = document.querySelectorAll('.c-background span'),
                getColor = this.getAttribute("style"),
                newColor = document.createElement("SPAN");
            for (var j = 0; j < allColors.length; j++) {
                allColors[j].classList.remove('active');
                setTimeout(function() {
                    for (var j = 0; j < allBackgrounds.length; j++) {
                        allBackgrounds[j].remove();
                    }
                }, 300);
            }
            this.classList.add('active');
            newColor.setAttribute('style', getColor);
            colorBackgroundWrapper.appendChild(newColor);
            setTimeout(function() {
                newColor.classList.add('colored');
            });


        });
    }

    for (var i = 0; i < allPriceTags.length; i++) {
        allPriceTags[i].addEventListener('click', function() {
            var thisPrice = this.innerText,
                currentPrice = document.querySelectorAll('.c-priceWrapper span');
            for (var k = 0; k < allPriceTags.length; k++) {
                allPriceTags[k].classList.remove('active');
            }
            this.classList.add('active');
            for (var j = 0; j < currentPrice.length; j++) {
                currentPrice[j].classList.add('hidden-price');
                setTimeout(function() {
                    for (var j = 0; j < currentPrice.length; j++) {
                        currentPrice[j].remove();
                    }
                }, 300);
            }
            var newPrice = document.createElement("SPAN");
            newPrice.innerHTML = thisPrice;
            newPrice.setAttribute('class', 'newAdded-price');
            priceWrapper.appendChild(newPrice);
            setTimeout(function() {
                newPrice.classList.remove('newAdded-price');
            }, 1);

        });
    }
    for (var i = 0; i < inputTexts.length; i++) {
        inputTexts[i].onkeyup = function() {
            var classId = this.getAttribute('id'),
                personContent = this.value,
                charLength = personContent.length,
                isFocused = (document.activeElement === msgInput);

            document.querySelector('.' + classId).innerHTML = personContent;
            document.querySelector('.' + classId).classList.remove('opacity-active');
            document.querySelector('.c-dummy').classList.remove('opacity-active');

            if (isFocused) {
                document.querySelector('.count').innerHTML = "Gjithsej shkronja: " + (100 - this.value.length);
            }

            if (charLength == 0) {
                var personPlaceholder = document.querySelector('.' + classId).getAttribute('data-placeholder');
                document.querySelector('.' + classId).innerHTML = personPlaceholder;
                document.querySelector('.' + classId).classList.add('opacity-active');
            }
            if (personInput.value.length == 0) {
                document.querySelector('.c-dummy').classList.add('opacity-active');
            }
        };
    }
    //     msgInput.onkeyup = function () {
    //   document.querySelector('.count').innerHTML = "Gjithsej shkronja: " + (100 - this.value.length);
    // };

    anonimButton.onclick = function() {
        document.getElementById('c-person').classList.add('disabled');
        document.querySelector('.c-name').classList.add('cname-hidden');
        // setTimeout(function(){
        // document.querySelector('.c-name').style.display = "none";
        // },400);
        if (anonymous.checked == false) {
            document.getElementById('c-person').classList.remove('disabled');
            // document.querySelector('.c-name').style.display = "block";
            document.querySelector('.c-name').classList.remove('cname-hidden');
        }

    };
    merreVet.onclick = function() {
        document.getElementById('c-adress').classList.add('disabled');
        if (merreID.checked == false) {
            document.getElementById('c-adress').classList.remove('disabled');
        }
    };
</script>