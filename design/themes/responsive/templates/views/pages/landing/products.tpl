{assign var="is_al" value=$al|fn_isAL}

{if $no_products}
    {assign var="empty_color_counter" value=1}
    {foreach from=$categories item=category key=key}
        {if $smarty.get.kategoria == $key}
            {$empty_color = $colors.$empty_color_counter}
            {$empty_category = $categories.$key}
        {/if}
        {assign var="empty_color_counter" value=$empty_color_counter + 1}
    {/foreach}

    {if !$empty_color}
        {$empty_color = 'cb1e24'}
    {/if}

    <h1 id="empty_category_container">Kategoria <span style="color: #{$empty_color};">{$empty_category}</span> për momentin nuk ka asnjë produkt, ju lutem kthehuni përsëri.</h1>
{else}

<div class="pd-list">
    {if isset($priority_products)}
        {foreach from=$products item=product}

        {if $smarty.session.auth.vip && !$is_al && $product.vip_price != null}
            {if $product.old_price < $product.vip_price}
                {$product.old_price = $product.price}
            {/if}
            {if $product.price > $product.vip_price}
                {$product.price = $product.vip_price}
            {/if}
        {/if}

            {if in_array($product.product_code, $priority_products)}
            <div class="pd-single flexing flex-h-between flex-column-xs" {if $is_al && !$product.price_al} style="display: none;"{/if}>
                <div class="pd-image flexing">
                    {if $product.discount}
                    <span class="pages-discount-label round">-{$product.discount|round}%</span>
                    {/if}
                    {if $product.image_count > 1}
                    <div class="arrows hidden-phone">
                        <span class="left-arr">
                            <svg><use xlink:href="#svg-left"></use></svg>
                        </span>
                        <span class="right-arr">
                            <svg><use xlink:href="#svg-right"></use></svg>
                        </span>
                    </div>
                    {/if}
                    <div class="peppermint peppermint-inactive" id="peppermint" style="opacity: 0;" onclick="goto('{"products.view&product_id={$product.product_id}"|fn_url}?utm_content=landing')">
                        {for $count=0 to $product.image_count - 1}
                        <figure>
                            <img class="pd-img" src="https://hhstsyoejx.gjirafa.net/gj50/img/{$product.product_id}/thumb/{$count}.jpg">
                        </figure>
                        {/for}
                    </div>
                    {if $product.total_stock <= 0}
                        <div class="sold-out-msi">
                            <h4>E SHITUR</h4>
                        </div>
                    {/if}
                    <div class="msi-loader"></div>
                    <div class="slide-bullet">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
                <div class="pd-content">
                    <a href="{"products.view&product_id={$product.product_id}"|fn_url}?utm_content=landing"><h3>{$product.product}</h3></a>
                    <div class="pd-tech-detail  mb10">
                        <h4 class="mb0 hidden-phone">Detajet Teknike</h4>
                        <ul class="detTech">
                            {foreach from=$product.features key=key item=value}
                            <li>
                                <span>{$key}</span>
                                <span>{$value}</span>
                            </li>
                            {/foreach}
                        </ul>
                        <h4 class="flexing text-primary flex-v-center expandDet">Shiko me shume <svg style="width:15px; height: 15px; margin-left:10px;"><use xlink:href="#svg-down"></use></svg></h4>
                    </div>
                    <div class="pd-price mt20 mb20">
                        <h2 class="mb0">
                            {include file="common/price.tpl" value=$product.price}
                            {if $product.old_price}
                                <span class="text-muted " style="font-size: 16px; text-decoration: line-through; font-weight: 300;">
                                    {include file="common/price.tpl" value=$product.old_price}
                                </span>
                            {/if}
                        </h2>
                        <span class="block">Përfshirë TVSH-në</span>
                    </div>
                    {if {$product.short_description}}
                    <p class="text-muted mb10">{$product.short_description}</p>
                    {/if}
                    <div class="pd-buttons flexing flex-v-center">
                        <form action="{""|fn_url}" method="post" class="cm-ajax pd-buttons flexing flex-v-center">
                            <input type="hidden" name="result_ids" value="cart_status*,wish_list*,checkout*,account_info*"/>
                            <input type="hidden" name="redirect_url" value="{$redirect_url|default:$config.current_url}"/>
                            <input type="hidden" name="product_data[{$product.product_id}][product_id]" value="{$product.product_id}"/>

                            <a href="{"products.view&product_id={$product.product_id}"|fn_url}?utm_content=landing" class="button btn-primary">SHIKO DETAJET</a>
                            <button class="button" type="submit" name="dispatch[checkout.add..{$product.product_id}]">
                                <svg><use xlink:href="#svg-cart"></use></svg>
                            </button>
                            <button class="button" type="submit" name="dispatch[wishlist.add..{$product.product_id}]">
                                <svg><use xlink:href="#svg-heart"></use></svg>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
            {/if}
        {/foreach}
    {/if}
</div>
{if $isAjax}
    {assign var="product_count" value=$products|count}
    {foreach from=$products item=product}

    {if $smarty.session.auth.vip && !$is_al && $product.vip_price != null}
        {if $product.old_price < $product.vip_price}
            {$product.old_price = $product.price}
        {/if}
        {if $product.price > $product.vip_price}
            {$product.price = $product.vip_price}
        {/if}
    {/if}

    {if $product.list_price && $product.list_price > $product.price}
        {$product.old_price = $product.list_price}
        {$product.discount = (($product.list_price - $product.price) / $product.list_price) * 100}
    {/if}

    <div class="grid-single">
        {if $product.discount}
        <span class="pages-discount-label">-{$product.discount|round}%</span>
        {/if}
        <div class="gsc" onclick="goto('{"products.view&product_id={$product.product_id}"|fn_url}?utm_content=landing')">
            <div class="pdg-img">
                <img class="pd-img" src="https://hhstsyoejx.gjirafa.net/gj50/img/{$product.product_id}/thumb/0.jpg">
                {if $product.total_stock <= 0}
                    <div class="sold-out-msi">
                        <h4>E SHITUR</h4>
                    </div>
                {/if}
            </div>
            <div class="mb10">
                <a href="{"products.view&product_id={$product.product_id}"|fn_url}?utm_content=landing"><h3 class="mb10 mt10 max-rows mr2 pd-g-title">{$product.product}</h3></a>
                <h2 class="mb0">{if $is_al}{$product.price_al|number_format} Lekë{else}{number_format((float)$product.price, 2, '.', '')} €{/if}
                {if $product.old_price && $product.old_price > $product.price && !$is_al}
                    <span class="text-muted discount">
                        {include file="common/price.tpl" value=$product.old_price}
                    </span>
                {elseif $product.old_price_al && $product.old_price_al > $product.price_al && $is_al}
                    <span class="text-muted discount">
                        {include file="common/price.tpl" value=$product.old_price_al}
                    </span>
                {/if}
                </h2>
                <span class="block">Përfshirë TVSH-në</span>
            </div>
        </div>
        <div class="pd-buttons p flexing flex-v-center">
            <form action="{""|fn_url}" method="post" class="cm-ajax flexing flex-v-center">
                <input type="hidden" name="result_ids" value="cart_status*,wish_list*,checkout*,account_info*"/>
                <input type="hidden" name="redirect_url" value="{$redirect_url|default:$config.current_url}"/>
                <input type="hidden" name="product_data[{$product.product_id}][product_id]" value="{$product.product_id}"/>

                <a href="{"products.view&product_id={$product.product_id}"|fn_url}?utm_content=landing" class="button btn-primary">SHIKO DETAJET</a>
                <button class="button" type="submit" name="dispatch[checkout.add..{$product.product_id}]">
                    <svg><use xlink:href="#svg-cart"></use></svg>
                </button>
                <button class="button" type="submit" name="dispatch[wishlist.add..{$product.product_id}]">
                    <svg><use xlink:href="#svg-heart"></use></svg>
                </button>
            </form>
        </div>
    </div>
    {/foreach}
    {if $product_count is not div by 4}
        {assign var="cn" value=fn_closest_number($product_count, 4)}
        {assign var="remaining" value=$cn - $product_count}
        {for $var=1 to $remaining}
            <div class="grid-single empty"></div>
        {/for}
    {/if}
    {if $product_count lt 100}
        <style type="text/css">
            #load_msi {
                display: none !important;
            }

            #no_results {
                display: block !important;
                color: #fff;
            }
        </style>
    {/if}
{else}
<div class="pd-grid flexing flex-wrap">

    {if $priority_products_2}
        {foreach from=$priority_products_2 item=product}

        {if $smarty.session.auth.vip && !$is_al && $product.vip_price != null}
            {if $product.old_price < $product.vip_price}
                {$product.old_price = $product.price}
            {/if}
            {if $product.price > $product.vip_price}
                {$product.price = $product.vip_price}
            {/if}
        {/if}

        {if $product.list_price && $product.list_price > $product.price}
            {$product.old_price = $product.list_price}
            {$product.discount = (($product.list_price - $product.price) / $product.list_price) * 100}
        {/if}

        <div class="grid-single" style="position: relative; {if $is_al && !$product.price_al} display: none; {/if}">
            {if $product.discount && !$is_al}
            <span class="pages-discount-label">-{$product.discount|round}%</span>
            {elseif $product.discount_al && $is_al}
            <span class="pages-discount-label">-{$product.discount_al|round}%</span>
            {/if}
            <div class="gsc" onclick="goto('{"products.view&product_id={$product.product_id}"|fn_url}?utm_content=landing')">
                <div class="pdg-img">
                    <img class="pd-img" src="https://hhstsyoejx.gjirafa.net/gj50/img/{$product.product_id}/thumb/0.jpg">
                    {if $product.total_stock <= 0}
                        <div class="sold-out-msi">
                            <h4>E SHITUR</h4>
                        </div>
                    {/if}
                </div>
                <div class="mb10">
                    <a href="{"products.view&product_id={$product.product_id}"|fn_url}?utm_content=landing"><h3 class="mb10 mt10 max-rows mr2 pd-g-title">{$product.product}</h3></a>
                    <h2 class="mb0">{if $is_al}{$product.price_al|number_format} Lekë{else}{number_format((float)$product.price, 2, '.', '')} €{/if}
                    {if $product.old_price && $product.old_price > $product.price && !$is_al}
                        <span class="text-muted discount">
                            {include file="common/price.tpl" value=$product.old_price}
                        </span>
                    {elseif $product.old_price_al && $product.old_price_al > $product.price_al && $is_al}
                        <span class="text-muted discount">
                            {include file="common/price.tpl" value=$product.old_price_al}
                        </span>
                    {/if}
                    </h2>
                    <span class="block">Përfshirë TVSH-në</span>
                </div>
            </div>
            <div class="pd-buttons p flexing flex-v-center">
                <form action="{""|fn_url}" method="post" class="cm-ajax flexing flex-v-center">
                    <input type="hidden" name="result_ids" value="cart_status*,wish_list*,checkout*,account_info*"/>
                    <input type="hidden" name="redirect_url" value="{$redirect_url|default:$config.current_url}"/>
                    <input type="hidden" name="product_data[{$product.product_id}][product_id]" value="{$product.product_id}"/>

                    <a href="{"products.view&product_id={$product.product_id}"|fn_url}?utm_content=landing" class="button btn-primary">SHIKO DETAJET</a>
                    <button class="button" type="submit" name="dispatch[checkout.add..{$product.product_id}]">
                        <svg><use xlink:href="#svg-cart"></use></svg>
                    </button>
                    <button class="button" type="submit" name="dispatch[wishlist.add..{$product.product_id}]">
                        <svg><use xlink:href="#svg-heart"></use></svg>
                    </button>
                </form>
            </div>
        </div>
        {/foreach}
    {/if}

    {assign var="product_count" value=$products|count}
    {foreach from=$products item=product}

    {if $smarty.session.auth.vip && !$is_al && $product.vip_price != null}
        {if $product.old_price < $product.vip_price}
            {$product.old_price = $product.price}
        {/if}
        {if $product.price > $product.vip_price}
            {$product.price = $product.vip_price}
        {/if}
    {/if}

    {if $product.list_price && $product.list_price > $product.price}
        {$product.old_price = $product.list_price}
        {$product.discount = (($product.list_price - $product.price) / $product.list_price) * 100}
    {/if}

    {if !in_array($product.product_code, $priority_product_codes_2)}
    <div class="grid-single" style="position: relative; {if $is_al && !$product.price_al} display: none; {/if}">
        {if $product.discount && !$is_al}
        <span class="pages-discount-label">-{$product.discount|round}%</span>
        {elseif $product.discount_al && $is_al}
        <span class="pages-discount-label">-{$product.discount_al|round}%</span>
        {/if}
        <div class="gsc" onclick="goto('{"products.view&product_id={$product.product_id}"|fn_url}?utm_content=landing')">
            <div class="pdg-img">
                <img class="pd-img" src="https://hhstsyoejx.gjirafa.net/gj50/img/{$product.product_id}/thumb/0.jpg">
                {if $product.total_stock <= 0}
                    <div class="sold-out-msi">
                        <h4>E SHITUR</h4>
                    </div>
                {/if}
            </div>
            <div class="mb10">
                <a href="{"products.view&product_id={$product.product_id}"|fn_url}?utm_content=landing"><h3 class="mb10 mt10 max-rows mr2 pd-g-title">{$product.product}</h3></a>
                <h2 class="mb0">
                    {if $is_al}
                        {include file="common/price.tpl" value=$product.price_al}
                    {else}
                        {include file="common/price.tpl" value=$product.price}
                    {/if}
                {if $product.old_price && $product.old_price > $product.price && !$is_al}
                    <span class="text-muted discount">
                        {include file="common/price.tpl" value=$product.old_price}
                    </span>
                {elseif $product.old_price_al && $product.old_price_al > $product.price_al && $is_al}
                    <span class="text-muted discount">
                        {include file="common/price.tpl" value=$product.old_price_al}
                    </span>
                {/if}
                </h2>
                <span class="block">Përfshirë TVSH-në</span>
            </div>
        </div>
        <div class="pd-buttons p flexing flex-v-center">
            <form action="{""|fn_url}" method="post" class="cm-ajax flexing flex-v-center">
                <input type="hidden" name="result_ids" value="cart_status*,wish_list*,checkout*,account_info*"/>
                <input type="hidden" name="redirect_url" value="{$redirect_url|default:$config.current_url}"/>
                <input type="hidden" name="product_data[{$product.product_id}][product_id]" value="{$product.product_id}"/>

                <a href="{"products.view&product_id={$product.product_id}"|fn_url}?utm_content=landing" class="button btn-primary">SHIKO DETAJET</a>
                <button class="button" type="submit" name="dispatch[checkout.add..{$product.product_id}]">
                    <svg><use xlink:href="#svg-cart"></use></svg>
                </button>
                <button class="button" type="submit" name="dispatch[wishlist.add..{$product.product_id}]">
                    <svg><use xlink:href="#svg-heart"></use></svg>
                </button>
            </form>
        </div>
    </div>
    {/if}
    {/foreach}
    {if $product_count is not div by 4}
        {assign var="cn" value=fn_closest_number($product_count, 4)}
        {assign var="remaining" value=$cn - $product_count}
        {for $var=1 to $remaining}
            <div class="grid-single empty"></div>
        {/for}
    {/if}
</div>
{/if}

{/if}