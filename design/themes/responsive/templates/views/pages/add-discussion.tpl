<link href="//cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css" rel="stylesheet"/>
<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet"/>

<style>
    {literal}
    .tygh-content .container-fluid {
        max-width: 100% !important;
        width: 100% !important;
        padding: 0;
        overflow: hidden;
    }

    .product {
        background: #fff;
        padding: 20px;
        margin: 0 !important;
        border-bottom: 1px solid #eee;
        border-left: 1px solid #fafafa;
        min-height: 210px;
    }

    .hw {
        width: 50% !important;
    }

    .product * {
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
    }

    .row-wrapper {
        box-shadow: 0px 10px 30px #eee;
        margin-top: 30px;
    }

    .text-center {
        text-align: center;
    }

    .tygh-content {
        background-color: #fafafa;
    }

    .product h2 {
        font-size: 19px;
        font-weight: bolder;
    }

    h2, h1 {
        margin: 0;
    }

    p {
        color: #999;
    }

    #tygh_footer {
        display: none;
    }

    .left {
        float: left;
    }

    .img {
        width: 40%;
        height: 150px;
        background-repeat: no-repeat;
        -webkit-background-size: contain;
        background-size: contain;
        background-position: center center;
    }

    .content {
        width: 60%;
        padding-left: 20px;
    }

    span.old-price {
        text-decoration: line-through;
        font-weight: lighter;
        color: #fe2d09;
        font-size: 17px;
    }

    .h-product {
        padding: 3%;
    }

    .h-product img {
        margin: 40px 0;
    }

    .h-product h1 {
        font-size: 45px;
    }

    .super-title {
        font-size: 40px;
        font-weight: lighter;
    }

    a.buy-now {
        padding: 10px 20px;
        background: #e65228;
        display: inline-block;
        color: #fff;
        font-weight: bolder;
        font-size: 25px;
        min-width: 250px;
    }

    .top-product {
        margin-bottom: 150px;
        margin-top: 190px;
        text-align: center;
        background: #232227;
        position: relative;
        padding: 0 20px;
    }

    .top-product h1 {
        font-weight: bolder !important;
    }

    .top-product img {
        position: relative;
        z-index: 5;
    }

    .top-product:before {
        content: '';
        position: absolute;
        top: -90px;
        left: -20px;
        width: 130%;
        height: 200px;
        background: #232227;
        transform: rotate(-4deg);
        -webkit-transform: rotate(-4deg);
        -moz-transform: rotate(-4deg);
        -o-transform: rotate(-4deg);
    }

    .top-product:after {
        content: '';
        position: absolute;
        bottom: -50px;
        left: -20px;
        width: 130%;
        height: 200px;
        background: #232227;
        transform: rotate(-4deg);
        -webkit-transform: rotate(-4deg);
        -moz-transform: rotate(-4deg);
        -o-transform: rotate(-4deg);
    }

    .top-product.black h1, .top-product.black h3 {
        color: #eee;
        font-weight: lighter;
    }

    .top-product.black h3, .top-product.gray h3 {
        margin: 20px 0;
    }

    .content-over {
        position: relative;
        z-index: 2;
    }

    .top-product.eadr:before, .top-product.eadr:after {
        background: #100604;
    }

    .top-product h1.price {
        font-weight: bolder !important;
        font-size: 45px;
    }

    .top-product.gray:before, .top-product.gray:after {
        transform: rotate(4deg);
        background: #ddd;
    }

    .top-product.gray:after {
        bottom: -100px;
    }

    .top-product.gray {
        background: #ddd !important;
    }

    .top-product.gray h1, .top-product.gray h3 {
        font-weight: lighter;
    }

    .content-over {
        width: 700px;
        margin: 0 auto;
        max-width: 100%;
    }

    .right-panel {
        text-align: left;
        padding-left: 20px;
    }

    .tygh-content {
        background: none !important;
        box-shadow: none !important;

    }

    @media screen and (max-width: 768px) {
        .span8.product {
            width: 100% !important;
            float: left;
        }

        .tygh-content {
            padding: 0 !important;
        }

        .top-product {
            margin-bottom: 70px;
            margin-top: 140px;
        }

        .top-product.gray {
            margin-bottom: 110px;
        }

        .tygh-content .container-fluid {
            margin-top: -1px;
        }

        .right-panel {
            text-align: center;
            padding: 0;
            width: 100%;
        }

        .top-product img {
            max-height: 300px !important;
        }
    }

    @media screen and (min-width: 769px) {
        .product .content p {
            overflow: hidden;
            text-overflow: ellipsis;
            display: -webkit-box;
            line-height: 1.6;
            max-height: 8em;
            margin: 5px 0;
            -webkit-line-clamp: 6;
            padding: 0;
            -webkit-box-orient: vertical;
        }

        .product {
            height: 220px;
        }
    }

    @media screen and (max-width: 400px) {
        .img {
            width: 30%;
        }

        .content {
            width: 70%;
        }
    }

    a:hover {
        text-decoration: none;
    }

    .product {
        position: relative;
        overflow: hidden;
    }

    .product::before {
        pointer-events: none;
        content: 'Vlerëso';
        position: absolute;
        left: -50px;
        top: -22px;
        height: 55px;
        line-height: 100px;
        background: #e65228;
        color: #fff;
        padding: 10px;
        transform: rotate(-45deg);
        width: 110px;
        text-align: center;
        font-weight: bolder;
        text-transform: uppercase;
        letter-spacing: -.6px;
    }

    /*---------------*/

    /****** Style Star Rating Widget *****/

    fieldset, label {
        margin: 0;
        padding: 0;
    }

    .rating {
        border: none;
        float: left;
    }

    .rating > input {
        display: none;
    }

    .rating > label:before {
        margin: 5px;
        font-size: 1.25em;
        font-family: FontAwesome;
        display: inline-block;
        content: "\f005";
    }

    .rating > .half:before {
        content: "\f089";
        position: absolute;
    }

    .rating > label {
        color: #ddd;
        float: right;
    }

    .content-list {
        /*float: left;*/
        margin-top: 5px;
    }

    /***** CSS Magic to Highlight Stars on Hover *****/

    .rating > input:checked ~ label, /* show gold star when clicked */
    .rating:not(:checked) > label:hover, /* hover current star */
    .rating:not(:checked) > label:hover ~ label {
        color: #FFD700;
    }

    /* hover previous stars in list */

    .rating > input:checked + label:hover, /* hover current star when changing rating */
    .rating > input:checked ~ label:hover,
    .rating > label:hover ~ input:checked ~ label, /* lighten current selection */
    .rating > input:checked ~ label:hover ~ label {
        color: #FFED85;
    }

    .product-price {
        display: none;
    }

    .h1-rating {
        text-align: center;
    }

    .content-list {
        background: #fff;
        border-bottom: 1px solid #ddd;
    }

    .product {
        border: none !important;
    }

    .rating-field {
        width: 100% !important;
        float: none;
        padding: 10px 0 10px 10px;
        box-sizing: border-box;
    }

    .rate-text {
        display: inline-block;
    }

    .rating {
        display: inline-block;
        float: none !important;
        height: 18px;
    }

    .text-area {
        padding: 10px;
        width: 100%;
        min-height: 130px;
        resize: none;
        border-color: #ddd;
    }

    .text-area:focus {
        border-color: #bbb;
    }

    /*.tygh-content {*/
    /*box-shadow: none !important;*/
    /*background: none !important;*/
    /*}*/
    @media (min-width: 992px) {
        .rating-field {
            width: 50% !important;
            float: left;
        }
    }

    {/literal}
</style>

<h1 style="text-align: center;margin-top: 30px; color: #333333;">Vlerësoni produktet</h1>
<div class="container">
    <div class="row-wrapper clearfix" style="box-shadow: none;">
        {foreach $order_info.products as $op}

        {if in_array($op.product_id, array_keys($reviewed_products_data))}
            {assign var="product_id" value=$op.product_id}
            {assign var="product" value=$reviewed_products_data.$product_id}

            {assign var="post_id" value=$product.post_id}
            {assign var="rating" value=$product.rating}
            {assign var="message" value=$product.message}
        {else}
            {assign var="post_id" value=''}
            {assign var="rating" value=''}
            {assign var="message" value=''}
        {/if}

        <form name="add_post_form" method="post"
              action="{"products.add_discussion_from_mail"|fn_url}"
              enctype='multipart/form-data'
              style="margin-bottom: 15px;">

            <input type="hidden" name="post_data_{$op.product_id}[name]"
                   value="{$order_info.firstname} {$order_info.lastname}"/>
            <input type="hidden" name="post_data[product_ids][]" value="{$op.product_id}"/>
            <input type="hidden" name="post_data_{$op.product_id}[object_type]" value="P">
            <input type="hidden" name="post_data_{$op.product_id}[object_id]" value="{$op.product_id}">
            <input type="hidden" name="order_id" value="{$order_info.order_id}">
            <input type="hidden" name="e_key" value="{$smarty.get.e_key}">
            <input type="hidden" name="post_id" value="{$post_id}">

            {assign var="meta_container" value=intval($op.product_id/8000+1)}
            {if $meta_container gt 5}
                {$meta_container=5}
            {/if}
            {assign var="meta_azure_path" value=("https://hhstsyoejx.gjirafa.net/gj50/img/`$op.product_id`/img/0.jpg")}
            <div class="content-list" style="border: none;">
                <div class="span8 product hw" data-aos="fade-right">
                    <a href="{"products.view&product_id={$op.product_id}"|fn_url}">
                        <div class="img left"
                             style="background-image:url({$meta_azure_path})"></div>
                        <div class="content left"><h2>{$op.product}</h2>
                            <p>{$op.full_description|strip_tags}</p>
                        </div>
                    </a>
                </div>
                <div class="rating-field">
                    <p class="rate-text">Vlerëso:</p>
                    <fieldset style="height: 18px;" class="rating" id="rating_{$op.product_id}">
                        <input type="radio" id="star5_{$op.product_id}"
                               name="post_data_{$op.product_id}[rating_value]" value="5" {if $rating == 5}checked{/if}/><label class="full"
                                                                                                 for="star5_{$op.product_id}"
                                                                                                 title="Shkelqyeshem - 5 yje"></label>
                        <input type="radio" id="star4_{$op.product_id}"
                               name="post_data_{$op.product_id}[rating_value]" value="4" {if $rating == 4}checked{/if}/><label class="full"
                                                                                                 for="star4_{$op.product_id}"
                                                                                                 title="Shume mire - 4 yje"></label>
                        <input type="radio" id="star3_{$op.product_id}"
                               name="post_data_{$op.product_id}[rating_value]" value="3" {if $rating == 3}checked{/if}/><label class="full"
                                                                                                 for="star3_{$op.product_id}"
                                                                                                 title="Mire - 3 yje"></label>
                        <input type="radio" id="star2_{$op.product_id}"
                               name="post_data_{$op.product_id}[rating_value]" value="2" {if $rating == 2}checked{/if}/><label class="full"
                                                                                                 for="star2_{$op.product_id}"
                                                                                                 title="Keq- 2 yje"></label>
                        <input type="radio" id="star1_{$op.product_id}"
                               name="post_data_{$op.product_id}[rating_value]" value="1" {if $rating == 1}checked{/if}/><label class="full"
                                                                                                 for="star1_{$op.product_id}"
                                                                                                 title="Shume keq - 1 yll"></label>
                    </fieldset>
                    <textarea rows="4" class="text-area" name="post_data_{$op.product_id}[message]" cols="50"
                              placeholder="Shkruani vlerësimin tuaj...">{$message}</textarea>
                    {if in_array($op.product_id, array_keys($reviewed_products_data))}
                        <p style="color: #e65228;">Ju tashmë keni vlerësuar këtë produkt. Vlerësimi i ardhshëm do të ndryshojë vlerësimin paraprak.</p>
                    {/if}
                </div>
            </div>
            <div style="text-align:  right; border-bottom: 1px solid #dddd;">
                <button style="float: none;color: #fff;background: #e65228;animation: flash 1s ease infinite;border-right: 0;border: none; margin: 0 0 15px 0; padding: 10px 15px;font-weight: 700;outline:  0;min-width: 130px; "
                        type="submit">Vlerëso
                </button>
            </div>
        </form>
        {/foreach}
    </div>
</div>

<script src="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js"></script>
<script>{literal}AOS.init({offset: 60, duration: 400, easing: 'ease', delay: 100,});{/literal}</script>