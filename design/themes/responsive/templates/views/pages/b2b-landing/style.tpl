<style>
.tygh-content{
  background-color: #f3f3f3;
}
.tygh-content > div{
  padding-bottom: 0;
}
.b2b-landing{
  background-color: white;
  padding: 0 !important;
}
.b2b-landing .b2b{
  overflow: hidden;
}
.b2b-landing .container {
  margin-left: auto;
  margin-right: auto;
  max-width: 1200px;
}
.b2b-landing .text-center {
  text-align: center;
}
.b2b-landing .fw-7 {
  font-weight: 700;
}
.b2b-landing .m-0 {
  margin: 0;
}
.b2b-landing * {
  box-sizing: border-box;
}
.b2b-landing *:focus {
  outline: none;
}
.b2b-landing .for-who {
  margin-bottom: 4rem;
}
.b2b-landing .for-who h1 {
  margin-top: 0;
  margin-bottom: 2rem;
  padding-top: 2rem;
}
.b2b-landing .for-who__wrapper,
.b2b-landing .benefits-wrapper {
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
}
.b2b-landing .for-who__item {
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
}
.b2b-landing .for-who__item svg {
  margin-top: 1rem;
  margin-bottom: 2rem;
  max-width: 50px;
  max-height: 50px;
  fill: #e65228;
}
.b2b-landing h1{
  margin-bottom: 2rem;
}
.b2b-landing .for-who__item,
.b2b-landing .benefits__item {
  padding: 1rem;
  border: 1px solid #ddd;
  width: 25%;
  transition: 0.3s;
  margin: -0.4px;
}
.b2b-landing .for-who__item:hover,
.b2b-landing .benefits__item:hover {
  transition: 0.3s;
  box-shadow: 0 5px 31px -1px rgba(0, 0, 0, 0.15);
  z-index: 1;
}
.b2b-landing .benefits {
  justify-content: center;
  background-color: #fafafa;
  padding-top: 3rem;
  margin-bottom: 3rem;
  padding-bottom: 3rem;
}
.b2b-landing .benefits__heading {
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  font-weight: 700;
}
.b2b-landing .benefits__heading svg {
  margin-top: 1rem;
  margin-bottom: 2rem;
  max-width: 50px;
  max-height: 50px;
  fill: #e65228;
}
.b2b-landing .benefits a.button,
.b2b-landing .b2b a.button {
  display: inline-block;
  border: none;
  background-color: #e65228;
  cursor: pointer;
  padding: 0.75rem 1.5rem;
  color: #fff;
  /* margin-top: 4rem; */
  transition: 0.3s;
  font-weight: 700;
  transform: translateY(-3px);
  box-shadow: 2px 2px 7px -1px #a3a2a2;
  font-size: 0.95rem;
  text-decoration: none;
}
.b2b-landing .benefits a.button {
  margin-top: 4rem;
}
.b2b-landing .benefits a.button:hover,
.b2b-landing .b2b a.button:hover {
  background-color: #e8e8e8;
  color: #e65228;
}
.b2b-landing .benefits a.button:active,
.b2b-landing .b2b a.button:active {
  background-color: #e8e8e8;
  color: #e65228;
  transition: 0.2s;
  transform: translateY(0);
  box-shadow: none;
}
.b2b-landing .benefits__item {
  position: relative;
}
.b2b-landing .benefits__item:hover .benefits__content {
  opacity: 1;
  transition: 0.3s;
}
.b2b-landing .benefits__content {
  padding: 0.5rem;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  position: absolute;
  opacity: 0;
  transition: 0.3s;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: white;
}
.b2b-landing .b2b {
  background-color: #fafafa;
  padding-bottom: 3rem;
}
.b2b-landing .b2b-1 {
  width: 60%;
  padding-left: 100px;
  padding-right: 100px;
}
.b2b-landing .b2b-2 {
  width: 20%;
  display: flex;
  align-items: center;
}
.b2b-landing .b2b-3 {
  width: 20%;
  display: flex;
  flex-direction: column;
  justify-content: center;
}
.b2b-landing .b2b-3 a{
  margin-bottom: 5px;
}
.b2b-landing .b2b__underimage{
  padding-top: 50px;
  display: flex;
}
/* .b2b-landing .b2b__underimage-button{
  position: absolute;
  top: 50%;
  transform: translateY(-50%);
  right: 270px;
  z-index: 2;
} */
/* .b2b-landing .b2b__underimage svg{
  position: absolute;
    right: 0;
    top: 0;
    fill: #e65228;
    opacity: .15;
    z-index: 0;
    transform: scale(2.1);
    z-index: 0;
} */

/* .b2b-landing .b2b__title,
.b2b-landing .b2b__undertitle {
    margin-bottom: 1rem;
    padding-left: 100px;
    width: 56%;
    padding-right: 3%;
}
.b2b__title{
  margin-top: 3rem;
} */
.b2b-landing .b2b__undertitle {
  margin-bottom: 0;
}
.b2b-landing .b2b__container {
  display: flex;
  flex-wrap: wrap;
  background-position: center;
  background-size: cover;
  background-repeat: no-repeat;
  background-position: bottom;
}
.b2b-landing .b2b__image {
  height: 100%;
  width: 100%;
  background-position: right;
  background-repeat: no-repeat;
  background-size: contain;
  min-height: 400px;
  transform: rotate(-8deg) scale(1.25);
}
.b2b-landing .b2b__content {
  width: 100%;
  display: flex;
  justify-content: flex-end;
  align-items: center;
  background-image: linear-gradient(90deg, rgba(0, 0, 0, 0.2) 0%, rgba(0, 0, 0, 0.8) 50%, rgba(0, 0, 0, 0.85) 100%);
}
.b2b-landing .b2b__content ul {
  padding: 3rem;
  max-width: 40%;
}
.b2b-landing .b2b__content ul li {
  position: relative;
  color: white;
  font-size: 15px;
}
.b2b-landing .b2b__content ul li:first-child {
  font-size: 1.5rem;
  border-bottom: 1px solid white;
  padding-bottom: .75rem;
  margin-bottom: 1.5rem;
  margin-left: -30px;
}
.b2b-landing .b2b__content ul li:not(:first-child) {
  padding-bottom: 1rem;
}
.b2b-landing .b2b__content ul li:not(:first-child):before {
  content: "";
  height: 4px;
  width: 18px;
  background-color: #e65228;
  position: absolute;
  left: -25px;
  top: 10px;
  transform: rotate(-45deg);
  border-radius: 1px;
}
.b2b-landing .b2b__content ul li:not(:first-child):after {
  content: "";
  height: 4px;
  width: 7px;
  background-color: #e65228;
  position: absolute;
  left: -27px;
  top: 13px;
  transform: rotate(45deg);
  border-radius: 1px;
}
.b2b-landing .services{
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
  padding-left: 3%;
  padding-right: 3%;
}
.b2b-landing .services__main-title{
  width: 100%;
  text-align: center;
}
.b2b-landing .services__item{
  width: 31%;
  margin-bottom: 1rem;
  background-color: #f9f9f9;
  box-shadow: 0 2px 6px -2px rgba(0,0,0,0.3);
}
.b2b-landing .services__content{
  font-size: 14px;
  line-height: 1.5;
}
.b2b-landing .services__title{
  text-align: right;
  color: white;
  padding: 1.25rem 1rem;
  background-color: #e65228;
  font-size: 2.25rem;
  display: flex;
  flex-direction: column;
}
.b2b-landing .services__title--thick{
  font-weight: 900;
}
.b2b-landing .services__title--thin{
  font-weight: 100;
}
.b2b-landing .services__content{
  padding: .75rem 1rem;
}
.b2b-landing .faq {
  background-color: #fafafa;
  margin-top: 2.5rem;
  padding-top: 2.5rem;
}
.b2b-landing .question-item {
  padding: 1rem;
  margin-top: 0;
  border-top: 1px solid #f2f2f2;
}
.b2b-landing .question {
  cursor: pointer;
  padding: 1rem;
  padding-right: 2rem;
  position: relative;
  font-size: 1.375rem;
  font-weight: 600;
  transition: 0.2s ease-in-out;
}
.b2b-landing .answer {
  padding-left: 1rem;
  padding-right: 1rem;
}
.b2b-landing .question.active {
  color: #e65228;
  transition: 0.2s ease-in-out;
}
.b2b-landing .question + .answer {
  transition: 0.1s ease-in-out;
  max-height: 0;
  overflow: hidden;
  transition: 0.2s ease-in-out;
}
.b2b-landing .question.active + .answer {
  max-height: 100px;
  overflow: hidden;
  transition: 0.2s ease-in-out;
}
.b2b-landing .question:before,
.b2b-landing .question:after {
  content: "";
  position: absolute;
  top: 50%;
  right: 0;
  width: 15px;
  height: 2px;
  background-color: #e65228;
  transition: 0.2s ease-in-out;
  transform: translateY(-50%) rotate(-45deg);
}
.b2b-landing .question:after {
  right: 10px;
  transform: translateY(-50%) rotate(45deg);
}
.b2b-landing .question.active:before {
  transition: 0.2s ease-in-out;
  transform: translateY(-50%) rotate(45deg);
}
.b2b-landing .question.active:after {
  transition: 0.2s ease-in-out;
  transform: translateY(-50%) rotate(-45deg);
}
.b2b-landing .form {
  position: fixed;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  background-color: white;
  z-index: -2;
  opacity: 0;
  border-radius: 5px;
  padding: 3rem;
  width: 100%;
  max-width: 500px;
  transition: opacity 0.2s ease-in-out;
  pointer-events: none;
}
.b2b-landing .form .close {
  position: absolute;
  top: 10px;
  right: 15px;
  width: 30px;
  height: 30px;
  cursor: pointer;
  z-index: 9;
}
.b2b-landing .form .close-inner {
  display: block;
  position: relative;
  width: 30px;
  height: 30px;
}
.b2b-landing .form .close-inner:before,
.b2b-landing .form .close-inner:after {
  content: "";
  position: absolute;
  width: 20px;
  height: 2px;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%) rotate(45deg);
  background-color: #e65228;
}
.b2b-landing .form .close-inner:after {
  transform: translate(-50%, -50%) rotate(-45deg);
}
.b2b-landing .form h2 {
  margin: 0;
  margin-bottom: 1.5rem;
}
.b2b-landing .form.active {
  opacity: 1;
  z-index: 2;
  transition: opacity 0.2s ease-in-out;
  pointer-events: all;
}
.b2b-landing form {
  display: flex;
  flex-direction: column;
  max-width: 500px;
  margin-left: auto;
  margin-right: auto;
  align-items: center;
  position: relative;
}
.b2b-landing form input,
.b2b-landing form textarea {
  width: 100%;
  padding: 0.5rem;
  border: 1px solid #ddd;
  border-radius: 3px;
  transition: 0.2s ease-in-out;
}
.b2b-landing form input:focus,
.b2b-landing form textarea:focus {
  border-color: #e65228;
  transition: 0.2s ease-in-out;
}
.b2b-landing form input {
  margin-bottom: 1rem;
}
.b2b-landing form textarea {
  margin-bottom: 1.75rem;
}
.b2b-landing form button {
  border: none;
  cursor: pointer;
  padding: 0.75rem 1.5rem;
  color: #fff;
  width: 160px;
  transition: 0.3s;
  font-weight: 700;
  transform: translateY(-3px);
  box-shadow: 2px 2px 7px -1px #a3a2a2;
  font-size: 0.95rem;
  background-color: #e65228;
  display: inline-block;
}
.b2b-landing form button:hover {
  background-color: #e8e8e8;
  color: #e65228;
}
.b2b-landing form button:active {
  background-color: #e8e8e8;
  color: #e65228;
  transition: 0.2s;
  transform: translateY(0);
  box-shadow: none;
}
.b2b-landing .overlay {
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: rgba(0, 0, 0, 0.6);
  opacity: 0;
  z-index: -1;
  transition: all 0.2s ease-in-out;
  pointer-events: none;
}
.b2b-landing .overlay.active {
  opacity: 1;
  z-index: 1;
  transition: all 0.2s ease-in-out;
  pointer-events: all;
}
@media (max-width: 1199.98px) {
  .b2b-landing .b2b__image {
    min-height: 36vw;
  }
  .b2b-landing .b2b__underimage-button{
    right: 175px;
  }
  .b2b-landing .b2b-1 {
    width: 50%;
    padding-left: 70px;
    padding-right: 70px;
  }
  .b2b-landing .b2b-2, .b2b-landing .b2b-3 {
    width: 25%;
  }
}
@media (max-width: 991.98px) {
  .b2b-landing .services__title{
    font-size: 1.5rem;
  }
  .b2b-landing .benefits__item,
.b2b-landing .for-who__item {
    width: 33.3333%;
  }
  .b2b-landing .question.active + .answer {
    max-height: 150px;
  }
  .b2b-landing .benefits__heading svg {
    margin-bottom: 1rem;
  }
  .b2b-landing .benefits__content {
    width: auto;
    height: auto;
    position: static;
    opacity: 1;
    background-color: #fafafa;
  }
  .b2b-landing .b2b__content ul{
    max-width: 50%;
  }
  .b2b-landing .b2b__underimage-button{
    right: 100px;
  }
}
@media (max-width: 979.98px) {
  .b2b-landing .b2b__underimage{
    flex-wrap: wrap;
  }
  .b2b-landing .b2b-1 {
    width: 100%;
  }
  .b2b-landing .b2b-2 {
    margin-top: 2rem;
    width: 55%;
    padding-left: 70px;
    /* padding-right: 70px; */
  }
  .b2b-landing .b2b-3 {
    margin-top: 2rem;
    width: 45%;
    /* padding-left: 70px; */
    padding-right: 70px;
  }
}
@media (max-width: 767.98px) {
  .b2b-landing .services__item, .b2b-landing .services__item:first-of-type{
    width: 100%;
  }
  .b2b-landing .benefits__item,
  .b2b-landing .for-who__item {
    width: 50%;
  }
  .b2b-landing .question.active + .answer {
    max-height: 200px;
  }
  .b2b-landing .b2b__content{
    background-image: linear-gradient(90deg, rgba(0, 0, 0, 0.2) 0%, rgba(0, 0, 0, 0.8) 40%, rgba(0, 0, 0, 0.85) 100%);
  }
  .b2b-landing .b2b__content ul{
    max-width: 70%;
  }
  .b2b-landing .b2b{
    padding-bottom: 1rem;
  }
  .b2b-landing .b2b__underimage svg{
    display: none;
  }
  .tygh-content {
    padding-top: 0 !important;
  }
}
@media (max-width: 575.98px) {
  .b2b-landing .services__title{
    padding: 1.5rem 1rem;
    font-size: 1.75rem;
  }
  .b2b-landing .question.active + .answer {
    max-height: 350px;
  }
  .b2b-landing .question-item {
    padding-left: 0;
  }
  .b2b-landing .b2b__image-container,
.b2b-landing .b2b__content {
    width: 100%;
  }
  .b2b-landing .b2b__image {
    min-height: 53vw;
  }
  .b2b-landing .form {
    padding: 1.5rem;
  }
  .b2b-landing .b2b__content{
    background-image: linear-gradient(90deg, rgba(0, 0, 0, 0.8) 0%, rgba(0, 0, 0, 0.8) 40%, rgba(0, 0, 0, 0.85) 100%);
  }
  .b2b-landing .b2b__content ul{
    max-width: 100%;
  }
  .b2b-landing .b2b-1 {
    width: 100%;
    padding-left: 40px;
    padding-right: 40px;
  }
  .b2b-landing .b2b-2 {
    margin-top: 2rem;
    width: 100%;
    padding-left: 40px;
    padding-right: 40px;
  }
  .b2b-landing .b2b-3 {
    margin-top: 2rem;
    margin-bottom: 2rem;
    width: 100%;
    padding-left: 40px;
    padding-right: 40px;
  }
}
</style>