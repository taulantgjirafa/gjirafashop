<link rel="stylesheet" href="var/themes_repository/responsive/css/lib/peppermint.css">
{include file="views/pages/landing/svg-icons.tpl"}
<main class="">
<div class="msi-bg ssi-bg">
    <img class="hidden-phone" src="images/steel4.jpg" alt="">
    <img class="hidden-desktop" src="images/steelMob.jpg" alt="">
</div>
    <div class="pd-header ss-header">
        <ul class="flexing flex-h-between">
            <li class="{if $smarty.get.kategoria == $key}active{/if}" style="border-color:#fff !important;  {if $smarty.get.kategoria == $key} box-shadow:0 0 20px -4px #fff {/if}">
                <a href="{"pages.steelseries-landing"|fn_url}" style="text-decoration: none;">
                    <svg style="fill:#fff;"><use xlink:href="#steelseries"></use></svg>
                    <span>Steelseries</span>
                </a>
            </li>
            {assign var="color" value=$colors}
            {assign var="color_count" value=1}
            {foreach from=$categories item=category key=key}
            <li class="{if $smarty.get.kategoria == $key}active{/if}" style="border-color:#{$color[$color_count]} !important; {if $smarty.get.kategoria == $key} box-shadow:0 0 20px -4px #{$color[$color_count]}{/if}">
                <a href="{"pages.steelseries-landing&kategoria=`$key`"|fn_url}" style="text-decoration: none;">
                    <svg style="fill:#{$color[$color_count]};"><use xlink:href="#{$key}"></use></svg>
                    <span>{$category}</span>
                </a>
            </li>
            {assign var="color_count" value=$color_count + 1}
            {/foreach}
        </ul>
    </div>
<div class="pd-wrapper ss-wrapper">
{include file="views/pages/landing/products.tpl"}
</div>
</main>

<script type="text/javascript">
    function loadedContent() {
        $('.peppermint').css('opacity', '1');
        $('.msi-loader').css('display', 'none');
    }

    function goto(url) {
        window.location = url;
    }
</script>
<script type="text/javascript" onload="loadedContent();" src="js/lib/Peppermint.js?ver=4.3.9"></script>
<script>
    $('.peppermint').Peppermint({
        dots: true,
        speed: 500,
        slideshowInterval: 5000,
        stopSlideshowAfterInteraction: true,
        onSetup: function(n) {

        }
    });
    $('body').on('click', '.right-arr', function(){
        var slider = $(this).parent().parent().find('.peppermint');
        slider.data('Peppermint').next();
    });
    $('body').on('click', '.left-arr', function(){
        var slider = $(this).parent().parent().find('.peppermint');
        slider.data('Peppermint').prev();
    });
    $('body').on('click', '.expandDet', function(){
        $(this).prev().toggleClass('detOpen');
        $(this).toggleClass('rotSvg');
    });

    document.title = "Blej online Steelseries Produkte - Gjirafa50";
</script>



<style>
    .tygh-content {
        background: #111;
    }

    .load_more_category {
        background: #111;
    }

    #load_more_infinite {
        box-shadow: 0 4px 10px -4px #000;
    }

    .sold-out-msi {
        position: absolute;
        width: 100%;
        height: 100%;
        background-color: rgba(255,255,255,0.5);
    }

    .sold-out-msi h4 {
        border-radius: 3px;
        box-shadow: 1px 1px 2px 0 #bebebe;
        color: #111;
        width: auto;
        transform: none;
        font-weight: bold;
        background: none;
        border: none;
        box-shadow: none;position: absolute;
        top: 45%;
        left: 40%;
    }

    .msi-loader {
        background: url(design/themes/responsive/media/images/icons/loader.svg) no-repeat;
        background-size: contain;
        width: 50px;
        height: 50px;
        position: absolute;
        top: 40%;
        left: 40%;
    }

    .pd-wrapper .pd-image .peppermint-dots {
        margin-top: 35px;
    }
</style>