<div class="ty-wysiwyg-content">
    {hook name="pages:page_content"}
    <div {live_edit name="page:description:{$page.page_id}"}>{$page.description nofilter}</div>
    {/hook}
</div>

{capture name="mainbox_title"}<span {live_edit name="page:page:{$page.page_id}"}>{$page.page}</span>{/capture}
    
{hook name="pages:page_extra"}
{/hook}

{literal}
    <script>

        // Accordion
        function smoothScroll(div) {
            $('html, body').animate({
                scrollTop: $(div).offset().top - 70
            }, 500);
        }

        var selectedAcc ="";
        $('.accordion').on('click', function (e) {
            e.preventDefault();
            selectedAcc =  $(this).attr('id')
            $(this).addClass('active').siblings().removeClass('active');
            $(this).next().addClass('show').siblings().removeClass('show');
            window.location.hash = selectedAcc;
            smoothScroll(this);
        });

        var hash = window.location.hash;
        if (hash) {
            $(hash).click();
        }
        window.onpopstate = function(event) {
            return;
        }
    </script>
{/literal}