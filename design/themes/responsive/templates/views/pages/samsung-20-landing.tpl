<style>
    .tygh-content {
        background: #fff;
    }

    @font-face {
        font-family: "Samsung Sharp Sans Regular";
        src: url("{$config.https_location}/design/themes/responsive/media/fonts/SamsungSharpSansRegular-Regular.ttf");
    }

    @font-face {
        font-family: "Samsung Sharp Sans Bold";
        src: url("{$config.https_location}/design/themes/responsive/media/fonts/SamsungSharpSans-Bold.ttf");
    }

    .text-muted {
        color: #777 !important;
    }

    .samsung-landing .samsung-banner div {
        height: 85%;
    }

    .samsung-landing .samsung-top-p .ss-prod {
        width: 50%;
    }

    .samsung-landing .samsung-top-p .ss-prod > div {
        border-color: #000;
    }

    .samsung-landing .samsung-top-p .ss-prod:last-child > div {
        border: none;
    }

    .samsung-landing .samsung-top-p .ss-prod .ssp-content h3,
    .samsung-landing .samsung-top-p .ss-prod .ssp-content h2 {
        color: #000;
    }

    .product-overview h1 {
        color: #000;
    }

    .product-overview .po-item:before {
        background: linear-gradient(to right, transparent, #000, transparent);
    }

    .samsung-landing button:hover, .samsung-landing a:hover {
        border-color: #A07870;
    }

    .samsung-landing .compare-phone {
        padding-top: 0;
    }

    .samsung-landing .compare-phone button {
        background: #000;
        color: #fff !important;
    }

    @media screen and (max-width: 768px) {
        .samsung-landing .samsung-top-p .ss-prod {
            width: 80%;
        }

        .tygh-content {
            padding-top: 0;
        }

        .product-overview .po-content {
            margin-top: 25px;
        }
    }

    .product-overview h2 {
        font-family: "Samsung Sharp Sans Regular";
    }

    .samsung-landing .samsung-modal .s-modal-img {
        background: none;
    }
</style>
<span class="samsungoverlay"></span>
<div class="samsung-landing">
    <div class="samsung-modal">
        <span class="close-sam-modal"><svg><use xlink:href="#svg-back"></use></svg></span>
        <h1 style="text-align: center;">KRAHASONI MODELET</h1>
        <div class="s-modal-img flexing flex-h-between">
            <img style="width: 50%; height: 50%;" src="https://gjirafaadnetwork.blob.core.windows.net/html5/note20_posht.jpg" alt="">
            <img style="width: 50%; height: 50%;" src="https://gjirafaadnetwork.blob.core.windows.net/html5/note20ultra_posht.jpg" alt="">
        </div>
        <div class="s-modal-content">
            <div class="modal-table-head flexing flex-h-between mdc-els">
                <div> </div>
                <div>Galaxy Note20</div>
                <div>Galaxy Note20 Ultra</div>
            </div>
            <div class="modal-table-content">
                <div class="flexing flex-h-between mdc-els">
                    <div>Ngjyra</div>
                    <ul>
                        <li>
                            <span class="phone-color" style="background:#5e6367"></span>
                            <span>E hirtë mistike</span>
                        </li>
                        <li>
                            <span class="phone-color" style="background:#B8918A;"></span>
                            <span>E bronztë mistike</span>
                        </li>
                        <li>
                            <span class="phone-color" style="background:#9FBFA8;"></span>
                            <span>E gjelbër mistike</span>
                        </li>
                    </ul>
                    <ul>
                        <li>
                            <span class="phone-color" style="background:#000"></span>
                            <span>E zezë mistike</span>
                        </li>
                        <li>
                            <span class="phone-color" style="background:#B8918A;"></span>
                            <span>E bronztë mistike</span>
                        </li>
                        <li>
                            <span class="phone-color" style="background:#EFEFEF;"></span>
                            <span>E bardhë mistike</span>
                        </li>

                    </ul>
                </div>
                <div class="flexing flex-h-between mdc-els">
                    <div>Madhësia e ekranit</div>
                    <div>
                        <h1>6.7 inç</h1>
                    </div>
                    <div>
                        <h1>6.9 inç</h1>
                    </div>
                </div>
                <div class="flexing flex-h-between mdc-els">
                    <div>Rezolucion i lartë</div>
                    <div>
                        <h1>64MP</h1>
                    </div>
                    <div>
                        <h1>108MP</h1>
                    </div>
                </div>
                <div class="flexing flex-h-between mdc-els">
                    <div>Zmadhim</div>
                    <div>
                        <h1>30x</h1>
                    </div>
                    <div>
                        <h1>50x</h1>
                    </div>
                </div>
                <div class="flexing flex-h-between mdc-els">
                    <div>Bateria</div>
                    <div>
                        <h1>4300mAh</h1>
                    </div>
                    <div>
                        <h1>4500mAh</h1>
                    </div>
                </div>
            </div>
            <div class="modal-table-desc">
                <ul>
                    <li>
                        1. Disponueshmëria e ngjyrës mund të ndryshojë në varësi të shtetit ose operatorit.
                    </li>
                    <li>
                        2. Matur diagonalisht, madhësia e ekranit të Galaxy Note20 është 6.7" si drejtkëndësh i plotë dhe 6.6" duke llogaritur këndet e rrumbullakosura dhe Galaxy Note20 Ultra është 6.9" si drejtkëndësh i plotë dhe 6.8" duke llogaritur këndet e rrumbullakosura; zona reale e shikueshme është më pak për shkak të këndeve të rrumbullakëta dhe prerjes së kamerës.
                    </li>
                    <li>
                        3. Space Zoom përfshin një zmadhim dixhital, i cili mund të shkaktojë përkeqësim të imazhit.
                    </li>
                    <li>
                        4. Vlera tipike e testuar nën kushtet e laboratorit të palës së tretë. Vlera tipike është vlera mesatare e vlerësuar duke marrë parasysh devijimin në kapacitetin e baterive në mesin e mostrave të baterive të testuara sipas standardit IEC 61960. Kapaciteti i vlerësuar (minimumi) është 4170mAh për Galaxy Note20 dhe 4370mAh për Galaxy Note20 Ultra. Jeta aktuale e baterisë mund të ndryshojë në varësi të mjedisit të rrjetit, modeleve të përdorimit dhe faktorëve të tjerë.
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="samsung-banner">
        <div class="hidden-phone" style="background-image:url('https://hhstsyoejx.gjirafa.net/storage/gjmall/banners/1hMall/38288ab222cf49429bd8b75407d22b32_reduced-baner_1920x500.jpg');"></div>
        <div class="hidden-desktop" style="background-image:url('https://hhstsyoejx.gjirafa.net/storage/gjmall/banners/1hMall/aa0f1bd766444dc590ede34e8c08dccf_reduced-baner_600x400.jpg');"></div>
    </div>
    <div class="samsung-top-p">
        <div class="ss-prod entry-animation" style="transition-delay: .1s;" >
            <div>
                <div class="ssp-img" >
                    <div class="active s20blue changable-s" id="s20blue" style="background-image:url('https://hhstsyoejx.gjirafa.net/storage/gjmall/banners/1hMall/91e165a1db0a453bb2be8d3ccda2fa9e_reduced-kaltert.jpg')"></div>
                    <div class="s20green changable-s" id="s20green" style="background-image:url('https://hhstsyoejx.gjirafa.net/gj50/img/138724/img/0.jpg')"></div>
                    <div class="s20lavender changable-s" id="s20lavender" style="background-image:url('https://hhstsyoejx.gjirafa.net/gj50/img/138722/img/1.jpg')"></div>
                    <div class="s20white changable-s" id="s20white" style="background-image:url('https://hhstsyoejx.gjirafa.net/gj50/img/138719/img/0.jpg')"></div>
                </div>
                <div class="ssp-content">
                    <div class="phone-title">
                        <h3 class="s20blue active changable-s">Celular Samsung Galaxy S20 FE, 6GB RAM, 128GB, i kaltër</h3>
                        <h3 class="s20green changable-s">Celular Samsung Galaxy S20 FE, 6GB RAM, 128GB, i gjelbërt</h3>
                        <h3 class="s20lavender changable-s">Celular Samsung Galaxy S20 FE, 6GB RAM, 128GB, lavander</h3>
                        <h3 class="s20white changable-s">Celular Samsung Galaxy S20 FE, 6GB RAM, 128GB, i bardhë</h3>
                    </div>
                    <h3 class="text-muted" style="font-family: 'Samsung Sharp Sans Regular';">
                        Memorie e brendshme GB: <span style="font-family: 'Samsung Sharp Sans Bold';">128</span> <br /> Madhësia e ekranit inç:<span style="font-family: 'Samsung Sharp Sans Bold';"> 6.5</span>
                        <br /> Galaxy Fit 2:<span style="font-family: 'Samsung Sharp Sans Bold';"> FALAS</span>

                    </h3>
                    <!-- <div class="chose-colors">
                        <span class="phone-color active" data-color="s20blue" style="background:#285796; border:1px solid #333;"></span>
                        <span class="phone-color" data-color="s20green" style="background:green; border:1px solid #333;"></span>
                        <span class="phone-color" data-color="s20lavender" style="background:#fdc18b; border:1px solid #333;"></span>
                        <span class="phone-color" data-color="s20white" style="background:white; border:1px solid #333;"></span>
                    </div> -->
                    <div class="ssp-btn-price">
                        <h2>709.50€</h4>
                            <div class="phone-links">
                                <a class="s20blue active changable-s" href="{"products.view&product_id=138721"|fn_url}">Rezervo tani</a>
                                <a class="s20green changable-s" href="{"products.view&product_id=138724"|fn_url}">Rezervo tani</a>
                                <a class="s20lavender changable-s" href="{"products.view&product_id=138722"|fn_url}">Rezervo tani</a>
                                <a class="s20white changable-s" href="{"products.view&product_id=138723"|fn_url}">Rezervo tani</a>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="ss-prod entry-animation" style="transition-delay: .2s;" >
            <div>
                <div class="ssp-img" >
                    <div class=" s20blue changable-s" id="s20blue" style="background-image:url('https://hhstsyoejx.gjirafa.net/gj50/img/138717/img/0.jpg')"></div>
                    <div class="s20green active changable-s" id="s20green" style="background-image:url('https://hhstsyoejx.gjirafa.net/storage/gjmall/banners/1hMall/75ebd052ec8a4f85a9a00f5fc6bb0fc7_reduced-gjelbert.jpg')"></div>
                    <div class="s20lavender changable-s" id="s20lavender" style="background-image:url('https://hhstsyoejx.gjirafa.net/gj50/img/138722/img/1.jpg')"></div>
                    <div class="s20white changable-s" id="s20white" style="background-image:url('https://hhstsyoejx.gjirafa.net/gj50/img/138719/img/0.jpg')"></div>
                </div>
                <div class="ssp-content">
                    <div class="phone-title">
                        <h3 class="s20blue  changable-s">Celular Samsung Galaxy S20 FE, 6GB RAM, 128GB, i kaltër</h3>
                        <h3 class="s20green active changable-s">Celular Samsung Galaxy S20 FE, 6GB RAM, 128GB, i gjelbërt</h3>
                        <h3 class="s20lavender changable-s">Celular Samsung Galaxy S20 FE, 6GB RAM, 128GB, lavander</h3>
                        <h3 class="s20white changable-s">Celular Samsung Galaxy S20 FE, 6GB RAM, 128GB, i bardhë</h3>
                    </div>
                    <h3 class="text-muted" style="font-family: 'Samsung Sharp Sans Regular';">
                        Memorie e brendshme GB: <span style="font-family: 'Samsung Sharp Sans Bold';">128</span> <br /> Madhësia e ekranit inç:<span style="font-family: 'Samsung Sharp Sans Bold';"> 6.5</span>
                        <br /> Galaxy Fit 2:<span style="font-family: 'Samsung Sharp Sans Bold';"> FALAS</span>

                    </h3>
                    <!-- <div class="chose-colors">
                        <span class="phone-color active" data-color="s20blue" style="background:#285796; border:1px solid #333;"></span>
                        <span class="phone-color" data-color="s20green" style="background:green; border:1px solid #333;"></span>
                        <span class="phone-color" data-color="s20lavender" style="background:#fdc18b; border:1px solid #333;"></span>
                        <span class="phone-color" data-color="s20white" style="background:white; border:1px solid #333;"></span>
                    </div> -->
                    <div class="ssp-btn-price">
                        <h2>709.50€</h4>
                            <div class="phone-links">
                                <a class="s20blue  changable-s" href="{"products.view&product_id=138721"|fn_url}">Rezervo tani</a>
                                <a class="s20green active changable-s" href="{"products.view&product_id=138724"|fn_url}">Rezervo tani</a>
                                <a class="s20lavender changable-s" href="{"products.view&product_id=138722"|fn_url}">Rezervo tani</a>
                                <a class="s20white changable-s" href="{"products.view&product_id=138723"|fn_url}">Rezervo tani</a>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="ss-prod entry-animation" style="transition-delay: .3s;" >
            <div>
                <div class="ssp-img" >
                    <div class=" s20blue changable-s" id="s20blue" style="background-image:url('https://hhstsyoejx.gjirafa.net/gj50/img/138717/img/0.jpg')"></div>
                    <div class="s20green changable-s" id="s20green" style="background-image:url('https://hhstsyoejx.gjirafa.net/gj50/img/138724/img/0.jpg')"></div>
                    <div class="s20lavender active changable-s" id="s20lavender" style="background-image:url('https://hhstsyoejx.gjirafa.net/storage/gjmall/banners/1hMall/32dd16c59be94c25b82113e2527299ad_reduced-lavander.jpg')"></div>
                    <div class="s20white changable-s" id="s20white" style="background-image:url('https://hhstsyoejx.gjirafa.net/gj50/img/138719/img/0.jpg')"></div>
                </div>
                <div class="ssp-content">
                    <div class="phone-title">
                        <h3 class="s20blue  changable-s">Celular Samsung Galaxy S20 FE, 6GB RAM, 128GB, i kaltër</h3>
                        <h3 class="s20green changable-s">Celular Samsung Galaxy S20 FE, 6GB RAM, 128GB, i gjelbërt</h3>
                        <h3 class="s20lavender active changable-s">Celular Samsung Galaxy S20 FE, 6GB RAM, 128GB, lavander</h3>
                        <h3 class="s20white changable-s">Celular Samsung Galaxy S20 FE, 6GB RAM, 128GB, i bardhë</h3>
                    </div>
                    <h3 class="text-muted" style="font-family: 'Samsung Sharp Sans Regular';">
                        Memorie e brendshme GB: <span style="font-family: 'Samsung Sharp Sans Bold';">128</span> <br /> Madhësia e ekranit inç:<span style="font-family: 'Samsung Sharp Sans Bold';"> 6.5</span>
                        <br /> Galaxy Fit 2:<span style="font-family: 'Samsung Sharp Sans Bold';"> FALAS</span>

                    </h3>
                    <!-- <div class="chose-colors">
                        <span class="phone-color active" data-color="s20blue" style="background:#285796; border:1px solid #333;"></span>
                        <span class="phone-color" data-color="s20green" style="background:green; border:1px solid #333;"></span>
                        <span class="phone-color" data-color="s20lavender" style="background:#fdc18b; border:1px solid #333;"></span>
                        <span class="phone-color" data-color="s20white" style="background:white; border:1px solid #333;"></span>
                    </div> -->
                    <div class="ssp-btn-price">
                        <h2>709.50€</h4>
                            <div class="phone-links">
                                <a class="s20blue  changable-s" href="{"products.view&product_id=138721"|fn_url}">Rezervo tani</a>
                                <a class="s20green changable-s" href="{"products.view&product_id=138724"|fn_url}">Rezervo tani</a>
                                <a class="s20lavender active changable-s" href="{"products.view&product_id=138722"|fn_url}">Rezervo tani</a>
                                <a class="s20white changable-s" href="{"products.view&product_id=138723"|fn_url}">Rezervo tani</a>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="ss-prod entry-animation" style="transition-delay: .4s;" >
            <div>
                <div class="ssp-img" >
                    <div class=" s20blue changable-s" id="s20blue" style="background-image:url('https://hhstsyoejx.gjirafa.net/gj50/img/138717/img/0.jpg')"></div>
                    <div class="s20green changable-s" id="s20green" style="background-image:url('https://hhstsyoejx.gjirafa.net/gj50/img/138724/img/0.jpg')"></div>
                    <div class="s20lavender changable-s" id="s20lavender" style="background-image:url('https://hhstsyoejx.gjirafa.net/gj50/img/138722/img/1.jpg')"></div>
                    <div class="s20white active changable-s" id="s20white" style="background-image:url('https://hhstsyoejx.gjirafa.net/storage/gjmall/banners/1hMall/ef40b3e9171e459dbdd923afe96ca9f3_reduced-bardh.jpg')"></div>
                </div>
                <div class="ssp-content">
                    <div class="phone-title">
                        <h3 class="s20blue  changable-s">Celular Samsung Galaxy S20 FE, 6GB RAM, 128GB, i kaltër</h3>
                        <h3 class="s20green changable-s">Celular Samsung Galaxy S20 FE, 6GB RAM, 128GB, i gjelbërt</h3>
                        <h3 class="s20lavender changable-s">Celular Samsung Galaxy S20 FE, 6GB RAM, 128GB, lavander</h3>
                        <h3 class="s20white active changable-s">Celular Samsung Galaxy S20 FE, 6GB RAM, 128GB, i bardhë</h3>
                    </div>
                    <h3 class="text-muted" style="font-family: 'Samsung Sharp Sans Regular';">
                        Memorie e brendshme GB: <span style="font-family: 'Samsung Sharp Sans Bold';">128</span> <br /> Madhësia e ekranit inç:<span style="font-family: 'Samsung Sharp Sans Bold';"> 6.5</span>
                        <br /> Galaxy Fit 2:<span style="font-family: 'Samsung Sharp Sans Bold';"> FALAS</span>

                    </h3>
                    <!-- <div class="chose-colors">
                        <span class="phone-color active" data-color="s20blue" style="background:#285796; border:1px solid #333;"></span>
                        <span class="phone-color" data-color="s20green" style="background:green; border:1px solid #333;"></span>
                        <span class="phone-color" data-color="s20lavender" style="background:#fdc18b; border:1px solid #333;"></span>
                        <span class="phone-color" data-color="s20white" style="background:white; border:1px solid #333;"></span>
                    </div> -->
                    <div class="ssp-btn-price">
                        <h2>709.50€</h4>
                            <div class="phone-links">
                                <a class="s20blue  changable-s" href="{"products.view&product_id=138721"|fn_url}">Rezervo tani</a>
                                <a class="s20green changable-s" href="{"products.view&product_id=138724"|fn_url}">Rezervo tani</a>
                                <a class="s20lavender changable-s" href="{"products.view&product_id=138722"|fn_url}">Rezervo tani</a>
                                <a class="s20white active changable-s" href="{"products.view&product_id=138723"|fn_url}">Rezervo tani</a>
                            </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- <div class="flexing compare-phone flex-h-center">
        <button class="">KRAHASO MODELET</button>
    </div> -->


    <div class="product-overview">
        <div class="po-item flexing">
            <div class="po-content text-right">
                <h1>Ngjyra që bëjnë xheloz dhe ylberin </h1>
                <h2 class="text-muted" style="line-height: 26px; word-spacing: 5px;">
                    Telefoni juaj nuk ju largohet kurrë nga dora, ndaj duhet të jetë një shtesë optimale për stilin tuaj. Zgjidhni nga një larmi e gjerë nuancash në modë me lustër elegante mat, nga të mëdhatë e të fortat deri tek ato më me takt dhe klasiket.1
                </h2>
                <a href="{"products.view&product_id=138721"|fn_url}">Rezervo tani</a>
            </div>

            <div class="po-img">
                <img src="https://hhstsyoejx.gjirafa.net/storage/gjmall/banners/1hMall/f1c1689607bd4720a887302e6ea28aaf_reduced-baneri1.jpg" />
            </div>
        </div>
        <div class="po-item flexing">
            <div class="po-img">
                <img src="https://hhstsyoejx.gjirafa.net/storage/gjmall/banners/1hMall/acf14c40bba94ce48b24e46f6e91c5f0_reduced-baneri2.jpg" />
            </div>
            <div class="po-content">
                <h1>Kamera e nivelit profesional me tri objektiva</h1>
                <h2 class="text-muted" style="line-height: 26px; word-spacing: 5px;">
                    Tre kamera nga mbrapa ju mundësojnë realizimin me lehtësi të atyre pozave profesionale të nivelit #nofilter. Inkuadrojeni skenën me kamerën këndgjerë, pastaj kaloni më gjerë me kamerën ultra të gjerë ose zmadhoni në zmadhim optik 3x në kamerën telefoto.
                </h2>
                <a href="{"products.view&product_id=138721"|fn_url}">Rezervo tani</a>
            </div>
        </div>
    </div>
</div>

<script>
    var banner = document.querySelector('.samsung-banner'),
        leftSpace = banner.offsetLeft;

    banner.style.marginLeft = '-' + leftSpace + 'px';
    banner.style.width = 'calc(100% + ' + leftSpace * 2 + 'px)';



    animationEL = document.querySelectorAll('.entry-animation');
    for (var i = 0; i < animationEL.length; i++) {
        animationEL[i].classList.add('active');
    }

    var phoneColors = document.querySelectorAll('.chose-colors .phone-color');

    for (var i = 0; i < phoneColors.length; i++) {
        phoneColors[i].onclick = function() {
            var thisPhoneColor = this.getAttribute('data-color'),
                thisClosestItem = this.closest('.ss-prod'),
                changableItems = thisClosestItem.querySelectorAll('.changable-s'),
                clickColorParent = this.parentElement.children;

            for (var k = 0; k < clickColorParent.length; k++) {
                clickColorParent[k].classList.remove('active');
                this.classList.add('active');
            }


            for (var j = 0; j < changableItems.length; j++) {
                changableItems[j].classList.remove('active');
            }
            document.querySelectorAll('.' + thisPhoneColor)[0].classList.add('active');
            document.querySelectorAll('.' + thisPhoneColor)[1].classList.add('active');
            document.querySelectorAll('.' + thisPhoneColor)[2].classList.add('active');

        }
    }

    var compareBtn = document.querySelector('.compare-phone button'),
        sModal = document.querySelector('.samsung-modal'),
        sOverlay = document.querySelector('.samsungoverlay'),
        closeSmodal = document.querySelector('.close-sam-modal');



    closeSmodal.onclick = function() {
        sOverlay.classList.remove('active');
        sModal.classList.remove('active');
        setTimeout(function() {
            sOverlay.style.display = 'none';
            sModal.style.display = 'none';
        }, 500);
    }

    compareBtn.onclick = function() {
        sOverlay.style.display = 'block';
        sModal.style.display = 'block';
        window.scrollTo(0, 150);
        setTimeout(function() {
            sOverlay.classList.add('active');
            sModal.classList.add('active');
        });
    }
</script>

<style>
    @media screen and (max-width:768px) {
        .samsung-landing .samsung-top-p .ss-prod {
            display:block;
            width:100%;
            box-sizing:border-box;
        }
        .samsung-landing .samsung-top-p .ss-prod > div {
            border-right:0;
            border-bottom:1px solid #000;
        }
    }
</style>