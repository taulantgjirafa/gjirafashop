<style>
    .tygh-content {
        background: #fff;
    }
    
    @font-face {
        font-family: "Samsung Sharp Sans Regular";
        src: url("{$config.https_location}/design/themes/responsive/media/fonts/SamsungSharpSansRegular-Regular.ttf");
    }

    @font-face {
        font-family: "Samsung Sharp Sans Bold";
        src: url("{$config.https_location}/design/themes/responsive/media/fonts/SamsungSharpSans-Bold.ttf");
    }
    
    .text-muted {
        color: #777 !important;
    }

    .samsung-landing .samsung-banner div {
        height: 85%;
    }

    .samsung-landing .samsung-top-p .ss-prod {
        width: 50%;
    }

    .samsung-landing .samsung-top-p .ss-prod > div {
        border-color: #000;
    }

    .samsung-landing .samsung-top-p .ss-prod:last-child > div {
        border: none;
    }

    .samsung-landing .samsung-top-p .ss-prod .ssp-content h3,
    .samsung-landing .samsung-top-p .ss-prod .ssp-content h2 {
        color: #000;
    }

    .product-overview h1 {
        color: #000;
    }

    .product-overview .po-item:before {
        background: linear-gradient(to right, transparent, #000, transparent);
    }

    .samsung-landing button:hover, .samsung-landing a:hover {
        border-color: #A07870;
    }

    .samsung-landing .compare-phone {
        padding-top: 0;
    }

    .samsung-landing .compare-phone button {
        background: #000;
        color: #fff !important;
    }

    @media screen and (max-width: 768px) {
        .samsung-landing .samsung-top-p .ss-prod {
            width: 80%;
        }

        .tygh-content {
            padding-top: 0;
        }

        .product-overview .po-content {
            margin-top: 25px;
        }
    }

    .product-overview h2 {
        font-family: "Samsung Sharp Sans Regular";
    }

    .samsung-landing .samsung-modal .s-modal-img {
        background: none;
    }
</style>
<span class="samsungoverlay"></span>
<div class="samsung-landing">
    <div class="samsung-modal">
        <span class="close-sam-modal"><svg><use xlink:href="#svg-back"></use></svg></span>
        <h1 style="text-align: center;">KRAHASONI MODELET</h1>
        <div class="s-modal-img flexing flex-h-between">
            <img style="width: 50%; height: 50%;" src="https://gjirafaadnetwork.blob.core.windows.net/html5/note20_posht.jpg" alt="">
            <img style="width: 50%; height: 50%;" src="https://gjirafaadnetwork.blob.core.windows.net/html5/note20ultra_posht.jpg" alt="">
        </div>
        <div class="s-modal-content">
            <div class="modal-table-head flexing flex-h-between mdc-els">
                <div> </div>
                <div>Galaxy Note20</div>
                <div>Galaxy Note20 Ultra</div>
            </div>
            <div class="modal-table-content">
                <div class="flexing flex-h-between mdc-els">
                    <div>Ngjyra</div>
                    <ul>
                        <li>
                            <span class="phone-color" style="background:#5e6367"></span>
                            <span>E hirtë mistike</span>
                        </li>
                        <li>
                            <span class="phone-color" style="background:#B8918A;"></span>
                            <span>E bronztë mistike</span>
                        </li>
                        <li>
                            <span class="phone-color" style="background:#9FBFA8;"></span>
                            <span>E gjelbër mistike</span>
                        </li>
                    </ul>
                    <ul>
                        <li>
                            <span class="phone-color" style="background:#000"></span>
                            <span>E zezë mistike</span>
                        </li>
                        <li>
                            <span class="phone-color" style="background:#B8918A;"></span>
                            <span>E bronztë mistike</span>
                        </li>
                        <li>
                            <span class="phone-color" style="background:#EFEFEF;"></span>
                            <span>E bardhë mistike</span>
                        </li>

                    </ul>
                </div>
                <div class="flexing flex-h-between mdc-els">
                    <div>Madhësia e ekranit</div>
                    <div>
                        <h1>6.7 inç</h1>
                    </div>
                    <div>
                        <h1>6.9 inç</h1>
                    </div>
                </div>
                <div class="flexing flex-h-between mdc-els">
                    <div>Rezolucion i lartë</div>
                    <div>
                        <h1>64MP</h1>
                    </div>
                    <div>
                        <h1>108MP</h1>
                    </div>
                </div>
                <div class="flexing flex-h-between mdc-els">
                    <div>Zmadhim</div>
                    <div>
                        <h1>30x</h1>
                    </div>
                    <div>
                        <h1>50x</h1>
                    </div>
                </div>
                <div class="flexing flex-h-between mdc-els">
                    <div>Bateria</div>
                    <div>
                        <h1>4300mAh</h1>
                    </div>
                    <div>
                        <h1>4500mAh</h1>
                    </div>
                </div>
            </div>
            <div class="modal-table-desc">
                <ul>
                    <li>
                        1. Disponueshmëria e ngjyrës mund të ndryshojë në varësi të shtetit ose operatorit.
                    </li>
                    <li>
                        2. Matur diagonalisht, madhësia e ekranit të Galaxy Note20 është 6.7" si drejtkëndësh i plotë dhe 6.6" duke llogaritur këndet e rrumbullakosura dhe Galaxy Note20 Ultra është 6.9" si drejtkëndësh i plotë dhe 6.8" duke llogaritur këndet e rrumbullakosura; zona reale e shikueshme është më pak për shkak të këndeve të rrumbullakëta dhe prerjes së kamerës.
                    </li>
                    <li>
                        3. Space Zoom përfshin një zmadhim dixhital, i cili mund të shkaktojë përkeqësim të imazhit.
                    </li>
                    <li>
                        4. Vlera tipike e testuar nën kushtet e laboratorit të palës së tretë. Vlera tipike është vlera mesatare e vlerësuar duke marrë parasysh devijimin në kapacitetin e baterive në mesin e mostrave të baterive të testuara sipas standardit IEC 61960. Kapaciteti i vlerësuar (minimumi) është 4170mAh për Galaxy Note20 dhe 4370mAh për Galaxy Note20 Ultra. Jeta aktuale e baterisë mund të ndryshojë në varësi të mjedisit të rrjetit, modeleve të përdorimit dhe faktorëve të tjerë.
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="samsung-banner">
        <div class="hidden-phoone" style="background-image:url('https://hhstsyoejx.gjirafa.net/gj50/img/500001/img/0.jpg');"></div>
        <div class="hidden-desktop" style="background-image:url('https://hhstsyoejx.gjirafa.net/gj50/img/500001/img/1.jpg');"></div>
    </div>
    <div class="samsung-top-p">
        <div class="ss-prod entry-animation" style="transition-delay: .1s;">
            <div>
                <div class="ssp-img">
                    <div class="active s20blue changable-s" id="s20blue" style="background-image:url('https://gjirafaadnetwork.blob.core.windows.net/html5/Note20/Roze.jpg')"></div>
                    <div class="s20gray changable-s" id="s20gray" style="background-image:url('https://gjirafaadnetwork.blob.core.windows.net/html5/Note20/Gray.jpg')"></div>
                    <div class="s20pink changable-s" id="s20pink" style="background-image:url('https://gjirafaadnetwork.blob.core.windows.net/html5/Note20/Gjelbert.jpg')"></div>
                </div>
                <div class="ssp-content">
                    <div class="phone-title">
                        <h3 class="s20blue active changable-s">Samsung Galaxy Note20, e bronztë mistike</h3>
                        <h3 class="s20gray changable-s">Samsung Galaxy Note20, e hirtë mistike</h3>
                        <h3 class="s20pink changable-s">Samsung Galaxy Note20, e gjelbër mistike</h3>
                    </div>
                    <h3 class="text-muted" style="font-family: 'Samsung Sharp Sans Regular';">
                        Memorie e brendshme GB: <span style="font-family: 'Samsung Sharp Sans Bold';">256</span> <br /> Madhësia e ekranit inç:<span style="font-family: 'Samsung Sharp Sans Bold';"> 6.7</span>
                        <br /> Galaxy Buds+:<span style="font-family: 'Samsung Sharp Sans Bold';"> FALAS</span>

                    </h3>
                    <div class="chose-colors">
                        <span class="phone-color " data-color="s20gray" style="background:#5e6367"></span>
                        <span class="phone-color active " data-color="s20blue" style="background:#B8918A"></span>
                        <span class="phone-color " data-color="s20pink" style="background:#9FBFA8"></span>
                    </div>
                    <div class="ssp-btn-price">
                        <h2>1039.50€</h4>
                            <div class="phone-links">
                                <a class="s20blue active changable-s" href="{"products.view&product_id=130605"|fn_url}">Rezervo tani</a>
                                <a class="s20pink changable-s" href="{"products.view&product_id=130606"|fn_url}">Rezervo tani</a>
                                <a class="s20gray changable-s" href="{"products.view&product_id=130607"|fn_url}">Rezervo tani</a>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="ss-prod entry-animation" style="transition-delay: .22s;">
            <div>
                <div class="ssp-img">
                    <div class="s20plusblack changable-s" id="s20plusblack" style="background-image:url('https://gjirafaadnetwork.blob.core.windows.net/html5/note20ultra/black.jpg')"></div>
                    <div class="active s20plusgray changable-s" id="s20plusgray" style="background-image:url('https://gjirafaadnetwork.blob.core.windows.net/html5/note20ultra/bronze.jpg')"></div>
                    <div class="s20plusblue changable-s" id="s20plusblue" style="background-image:url('https://gjirafaadnetwork.blob.core.windows.net/html5/note20ultra/white.jpg"></div>
                </div>
                <div class="ssp-content">
                    <div class="phone-title">
                        <h3 class="s20plusblack active changable-s">Samsung Galaxy Note20 Ultra, zezë mistike</h3>
                        <h3 class="s20plusgray changable-s">Samsung Galaxy Note20 Ultra, e bronztë mistike</h3>
                        <h3 class="s20plusblue changable-s">Samsung Galaxy Note20 Ultra, e bardhë mistike</h3>
                    </div>

                    <h3 class="text-muted" style="font-family: 'Samsung Sharp Sans Regular'">
                        Memorie e brendshme GB: <span style="font-family: 'Samsung Sharp Sans Bold';">256</span> <br /> Madhësia e ekranit inç:<span style="font-family: 'Samsung Sharp Sans Bold';"> 6.9</span>
                        <br /> Galaxy Buds Live:<span style="font-family: 'Samsung Sharp Sans Bold';"> FALAS</span>

                    </h3>
                    <div class="chose-colors">
                        <span class="phone-color " data-color="s20plusblack" style="background:#000"></span>
                        <span class="phone-color active " data-color="s20plusgray" style="background:#B8918A"></span>
                        <span class="phone-color " data-color="s20plusblue" style="background:#EFEFEF"></span>
                    </div>
                    <div class="ssp-btn-price">
                        <h2>1299.50€</h4>
                            <div class="phone-links">
                                <a class="s20plusblack changable-s" href="{"products.view&product_id=130609"|fn_url}">Rezervo tani</a>
                                <a class="s20plusgray active changable-s" href="{"products.view&product_id=130608"|fn_url}">Rezervo tani</a>
                                <a class="s20plusblue changable-s" href="{"products.view&product_id=130610"|fn_url}">Rezervo tani</a>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="flexing compare-phone flex-h-center">
        <button class="">KRAHASO MODELET</button>
    </div>
    <div class="product-overview">
        <div class="po-item flexing">
            <div class="po-content text-right">
                <h1>Samsung Galaxy Note20 </h1>
                <h2 class="text-muted">
                    Njihuni me Samsung Galaxy Note20, apo telefonin që do të ndryshojë përgjithmonë mënyrën se si punoni dhe luani. Me ekranin 6.7” Infinity-O, lapsin e ri S Pen, sistemin e kamerës 64 MP, dhe super performancë të nivelit kompjuterik.

                </h2>
                <a href="{"products.view&product_id=130605"|fn_url}">Rezervo tani</a>
            </div>

            <div class="po-img">
                <img src="https://gjirafaadnetwork.blob.core.windows.net/html5/note20_posht.jpg" />
            </div>
        </div>
        <div class="po-item flexing">
            <div class="po-img">
                <img src="https://gjirafaadnetwork.blob.core.windows.net/html5/note20ultra_posht.jpg" />
            </div>
            <div class="po-content">
                <h1>Samsung Galaxy Note20 Ultra</h1>
                <h2 class="text-muted">
                    Samsung Galaxy Note20 Ultra, ju sjell ekranin më të fortë ndonjëherë 6.9” Infinity-O 120Hz, kamerën e nivelit profesionist me rezolucion 108MP dhe 8K, lapsin e ri S Pen, për një përvojë të paparë deri më tani.
                </h2>
                <a href="{"products.view&product_id=130609"|fn_url}">Rezervo tani</a>
            </div>
        </div>
    </div>
</div>

<script>
    var banner = document.querySelector('.samsung-banner'),
        leftSpace = banner.offsetLeft;

    banner.style.marginLeft = '-' + leftSpace + 'px';
    banner.style.width = 'calc(100% + ' + leftSpace * 2 + 'px)';



    animationEL = document.querySelectorAll('.entry-animation');
    for (var i = 0; i < animationEL.length; i++) {
        animationEL[i].classList.add('active');
    }

    var phoneColors = document.querySelectorAll('.chose-colors .phone-color');

    for (var i = 0; i < phoneColors.length; i++) {
        phoneColors[i].onclick = function() {
            var thisPhoneColor = this.getAttribute('data-color'),
                thisClosestItem = this.closest('.ss-prod'),
                changableItems = thisClosestItem.querySelectorAll('.changable-s'),
                clickColorParent = this.parentElement.children;

            for (var k = 0; k < clickColorParent.length; k++) {
                clickColorParent[k].classList.remove('active');
                this.classList.add('active');
            }


            for (var j = 0; j < changableItems.length; j++) {
                changableItems[j].classList.remove('active');
            }
            document.querySelectorAll('.' + thisPhoneColor)[0].classList.add('active');
            document.querySelectorAll('.' + thisPhoneColor)[1].classList.add('active');
            document.querySelectorAll('.' + thisPhoneColor)[2].classList.add('active');

        }
    }

    var compareBtn = document.querySelector('.compare-phone button'),
        sModal = document.querySelector('.samsung-modal'),
        sOverlay = document.querySelector('.samsungoverlay'),
        closeSmodal = document.querySelector('.close-sam-modal');



    closeSmodal.onclick = function() {
        sOverlay.classList.remove('active');
        sModal.classList.remove('active');
        setTimeout(function() {
            sOverlay.style.display = 'none';
            sModal.style.display = 'none';
        }, 500);
    }

    compareBtn.onclick = function() {
        sOverlay.style.display = 'block';
        sModal.style.display = 'block';
        window.scrollTo(0, 150);
        setTimeout(function() {
            sOverlay.classList.add('active');
            sModal.classList.add('active');
        });
    }
</script>