<title>Black friday Gjirafa50 - Deri 70% ZBRITJE</title>
<meta itemprop="name" content="Black friday Gjirafa50 - Deri 70% ZBRITJE">
<meta itemprop="description" content="Gjirafa50.com, dyqani më i madh për teknologji në rajon për dy vitet e kaluara ka ofruar zbritje marramendëse në shumë produkte teknologjike, çka ka bërë që interesimi për blerje në Gjirafa50 të ishte i jashtëzakonshëm.">
<meta itemprop="image" content="https://hhstsyoejx.gjirafa.net/gj50/landings/black-friday/og_image.jpg">
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:title" content="Black friday Gjirafa50 - Deri 70% ZBRITJE">
<meta name="twitter:description" content=" Gjirafa50.com, dyqani më i madh për teknologji në rajon për dy vitet e kaluara ka ofruar zbritje marramendëse në shumë produkte teknologjike për Black-Friday. Edhe sivjet tradita vazhdon, deri 70% zbritje!">
<meta name="twitter:image:src" content="https://hhstsyoejx.gjirafa.net/gj50/landings/black-friday/og_image.jpg">
<meta property="og:title" content="Black friday Gjirafa50 - Deri 70% ZBRITJE"/>
<meta property="og:url" content="https://gjirafa50.com/black-friday-2018"/>
<meta property="og:image" content="https://hhstsyoejx.gjirafa.net/gj50/landings/black-friday/og_image.jpg"/>
<meta property="og:image:width" content="200" />
<meta property="og:image:height" content="200" />
<meta property="og:description" content=" Gjirafa50.com, dyqani më i madh për teknologji në rajon për dy vitet e kaluara ka ofruar zbritje marramendëse në shumë produkte teknologjike për Black-Friday. Edhe sivjet tradita vazhdon, deri 70% zbritje!"/>
{include file="views/pages/black-friday/style.tpl"}
<link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
<div class="heading">
    <img src="https://hhstsyoejx.gjirafa.net/gj50/landings/{$key}/cover{if $smarty.const.IS_MOBILE}-mob{/if}.jpg"
         alt="">
    <div id="bf-countdown" class="text-center"></div>
    <h2 class="ty-center" id="bf-heading">SASIA E LIMITUAR!</h2>
</div>
<div class="content product-masonry">
    <div class="container">
        {assign "iteration" 0}
        {foreach $categorized_products as $cproducts}
            {if $iteration == 1}
                <div class="span16">
                    <a href="#" id="rbko-bnr">
                        <img src="images/black-friday18/RBKO_500X150.jpg" style="margin: 25px auto;display: block;" alt="Këto produkte mund të blihen vetëm me bonus kartele të Raiffeisen bank.">
                    </a>
                </div>
            {/if}
            <div class="product-masonry grid-list span16" {if $iteration == 2}id="rbko" style="margin:20px 0 0 0;" {else} style="margin:0;"{/if}>
                {if $iteration == 2}
                    <h2 id="rbko-title">
                        <img id="rbko" src="images/black-friday18/rbko_logo.png" alt="Këto produkte mund të blihen vetëm me bonus kartele të Raiffeisen bank.">
                        Këto produkte mund të blihen vetëm me bonus kartele të Raiffeisen bank.
                    </h2>
                {/if}
                {foreach $cproducts as $product}
                    {assign var="product" value=$elastic_products[$product.product_code]}
                    {if !$product}{continue}{/if}
                    <div class="ty-column5 ty-grid-list__item--container">
                        <div class="ty-grid-list__image">
                            <a href="https://gjirafa50.com/index.php?dispatch=products.view&product_id={$product.product_id}">
                                <img class="lazyload"
                                     src="{get_images_from_blob product_id=$product.product_id count=1}"
                                     alt="{$product.product}">
                            </a>
                            {if $product.old_price == null}
                                {$product.old_price = $product.price}
                            {/if}
                            {if !$hide_price || $iteration == 1}
                                <div class="discount-label">
                                    {math assign="savex" equation='(x-y)' x=$product.old_price y=$csv_products[$product.product_code].new_price}
                                    {if $savex <= 10}
                                        {math assign="sale_percentage" equation='((x-y)*100)/x' x=$product.old_price y=$csv_products[$product.product_code].new_price}
                                        <span>Zbritje</span>
                                        {$sale_percentage|intval}%
                                    {else}
                                        <span>Kurseni</span>
                                        {$savex}€
                                    {/if}
                                </div>
                            {/if}
                        </div>
                        <a href="https://gjirafa50.com/index.php?dispatch=products.view&product_id={$product.product_id}">
                            <div class="ty-grid-list__item-details">
                                <h3 class="title">{$product.product}</h3>
                                {if !$hide_price || $iteration == 1}
                                    <h2 class="price">
                                        <span class="o-price">{$product.old_price}€</span> <br/>
                                        {$csv_products[$product.product_code].new_price}€
                                    </h2>
                                {/if}
                            </div>
                        </a>
                        {if $product.total_stock == 0 && !$hide_price}
                            <div class="sold-out"><h4>E SHITUR</h4></div>
                        {/if}
                        {if $product.amount > 0}
                            <img class="stockimage" src="https://gjirafa50.com/images/icons/upTo48.png"/>
                        {/if}
                    </div>
                {/foreach}
            </div>
            {$iteration = $iteration + 1}
        {/foreach}

    </div>
</div>
<script>
    {if $smarty.get.rbko}
        $(window).load(function () {
            $('#rbko-bnr').click();
        });
        {/if}
    {if $hide_price}
    var end_date = '{$landing_config.hide_prices_until}';
            {literal}
    $('#rbko-bnr').click(function(){
        $('html, body').animate({
            scrollTop: $('#rbko').offset().top
        }, 800);
        return false;
    });
    var end = new Date(end_date);
    console.log(end);
    var _second = 1000;
    var _minute = _second * 60;
    var _hour = _minute * 60;
    var _day = _hour * 24;
    var timer;
    function showRemaining() {
        var now = new Date();
        var distance = end - now;
        if (distance < 0) {
            clearInterval(timer);
            document.getElementById('bf-countdown').innerHTML = '';
            return
        }
        var days = Math.floor(distance / _day);
        var hours = Math.floor((distance % _day) / _hour);
        var minutes = Math.floor((distance % _hour) / _minute);
        var seconds = Math.floor((distance % _minute) / _second);
        document.getElementById('bf-countdown').innerHTML = 'Fillon edhe <br/><span>' + days + '</span> ditë <span>' + hours + '</span> ore <span>' + minutes + '</span> min <span>' + seconds + '</span> sek';
    }
    timer = setInterval(showRemaining, 1000);{/literal}
    {/if}
</script>
