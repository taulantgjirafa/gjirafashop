{capture name="testtt"}
    {if $boosted_products != null}
        <div class="bproducts">
            <h1 class="ty-center text-light">Ja vlen me ja l'shu ni sy!</h1>
            <div class="bproduct-wrapper">
        {foreach $boosted_products as $product}
            <div class="bproduct">
                <div class="bproduct-inner">
                <div class="img" style="background-image:url({get_images_from_blob product_id=$product.product_id count=1})"></div>
                <div class="content">
                    <h4>{$product.product|truncate:70:"...":true}</h4>
                    <h1>{$product.price}</h1>
                </div>
            </div>
            </div>
        {/foreach}
            </div>
        </div>
    {/if}
{/capture}
<link rel="stylesheet" href="images/black-friday17/bf22.css?v={__("cache_landing_version")}"/>
<style>.bf-cover {ldelim}background-image: url(https://hhstsyoejx.gjirafa.net/gj50/landings/{$key}/cover{if $is_mobile && $key == "raiffeisen"}-mob{/if}.jpg);{rdelim}
    .product.small .save-percentage, .product.medium .save-percentage {ldelim}background:#{$landing_config.percentage_bg_color};{rdelim}
    .product.small .save-percentage span, .product.medium .save-percentage span {ldelim}color:#{$landing_config.percentage_color};{rdelim}
    .product.small .bnow, .product.medium .bnow {ldelim}background:#{$landing_config.buynow_color};{rdelim}
    .bf-cover, .bf-bottom {ldelim}background-color: #{$landing_config.bg_color};{rdelim}
    #bf-countdown {ldelim}background-color: #{$landing_config.bg_color};{rdelim}
    #bf-countdown span {ldelim}color: #{$landing_config.header_color};{rdelim}
    .bnow, .product  {ldelim}color: #{$landing_config.btn_color};{rdelim}
    .save-percentage strong {ldelim}color: #{$landing_config.percentage_color};{rdelim}
    .bf-cover, #bf-vid{ldelim}background-size:cover;{rdelim}
    .bf-cover{ldelim}display: flex;flex-direction: column;justify-content: center;align-items: center;text-align: center;cursor:normal;{rdelim}
     {literal}
     .bproducts{padding:10% 0;float:left;width:100%}.bproduct{width:33.33336%;float:left;position:relative;box-sizing:border-box;padding-right:10px;padding-bottom:10px}.bproduct-inner{-webkit-box-shadow:0 4px 9px 0 #ccc;box-shadow:0 4px 9px 0 #ccc;background:#fff;padding-left:150px;position:relative;height:130px}.bproduct-inner .content{padding:10px}.bproduct-inner h4{font-weight:300}.bproduct-inner h1{letter-spacing:-.5px}.bproduct .img{position:absolute;left:5px;top:5%;width:150px;height:85%;text-align:center;background-size:contain;background-position:center center;background-repeat:no-repeat}@media screen and (max-width:768px){.bproduct-wrapper{overflow-y:hidden;overflow-x:scroll;white-space:nowrap;transform:translateZ(0);will-change:transform;-webkit-overflow-scrolling:touch}.bproduct{display:inline-block;float:none;width:80%}.bproduct-inner .content{white-space:normal}.bproduct-inner h4{font-weight:300;height:70px;overflow:hidden;-ms-text-overflow:ellipsis;-o-text-overflow:ellipsis;text-overflow:ellipsis;display:-webkit-box;-webkit-box-orient:vertical;-webkit-line-clamp:3;-moz-box-orient:vertical;-moz-line-clamp:3;margin-bottom:10px}}
    {/literal}
    {if $key == "raiffeisen"}
    {literal}
    @media screen and (max-width: 768px){
        .bf-cover {
            height: 350px;
        }}
    {/literal}
    {/if}{literal}
    @media screen and (min-width: 769px){
        .product.small .price-wrapper{min-height:76px;}
    }
    .product.medium .product-inner {
        min-height: 205px;
    }{/literal}
</style>

<div class="bf-cover clearfix">
    {*{if !$is_mobile}*}
    {*<video oncontextmenu="return false" id="bf-vid" class="span16" style="margin:0;" autoplay loop poster="https://hhstsyoejx.gjirafa.net/gj50/landings/{$key}/cover{if $is_mobile}-mob{/if}.jpg">*}
            {*<source src="https://hhstsyoejx.gjirafa.net/gj50/landings/{$key}/cover.webm" type="video/webm"/>*}
    {*</video>*}
    {*{/if}*}

    <h1 style="margin-top: -50px;letter-spacing: -1.5px;font-weight:bolder;font-size:35px;color:#{$landing_config.buynow_color};">{$landing_config.cover_txt1 nofilter}</h1>
    <h3 style="font-weight:400;">{$landing_config.cover_txt2 nofilter}</h3>
</div>
{if $key == 'raiffeisen'}
    {literal}
        <script>document.querySelector('.bf-cover').onclick = function(){ window.open('https://www.raiffeisen-kosovo.com/cards/apply/apply-bonus-card.php?utm_source=Gjirafa50&utm_medium=RBKO%20Landing&utm_campaign=Zbritje%20me%20bonus%20kartele', '_blank'); }</script>
    {/literal}
{/if}
<div class="bf-bottom">
    {if $landing_config.heading_1 != ""}
        <h3>{$landing_config.heading_1}</h3>
    {/if}
    {if $landing_config.heading_2 != ""}
        <h5 class="text-light text-error">{$landing_config.heading_2}</h5>
    {/if}
    {if $hide_price == true}
        <div id="bf-countdown" style="margin-top:0;" class="text-center"></div>
    {/if}
</div>

<div class="container-fluid" id="products">
    <div class="row">
        {assign "iteration" 0}
        {assign "cat" 0}
        {foreach $csv_products as $product}{if $elastic_products[$product.product_code] == null}{continue}{/if}<div class="product {if $combo[$product.product_code] == null}small{else}medium{/if} bbox"><div class="product-inner clearfix">
                <a class="bf-link" href="/index.php?dispatch=products.view&product_id={$elastic_products[$product.product_code].product_id}">

                        {if $combo[$product.product_code] != null}
                            {assign var=foo value=" + "|explode:$elastic_products[$product.product_code].product}
                            {$elastic_products[$product.product_code].product = $foo[0]}
                        {/if}

                        <h3 class="title" {if $combo[$product.product_code] != null}style="height:45px;"{/if}>{$elastic_products[$product.product_code].product}</h3>
                        {if $elastic_products[$product.product_code].amount > 0}
                            <img class="stockimage" src="https://gjirafa50.com/images/icons/upto48.svg"/>
                        {/if}

                        <div class="image-wrapper">
                            <img thumb="{get_images_from_blob product_id=$elastic_products[$product.product_code].product_id count=1}"/>

                        </div>
                    {if !$hide_price}
                        {assign var="sameprice" value=false}
                        {if $elastic_products[$product.product_code].old_price != null}
                            {$elastic_products[$product.product_code].price = $elastic_products[$product.product_code].old_price}
                        {/if}
                        {if $product.new_price - $elastic_products[$product.product_code].price == 0}
                            {$sameprice = true}
                        {/if}

                        <div class="price-wrapper" {if $product.new_price == '0.00'}style="width:100%;"{/if}>
                            {if !$sameprice && $product.new_price != '0.00'}
                                <h2 class="oprice text-muted text-light text-linethrough">€{$elastic_products[$product.product_code].price}</h2>
                                <h2 class="nprice text-bold">€{$product.new_price}</h2>
                            {elseif $product.new_price != '0.00'}
                                <h2 class="nprice text-bold ty-center">€{$product.new_price}</h2>
                                {else}
                                <h2 class="nprice text-bold ty-center">€{$elastic_products[$product.product_code].price}</h2>
                            {/if}
                        </div>
                        {if !$sameprice && $combo[$product.product_code] == null  && $product.new_price != '0.00'}
                            {math assign="savex" equation='(x-y)' x=$elastic_products[$product.product_code].price y=$product.new_price}
                            <h2 class="save-percentage"><strong>JU KURSENI</strong> <span>€{$savex}</span></h2>
                            {elseif $combo[$product.product_code] != null}
                            <h2 class="save-percentage" style="line-height: 1;padding-top: 2px;"><strong style="font-size: 15px;">FALAS!</strong> <span style="font-size: 15px;">{$combo[$product.product_code]}</span></h2>
                        {/if}
                    {/if}
                </a>
                {if !$hide_price}
                    <a href="/index.php?dispatch=products.view&product_id={$elastic_products[$product.product_code].product_id}" class="ty-btn ty-btn__primary bbox bnow">
                        {if $product.out_of_stock}
                            E SHITUR
                        {else}
                            BLEJ TANI
                        {/if}
                    </a>
                {/if}

                {assign var="stock" value="`$elastic_products[$product.product_code].amount+$elastic_products[$product.product_code].czc_stock+$elastic_products[$product.product_code].czc_retailer_stock`"}
                {if $stock == "0"}
                    <div class="sold-out-bf bbox"><img src="images/black-friday17/so.png"/></div>
                {/if}
            </div>

            </div>{$iteration = $iteration + 1}{if $iteration % 20 == 0 && $cat < 4}<div class="span16 catimg" style="clear: both;"><a href="{$categories_img[$cat]}"><div class="catimg-inner ty-center"><img src="https://hhstsyoejx.gjirafa.net/gj50/{$cat}.jpg"/></div></a></div>{$cat = $cat + 1}{/if}{/foreach}
    </div>
</div>
{**}
<div id="scrollbarmobile">
    <span></span>
</div>
<input type="hidden" id="landing_id" value="{$key}">
<script src="js/lib/lazyload/lazyload.js"></script>

<script>
{if $hide_price}
var end_date = '{$landing_config.hide_prices_until}';
            {literal}var end=new Date(end_date);var _second=1000;var _minute=_second*60;var _hour=_minute*60;var _day=_hour*24;var timer;function showRemaining(){var now=new Date();var distance=end-now;if(distance<0){clearInterval(timer);document.getElementById('bf-countdown').innerHTML='';return}var days=Math.floor(distance/_day);var hours=Math.floor((distance%_day)/_hour);var minutes=Math.floor((distance%_hour)/_minute);var seconds=Math.floor((distance%_minute)/_second);document.getElementById('bf-countdown').innerHTML='<span class="startat">Fillon edhe</span> '+days+'<span> ditë</span> ';document.getElementById('bf-countdown').innerHTML+=hours+'<span> orë</span> ';document.getElementById('bf-countdown').innerHTML+=minutes+'<span> min</span> ';document.getElementById('bf-countdown').innerHTML+=seconds+'<span> sek</span>'}
    timer=setInterval(showRemaining,1000);{/literal}
{/if}
{literal}
    $(window).scroll(function(e) {
        var $this = $(this),
            docHeight = $(document).height() - $(window).height();
        $('#scrollbarmobile span').css('height', ($this.scrollTop() * 100) / docHeight + '%');

    });
    //document.getElementById('bf-vid').addEventListener('contextmenu', event => event.preventDefault());
    {/literal}
</script>