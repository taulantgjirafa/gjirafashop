<style>
    .tygh-content > div {
        padding-bottom: 0;
    }
    @media (min-width: 758px) {
        .tygh-content > div {
            padding-top: 15px;
        }
    }
    @media (max-width: 768px) {
        .tygh-content {
            padding-top: 0 !important
        }
    }
</style>
{if $products}
    {assign var="layouts" value=""|fn_get_products_views:false:0}
    {if $layouts.$selected_layout.template}
        <div class="brands-divider">
            <div class="brands-divider-bg"></div>
            <img src="{$brand_image.image}">
        </div>
        {include file="`$layouts.$selected_layout.template`" columns=$settings.Appearance.columns_in_products_list show_qty=true no_sorting= true  brand_image = $brand_image}
    {/if}
{/if}

<div class="sticky-smooth-div" style="display: none; height: 50px;"></div>

<div class="ty-brands">
    <div class="ty-brands__top brand-section" id="topbrands">
        <p>Top Brendet</p>
        <div class="grid-list" style="margin: 0 -10px;">
            <div class="ty-column3 top-brand-item">
                <div class=" top-brand-item__inner">
                    <a href="/index.php?dispatch=pages.brands&brand=steelseries">
                        <img src="images/brands/steelseries.jpg" alt="">
                    </a>
                </div>
            </div>
            <div class="ty-column3 top-brand-item">
                <div class=" top-brand-item__inner">
                    <a href="/index.php?dispatch=pages.brands&brand=jbl">
                        <img src="images/brands/jbl.jpg" alt="">
                    </a>
                </div>
            </div>

            <div class="ty-column3 top-brand-item">
                <div class=" top-brand-item__inner">
                    <a href="/index.php?dispatch=pages.brands&brand=hp">
                        <img src="images/brands/hp.jpg"
                             alt="">
                    </a>
                </div>
            </div>
            <div class="ty-column3 top-brand-item">
                <div class=" top-brand-item__inner">
                    <a href="/index.php?dispatch=pages.brands&brand=apple">
                        <img src="images/brands/apple.png" alt="">
                    </a>
                </div>
            </div>

            <div class="ty-column3 top-brand-item">
                <div class=" top-brand-item__inner">
                    <a href="/index.php?dispatch=pages.brands&brand=dell">
                        <img src="images/brands/dell.jpg" alt="">
                    </a>
                </div>
            </div>
            <div class="ty-column3 top-brand-item">
                <div class=" top-brand-item__inner">
                    <a href="/index.php?dispatch=pages.brands&brand=razer">
                        <img src="images/brands/razer.png" alt="">
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="alphabet-nav" >
        <div class="alphabet-nav__wrap">
            <a data-tab-panel-link="" class="alphabet-nav__link" data-tab-link-id="topbrands">
                <span class="alphabet-nav__link-wrap">
                   <i class="ty-stars__icon ty-icon-star"></i>
                </span>
            </a>
            <a data-tab-panel-link="" class="alphabet-nav__link " data-tab-link-id="number">
                #
            </a>

            <a data-tab-panel-link="" class="alphabet-nav__link  " data-tab-link-id="A">
                A
            </a>

            <a data-tab-panel-link="" class="alphabet-nav__link " data-tab-link-id="B">
                B
            </a>

            <a data-tab-panel-link="" class="alphabet-nav__link " data-tab-link-id="C">
                C
            </a>

            <a data-tab-panel-link="" class="alphabet-nav__link " data-tab-link-id="D">
                D
            </a>

            <a data-tab-panel-link="" class="alphabet-nav__link " data-tab-link-id="E">
                E
            </a>

            <a data-tab-panel-link="" class="alphabet-nav__link " data-tab-link-id="F">
                F
            </a>

            <a data-tab-panel-link="" class="alphabet-nav__link " data-tab-link-id="G">
                G
            </a>

            <a data-tab-panel-link="" class="alphabet-nav__link " data-tab-link-id="H">
                H
            </a>

            <a data-tab-panel-link="" class="alphabet-nav__link " data-tab-link-id="I">
                I
            </a>

            <a data-tab-panel-link="" class="alphabet-nav__link " data-tab-link-id="J">
                J
            </a>

            <a data-tab-panel-link="" class="alphabet-nav__link " data-tab-link-id="K">
                K
            </a>

            <a data-tab-panel-link="" class="alphabet-nav__link" data-tab-link-id="L">
                L
            </a>

            <a data-tab-panel-link="" class="alphabet-nav__link " data-tab-link-id="M">
                M
            </a>

            <a data-tab-panel-link="" class="alphabet-nav__link" data-tab-link-id="N">
                N
            </a>

            <a data-tab-panel-link="" class="alphabet-nav__link " data-tab-link-id="O">
                O
            </a>

            <a data-tab-panel-link="" class="alphabet-nav__link " data-tab-link-id="P">
                P
            </a>

            <a data-tab-panel-link="" class="alphabet-nav__link" data-tab-link-id="Q">
                Q
            </a>

            <a data-tab-panel-link="" class="alphabet-nav__link" data-tab-link-id="R">
                R
            </a>

            <a data-tab-panel-link="" class="alphabet-nav__link" data-tab-link-id="S">
                S
            </a>

            <a data-tab-panel-link="" class="alphabet-nav__link " data-tab-link-id="T">
                T
            </a>

            <a data-tab-panel-link="" class="alphabet-nav__link " data-tab-link-id="U">
                U
            </a>

            <a data-tab-panel-link="" class="alphabet-nav__link " data-tab-link-id="V">
                V
            </a>

            <a data-tab-panel-link="" class="alphabet-nav__link " data-tab-link-id="W">
                W
            </a>

            <a data-tab-panel-link="" class="alphabet-nav__link " data-tab-link-id="X">
                X
            </a>

            <a data-tab-panel-link="" class="alphabet-nav__link " data-tab-link-id="Y">
                Y
            </a>

            <a data-tab-panel-link="" class="alphabet-nav__link  " data-tab-link-id="Z">
                Z
            </a>

        </div>
    </div>
    <div class="variants-wrapper">

    <div class="ty-brands__row brand-section" style="display: none;">

        {assign var="char" value=0}
        {assign var="last_char" value=0}

        {foreach $brands as $brand}
        {if $brand.letter != $char}
        {if $brand.letter != $char}
        {$last_char = $char}
    </div>
    {/if}
    {$char = $brand.letter}
    <div class="ty-brands__row brand-section clearfix" id="{$char}">
        <div class="ty-brands__letter">
            {$char}
        </div>
        {*<h1 class="ty-mainbox-title__shadow"> {$char}</h1>*}

        {/if}

        <div class="span2">
            <a href="/index.php?dispatch=pages.brands&brand={$brand.variant}">{$brand.variant}</a>
        </div>

        {/foreach}
    </div>
    </div>
</div>



{literal}
    <script>
        $('.alphabet-nav__link').click(function () {
            var letter = $(this).attr('data-tab-link-id'),
                offset = 50;

            if($(window).innerWidth() < 768)
                offset = 100;

            if(letter == undefined)
                return;

            if (letter == "number") {

                var target = $('.brand-section#1').offset().top - offset;
                $('html, body').animate({
                    scrollTop: target
                }, 700);
            }

            else {
                var target = $('.brand-section#' + letter).offset().top - offset;
                $('html, body').animate({
                    scrollTop: target
                }, 700);
            }


        });
        var isSticky = false,
            alphabet = $('.alphabet-nav'),
            alphabetTop = alphabet.offset().top;

        if($(window).innerWidth() < 769)
            alphabetTop = alphabetTop - 55;

        $(window).scroll(function () {
            if ($(window).scrollTop() > alphabetTop) {
                if (isSticky == false) {
                    alphabet.addClass('alphabet-nav__sticky');
                    $('.sticky-smooth-div').show();
                    isSticky = true;
                }
            }
            else {
                if (isSticky == true) {
                    alphabet.removeClass('alphabet-nav__sticky');
                    $('.sticky-smooth-div').hide();
                    isSticky = false;
                }
            }
        })
    </script>
{/literal}