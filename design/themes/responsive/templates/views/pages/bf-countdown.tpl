<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, user-scalable=no">

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flipclock/0.7.8/flipclock.css">
	<style type="text/css">
		body {
			background-color: rgb(250, 250, 252)!important;
			font-family: 'Roboto', sans-serif!important;
			background-color: #101010 !important;
		}
		.background {
			background: url("./images/background.jpg") no-repeat center center;
			height: 90vh;
			background-color: #111 !important;
			background-position: 50% 50% !important;
			position: relative;
			background-size: 135%;
		}
		@media (max-width: 767.98px) {
			.background {
				background: url("./images/background-mobile.jpg") no-repeat center center;
				background-size: contain;
			}
		}

		.countdown {
			position: absolute;
			top: 50%;
			left: 50%;
			transform: translate(-50%, -50%);
			padding-top: 15px;
			padding-bottom: 25px;
			text-align: center;
			margin-top: 50px;
			width: 100%;
		}

		.countdown .row {
			margin-bottom: 50px;
		}

		h1 {
			font-size: 70px;
			font-weight: 700;
			margin: 0;
		}
		.discount {
			font-size: 100px;
			color: #f04d24;
			transform: rotate(-15deg);
			display: inline-block;
			font-weight: 900;
		}
		.pre-text {
			font-size: 25px;
			font-weight: 700;
			text-align: left;
			margin-left: 23px;
			margin-bottom: -10px;
		}
		p {
			text-align: center;
		}
		.flip-clock-wrapper {
			max-width: 508px;
			margin: 3em auto 2em;
			display: flex;
			justify-content: center;
		}

		.flip-clock-wrapper ul {
			width: 46px!important;
		}

		.flip-clock-divider .flip-clock-label {
			font-size: 20px;
			right: -80px!important;
			bottom: -30px!important;
			top: unset!important;
			text-transform: uppercase;
			color: #f04d24;
		}

		.flip-clock-divider.minutes .flip-clock-label {
			right: -98px!important;
		}

		.flip-clock-divider.seconds .flip-clock-label {
			right: -106px!important;
		}

		.flip-clock-wrapper ul li a div div.inn {
			color: #fff;
		}

		.col-md-4 {
			display: flex;
			justify-content: center;
		}
		.col-md-2 {
			display: flex;
			justify-content: center;
			height: 34px;
			align-items: center;
		}
		.counter {
			display: flex;
			justify-content: center;
		}
		.clock {
			margin-top: 30px;
		}
		.container {
			width: 500px;
		}
		.middle {
			display: inline-block;
		}
		#btns {
			display: flex;
			justify-content: center;
		}
		#stop {
			margin-left: 10px;
			margin-right: 10px;
		}
		.btn {
			background-color: #333;
			color: #ccc;
		}
		#sessInc, #sessDec, #breakInc, #breakDec {
			font-weight: bold;
		}
		#stats {
			background-color: #333;
			width: 220px;
			height: 70px;
			border-radius: 10px;
			color: #ccc;
			font-size: 45px;
			text-align: center;
		}
		#statRow {
			display: flex;
			justify-content: center;
			margin-bottom: 20px;
		}

		.mb-50 {
			margin-bottom: 50px;
		}

		h1 {
			position: relative;
			color: #fff;
			font-size: 3em;
			text-transform: uppercase;
			font-weight: 400;
		}

		.paragraph {
			font-size: 21px;
			font-weight: 700;
			color: #222;
		}

		@media (max-width: 575.98px) {
			.background {
				max-height: 450px;
			}

			.flip-clock-wrapper ul {
				margin: 0 !important;
			}

			h1 {
				font-size: 1.5em;

				top: 25px;
			}
			.countdown .row {
				transform: scale(0.7);
			}

			.flip-clock-wrapper{
				top: -30px;
			}

			.flip-clock-divider .flip-clock-label {
				right: -65px !important;
			}

			.flip-clock-divider.minutes .flip-clock-label {
			    right: -80px!important;
			}

			.flip-clock-divider.seconds .flip-clock-label {
			    right: -95px!important;
			}
		}

		@media (max-width: 400px) { 
			.countdown .row {
				transform: scale(0.5);
			}
		}

		@media (max-width: 350px) {
			.flip-clock-divider.minutes .flip-clock-label {
			    right: -70px!important;
			}

			.flip-clock-divider.seconds .flip-clock-label {
			    right: -90px!important;
			}

			.flip-clock-divider .flip-clock-label {
				right: -60px !important;
			}
		}

		@media screen and (max-width: 768px) {
			.flip-clock-divider{
			}
		}        

		@media only screen and (min-width:768px) and (max-width:1024px) {
			.background {
				background-size:185% ;
			}
		}

		.container-fluid {
			padding: 0 !important;
			max-width: none;
			min-width: none;
		}

		.tygh-content.clearfix {
			padding: 0 !important;
		}
	</style>
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700,900&display=swap" rel="stylesheet">
	<title>Gjirafa50</title>
</head>

<body>
	<div class="background">
		<div class="countdown">
			<h1>Zbritje deri në 70%</h1>
			<div id="clock" class="row">
				<div class="timer">
					<div class="middle"></div>
				</div>
			</div>
		</div>
	</div>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/flipclock/0.7.8/flipclock.min.js"></script>
	<script>
		{literal}
		$(document).ready(function(){

			var today = new Date();
			var blackFriday = new Date("11-29-2019");
			var delta = Math.abs(blackFriday - today) / 1000;
			var days = Math.floor(delta / 86400);
			delta -= days * 86400;
			var hours = Math.floor(delta / 3600) % 24;
			delta -= hours * 3600;
			var minutes = Math.floor(delta / 60) % 60;
			delta -= minutes * 60;
			var seconds = parseInt(delta % 60);
			var diffMs = (blackFriday - today);
			var allMins = parseInt(diffMs/(1000*60));

			var excMinutes = allMins+"."+seconds

			var countS =excMinutes;
			var clock = $(".timer").FlipClock(0, {
				countdown: true,
				clockFace: 'DailyCounter',
				autoStart: true,
				callbacks: {
					interval: function(){
						if (clock.getTime() == 0){
							$("#clock").html('<br/><h1 style="color: #e65228">Black Friday ka filluar...</h1><br /><h3 style="color: white">Shpejto për të blerë produktin me ekstra zbritje</h3>');
						}
					}
				}
			})

			$('.seconds').children()[0].innerText = "Sekonda";
			$('.minutes').children()[0].innerText = "Minuta";
			$('.hours').children()[0].innerText = "Orë";
			$('.days').children()[0].innerText = "Ditë";

			clock.setTime(countS*60);
			clock.start();
		});
		{/literal}
	</script>
</body>

</html>