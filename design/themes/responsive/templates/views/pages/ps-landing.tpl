<style>
    .tygh-content {
        background: #f5f6fa;
    }

    @font-face {
        font-family: "Samsung Sharp Sans Bold";
        src: url("//db.onlinewebfonts.com/t/03fe5644d1605049951f58ca7961c33f.eot");
        src: url("//db.onlinewebfonts.com/t/03fe5644d1605049951f58ca7961c33f.eot?#iefix") format("embedded-opentype"), url("//db.onlinewebfonts.com/t/03fe5644d1605049951f58ca7961c33f.woff2") format("woff2"), url("//db.onlinewebfonts.com/t/03fe5644d1605049951f58ca7961c33f.woff") format("woff"), url("//db.onlinewebfonts.com/t/03fe5644d1605049951f58ca7961c33f.ttf") format("truetype"), url("//db.onlinewebfonts.com/t/03fe5644d1605049951f58ca7961c33f.svg#Samsung Sharp Sans Bold") format("svg");
    }
    
    .text-muted {
        color: #777 !important;
    }

    /*.compare-phone button {*/
    /*    border: 2px solid #000;*/
    /*}*/

    .samsung-landing .compare-phone {
        padding-top: 0;
        margin-top: -200px;
    }

    .samsung-landing .samsung-banner {
        height: 500px;
    }

    .ps-modal-list {
        margin-top: 40px;
    }
    .ps-modal-list li:first-child {
        padding-top: 20px;
    }
    .ps-modal-list li{
        border-top:1px solid #eee;
        padding-top: 10px;
        margin-top: 10px;
    }
    .product-overview .po-item:before {
        background: linear-gradient(to right,transparent,#000,transparent);
    }
    .samsung-landing .compare-phone button{
       border:0 !important;
        outline: none;
    }
    .samsung-landing .compare-phone button:hover{
        background: #000;
        color:#fff;
    }
    .product-overview h1 {
        color: #000;
    }
    .product-overview .po-content {
        padding: 15px;
    }
    .po-img img {
        border-radius: 5px;
    }
    .game-list-first {
        margin-top: 0 !important;
    }
    .game-list-first::before {
        background: none !important;
    }
    @media screen and (max-width: 768px) {
        .samsung-landing .samsung-banner div {
            height: 44%;
        }
        .product-overview .po-item:last-child {
            flex-direction: column;
            padding-bottom: 30px;
        }
        .product-overview .po-content {
            padding: 0 !important;
        }
    }
    @media screen and (min-width: 768px) {
        .samsung-banner {
            margin-left: -352px !important;
            height: 930px !important;
            position: inherit !important;
        }
        .samsung-landing .samsung-banner div {
            height: 35% !important;
        }
    }
</style>
<span class="samsungoverlay"></span>
<div class="samsung-landing">
    <div class="samsung-modal">
        <span class="close-sam-modal"><svg><use xlink:href="#svg-back"></use></svg></span>
        <h1 style="text-align: center;">Informohu dhe rezervo i pari</h1>
        <div class="flexing flex-h-between">
            <img src="https://hhstsyoejx.gjirafa.net/gj50/img/500000/img/1.jpg" alt="">
        </div>
        <div>
            <ul class="ps-modal-list">
                <li>
                    <h2>Loja që nuk ka kufij</h2>
                    <p>Përjeto shpejtësi të rrufeshme të ngarkimit (loading) me SSD-në ultra të shpejtë, zhytje edhe më të thellë në lojë, kontroller me buton adaptiv, 3D Audio, dhe një gjeneratë tërësisht të re të lojërave të jashtëzakonshme PlayStation ®.</p>
                </li>
                <li>
                    <h2>Shpejtësi e rrufeshme</h2>
                    <p>Shfrytëzo fuqinë e procesorit special, kartelës grafike, dhe SSD-së, që së bashku po rishkruajnë rregullat e konzolës PlayStation.</p>
                </li>
                <li>
                    <h2>Lojëra fantastike</h2>
                    <p>Magjepsu me grafikën e jashtëzakonshme dhe përjeto karakteristikat e reja të PS5.</p>
                </li>
                <li>
                    <h2>Zhytja më e thellë në lojë</h2>
                    <p>Zbulo përvojën më të thellë të gamingut dhe ndjej çdo lëvizje në lojë me kontrollerin me “haptic feedback”, buton adaptues, mikrofon të integruar, dhe 3D Audio teknologji.</p>
                </li>
            </ul>

        </div>
    </div>
    <div class="samsung-banner">
        <div class="hidden-phone" style="background-image:url('https://hhstsyoejx.gjirafa.net/gj50/img/500000/img/0.jpg');"></div>
        <div class="hidden-desktop" style="background-image:url('https://hhstsyoejx.gjirafa.net/gj50/img/500000/img/0.jpg');"></div>
    </div>
    <div class="samsung-top-p">

    </div>
    <div class="flexing compare-phone flex-h-center">
        <button class="">Informohu dhe rezervo i pari</button>
    </div>
    <div class="product-overview">
        <div class="po-item flexing text-center" style="justify-content: center">
                <h1>Lojët që i dëshironi</h1>
        </div>
        <div class="po-item flexing game-list-first">
            <div class="po-content">
                <h1>Marvel's Spider-Man: Miles Morales</h1>
                <h2 class="text-muted">Përjeto ngritjen e Miles Morales si hero i ri me fuqi të jashtëzakonshme shpërthyese.</h2>
            </div>
            <div class="po-img">
                <img src="https://hhstsyoejx.gjirafa.net/gj50/img/500000/img/5.jpg" />
            </div>
        </div>
        <div class="po-item flexing">
            <div class="po-content">
                <h1>Horizon Forbidden West</h1>
                <h2 class="text-muted">Bashkoju Aloy-s në luftën e saj të guximshme në Forbidden West, një kufi madhështor por i rrezikshëm, që fsheh kërcënime të reja misterioze.</h2>
            </div>
            <div class="po-img">
                <img src="https://hhstsyoejx.gjirafa.net/gj50/img/500000/img/3.jpg" />
            </div>
        </div>
        <div class="po-item flexing">
            <div class="po-content">
                <h1>Ratchet & Clank: Rift Apart</h1>
                <h2 class="text-muted">Shpërthe rrugën përmes një aventure ndërdimensionale me Ratchet dhe Clank.</h2>
            </div>

            <div class="po-img">
                <img src="https://hhstsyoejx.gjirafa.net/gj50/img/500000/img/4.jpg" />
            </div>
        </div>
        <div class="po-item flexing">
            <div class="po-content">
                <h1>Gran Turismo 7</h1>
                <h2 class="text-muted">Gran Turismo 7, është e ndërtuar mbi përvojën e saj 7 vjeçare për të ju sjell karakteristikat më të mira në histori të franshizës.</h2>
            </div>
            <div class="po-img">
                <img src="https://hhstsyoejx.gjirafa.net/gj50/img/500000/img/2.jpg" />
            </div>
        </div>
    </div>
</div>

<script>
    // var banner = document.querySelector('.samsung-banner'),
    //     leftSpace = banner.offsetLeft;
    //
    // banner.style.marginLeft = '-' + leftSpace + 'px';
    // banner.style.width = 'calc(100% + ' + leftSpace * 2 + 'px)';



    animationEL = document.querySelectorAll('.entry-animation');
    for (var i = 0; i < animationEL.length; i++) {
        animationEL[i].classList.add('active');
    }

    var phoneColors = document.querySelectorAll('.chose-colors .phone-color');

    for (var i = 0; i < phoneColors.length; i++) {
        phoneColors[i].onclick = function() {
            var thisPhoneColor = this.getAttribute('data-color'),
                thisClosestItem = this.closest('.ss-prod'),
                changableItems = thisClosestItem.querySelectorAll('.changable-s'),
                clickColorParent = this.parentElement.children;

            for (var k = 0; k < clickColorParent.length; k++) {
                clickColorParent[k].classList.remove('active');
                this.classList.add('active');
            }


            for (var j = 0; j < changableItems.length; j++) {
                changableItems[j].classList.remove('active');
            }
            document.querySelectorAll('.' + thisPhoneColor)[0].classList.add('active');
            document.querySelectorAll('.' + thisPhoneColor)[1].classList.add('active');
            document.querySelectorAll('.' + thisPhoneColor)[2].classList.add('active');

        }
    }

    var compareBtn = document.querySelector('.compare-phone button'),
        sModal = document.querySelector('.samsung-modal'),
        sOverlay = document.querySelector('.samsungoverlay'),
        closeSmodal = document.querySelector('.close-sam-modal');



    closeSmodal.onclick = function() {
        sOverlay.classList.remove('active');
        sModal.classList.remove('active');
        setTimeout(function() {
            sOverlay.style.display = 'none';
            sModal.style.display = 'none';
        }, 500);
    }

    compareBtn.onclick = function() {
        sOverlay.style.display = 'block';
        sModal.style.display = 'block';
        window.scrollTo(0, 150);
        setTimeout(function() {
            sOverlay.classList.add('active');
            sModal.classList.add('active');
        });
    }
</script>