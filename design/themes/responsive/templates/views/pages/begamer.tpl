
{if $isAjax}
    {assign "iteration" 0}
    {assign "cat" 0}
    {foreach $products as $product}<div class="product {if $iteration > -1}small{else}medium{/if} bbox"><div class="product-inner clearfix">
            <a class="bf-link" href="/index.php?dispatch=products.view&product_id={$product.product_id}">
                <h3 class="title">{$product.product}</h3>
                {if $product.amount > 0}
                    <img class="stockimage" src="images/begamer/48ore"/>
                {/if}

                <div class="image-wrapper">
                    <img src="{get_images_from_blob product_id=$product.product_id count=1}"/>

                </div>
                <h1 class="begamer-price">{$product.price}€</h1>
            </a>
            {if !$hide_price}
                <a href="/index.php?dispatch=products.view&product_id={$product.product_id}" class="ty-btn ty-btn__primary bbox bnow">
                    {if $product.out_of_stock}
                        E SHITUR
                    {else}
                        BLEJ TANI
                    {/if}
                </a>
            {/if}

            {assign var="stock" value="`$product.amount+$product.czc_stock+$product.czc_retailer_stock`"}
            {if $stock == "0"}
                <div class="sold-out-bf bbox"><img src="images/black-friday17/so.png"/></div>
            {/if}
        </div>

        </div>{/foreach}
{else}


<link rel="stylesheet" href="images/begamer/style.css?v=32"/>
    <div  id="wloader">
        <img src="https://hhstsyoejx.gjirafa.net/gj50/landings/{$key}/loader.gif"/>
    </div>
<style>
    .tygh-content {ldelim}background-color: #111;{rdelim}
    .bf-cover {ldelim}background-image: url(https://hhstsyoejx.gjirafa.net/gj50/landings/{$key}/cover.jpg);font-family: 'Pirulen Rg';{rdelim}
    .product.small .save-percentage, .product.medium .save-percentage {ldelim}background:#{$landing_config.percentage_bg_color};{rdelim}
    .product.small .save-percentage span, .product.medium .save-percentage span {ldelim}color:#{$landing_config.percentage_color};{rdelim}
    .product.small .bnow, .product.medium .bnow {ldelim}background:#{$landing_config.buynow_color};font-family: 'Pirulen Rg';{rdelim}
    .bf-cover, .bf-bottom {ldelim}background-color: #{$landing_config.bg_color};{rdelim}
    #bf-countdown span {ldelim}color: #{$landing_config.header_color};{rdelim}
    .bf-cover, #bf-vid{ldelim}background-size:cover;{rdelim}
    .bf-cover{ldelim}display: flex;flex-direction: column;justify-content: center;align-items: center;text-align: center;cursor:normal;{rdelim}
</style>
<div class="bf-cover clearfix">
    {*{if !$is_mobile}*}
    {*<video oncontextmenu="return false" id="bf-vid" class="span16" style="margin:0;" autoplay loop poster="https://hhstsyoejx.gjirafa.net/gj50/landings/{$key}/cover{if $is_mobile}-mob{/if}.jpg">*}
            {*<source src="https://hhstsyoejx.gjirafa.net/gj50/landings/{$key}/cover.webm" type="video/webm"/>*}
    {*</video>*}
    {*{/if}*}
    {*<!-- FIFI lol -->*}
    <img src="https://hhstsyoejx.gjirafa.net/gj50/landings/{$key}/logo.png" style="max-width: 80%;" alt="BONU GAMER">
    <h1 id="mhead">{$landing_config.cover_txt1 nofilter}</h1>
    <h3 style="font-weight:400;color:#fff;font-size: 12px;">{$landing_config.cover_txt2 nofilter}</h3>
    <div id="seperator"></div>
    <h3 id="combo-desc">FITUESI DO TË ZGJEDH NJËRËN NGA KËTO COMBO:</h3>
    <div class="peppermint peppermint-inactive" id="peppermint">
        {for $i = 1; $i < 7; $i++}
            <figure>
                <img src="images/begamer/carousel/{$i}.jpg" alt="">
            </figure>
        {/for}
    </div>
<div class="title container-fluid" style="width: 100%">
    <span style="color: whitesmoke;float:left; margin-bottom: 5px;font-size: 13px;">Grupim sipas vlerës (€)</span>
</div>
  <div class="container-fluid" style="width: 100%">
          <div class="left-filters">
            <div class="price-range">
                <a href="{"pages.begamer&range_min=20&range_max=50"|fn_url}"  {if $smarty.get.range_min == 20}class="active"{/if}>20-50 €</a>
                <a href="{"pages.begamer&range_min=50&range_max=100"|fn_url}" {if $smarty.get.range_min == 50}class="active"{/if}>50-100 €</a>
                <a href="{"pages.begamer&range_min=100&range_max=200"|fn_url}" {if $smarty.get.range_min == 100}class="active"{/if}>100-200 €</a>
                <a href="{"pages.begamer&range_min=200&range_max=300"|fn_url}" {if $smarty.get.range_min == 200}class="active"{/if}>200-300 €</a>
                <a href="{"pages.begamer&range_min=300&range_max=500"|fn_url}" {if $smarty.get.range_min == 300}class="active"{/if}>300-500 €</a>
                <a href="{"pages.begamer&range_min=500&range_max=1000"|fn_url}" {if $smarty.get.range_min == 500}class="active"{/if}>500-1000 €</a>
                <a href="{"pages.begamer&range_min=1000"|fn_url}" {if $smarty.get.range_min == 1000}class="active"{/if}>1000+ €</a>
            </div>
          </div>
          <div class="right-filters">
            <div class="price-sort">
                <span style="color:whitesmoke">Rendit Sipas</span>
                {if !empty($smarty.get.range_min)}
                    {assign var="url" value="pages.begamer&range_min={$smarty.get.range_min}&range_max={$smarty.get.range_max}&sort="|fn_url}
                {else}
                    {assign var="url" value="pages.begamer&sort="|fn_url}
                {/if}
                <div class="custom-dropdown">
                <select id="gamer_sort" onchange="location = '{$url}'+this.value;">
                    <option value="">zgjedh</option>
                    <option {if $selected_sort == 'asc'}selected{/if} value="asc">çmimit të ulët në të lartë</option>
                    <option {if $selected_sort == 'desc'}selected{/if} value="desc">çmimit të lartë në të ulët</option>
                </select>
                </div>
            </div>
          </div>
  </div>

</div>

{if $landing_config.heading_1 != "" || $landing_config.heading_2 != ""}
<div class="bf-bottom">
    <h3>{$landing_config.heading_1}</h3>
    <h5 class="text-light text-error">{$landing_config.heading_2}</h5>
    <div id="bf-countdown" class="text-center"></div>
</div>
{/if}
<div class="container-fluid" id="products">

    <div class="row">
        {assign "iteration" 0}
        {assign "cat" 0}
        {foreach $products as $product}<div class="product {if $iteration > -1}small{else}medium{/if} bbox"><div class="product-inner clearfix">
                <a class="bf-link" href="/index.php?dispatch=products.view&product_id={$product.product_id}">
                        <h3 class="title">{$product.product|truncate:40:"...":true}</h3>
                        {if $product.amount > 0}
                            <img class="stockimage" src="images/begamer/48ore.svg"/>
                        {/if}

                        <div class="image-wrapper">
                            <img thumb="{get_images_from_blob product_id=$product.product_id count=1}"/>

                        </div>
                        <h1 class="begamer-price">{$product.price}€</h1>
                </a>
                {if !$hide_price}
                    <a href="/index.php?dispatch=products.view&product_id={$product.product_id}" class="ty-btn ty-btn__primary bbox bnow">
                        {if $product.out_of_stock}
                            E SHITUR
                        {else}
                            BLEJ TANI
                        {/if}
                    </a>
                {/if}

                {assign var="stock" value="`$product.amount+$product.czc_stock+$product.czc_retailer_stock`"}
                {if $stock == "0"}
                    <div class="sold-out-bf bbox"><img src="images/black-friday17/so.png"/></div>
                {/if}
            </div>

            </div>{$iteration = $iteration + 1}{if $iteration % 20 == 0 && $cat < 7}<div class="span16 catimg"><a href="https://gjirafa50.com/?q={$categories_img[$cat]}&dispatch=products.search"><div class="catimg-inner ty-center"><img src="images/begamer/s/{$categories_img[$cat]}.jpg"/></div></a></div>{$cat = $cat + 1}{/if}{/foreach}
    </div>
    <div id="is_loader" style="display: none;"></div>
    <a href="#" id="load_gp">SHFAQ MË SHUMË PRODUKTE</a>
</div>
<div id="scrollbarmobile">
    <span></span>
</div>
<input type="hidden" id="landing_id" value="{$key}">
<script src="js/lib/lazyload/lazyload.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/peppermint/1.4.0/peppermint.min.js"></script>
<script>

    {literal}
    $(document).ready(function () {
        $('#wloader').fadeOut();
    })
    $('.peppermint').Peppermint({dots: 1});
    var offset = 100;
    var clicked = false;
    var infinite = false;
    $('#load_gp').click(function(){
        if(clicked)
            return false;

        if(offset == 100){
            infinite = true;
            $('#load_gp').hide();
            $('#tygh_footer').css('height', 0);
        }

        $('#is_loader').show();
        clicked = true;
        var url_string = window.location.href; 
        var url = new URL(url_string);
        var range_min = url.searchParams.get("range_min");
        var range_max = url.searchParams.get("range_max");
        var sort = url.searchParams.get("sort");
        url = '/index.php?dispatch=pages.begamer';
        $.ajax({
            url: url,
            data: {isAjax: 1, offset: offset, limit: 40 , range_min:range_min,range_max:range_max,sort:sort},
            success: function (res) {
                $('#products > .row').append(res);
                offset = offset + 40;
                $('#is_loader').hide();
                clicked = false;

            }
        });
        return false;
    });

    $(window).scroll(function(e) {
        var $this = $(this),
            docHeight = $(document).height() - $(window).height();
        $('#scrollbarmobile span').css('height', ($this.scrollTop() * 100) / docHeight + '%');

        if (infinite && $(window).scrollTop() >= $(document).height() - $(window).height() - 450) {
            document.getElementById('load_gp').click();
        }
    });

    {/literal}


</script>

{/if}