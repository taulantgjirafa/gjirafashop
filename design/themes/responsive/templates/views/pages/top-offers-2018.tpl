<title>Black friday Gjirafa50 - Deri 70% ZBRITJE</title>
<meta itemprop="name" content="Black friday Gjirafa50 - Deri 70% ZBRITJE">
<meta itemprop="description" content="Gjirafa50.com, dyqani më i madh për teknologji në rajon për dy vitet e kaluara ka ofruar zbritje marramendëse në shumë produkte teknologjike, çka ka bërë që interesimi për blerje në Gjirafa50 të ishte i jashtëzakonshëm.">
<meta itemprop="image" content="https://hhstsyoejx.gjirafa.net/gj50/landings/black-friday/og_image.jpg">
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:title" content="Black friday Gjirafa50 - Deri 70% ZBRITJE">
<meta name="twitter:description" content=" Gjirafa50.com, dyqani më i madh për teknologji në rajon për dy vitet e kaluara ka ofruar zbritje marramendëse në shumë produkte teknologjike për Black-Friday. Edhe sivjet tradita vazhdon, deri 70% zbritje!">
<meta name="twitter:image:src" content="https://hhstsyoejx.gjirafa.net/gj50/landings/black-friday/og_image.jpg">
<meta property="og:title" content="Black friday Gjirafa50 - Deri 70% ZBRITJE"/>
<meta property="og:url" content="https://gjirafa50.com/black-friday-2018"/>
<meta property="og:image" content="https://hhstsyoejx.gjirafa.net/gj50/landings/black-friday/og_image.jpg"/>
<meta property="og:image:width" content="200" />
<meta property="og:image:height" content="200" />
<meta property="og:description" content=" Gjirafa50.com, dyqani më i madh për teknologji në rajon për dy vitet e kaluara ka ofruar zbritje marramendëse në shumë produkte teknologjike për Black-Friday. Edhe sivjet tradita vazhdon, deri 70% zbritje!"/>
{include file="views/pages/top-offers-2018/style.tpl"}
<link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
<div class="heading">
    <img src="https://hhstsyoejx.gjirafa.net/gj50/landings/{$key}/cover{if $smarty.const.IS_MOBILE}-mob{/if}.jpg"
         alt="">
    <h1 class="ty-center" id="bf-heading">TOP OFERTAT E VITIT 2018</h1>
    <div id="bf-countdown" class="text-center"></div>
</div>
<div class="content product-masonry">
    <div class="container">
        {assign "iteration" 0}
        {foreach $categorized_products as $cproducts}

            <div class="product-masonry grid-list span16" id="{$offers[$iteration]}">
                <div class="span16 ty-center">
                    <img style="position: relative;" src="https://hhstsyoejx.gjirafa.net/gj50/landings/top-offers-2018/{$offers[$iteration]}.jpg" alt="">
                </div>
                {foreach $cproducts as $product}
                    {if $iteration == 2}
                        <img src="https://hhstsyoejx.gjirafa.net/gj50/landings/top-offers-2018/2.jpg" alt="">
                        {break}
                    {/if}
                    {assign var="product" value=$elastic_products[$product.product_code]}
                    {if !$product}{continue}{/if}

                    {if $combo[$product.product_code] != null && $offers[$iteration] == 'oneplusone'}
                        {assign var=foo value=" + "|explode:$product.product}
                        {$product.product = $foo[0]}
                    {/if}

                    <div class="ty-column5 ty-grid-list__item--container {if $combo[$product.product_code] == null}small{else}medium{/if} bbox">
                        <div class="ty-grid-list__image">
                            <a href="https://gjirafa50.com/index.php?dispatch=products.view&product_id={$product.product_id}">
                                <img class="lazyload"
                                     src="{get_images_from_blob product_id=$product.product_id count=1}"
                                     alt="{$product.product}">
                            </a>
                            {if $product.old_price == null}
                                {$product.old_price = $product.price}
                            {/if}
                            {if !$hide_price || $iteration == 1  && $offers[$iteration] != 'oneplusone'}
                                {math assign="savex" equation='(x-y)' x=$product.old_price y=$csv_products[$product.product_code].new_price}
                                {if $savex > 0 && $combo[$product.product_code] == null}
                                    <div class="discount-label">
                                        {if $savex <= 10}
                                            {math assign="sale_percentage" equation='((x-y)*100)/x' x=$product.old_price y=$csv_products[$product.product_code].new_price}
                                            <span>Zbritje</span>
                                            {$sale_percentage|intval}%
                                        {else}
                                            <span>Kurseni</span>
                                            {$savex}€
                                        {/if}
                                    </div>
                                {/if}
                            {/if}
                        </div>
                        <a href="https://gjirafa50.com/index.php?dispatch=products.view&product_id={$product.product_id}">
                            <div class="ty-grid-list__item-details">
                                <h3 class="title">{$product.product}</h3>
                                {if $combo[$product.product_code] != null && $offers[$iteration] == 'oneplusone'}
                                    <h2 class="free-p"><strong style="font-size: 15px;display: block;">FALAS!</strong> <span style="font-size: 15px;display: block;">{$combo[$product.product_code]}</span></h2>
                                {/if}

                                {if !$hide_price || $iteration == 1}
                                    <h2 class="price">
                                        {if $offers[$iteration] != 'oneplusone'}
                                        <span class="o-price">{$product.old_price}€</span>{/if} <br/>
                                        {$csv_products[$product.product_code].new_price}€
                                    </h2>
                                {/if}
                            </div>
                        </a>


                        {if $product.total_stock == 0 && !$hide_price}
                            <div class="sold-out"><h4>E SHITUR</h4></div>
                        {/if}
                        {if $product.amount > 0}
                            <img class="stockimage" src="https://gjirafa50.com/images/icons/upTo48.png"/>
                        {/if}
                    </div>
                {/foreach}
                {if $iteration == 2}
                    <div class="span16">
                        <a href="https://gjirafa50.com/gaming/" style="position:relative;background: transparent;border: 2px solid #fff;font-weight: 900;font-size: 20px;padding: 15px;" class="ty-block bbox ty-btn__primary ty-btn">SHIKO PRODUKTET GAMING</a>
                    </div>
                {/if}
            </div>
            {$iteration = $iteration + 1}
        {/foreach}

    </div>
</div>
<script>
    {if $smarty.get.rbko}
    $(window).load(function () {
        $('#rbko-bnr').click();
    });
    {/if}
    {if $hide_price}
    var end_date = '{$landing_config.hide_prices_until}';
    {literal}
    $('#rbko-bnr').click(function(){
        $('html, body').animate({
            scrollTop: $('#rbko').offset().top
        }, 800);
        return false;
    });
    var end = new Date(end_date);
    console.log(end);
    var _second = 1000;
    var _minute = _second * 60;
    var _hour = _minute * 60;
    var _day = _hour * 24;
    var timer;
    function showRemaining() {
        var now = new Date();
        var distance = end - now;
        if (distance < 0) {
            clearInterval(timer);
            document.getElementById('bf-countdown').innerHTML = '';
            return
        }
        var days = Math.floor(distance / _day);
        var hours = Math.floor((distance % _day) / _hour);
        var minutes = Math.floor((distance % _hour) / _minute);
        var seconds = Math.floor((distance % _minute) / _second);
        document.getElementById('bf-countdown').innerHTML = 'Fillon edhe <br/><span>' + days + '</span> ditë <span>' + hours + '</span> ore <span>' + minutes + '</span> min <span>' + seconds + '</span> sek';
    }
    timer = setInterval(showRemaining, 1000);{/literal}
    {/if}
</script>
