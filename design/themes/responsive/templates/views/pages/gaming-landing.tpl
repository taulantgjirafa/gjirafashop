{assign var="kategoria" value=$smarty.get.kategoria}
{assign var="nenkategoria" value=$smarty.get.nenkategoria}

{* <span class="main-bg hidden-phone"></span> *}
<main class="gg-wp">
	<div class="gg-sidebar hidden-phone">
		<span class="hr-line"></span>
		<span class="vr-line"></span>
		<div class="ggs-brands ggside-sections sidebarSectionsDesktop">
			<div {if !$kategoria && !$nenkategoria}class="active"{/if}>
				<a data-category="all" class="filter" href="{"pages.gaming-landing"|fn_url}">
					<img src="images/appicon/50-112px.png" alt="">
				</a>
			</div>
			{foreach from=$categories item=category key=key}
			<div data-name="{$key}-group" {if $smarty.get.kategoria == $key}class="active"{/if}>
				{if $nenkategoria}
				<a data-category="{$key}" class="filter" href="{"pages.gaming-landing&kategoria=`$key`&nenkategoria=`$nenkategoria`"|fn_url}">
					<img src="images/brands/{$category|lower}logo.png" alt="">
				</a>
				{else}
				<a data-category="{$key}" class="filter" href="{"pages.gaming-landing&kategoria=`$key`"|fn_url}">
					<img src="images/brands/{$category|lower}logo.png" alt="">
				</a>
				{/if}
			</div>
			{/foreach}
		</div>
	</div>

	<div class="container">
		<div class="clip-mobile">
			<div class="innerClip">
				{if !$cat_filter && !$sub_filter}
					{include file="views/pages/gaming/slider.tpl" slider=$slider}
				{/if}
				<div id="gaming_filtered_products_container">
					{include file="views/pages/gaming/sections.tpl" sub_categories=$sub_categories priority_products=$priority_products products=$products}
				</div>
			</div>
		</div>
	</div>
</main>
<div class="load_more_category" style="text-align: center;">
	<a href="#" class="ty-btn__primary ty-btn" id="load_more_gaming" style="box-shadow: 0 4px 10px -4px #000;">SHFAQ MË SHUMË PRODUKTE</a>
	<div id="is_loader" class="is_loader active" style="display: none; margin: 0px auto;"></div>
	<div id="no_results" style="display:none; text-align:center; padding:30px 0px; font-size:14px;">
		<span class="text-muted">Fundi i rezultateve</span>
	</div>
</div>
<!-- <span style="position:fixed; left: 15px; top:0; width:1px; height:100%; background:cyan; z-index:1123123123;"></span>
<span style="position:fixed; left: 30px; top:0; width:1px; height:100%; background:cyan; z-index:1123123123;"></span>
<span style="position:fixed; right: 0; top:0; width:1px; height:100%; background:cyan; z-index:1123123123; margin-right:15px;"></span>
<span style="position:fixed; right: 0; top:0; width:1px; height:100%; background:cyan; z-index:1123123123; margin-right:30px;"></span> -->
<style>
	body,
	p,
	div,
	li {
		font-family: inherit;
	}

	.ty-footer-grid__full-width.footer-copyright,
	.header-grid .row-fluid:nth-child(2) {
		background: #111;
	}

	.footer-copyright {
		border-color: #e65228 !important;
		background: #8b2f14 !important;
	}

	.footer-copyright .row-fluid:first-child {
		position: relative;
		z-index: 2;
	}

	.ty-footer-grid__full-width.footer-copyright a,
	.ty-menu__item .ty-menu__item-link,
	.footer-links .heading {
		color: #fff;
		border: none;
	}

	.tygh-content,
	.mobile-header {
		background: #000 !important;
	}

	.small-footer {
		background: transparent !important;
	}

	.top-banner-desktop,
	.top-banner-mobile {
		display: none;
	}

	.header-grid>.row-fluid:first-child {
		width: 100%;
		max-width: 100%;
	}

	.header-grid>.row-fluid:nth-child(2) {
		display: none;
	}

	.tygh-top-panel {
		border: 0;
	}

	.tygh-header {
		position: fixed;
		top: 0;
		width: 100%;
		background: #000;
		z-index: 1111;
	}

	.scroll-top-button {
		box-shadow: -16px 0px 40px -10px #e65228;
		background: transparent;
		border: 1px solid #e65228;
	}

	.scroll-top-button svg {
		fill: #e65228;
	}

	.header-grid .ty-dropdown-box__title:hover,
	.header-grid .ty-dropdown-box__title:hover .ty-account-info__title,
	.header-grid .ty-dropdown-box__title:hover .ty-account-info__user-arrow {
		background: transparent !important;
	}

	@media screen and (max-width:1400px) {
		.container {
			width: calc(95% - 95px);
			padding-left: 95px;
		}
	}

	@media screen and (max-width:768px) {
		.container {
			width: 100%;
			padding: 0 !important;
			box-sizing: border-box;
		}
	}
</style>

<script>	
	var limit = 32;

	if ({$product_count} < limit) {
		document.querySelector('#load_more_gaming').style.display = 'none';
	}

{literal}

	var offset = 32;
	var clicked = false;
	var	current_url_string = window.location.href;
	var current_url = new URL(current_url_string);
	var category_filter = current_url.searchParams.get('kategoria');
	var sub_category_filter = current_url.searchParams.get('nenkategoria');

	$('body').on('click', '#load_more_gaming', function() {
		if (clicked) {
			return false;
		}

		$('.is_loader').show();

		clicked = true;
		url = fn_url('pages.gaming-landing');

		$.ajax({
			url: url,
			data: {is_ajax: 1, limit: limit, offset: offset, kategoria: category_filter, nenkategoria: sub_category_filter, load_more: true},
			success: function (res) {
				offset = offset + limit;
				clicked = false;
				$('#tygh_footer').css('height', 0);
				$('#gaming_load_more_container').append(res.text);
				$('.is_loader').hide();

				if (res.product_count < limit) {
					document.querySelector('#load_more_gaming').style.display = 'none';
					document.querySelector('#no_results').style.display = 'block';
				} else {
					document.querySelector('#load_more_gaming').style.display = 'inline-block';
					document.querySelector('#no_results').style.display = 'none';
				}
			}
		});

		return false;
	});

	$('body').on('click', '.filter', function() {
		$('#ajax_overlay').show();
		$('#ajax_loading_box').show();

		let category = $(this).data('category');
		let sub_category = $(this).data('subcategory');

		url = fn_url('pages.gaming-landing');

		if (category) {
			if (sub_category_filter) {
				window.history.pushState('', '', url + '&kategoria=' + category + '&nenkategoria=' + sub_category_filter);
			} else {
				window.history.pushState('', '', url + '&kategoria=' + category);
			}

			category_filter = category;
		}

		if (sub_category) {
			if (category_filter) {
				window.history.pushState('', '', url + '&kategoria=' + category_filter + '&nenkategoria=' + sub_category);
			} else {
				window.history.pushState('', '', url + '&nenkategoria=' + sub_category);
			}

			sub_category_filter = sub_category;
		}

		$.ajax({
			url: url,
			data: {is_ajax: 1, kategoria: category_filter, nenkategoria: sub_category_filter, filtering: true},
			success: function (res) {
				$('#gaming_filtered_products_container').html(res.text);

				if (res.product_count < limit) {
					document.querySelector('#load_more_gaming').style.display = 'none';
					document.querySelector('#no_results').style.display = 'block';
				} else {
					document.querySelector('#load_more_gaming').style.display = 'inline-block';
					document.querySelector('#no_results').style.display = 'none';
				}

				$('#ajax_overlay').hide();
				$('#ajax_loading_box').hide();
			}
		});

		return false;
	});

{/literal}
</script>

<script>
	document.title = "Gaming - Gjirafa50";

	var consoleTypeName = document.querySelectorAll('.ggside-sections > div'),
		consoleWrapper = document.querySelector('.ggs-type'),
		consoleType = document.querySelectorAll('.console-type'),
		slider = document.querySelector('.slider-animation'),
		sliderWidth = slider.offsetWidth,
		ggLeft = document.querySelector('.gg-left'),
		ggRight = document.querySelector('.gg-right'),
		index = 0,
		countSliderItems = document.querySelectorAll('.gg-brand-item').length,
		deviceTypeWrapper = document.querySelector('.device-type'),
		checkProgressBarDesktop = document.querySelector('.progressBarSlider'),
		mobsidebar = document.querySelector('.mob-sidebar'),
		sideSections = document.querySelectorAll('.sidebarSectionsDesktop > div');

	function windowIsUnder(checkNumber) {
		var windowWidth = window.innerWidth;
		if (windowWidth < checkNumber) {
			return checkNumber;
		}
	}

	// if (windowSize('xlScreen')) {
	// 	console.log(2)
	// }

	function windowSize(sizeParam) {
		var windowWidth = window.innerWidth;

		var sizesObj = {
			xlScreen: 1200,
			lScreen: 992,
			mScreen: 768,
			sScreen: 576,
			xsScreen: 320
		}

		var actualSize = sizesObj[sizeParam];
		if (windowWidth < actualSize) {
			return actualSize;
		}
	}

	if (windowIsUnder(768)) {
		for (var i = 0; i < sideSections.length; i++) {
			var allSideSections = sideSections[i];
			mobsidebar.appendChild(allSideSections);
		}
	}

	for (var i = 0; i < consoleTypeName.length; i++) {
		consoleTypeName[i].onclick = function() {
			var thisTypeName = this.getAttribute('data-name');
			for (var j = 0; j < consoleTypeName.length; j++) {
				consoleTypeName[j].classList.remove('active');
				consoleType[j].classList.remove('active');
			}
			this.classList.add('active');
			document.querySelector('.' + thisTypeName).classList.add('active');
		};
	}

	if (checkProgressBarDesktop) {
		var progressBarWidth = 100 / countSliderItems;
		var progressbarPercent = progressBarWidth;
		document.querySelector('.progressBarSlider span').setAttribute('style', 'width: ' + progressbarPercent + '%');
	}

	ggLeft.onclick = function() {
		if (index > 0) {

			index--;
			sliderWidth = slider.offsetWidth * index;
			slider.style.transform = 'translate3d(-' + sliderWidth + 'px,0,0)';
			if (checkProgressBarDesktop) {
				progressbarPercent = progressbarPercent - progressBarWidth;
				document.querySelector('.progressBarSlider span').setAttribute('style', 'width: ' + progressbarPercent + '%');
				var activeSlideTitle = document.querySelector('.brand-title .active');
				activeSlideTitle.previousElementSibling.classList.add('active');
				activeSlideTitle.classList.remove('active');
			}

		}
	}

	ggRight.onclick = function() {
		if (index != countSliderItems - 1) {

			index++;
			sliderWidth = slider.offsetWidth * index;
			slider.style.transform = 'translate3d(-' + sliderWidth + 'px,0,0)';
			if (checkProgressBarDesktop) {
				progressbarPercent = progressBarWidth + progressbarPercent;
				document.querySelector('.progressBarSlider span').setAttribute('style', 'width: ' + progressbarPercent + '%')
				var activeSlideTitle = document.querySelector('.brand-title .active');
				activeSlideTitle.nextElementSibling.classList.add('active');
				activeSlideTitle.classList.remove('active');
			}
		}
	}
</script>