{assign var="is_al" value=$al|fn_isAL}

<style>
    .single-wd{
        width:100%;
    }
    .single-wd .w50{
        flex:none;
        width:250px;
    }
    .single-wd .w66{
        width:100%;
    }

    @media screen and (max-width: 768px) {
        .tygh-content {
            padding-top: 0;
        }
    }
</style>

<div class="wd-title flexing flex-h-between flex-v-center flex-column-xs">
    <div class="wd-title-holder">
        <div class="wd-title-scroll flexing flex-v-center">
            {foreach from=$item_names item=item_name}
            <h3>{$item_name}</h3>
            {/foreach}
        </div>
    </div>
    <div class="title-options flexing flex-h-between-xs">
        <div class="bullets flexing flex-v-center"></div>
        <div class="wd-filter">
            <span class="toggle-wdFilter flexing flex-v-center">
                <span>Rendit sipas</span>
                <svg><use xlink:href="#svg-down"></use></svg>
            </span>
            <div class="filter-container">
                <ul class="wd-filter-list">
                    <li data-cat="fCat">
                        <span>Kategorisë</span>
                        <svg><use xlink:href="#svg-right"></use></svg>
                    </li>
                    <li data-cat="fBrand">
                        <span>Brendeve</span>
                        <svg><use xlink:href="#svg-right"></use></svg>
                    </li>

                </ul>
                <div id="fCat" class="wd-filter-wrappers">
                    <div class="wd-filter-title flexing flex-v-center">
                        <svg><use xlink:href="#svg-left"></use></svg>
                        <span>Kategoritë</span>
                    </div>
                    <ul>
                        {assign var="ccounter" value=1}
                        {foreach from=$categories item=category}
                        <li class="filter" data-type="category" data-current="{$filter}" data-index="{$ccounter}">{$category}</li>
                        {assign var="ccounter" value=$ccounter+1}
                        {/foreach}
                    </ul>
                </div>
                <div id="fBrand" class="wd-filter-wrappers">
                    <div class="wd-filter-title flexing flex-v-center">
                        <svg><use xlink:href="#svg-left"></use></svg>
                        <span>Brendet</span>
                    </div>
                    <ul>
                        {assign var="bcounter" value=1}
                        {foreach from=$brands item=brand}
                        <li class="filter" data-type="brand" data-current="{$filter}" data-index="{$bcounter}">{$brand}</li>
                        {assign var="bcounter" value=$bcounter+1}
                        {/foreach}
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="wd-slide">
    <span class="wd-left flexing flex-v-center flex-h-center" style="display:none;"><svg><use xlink:href="#svg-left"></use></svg></span>
    <span class="wd-right flexing flex-v-center flex-h-center"><svg><use xlink:href="#svg-right"></use></svg></span>
    <div class="wd-wrapper cat1">
        <div class="wd-scroll">
            {foreach from=$group item=category}
            {assign var="count" value=$category|count}
            {assign var="count" value=$count - 1}
            {assign var="features" value=$category[0].features|fn_get_product_features_limited}
            <div class="wd-cats {if $count <= 4}single-wd{/if}">
                <div class="flexing flex-h-between flex-column-xs">
                    <div class="wd-main-box w66 w100-xs flex-grow ">
                        <div class="flexing flex-v-center wd-main-wrapper flex-column-xs" style="border-left: 1px solid #ddd;">
                            <div class="wdm-img w50 w100-xs" >
                               <a href=""><img src="https://hhstsyoejx.gjirafa.net/gj50/img/{$category[0].product_id}/img/0.jpg" alt=""></a>
                            </div>
                            <div class="wdm-content ml15 w66 mt20-xs w100-xs m0-xs">
                                <a href="{"products.view&product_id={$category[0].product_id}"|fn_url}?utm_content=weekly_deals"><h2 class="text-light max-rows mr2">{$category[0].product}</h2></a>
                                <ul class="detTech">
                                    {foreach from=$features[0] key=key item=value}
                                    <li>
                                        <span>{$key}</span>
                                        <span>{$value}</span>
                                    </li>
                                    {/foreach}
                                </ul>
                                <h2 class="mt20 mb0">{if $is_al}{$category[0].price|number_format} Lekë{else}{number_format((float)$category[0].price, 2, '.', '')} €{/if} 
                                    {if $category[0].old_price && !$is_al}
                                    <span class="text-muted h5" style="text-decoration:line-through; font-size:14px; font-weight: 400;">{number_format((float)$category[0].old_price, 2, '.', '')} €</span>
                                    {elseif $category[0].old_price_al && $is_al}
                                    <span class="text-muted h5" style="text-decoration:line-through; font-size:14px; font-weight: 400;">{$category[0].old_price_al} Lekë</span>
                                    {/if}
                                </h2>
                                <p class="max-rows mr2 mb10 text-muted text-light">{$category[0].description}</p>
                                <div class="wd-main-button flexing">
                                    <form action="{""|fn_url}" method="post" class="cm-ajax">
                                        <input type="hidden" name="result_ids" value="cart_status*,wish_list*,checkout*,account_info*"/>
                                        <input type="hidden" name="redirect_url" value="index.php?dispatch=pages.weekly-deals"/>
                                        <input type="hidden" name="product_data[{$category[0].product_id}][product_id]" value="{$category[0].product_id}"/>

                                        <a class="btn btn-primary w33-xs btn-custom" href="{"products.view&product_id={$category[0].product_id}"|fn_url}?utm_content=weekly_deals">SHIKO DETAJET</a>
                                        <button type="submit" name="dispatch[checkout.add..{$category[0].product_id}]" class="w33-xs ty-btn__primary ty-btn__add-to-cart"><svg><use xlink:href="#svg-cart"></use></svg></button>
                                        <button type="submit" name="dispatch[wishlist.add..{$category[0].product_id}]" class="w33-xs"><svg><use xlink:href="#svg-heart"></use></svg></button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        {if $category[1] && $count > 1}
                        <div class="w99 flexing {* flex-h-between *} grid-mobile-half" style="border-left: 1px solid #ddd;">
                            {for $var=1 to 3}
                            {if $smarty.session.auth.vip && !$is_al && $category[$var].vip_price != null}
                                {if $category[$var].old_price < $category[$var].vip_price}
                                    {$category[$var].old_price = $category[$var].price}
                                {/if}
                                {if $category[$var].price > $category[$var].vip_price}
                                    {$category[$var].price = $category[$var].vip_price}
                                {/if}
                            {/if}
                            {if $category[$var]}
                            <div class="wd-off-box {if $count == 2}w100{else}w33{/if} w50-xs flexing flex-column flex-h-between">
                                <a href="{"products.view&product_id={$category[$var].product_id}"|fn_url}?utm_content=weekly_deals">
                                    <div class="w50 block-center wd-image flexing flex-v-center">
                                        <img src="https://hhstsyoejx.gjirafa.net/gj50/img/{$category[$var].product_id}/thumb/0.jpg" alt="">
                                    </div>
                                </a>
                                <div class="wd-content">
                                    <a href="{"products.view&product_id={$category[$var].product_id}"|fn_url}?utm_content=weekly_deals">
                                        <h3 class="max-rows mr1">{$category[$var].product}</h3>
                                    </a>
                                    <h3>{if $is_al}{$category[$var].price|number_format} Lekë{else}{number_format((float)$category[$var].price, 2, '.', '')} €{/if} 
                                        {if $category[$var].old_price && !$is_al}
                                        <span class="text-muted h5" style="text-decoration:line-through; font-size:14px; font-weight: 400;">{number_format((float)$category[$var].old_price, 2, '.', '')} €</span>
                                        {elseif $category[$var].old_price_al && $is_al}
                                        <span class="text-muted h5" style="text-decoration:line-through; font-size:14px; font-weight: 400;">{$category[$var].old_price_al|number_format} €</span>
                                        {/if}
                                    </h3>
                                </div>
                            </div>
                            {/if}
                            {/for}
                        </div>
                        {/if}
                    </div>
                    {if $category[4]}
                    <div class="flexing flex-wrap w33 w100-xs flex-grow">
                        {for $var=4 to 6}
                        {if $category[$var]}
                        <div class="wd-off-box w100 flexing flex-v-center">
                            <div class="w33 flex-none">
                                <a href="{"products.view&product_id={$category[$var].product_id}"|fn_url}?utm_content=weekly_deals">
                                    <img src="https://hhstsyoejx.gjirafa.net/gj50/img/{$category[$var].product_id}/thumb/0.jpg" alt="">
                                </a>
                            </div>
                            <div class="wd-content ml15">
                                <a href="{"products.view&product_id={$category[$var].product_id}"|fn_url}?utm_content=weekly_deals">
                                    <h3 class="max-rows mr2">{$category[$var].product}</h3>
                                </a>
                                <h3>{if $is_al}{$category[$var].price|number_format} Lekë{else}{number_format((float)$category[$var].price, 2, '.', '')} €{/if} 
                                    {if $category[$var].old_price && !$is_al}
                                    <span class="text-muted h5" style="text-decoration:line-through; font-size:14px; font-weight: 400;">{number_format((float)$category[$var].old_price, 2, '.', '')} €</span>
                                    {elseif $category[$var].old_price_al && $is_al}
                                    <span class="text-muted h5" style="text-decoration:line-through; font-size:14px; font-weight: 400;">{$category[$var].old_price_al|number_format} €</span>
                                    {/if}
                                </h3>
                            </div>
                        </div>
                        {/if}
                        {/for}
                    </div>
                    {elseif $count == 1}
                    <div class="flexing flex-wrap w100-xs flex-grow">
                        {if $category[1]}
                        <div class="wd-off-box w100 flexing flex-v-center">
                            <div class="w33 flex-none">
                                <a href="{"products.view&product_id={$category[1].product_id}"|fn_url}?utm_content=weekly_deals">
                                    <img src="https://hhstsyoejx.gjirafa.net/gj50/img/{$category[1].product_id}/thumb/0.jpg" alt="">
                                </a>
                            </div>
                            <div class="wd-content ml15">
                                <a href="{"products.view&product_id={$category[1].product_id}"|fn_url}?utm_content=weekly_deals">
                                    <h3 class="max-rows mr2">{$category[1].product}</h3>
                                </a>
                                <h3>{if $is_al}{$category[1].price|number_format} Lekë{else}{number_format((float)$category[1].price, 2, '.', '')} €{/if} 
                                    {if $category[1].old_price && !$is_al}
                                    <span class="text-muted h5" style="text-decoration:line-through; font-size:14px; font-weight: 400;">{number_format((float)$category[1].old_price, 2, '.', '')} €</span>
                                    {elseif $category[1].old_price_al && $is_al}
                                    <span class="text-muted h5" style="text-decoration:line-through; font-size:14px; font-weight: 400;">{$category[1].old_price_al|number_format} €</span>
                                    {/if}
                                </h3>
                            </div>
                        </div>
                        {/if}
                    </div>
                    {/if}
                </div>
                {if $category[7]}
                <div class="flexing flex-wrap w99" style="border-left: 1px solid #ddd; border-bottom: 1px solid #ddd;">
                    {for $var=7 to 10}
                    {if $category[$var]}
                    <div class="wd-off-box {if $count >= 10}w25{elseif $count == 9}w33{elseif $count == 8}w50{elseif $count == 7}w100{/if} w50-xs flexing flex-column flex-h-center">
                        <a href="{"products.view&product_id={$category[$var].product_id}"|fn_url}?utm_content=weekly_deals">
                            <div class="wd-image flexing flex-v-center">
                                <img class="block-center" src="https://hhstsyoejx.gjirafa.net/gj50/img/{$category[$var].product_id}/thumb/0.jpg" alt="">
                            </div>
                        </a>
                        <div class="wd-content">
                            <a href="{"products.view&product_id={$category[$var].product_id}"|fn_url}?utm_content=weekly_deals">
                                <h3 class="max-rows mr2">{$category[$var].product}</h3>
                            </a>
                            <h3>{if $is_al}{$category[$var].price|number_format} Lekë{else}{number_format((float)$category[$var].price, 2, '.', '')} €{/if} 
                                {if $category[$var].old_price && !$is_al}
                                <span class="text-muted h5" style="text-decoration:line-through; font-size:14px; font-weight: 400;">{number_format((float)$category[$var].old_price, 2, '.', '')} €</span>
                                {elseif $category[$var].old_price_al && $is_al}
                                <span class="text-muted h5" style="text-decoration:line-through; font-size:14px; font-weight: 400;">{$category[$var].old_price_al|number_format} €</span>
                                {/if}
                            </h3>
                        </div>
                    </div>
                    {/if}
                    {/for}
                </div>
                {/if}
            </div>
            {/foreach}
         </div>
    </div>
</div>

<script>
    var categories = document.querySelectorAll('.wd-cats').length,
        bulletWrapper = document.querySelector('.bullets'),
        leftArrow = document.querySelector('.wd-left'),
        rightArrow = document.querySelector('.wd-right'),
        index = 1,
        itemWidth = document.querySelector('.wd-wrapper').offsetWidth + 2,
        titleWidth = document.querySelector('.wd-title-holder').offsetWidth,
        titleScroll = document.querySelector('.wd-title-scroll'),
        fCatItems = document.querySelectorAll('#fCat li'),
        fBrandItems = document.querySelectorAll('#fBrand li');

    for(var i = 0; i < categories; i++){
        var bullets = document.createElement("SPAN");
        if(i == 0) bullets.className = " active";
        bulletWrapper.appendChild(bullets);
        bullets.addEventListener('click', function () {
            slider(0,1,0, counter(this))
            this.classList.add('active');
        })
        fCatItems[i].addEventListener('click', function () {
            if (this.dataset.type == this.dataset.current) {
                slider(0,1,0, counter(this))
                bulletWrapper.children[index -1].classList.add('active')
            }
        })
    }

    for (var i = 0; i < fBrandItems.length; i++) {
        fBrandItems[i].addEventListener('click', function () {
            if (this.dataset.type == this.dataset.current) {
                slider(0,1,0, counter(this))
                bulletWrapper.children[index -1].classList.add('active')
            }
        })
    }

    rightArrow.addEventListener('click', function () {
        slider(0,0,1)
    });
    leftArrow.addEventListener('click', function () {
        slider(1,0,0)
    });

    function slider(left, mid, right, count){
        for(var i = 0; i < bulletWrapper.childNodes.length; i++){
            bulletWrapper.childNodes[i].className = " ";
        }
        if(left == true){
            if(index > 1){
                index--;
                bulletWrapper.children[index -1].classList.add('active');
                document.querySelector('.wd-scroll').style.transform += 'translate3d(' + itemWidth + 'px, 0, 0)';
                titleScroll.style.transform += 'translate3d(' + titleWidth + 'px, 0,0)';
            }
            hideShowArrows();
        }
        if(mid == true){
            index = count;
            hideShowArrows();
            document.querySelector('.wd-scroll').setAttribute('style', 'transform:translate3d(-'+ (itemWidth  * count - itemWidth) + 'px, 0,0)');
            titleScroll.setAttribute('style', 'transform:translate3d(-'+ (titleWidth  * count - titleWidth) + 'px, 0,0)');

        }
        if(right == true){
            if(index <= categories){
                index++;
                bulletWrapper.children[index -1].classList.add('active');
                document.querySelector('.wd-scroll').style.transform += 'translate3d(-' + itemWidth + 'px, 0, 0)';
                titleScroll.style.transform += 'translate3d(-' + titleWidth + 'px, 0,0)';
            }
            hideShowArrows();
        }
    }
    function hideShowArrows(){
        if(index > 1) rightArrow.style.display = "flex";
        if(index <= categories) leftArrow.style.display = "flex";

        if(index == 1) {
            leftArrow.style.display = "none";
            rightArrow.style.display = "flex";
        }

        if(index == categories) rightArrow.style.display = "none";
    }
    function counter(el) {
        if (!el) return -1;
        var k = 0;
        do {
            k++;
        } while (el = el.previousElementSibling);
        return k;
    }



    var filterCount = document.querySelectorAll('.wd-filter-list li'),
        catItems = document.querySelectorAll('.wd-filter-title'),
        dateAheight = 0;

    document.querySelector('.toggle-wdFilter').addEventListener('click', function () {
        dateAheight = document.querySelector('.wd-filter-list').offsetHeight;
        document.querySelector('.filter-container').style.height = dateAheight + 'px';
        document.querySelector('.filter-container').classList.add('filters-open');
    })

    for(var i = 0; i<filterCount.length; i++){
        filterCount[i].addEventListener('click', function () {
            var dateAte =  this.getAttribute('data-cat');
                dateAheight = document.querySelector('#' + dateAte).offsetHeight;

            document.querySelector('.filter-container').style.height = dateAheight + 'px';
            document.querySelector('#' + dateAte).classList.add('filter-active');
            this.parentElement.classList.add('filter-deactive');
        })
    }
    for (var i = 0; i < catItems.length; i++){
        catItems[i].addEventListener('click', function(){
            dateAheight = document.querySelector('.wd-filter-list').offsetHeight;
            document.querySelector('.filter-container').style.height = dateAheight + 'px';
            document.querySelector('.wd-filter-list').classList.remove('filter-deactive');
            this.parentElement.classList.remove('filter-active');
        });
    }
    document.addEventListener('click', function (event) {
        if (!event.target.closest('.filters-open') && document.querySelector('.filter-container').classList.contains('filters-open')) {
            document.querySelector('.filter-container').classList.remove('filters-open');
            document.querySelector('.wd-filter-list').classList.remove('filter-deactive');
            for(var i = 0; i<filterCount.length; i++) {
                document.querySelectorAll('.wd-filter-wrappers')[i].classList.remove('filter-active');
            }

        }

    }, true);
</script>