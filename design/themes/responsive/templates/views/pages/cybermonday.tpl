<link rel="stylesheet" href="images/black-friday17/cm22.css?v=19"/>
<div class="bf-cover">
    <video oncontextmenu="return false" id="bf-vid" autoplay loop poster="images/black-friday17/covercm.jpg">
            <source src="images/black-friday17/CM50.webm" type="video/webm"/>
    </video>
</div>
<div class="bf-bottom">
    <div id="bf-countdown" class="text-center"></div>
</div>
<div class="container-fluid" id="products">
    <div class="row">
        {assign "iteration" 0}
        {assign "cat" 0}
        {foreach $csv_products as $product}{if $elastic_products[$product.product_id] == null}{continue}{/if}<div class="product small bbox"><div class="product-inner clearfix">
                <a class="bf-link" href="/index.php?dispatch=products.view&product_id={$product.product_id}">
                        <h3 class="title">{$elastic_products[$product.product_id].product}</h3>
                        {if $elastic_products[$product.product_id].amount > 0}
                            <img class="stockimage" src="https://gjirafa50.com/images/icons/upto48.svg"/>
                        {/if}
                        <div class="image-wrapper">
                            <img thumb="{get_images_from_blob product_id=$product.product_id count=1}"/>

                        </div>
                    {if $cm_s}
                        <div class="price-comparison">

                            <h4 class="ty-center">{if $product.amazon_price != 0}AMAZON <i class="ty-icon-right-dir" style="margin:0 2px;"></i> KOSOVË = €{$product.amazon_price}{/if}</h4>
                            {math assign="percentage" equation='(x-y)/x*100' x=$elastic_products[$product.product_id].price y=$product.new_price}
                            <div class="cm-price" {if $percentage <= 0}style="padding: 18px 10px;"{/if}>
                                <h1>€{$product.new_price}</h1>
                                <span>
                                    {if $percentage > 0}
                                        <strong>{$percentage|string_format:"%d"}%</strong> ZBRITJE
                                    {/if}
                                </span>
                            </div>
                        </div>
                    {/if}
                </a>
                {if $cm_s}
                    <a href="/index.php?dispatch=products.view&product_id={$product.product_id}" class="ty-btn ty-btn__primary bbox bnow">
                        {if $elastic_products[$product.product_id].out_of_stock}
                            E SHITUR
                        {else}
                            BLEJ TANI
                        {/if}
                    </a>
                {/if}

                {assign var="stock" value="`$elastic_products[$product.product_id].amount+$elastic_products[$product.product_id].czc_stock+$elastic_products[$product.product_id].czc_retailer_stock`"}
                {if $stock == "0"}
                    <div class="sold-out-bf bbox"><img src="images/black-friday17/so.png"/></div>
                {/if}
            </div>

            </div>{$iteration = $iteration + 1}{if $iteration % 20 == 0 && $cat < 4}<div class="span16 catimg"><a href="{$categories_img[$cat]}"><div class="catimg-inner ty-center"><img src="https://hhstsyoejx.gjirafa.net/gj50/{$cat}.jpg"/></div></a></div>{$cat = $cat + 1}{/if}{/foreach}
    </div>
</div>
<div id="scrollbarmobile">
    <span></span>
</div>
<script src="js/lib/lazyload/lazyload.js"></script>
<script>
{if !$cm_s}
            {literal}var end=new Date('11/27/2017 00:00 AM');var _second=1000;var _minute=_second*60;var _hour=_minute*60;var _day=_hour*24;var timer;function showRemaining(){var now=new Date();var distance=end-now;if(distance<0){clearInterval(timer);document.getElementById('bf-countdown').innerHTML='';return}var days=Math.floor(distance/_day);var hours=Math.floor((distance%_day)/_hour);var minutes=Math.floor((distance%_hour)/_minute);var seconds=Math.floor((distance%_minute)/_second);document.getElementById('bf-countdown').innerHTML='<span class="startat">Fillon edhe</span> '+days+'<span> ditë</span> ';document.getElementById('bf-countdown').innerHTML+=hours+'<span> orë</span> ';document.getElementById('bf-countdown').innerHTML+=minutes+'<span> min</span> ';document.getElementById('bf-countdown').innerHTML+=seconds+'<span> sek</span>'}
    timer=setInterval(showRemaining,1000);{/literal}
{else}
{literal}var end=new Date('11/28/2017 00:00 AM');var _second=1000;var _minute=_second*60;var _hour=_minute*60;var _day=_hour*24;var timer;function showRemaining(){var now=new Date();var distance=end-now;if(distance<0){clearInterval(timer);document.getElementById('bf-countdown').innerHTML='';return}var days=Math.floor(distance/_day);var hours=Math.floor((distance%_day)/_hour);var minutes=Math.floor((distance%_hour)/_minute);var seconds=Math.floor((distance%_minute)/_second);document.getElementById('bf-countdown').innerHTML='<span class="startat">Mbaron edhe</span> '+days+'<span> ditë</span> ';document.getElementById('bf-countdown').innerHTML+=hours+'<span> orë</span> ';document.getElementById('bf-countdown').innerHTML+=minutes+'<span> min</span> ';document.getElementById('bf-countdown').innerHTML+=seconds+'<span> sek</span>'}
timer=setInterval(showRemaining,1000);{/literal}
{/if}
{literal}
    $(window).scroll(function(e) {
        var $this = $(this),
            docHeight = $(document).height() - $(window).height();
        $('#scrollbarmobile span').css('height', ($this.scrollTop() * 100) / docHeight + '%');
        var a = $(window).scrollTop();
        $(".category-section").each(function(t) {
            if($(this).position().top <= a){
                $("#category-menu a.active").removeClass("active");
                $('#category-menu a[data-id="' + $(this).data("cat") + '"]').addClass("active");
            }
        });
    });
    document.getElementById('bf-vid').addEventListener('contextmenu', event => event.preventDefault());
    {/literal}
</script>