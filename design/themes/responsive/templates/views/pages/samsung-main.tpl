<style>
    .container-fluid {
        margin: 0 !important;
        padding: 0 !important;
        max-width: unset !important;
    }
</style>

<div id="esis"></div>
<script
        type="text/javascript"
        src="https://test.esisapp.com/loader/esis-loader.v0.js"
        data-esis-base-route="/samsung"
        data-esis-partner="gjirafaks"
        data-esis-locale="al"
        data-esis-wrapper="esis"
        async ></script>