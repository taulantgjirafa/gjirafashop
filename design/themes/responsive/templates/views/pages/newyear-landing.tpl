<link rel="stylesheet" href="var/themes_repository/responsive/css/lib/peppermint.css">
<style>
    .tygh-content {
        background: #0b1534;
    }

    .load_more_category {
        background: #0B1534;
    }

    #load_more_infinite {
        box-shadow: 0 4px 10px -4px #000;
    }

    .pd-header{
        border-bottom: 0;
        margin-top: 30%;
    }

    @media screen and (max-width:768px){
        .pd-header{
            margin-top:250px;
        }
        .pd-header::after, .pd-header::before{
            content:none;
        }
    }

    .pd-wrapper{
        position: relative;
        background:#fff;
        z-index: 1;
        /*border-radius:10px;*/
    }

    .sold-out-msi {
        position: absolute;
        width: 100%;
        height: 100%;
        background-color: rgba(255,255,255,0.5);
    }

    .sold-out-msi h4 {
        border-radius: 3px;
        box-shadow: 1px 1px 2px 0 #bebebe;
        color: #111;
        width: auto;
        transform: none;
        font-weight: bold;
        background: none;
        border: none;
        box-shadow: none;position: absolute;
        top: 45%;
        left: 40%;
    }

    .msi-loader {
        background: url(design/themes/responsive/media/images/icons/loader.svg) no-repeat;
        background-size: contain;
        width: 50px;
        height: 50px;
        position: absolute;
        top: 40%;
        left: 40%;
    }

    .pd-grid .grid-single.empty {
        background: #0B1534;
        max-width: 100%;
    }

    .pd-grid .grid-single {
        position: relative;
        border-color: #0B1534;
    }

    #empty_category_container {
        background-color: #0B1534;
        border-radius: 10px;
        border: 5px solid #fff;
        margin: 0 !important;
    }

    .pd-wrapper .pd-single {
        border-color: #0B1534;
    }

    .banner-newyear-web {
        background-image: url(https://hhstsyoejx.gjirafa.net/gj50/banners/d-1576761976.jpg);
        background-position: center center;
        width: 100%;
        height: 400px;
        background-size: cover;
        position: absolute;
        left: 0;
        top: 0;
    }

    .banner-newyear-mobile {
        background-image: url(https://gjirafaadnetwork.blob.core.windows.net/html5/360x300.jpg);
        background-position: center center;
        width: 100%;
        height: 300px;
        background-size: cover;
        position: absolute;
        left: 0;
        top: 0;
    }
</style>
<main class="msi">
<div class="newyear-bg">
    {* <video style="height:auto;" class="hidden-phone" autoplay muted loop controls="false" allowfullscreen="false"> *}
        {* <source src="images/landing-page/Gjirafa50_landing.mp4" type="video/mp4"> *}
        <div class="hidden-phone banner-newyear-web"></div>
    {* </video> *}
    {* <video class="hidden-desktop" autoplay muted loop controls="false" allowfullscreen="false"> *}
        {* <source src="images/landing-page/Gjirafa50_landingmobile.mp4" type="video/mp4"> *}
        <div class="hidden-desktop banner-newyear-mobile"></div>
    {* </video> *}
</div>
    <div class="pd-header">
        {* <ul class="flexing flex-h-between">
            <li class="{if $smarty.get.kategoria == $key}active{/if}"><a href="{"pages.newyear-landing"|fn_url}">MSI</a></li>
            {foreach from=$categories item=category key=key}
            <li class="{if $smarty.get.kategoria == $key}active{/if}"><a href="{"pages.newyear-landing&kategoria=`$key`"|fn_url}">{$category}</a></li>
            {/foreach}
        </ul> *}
    </div>
<div class="pd-wrapper">
{include file="views/pages/landing/products.tpl"}
{if !$hide_show_more}
    <div class="load_more_category" style="text-align: center;">
        <a href="#" class="ty-btn__primary ty-btn" id="load_msi" style="box-shadow: 0 4px 10px -4px #000;">SHFAQ MË SHUMË PRODUKTE</a>
        <div id="is_loader" class="is_loader active" style="display: none; margin: 0px auto;"></div>
        <div id="no_results" style="display:none;text-align:center;padding:30px 0px;font-size:14px;">
            <span class="text-muted">Fundi i rezultateve</span>
        </div>
    </div>
{/if}
</div>
</main>

<script type="text/javascript">
    function loadedContent() {
        $('.peppermint').css('opacity', '1');
        $('.msi-loader').css('display', 'none');
    }

    function goto(url) {
        window.location = url;
    }
</script>
<script type="text/javascript" onload="loadedContent();" src="js/lib/Peppermint.js?ver=4.3.9"></script>
<script>
    $('.peppermint').Peppermint({
        dots: true,
        speed: 500,
        slideshowInterval: 5000,
        stopSlideshowAfterInteraction: true,
        onSetup: function(n) {

        }
    });
    $('body').on('click', '.right-arr', function(){
        var slider = $(this).parent().parent().find('.peppermint');
        slider.data('Peppermint').next();
    });
    $('body').on('click', '.left-arr', function(){
        var slider = $(this).parent().parent().find('.peppermint');
        slider.data('Peppermint').prev();
    });
    $('body').on('click', '.expandDet', function(){
        $(this).prev().toggleClass('detOpen');
        $(this).toggleClass('rotSvg');
    });

    document.title = "Zbritjet e fundvitit - Gjirafa50";
</script>

<script>
    {literal}

    var offset = 100;
    var clicked = false;
    var infinite = false;

    $('body').on('click', '#load_msi', function(){
        if(clicked)
            return false;

        if(offset == 100){
            infinite = true;
        }

        $('.is_loader').show();
        clicked = true;
        var url_string = window.location.href;
        var url = new URL(url_string);
        // url = '/gjirafa5038/index.php?dispatch=pages.newyear-landing';
        url = '/index.php?dispatch=pages.newyear-landing';
        $.ajax({
            url: url,
            data: {isAjax: 1, limit: 100, offset: offset},
            success: function (res) {
                offset = offset + 100;
                clicked = false;
                $('#tygh_footer').css('height', 0);
                $('.grid-single.empty').hide();
                $('.pd-grid').append(res);
                $('.is_loader').hide();
                $('#load_msi').show();
            }
        });

        return false;
    });

    {/literal}
</script>