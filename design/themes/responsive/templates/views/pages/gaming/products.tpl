{foreach from=$products item=product}
<div class="brand-item">
    <h3 class="item-title">{$product.product|unescape}</h3>
    <div class="brand-image">
        <div style="background-image:url('https://hhstsyoejx.gjirafa.net/gj50/img/{$product.product_id}/thumb/0.jpg');"></div>
    </div>
    <div class="price-buttons">
        <div><span>{number_format((float)$product.price, 2, '.', '')} €</span></div>
        <a href="{"products.view&product_id={$product.product_id}"|fn_url}?utm_source=gaming-landing"><span>BLEJ</span></a>
    </div>
</div>
{/foreach}