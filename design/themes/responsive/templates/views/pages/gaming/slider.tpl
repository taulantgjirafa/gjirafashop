<section>
    <div class="gg-brand-slider">
        <div class="gg-arrows">
            <span class="gg-left"><svg><use xlink:href="#svg-left"></use></svg></span>
            <span class="gg-right"><svg><use xlink:href="#svg-right"></use></svg></span>
        </div>
        <div class="progressBarSlider">
            <span></span>
        </div>
        <div class="gg-slider-wrapper">
            <div class="slider-animation">
                {foreach from=$slider item=slide_container}
                <div class="gg-brand-item">
                    <div class="hidden-phone">
                        <div class="brand-items-wrapper flexing flex-h-between">
                            {foreach from=$slide_container item=slide_item}
                                {foreach from=$slide_item item=product}
                                <div class="brand-item">
                                    <img src="https://hhstsyoejx.gjirafa.net/gj50/img/{$product.product_id}/thumb/0.jpg" alt="">
                                    <h3>{$product.product}</h3>
                                    <div class="slide-buttons">
                                        <h2>{number_format((float)$product.price, 2, '.', '')} €</h2>
                                        <a href="{"products.view&product_id={$product.product_id}"|fn_url}?utm_source=gaming-landing" target="_blank"><span>BLEJ TANI</span></a>
                                    </div>
                                </div>
                                {/foreach}
                            {/foreach}
                        </div>
                    </div>
                    <div class="hidden-desktop w100">
                        <div class="banner-img-mob">
                            <div style="background-image:url('images/ZOWIE.jpg');"></div>
                        </div>
                    </div>
                </div>
                {/foreach}
            </div>
        </div>
        <div class="brand-title hidden-phone">
            {foreach from=$slider item=item key=key}
                <span {if $item@first}class="active"{/if}>{$categories_seo.$key}</span>
            {/foreach}
        </div>
        <span class="section-seperater"></span>

    </div>
</section>