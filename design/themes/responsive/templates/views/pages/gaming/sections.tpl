<section>
    <div class="device-type ">
        <div class="ggs-type ggside-sections hidden-desktop mob-sidebar">
        </div>
        <div class="gg-allTitle">
            <div class="pc-group console-type active">
                <span>{if $smarty.get.kategoria}{$smarty.get.kategoria|upper}{else}PC{/if}</span>
                {foreach from=$sub_categories item=sub_category key=key}
                <span {if $smarty.get.nenkategoria == $key}class="active"{/if}>
                    {if $kategoria}
                        <a data-category="{$kategoria}" data-subcategory="{$key}" class="filter" href="{"pages.gaming-landing&kategoria=`$kategoria`&nenkategoria=`$key`"|fn_url}">{$sub_category}</a>
                    {else}
                        <a data-subcategory="{$key}" class="filter" href="{"pages.gaming-landing&nenkategoria=`$key`"|fn_url}">{$sub_category}</a>
                    {/if}
                </span>
                {/foreach}
            </div>
        </div>
        <div class="brand-items-wrapper flexing flex-h-between">
            {foreach from=$priority_products item=p_product}
            <div class="brand-item">
                <span class="brand-item-logo">
                    {if $p_product.product|stristr:"msi"}
                    <img src="images/brands/msilogo.png" alt="">
                    {elseif $p_product.product|stristr:"razer"}
                    <img src="images/brands/razerlogo.png" alt="">
                    {elseif $p_product.product|stristr:"steelseries"}
                    <img src="images/brands/steelserieslogo.png" alt="">
                    {elseif $p_product.product|stristr:"zowie"}
                    <img src="images/brands/zowielogo.png" alt="">
                    {elseif $p_product.product|stristr:"playstation" || $p_product.product|stristr:"ps4"}
                    <img src="images/brands/playstationlogo.png" alt="">
                    {elseif $p_product.product|stristr:"xbox"}
                    <img src="images/brands/xboxlogo.png" alt="">
                    {else}
                    <img src="images/appicon/50-112px.png" alt="">
                    {/if}
                </span>
                <div class="gg-edgy">
                    <h3 class="item-title">{$p_product.product}</h3>
                    <div class="brand-image">
                        <div style="background-image:url('https://hhstsyoejx.gjirafa.net/gj50/img/{$p_product.product_id}/thumb/0.jpg');"></div>
                    </div>
                    <div class="hidden-desktop mob-brand-log">MSI</div>
                    <div class="price-buttons">
                        <div><span>{number_format((float)$p_product.price, 2, '.', '')} €</span></div>
                        <a href="{"products.view&product_id={$p_product.product_id}"|fn_url}?utm_source=gaming-landing"><span>BLEJ</span></a>
                    </div>
                </div>
            </div>
            {/foreach}
        </div>
    </div>
</section>
<section>
    <div class="gg-all">
    	{if $priority_products}
        	<div class="gg-allTitle">
            	<h4>TE GJITHA PRODUKTET</h4>
        	</div>
    	{/if}
        <div class="brand-items-wrapper flexing {* flex-h-between *} gg-all-wrapper" id="gaming_load_more_container">
        	{if $no_products}
				<h1>Kategoria për momentin nuk ka asnjë produkt, ju lutem kthehuni përsëri.</h1>
        	{else}
            	{include file="views/pages/gaming/products.tpl" products=$products}
        	{/if}
        </div>
    </div>
</section>