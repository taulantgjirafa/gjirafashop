<style>
    .tygh-content {
        background: #fff;
    }

    @font-face {
        font-family: "Samsung Sharp Sans Regular";
        src: url("{$config.https_location}/design/themes/responsive/media/fonts/SamsungSharpSansRegular-Regular.ttf");
    }

    @font-face {
        font-family: "Samsung Sharp Sans Bold";
        src: url("{$config.https_location}/design/themes/responsive/media/fonts/SamsungSharpSans-Bold.ttf");
    }

    .text-muted {
        color: #777 !important;
    }

    .samsung-landing .samsung-banner div {
        height: 100%;
    }

    .samsung-landing .samsung-top-p .ss-prod {
        width: 50%;
    }

    .samsung-landing .samsung-top-p .ss-prod > div {
        border-color: #000;
    }

    .samsung-landing .samsung-top-p .ss-prod:last-child > div {
        border: none;
    }

    .samsung-landing .samsung-top-p .ss-prod .ssp-content h3,
    .samsung-landing .samsung-top-p .ss-prod .ssp-content h2 {
        color: #000;
    }

    .product-overview h1 {
        color: #000;
    }

    .product-overview .po-item:before {
        background: linear-gradient(to right, transparent, #000, transparent);
    }

    .samsung-landing button:hover, .samsung-landing a:hover {
        border-color: #A07870;
    }

    .samsung-landing .compare-phone {
        padding-top: 0;
    }

    .samsung-landing .compare-phone button {
        background: #000;
        color: #fff !important;
    }

    .samsung-landing .samsung-top-p {
        margin: 0;
    }

    @media screen and (max-width: 768px) {

        .samsung-landing .samsung-top-p .ss-prod {
            width: 80%;
        }

        .tygh-content {
            padding-top: 0;
        }

        .product-overview .po-content {
            margin-top: 25px;
        }
    }

    .product-overview h2 {
        font-family: "Samsung Sharp Sans Regular";
        line-height: 25px;
        word-spacing: 5px;
        font-size: 17px;
    }

    .samsung-landing .samsung-modal .s-modal-img {
        background: none;
    }

    .samsung-landing .samsung-modal .modal-table-content {
        margin-top: 50px;
        padding-bottom: 30px;
        border-top: 0 !important;
    }
</style>
<span class="samsungoverlay"></span>
<div class="samsung-landing">
    <div class="samsung-modal">
        <span class="close-sam-modal"><svg><use xlink:href="#svg-back"></use></svg></span>
        <div class="modal-table-content">
            <h1>Ekran Super Smooth 120 Hz</h1>
            <h3>Ekrani ynë me lëvizje më të butë qëndron në një hap me furnizimet në ekran. Ky ekran me reagim tepër të
                jashtëzakonshëm, optimizon shpejtësinë e rifreskimit për atë që po bëni me çdo prekje.</h3>
        </div>
        <div class="modal-table-content">
            <h1>Dynamic AMOLED 2X</h1>
            <h3>Ndriçim i pakonceptueshëm. Gjallëri e pabesueshme. Ky ekran çuditërisht me lëvizje të butë, shkëlqen me
                ngjyra të gjalla dhe qartësi të mahnitshme, duke dhënë një përvojë shikimi të pashembullt. Dhe kjo edhe
                në dritën e diellit.</h3>
            <h4 class="text-muted">*Ekrani Dynamic AMOLED 2X në Galaxy S21 5G dhe S21+ 5G ka marrë certifikim nga VDE
                Germany për 100 përqind Mobile Color Volume në gamën e ngjyrave DCI-P3, që do të thotë se imazhet nuk
                lënë gjurmë dhe do të keni ngjyra të gjalla të pabesueshme pavarësisht niveleve të ndryshme të
                ndriçimit. Ekrani mund të arrijë ndriçimin maksimal deri në 1300 nit, duke përmirësuar kontrastin mes
                aspekteve të errëta dhe të ndritshme të përmbajtjes dixhitale për një cilësi figure më të shkëlqyer, me
                raport kontrasti 2 000 000:1 për ta bërë përvojën tuaj në celular më tërheqëse.</h4>
        </div>
        <div class="modal-table-content">
            <h1>Mburoja Eye Comfort</h1>
            <h3>Maratona juaj e ardhshme e filmave tani është edhe më komode. Galaxy S21 5G dhe S21 + 5G zvogëlojnë
                ndjeshëm lodhjen e syve, kështu që mund të shikoni pa ndërprerje programet tuaja të preferuara me
                komoditet në mëngjes, në drekë dhe natën.
                <h3>
                    <h4 class="text-muted">*SGS, kompania lider botërore e certifikimit, akordoi për ekranin e Galaxy
                        S21 5G dhe S21+ 5G Certifikimin Eye Care bazuar në aftësitë e veta për të reduktuar në mënyrë
                        drastike efektet e dëmshme të dritës blu. Ky certifikim mund të gjendet në www.sgs.com.</h4>
        </div>
    </div>
    <div class="samsung-banner">
        <div class="hidden-phone"
             style="background-image:url('https://hhstsyoejx.gjirafa.net/gj50/landings/s21/1920x500.jpg');"></div>
        <div class="hidden-desktop"
             style="background-image:url('https://hhstsyoejx.gjirafa.net/gj50/landings/s21/600x400.jpg');"></div>
    </div>
    <div class="samsung-top-p">
        <div class="ss-prod entry-animation" style="transition-delay: .1s;">
            <div>
                <div class="ssp-img">
                    <div class="s20gray active changable-s" id="s20gray"
                         style="background-image:url('https://hhstsyoejx.gjirafa.net/gj50/landings/s21/s21/gray.png')"></div>
                    <div class=" s20purple changable-s" id="s20purple"
                         style="background-image:url('https://hhstsyoejx.gjirafa.net/gj50/landings/s21/s21/violet.png')"></div>
                    <div class="s20white changable-s" id="s20white"
                         style="background-image:url('https://hhstsyoejx.gjirafa.net/gj50/landings/s21/s21/white.png')"></div>
                    <div class="s20pink changable-s" id="s20pink"
                         style="background-image:url('https://hhstsyoejx.gjirafa.net/gj50/landings/s21/s21/rose.png')"></div>
                </div>
                <div class="ssp-content">
                    <div class="phone-title">
                        <h3 class="s20gray active changable-s">Celular Samsung Galaxy S21 8GB/128GB, e hirtë </h3>
                        <h3 class="s20purple  changable-s">Celular Samsung Galaxy S21 8GB/128GB, vjollcë </h3>
                        <h3 class="s20white changable-s">Celular Samsung Galaxy S21 8GB/128GB, bardhë </h3>
                        <h3 class="s20pink changable-s">Celular Samsung Galaxy S21 8GB/128GB, rozë </h3>
                    </div>
                    <h3 class="text-muted" style="font-family: 'Samsung Sharp Sans Regular';">
                        <br/>
                        Memorie e brendshme GB: <span style="font-family: 'Samsung Sharp Sans Bold';">128</span> <br/>
                        Madhësia e ekranit inç:<span style="font-family: 'Samsung Sharp Sans Bold';"> 6.2</span>
                        <br/> Memoria operuese GB:<span style="font-family: 'Samsung Sharp Sans Bold';"> 8</span>
                        <br> Galaxy Watch Active 2 44mm: <span style="font-family: 'Samsung Sharp Sans Bold';"> FALAS</span>
                        <br">
                    </h3>
                    <div class="chose-colors">
                        <span class="phone-color active" data-color="s20gray" style="background:#5e6367"></span>
                        <span class="phone-color  " data-color="s20purple" style="background:#b8b8d2"></span>
                        <span class="phone-color " data-color="s20white"
                              style="background:#fff; border:1px solid #eee;"></span>
                        <span class="phone-color " data-color="s20pink" style="background:#fee0d8"></span>
                    </div>
                    <div class="ssp-btn-price">
                        <div>
                            <p style="text-decoration: line-through; color: #000; font-size: 16px; font-weight: normal;">929.50€</p>
                            <h2>849.00€</h2>
                        </div>
                        <div class="phone-links">
                            <a class="s20gray active changable-s"
                               href="https://gjirafa50.com/celulare-tablete-dhe-pajisje-smart/celulare/touchscreen/celular-samsung-galaxy-s21-8gb128gb-e-hirt%C3%AB-e-tejdukshme/">Blej
                                tani</a>
                            <a class="s20purple  changable-s"
                               href="https://gjirafa50.com/celulare-tablete-dhe-pajisje-smart/celulare/aksesore-1/celular-samsung-galaxy-s21-8gb128gb-vjollc%C3%AB-e-tejdukshme-0/">Blej
                                tani</a>
                            <a class="s20white changable-s"
                               href="https://gjirafa50.com/celulare-tablete-dhe-pajisje-smart/celulare/touchscreen/celular-samsung-galaxy-s21-8gb128gb-bardh%C3%AB-e-tejdukshme/">Blej
                                tani</a>
                            <a class="s20pink changable-s"
                               href="https://gjirafa50.com/celulare-tablete-dhe-pajisje-smart/celulare/touchscreen/celular-samsung-galaxy-s21-8gb128gb-roz%C3%AB-e-tejdukshme/">Blej
                                tani</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="ss-prod entry-animation" style="transition-delay: .22s;">
            <div>
                <div class="ssp-img">
                    <div class="s20plusblack changable-s" id="s20plusblack"
                         style="background-image:url('https://hhstsyoejx.gjirafa.net/gj50/landings/s21/s21+/black.png')"></div>
                    <div class="s20pluspurple changable-s active" id="s20pluspurple"
                         style="background-image:url('https://hhstsyoejx.gjirafa.net/gj50/landings/s21/s21+/violet.png')"></div>
                    <div class="s20plusgold changable-s" id="s20plusgold"
                         style="background-image:url('https://hhstsyoejx.gjirafa.net/gj50/landings/s21/s21+/silver.png')"></div>
                </div>
                <div class="ssp-content">
                    <div class="phone-title">
                        <h3 class="s20plusblack  changable-s">Celular Samsung Galaxy S21+, 8GB/128GB, e zezë </h3>
                        <h3 class="s20pluspurple active changable-s">Celular Samsung Galaxy S21+, 8GB/128GB, vjollcë </h3>
                        <h3 class="s20plusgold changable-s">Celular Samsung Galaxy S21+, 8GB/128GB, e
                            argjendtë </h3>
                    </div>

                    <h3 class="text-muted" style="font-family: 'Samsung Sharp Sans Regular'">
                        <br/>
                        Memorie e brendshme GB: <span style="font-family: 'Samsung Sharp Sans Bold';">128</span> <br/>
                        Madhësia e ekranit inç:<span style="font-family: 'Samsung Sharp Sans Bold';"> 6.7</span>
                        <br/> Memoria operuese GB:<span style="font-family: 'Samsung Sharp Sans Bold';"> 8</span>
                        <br> Galaxy Watch Active 2 44mm: <span style="font-family: 'Samsung Sharp Sans Bold';"> FALAS</span>
                        <br">
                    </h3>
                    <div class="chose-colors">
                        <span class="phone-color " data-color="s20plusblack" style="background:#000"></span>
                        <span class="phone-color active" data-color="s20pluspurple" style="background:#b8b8d2"></span>
                        <span class="phone-color " data-color="s20plusgold" style="background:#e2e2ea"></span>
                    </div>
                    <div class="ssp-btn-price">
                        <div>
                            <p style="text-decoration: line-through; color: #000; font-size: 16px; font-weight: normal;">1139.50€</p>
                            <h2>1059.00€</h2>
                        </div>
                        <div class="phone-links">
                            <a class="s20plusblack  changable-s"
                               href="https://gjirafa50.com/celulare-tablete-dhe-pajisje-smart/celulare/touchscreen/celular-samsung-galaxy-s21-8gb128gb-e-zez%C3%AB-e-tejdukshme/">Blej
                                tani</a>
                            <a class="s20pluspurple active changable-s"
                               href="https://gjirafa50.com/celulare-tablete-dhe-pajisje-smart/celulare/touchscreen/celular-samsung-galaxy-s21-8gb128gb-vjollc%C3%AB-e-tejdukshme/">Blej
                                tani</a>
                            <a class="s20plusgold  changable-s"
                               href="https://gjirafa50.com/celulare-tablete-dhe-pajisje-smart/celulare/touchscreen/celular-samsung-galaxy-s21-8gb128gb-e-argjendt%C3%AB-e-tejdukshme/">Blej
                                tani</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="ss-prod entry-animation" style="transition-delay: .33s;">
            <div>
                <div class="ssp-img">
                    <div class="s21ultrablack  active changable-s" id="s21ultrablack"
                         style="background-image:url('https://hhstsyoejx.gjirafa.net/gj50/landings/s21/ultra/black.png')"></div>
                    <div class=" s20plusgray   changable-s" id="s20plusgray"
                         style="background-image:url('https://hhstsyoejx.gjirafa.net/gj50/landings/s21/ultra/silver.png')"></div>
                </div>
                <div class="ssp-content">
                    <div class="phone-title">
                        <h3 class="s21ultrablack active  changable-s">Celular Samsung Galaxy S21 Ultra, 12GB/128GB, e
                            zezë </h3>
                        <h3 class="s20plusgray  changable-s">Celular Samsung Galaxy S21 Ultra, 12GB/128GB, e
                            argjendtë </h3>
                    </div>

                    <h3 class="text-muted" style="font-family: 'Samsung Sharp Sans Regular'">
                        <br/>
                        Memorie e brendshme GB: <span style="font-family: 'Samsung Sharp Sans Bold';">128</span> <br/>
                        Madhësia e ekranit inç:<span style="font-family: 'Samsung Sharp Sans Bold';"> 6.8</span>
                        <br/> Memoria operuese GB:<span style="font-family: 'Samsung Sharp Sans Bold';"> 12</span>
                        <br> Galaxy Watch Active 2 44mm: <span style="font-family: 'Samsung Sharp Sans Bold'"> FALAS</span>

                    </h3>
                    <div class="chose-colors">
                        <span class="phone-color  active" data-color="s21ultrablack" style="background:#000"></span>
                        <span class="phone-color " data-color="s20plusgray" style="background:#e2e2ea"></span>
                    </div>
                    <div class="ssp-btn-price">
                        <p style="text-decoration: line-through; color: #000; font-size: 16px; font-weight: normal;">1359.50€</p>
                        <h2>1249.00€</h2>
                        <div class="phone-links">
                            <a class="s21ultrablack active changable-s"
                               href="https://gjirafa50.com/celulare-tablete-dhe-pajisje-smart/celulare/touchscreen/celular-samsung-galaxy-s21-ultra-12gb128gb-e-zez%C3%AB-e-tejdukshme/">Blej
                                tani</a>
                            <a class="s20plusgray  changable-s "
                               href="https://gjirafa50.com/celulare-tablete-dhe-pajisje-smart/celulare/touchscreen/celular-samsung-galaxy-s21-ultra-12gb128gb-e-argjendt%C3%AB-e-tejdukshme/">Blej
                                tani</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="product-overview">
        <div class="po-item flexing">
            <div class="po-content text-right">
                <h1>Galaxy S21 Ultra 5G</h1>
                <h2 class="text-muted">
                    Projektuar me një kamerë unike me prerje të kontureve për të
                    krijuar një revolucion në fotografi - duke ju lejuar të regjistroni video kinematografike 8K dhe të
                    bëni fotografi epike, të gjitha me një veprim. Dhe me çipin më të shpejtë të Galaxy, xhamin më të
                    fortë, 5G dhe një bateri që zgjat gjatë gjithë ditës, Ultra i qëndron lehtësisht besnik emrit të
                    vet.
                </h2>
                <a href="https://gjirafa50.com/celulare-tablete-dhe-pajisje-smart/celulare/touchscreen/celular-samsung-galaxy-s21-ultra-12gb128gb-e-zez%C3%AB-e-tejdukshme/">Blej
                    tani</a>
            </div>

            <div class="po-img">
                <img src="https://hhstsyoejx.gjirafa.net/storage/gjmall/banners/1hMall/9dbc6e1bc4094c5ea0e3c3baef02fb1b_reduced-s21ultra_kategori.jpg"/>
            </div>
        </div>
        <div class="po-item flexing">
            <div class="po-img">
                <img src="https://hhstsyoejx.gjirafa.net/storage/gjmall/banners/1hMall/c045747eea5747b9a8e7af4a717a6f25_reduced-s21+_kategori.jpg"/>
            </div>
            <div class="po-content">
                <h1>Galaxy S21 5G | S21+ 5G</h1>
                <h2 class="text-muted">
                    Mos e humbisni kurrë atë fotografi perfekte përsëri. Njihuni me Galaxy S21 5G dhe S21+ 5G.
                    Projektuar për të revolucionarizuar videon dhe fotografinë, me rezolucion 8K përtej kinematografisë,
                    në mënyrë që të mund të bëni foto epike menjëherë nga videot. I ka të gjitha në dy madhësi: 64 MP,
                    çipi ynë më i shpejtë dhe një bateri masive që zgjat gjatë gjithë ditës. Gjërat sapo u bënë
                    epike. </h2>
                <a href="https://gjirafa50.com/celulare-tablete-dhe-pajisje-smart/celulare/touchscreen/celular-samsung-galaxy-s21-8gb128gb-e-argjendt%C3%AB-e-tejdukshme/">Blej
                    tani</a>
            </div>
        </div>
        <div class="po-item flexing flex-h-center center_image_s21">
            <div class="hidden-desktop half21mobile_wrap flexing flex-h-between">
                <div class="half_s21 ">
                    <h2>Galaxy S21+ 5g</h2>
                    <h1 class="text-muted">6.7"</h1>
                    <h3 class="text-muted">EKRANI INFINITY-O
                        DYNAMIC AMOLED 2X
                        MBUROJA EYE COMFORT
                        EKRAN SUPER SMOOTH 120HZ</h3>
                </div>
                <div class="half_s21 ">
                    <h2>Galaxy S21 5g</h2>
                    <h1 class="text-muted">6.2"</h1>
                    <h3 class="text-muted">EKRANI INFINITY-O
                        DYNAMIC AMOLED 2X
                        MBUROJA EYE COMFORT
                        EKRAN SUPER SMOOTH 120HZ</h3>
                </div>
            </div>
            <div class="half_s21 hidden-phone">
                <h2>Galaxy S21+ 5g</h2>
                <h1 class="text-muted">6.7"</h1>
                <h3 class="text-muted">EKRANI INFINITY-O
                    DYNAMIC AMOLED 2X
                    MBUROJA EYE COMFORT
                    EKRAN SUPER SMOOTH 120HZ</h3>
            </div>
            <div class="po-img">
                <img src="https://hhstsyoejx.gjirafa.net/storage/gjmall/banners/1hMall/617fecc403b94da7a8878a2545f2f534_reduced-doubles21.jpg"/>
            </div>
            <div class="half_s21 hidden-phone">
                <h2>Galaxy S21 5g</h2>
                <h1 class="text-muted">6.2"</h1>
                <h3 class="text-muted">EKRANI INFINITY-O
                    DYNAMIC AMOLED 2X
                    MBUROJA EYE COMFORT
                    EKRAN SUPER SMOOTH 120HZ</h3>
            </div>

        </div>
        <div class="flexing compare-phone flex-h-center pb20">
            <button class="">MËSONI MË SHUMË</button>
        </div>
    </div>
</div>

<style>
    .half21mobile_wrap {
        width: 100%;
    }

    .center_image_s21 {
        justify-content: center !important;
    }

    .center_image_s21 h1, .center_image_s21 h2, .center_image_s21 h3, .center_image_s21 h4, .center_image_s21 h5 {
        color: #000;
    }

    @media screen and (min-width: 768px) {
        .half_s21 {
            width: 150px;
        }
    }

    @media screen and (max-width: 768px) {
        .center_image_s21 {
            flex-wrap: initial !important;
            flex-direction: column !important;
        }

        .half_s21 {
            width: 135px;
            flex: none;
        }
    }

    .center_image_s21 .po-img {
        width: 320px;
        margin: 0 50px;
    }

    .half_s21 h1 {
        font-size: 37px;
    }

    .half_s21 h2 {
        font-family: 'Samsung Sharp Sans Bold';
    }

</style>
<script>
    var banner = document.querySelector('.samsung-banner'),
        leftSpace = banner.offsetLeft;

    banner.style.marginLeft = '-' + leftSpace + 'px';
    banner.style.width = 'calc(100% + ' + leftSpace * 2 + 'px)';


    animationEL = document.querySelectorAll('.entry-animation');
    for (var i = 0; i < animationEL.length; i++) {
        animationEL[i].classList.add('active');
    }

    var phoneColors = document.querySelectorAll('.chose-colors .phone-color');

    for (var i = 0; i < phoneColors.length; i++) {
        phoneColors[i].onclick = function () {
            var thisPhoneColor = this.getAttribute('data-color'),
                thisClosestItem = this.closest('.ss-prod'),
                changableItems = thisClosestItem.querySelectorAll('.changable-s'),
                clickColorParent = this.parentElement.children;

            for (var k = 0; k < clickColorParent.length; k++) {
                clickColorParent[k].classList.remove('active');
                this.classList.add('active');
            }


            for (var j = 0; j < changableItems.length; j++) {
                changableItems[j].classList.remove('active');
            }
            document.querySelectorAll('.' + thisPhoneColor)[0].classList.add('active');
            document.querySelectorAll('.' + thisPhoneColor)[1].classList.add('active');
            document.querySelectorAll('.' + thisPhoneColor)[2].classList.add('active');

        }
    }

    var compareBtn = document.querySelector('.compare-phone button'),
        sModal = document.querySelector('.samsung-modal'),
        sOverlay = document.querySelector('.samsungoverlay'),
        closeSmodal = document.querySelector('.close-sam-modal');


    closeSmodal.onclick = function () {
        sOverlay.classList.remove('active');
        sModal.classList.remove('active');
        setTimeout(function () {
            sOverlay.style.display = 'none';
            sModal.style.display = 'none';
        }, 500);
    }

    compareBtn.onclick = function () {
        sOverlay.style.display = 'block';
        sModal.style.display = 'block';
        window.scrollTo(0, 150);
        setTimeout(function () {
            sOverlay.classList.add('active');
            sModal.classList.add('active');
        });
    }
</script>