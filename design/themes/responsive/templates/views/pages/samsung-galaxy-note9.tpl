{literal}
    <style>
        .bigmedium {
            width: 100% !important;
        }

        .bf-bottom {
            display: none;
        }
        .bf-cover, #bf-vid {
            height: 600px !important;
            background-image: linear-gradient(187deg, #000000, #3856a8) !important;
            background-size: 400% 400%;
            -webkit-animation: AnimationName 6s ease infinite;
            -moz-animation: AnimationName 6s ease infinite;
            -o-animation: AnimationName 6s ease infinite;
            animation: AnimationName 6s ease infinite !important;
            padding-bottom: 130px;
        }
        .bigmedium .product-inner {
            box-shadow: 0 15px 20px -15px rgba(0,0,0,0.15), 0 35px 50px -25px rgba(0,0,0,0.15), 0 85px 60px -25px rgba(0,0,0,0.05);
            margin-bottom: 50px;
            padding: 0 !important;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .bigmedium .image-wrapper {
            height: auto !important;
            padding-top: 71% !important;
            background: url(https://hhstsyoejx.gjirafa.net/gj50/landings/samsung-galaxy-note9/galaxy-s9-product.jpg) center center / cover no-repeat !important;
        }

        .bigmedium .image-wrapper img{
            display: none;
        }


        .bigmedium .bigmedium-productdetails {
            text-align: left;
            margin: 0;
            padding: 20px 25px;
        }

        .bigmedium .bigmedium-productdetails .title {
            height: 20px;
            text-align: left;
            color: #2f73d2;
            font-weight: 500;
        }

        .bigmedium .bigmedium-productdetails p {
            line-height: 1.5;
            font-size: 15px;
        }

        .bigmedium .nprice {
            text-align: left;
            font-weight: bold;
            margin-bottom: 19px;
            margin-top: 10px;
        }

        .catvideo {
            position: relative;
            padding-top: 56.66% !important;
        }

        .catvideo iframe {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            height: 100%;
            width: 100%;
        }

        .ty-btn.ty-btn__primary {
            background: #2f73d2;
            transition: background .3s ease;
        }

        .ty-btn.ty-btn__primary:hover, .ty-btn.ty-btn__primary:focus {
            background: #09408e;
        }

        .product.small .product-inner {
            border-top: 3px solid #fff;
            transition: border .3s ease;
        }

        .product.small .product-inner:hover, .product.small .product-inner:focus {
            border-color: #2f73d2;
        }

        #products {
            margin-top: -155px;
        }

        @-webkit-keyframes AnimationName {
            0%{background-position:0% 78%}
            50%{background-position:100% 23%}
            100%{background-position:0% 78%}
        }
        @-moz-keyframes AnimationName {
            0%{background-position:0% 78%}
            50%{background-position:100% 23%}
            100%{background-position:0% 78%}
        }
        @-o-keyframes AnimationName {
            0%{background-position:0% 78%}
            50%{background-position:100% 23%}
            100%{background-position:0% 78%}
        }
        @keyframes AnimationName {
            0%{background-position:0% 78%}
            50%{background-position:100% 23%}
            100%{background-position:0% 78%}
        }

        .gn-promote {
            width: 100%;
        }

        .gn-promote .container-fluid {
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .gn-promote-image img {
            width: 400px;
        }

        .gn-promote-text {
            color: #fff;
            text-align: left;
        }

        .gn-promote-text * {
            color: #fff;
            font-weight: 100;
        }

        .gn-promote-text b {
            font-weight: 600;
        }

        .gn-promote-text h1 {
            margin: 30px 0;
        }


        .gn-promote-text .price {
            font-size: 30px;
        }

        .oldprice {
            position: relative;
        }
        .oldprice:after {
            content: "";
            position: absolute;
            width: 100%;
            height: 4px;
            background-color: yellow;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            transform: rotate(-10deg);
            transform-origin: 120px 0;;
        }

        .gn-promote-text img {
            max-width: 150px;
            margin: 10px 0;
        }


        @media screen and (max-width: 768px) {
            .product.small {
                width: 49% !important;
            }

            .bf-cover {
                padding-top: 20px;
                height: auto !important;
            }

            .gn-promote .container-fluid, .bigmedium .product-inner {
                display: block;
            }

            .gn-promote-text {
                padding: 0 20px;
                margin: 10px;
            }

            .gn-promote-image img {
                width: 200px;
            }

            .gn-promote-text * {
                margin: 0;
                text-align: center;
                font-size: 17px;
            }

            .gn-promote-text .price {
                font-size: 30px;
                margin: 15px 0;
            }

            .gn-promote-text .price b {
                font-size: 30px;
            }

            .gn-promote-text .price small b{
                font-size: 22px;
            }

            .oldprice:after{
                transform-origin: 80px 0;
            }

            .bigmedium .bigmedium-productdetails p {
                display: none;

            }

            .bigmedium-productdetails .price-wrapper {
                margin: 10px 0;
            }

            .bigmedium .ty-btn.ty-btn__primary {
                width: 100%;
            }


        }
    </style>
{/literal}
{capture name="testtt"}
    {if $boosted_products != null}
        <div class="bproducts">
            <h1 class="ty-center text-light">Ja vlen me ja l'shu ni sy!</h1>
            <div class="bproduct-wrapper">
                {foreach $boosted_products as $product}
                    <div class="bproduct">
                        <div class="bproduct-inner">
                            <div class="img" style="background-image:url({get_images_from_blob product_id=$product.product_id count=1})"></div>
                            <div class="content">
                                <h4>{$product.product|truncate:70:"...":true}</h4>
                                <h1>{$product.price}</h1>
                            </div>
                        </div>
                    </div>
                {/foreach}
            </div>
        </div>
    {/if}
{/capture}
<link rel="stylesheet" href="images/black-friday17/bf22.css?v={__("cache_landing_version")}"/>
<style>.bf-cover {ldelim}background-image: url(https://hhstsyoejx.gjirafa.net/gj50/landings/{$key}/cover{if $is_mobile && $key == "raiffeisen"}-mob{/if}.jpg);{rdelim}
    .product.small .save-percentage, .product.medium .save-percentage {ldelim}background:#{$landing_config.percentage_bg_color};{rdelim}
    .product.small .save-percentage span, .product.medium .save-percentage span {ldelim}color:#{$landing_config.percentage_color};{rdelim}
    .product.small .bnow, .product.medium .bnow {ldelim}background:#{$landing_config.buynow_color};{rdelim}
    .bf-cover, .bf-bottom {ldelim}background-color: #{$landing_config.bg_color};{rdelim}
    #bf-countdown {ldelim}background-color: #{$landing_config.bg_color};{rdelim}
    #bf-countdown span {ldelim}color: #{$landing_config.header_color};{rdelim}
    .bnow, .product  {ldelim}color: #{$landing_config.btn_color};{rdelim}
    .save-percentage strong {ldelim}color: #{$landing_config.percentage_color};{rdelim}
    .bf-cover, #bf-vid{ldelim}background-size:cover;{rdelim}
    .bf-cover{ldelim}display: flex;flex-direction: column;justify-content: center;align-items: center;text-align: center;cursor:normal;{rdelim}
    {literal}
    .bproducts{padding:10% 0;float:left;width:100%}.bproduct{width:33.33336%;float:left;position:relative;box-sizing:border-box;padding-right:10px;padding-bottom:10px}.bproduct-inner{-webkit-box-shadow:0 4px 9px 0 #ccc;box-shadow:0 4px 9px 0 #ccc;background:#fff;padding-left:150px;position:relative;height:130px}.bproduct-inner .content{padding:10px}.bproduct-inner h4{font-weight:300}.bproduct-inner h1{letter-spacing:-.5px}.bproduct .img{position:absolute;left:5px;top:5%;width:150px;height:85%;text-align:center;background-size:contain;background-position:center center;background-repeat:no-repeat}@media screen and (max-width:768px){.bproduct-wrapper{overflow-y:hidden;overflow-x:scroll;white-space:nowrap;transform:translateZ(0);will-change:transform;-webkit-overflow-scrolling:touch}.bproduct{display:inline-block;float:none;width:80%}.bproduct-inner .content{white-space:normal}.bproduct-inner h4{font-weight:300;height:70px;overflow:hidden;-ms-text-overflow:ellipsis;-o-text-overflow:ellipsis;text-overflow:ellipsis;display:-webkit-box;-webkit-box-orient:vertical;-webkit-line-clamp:3;-moz-box-orient:vertical;-moz-line-clamp:3;margin-bottom:10px}}
    {/literal}
    {if $key == "raiffeisen"}
    {literal}
    @media screen and (max-width: 768px){
        .bf-cover {
            height: 350px;
        }}
    {/literal}
    {/if}
</style>

<div class="bf-cover clearfix">
    {*{if !$is_mobile}*}
    {*<video oncontextmenu="return false" id="bf-vid" class="span16" style="margin:0;" autoplay loop poster="https://hhstsyoejx.gjirafa.net/gj50/landings/{$key}/cover{if $is_mobile}-mob{/if}.jpg">*}
    {*<source src="https://hhstsyoejx.gjirafa.net/gj50/landings/{$key}/cover.webm" type="video/webm"/>*}
    {*</video>*}
    {*{/if}*}

    <div class="bf gn-promote">
        <div class="container-fluid">
            <div class="span8 gn-promote-image">
                <img src="https://hhstsyoejx.gjirafa.net/gj50/landings/samsung-galaxy-note9/samsung-note-9.png" alt="">
            </div>

            <div class="span8 text-left gn-promote-text">
                <h2>
                    <b>Samsung</b>
                    <br>
                    GALAXY NOTE9
                </h2>
                <br>
                <h2>
                    E pamundura bëhet e mundur me Note 9!
                    <br>
                    <b>Oferta më e mirë në vend.</b>
                </h2>
                <h1 class="price">
                    <b>899.50€</b>
                    <small class="oldprice"><b>1156.50€</b></small>
                </h1>
                <h4>
                    * Kohë dhe sasi e limituar!
                    <br>
                    <img src="https://gjirafa50.com/images/logos/1/gjirafa50.png">
                </h4>
            </div>
        </div>

    </div>


    <h1 style="margin-top: -50px;letter-spacing: -1.5px;font-weight:bolder;font-size:35px;color:#{$landing_config.buynow_color};">{$landing_config.cover_txt1 nofilter}</h1>
    <h3 style="font-weight:400;">{$landing_config.cover_txt2 nofilter}</h3>
</div>
{if $key == 'raiffeisen'}
{literal}
    <script>document.querySelector('.bf-cover').onclick = function(){ window.open('https://www.raiffeisen-kosovo.com/cards/apply/apply-bonus-card.php?utm_source=Gjirafa50&utm_medium=RBKO%20Landing&utm_campaign=Zbritje%20me%20bonus%20kartele', '_blank'); }</script>
{/literal}
{/if}
<div class="bf-bottom">
    {if $landing_config.heading_1 != ""}
        <h3>{$landing_config.heading_1}</h3>
    {/if}
    {if $landing_config.heading_2 != ""}
        <h5 class="text-light text-error">{$landing_config.heading_2}</h5>
    {/if}
    {if $hide_price == true}
        <div id="bf-countdown" style="margin-top:0;" class="text-center"></div>
    {/if}
</div>

<div class="container-fluid" id="products">
    <div class="row">
        <div class="product bigmedium bbox"><div class="product-inner clearfix">
                <div class="span6">
                    <a class="bf-link" href="/gjirafa5038/index.php?dispatch=products.view&product_id=87763">
                        <div class="image-wrapper">
                            <img thumb="https://image.samsung.com/al/smartphones/galaxy-note9/performance/images/galaxy-note9_performance_biometric.jpg"
                                 src="https://image.samsung.com/al/smartphones/galaxy-note9/performance/images/galaxy-note9_performance_biometric.jpg">
                        </div>
                    </a>
                </div>

                <div class="span11 bigmedium-productdetails">
                    <a class="bf-link" href="/index.php?dispatch=products.view&product_id=87763">
                        <h1 class="title" style="height: 20px;">Samsung Galaxy Note9 - 128GB, 6GB RAM, i zi</h1>
                    </a>

                    <p>
                        Samsung e prezanton një prej celularëve më të fuqishëm në treg - Samsung Galaxy Note9. I shoqëruar me lapsin digjital S Pen ju mund ta lidhni celularin me të nëpërmjet teknologjisë Bluetooth dhe të punoni nga një distancë deri në 10 metra. Lapsi mund të shkruaj shënime në ekran, të përkthej tekst nga gjuhë të huaja dhe t'ju bëjë komunikimin më origjinal dhe kreativ në mes të celularit dhe lapsit. Celulari arrinë deri në 4096 nivele të presionit në mënyrë që të shfaq me precizitet të lartë se çfarë keni shënuar apo vizatuar.
                    </p>

                    <div class="price-wrapper" style="width:100%;">
                        <h2 class="nprice text-bold ty-center">
                            <b>899.50€</b>
                            <small class="oldprice"><b>1156.50€</b></small>
                        </h2>
                    </div>
                    <a href="/index.php?dispatch=products.view&amp;product_id=87763" class="ty-btn ty-btn__primary bbox bnow">
                        BLEJ TANI
                    </a>
                </div>

                {*<div class="sold-out-bf bbox"><img src="images/black-friday17/so.png"></div>*}
            </div>
            <span id="end_date" style="display: none"></span>
        </div>

        {assign "iteration" 0}
        {assign "cat" 0}
        {foreach $csv_products as $product}{if $elastic_products[$product.product_code] == null}{continue}{/if}

        <div class="product {if $iteration > -1 }small{else}medium{/if} bbox"><div class="product-inner clearfix">

                <a class="bf-link" href="/gjirafa5038/index.php?dispatch=products.view&product_id={$elastic_products[$product.product_code].product_id}">
                    <h3 class="title" {if $iteration < 4}style="height: 20px;"{/if}>{$elastic_products[$product.product_code].product}</h3>
                    {if $elastic_products[$product.product_code].amount > 0}
                        <img class="stockimage" src="https://gjirafa50.com/images/icons/upto48.svg"/>
                    {/if}

                    <div class="image-wrapper">
                        <img thumb="{get_images_from_blob product_id=$elastic_products[$product.product_code].product_id count=1}"/>
                    </div>
                    {if !$hide_price}
                        {assign var="sameprice" value=false}
                        {if $elastic_products[$product.product_code].old_price != null}
                            {$elastic_products[$product.product_code].price = $elastic_products[$product.product_code].old_price}
                        {/if}
                        {if $product.new_price - $elastic_products[$product.product_code].price == 0}
                            {$sameprice = true}
                        {/if}

                        <div class="price-wrapper" {if $product.new_price == '0.00'}style="width:100%;"{/if}>
                            {if !$sameprice && $product.new_price != '0.00'}
                                <h2 class="oprice text-muted text-light text-linethrough">€{$elastic_products[$product.product_code].price}</h2>
                                <h2 class="nprice text-bold">€{$product.new_price}</h2>
                            {elseif $product.new_price != '0.00'}
                                <h2 class="nprice text-bold ty-center">€{$product.new_price}</h2>
                            {else}
                                <h2 class="nprice text-bold ty-center">€{$elastic_products[$product.product_code].price}</h2>
                            {/if}
                        </div>
                        {if !$sameprice && $combo[$product.product_code] == null  && $product.new_price != '0.00'}
                            {math assign="savex" equation='(x-y)' x=$elastic_products[$product.product_code].price y=$product.new_price}
                            <h2 class="save-percentage"><strong>JU KURSENI</strong> <span>€{$savex}</span></h2>
                        {elseif $combo[$product.product_code] != null}
                            <h2 class="save-percentage" style="font-size: 17px;"><strong>FALAS!</strong> <span>{$combo[$product.product_code]}</span></h2>
                        {/if}
                    {/if}
                </a>
                {if !$hide_price}
                    <a href="/index.php?dispatch=products.view&product_id={$elastic_products[$product.product_code].product_id}" class="ty-btn ty-btn__primary bbox bnow">
                        {if $product.out_of_stock}
                            E SHITUR
                        {else}
                            BLEJ TANI
                        {/if}
                    </a>
                {/if}

                {assign var="stock" value="`$elastic_products[$product.product_code].amount+$elastic_products[$product.product_code].czc_stock+$elastic_products[$product.product_code].czc_retailer_stock`"}
                {if $stock == "0"}
                    <div class="sold-out-bf bbox"><img src="images/black-friday17/so.png"/></div>
                {/if}
            </div>
            <span id="end_date" style="display: none">{$landing_config.hide_prices_until}</span>
            </div>
            {$iteration = $iteration + 1}{if $iteration % 4 == 0 && $cat < 1}
            <div class="span16 catimg catvideo">
                <iframe src="https://www.youtube-nocookie.com/embed/rjaL94yEJME?autoplay=0&showinfo=0&controls=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
            {$cat = $cat + 1}{/if}{/foreach}
    </div>
</div>
{**}
<div id="scrollbarmobile">
    <span></span>
</div>
<input type="hidden" id="landing_id" value="{$key}">
<script src="js/lib/lazyload/lazyload.js"></script>

<script>
    {if $hide_price}

    var end_date = $('#end_date').text();
            {literal}var end=new Date(end_date);var _second=1000;var _minute=_second*60;var _hour=_minute*60;var _day=_hour*24;var timer;function showRemaining(){var now=new Date();var distance=end-now;if(distance<0){clearInterval(timer);document.getElementById('bf-countdown').innerHTML='';return}var days=Math.floor(distance/_day);var hours=Math.floor((distance%_day)/_hour);var minutes=Math.floor((distance%_hour)/_minute);var seconds=Math.floor((distance%_minute)/_second);document.getElementById('bf-countdown').innerHTML='<span class="startat">Fillon edhe</span> '+days+'<span> ditë</span> ';document.getElementById('bf-countdown').innerHTML+=hours+'<span> orë</span> ';document.getElementById('bf-countdown').innerHTML+=minutes+'<span> min</span> ';document.getElementById('bf-countdown').innerHTML+=seconds+'<span> sek</span>'}
    timer=setInterval(showRemaining,1000);{/literal}
    {/if}
    {literal}
    $(window).scroll(function(e) {
        var $this = $(this),
            docHeight = $(document).height() - $(window).height();
        $('#scrollbarmobile span').css('height', ($this.scrollTop() * 100) / docHeight + '%');

    });
    //document.getElementById('bf-vid').addEventListener('contextmenu', event => event.preventDefault());
    {/literal}
</script>