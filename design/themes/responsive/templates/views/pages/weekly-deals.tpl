<main>
    <div class="wd-banner">
        <img src="images/OfertaJavore2.jpg" alt="">
    </div>
    <div class="weekly-deals">
        {include file="views/pages/weekly-deals/slider.tpl"}
    </div>
    <div id="ajax_overlay" class="ty-ajax-overlay" style="display: none;"></div>
    <div id="ajax_loading_box" class="ty-ajax-loading-box" style="display: none;"></div>
</main>
<script>
    document.title = "Weekly Deals - Gjirafa50";

    var json_categories = '{$mapped_categories}'.replace(/&quot;/g, '\"');
    var json_brands = '{$mapped_brands}'.replace(/&quot;/g, '\"');

    mapped_categories = JSON.parse(json_categories);
    mapped_brands = JSON.parse(json_brands);

    {literal}

    var current_url = window.location.href;
    var current_url = new URL(current_url);
    var filter = current_url.searchParams.get('filter');
    var value = current_url.searchParams.get('value');

    if (filter == 'brand') {
        slider(0, 1, 0, getKeyByValue(mapped_brands, value));
        bulletWrapper.children[getKeyByValue(mapped_brands, value) - 1].classList.add('active');
    } else if (filter == 'category') {
        slider(0, 1, 0, getKeyByValue(mapped_categories, value));
        bulletWrapper.children[getKeyByValue(mapped_categories, value) - 1].classList.add('active');
    }

    $('body').on('click', '.filter', function() {
        var filterType = $(this).attr('data-type');
        var currentType = $(this).attr('data-current');
        var sliderIndex = $(this).attr('data-index');

        current_url.searchParams.set('filter', filterType);

        if (filterType == 'category') {
            current_url.searchParams.set('value', mapped_categories[sliderIndex - 1]);
        } else if (filterType == 'brand') {
            current_url.searchParams.set('value', mapped_brands[sliderIndex - 1]);
        }

        window.history.pushState('', '', current_url);

        if (filterType == currentType) {
            return false;
        }

        $('#ajax_overlay').show();
        $('#ajax_loading_box').show();

        url = fn_url('pages.weekly-deals');

        $.ajax({
            url: url,
            data: {is_ajax: 1, filter: filterType},
            success: function (res) {
                $('#ajax_overlay').hide();
                $('#ajax_loading_box').hide();
                $('.weekly-deals').html(res.text);
                slider(0, 1, 0, sliderIndex);
                bulletWrapper.children[sliderIndex - 1].classList.add('active');
            }
        });

        return false;
    });

    {/literal}

    function getKeyByValue(object, value) {
        return parseInt(Object.keys(object).find(key => object[key] === value)) + 1;
    }
</script>