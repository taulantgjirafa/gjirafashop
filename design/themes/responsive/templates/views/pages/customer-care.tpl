{assign var="is_al" value=$al|fn_isAL}
{if $is_al}
    {assign var="company_domain" value="Gjirafa50.al"}
{else}
    {assign var="company_domain" value="Gjirafa50.com"}
{/if}


<div id="boxes" class="customer-care-boxes content-grid">

    <h1 class="ty-mainbox-title">Kujdesi ndaj klientëve</h1>
    <h3 style="font-size: 15px; font-weight: 400; ">Faleminderit që keni zgjedhur Gjirafa50 për të bërë
        blerjet tuaja për pajisje teknologjike. Ne jemi të lumtur që keni zgjedhur dyqanin tonë online dhe jemi
        vazhdimisht
        në gatishmëri që tu ndihmojmë që blerja juaj në platformën tonë tu lërë përshtypje sa më të mirë.</h3>
    <p style="
    color: #e65228;
    font-weight: 500;
    font-size: 18px;"> Për tu njoftuar më shumë me {$company_domain}, ju lutem lexoni informatat në vijim.</p>
    <div class="questions">
        <button class="accordion" id="50">
                    <span class="icon">
                        <img src="/images/customer-care/50.png" alt="">
                    </span>
            Për Gjirafa 50
        </button>
        <div class="panel">
            <p>
                <span>Çka është {$company_domain}?</span>
                <br>
                Gjirafa50 është platformë e shitjes online, e krijuar nga Gjirafa, Inc. Është dyqan online i
                specializuar në shitjen e pajisjeve më të fundit të teknologjisë dhe që njëkohësisht janë më të
                kërkuarat në treg.
            </p>

            <p>
                <span>Pse {$company_domain}?</span>
                <br>
                Në fillimet e saj Gjirafa50, kishte vetëm nga 50 produkte të cilat ishin më të rejat dhe njëkohësisht më
                të kërkuarat të fushës së teknologjisë. Tani, Gjirafa50.com është dyqani më i madh online në rajon, që
                ofron mbi 80,000 produkte të tilla.
            <p>
                <span>Ku gjindet {$company_domain}?</span>
                <br>
                {$company_domain} është dyqan online dhe si i tillë nuk posedon dyqan fizik.
                <br>
                {if $is_al}
                    Mirëpo për korrektësi ndaj klientëve, është mundësia për të i marrë produktet në zyret e Gjirafa50.
                {else}
                    Mirëpo për korrektësi ndaj klientëve posedon Showroom-in e vet që konsumatorët të kenë mundësi që t'i shohin dhe t'i provojnë produktet edhe në mënyrë fizike. Ju lutem gjeni lokacionin tonë përmes linkut të mëposhtëm:
                    <br>
                    <a href="https://gjirafa.biz/gjirafa50-1"><b>Gjirafa50</b></a>
                {/if}
            </p>

            <p>
                <span>Si mund të kontaktoj me {$company_domain}?</span>
                <br>
                Që të jemi sa më afër jush, ne i kemi bërë disa mënyra për të kontaktuar. Prandaj, çdo ditë nga ora
                08:00 deri në ora 00:00 pas mesanatës, përveç të dielave nga ora 9:00 deri në ora 00:00, mund të na
                gjeni në:
                <br>
                <br>
                - Në Live Chat në faqen tonë
                <br>
                - Në telefon (si dhe Viber dhe
                Whatsapp): {"number"|fn_get_contact_info}
                <br>
                - Me email: {"email"|fn_get_contact_info}
                <br>
                - Në Facebook: facebook.com/Gjirafa50
                <br>
                - Në Instagram me userin: Gjirafa50
                <br>
                - Në Twitter me userin: Gjirafa50
                <br>
                - Në Snapchat me userin: Gjirafa50


            </p>


        </div>
        <button class="accordion" id="flex">
                 <span class="icon">
                        <img src="/images/icons/flex.png" alt="" style="max-width: 75%;">
                    </span>
            GjirafaFlex
        </button>
        <div class="panel">
            <p>
                <span>Cka është GjirafaFLEX? </span>
                <br>
                GjirafaFLEX është shërbim shtesë që ne e ofrojmë si zgjidhje të shpejtë nëse produkti ka defekt nga
                fabrika (nuk vlen për dëmtime fizike), brenda periudhës 1 vjeçare të garancionit. Ky shërbim nuk është i
                obligueshëm, por përparesia e tij është që problemi zgjidhet për 5-9 ditë, ndërsa nëse nuk paguani
                GjirafaFlex, ju bie të prisni deri në 30 ditë për servisim të produktit (sipas procesit standard).
                <br>
                <span style="padding: 0;color: #777;font-weight: normal;font-style: italic;" class="text-muted">
                    *Defektet përfshijnë vetëm ato fabrike dhe jo dëmet fizike
                    që mund të i shkaktohen produktit.
                </span>
            </p>
            <p>
                <span>A mund ta marr GjirafaFlex pas përfundimit të blerjes?</span>
                <br>
                GjirafaFLEX mund të zgjedhet vetëm në hapin e parë gjatë procesit të blerjes duke klikuar në Po ose Jo.
            </p>
            <p>
                <span>A vlen GjirafaFlex edhe për produktin e ri që e marr pas kthimit të produktit me defekt?</span>
                <br>
                GjirafaFLEX vlen vetëm për një produkt specifik dhe mbaron me zgjidhjen e problemit për atë produkt.
                Caktimi i GjirafaFLEX për produktin e ri është opsional.
            </p>
            <p>
                <span>A mund të rimbursohem për vlerën e GjirafaFlex nëse gjatë vitit të garancionit nuk kam pasur nevojë për të?</span>
                <br>
                Jo, nëse GjirafaFlex nuk përdoret gjatë periudhës një vjeçare të garancionit, nuk mund të rimburosohet.
            </p>
        </div>
        <button class="accordion" id="pagesat">
                 <span class="icon">
                        <img src="/images/customer-care/payment.png" alt="">
                    </span>
            Pagesat
        </button>
        <div class="panel">
            <p>
                <span>Si mund të realizohen blerje në këtë faqe?</span>
                <br>
                Procesi i blerjeve të pajisjeve teknologjike është shumë i thjeshtë.
                Pasi ta keni hapur faqen me përmbajtjen e specifikave të produktit, ju duhet të klikoni në hapësirën
                "Shto në shportë" dhe të vazhdoni me procesin e blerjes nëpërmjet butonit “Bëje porosinë”.
            </p>

            <p>
                <span>Cilat janë mundësitë e pagesave? </span>
                <br>
                Për tu ofruar lehtësi në pagesat tuaja, ofrohen disa mënyra të pagesave për produktet e dëshiruara.
                {if !is_al}
                    <br>
                    - Përmes kredit kartelave të bankës Raiffeisen dhe bankës TEB, mund të bëni blerje deri në 12 këste pa interes;
                {/if}
                <br>
                - Përmes kartelave bankare (nga secila bankë)
                <br>
                {*                - Përmes PayPal*}
                {*                <br>*}
                - Transfer bankar
                <br>
                {if !$is_al}
                    - Me këste përmes Raiffeisen Bonus Kartelë dhe TEB Starcard.
                    <br>
                {/if}
                - Duke paguar me para në dorë kur bëhet pranimi i produktit.
                {if $is_al}
                    <br>
                    - Përmes POS kur bëhet pranimi i produktit, duke lejuar edhe pagesat me këste (së shpejti)

                {/if}

            </p>

            {*            <p>*}
            {*                <span>Çka është PayPal? </span>*}
            {*                <br>*}
            {*                PAYPAL është kompani që veprimtarinë e saj e kryen në Internet dhe si shërbim kryesor ka kryerjen e*}
            {*                pagesave dhe transaksioneve monetare online duke bërë transferimin e sigurtë të pagesave nga një*}
            {*                individ drejt një biznesi të caktuar të shitjeve online*}
            {*            </p>*}

            <p>
                <span>Me cilat kartela mund ta bëhen pagesat?</span>
                <br>
                Pagesat realizohen me të gjitha llojet e kartelave bankare (kredit apo debit kartela), të të gjitha
                bankave; si {if $is_al}Shqipëtare{else}Kosovare{/if} poashtu edhe ndërkombëtare.
            </p>

            <p>
                <span>A mundësohen pagesat me këste? </span>
                {if $is_al}
                    <br>
                    Pagesat me këste do të mundësohen së shpejti, përmes POS (Point of sale) terminaleve në momentin kur e pranoni produktin.
                {else}
                    <br>
                    Pagesat me këste mund të bëhen vetëm nëpërmjet Bankës Raiffeisen dhe Bankës TEB me kredit kartelat e këtyre bankave (jo debit), ku ju mund ta bleni produktin që dëshironi deri në 12 këste pa interes.
                {/if}
            </p>

            <p>
                <span>Si bëhet pagesa me këste?</span>
                {if $is_al}
                    <br>
                    Pagesat me këste do të mundësohen së shpejti, përmes POS (Point of sale) terminaleve në momentin kur e pranoni produktin.
                {else}
                    <br>
                    Kur zgjedhni paguaj me këste, sistemi online nuk do t'ju kërkoj të dhënat e kartelës suaj, por në momentin që mbërrin porosia, postieri e ka me vete aparatin e kartelës (POS), dhe aty për aty e bëni pagesën, si dhe e ndani me këste. Pagesat me këste mund të paguhen në maksimum deri në 12 muaj, me përjashtim të celularëve që duhet të paguhen deri në 6 muaj.
                {/if}
            </p>

            <p>
                <span>A bën më lirë? </span>
                <br>
                Çmimet e produkteve nuk janë të negociueshme.
                Mirëpo për të përfituar nga zbritjet që ofrohen, përveç vizitave në uebfaqen tonë, ju lutem bëni
                regjistrohuni si konsumatorë me anë të emailit ku më pas do tu vijnë të gjitha njoftimet e zbritjeve
                dhe të arritjes së produkteve të reja të teknologjisë, poashtu edhe mund të na ndiqni në Facebook,
                Instagram, Twitter apo Snapchat.

            </p>

            <p>
                <span>A ofrohet zbritje nëse blihet më shumë se një produkt? </span>
                <br>
                Nëse ju bleni më shumë se një produkt çmimet nuk ndryshojnë. Mirëpo kohë pas kohe Gjirafa50 ka
                oferta të zbritjeve për të gjithë konsumatorët.
                Gjithashtu për klientët besnik Gjirafa50 kujdeset që tu bëjë dhurata në vazhdimësi.

            </p>

            <p>
                <span>A mund të bëhen blerje me kartelën e dikujt tjetër? </span>
                <br>
                Këto blerje tolerohen vetëm atëherë kur personi me kartelën i/e së cilit/cilës bëhet blerja është në
                dijeni dhe ka dhënë autorizimin.
                Poashtu, kur plotësoni të dhënat gjatë blerjes së produktit, keni dy adresa: atë të faturimit (me
                informata si të kredit kartelës), si dhe adresën ku do të bëhet dërgesa.
            </p>
            <p>
                <span>A përfshihen dogana dhe taksat në çmim? </span>
                <br>
                Dogana dhe taksat janë të përfshira në çmim. Që nënkupton që shuma për të cilën paguani ështe çmimi
                final për secilin produkt.
            </p>

            {if !$is_al}
                <p>
                    <span>A kushton më lirë nëse e blej produktin në showroomin tuaj?  </span>
                    <br>
                    Çmimi i produktit nuk ndryshon në këtë rast. Por, është një mundësi e mirë për ju që kur të vini për
                    ta marrë produktin, njëherit t’i shihni disa nga produktet tjera në Showroom-in tonë për të cilat
                    keni interesim.
                </p>
            {/if}

            <p>
                <span>A mund të më kthehen paratë nëse çmimi i produktit që kam porositur është zbritur?</span>
                <br>
                Paratë nuk mund të kthehen, pasi që zbritjet vlejnë vetëm për periudha të caktuara.
            </p>

            <p>
                <span>Me çfarë valuta mund të bëj pagesën?</span>
                <br>
                Pagesat me para në dorë pranohen vetëm me euro, ndërsa pagesat përmes bankës me valuta tjera nuk
                përbëjnë ndonjë problem.
            </p>

            <p>
                <span>Pagesa më është refuzuar, çfarë duhet të bëj?</span>
                <br>
                Ju lusim që të kontaktoni me kujdesin ndaj klientit në mënyrë që të shqyrtohet rasti së bashku me stafin
                teknik, përkatësisht, nëse problemi është te platforma e Gjirafës apo e bankës përkatëse.
            </p>

            <p>
                <span>Si mund të përfitoj ndonjë kod promocional?</span>
                <br>
                Një nga mënyrat për të përfituar kod promocional është duke e vlerësuar ndonjë produkt që veçse e keni
                blerë më herët. Nëse është blerja e parë, atëherë mund të na kontaktoni në LiveChat në mënyrë që t'ju
                ofrojmë ndonjë zbritje apo dhuratë simbolike.
            </p>

            <p>
                <span>A mund të përdor më shumë se një kod promocional për një porosi?</span>
                <br>
                Jo, vetëm një kod mund të aplikohet për porosi.
            </p>

            <p>
                <span>A mund të aplikohet më shumë se një zbritje për një porosi?</span>
                <br>
                Jo, nuk mund të aplikohet më shumë se një zbritje për porosi.
            </p>

            <p>
                <span>Dua të më montoni një kompjuter, a duhet të bëj pagesë shtesë?</span>
                <br>
                Jo, montimi i kompjuterëve bëhet pa pagesë shtesë.
            </p>

            <p>
                <span>A mund të bëj pagesën e produkteve gjysmë para në dorë dhe gjysmë me këste gjatë pranimit të porosisë?</span>
                <br>
                Po, mjafton që kërkesa juaj për pagesë me këste dhe para në dorë të jetë e shënuar në komentin e
                porosisë suaj.
            </p>

            <p>
                <span>Sa duhet të paguaj në muaj për një produkt të caktuar nëse dua ta bëj pagesën me këste?</span>
                <br>
                Kësti mujor i çdo produkti është i shënuar në faqe poshtë çmimit të përgjithshëm të tij. Në përgjithësi,
                vlera mujore përcaktohet në bazë të numrit të kësteve.
            </p>
        </div>

        <button class="accordion" id="porosia">
                 <span class="icon">
                        <img src="/images/customer-care/product.png" alt="">
                    </span>
            Porosia
        </button>
        <div class="panel">
            <p>
                <span>Si mund ta bëj porosinë?</span>
                <br>
                Porosinë mund ta bëni vetëm online direkt në faqe. Në linqet më poshtë mund të shihni hap pas hapi se si
                bëhet porosia online: https://video.gjirafa.com/si-te-porosisim-permes-telefonit-ne-gjirafa50 dhe
                https://video.gjirafa.com/si-te-porosisim-permes-kompjuterit-ne-gjirafa50
            </p>

            <p>
                <span>A duhet të regjistrohem për të bërë porosi?</span>
                <br>
                Po, duhet të regjistroheni si përdorues, qoftë me facebook apo email. Nëse dëshironi të regjistroheni në
                Gjirafa, atëherë mund të ndiqni hapat e më poshtëm:
                <br>
                - Së pari duhet të klikoni në butonin “Kyçu”, i cili gjendet në shiritin e sipërm të faqes në anën e
                djathtë.
                <br>
                - Pastaj, do t’ju hapet një dritare ku në pjesën e poshtme gjendet një buton “Regjistrohu” në të cilin
                duhet të klikoni.
                <br>
                - Pasi ta shtypni atë tast, do të hapet faqja ku duhet t’i shënoni të dhënat tuaja personale, të klikoni
                në butonin “Unë pranoj kushtet e përdorimit” dhe në fund të klikoni në butonin "Regjistrohu".
                <br>
                - Pas regjistrimit, do te riktheheni në mënyrë automatike në faqen fillestare prej së cilës tani mund të
                bëni porosi me llogarinë e krijuar dhe më të dhënat qe ju i keni shënuar.
            </p>

            <p>
                <span>Si ta shfrytëzoj kodin promocional?</span>
                <br>
                Gjatë procesit të blerjes, te hapi i parë "Të dhënat e porosisë" është hapësira "Gift Card dhe Kodi
                Promocional" në të cilën mund ta shkruani kodin që posedoni dhe në fund të klikoni në butonin "Apliko",
                në mënyrë që diferenca të largohet nga totali i porosisë.
            </p>

            <p>
                <span>A mund të bëj porosi përmes telefonit, rrjeteve sociale apo live chat?</span>
                <br>
                Porositë duhet të bëhen direkt në faqen tonë nga vet përdoruesi. Ndërsa, porositë mund të bëhen edhe në
                platforma tjera vetëm në rastet kur ka kampanja të caktuara aktive.
            </p>

            <p>
                <span>A mund të blej në emër të kompanisë?</span>
                <br>
                Patjeter, ju lëshojmë dhe faturë të rregullt për porosinë që bëni, mjafton të shënoni të dhënat e
                biznesit te të dhënat e porosisë (emrin ligjor të biznesit dhe numrin fiskal), pas porosisë do t'ju
                dërgojmë profaturën në email, ndërsa faturën e rregullt e pranoni bashkë me porosinë tuaj.
            </p>

            <p>
                <span>Ku bëhen porositë dhe ku gjendet porosia ime në faqe?</span>
                <br>
                Porositë bëhen duke klikuar "Shto në shportë" në faqen e produktit. Porositë tuaja mund t'i shihni kur
                të keni klikuar mbi llogarinë tuaj në të djathtë "Emri Mbiemri" dhe pastaj te hapësira "Porositë".
            </p>

            <p>
                <span>Kur anulohen porositë nga Gjirafa50.com?</span>
                <br>
                - Nëse për çfarëdo arsye porosia e bërë është e dyshimtë, atëherë bëhet anulimi i porosisë dhe dërgohet
                një email informues lidhur mbi anulimin e porosisë.
                <br>
                - Nëse ju kemi thirrur në telefon për të konfirmuar porosinë e bërë dhe nuk jeni lajmëruar, atëherë ajo
                porosi do të anulohet, ku edhe do të njoftoheni përmes emailit.
                <br>
                - Nëse adresa e shënuar nuk përmban të dhënat e sakta për vendndodhjen tuaj dhe nëse edhe pas
                kontaktimit nuk ofroni informatat e nevojshme shtesë, atëherë ajo porosi do të anulohet.
                <br>
                - Nëse dy herë është tentuar që t'u sillet porosia në adresën tuaj dhe nuk keni qenë në shtëpi/zyre për
                ta pranuar atë, atëherë ajo porosi do të anulohet.
            </p>

            <p>
                <span>Çka është Blacklist?</span>
                <br>
                Blacklist është listë e përdoruesve që duhet të parapaguajnë për çdo porosi që bëjnë, për shkak se kanë
                bërë anulim të vonshëm apo kanë refuzuar një porosi të mëherëshme. Në këto raste, përdoruesi kalon në
                mënyrë automatike në këtë listë.
            </p>
            <p>
                <span>Si informohem që porosia ime është procesuar?</span>
                <br>
                Nëpër secilin hap që kalon porosia, përdoruesi pranon e-mail për statusin aktual.
            </p>

            <p>
                <span>A mund ta kthej porosinë?</span>
                <br>
                Po, porosia mund të kthehet në afat prej 14 ditësh nga dita kur është blerë produkti, vetëm në gjendjen
                që është pranuar me paketim, kupon fiskal dhe fletëgarancion.
            </p>

            <p>
                <span>A mund ta ndryshoj adresën pasi që është procesuar/nisur porosia?</span>
                <br>
                Po adresa mund të ndryshohet, por koha e transportit do të rritet për të paktën 24 orë.
            </p>

            <p>
                <span>Nëse dua, si mund ta anuloj porosinë?</span>
                <br>
                Duke na kontaktuar në cilindo kanal të komunikimit.
            </p>

            <p>
                <span>Kam anuluar porosinë, si mund ta shfrytëzoj kredinë?</span>
                <br>
                Duke bërë porosi të re për çfarëdo produkti që dëshironi brenda platformës përkatëse dhe duke shkruar në
                koment të porosisë që po shfrytëzoni kredinë ekzistuese në Gjirafa.
            </p>

            <p>
                <span>Kam anuluar porosinë, kur më kthehen paratë (nëse pagesa është bërë online)? </span>
                <br>
                Kompletimi i transakcionit bëhet brenda 5 ditëve, pasi të jetë bërë kërkesa për kthim të mjeteve nga ana
                jonë.
            </p>

            <p>
                <span>A mund ta dërgoj porosinë si dhuratë?</span>
                <br>
                Po, mund të bëni pagesë online dhe tek adresa e transportit të vendosni adresën ku duhet tē dërgohet
                porosia.
            </p>

            <p>
                <span>Produkti që dua të blej është në zbritje, a mund të më aplikohet edhe një tjetër?</span>
                <br>
                Jo, për produktet që veçse janë në zbritje nuk mund të aplikohet zbritje shtesë.
            </p>

            <p>
                <span>A mund të shtoj apo të largoj produkte, pasi që porosia është procesuar?</span>
                <br>
                Produktet mund të shtohen në porosi dhe si rrjedhojë mund të rritet edhe koha e ardhjes së produkteve.
                Ndërsa, ato nuk mund të largohen veçanërisht pasi është bërë rezervimi i tyre, por për më shumë
                kontaktoni kujdesin ndaj klientit në cilindo kanal të komunikimit.
            </p>
        </div>

        <button class="accordion" id="produktet">
                   <span class="icon">
                        <img src="/images/customer-care/product.png" alt="">
                    </span>
            Produktet
        </button>
        <div class="panel">
            <p>
                <span>A janë të gjitha produktet origjinale?</span>
                <br>
                Ne shesim vetëm produkte origjinale. Të gjitha vijnë të mbyllura në paketimet e tyre të prodhuesit. Për
                më shumë informata rreth secilit produkt, mund të kërkoni me numrin e tij serial nëpër faqet zyrtare të
                brendeve.

            </p>

            <p>
                <span>Çka janë produktet Outlet?</span>
                <br>
                Kategoria Outlet përfshinë produkte të reja që vijnë me paketime të hapura dhe produkte të përdorura (me
                apo pa dëmtime fizike që nuk pengojnë operimin), gjë e cila ceket në përshkrimet e secilit produkt që i
                përket kësaj kategorie. Po ashtu, produktet e kësaj kategorie kanë vetëm 3 muaj garancion.
            </p>

            <p>
                <span>A kanë produktet garancion?</span>
                <br>
                Produktet tona posedojnë garancion ndërkombëtar, që do të thotë se në të gjitha pikat e servisimit që
                janë të autorizuara nga brendet përkatëse mund të bëhet servisimi pa pagesë. Për listën e këtyre pikave
                të autorizuara ju lutem kontaktoni Kujdesin ndaj Klientit. Nëse nuk ka pika të tilla të autorizuara për
                brende të caktuara, mund ta sillni produktin te zyret tona dhe brenda 30 ditëve ua kthejmë atë të
                rregulluar, u ofrojmë ndërrimin e produktit apo u japim kredi në Gjirafa50.com.
            </p>

            <p>
                <span>Si mund ta gjej një produkt?</span>
                <br>
                Duke vendosur në hapësirën për kërkim kodin e produktit, emrin e tij, ose përmes filtrimit të
                specifikave për produktin që ju duhet.
            </p>

            <p>
                <span>Produkti që dua nuk është në stok, çfarë duhet të bëj?</span>
                <br>
                Mund të klikoni te butoni "Më njofto kur ky produkt kthehet në stok" dhe do të lajmëroheni përmes
                email-it.
            </p>
        </div>

        <button class="accordion" id="transport">
                  <span class="icon">
                        <img src="/images/customer-care/transport.png" alt="">
                    </span>Transporti
        </button>
        <div class="panel">
            <p>
                <span>A është transporti me pagesë?</span>
                <br>
                {if $is_al}
                Transporti ēshtë 200 lekë dhe mund të dërgohet kudo në Shqipëri.
                {else}
                Në Gjirafa50, transporti ēshtë falas kudo në Kosovë.
                {/if}
            </p>

            <p>
                <span>Nëse bëj porosi në Gjirafa50, për sa arrijnë produktet?</span>
                <br>
                Gjirafa50 ofron dy mënyra transporti:
                <br>
                1. Mënyra Standard, që do të thotë se porosia e bërë arrin për 5 deri në 8 ditë OSE për 8 deri në 14
                ditë në raste të vonesave të mundshme.
                <br>
                2. Mënyra GjirafaSWIFT, që do të thotë se porosia mbërrin brenda periudhës kohore prej 24 deri në 48 orë
                pune.
                <br>
                Por, ju si klient e keni po ashtu mundësinë që të vini direkt në zyret tona për ta marr porosinë. Nga
                momenti që ne ju lajmërojmë me email se dërgesa juaj është gati, ju mund të vini ta merrni kurdo që ju
                konvenon juve (por brenda një limiti kohor).
                <br>
                {if !$is_al}
                    Për t'u informuar më shumë për kodet postare për të gjitha qytetet e Kosovës ju lutemi klikoni më poshtë:
                    <br>
                    <a href="https://postakosoves.com/kodet-postare/">- Prishtina dhe rrethi i Prishtinës</a>
                    <a href="https://postakosoves.com/kodet-postare/">- Prizreni dhe rrethi i Prizrenit</a>
                    <a href="https://postakosoves.com/kodet-postare/">- Peja dhe rrethi i Pejës</a>
                    <a href="https://postakosoves.com/kodet-postare/">- Mitrovica dhe rrethi i Mitrovicës</a>
                    <a href="https://postakosoves.com/kodet-postare/">- Gjakova dhe rrethi i Gjakovës</a>
                    <a href="https://postakosoves.com/kodet-postare/">- Gjilani dhe rrethi i Gjilanit</a>
                    <a href="https://postakosoves.com/kodet-postare/">- Ferizaji dhe rrethi i Ferizajit</a>
                {/if}
            </p>

            <p>
                <span>Çka ndodh nëse nuk gjendeni në shtëpi kur t'u vijë porosia?</span>
                <br>
                Para se të niset postieri për t'ua sjell porosinë, ne ju kontaktojmë përmes telefonit që të sigurohemi
                që jeni në shtëpi. Megjithatë, nëse nuk gjindeni në shtëpi ne do t'u kontaktojmë edhe ditën e nesërme.
                Nëse mendoni që nuk mund të jeni në shtëpi kur ne ju informojmë që produkti juaj do të arrijë, atëherë
                keni mundësi të vini te zyret tona për ta marr produktin.
            </p>

            <p>
                <span>A mund të bëhet transporti jashtë {if $is_al}Shqipërisë{else}Kosovës{/if}?</span>
                <br>
                Jo, transporti vlen vetëm brenda territorit të {if $is_al}Shqipërisë{else}Republikës së Kosovës{/if}.
            </p>

            <p>
                <span>A mund ta caktoj kohën dhe ditën e saktë se kur mund të mbërrij porosia?</span>
                <br>
                Po, paraprakisht mund të këshilloheni me Kujdesin ndaj Klientit apo ta shkruani kohën e përshtashme në
                hapësirën e komentit gjatë bërjes së porosisë.
            </p>

            <p>
                <span>A mund ta gjurmoj porosinë time?</span>
                <br>
                Për çdo hap të porosisë do të njoftoheni përmes email-it, ndërsa për të parë statusin aktual të porosisë
                suaj, mund të klikoni në profilin juaj te opsioni "porositë".
            </p>

            <p>
                <span>Si llogaritet koha e transportit?</span>
                <br>
                Porosia hyn në proces vetëm pas konfirmimit nga stafi ynë dhe pas kësaj fillon llogaritja në bazë të
                orëve dhe ditëve të punës.
            </p>

            <p>
                <span>A mund të bëj sot porosi dhe të ma sjellni menjëherë?</span>
                <br>
                Në shumicën e rasteve jo, por duhet të konsultoheni me kujdesin ndaj klientit.
            </p>

            <p>
                <span>Çfarë duhet të bëj nëse nuk kam arritur t'i përgjigjem postës për pranim të porosisë?</span>
                <br>
                Posta ju kontakton më shumë se një herë, por për t'u siguruar kontaktoni kujdesin ndaj klientit.
            </p>

            <p>
                <span>Deri në ora sa punon transporti?</span>
                <br>
                Transporti bëhet deri në ora 18:00.
            </p>

            <p>
                <span>A bëni dërgim të porosive ditës së diel?</span>
                <br>
                Ditën e diel nuk bëjmë dërgim të porosive.
            </p>

            <p>
                <span>Kur mund të vonohet porosia? </span>
                <br>
                Disa nga arsyet se përse mund të vonohet një porosi janë:
                <br>
                - Adresa nuk është e saktë
                <br>
                - Nuk është shënuar adresa e plotë
                <br>
                - Procedura ndërkombëtare doganore
            </p>

        </div>

        <button class="accordion" id="teknike">
                   <span class="icon">
                        <img src="/images/customer-care/tech.png" alt="">
                    </span>
            Çështje teknike
        </button>
        <div class="panel">
            <p>
                <span>Si të kyçem në {$company_domain}?</span>
                <br>
                Nëse keni llogari në Gjirafa.com apo në ndonjërën prej platformave të Gjirafa, atëherë ajo llogari vlen
                edhe për Gjirafa50.com.
                Nëse jeni i ri në platformën tonë dhe dëshironi që të regjistroheni në Gjirafa50.com, atëherë pasi të
                futeni në faqen zyrtare www.gjirafa50.com, klikoni mbi butonin “Kyçu” që gjendet në pjesën e sipërme të
                faqes në anën e djathtë, ku do t'u shfaqet menyja me opsionet e regjistrimit. Përveç kësaj mënyre, ju
                mund të kyçeni edhe nëpërmjet llogarisë suaj në Facebook.
            </p>

            <p>
                <span>Si të kyçem nëse e kam harruar fjalëkalimin? </span>
                <br>
                Derisa jeni duke u kyçur me llogarinë tuaj dhe në rast se e keni harruar fjalëkalimin, poshtë të
                dhënave tuaja mund të gjeni opsionin që ju ofron mundësinë e ripërtrirjes së fjalëkalimit tuaj.
            </p>

            <p>
                <span>A mund të ndryshohen të dhënat e llogarisë? </span>
                <br>
                Në rast se dëshironi të bëni ndryshimin e të dhënave të llogarisë tuaj, pasi të jeni kyçur, klikoni
                në butonin “Të dhënat e profilit” dhe në këtë mënyrë do tu shfaqet hapësira ku mund të bëni
                ndryshimet e të dhënave që ju dëshironi.
            </p>

            <p>
                <span>Si të veproni nëse dëshironi ta mbyllni llogarinë? </span>
                <br>
                Nëse keni vendosur që ta mbyllni llogarinë tuaj, ju lutem kontaktoni në adresën: <a
                        href="mailto:{"email"|fn_get_contact_info}">{"email"|fn_get_contact_info}</a> dhe më pas
                stafi do të përkujdeset për mbylljen e llogarisë tuaj.
                Pas procesit të mbylljes do ta pranoni një email që konfirmon mbylljen e llogarisë tuaj.
            </p>

            <p>
                <span>Nuk dua të pranoj email-e nga ju, si duhet të veproj?</span>
                <br>
                Në cilindo email që keni pranuar nga ne, e keni mundësinë të çabonoheni nga lajmërimet apo ofertat e
                dërguara, vetëm duke klikuar në "UNSUBSCRIBE".
            </p>
        </div>

        <button class="accordion" id="kthime">
                 <span class="icon">
                        <img src="/images/customer-care/50.png" alt="">
                    </span>
            Kthime
        </button>
        <div class="panel">
            <p>
                <span>Si ta kthej një porosi?</span>
                <br>
                Porosia duhet të kthehet në zyret tona në afat prej 14 ditësh nga dita kur është blerë produkti, së
                bashku me paketim, kupon fiskal dhe fletëgarancion.
            </p>

            <p>
                <span>Produkti i blerë nuk punon si duhet, çfarë duhet të bëj?</span>
                <br>
                Në raste të tilla, njoftoni kujdesin ndaj klientit menjëherë.
            </p>

            <p>
                <span>A mund ta kthej produktin, për të marr një tjetër?</span>
                <br>
                Si blerës, keni të drejtë që ta ndërroni produktin me një tjetër (nëse produkti që keni porositur nuk ka
                formë, madhësi, model ose ngjyrë të njëjtë sikurse në faqe apo për arsye tjera) në afat prej 14 ditësh
                nga dita kur është blerë produkti, me kusht që produkti nuk është përdorur, është ruajtur pamja, vetitë
                përdoruese, vulat, etiketat e fabrikës, paketimi si dhe fatura fiskale. Afati i reklamacionit për
                aksesorë të celularëve edhe tabletëve si mbështjellëset, foliet, etj. është 24 orë.
            </p>

            <p>
                <span>A mund ta kthej produktin dhe të më ktheni pagesën e plotë?</span>
                <br>
                Vendimi për kthim vendoset nga stafi teknik në bazë të gjendjes së produktit. Në rast se po, mund të
                kthehet vetëm nëse e keni paketimin, kuponin fiskal dhe fletëgarancionin, nëse produkti është akoma nën
                garancion dhe nuk e ka kaluar periudhën e lejuar kohore për kthim. Ndërsa, pagesa mund të kthehet vetëm
                brenda 14 ditëve pas pranimit të porosisë, në të kundërten mjetet mbesin si kredi për blerjen e radhës.
            </p>

            <p>
                <span>A mund të kthej produkte të cilat i kam blerë me zbritje / gift voucher?</span>
                <br>
                Vendimi për kthim vendoset nga stafi teknik në bazë të gjendjes së produktit. Në rast se po, mund të
                kthehet vetëm nëse e keni paketimin, kuponin fiskal dhe fletëgarancionin, nëse produkti është akoma nën
                garancion dhe nuk e ka kaluar periudhën e lejuar kohore për kthim.
            </p>

            <p>
                <span>A mund të kthej produkte të cilat më janë dhënë si dhuratë?</span>
                <br>
                Jo, produktet që janë dhënë si dhuratë nuk mund të kthehen.
            </p>

            <p>
                <span>Kam pranuar produktin e gabuar, çfarë duhet të bëj?</span>
                <br>
                Në raste të tilla, njoftoni kujdesin ndaj klientit menjëherë.
            </p>

            <p>
                <span>Produkti që kam pranuar nuk është i njejtë me përshkrimin në faqe, çfarë duhet të bëj?</span>
                <br>
                Në raste të tilla, njoftoni kujdesin ndaj klientit menjëherë.
            </p>

            <p>
                <span>Në porosinë e pranuar, mungon një produkt, çfarë duhet të bëj?</span>
                <br>
                Në raste të tilla, njoftoni kujdesin ndaj klientit menjëherë.
            </p>

            <p>
                <span>Produkti i pranuar nuk është në gjendje të rregullt, çfarë duhet të bëj?</span>
                <br>
                Në raste të tilla, njoftoni kujdesin ndaj klientit menjëherë.
            </p>

            <p>
                <span>Nëse dua të kthej një produkt, a duhet të paguaj transport?</span>
                <br>
                Nëse në atë porosi ju keni paguar edhe GjirafaFlex atëherë produktin vijmë e marrim ne, në të kundërtën,
                vet klienti duhet të sjell produktin në zyrën tonë duke përfshirë paketimin e produktit, kuponin fiskal
                dhe fletëgarancionin.
            </p>
        </div>

        <button class="accordion" id="servisime">
                 <span class="icon">
                        <img src="/images/customer-care/50.png" alt="">
                    </span>
            Servisime
        </button>
        <div class="panel">
            <p>
                <span>Si shkon procesi i servisimit të produkteve?</span>
                <br>
                Pasi produkti të arrijë tek ne, stafi teknik bën testimin e produktit dhe i'u njofton për procesin e
                mëtutjeshëm. Gjatë dorëzimit të produktit për servisim ju duhet të sillni edhe fletëgarancionin e
                plotësuar së bashku me të dhënat e produktit, datën e shitjes, faturën origjinale ose atë fiskale.
                Garancioni i produktit nuk vlen në mungesë të këtyre dokumenteve përcjellëse.
            </p>

            <p>
                <span>Sa kohë zgjatë servisimi i produkteve?</span>
                <br>
                Varësisht nga problemi i shfaqur në produkt, në rastet e zakonshme afati i servisimit është deri në 30
                ditë, ndërsa në rastet në të cilat është përfshirë edhe GjirafaFlex, afati i servisimi të produkteve
                zgjat deri në 8 ditë.
            </p>

            {*            <p>*}
            {*                <span>Produkti i servisuar, ende ka probleme në operim, çfarë duhet të bëj?</span>*}
            {*                <br>*}
            {*                Kontaktoni stafin teknik në numrin kontaktues 044 228 188 ose kujdesin ndaj klientit për sugjerime.*}
            {*            </p>*}

            <p>
                <span>Garancioni i produktit ka përfunduar, a mund të servisohet ende?</span>
                <br>
                Në këtë rast jo, sepse garancioni është direkt nga prodhuesi.
            </p>

            <p>
                <span>Produkti është hapur nga serviserë të paautorizuar, a mund të servisohet nga ju? </span>
                <br>
                Jo, në momentin që produkti është hapur nga serviserë të paautorizuar, garancioni i produktit shpallet i
                pavlefshëm.
            </p>
        </div>

        <button class="accordion" id="garancioni">
                 <span class="icon">
                        <img src="/images/customer-care/50.png" alt="">
                    </span>
            Garancioni
        </button>
        <div class="panel">
            <p>
                <span>Cilat produkte nuk i mbulon garancioni vjetor?</span>
                <br>
                Në asnjë rast pjesë e garancionit nuk janë çantat, mbështjellësit (covers & cases), kabllot, filterët e
                ndryshëm, telekomandat, adapterët, bateritë dhe pjesët e ngjashme, si dhe produktet e konsumueshme.
            </p>

            <p>
                <span>Çfarë dëmtime mbulon garancioni?</span>
                <br>
                Garancioni i produktit vlen vetëm për defektet që janë si rezultat i prodhuesit ndërsa produkti është
                përdorur nga ju në përputhje me instruksionet e përdorimit dhe udhëzimeve teknike, duke përfshirë
                udhëzimet për montim dhe përdorimit për qëllime adekuate.
            </p>

            <p>
                <span>Çfarë dëmtime nuk i mbulon garancioni?</span>
                <br>
                Produkti ka shenja të dëmtimit fizik, kozmetik, të jashtëm ose të brendshëm që nuk janë rezultat i
                dëmeve të shkaktuara nga prodhuesi.
                <br>
                Dëmtimet në produkt janë shkaktuar si rezultat i mbitensionit ose nëntensionit të energjisë elektrike,
                në variacionin plus/minus 15% të kufirit të përcaktuar të 230v.
                <br>
                Produkti është dëmtuar si rezultat i ndodhive natyrore përtej kontrollit njerëzor, përfshirë, por pa u
                kufizuar në rrufe, vetëtimë, tërmet, vërshime, ujë, materiale të lëngshme, zjarr.
                <br>
                Dëmtimet mekanike të shkaktuara nga rrëzimi, goditja dhe thyerja e produktit.
                <br>
                Dëmtimi i produktit ka ndodhur si rezultat i hapësirës në të cilën e keni vendosur produktin (hapësirat
                e papërshtatshme), përfshirë vendet e ndryshme me prezencë të lagështisë, insekteve, papastërtisë,
                ndryshkut, etj.
                <br>
                Nuk e keni mirëmbajtur produktin në mënyrën e duhur, ashtu siç kërkohet për përdorimin e rregullt të
                tij.
                <br>
                Produkti është dëmtuar për shkak të ekspozimit ndaj temperaturave të papërshtatshme për produktin.
                <br>
                Produkti është hapur fizikisht nga ju ose nga një palë e tretë e pa autorizuar për të hapur produktin
                dhe ndërhyrë në pjesët e brendshme të tij.
                <br>
                Produktin e keni kyçur ose keni krijuar lidhje me çfarëdo pajisje të papërshtatshme ose jo origjinale
                ose pajisje e cila nuk është kompatibile me atë produkt.
                <br>
                Defekti është shkaktuar si rezultat i ndërhyrjes suaj në sistemin e produktit ose instalimet e sistemeve
                të papërshtatshme.
                <br>
                Ekranet televizive ose monitorët kanë qëndruar për një kohë shumë të gjatë të ndezur, e si rezultat në
                të to janë shfaqur njolla të cilat nuk mund të riparohen.
                <br>
                Në asnjë rast pjesë e ketij garancioni nuk janë kabllot, filterët e ndryshëm, telekomandat, adapterët,
                bateritë dhe pjesët e ngjashme.
            </p>
        </div>

        <button class="accordion" id="garancioni">
                 <span class="icon">
                        <img src="/images/customer-care/50.png" alt="">
                    </span>
            Pyetje të shpeshta
        </button>
        <div class="panel">
            <p>
                <span>Çfarë orari të punës keni?</span>
                <br>
                Për çdo pranim apo kthim të porosisë, si dhe kërkesë për servisim mund të na gjeni në Showroom nga e
                hëna në të shtunën në këtë orar 9:00 - 18:00. Ndërsa për çfarëdo të dhëne, pyetje apo kërkese, ju mund
                ta kontaktoni Kujdesin ndaj klientëve në cilindo kanal të komunikimit çdo ditë të javës në oraret si më
                poshtë: 08:00-01:00 (Hëne-Premte); 09:00-01:00 (Shtunë); 11:00-23:00 (Dielë).
            </p>

            <p>
                <span>Si mund të bëjë porosi?</span>
                <br>
                Porosinë mund ta bëni vetëm online direkt në faqe. Në linqet më poshtë mund të shihni hap pas hapi se si
                bëhet porosia online: https://video.gjirafa.com/si-te-porosisim-permes-telefonit-ne-gjirafa50 dhe
                https://video.gjirafa.com/si-te-porosisim-permes-kompjuterit-ne-gjirafa50. Po ashtu, porosinë mund ta
                bëni edhe pa u regjistruar, duke klikuar brenda produktit në "Blej tani", ku vetëm duhet t'i shënoni të
                dhënat tuaja, adresën dhe numrin e telefonit.
            </p>

            <p>
                <span>Kur vjen porosia?</span>
                <br>
                Për ta parë saktë datën e arritjes mund të klikoni te porositë e mia ose të shikoni emailin të cilin e
                keni pranuar gjatë bërjes së porosisë.
            </p>

            <p>
                <span>Cilat janë mundësitë e pagesave?</span>
                <br>
                Për t'u ofruar lehtësi në blerje, ofrohen disa mënyra të pagesave për produktet e dëshiruara:
                - Përmes kartelave bankare (nga secila bankë, 3D secure)
                - Transfer bankar
                - Me këste përmes Raiffeisen me anë të Bonus kartelës dhe TEB Starcard (me aparat POS).
                - Duke paguar me para në dorë kur bëhet pranimi i produktit."
            </p>

            <p>
                <span>A mund ta shikojë produktin pastaj ta paguaj me kartelë?</span>
                <br>
                Na vjen keq, por shumicën e produkteve e pranojmë direkt nga prodhuesit me paketim të mbyllur, vetëm
                disa produkte nga brendi MSI, Samsung, BenQ, Steelseries dhe PrimeComputer mund t'i shihni e provoni në
                Showroom-in tonë. Por, si blerës, keni të drejtë që ta ndërroni produktin me një tjetër (nëse produkti
                që keni porositur nuk ka formë, madhësi, model ose ngjyrë të njëjtë sikurse në faqe apo për arsye tjera)
                në afat prej 48 orësh nga dita kur është blerë produkti, me kusht që produkti nuk është përdorur, është
                ruajtur pamja, vetitë përdoruese, vulat, etiketat e fabrikës, paketimi si dhe fatura fiskale.
            </p>

            <p>
                <span>Si bëhet anulimi i porosisë?</span>
                <br>
                Porosinë mund ta anuloni duke na kontaktuar në cilindo kanal të komunikimit.
            </p>

            <p>
                <span>A ka zbritje? A ka zbritje nëse marr me shumicë?</span>
                <br>
                Për zbritje eventuale mund të na shkruani në cilindo kanal të komunikimit dhe mund t'ju bëjmë një ofertë
                për sasinë e kërkuar.
            </p>

            <p>
                <span>A ka zbritje pasi që jam klient i rregullt?</span>
                <br>
                Po, mund ta përdorni kodin promocional "5yje" për të përfituar 5% ulje vetëm në produktet që nuk janë në
                zbritje momentalisht. Së pari duhet të vlerësoni produktin që keni blerë paraprakisht dhe pastaj të
                aktivizohet kodi.
            </p>

            <p>
                <span>Sa vonohet porosia dhe ku mund ta shikojmë?</span>
                <br>
                Për ta parë saktë datën e arritjes mund të klikoni te porositë e mia ose të shikoni emailin të cilin e
                keni pranuar gjatë bërjes së porosisë.
            </p>

            <p>
                <span>Sa kushton transporti?</span>
                <br>
                Në Gjirafa50, transporti ēshtë falas kudo në Kosovë.
            </p>

            <p>
                <span>A transportoni edhe jashtë vendit? Nëse po, ku?</span>
                <br>
                Transporti vlen vetëm brenda territorit të Republikës së Kosovës për Gjirafa50.com. Për Shqipëri e kemi
                Gjirafa50.al. Për më shumë informata, kontaktoni kujdesin ndaj klientëve në kanalet tona të komunikimit.
            </p>

            <p>
                <span>Nëse porosia caktohet të vijë në një ditë kur nuk jam në shtëpi, si ta menaxhojë këtë situatë?</span>
                <br>
                Mund të na kontaktoni paraprakisht dhe mund ta nisim atë në një ditë që ju përshtatet.
            </p>

            <p>
                <span>Si mund ta bëjë një dhuratë?</span>
                <br>
                Gjatë bërjes së porosisë te hapësira e adresës së transportit mund ta shënoni adresën e pranuesit dhe
                kështu porosia niset direkt te personi. Sa i përket pagesës mund ta bëni me kartelë online apo me
                transfer bankar dhe duhet ta shënoni në koment që është dhuratë. Po ashtu, mund të blini Gift Card, të
                caktoni vlerën e dëshiruar, të bëni pagesën dhe do t'ju jepet një kod i cili mund të perdoret nga
                personi që ja dhuroni.
            </p>

            <p>
                <span>Si bëhet kërkesa për ndërrim apo riparim të produktit?</span>
                <br>
                Kthimi apo ndërrimi i produktit mund të bëhet vetëm brenda periudhës prej 48 orë në gjendje të
                papërdorur, me paketim të padëmtuar, fletëgarancion dhe kupon fiskal. Kthimi i produktit mund të bëhet
                vetëm në zyret tona. Ndërsa, kërkesa për servisim mund të bëhet brenda periudhës një vjeçare (po aq sa
                lejon garancioni) në zyret tona. Kthimi, ndërrimi apo servisimi bëhet nga korrierët tanë vetëm në rastet
                kur keni GjirafaFlex.
            </p>

            <p>
                <span>Si të hapim një llogari në Gjirafa50?</span>
                <br>
                Nëse keni llogari në Gjirafa.com apo në ndonjërën prej platformave të Gjirafa, atëherë ajo llogari vlen
                edhe për Gjirafa50.com. Nëse jeni i ri në platformën tonë dhe dëshironi që të regjistroheni në
                Gjirafa50.com, atëherë pasi të futeni në faqen zyrtare www.gjirafa50.com, klikoni mbi butonin “Kyçu” që
                gjendet në pjesën e sipërme të faqes në anën e djathtë, ku do t'u shfaqet menyja me opsionet e
                regjistrimit. Përveç kësaj mënyre, ju mund të kyçeni edhe nëpërmjet llogarisë suaj në Facebook.
            </p>

            <p>
                <span>Porosia nuk ka arritur në datat e caktuara, si t'ja bëj?</span>
                <br>
                Ju lutem kontaktoni kujdesin ndaj klientëve për më shumë detaje rreth porosisë tuaj. Ju mund të na
                kontaktoni përmes Livechat, Facebook, Instagram, Viber, apo telefon.
            </p>

            <p>
                <span>Pagesat me këste për sa muaj duhet të paguhen?</span>
                <br>
                Pagesa mund të bëhet deri në 12 këste për shumicën e produkteve, përveç për celular që është deri në 6
                këste.
            </p>

            <p>
                <span>Nëse porosia vonohet, a mund të vij personalisht ta marr më shpejtë (më herët)?</span>
                <br>
                Data e arritjes së porosisë është e njëjtë si për transport "Standard" ashtu edhe për transport "Merre
                vet". Mënyrën e transportit mund ta ndërroni vetëm nëse porosia është ende në statusin "E rezervuar" apo
                "Verifikuar", pasi që kalon në statuse tjera çdo ndryshim për porosi merr kohë deri në 24 orë.
            </p>

            <p>
                <span>A kanë produktet garancion? Sa?</span>
                <br>
                Produktet tona posedojnë garancion ndërkombëtar, që do të thotë se në të gjitha pikat e servisimit që
                janë të autorizuara nga brendet përkatëse mund të bëhet servisimi pa pagesë. Për listën e këtyre pikave
                të autorizuara ju lutem kontaktoni Kujdesin ndaj Klientit. Nëse nuk ka pika të tilla të autorizuara për
                brende të caktuara, mund ta sillni produktin te zyret tona dhe brenda 30 ditëve ua kthejmë atë të
                rregulluar, u ofrojmë ndërrimin e produktit apo u japim kredi në Gjirafa50.com.
            </p>

            <p>
                <span>Përse nuk kam pranuar garancion në fletë, por vetëm në email?</span>
                <br>
                Garancionin e dërgojmë vetëm në email për shkak të evitimit të humbjes së kopjes fizike. Garancioni në
                email ju mundëson që ta keni aty sa herë që ju nevojitet për t'u informuar për kushtet e garancionit, si
                dhe mbetet si dëshmi që garancioni është dërguar nga ne dhe pranuar nga ju.
            </p>

            <p>
                <span>Përse nuk janë të paraqituara përbërësit e produkteve?</span>
                <br>
                Jemi ende në proces të shtimit të tyre, por nëse jeni të interesuar për ndonjë produkt mund të ju
                tregojmë këtu apo në cilindo kanal të komunikimit.
            </p>

            <p>
                <span>A mund ta shikojë produktin, pastaj ta blejë?</span>
                <br>
                Na vjen keq, por shumicën e produkteve e pranojmë direkt nga prodhuesit me paketim të mbyllur, vetëm
                disa produkte nga brendi MSI, Samsung, BenQ, Steelseries dhe PrimeComputer mund t'i shihni e provoni në
                Showroom-in tonë.
            </p>

            <p>
                <span>Kur niset produkti te adresa e caktuar, a lajmërohet pranuesi në telefon?</span>
                <br>
                Për çdo ndërrim të statusit të porosisë tuaj do të njoftoheni përmes e-mailit tuaj, por gjithashtu ju do
                të njoftoheni edhe përmes telefonit nga korrierët tanë në momentin që gjenden afër jush me porosinë
                tuaj.
            </p>

            <p>
                <span>Nëse i bëj dy apo më shumë porosi, a ka mundësi t'i sillni të gjitha në të njejtën kohë?</span>
                <br>
                Nëse keni bërë më shumë se një porosi në kohë të ndryshme, ato do të ju arrijnë në datat e tyre
                përkatëse që u janë shfaqur në momentin që keni bërë porositë.
            </p>

            <p>
                <span>Nëse i bëj dy apo më shumë porosi në të njejtën kohë, a duhet ta paguaj transportin vetëm një herë apo aq sa është numri i produkteve?</span>
                <br>
                Në Gjirafa50, transporti ēshtë falas për çfarëdo numri të porosive të bëra.
            </p>

            <p>
                <span>Si të aplikoni kupon apo Gift Card për zbritje?</span>
                <br>
                Patjetër! Gjatë bërjes së porosisë, pasi që klikoni në "Bëje porosinë", do të hapet faqja e shportës,
                aty në anën e djathtë keni një hapësirë "GiftCard dhe kodi promocional" në të cilën duhet ta shënoni atë
                kod. Nëse kodi është aktiv, vlera e tij do të aplikohet si zbritje menjëherë.
            </p>

            <p>
                <span>Si ta aplikoj kuponin për zbritje?</span>
                <br>
                Gjatë bërjes së porosisë, pasi që klikoni në "Bëje porosinë", do të hapet faqja e shportës, aty në anën
                e djathtë keni një hapësirë "GiftCard dhe kodi promocional" në të cilën duhet ta shënoni atë kod. Nëse
                kodi është aktiv, vlera e tij do të aplikohet si zbritje menjëherë.
            </p>
            <p>
                <span>Nëse bëj kthimin e një produkti, a më kthehet pagesa?</span>
                <br>
                Pagesa mund të kthehet vetëm brenda 14 ditëve pas pranimit të porosisë, në të kundërtën, ato mjete
                mbesin kredi dhe mund të përdoren në blerjet e radhës.
            </p>
        </div>

    </div>
</div>
{literal}
    <script>

        // Accordion
        function smoothScroll(div) {
            $('html, body').animate({
                scrollTop: $(div).offset().top - 70
            }, 500);
        }

        var selectedAcc = "";
        $('.accordion').on('click', function (e) {
            e.preventDefault();
            selectedAcc = $(this).attr('id')
            $(this).addClass('active').siblings().removeClass('active');
            $(this).next().addClass('show').siblings().removeClass('show');
            window.location.hash = selectedAcc;
            smoothScroll(this);
        });

        var hash = window.location.hash;
        if (hash) {
            $(hash).click();
        }
        window.onpopstate = function (event) {
            return;
        }
    </script>
{/literal}