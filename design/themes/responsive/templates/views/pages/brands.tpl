{if !$smarty.get.brand}
    {include file="views/pages/all_brands.tpl"}
{else}
    {if $isAjax}
        {if $products}
            {assign var="layouts" value=""|fn_get_products_views:false:0}
            {if $layouts.$selected_layout.template}
                {include file="`$layouts.$selected_layout.template`" columns=$settings.Appearance.columns_in_products_list show_qty=true no_sorting= true show_add_to_wishlist=true}
            {/if}
        {/if}
    {else}
        {assign var="brandName" value="{$smarty.get.brand}"}

        <style>
            @media screen and (max-width: 768px) {
                .tygh-content {
                    padding-top: 0px;
                }
            }
        </style>

        {if $brandName == "jbl"}
            <style>
                .tygh-content {
                    background: #ff582b;
                }

                .ty-brand-name {
                    display: none;
                }

                .ty-brand-wrapper {
                    display: block;
                }
                .load_more_category #load_more_infinite {
                    background: transparent;
                    border: 3px solid #fff;
                    color: #fff;
                }
            </style>
        {/if}

        {if $brandName == "razer"}
            <style>
                .tygh-content {
                    background: #000000;
                }

                .ty-brand-name {
                    display: none;
                }

                .ty-brand-wrapper {
                    display: block;
                }
                .load_more_category #load_more_infinite {
                    box-shadow: none;
                }
            </style>
        {/if}

        {if $brandName == "steelseries"}
            <style>
                .tygh-content {
                    background: #000000;
                }

                .ty-brand-name {
                    display: none;
                }

                .ty-brand-wrapper {
                    display: block;
                }
                .load_more_category #load_more_infinite {
                    background: transparent;
                    border: 3px solid #fff;
                    color: #fff;
                }
            </style>
        {/if}

        {if $brandName == "apple"}
            <style>
                .tygh-content {
                    background: #fafafa;
                }

                .ty-brand-name {
                    display: none;
                }

                .ty-brand-wrapper {
                    display: block;
                }
            </style>
        {/if}

        {if $brandName == "hp"}
            <style>
                .tygh-content {
                    background: #4887ce;
                }

                .ty-brand-name {
                    display: none;
                }

                .ty-brand-wrapper {
                    display: block;
                }
                .load_more_category #load_more_infinite {
                    background: transparent;
                    border: 3px solid #fff;
                    color: #fff;
                }
            </style>
        {/if}

        {if $brandName == "dell"}
            <style>
                .tygh-content {
                    background: #fff;
                }

                .ty-brand-name {
                    display: none;
                }

                .ty-brand-wrapper {
                    display: block;
                }
            </style>
        {/if}
        <div class="ty-brand-wrapper">

            <div class="ty-brand-bg" style="background-image: url('images/brands/{$smarty.get.brand}-bg.jpg')">
            </div>


        </div>
        <div class="ty-brand-name">
            <h3>{$brandName}</h3>
        </div>
        {if $products}
            {assign var="layouts" value=""|fn_get_products_views:false:0}
            {if $layouts.$selected_layout.template}
                {include file="`$layouts.$selected_layout.template`" columns=$settings.Appearance.columns_in_products_list show_qty=true no_sorting= true show_add_to_wishlist=true}
            {/if}
        {/if}

    {include file="common/load_more_products.tpl" button_visible=true button_text="SHFAQ MË SHUMË PRODUKTE NGA {$brandName|upper}"}

    {literal}
        <script>
            var contentDisatance = $('.ty-brand-wrapper').offset().left;

            $('.ty-brand-bg').css({
                'left': '-' + contentDisatance + 'px',
                'right': '-' + contentDisatance + 'px'

            })


        </script>
    {/literal}
    {/if}
{/if}
