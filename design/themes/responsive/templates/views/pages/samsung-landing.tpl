<style>
    .tygh-content {
        background: #000;
    }
    
    @font-face {
        font-family: "Samsung Sharp Sans Bold";
        src: url("//db.onlinewebfonts.com/t/03fe5644d1605049951f58ca7961c33f.eot");
        src: url("//db.onlinewebfonts.com/t/03fe5644d1605049951f58ca7961c33f.eot?#iefix") format("embedded-opentype"), url("//db.onlinewebfonts.com/t/03fe5644d1605049951f58ca7961c33f.woff2") format("woff2"), url("//db.onlinewebfonts.com/t/03fe5644d1605049951f58ca7961c33f.woff") format("woff"), url("//db.onlinewebfonts.com/t/03fe5644d1605049951f58ca7961c33f.ttf") format("truetype"), url("//db.onlinewebfonts.com/t/03fe5644d1605049951f58ca7961c33f.svg#Samsung Sharp Sans Bold") format("svg");
    }
    
    .text-muted {
        color: #777 !important;
    }
</style>
<span class="samsungoverlay"></span>
<div class="samsung-landing">
    <div class="samsung-modal">
        <span class="close-sam-modal"><svg><use xlink:href="#svg-back"></use></svg></span>
        <h1 style="text-align: center;">KRAHASONI MODELET</h1>
        <div class="s-modal-img flexing flex-h-between">
            <img src="https://images.samsung.com/al/smartphones/galaxy-s20/images/galaxy-s20_highlights_color_s20_pink.jpg" alt="">
            <img src="https://images.samsung.com/al/smartphones/galaxy-s20/images/galaxy-s20_highlights_color_s20-plus_gray.jpg" alt="">
            <img src="https://images.samsung.com/al/smartphones/galaxy-s20/images/galaxy-s20_highlights_color_s20-ultra_gray.jpg" alt="">
        </div>
        <div class="s-modal-content">
            <div class="modal-table-head flexing flex-h-between mdc-els">
                <div> </div>
                <div>Galaxy S20</div>
                <div>Galaxy S20+</div>
                <div>Galaxy S20 Ultra</div>
            </div>
            <div class="modal-table-content">
                <div class="flexing flex-h-between mdc-els">
                    <div>Ngjyra</div>
                    <ul>
                        <li>
                            <span class="phone-color" style="background:#5e6367"></span>
                            <span>Gri Kozmike</span>
                        </li>
                        <li>
                            <span class="phone-color" style="background:#9bc9e4;"></span>
                            <span>Blu e ëmbël</span>
                        </li>
                        <li>
                            <span class="phone-color" style="background:#e5a9ba;"></span>
                            <span>Rozë e ëmbël</span>
                        </li>
                    </ul>
                    <ul>
                        <li>
                            <span class="phone-color" style="background:#2e2926"></span>
                            <span>E zezë Kozmike</span>
                        </li>
                        <li>
                            <span class="phone-color" style="background:#9bc9e4;"></span>
                            <span>Blu e ëmbël</span>
                        </li>
                        <li>
                            <span class="phone-color" style="background:#5e6367;"></span>
                            <span>Gri Kozmike</span>
                        </li>

                    </ul>
                    <ul>
                        <li>
                            <span class="phone-color" style="background:#2e2926"></span>
                            <span>E zezë Kozmike</span>
                        </li>
                        <li>
                            <span class="phone-color" style="background:#9bc9e4;"></span>
                            <span>Gri Kozmike</span>
                        </li>
                    </ul>
                </div>
                <div class="flexing flex-h-between mdc-els">
                    <div>Madhësia e ekranit</div>
                    <div>
                        <h1>6.2 inç</h1>
                    </div>
                    <div>
                        <h1>6.7 inç</h1>
                    </div>
                    <div>
                        <h1>6.9 inç</h1>
                    </div>
                </div>
                <div class="flexing flex-h-between mdc-els">
                    <div>Rezolucion i lartë</div>
                    <div>
                        <h1>64MP</h1>
                    </div>
                    <div>
                        <h1>64MP</h1>
                    </div>
                    <div>
                        <h1>108MP</h1>
                    </div>
                </div>
                <div class="flexing flex-h-between mdc-els">
                    <div>Zmadhim</div>
                    <div>
                        <h1>30x</h1>
                    </div>
                    <div>
                        <h1>30x</h1>
                    </div>
                    <div>
                        <h1>100x</h1>
                    </div>
                </div>
                <div class="flexing flex-h-between mdc-els">
                    <div>Bateria</div>
                    <div>
                        <h1>4000mAh</h1>
                    </div>
                    <div>
                        <h1>4500mAh</h1>
                    </div>
                    <div>
                        <h1>5000mAh</h1>
                    </div>
                </div>
            </div>
            <div class="modal-table-desc">
                <ul>
                    <li>
                        1. Disponueshmëria e ngjyrës mund të ndryshojë në varësi të shtetit ose operatorit.
                    </li>
                    <li>
                        2. E matur në mënyrë diagonale, madhësia e ekranit të Galaxy S20 është 6.2 inç si katror i plotë dhe 6.1 inç duke përfshirë cepat e rrumbullakosur; madhësia e ekranit të Galaxy S20+ është 6.7 inç si katror i plotë dhe 6.5 inç duke përfshirë cepat e rrumbullakosur;
                        madhësia e ekranit të Galaxy S20 Ultra është 6.9 inç si katror i plotë dhe 6.7 inç duke përfshirë cepat e rrumbullakosur; zona reale e shikimit është me pak për shkak të cepave të rrumbullakosur dhe vrimës së kamerës.
                    </li>
                    <li>
                        3. Space Zoom përfshin një zmadhim dixhital, i cili mund të shkaktojë përkeqësim të imazhit.
                    </li>
                    <li>
                        4. Bateria 5000mAh (zakonisht) e disponueshme vetëm në Galaxy S20 Ultra. Bateria e Galaxy S20 është 4000mAh (zakonisht) dhe bateria e Galaxy S20+ është 4500mAh (zakonisht). Vlera e zakonshme e analizuar në kushte laboratorike të palës së tretë. Vlera
                        e zakonshme është vlera e parashikuar mesatare duke marrë parasysh devijimin në kapacitetin e baterive midis mostrave të baterive të analizuara sipas standardit IEC 61960. Kapaciteti nominal (minimal) është 3880 mAh për Galaxy
                        S20, 4370 mAh për Galaxy S20+ dhe 4855 mAh për Galaxy S20 Ultra. Kohëzgjatja reale e baterisë mund të ndryshojë në varësi të mjedisit të rrjetit, formës së përdorimit dhe faktorëve të tjerë.
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="samsung-banner">
        <div class="hidden-phoone" style="background-image:url('https://gjirafaadnetwork.blob.core.windows.net/html5/Samsung_1920x500.jpg');"></div>
        <div class="hidden-desktop" style="background-image:url('https://gjirafaadnetwork.blob.core.windows.net/html5/600x400.jpg');"></div>
    </div>
    <div class="samsung-top-p">

        <div class="ss-prod entry-animation" style="transition-delay: .1s;">
            <div>
                <div class="ssp-img">
                    <div class="active s20blue changable-s" id="s20blue" style="background-image:url('https://gjirafaadnetwork.blob.core.windows.net/html5/S20_blue.png')"></div>
                    <div class="s20gray changable-s" id="s20gray" style="background-image:url('https://gjirafaadnetwork.blob.core.windows.net/html5/S20_gray.png')"></div>
                    <div class="s20pink changable-s" id="s20pink" style="background-image:url('https://gjirafaadnetwork.blob.core.windows.net/html5/S20_pink.png')"></div>
                </div>
                <div class="ssp-content">
                    <div class="phone-title">
                        <h3 class="s20blue active changable-s">Samsung Galaxy S20 128 GB Blu e ëmbël</h3>
                        <h3 class="s20gray changable-s">Samsung Galaxy S20 128 GB Blu Hirtë Kozmike</h3>
                        <h3 class="s20pink changable-s">Samsung Galaxy S20 128 GB Rozë e ëmbël</h3>
                    </div>
                    <h3 class="text-muted">
                        Memorie e brendshme GB: <span style="color: #fff;">128</span> <br /> Lloji i ekranit:<span style="color: #fff;"> AMOLED</span>
                        <br /> Rezolucioni i kamerës së prapme Mpx:<span style="color: #fff;"> 64</span>

                    </h3>
                    <div class="chose-colors">
                        <span class="phone-color " data-color="s20gray" style="background:#5e6367"></span>
                        <span class="phone-color active " data-color="s20blue" style="background:#9bc9e4"></span>
                        <span class="phone-color " data-color="s20pink" style="background:#e5a9ba"></span>
                    </div>
                    <div class="ssp-btn-price">
                        <h2>979.50€</h4>
                            <div class="phone-links">
                                <a class="s20blue active changable-s" href="https://gjirafa50.com/samsung/celular-samsung-galaxy-s20-128-gb-e-kalter-e-embel-0/">Blej tani</a>
                                <a class="s20pink changable-s" href="https://gjirafa50.com/samsung/celular-samsung-galaxy-s20-128-gb-roze-e-embel-0/">Blej tani</a>
                                <a class="s20gray changable-s" href="https://gjirafa50.com/samsung/celular-samsung-galaxy-s20-128-gb-e-hirte-kozmike-0/">Blej tani</a>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="ss-prod entry-animation" style="transition-delay: .22s;">
            <div>
                <div class="ssp-img">
                    <div class="s20plusblack changable-s" id="s20plusblack" style="background-image:url('https://gjirafaadnetwork.blob.core.windows.net/html5/S20%2B_black.png')"></div>
                    <div class="active s20plusgray changable-s" id="s20plusgray" style="background-image:url('https://gjirafaadnetwork.blob.core.windows.net/html5/S20%2B_gray.png')"></div>
                    <div class="s20plusblue changable-s" id="s20plusblue" style="background-image:url('https://gjirafaadnetwork.blob.core.windows.net/html5/S20%2B_blue.png')"></div>
                </div>
                <div class="ssp-content">
                    <div class="phone-title">
                        <h3 class="s20plusblack active changable-s">Samsung Galaxy S20+ 128 GB Zezë Kozmike</h3>
                        <h3 class="s20plusgray changable-s">Samsung Galaxy S20+ 128 GB Hirtë Kozmike</h3>
                        <h3 class="s20plusblue changable-s">Samsung Galaxy S20+ 128 GB Blu e ëmbël</h3>
                    </div>

                    <h3 class="text-muted">
                        Memorie e brendshme GB: <span style="color: #fff;">128</span> <br /> Madhësia e ekranit inç:<span style="color: #fff;"> 6.7</span>
                        <br /> Galaxy Buds+:<span style="color: #fff;"> falas</span>

                    </h3>
                    <div class="chose-colors">
                        <span class="phone-color " data-color="s20plusblack" style="background:#000"></span>
                        <span class="phone-color active " data-color="s20plusgray" style="background:#5e6367"></span>
                        <span class="phone-color " data-color="s20plusblue" style="background:#9bc9e4"></span>
                    </div>
                    <div class="ssp-btn-price">
                        <h2>1089.50€</h4>
                            <div class="phone-links">
                                <a class="s20plusblack changable-s" href="https://gjirafa50.com/samsung/celular-samsung-galaxy-s20-128-gb-roze-e-embel/">Blej tani</a>
                                <a class="s20plusgray active changable-s" href="https://gjirafa50.com/samsung/celular-samsung-galaxy-s20-128-gb-e-hirte-kozmike/">Blej tani</a>
                                <a class="s20plusblue changable-s" href="https://gjirafa50.com/samsung/celular-samsung-galaxy-s20-128-gb-e-kalter-e-embel/">Blej tani</a>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="ss-prod entry-animation" style="transition-delay: .35s">
            <div style="border:0;">
                <div class="ssp-img">
                    <div id="s20ublack " class="s20ublack active changable-s" style="background-image:url('https://gjirafaadnetwork.blob.core.windows.net/html5/S20Ultra_black.png')"></div>
                    <div id="s20ugray" class="s20ugray changable-s" style="background-image:url('https://gjirafaadnetwork.blob.core.windows.net/html5/S20Ultra_gray.png')"></div>
                </div>
                <div class="ssp-content">
                    <div class="phone-title">
                        <h3 class="s20ugray changable-s">Samsung Galaxy S20 Ultra 128GB Hirtë Kozmike</h3>
                        <h3 class="s20ublack active changable-s">Samsung Galaxy S20 Ultra 128GB Zezë Kozmike</h3>
                    </div>
                    <h3 class="text-muted">
                        Rezolucioni i kamerës së prapme Mpx:<span style="color: #fff;"> 108</span>
                        <br /> Galaxy Buds+:<span style="color: #fff;"> falas</span> <br /> Kualiteti i ekranit:<span style="color: #fff;"> HDR10+, 120Hz</span>


                    </h3>
                    <div class="chose-colors ">
                        <span class="phone-color active " data-color="s20ublack" style="background:#000"></span>
                        <span class="phone-color " data-color="s20ugray" style="background:#5e6367"></span>
                    </div>
                    <div class="ssp-btn-price">
                        <h2>1469.50€</h4>
                            <div class="phone-links">
                                <a class="s20ublack active changable-s" href="https://gjirafa50.com/samsung/celular-samsung-galaxy-s20-ultra-128gb-e-kalter-e-embel/">Blej tani</a>
                                <a class="s20ugray changable-s" href="https://gjirafa50.com/samsung/celular-samsung-galaxy-s20-ultra-128gb-e-hirte-kozmike/">Blej tani</a>
                            </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
    <div class="flexing compare-phone flex-h-center">
        <button class="">KRAHASO MODELET</button>
    </div>
    <div class="product-overview">
        <div class="po-item flexing">
            <div class="po-content text-right">
                <h1>Samsung Galaxy S20 </h1>
                <h2 class="text-muted">
                    Njihuni me celularin më të fundit të serisë S. Përmirësimi në hapësirë, kualitet të fotografisë dhe jetëgjatësia e baterisë, janë vetëm disa nga shumë veçorit të cilat Samsung po iu ofron përdoruesve të tij.

                </h2>
                <a href="{"products.view&product_id=113701"|fn_url}">Blej tani</a>
            </div>

            <div class="po-img">
                <img src="https://gjirafaadnetwork.blob.core.windows.net/html5/sam3.png" />
            </div>
        </div>
        <div class="po-item flexing">
            <div class="po-img">
                <img src="https://gjirafaadnetwork.blob.core.windows.net/html5/sam2.png" />
            </div>
            <div class="po-content">
                <h1>Samsung Galaxy S20+</h1>
                <h2 class="text-muted">
                    Performanca e lartë e përpunimit të të dhënave, mundësohet falë procesorit të fuqishëm që është integruar në këtë model.

                </h2>
                <a href="{"products.view&product_id=113700"|fn_url}">Blej tani</a>
            </div>


        </div>
        <div class="po-item flexing">
            <div class="po-content text-right">
                <h1>Samsung Galaxy S20 Ultra</h1>
                <h2 class="text-muted">
                    Përjetoni ngjarjet me imazhe të pastërta dhe të detajizuara sikur të ishin reale falë kamerës së veçantë që Samsung po sjell. Zërimi i pastër, mbështetja e teknologjive të ndryshme dhe specifikat e reja e bën padyshim këtë celular njërin ndër më të fuqishmit
                    ne treg.

                </h2>
                <a href="{"products.view&product_id=113695"|fn_url}">Blej tani</a>
            </div>

            <div class="po-img">
                <img src="https://gjirafaadnetwork.blob.core.windows.net/html5/sam1.png" />
            </div>
        </div>
    </div>
</div>

<script>
    var banner = document.querySelector('.samsung-banner'),
        leftSpace = banner.offsetLeft;

    banner.style.marginLeft = '-' + leftSpace + 'px';
    banner.style.width = 'calc(100% + ' + leftSpace * 2 + 'px)';



    animationEL = document.querySelectorAll('.entry-animation');
    for (var i = 0; i < animationEL.length; i++) {
        animationEL[i].classList.add('active');
    }

    var phoneColors = document.querySelectorAll('.chose-colors .phone-color');

    for (var i = 0; i < phoneColors.length; i++) {
        phoneColors[i].onclick = function() {
            var thisPhoneColor = this.getAttribute('data-color'),
                thisClosestItem = this.closest('.ss-prod'),
                changableItems = thisClosestItem.querySelectorAll('.changable-s'),
                clickColorParent = this.parentElement.children;

            for (var k = 0; k < clickColorParent.length; k++) {
                clickColorParent[k].classList.remove('active');
                this.classList.add('active');
            }


            for (var j = 0; j < changableItems.length; j++) {
                changableItems[j].classList.remove('active');
            }
            document.querySelectorAll('.' + thisPhoneColor)[0].classList.add('active');
            document.querySelectorAll('.' + thisPhoneColor)[1].classList.add('active');
            document.querySelectorAll('.' + thisPhoneColor)[2].classList.add('active');

        }
    }

    var compareBtn = document.querySelector('.compare-phone button'),
        sModal = document.querySelector('.samsung-modal'),
        sOverlay = document.querySelector('.samsungoverlay'),
        closeSmodal = document.querySelector('.close-sam-modal');



    closeSmodal.onclick = function() {
        sOverlay.classList.remove('active');
        sModal.classList.remove('active');
        setTimeout(function() {
            sOverlay.style.display = 'none';
            sModal.style.display = 'none';
        }, 500);
    }

    compareBtn.onclick = function() {
        sOverlay.style.display = 'block';
        sModal.style.display = 'block';
        window.scrollTo(0, 150);
        setTimeout(function() {
            sOverlay.classList.add('active');
            sModal.classList.add('active');
        });
    }
</script>