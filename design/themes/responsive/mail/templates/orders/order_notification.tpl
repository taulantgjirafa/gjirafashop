{include file="common/letter_header.tpl"}

{__("dear")} {$order_info.firstname},<br/><br/>

{$order_status.email_header nofilter}<br/><br/>

{assign var="order_header" value=__("invoice")}
{if $status_settings.appearance_type == "C" && $order_info.doc_ids[$status_settings.appearance_type]}
    {assign var="order_header" value=__("credit_memo")}
{elseif $status_settings.appearance_type == "O"}
    {assign var="order_header" value=__("order_details")}
{/if}

{if $order_info.status == 'J' || $order_info.status == 'R'}
    {$delivery_dates = $order_info.order_id|fn_insert_order_delivery_dates}

    {if $delivery_dates}
        <p>
            Porosia juaj do të arrijë me datë <br>
            {$delivery_dates.standard|date_format:$settings.Appearance.date_format|fn_custom_date_format} - {$delivery_dates.standard_range|date_format:$settings.Appearance.date_format}
        </p>
        <br>
    {/if}
{/if}

<b>{$order_header}:</b><br/>

{include file="orders/invoice.tpl"}

{if $order_info.status == 'C' || $order_info.status == 'E'}
    {include file="common/email_rating.tpl" order_info=$order_info e_key= $e_key}
{/if}
{*{include file="common/letter_footer.tpl"}*}