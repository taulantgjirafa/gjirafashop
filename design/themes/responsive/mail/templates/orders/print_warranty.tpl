<!DOCTYPE html>
<html dir="{$language_direction}">
<head>
{literal}
<style type="text/css" media="print">
.main-table {
    background-color: #ffffff !important;
}
.main-table td {
    vertical-align: top;
}
</style>
<style type="text/css" media="screen,print">
    .xdebug-error{
        display: none !important;
        visibility: hidden !important;
    }
body,p,div,td {
    color: #000000;
    font: 10.5px Arial;
}
body {
    padding: 0;
    margin: 0;
}
p, li {
    line-height: 1.3;
}
h2 {
    font-size: 1.3em;
    font-weight: bolder;
}
a, a:link, a:visited, a:hover, a:active {
    color: #000000;
    text-decoration: underline;
}
a:hover {
    text-decoration: none;
}
    .container{
        width:1000px;
        margin:0 auto;
        display: block;
        padding:20px;
        max-width:90%;
    }
    .text-center{
        text-align: center;
    }
    .half-container{
        width:500px;
        border:1px solid #000;
        display: inline-block;
        box-sizing: border-box;
        margin-top: 15px;
        vertical-align: top;
        min-height:150px;
        max-width:50%;
    }
.half-container:nth-child(3){
    border-left:0;
}
.half-container h3 {
    margin: 0;
    border-bottom: 1px solid #000;
    padding: 10px;
}
.half-container p {
    margin: 0;
    padding: 10px;
    min-height: 60px;
}
    .half-container.n{
        min-height: 125px;
    }
    .half-container.n p{
        min-height: 20px;
    }
    .text-bold{
        font-weight:bolder;
    }
</style>
{/literal}
</head>

<body>
    <div class="container">
        <div class="text-center">
            <h1>FLETËGARANCIONI #{$order_info.order_id}</h1>
        </div>

        <div class="half-container n">
            <h3>Të dhënat e shitësit</h3>
            <p>Gjirafa Inc. – Dega në Kosovë (“<strong>Gjirafa</strong>”), shoqëri tregtare e Shteteve të Bashkuara të Amerikës, me degë në Republikën e Kosovës, Prishtinë, Magjistralja Prishtinë-Ferizaj, Kilometri i 6 (Lapnasellë), me numër të regjistrimit të biznesit 71075281 dhe numër fiskal 601144249</p>
        </div><div class="half-container n" style="min-height:129px; height: 129px;">
            <h3>Vula</h3>
            <p></p>
        </div>

        <p>Ky garancion lëshohet në përputhje me nenin 81 të Ligjit Nr. 06/L-084 për Mbrojtjen e Konsumatorit të Republikës së Kosovës dhe është përpiluar në gjuhën zyrtare të Republikës së Kosovës.</p>

        <h2>Kushtet e përgjithshme të garancionit - Deklarata</h2>

        <p>1. Gjirafa është importuesi dhe shitësi i produktit dhe ky garancion lëshohet në cilësi të importuesit. Gjirafa garanton se në momentin e shitjes së produktit, i njejti është funksional dhe se shërbimet e servisimit për të njejtin mund të kryhen në servisin e autorizuar, i cili për disa produkte mund të jetë jashtë Kosovës. Në rast se servisi i autorizuar është jashtë Kosovë, Gjirafa në kuadër të këtij garancioni, produktin e blerë nga konsumatori do ta dërgoj për servisim jashtë Kosovës dhe shpenzimet në lidhje me këtë do t’i mbuloj Gjirafa.</p>

        <p>2. Gjirafa garanton funksionalitetin e produktit në përputhje me karakteristikat teknike të prodhuesit. Gjatë gjithë periudhës së garancionit të shënuar në këtë fletëgarancion, ne do të sigurohemi të menjanojmë çfarëdo të mete, mungese ose prishje që paraqitet, në rastet se ju si konsumator keni përfillur kushtet e këtij garancioni, udhëzimet e përdorimit, produkti është përdorur normalisht dhe duke respektuar rregullat e përcaktuara nga vetë prodhuesi.</p>

        <p>3. Ju lutem t’i përcjellni procedurat në vijim në rast të identifikimit të çfarëdo prishje të produktit:</p>
<ul>
    <li>Sigurohuni se produktin e keni përdorur për qëllimet e destinuara të produktit (nëse është produkti për përdorim shtëpiak, ju nuk e keni përdorur për qëllime komerciale) dhe se përdorimi i produktit ka qenë në përputhje me instruksionet e prodhuesit.</li>
    <li>Kontaktoni me shitësin ose me një servis të autorizuar. Ju lusim të keni parasysh se jo të gjitha serviset janë të autorizuara, edhe nëse të njejtat mbajnë shenja të atij produkti të vendosura në lokal. Në rast se nuk keni një servis të autorizuar afër, ju lusim ta sjellni produktin në zyret qëndrore të Gjirafa në Prishtinë, në adresën e përcaktuar në fillim të këtij fletëgarancioni.</li>
    <li>Produkti duhet të dorëzohet në paketimin origjinal për të shmangur çfarëdo rreziku nga dëmtimet gjatë transportit. Kjo për shkak se në rast të dëmtimeve gjatë transportit, ne nuk mund të mbajmë përgjegjësi.</li>
    <li>Këtë fletëgarancion të plotësuar së bashku me të dhënat e produktit, datën e shitjes, faturën origjinale dhe atë fiskale (kur aplikohet) duhet ta dorëzoni në momentin e dorëzimit të produktit për servisim. Garancioni i produktit nuk vlen në mungesë të këtyre dokumenteve përcjellëse.</li>
</ul>

        <p>4. Gjirafa garanton mbi konformitetin e produktit, repektivisht mbi faktin se produkti është identik me përshkrimin e tij, ashtu siq kërkohet me Ligjin për Mbrojten e Konsumatorit. Gjirafa do të jetë përgjegjëse për 2 vite pas dorëzimit të mallrave për mungesën e konformitetit, për mallrat për të cilat kërkohet konformiteti sipas ligjit.</p>

        <h2 style="border:1px solid;padding:5px;">VINI RE</h2>

       <p> Garancioni i produktit vlen vetëm për defektet që kanë rezultuar si rezultat i prodhuesit, kur produkti është përdorur nga ju në përputhje me instruksionet e përdorimit dhe udhëzimeve teknike, duke përfshirë udhëzimet për montim dhe përdorimit për qëllime adekuate.</p>

        <p>Periudha e riparimit të produktit është maksimalisht 30 ditë. Për shkak se produktet e ofruar për shitje nga Gjirafa janë origjinale dhe importohen nga vendet e ndryshme të Bashkimit Evropian, në rast të nevojës së zëvendësimit të produktit me një produkt tjetër, periudha e zëvendësimit mund të zgjas më shumë se 30 ditë. Në atë rast, ju mund të zgjedhni të ju zëvendësohet produkti nëse një produkt i ngjashëm është në dispozicion, ose për periudhën e pritjes më të gjatë se 30 ditë, ne do të ofrojmë zgjatje të periudhës së garancionit pa pagesë shtesë për ato ditët shtesë të pritjes së zëvendësimit të produktit. Përzgjedhja mbetet të bëhet nga ju si konsumator.</p>

        <p>Në faqen në të cilën keni bërë porosinë (www.gjirafa.com, www.gjirafa50.com ose www.gjirafamall.com), gjenden informacione të plota dhe të sakta në lidhje me cilësinë, karakteristikat, apo ku është e aplikueshme, mënyrën e përdorimit dhe montimit, si dhe kushtet e shitjes, garancionin dhe afatin e garancionit të produktit.</p>

        <p>Si blerës, keni të drejtë që nga shitësi ku e keni blerë produktin ta ndërroni atë me tjetër (që nuk ka formë, madhësi, model apo ngjyrë të njejtë apo për arsye tjera) në afat prej 48 orësh nga dita kur është blerë produkti me kusht që produkti nuk është përdorur, është ruajtur pamja, vetitë përdoruese, vulat, etiketat e fabrikës, paketimi si dhe fatura dhe fatura fiskale (kur aplikohet). <br/>Afati i reklamacionit për aksesorë të celularëve edhe tabletëve si mbështjellëset, foliet, etj. është 24 orë.</p>

        <p>Si blerës, ju i gëzoni të gjitha të drejtat ligjore të cilat dalin nga ligjet e Kosovës dhe këto të drejta në asnjë rast nuk shkelen nga kjo fletë garancioni.</p>

        <p>KY GARANCION <strong>NUK</strong> DO TË VLEJË PËR RASTET SI VIJON:</p>
<ul>
    <li>Produktet i keni shfrytëzuar në kundërshtim me udhëzimet e përdorimit dhe çfarëdo manuali për përdorim të përcaktuar nga prodhuesi;</li>
    <li>Produktet e destinuara për përdorim individual ose shtëpiak përdoren për qëllime komerciale dhe profesionale;</li>
    <li>Produkti i sjellur për servisim nuk përkon me produktin e shitur për të cilin është lëshuar ky garancion dhe i cili është specifikuar në faturën e shitjes;</li>
    <li>Produkti ka shenja të dëmtimit fizik, kozmetik, të jashtëm ose të brendshëm që nuk janë rezultat i dëmeve të shkaktuara nga prodhuesi;</li>
    <li>Dëmtimet në produkt janë shkaktuar si rezultat i mbitensionit ose nëntensionit të energjisë elektrike, në variacionin plus/minus 15% të kufirit të përcaktuar të 230v.</li>
    <li>Produkti është dëmtuar si rezultat i ndodhive natyrore përtej kontrollit njerëzor, përfshirë, por pa u kufizuar në rrufe, vetëtimë, tërmet, vërshime, ujë, materiale të lëngshme, zjarr.</li>
    <li>Dëmtimet mekanike të shkaktuara nga rrëzimi, goditja dhe thyerja e produktit.</li>
    <li>Dëmtimi i produktit ka ndodhur si rezultat i hapësirës në të cilën e keni vendosur produktin (hapësirat e papërshtatshme), përfshirë vendet e ndryshme me prezencë të lagështisë, insekteve, papastërtisë, ndryshkut, etj.</li>
    <li>Nuk e keni mirëmbajtur produktin në mënyrën e duhur, ashtu siç kërkohet për përdorimin e rregullt të tij.</li>
    <li>Produkti është dëmtuar për shkak të ekspozimit ndaj temperaturave të papërshtatshme për produktin.</li>
    <li>Produkti është hapur fizikisht nga ju ose nga një palë e tretë e pa autorizuar për të hapur produktin dhe ndërhyrë në pjesët e brendshme të tij.</li>
    <li>Produktin e keni kyçur ose keni krijuar lidhje me çfarëdo pajisje të papërshtatshme ose jo origjinale ose pajisje e cila nuk është kompatibile me atë produkt.</li>
    <li>Ky garancion nuk vlen për produkte të konsumueshme ose për ndonjë pjesë të konsumueshme brenda produktit të blerë nga ju.</li>
    <li>Defekti është shkaktuar si rezultat i ndërhyrjes suaj në sistemin e produktit ose instalimet e sistemeve të papërshtatshme.</li>
    <li>Ekranet televizive ose monitorët kanë qëndruar për një kohë shumë të gjatë të ndezur, e si rezultat në të to janë shfaqur njolla të cilat nuk mund të riparohen.</li>
    <li>Në asnjë rast pjesë e ketij garancioni nuk janë çantat, mbështjellësit (covers & cases), kabllot, filterët e ndryshëm, telekomandat, adapterët, bateritë dhe pjesët e ngjashme.</li>
</ul>
        <div style='page-break-before: auto;'>&nbsp;</div>
        <h2>DEKLARATA E BLERËSIT</h2>

        <p class="text-bold">Unë, blerësi i produktit deklaroj se jam dakord me blerjen e produktit sipas karakteristikave të prezantuara nga shitësi, së bashku me të gjitha instruksionet dhe manualet e përdorimit. Unë e kuptoj gjuhën në të cilën më janë ofruar instruksionet, manualet dhe udhëzimet. Jam në njohuri me karakteristikat teknike të produktit, origjinën e tij dhe kushtet e shitjes. Jam në njohuri edhe me mënyrat e përdorimit të produktit.</p>

        <p class="text-bold">Në cilësi të blerësit, pajtohem në tërësi me kushtet e këtij Fletë Garancioni.</p>
        <hr>

        <p>Këto kushte të garancionit hyjnë në fuqi dhe blerësi jep pëlqimin për to, kurdo që shfaqet cilado nga rrethanat në vijim:</p>
        <ul style="list-style-type: decimal;">
            <li>Në momentin e nënshkrimit të tyre nga blerësi, në kopje fizike; ose</li>
            <li>Në momentin e regjistrimit të Blerësit në platformën e Gjirafa dhe pranimit të kushteve të këtij Garancioni, dhe të njejtat do t’i dërgohen në adresën e Blerësit në kopje fizike apo elektronike; ose;</li>
            <li>Në momentin e shkëmbimit të saj përmes postës elektronike dhe konfirmimit nga Ofruesi se i ka pranuar kushtet e së njejtës.</li>
        </ul>
        <p>Për shmangie të çfarëdo dyshimi, momenti i hyrjes në fuqi të këtyre kushteve të Garancionit do të konsiderohet momenti i parë i shfaqjes së cilësdo rrethanë të përcaktuar në paragrafin paraprak.</p>
        {* ____________________________ *}

        {* <div class="half-container" style="margin-top:0;width:100%;max-width: 100%; min-height:100px; height: 100px;">
            <h3>Nënshkrimi dhe vula</h3>
            <p></p>
        </div> *}
        <div class="half-container" style="margin-top:0;margin-bottom:15px;width:100%;border-top:5;max-width: 100%;">
            <p style="min-height:1px;">
                Të dhënat e produkteve:
                <span style="display: block;padding:5px;"></span>
                {foreach $order_info.products as $product}
                    {foreach $products_data[$product.product_code] as $serial_key}
                        {$product.product_code}  /  <strong>{$product.product}</strong> / {$serial_key} {if count($order_info.products) > 1}<br/>{/if}
                    {/foreach}
                {/foreach}
            </p>
        </div>
</body>
</html>