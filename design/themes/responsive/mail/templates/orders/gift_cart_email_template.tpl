{if $order_info}
    {assign "bonus" 0}
    {if $order_info.total > 50 && $order_info.total < 100}
        {$bonus = 5}
    {elseif $order_info.total > 100 && $order_info.total < 200}
        {$bonus = 8}
    {elseif $order_info.total > 200 && $order_info.total < 400}
        {$bonus = 15}
    {elseif $order_info.total > 400 && $order_info.total < 900}
        {$bonus = 25}
    {elseif $order_info.total > 900}
        {$bonus = 45}
    {/if}
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><meta name="viewport" content="initial-scale=1.0"><meta name="format-detection" content="telephone=no"><title>Gjirafa50 GiftCard</title><style type="text/css">
            {literal}  .socialLinks {font-size: 6px;}
            .socialLinks a {display: inline-block;}
            .socialIcon {display: inline-block;vertical-align: top;padding-bottom: 0px;border-radius: 100%;}
            table.vb-row.fullwidth {border-spacing: 0;padding: 0;}
            table.vb-container.fullwidth {padding-left: 0;padding-right: 0;}</style><style type="text/css">
            /* yahoo, hotmail */
            .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{ line-height: 100%; }
            .yshortcuts a{ border-bottom: none !important; }
            .vb-outer{ min-width: 0 !important; }
            .RMsgBdy, .ExternalClass{
                width: 100%;
                background-color: #3f3f3f;
                background-color: #3f3f3f}

            /* outlook */
            table{ mso-table-rspace: 0pt; mso-table-lspace: 0pt; }
            #outlook a{ padding: 0; }
            img{ outline: none; text-decoration: none; border: none; -ms-interpolation-mode: bicubic; }
            a img{ border: none; }

            @media screen and (max-device-width: 600px), screen and (max-width: 600px) {
                table.vb-container, table.vb-row{
                    width: 95% !important;
                }

                .mobile-hide{ display: none !important; }
                .mobile-textcenter{ text-align: center !important; }

                .mobile-full{
                    float: none !important;
                    width: 100% !important;
                    max-width: none !important;
                    padding-right: 0 !important;
                    padding-left: 0 !important;
                }
                img.mobile-full{
                    width: 100% !important;
                    max-width: none !important;
                    height: auto !important;
                }
            }{/literal}
        </style><style type="text/css">{literal}#ko_textBlock_4 .links-color a:visited, #ko_textBlock_4 .links-color a:hover {color: #3f3f3f;color: #3f3f3f;text-decoration: underline;}
            #ko_textBlock_5 .links-color a:visited, #ko_textBlock_5 .links-color a:hover {color: #3f3f3f;color: #3f3f3f;text-decoration: underline;}
            #ko_footerBlock_2 .links-color a:visited, #ko_footerBlock_2 .links-color a:hover {color: #ccc;color: #ccc;text-decoration: underline;}{/literal}</style></head><body bgcolor="#eee" text="#919191" alink="#cccccc" vlink="#cccccc" style="margin: 0;padding: 0;padding-top:50px;background-color: #eee;color: #919191;">

    <center>
        <table class="vb-outer" width="500" cellpadding="0" border="0" cellspacing="0" bgcolor="#eee" style="background-color: #eee;" id="ko_textBlock_4"><tbody><tr><td class="vb-outer" align="center" valign="top" bgcolor="#eee" style="padding-left: 9px;padding-right: 9px;background-color: #eee;">

                    <!--[if (gte mso 9)|(lte ie 8)]><table align="center" border="0" cellspacing="0" cellpadding="0" width="570"><tr><td align="center" valign="top"><![endif]-->
                    <div class="oldwebkit" style="max-width: 570px;">
                        <table width="570" border="0" cellpadding="0" cellspacing="18" class="vb-container fullpad" bgcolor="#ffffff" style="border-collapse: separate;border-spacing: 18px;padding-left: 0;padding-right: 0;width: 100%;max-width: 570px;background-color: #fff;"><tbody><tr><td align="left" class="long-text links-color" style="text-align: left; font-size: 13px; font-family: Helvetica, Arial, sans-serif; color: #3f3f3f;">
                                    <p style="margin:0;float:left;"><a href="" style="color: #3f3f3f;text-decoration: underline;"><img style="max-width:170px;" src="https://gjirafa50.com/images/logos/1/gjirafa50.png"/></a></p>
                                    <p style="margin: 0; float:right;font-size: 16px;font-weight:lighter;text-align:right;color:#999;">Kodi kuponit dhuratë<br/><strong style="color:#333;">50Urime2018-{$bonus}</strong></p>
                                </td>
                            </tr></tbody></table></div>
                    <!--[if (gte mso 9)|(lte ie 8)]></td></tr></table><![endif]-->
                </td>
            </tr></tbody></table><table class="vb-outer" width="500" cellpadding="0" border="0" cellspacing="0" bgcolor="#eee" style="background-color: #e;" id="ko_textBlock_5"><tbody><tr><td class="vb-outer" align="center" valign="top" bgcolor="#eee" style="padding-left: 9px;padding-right: 9px;background-color: #eee;">

                    <!--[if (gte mso 9)|(lte ie 8)]><table align="center" border="0" cellspacing="0" cellpadding="0" width="500"><tr><td align="center" valign="top"><![endif]-->
                    <div class="oldwebkit" style="max-width: 500px;">
                        <table width="500" border="0" cellpadding="0" cellspacing="18" class="vb-container fullpad" bgcolor="#e65228" style="border-collapse: separate;border-spacing: 18px;padding-left: 0;padding-right: 0;width: 100%;max-width: 570px;background-color: #e65228;border-radius: 0 0 30px 30px;"><tbody><tr><td align="left" class="long-text links-color" style="text-align: left; font-size: 13px; font-family: Helvetica, Arial, sans-serif; color: #fff;">
                                    <div style="width:65%;float:left;">
                                        <h1 style="margin:0;color:#fff;">KUPONI DHURATË</h1>
                                        <div style="width:100%;border-bottom:2px dashed #ee8c70;margin-top:10px;"></div>
                                        <p style="font-size: 15px;margin-bottom:0;color:#fff;">
                                            Përshëndetje {if $order_info.firstname}{$order_info.firstname}{/if} <br/><br/>
                                            Ju sapo keni blerë produkte me vlerë prej <strong>{$order_info.total}</strong> në Gjirafa50.com, dhe me rastin e festave të fundvitit ju keni fituar <strong>{$bonus}€</strong> dhuratë në llogarinë tuaj të cilat mund t’i shpenzoni në Gjirafa50.com duke filluar nga <strong>01.01.2018</strong>.
                                            <br/><br/>
                                            <span style="color:#fff;">Kjo vlerë prej {$bonus} eurosh është e vlefshme për përdorim deri me <strong>01.03.2018</strong>.</span><br/><br/>

                                        </p>
                                    </div>
                                    <div style="width:35%;float:left;">
                                        <div style="width: 110px;height: 110px;-webkit-border-radius: 100%;-moz-border-radius: 100%;border-radius: 100%;background:#fff;border:2px dashed #ee9d86;;text-align: center;margin: 0 auto;margin-left: 40px;color: #333;line-height: 110px;font-size: 45px;font-weight: bolder;">
                                            {$bonus}€
                                        </div>
                                    </div>
                                    <div style="width:100%;float:left;color: #ee9d86;text-align:center;">Për ndihmë si të shfrytëzoni kuponin tuaj telefono në <a href="tel:+37745101953" style="text-decoration:none;color: #ee9d86;">+377 45 101 953</a></div>

                                </td>
                            </tr></tbody></table></div>
                    <!--[if (gte mso 9)|(lte ie 8)]></td></tr></table><![endif]-->
                </td>
            </tr></tbody></table>
        <table class="vb-outer" width="500" cellpadding="0" border="0" cellspacing="0" bgcolor="#eee" >
            <tr>
                <td>
                    <p style="font-family: Helvetica, Arial, sans-serif;text-align:center;font-size:13px;color:#999;"><br/>Ju faleminderit që keni zgjedhur Gjirafa50.com për te bërë blerjet e juaja!<br/>
                        Me dashuri, Gjirafa</p>
                </td>
            </tr>
        </table>
    </center>
    </body></html>
{/if}