{* {include file="common/letter_header.tpl"}

{__("dear")},<br><br>

{__("back_in_stock_notification_header")}<br><br>
{assign var="suffix" value=""}
{if "ULTIMATE"|fn_allowed_for}
    {assign var="suffix" value="&company_id=`$product.company_id`"}
{/if}

<a href="{"products.view?product_id=`$product_id``$suffix`"|fn_url:'C':'http'}" style="text-decoration: none;">
	<img src="https://hhstsyoejx.gjirafa.net/gj50/img/{$product_id}/img/0.jpg" style="max-height: 300px;">
</a>
<br><br>

<b><a href="{"products.view?product_id=`$product_id``$suffix`"|fn_url:'C':'http'}">{$product.name nofilter}</a></b><br><br>

{include file="common/letter_footer.tpl"}

{__("back_in_stock_notification_footer")}<br><br>

<img src="https://gjirafa50.com/images/logos/1/gjirafa50.png"><br> *}

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml"
      xmlns:o="urn:schemas-microsoft-com:office:office">
<head><title></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style type="text/css">  #outlook a {
            padding: 0;
        }

        .ReadMsgBody {
            width: 100%;
        }

        .ExternalClass {
            width: 100%;
        }

        .ExternalClass * {
            line-height: 100%;
        }

        body {
            margin: 0;
            padding: 0;
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
        }

        table, td {
            border-collapse: collapse;
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        img {
            border: 0;
            height: auto;
            line-height: 100%;
            outline: none;
            text-decoration: none;
            -ms-interpolation-mode: bicubic;
        }
        .rating-stars img {
            margin-left: 5px;
            margin-right: 5px;
            width: 15px;
            height: 15px;
            display: inline-block;
        }
        p {
            display: block;
            margin: 13px 0;
        }
    </style>
    <style type="text/css">  @media only screen and (max-width: 480px) {
            @-ms-viewport {
                width: 320px;
            }    @viewport {
                width: 320px;
            }
        }</style>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet" type="text/css">
    <style type="text/css">        @import url(https://fonts.googleapis.com/css?family=Roboto);
        @import url(https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700);    </style>  <!--<![endif]-->
    <style type="text/css">  @media only screen and (min-width: 480px) {
            .mj-column-per-100 {
                width: 100% !important;
            }

            .mj-column-per-40 {
                width: 40% !important;
            }

            .mj-column-per-60 {
                width: 60% !important;
            }
        }</style>
</head>
<body>
	<div class="mj-container">
	    <div style="margin:0px auto;max-width:600px;background:#222;">
	        <table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;background:#222;"
	               align="center" border="0">
	            <tbody>
	            <tr>
	                <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:0px 0px 0px 0px;">
	                    <div class="mj-column-per-100 outlook-group-fix"
	                         style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
	                        <table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
	                            <tbody>
	                            <tr>
	                                <td style="word-wrap:break-word;font-size:0px;padding:19px 19px 19px 19px;"
	                                    align="center">
	                                    <table role="presentation" cellpadding="0" cellspacing="0"
	                                           style="border-collapse:collapse;border-spacing:0px;" align="center"
	                                           border="0">
	                                        <tbody>
	                                        <tr>
	                                            <td style="width:150px;"><img alt="" title="" height="auto"
	                                                                          src="https://storage.googleapis.com/topolio2241/gjirafa50.png"
	                                                                          style="border:none;border-radius:0px;display:block;font-size:13px;outline:none;text-decoration:none;width:100%;height:auto;"
	                                                                          width="150"></td>
	                                        </tr>
	                                        </tbody>
	                                    </table>
	                                </td>
	                            </tr>
	                            </tbody>
	                        </table>
	                    </div>
	                    </td>
	            </tr>
	            </tbody>
	        </table>
	    </div>
	    
	    <div style="margin:0px auto;max-width:600px;background:#FFFFFF;">
	        <table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;background:#FFFFFF;"
	               align="center" border="0">
	            <tbody>
	            <tr>
	                <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:9px 0px 9px 0px;">
	                    <div class="mj-column-per-100 outlook-group-fix"
	                         style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
	                        <table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
	                            <tbody>
	                            <tr>
	                                <td style="word-wrap:break-word;font-size:0px;padding:0px 24px 0px 24px;"
	                                    align="center">
	                                    <div style="cursor:auto;color:#757575;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:center;">
	                                        <p><span style="font-size:14px;">{__("dear")},</span>
	                                        </p>
	                                        <p><span style="font-size:14px;">{__("back_in_stock_notification_header")}</span>
	                                        </p></div>
	                                </td>
	                            </tr>
	                            <tr>
	                                <td style="word-wrap:break-word;font-size:0px;padding:10px 25px;padding-top:10px;padding-bottom:10px;">
	                                    <p style="font-size:1px;margin:0px auto;border-top:1px solid #ddd;width:100%;"></p>
	                                </td>
	                            </tr>
	                            </tbody>
	                        </table>
	                    </div>
	                    </td>
	            </tr>
	            </tbody>
	        </table>
	    </div>
	    
	    <div style="margin:0px auto;max-width:600px;background:#FFFFFF;">
	        <table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;background:#FFFFFF;"
	               align="center" border="0">
	            <tbody>
		            <tr>
		            	<td align="center">
		            		{assign var="suffix" value=""}
							{if "ULTIMATE"|fn_allowed_for}
							    {assign var="suffix" value="&company_id=`$product.company_id`"}
							{/if}

							<a href="{"products.view?product_id=`$product_id``$suffix`"|fn_url:'C':'http'}" style="text-decoration: none;">
								<img src="https://hhstsyoejx.gjirafa.net/gj50/img/{$product_id}/img/0.jpg" style="max-height: 300px;">
							</a>
		            	</td>
		            </tr>
		            <tr>
		            	<td style="font-size: 14px; padding: 15px 0;"
	                                    align="center">
							<b><a href="{"products.view?product_id=`$product_id``$suffix`"|fn_url:'C':'http'}">{$product.name nofilter}</a></b>
		            	</td>
		            </tr>
	            </tbody>
	        </table>
	    </div>
	    <div style="margin:0px auto;max-width:600px;background:#222;">
	        <table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;background:#222;"
	               align="center" border="0">
	            <tbody>
	            <tr>
	                <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:9px 0px 9px 0px;">
	                    <div class="mj-column-per-100 outlook-group-fix"
	                         style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
	                        <table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
	                            <tbody>
	                            <tr>
	                                <td style="word-wrap:break-word;padding:0px 20px 0px 20px; color: #fff;"
	                                    align="center">
	                                    {include file="common/letter_footer.tpl"}
	                                </td>
	                            </tr>
	                            <tr>
	                            	<td style="word-wrap:break-word;padding:0px 20px 0px 20px; color: #fff; font-size: 13px;"
	                                    align="center">
	                            		{__("back_in_stock_notification_footer")}
	                            	</td>
	                            </tr>
	                            <tr>
	                                <td style="word-wrap:break-word;font-size:0px;padding:10px 25px;" align="center">
	                                    <div>
	                                        <table role="presentation" cellpadding="0" cellspacing="0"
	                                               style="float:none;display:inline-table;" align="center" border="0">
	                                            <tbody>
	                                            <tr>
	                                                <td style="padding:4px;vertical-align:middle;">
	                                                    <table role="presentation" cellpadding="0" cellspacing="0"
	                                                           style="background:none;border-radius:3px;width:35px;"
	                                                           border="0">
	                                                        <tbody>
	                                                        <tr>
	                                                            <td style="vertical-align:middle;width:35px;height:35px;padding: 5px;"><a
	                                                                        href="https://www.facebook.com/Gjirafa50/"><img
	                                                                            alt="facebook" height="35"
	                                                                            src="https://s3-eu-west-1.amazonaws.com/ecomail-assets/editor/social-icos/outlinedbw/facebook.png"
	                                                                            style="display:block;border-radius:3px;"
	                                                                            width="35"></a>
	                                                            </td>
	                                                            <td style="vertical-align:middle;width:35px;height:35px;padding: 5px;"><a
	                                                                        href="https://www.instagram.com/gjirafa50"><img
	                                                                            alt="facebook" height="35"
	                                                                            src="https://s3-eu-west-1.amazonaws.com/ecomail-assets/editor/social-icos/outlinedbw/instagram.png"
	                                                                            style="display:block;border-radius:3px;"
	                                                                            width="35"></a>
	                                                            </td>
	                                                            <td style="vertical-align:middle;width:35px;height:35px;padding: 5px;"><a
	                                                                        href="https://twitter.com/gjirafa50"><img
	                                                                            alt="facebook" height="35"
	                                                                            src="https://s3-eu-west-1.amazonaws.com/ecomail-assets/editor/social-icos/outlinedbw/twitter.png"
	                                                                            style="display:block;border-radius:3px;"
	                                                                            width="35"></a>
	                                                            </td>
	                                                        </tr>
	                                                        </tbody>
	                                                    </table>
	                                                </td>
	                                                <td style="padding:4px 4px 4px 0;vertical-align:middle;"><a
	                                                            href="https://www.facebook.com/Gjirafa50/"
	                                                            style="text-decoration:none;text-align:left;display:block;color:#333333;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:22px;border-radius:3px;"></a>
	                                                </td>
	                                            </tr>
	                                            </tbody>
	                                        </table>
	                                        </div>
	                                </td>
	                            </tr>
	                            </tbody>
	                        </table>
	                    </div>
	                    </td>
	            </tr>
	            </tbody>
	        </table>
	    </div>
	</div>
</body>
</html>