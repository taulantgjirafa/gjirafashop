$(document).ready(function () {
    // $('.ty-grid-list__carousel--container li').css('width', $('.make3D').width());
    var currentParentEl;

    // Flip card to the back side
    $(document).on('click', '.view_gallery', function (e) {
        e.preventDefault();
        currentParentEl = $(this).parents('.ty-grid-list__item--container');

        currentParentEl.find('div.ty-grid-list__carousel--arrow-next, div.ty-grid-list__carousel--arrow-prev').removeClass('visible');
        currentParentEl.find('.make3D').addClass('flip180');
        currentParentEl.find('.ty-grid-list__item-overlay, .view_gallery, .grid-list-icons, .ty-discount-label').hide();
        currentParentEl.find('.ty-grid-list__back-images').show();

        // DESTROY SIBLINGS' CAROUSEL
        currentParentEl.siblings().find('.ty-grid-list__buy-now').removeClass('show');
        currentParentEl.find('.ty-grid-list__carousel--arrow-next').css('z-index','9');
        currentParentEl.siblings().find('.make3D.flip180')[0] != undefined && currentParentEl.siblings().find('.make3D.flip180')[0].setAttribute("data-slide", 0);
        currentParentEl.siblings().find('.make3D.flip180').removeClass('flip180');
        currentParentEl.siblings().find('.ty-grid-list__item-overlay, .view_gallery, .grid-list-icons').show();
        currentParentEl.siblings().find('.ty-grid-list__item-overlay, .view_gallery, .grid-list-icons, .ty-discount-label').show();
        currentParentEl.siblings().find('.ty-grid-list__back-images').hide();

        setTimeout(function () {
            currentParentEl.find('.cx, .cy').addClass('c-mod');
            currentParentEl.find('div.ty-grid-list__carousel--arrow-next, div.ty-grid-list__carousel--arrow-prev').addClass('visible');
        }, 150);

        makeCarousel(currentParentEl);
    });

    // Flip card back to the front side
    $(document).on('click', '.ty-grid-list__carousel--close', function () {

        //// RESET ALL
        currentParentEl.find('.ty-grid-list__buy-now').removeClass('show');
        currentParentEl.find('.make3D')[0] != undefined && currentParentEl.find('.make3D')[0].setAttribute("data-slide", 0);
        currentParentEl.find('.make3D').removeClass('flip180');
        currentParentEl.find('.ty-grid-list__item-overlay, .view_gallery, .grid-list-icons').show();
        currentParentEl.find('.ty-grid-list__item-overlay, .view_gallery, .grid-list-icons, .ty-discount-label').show();
        currentParentEl.find('.ty-grid-list__back-images').hide();

        setTimeout(function () {
            currentParentEl.find('.cx, .cy').removeClass('c-mod');
        }, 150);

    });

    //// CHANGE IMG SRC FUNCTION
    function generateImgLink(product_id, img_count) {
        var a = ((product_id / 8000 + 1) > 5) ? 5 : (product_id / 8000 + 1);
        var container = Math.floor(a);
        return "https://hhstsyoejx.gjirafa.net/gj50/img/" + product_id + "/thumb/" + img_count + ".jpg";
    }

    /* ----  Image Gallery Carousel   ---- */

    function makeCarousel(el) {

        var i = 0,
            isInit = $(el).find('.ty-quick-view-button__wrapper')[0].getAttribute('data-init'),
            productId = $(el).find('.ty-grid-list__item')[0].getAttribute('data-id');


        if (isInit == 1) {
            $(el).find('.ty-grid-list__img-to-change img')[0].setAttribute('src', generateImgLink(productId, 0));
            return false;
        } else {
            $(el).find('.ty-grid-list__img-to-change img')[0].setAttribute('src', generateImgLink(productId, i));
            $(el).find('.ty-quick-view-button__wrapper')[0].setAttribute('data-init', 1)
        }

    }

    // Load Next Image
    $(document).on('click', 'div.ty-grid-list__carousel--arrow-next, .ty-grid-list__img-to-change', function () {
        var bigParent = $(this).parents('.ty-grid-list__item--container');
        var current = bigParent.find(".ty-quick-view-button__wrapper")[0].getAttribute("data-slide", current);
        var allImages = bigParent.find('.ty-grid-list__item')[0].getAttribute('data-img-count');
        var productId = bigParent.find('.ty-grid-list__item')[0].getAttribute('data-id');


        if (current < allImages) {
            current++;
            bigParent.find(".ty-quick-view-button__wrapper")[0].setAttribute("data-slide", current);

            if (current <= allImages - 1) {
                bigParent.find('.ty-grid-list__img-to-change img')[0].setAttribute('src', generateImgLink(productId, current));

            }
            else {
                bigParent.find('.ty-grid-list__buy-now').addClass('show');
                bigParent.find('.ty-grid-list__carousel--arrow-next').css('z-index','-1');
            }

        }
        else {
            return;
        }

    });

    // Load Previous Image
    $(document).on('click', 'div.ty-grid-list__carousel--arrow-prev', function () {
        var bigParent = $(this).parents('.ty-grid-list__item--container');
        var current = bigParent.find(".ty-quick-view-button__wrapper")[0].getAttribute("data-slide", current);
        var allImages = bigParent.find('.ty-grid-list__item')[0].getAttribute('data-img-count')
        var productId = bigParent.find('.ty-grid-list__item')[0].getAttribute('data-id');

        if (current > 0) {
            if (current == allImages) {
                bigParent.find('.ty-grid-list__buy-now').removeClass('show');
                bigParent.find('.ty-grid-list__carousel--arrow-next').css('z-index','9');
            }

            current--;
            bigParent.find(".ty-quick-view-button__wrapper")[0].setAttribute("data-slide", current);
            bigParent.find('.ty-grid-list__img-to-change img')[0].setAttribute('src', generateImgLink(productId, current))
        }
        else {
            return
        }

    });
});

//horizontal homepage slider animation
function StartSlider(element) {
    this.mainElement = element;
    this.allSlides = [].slice.call(element.querySelectorAll('.hs-item'));
    this.arrows = [].slice.call(element.querySelectorAll('.hs-left, .hs-right'));
    this.wrapperToSlide = element.querySelector('.hs-scroll');
    this.widthOfAnItem = 0;
    this.slideCount = 0;
    this.counter = 0;
    this.init();
}
StartSlider.prototype = {
    init: function() {
        this.howManyOnViewFirst = this.howManyOnView() + 1;
        this.slideCount = this.allSlides.length - this.howManyOnViewFirst;

        if(this.allSlides.length <= 3) this.hideOrShowArrows(null, 'none');
        else {
            this.hideOrShowArrows(0);
            this.addClickEvents();
        }

        this.piecesToMove = [];
        var allCount = this.slideCount;
        while(allCount > 0) {
            var toPush = (allCount >= this.howManyOnViewFirst) ? this.howManyOnViewFirst : allCount;
            this.piecesToMove.push(toPush);
            allCount = allCount - (toPush);
        }
    },
    howManyOnView: function() {
        for(var i = 0; i < this.allSlides.length; i++) {
            if((this.allSlides[i].getBoundingClientRect().right - this.mainElement.getBoundingClientRect().left) >= this.mainElement.offsetWidth + 1)
                return i;
        }
    },
    hideOrShowArrows: function(which, all) {
        this.arrows.forEach(function(arrow, index) {
            arrow.style.setProperty('visibility', (which === index || all === 'none') ? 'hidden' : 'visible');
            if(all === 'all') arrow.style.setProperty('visibility', 'visible');
        });
    },
    clickHandler: function(arrow, index) {
        this.widthOfAnItem = this.allSlides[0].getBoundingClientRect().width + 13;

        if(index && (this.counter * this.howManyOnViewFirst) < this.slideCount) ++this.counter;
        else if (!index || this.counter > 0) --this.counter;

        var manyMoved = this.counter * this.howManyOnViewFirst;
        if(this.counter === this.piecesToMove.length) manyMoved = manyMoved - (this.howManyOnViewFirst - this.piecesToMove[this.counter - 1]);

        if(this.counter <= 0) this.hideOrShowArrows(0);
        else if(this.counter === this.slideCount || this.counter === this.piecesToMove.length) this.hideOrShowArrows(1);
        else this.hideOrShowArrows(null, 'all');

        this.wrapperToSlide.style.setProperty('transform', 'translate3d(-' + (manyMoved * this.widthOfAnItem) + 'px, 0, 0)');
    },
    addClickEvents: function() {
        this.arrows.forEach(function(arrow, i) {
            arrow.addEventListener('click', function(arrow, i) {
                this.clickHandler(arrow, i);
            }.bind(this, arrow, i));
        }, this);
    }
};
[].slice.call(document.querySelectorAll('.hs-section')).forEach(function(sliderContainer) {
    if(!sliderContainer.classList.contains('inited'))
        new StartSlider(sliderContainer);

    sliderContainer.classList.add('inited');
});