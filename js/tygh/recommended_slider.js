
var scroll = {
        timeStamp: 0, run: function (t) {
            t.timeStamp / 1e3 - this.timeStamp <= 1 || (this.timeStamp = t.timeStamp / 1e3)
        }
    },
    sliderWrappers = [].slice.call(document.querySelectorAll(".sliderWrapper"));

function Slider(t, e, i) {
    this.sliderWrapper = t, this.innerWrapper = e, this.images = Array.from(this.innerWrapper.children), this.lastItem = this.images[this.images.length - 1], this.arrows = i, this.count = 0, timeStamp = 0, this.init()
}

Slider.prototype = {
    init: function () {
        this.lastItem.getBoundingClientRect().right > this.sliderWrapper.getBoundingClientRect().right ? this.addEvents() : this.removeArrows()
    },
    clickHandler: function (t) {
        if (!(Date.now() / 1e3 - this.timeStamp <= .3)) {
            this.timeStamp = Date.now() / 1e3;
            var e = this.sliderWrapper.getBoundingClientRect().right,
                i = this.lastItem.getBoundingClientRect().right, r = this.count;
            if (0 === t ? r-- : r++, !(r < 0 || t && i < e)) {
                var n = this.innerWrapper.getBoundingClientRect().left - this.images[r].getBoundingClientRect().left + 45;
                this.innerWrapper.style.setProperty("transform", "translate3d(" + parseFloat(n) + "px,0,0)"), this.count = r
            }
        }
    },
    addEvents: function () {
        this.arrows.forEach(function (t, e) {
            t.addEventListener("click", this.clickHandler.bind(this, e))
        }, this)
    },
    removeArrows: function () {
        this.arrows.forEach(function (t) {
            t.style.display = "none"
        })
    }
};

sliderWrappers.forEach(function (slidWrapp) {
    var innerWrapper = slidWrapp.querySelector(".innerWrapper"),
        arrows = Array.prototype.slice.call(slidWrapp.querySelectorAll(".left, .right"))
    if($('.innerWrapper *').length) {
        $('.cart-recommended__wrapper').css('display','block');
        new Slider(slidWrapp, innerWrapper, arrows);
    }

});
