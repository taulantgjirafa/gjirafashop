var preChatTemplate = {
    //Form header color and Submit button color.
    mainbgColor: '#f04d24',
    //Form Header Text and Submit button text color.
    maintxColor: '#fff',
    //Chat Form Title
    heading: 'Ndihma e drejpërdrejtë',
    //Chat form Welcome Message
    textBanner: 'Mezi presim të ju shërbejmë. Por paraprakisht, ju lutem ndani pak sekonda të na tregoni pak për juve. ',
    //Submit Button Label.
    SubmitLabel: 'Fillo bisedën',
    CaretPosition: 'left',
    Position: 'left',
    position: 'left',
    //Fields List - Maximum is 5
    //All the values are mandatory and the script will not work if not available.
    fields : {
        field2 : {
            //Type for Name - Do not Change
            type: "name",
            //Label for Field Name, can be in any language
            label: "Emri",
            //Default - Field ID for Name - Do Not Change
            fieldId: "name",
            //Required "yes" or "no"
            required: "yes",
            //Error text to be displayed
            error: "Ju lutem shkruani emrin tuaj"
        },
        field3 : {
            //Type for Email - Do Not Change
            type: "email",
            //Label for Field Email, can be in any language
            label: "Email",
            //Default - Field ID for Email - Do Not Change
            fieldId: "email",
            //Required "yes" or "no"
            required: "yes",
            //Error text to be displayed
            error: "Ju lutem shkruani një email të vlefshëm"
        },
        field4 : {
            //Type for Phone - Do Not Change
            type: "phone",
            //Label for Field Phone, can be in any language
            label: "Numri i telefonit",
            //Default - Field ID for Phone - Do Not Change
            fieldId: "phone",
            //Required "yes" or "no"
            required: "no",
            //Error text to be displayed
            error: "Ju lutem shkruani një numer telefoni të vlefshëm"
        }
    }
};
window.fcSettings = {
    token: "eda20a1c-6e17-46e7-a349-fb7eae2e600e",
    host: "https://wchat.eu.freshchat.com",
    siteId: "1",
    tags: ["gjirafa50"],
    faqTags: { tags: ["gjirafa50"], filterType: "category"},
    config: {
        disableEvents: true,
        cssNames: {
            widget: 'fc_frame',
            open: 'fc_open',
            expanded: 'fc_expanded'
        },
        showFAQOnOpen: false,
        hideFAQ: false,
        agent: {
            hideName: false,
            hidePic: false,
            hideBio: false,
        },
        headerProperty: {
            //If you have multiple sites you can use the appName and appLogo to overwrite the values.
            appName: 'Gjirafa50',
            //appLogo: 'https://noah.gjirafa.com/api/storage/gjvideo/images/general/70b40a77-7c06-4bfe-98bb-5416302dbc1c.jpg',
            appLogo: '@T("Freshdesk.Applogo")',
            ///api/storage/gjvideo/images/general/b07f52bc-4abe-4cad-a819-b69e5a39629d.jpg
            backgroundColor: '#f04d24',
            foregroundColor: '#333333',
            backgroundImage: '@T("Freshdesk.BackgroundImage")',
            direction: "ltr"
        },
        content: {
            placeholders: {
                search_field: '@T("Freshdesk.Search_field")',
                reply_field: '@T("Freshdesk.Reply_field")',
                csat_reply: '@T("Freshdesk.Csat_reply")'
            },
            actions: {
                csat_yes: '@T("Freshdesk.Csat_yes")',
                csat_no: '@T("Freshdesk.Csat_no")',
                push_notify_yes: '@T("Freshdesk.Push_notify_yes")',
                push_notify_no: '@T("Freshdesk.Push_notify_no")',
                tab_faq: '@T("Freshdesk.Tab_faq")',
                tab_chat: '@T("Freshdesk.Tab_chat")',
                csat_submit: '@T("Freshdesk.Csat_submit")'
            },
            headers: {
                chat: '@T("Freshdesk.Chat")',
                chat_help: '@T("Freshdesk.Chat_help")',
                faq: '@T("Freshdesk.Faq")',
                faq_help: '@T("Freshdesk.faq_help")',
                faq_not_available: '@T("Freshdesk.faq_not_available")',
                faq_search_not_available: '@T("Freshdesk.faq_search_not_available")',
                faq_useful: '@T("Freshdesk.faq_useful")',
                faq_thankyou: '@T("Freshdesk.faq_thankyou")',
                faq_message_us: '@T("Freshdesk.faq_message_us")',
                push_notification: '@T("Freshdesk.push_notification")',
                csat_question: '@T("Freshdesk.csat_question")',
                csat_yes_question: '@T("Freshdesk.csat_yes_question")',
                csat_no_question: '@T("Freshdesk.csat_no_question")',
                csat_thankyou: '@T("Freshdesk.csat_thankyou")',
                csat_rate_here: '@T("Freshdesk.csat_rate_here")',
                channel_response: {
                    offline: '@T("Freshdesk.offline")',
                    online: {
                        minutes: {
                            one: '@T("Freshdesk.oneminute")',
                            more: '@T("Freshdesk.moreminutes")'
                        },
                        hours: {
                            one: '@T("Freshdesk.onehour")',
                            more: '@T("Freshdesk.morehours")',
                        }
                    }
                }
            }
        }
    },
    onInit: function() {
        fcPreChatform.fcWidgetInit(preChatTemplate);
    }
};