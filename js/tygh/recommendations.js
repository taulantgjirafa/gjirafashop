function htmlDecode(input) {
    var e = document.createElement('div');
    e.innerHTML = input;
    return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}

var biskoSid = window.localStorage.getItem('bisko-sid');
var rstore = $('.store').text();

// var biskoSidCookie = getCookie('bsid');
//
// if (biskoSid || (biskoSid && biskoSidCookie != biskoSid)) {
//     setCookie('bsid', biskoSid);
// }

// if ($('html').hasClass('homepage') && !biskoSidCookie) {
if ($('html').hasClass('homepage') && rstore != 'AL' && performance.navigation.type !== 2) {
    $.ajax({
        // url: fn_url('pages.recommendations'),
        url: 'index.php?dispatch=pages.recommendations&bisko_sid=' + biskoSid,
        type: "get",
        data: {bisko_sid: biskoSid},
        success: function (htmlResponse) {
            htmlDecoded = htmlResponse;
            var htmlArray = htmlDecoded.split("delimiter");

            $(htmlArray[0]).insertAfter('.ty-grid-list__item--container:eq(0)');
            $(htmlArray[1]).insertAfter('.ty-grid-list__item--container:eq(2)');
            $(htmlArray[2]).insertAfter('.ty-grid-list__item--container:eq(4)');
            $(htmlArray[3]).insertAfter('.ty-grid-list__item--container:eq(6)');
            $(htmlArray[4]).insertAfter('.ty-grid-list__item--container:eq(8)');
        },
        error: function () {
        },
        complete: function () {
            $('#is_loader.rec-l').removeClass('active');
            $('.grid-list').removeClass('hidden');
            $('.load_more_category').removeClass('hidden');
        },
        // async: false
        // timeout: 10000
    });
} else {
    $('#is_loader.rec-l').removeClass('active');
    $('.grid-list').removeClass('hidden');
    $('.load_more_category').removeClass('hidden');
}