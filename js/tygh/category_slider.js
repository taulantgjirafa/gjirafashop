var imgHeight = document.querySelector('.rec-banner img').clientHeight;
document.querySelector('.rec-banner').style.height = imgHeight + 'px';
function StartSlider(element) {
    this.mainElement = element;
    this.allSlides = [].slice.call(element.querySelectorAll('.wrapperToSlide .subcategories:nth-of-type(1) .bbox'));
    this.arrows = [].slice.call(element.querySelectorAll('.sliderOuterWrapper .sliderArrows'));
    this.wrapperToSlide = element.querySelector('.wrapperToSlide');
    this.widthOfAnItem = 0;
    this.slideCount = 0;
    this.counter = 0;
    this.init();
}
StartSlider.prototype = {
    init: function() {
        this.howManyOnViewFirst = this.howManyOnView();
        this.slideCount = this.allSlides.length - this.howManyOnViewFirst;

        if(this.allSlides.length <= 3) this.hideOrShowArrows(null, 'none');
        else {
            this.hideOrShowArrows(0);
            this.addClickEvents();
        }

        this.piecesToMove = [];
        var allCount = this.slideCount;
        while(allCount > 0) {
            var toPush = (allCount >= this.howManyOnViewFirst) ? this.howManyOnViewFirst : allCount;
            this.piecesToMove.push(toPush);
            allCount = allCount - (toPush);
        }
    },
    howManyOnView: function() {
        for(var i = 0; i < this.allSlides.length; i++) {
            if((this.allSlides[i].getBoundingClientRect().right - this.mainElement.getBoundingClientRect().left + 10) >= this.mainElement.offsetWidth)
                return i + 1;
        }
    },
    hideOrShowArrows: function(which, all) {
        this.arrows.forEach(function(arrow, index) {
            arrow.style.setProperty('display', (which === index || all === 'none') ? 'none' : 'block');
            if(all === 'all') arrow.style.setProperty('display', 'block');
        });
    },
    clickHandler: function(arrow, index) {
        this.widthOfAnItem = this.allSlides[0].getBoundingClientRect().width + 10;

        if(index && (this.counter * this.howManyOnViewFirst) < this.slideCount) ++this.counter;
        else if (!index || this.counter > 0) --this.counter;

        var manyMoved = this.counter * this.howManyOnViewFirst;
        if(this.counter === this.piecesToMove.length) manyMoved = manyMoved - (this.howManyOnViewFirst - this.piecesToMove[this.counter - 1]);

        if(this.counter <= 0) this.hideOrShowArrows(0);
        else if(this.counter === this.slideCount || this.counter === this.piecesToMove.length) this.hideOrShowArrows(1);
        else this.hideOrShowArrows(null, 'all');

        this.wrapperToSlide.style.setProperty('transform', 'translate3d(-' + (manyMoved * this.widthOfAnItem) + 'px, 0, 0)');
    },
    addClickEvents: function() {
        this.arrows.forEach(function(arrow, i) {
            arrow.addEventListener('click', function(arrow, i) {
                this.clickHandler(arrow, i);
            }.bind(this, arrow, i));
        }, this);
    }
};
    
var catSlider = new StartSlider(document.querySelector('.sliderOuterWrapper'));