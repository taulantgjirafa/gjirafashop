if ($('.db_switch').text() != '1') {
    var clicked = false;
    var infinite = false;
    var sort_by = '';
    var sort_order = '';
    var page = 2;
    var offset = 24;
    var limit = 24;
    var category_id = $('#category_id').val();
    var es_url_data = "";
    var checkedCount = 0;
    var search_page = window.location.href.indexOf("search") > -1;
    var requestReturned = true;
    var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
    var brands_page = window.location.href.indexOf("pages.brands") > -1 && window.location.href.indexOf("brand=") === -1;
    var brand_ids = [771, 403, 240, 533, 2615, 2324, 2769, 1973, 110531, 2145, 44090, 3311, 2032, 12583, 109515, 64177];
    var hmpg = $('html').hasClass('homepage');
    var product_view = $('.ty-product-bigpicture').length;
    var start = 771;
    var next = brand_ids[brand_ids.indexOf(start) + 1];
    var brand_page = window.location.href.indexOf("?brand=") > -1;

    var slice = 0;
    var a = 0;
    
    var obF = {
        findAncestor: function(element, classNAme, count) {
            if(element.classList.contains(classNAme)) return true;

            var parent = element.parentNode,
                conditions = (parent && parent.classList);

            if (conditions && !parent.classList.contains(classNAme) && count)
                return this.findAncestor(parent, classNAme, --count);
            else if (conditions && parent.classList.contains(classNAme))
                return parent;
        },
        findAncestorById: function(element, id, count) {
            if(element.id === id) return true;

            var parent = element.parentNode;

            if (parent && parent.id !== id && count)
                return this.findAncestorById(parent, id, --count);
            else if (parent && parent.id === id)
                return parent;
        },
        feature_hash: function() {
            var es_url_data = "";
            var $checkedInput = $('[name^="product_filters"]:checked');
            var lastdcfid = $checkedInput.eq(0).data('ca-filter-id');
            checkedCount = 0;
            for (var i = 0; i < $checkedInput.length; i++) {
                checkedCount++;

                if (lastdcfid != $checkedInput.eq(i).data('ca-filter-id')) {
                    es_url_data = es_url_data + "_";
                }
                lastdcfid = $checkedInput.eq(i).data('ca-filter-id');
                es_url_data = es_url_data + $checkedInput.eq(i).data('ca-filter-id') + '-' + $checkedInput.eq(i).val();
                price_left = $('#slider_961_left').val();
                price_right = $('#slider_961_right').val();
                es_url_data = es_url_data.replace("961-1-100000000-EUR", "961-" + price_left + "-" + price_right + "-EUR");
                window.es_url_data = es_url_data;
            }
            return es_url_data;
        },
        getParameterByName: function(name, url) {
            if (!url) url = window.location.href;
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        },
        sort_url: function (url) {
            window.sort_by = obF.getParameterByName("sort_by", url);
            window.sort_order = obF.getParameterByName("sort_order", url);
            items_per_page = obF.getParameterByName("items_per_page", url);

            if (items_per_page == null) {
                window.offset = 24;
                window.page = 2;
                window.limit = 24;
            } else {
                window.offset = parseInt(items_per_page);
                window.page = 2;
                window.limit = parseInt(items_per_page);
            }

            history.pushState({}, null, url);
        },
        throttle: function(fn, wait) {
            let time = Date.now();
            return function() {
                if ((time + wait - Date.now()) < 0) {
                    fn();
                    time = Date.now();
                }
            }
        },
        load_more_infinite: function() {
            if (clicked) {
                return false;
            }

            $('#is_loader').addClass('active');

            obF.feature_hash();

            if (document.getElementById('tygh_footer')) {
                infinite = true;

                $('#tygh_footer, .ty-gjirafaFlex').remove();
                $('.brands-list').parent().parent().remove();
                $('#load_more_infinite').hide();
            }

            if (offset == limit)
                $('#load_more_infinite').hide();

            clicked = true;
            requestReturned = false;
            $.ajax({
                url: obF.handlerInsideRequest('forUrl'),
                data: obF.handlerInsideRequest('forObject'),
                success: function (res) {
                    obF.handlerInsideRequest('forFunction', res);
                }
            });

            if (search_page && !($('#es_q').val() != undefined)) {
                $('#is_loader').removeClass('active');
                $('#no_results').show();
                return false;
            }

            return false;
        },
        productsInStockFileerHandler: function(e) {
            e.preventDefault();
            var url = '';
            var es_url_data = obF.feature_hash();
            var category_id = $('#category_id').val();
            if (window.location.href.indexOf("search") > -1) {
                url = '?dispatch=products.search&q=' + obF.getParameterByName("q", window.location.href) + '&features_hash=' + es_url_data + '_2041-135554';
            } else {
                url = '?dispatch=categories.view&category_id=' + category_id + '&features_hash=' + es_url_data + '_2041-135554';
            }
            window.location.assign(url);
            return false;
        },
        handlerInsideRequest: function(type, results) {
            if (search_page) this.inWhichPage = 'search';
            else if (brands_page) this.inWhichPage = 'default_brands';
            else if (brand_page) this.inWhichPage = 'brand';
            else if (hmpg) this.inWhichPage = 'homepage';
            else if (product_view) this.inWhichPage = 'product_view';

            return this[type](results);
        },
        forUrl: function() {
            var returnValue;
            
            switch(this.inWhichPage) {
                case 'search':
                    returnValue = 'index.php?dispatch=products.search';
                    break;
                case 'default_brands':
                    returnValue = 'index.php?dispatch=pages.brands';
                    break;
                case 'brand':
                    returnValue = 'index.php?dispatch=pages.brands&brand=' + this.getParameterByName('brand', window.location.href);
                    break;
                case 'homepage':
                    returnValue = 'index.php?dispatch=products.load_products';
                    break;
                case 'product_view':
                    returnValue = 'index.php?dispatch=products.is_products';
                    break;
                default:
                    returnValue = 'index.php?dispatch=categories.view';
                    break;
            }
            
            return returnValue;
        },
        forObject: function() {
            var returnValue;
            
            switch (this.inWhichPage) {
                case 'search':
                    returnValue = {
                        q: $('#es_q').val(),
                        isAjax: 1,
                        page: page,
                        limit: limit,
                        sort_by: sort_by,
                        sort_order: sort_order,
                        features_hash: es_url_data
                    };
                    break;
                case 'default_brands':
                    returnValue = {
                        isAjax: 1,
                        brand_value: next
                    };
                    break;
                case 'brand':
                    returnValue = {
                        isAjax: 1,
                        page: page,
                        offset: offset,
                        limit: 20
                    };
                    break;
                case 'homepage':
                    returnValue = {
                        isAjax: 1,
                        page: page,
                        limit: 20,
                        offset: offset
                    }
                    break;
                case 'product_view':
                    returnValue = {
                        category_id: category_id,
                        isAjax: 1,
                        page: page,
                        limit: 10,
                        offset: offset
                    }
                    break;
                default:
                    returnValue = {
                        category_id: category_id,
                        isAjax: 1,
                        page: page,
                        limit: limit,
                        sort_by: sort_by,
                        sort_order: sort_order,
                        features_hash: es_url_data
                    };
                    break;
            }
            
            return returnValue;
        },
        forFunction: function(results) {
            requestReturned = true;

            if (next == undefined) {
                infinite = false;
                $('#no_results').show();
                return;
            }
            
            if (window.innerWidth < 769 && !document.querySelectorAll('.product-content').length) {
                $('.grid-list').last().append(results);
                a++;
                $.processForms($('.grid-list').last());
            } else {
                $('.grid-list').last().append(results);
                $.processForms($('.grid-list').last());
            }

            if (next === brand_ids[brand_ids.length]) {
                infinite = false;
                $('#no_results').show();
                return;
            }

            if (results.trim() == '') {
                $('#is_loader').removeClass('active');
                $('#no_results').show();
                infinite = false;
                return false;
            } else {
                switch (this.inWhichPage) {
                    case 'search':
                        page++;
                        break;
                    case 'default_brands':
                        start = next;
                        next = brand_ids[brand_ids.indexOf(start) + 1];
                        break;
                    case 'brand':
                        page++;
                        offset = offset + 24;
                        break;
                    default:
                        page++;
                        break;
                }
                $('#is_loader').removeClass('active');
                clicked = false;
            }
        }
    };
    
    var feature_hash = obF.feature_hash,
        getParameterByName = obF.getParameterByName,
        sort_url = obF.sort_url,
        load_more_infinite = obF.load_more_infinite,
        productsInStockFileerHandler = obF.productsInStockFileerHandler;

    document.addEventListener('click', function(e) {
      if(obF.findAncestor(e.target, 'cm-product-filters-checkbox', 10)) {
         page = 2;
         limit = 24;
         clicked = false;
      }
      
      if(obF.findAncestorById(e.target, "load_more_infinite", 1)) {
         e.preventDefault();
         obF.load_more_infinite();
      }
   });

    $(document).on('click', '.products_in_stock_filter', obF.productsInStockFileerHandler);
   
    function callScroll(calledAfter) {
        clearTimeout(obF.timeoutId);

        if(load_more_available &&
           requestReturned && infinite &&
           ((window.innerHeight + window.pageYOffset) >= (document.body.offsetHeight - 450))) obF.load_more_infinite();

        if(!calledAfter && requestReturned)
           obF.timeoutId = setTimeout(callScroll.bind(null, true), 300);
    }
    var load_more_available = document.getElementById('load_more_infinite');
    window.addEventListener('scroll', obF.throttle(callScroll, 200));
}