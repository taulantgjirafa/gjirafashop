function initFreshChat() {
    window.fcWidget.init({
        token: "eda20a1c-6e17-46e7-a349-fb7eae2e600e",
        host: "https://wchat.eu.freshchat.com",
        siteId: "1",
        tags: ["gjirafa50"],
        faqTags: {
            tags: ['gjirafa50'],
            filterType: 'category'
        },
        config: {
            disableEvents: true,
            cssNames: {
                widget: 'fc_frame',
                open: 'fc_open',
                expanded: 'fc_expanded'
            },
            showFAQOnOpen: false,
            hideFAQ: false,
            agent: {
                hideName: false,
                hidePic: false,
                hideBio: false,
            },
            headerProperty: {
                //If you have multiple sites you can use the appName and appLogo to overwrite the values.
                appName: 'Gjirafa50',
                appLogo: 'https://gjirafa50.com/images/appicon/50-48px.png',
                backgroundColor: '#e65228',
                foregroundColor: '#ffffff',
                backgroundImage: 'https://wchat.freshchat.com/assets/images/texture_background_1-bdc7191884a15871ed640bcb0635e7e7.png',
                direction: "ltr"
            },
            content: {
                placeholders: {
                    search_field: 'Kërko',
                    reply_field: 'Përgjigju',
                    csat_reply: 'Shto komente këtu'
                },
                actions: {
                    csat_yes: 'Po',
                    csat_no: 'Jo',
                    push_notify_yes: 'Po',
                    push_notify_no: 'Jo',
                    tab_faq: 'Zgjidhje',
                    tab_chat: 'Chat',
                    csat_submit: 'Submit'
                },
                headers: {
                    chat: 'Gjirafa50 live chat',
                    chat_help: 'Na kontaktoni për çdo paqartësi',
                    faq: 'Pytje të shpeshta',
                    faq_help: 'Shfletoni artikujt tonë',
                    faq_not_available: 'Nuk u gjetë asnjë rezultat',
                    faq_search_not_available: 'Nuk u gjetë asnjë rezultat për {{query}}',
                    faq_useful: 'A ju ndihmoi ky artikull?',
                    faq_thankyou: 'Faleminderit për vlerësimin tuaj.',
                    faq_message_us: 'Na kontaktoni këtu.',
                    push_notification: 'Mos humbni asnjë përgjigje nga ne! Dëshironi të lejoni njoftimet?',
                    csat_question: 'A kemi adresuar shqetësimet e juaja?',
                    csat_yes_question: 'Si do ta vlerësonit këtë bisedë?',
                    csat_no_question: 'Si mund të kishim ndihmuar më mirë?',
                    csat_thankyou: 'Faleminderit për përgjigjen tuaj',
                    csat_rate_here: 'Vendosni vlerësimin tuaj këtu',
                    channel_response: {
                        offline: 'Momentalisht jemi offline. Ju lutem lini një mesazh.',
                        online: {
                            minutes: {
                                one: "Aktualisht përgjigjemi për {!time!} minutë",
                                more: "Zakonisht përgjigjemi për {!time!} minuta"
                            },
                            hours: {
                                one: "Aktualisht përgjigjemi për më shpejtë se një orë.",
                                more: "Zakonisht përgjigjemi për {!time!} orë",
                            }
                        }
                    }
                }
            }
        }
    });
}
function initialize(i, t) {
    var e;
    i.getElementById(t) ? initFreshChat() :
        ((e = i.createElement("script")).id = t,
            e.async = !0, e.src = "https://wchat.eu.freshchat.com/js/widget.js",
            e.onload = initFreshChat, i.head.appendChild(e))
}
function initiateCall() {
    initialize(document, "freshchat-js-sdk")
}