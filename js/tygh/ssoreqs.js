var ssourl = 'https://sso.gjirafa.com/';
var is_safari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);

function loadAsyncIframe(src, id) {
    var s,
        r,
        t;
    r = false;
    s = document.createElement('iframe');
    s.src = src;

    if (id === 'sso_jwt') {
        if (is_safari) {
            s.src = src + '&sid=' + sid;
        }
    }

    s.setAttribute('id', id);
    s.onload = s.onreadystatechange = function () {
        if (!r && (!this.readyState || this.readyState == 'complete')) {
            r = true;
        }
    };
    t = document.getElementsByTagName('div')[0];
    t.parentNode.insertBefore(s, t);
}
var SSOterms = {
    show: function (px) {
        // document.querySelector('#iframe_cookie').style.height = px + "px";
        document.querySelector('#iframe_cookie').style.display = "block";
    },
    hide: function () {
        document.querySelector('#iframe_cookie').style.display = "none";
    }
};
var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent",
    eventer = window[eventMethod],
    messageEvent = eventMethod === "attachEvent" ? "onmessage" : "message";
eventer(messageEvent, function (e) {
    if (e.data === "true" || e.message === "true")
        SSOterms.hide();
    else if (e.data.acceptTarget === "cookie") {
        SSOterms.show(e.data.acceptHeight);
    }
});

var SSOVerify = {
    show: function (px) {
        document.querySelector('#verifyAcc').style.height = px + "px";
    },
    hide: function () {
        document.querySelector('#verifyAcc').style.height = "0px";
    }
};

function setCookie(name, value, minutes) {
    var expires = "";
    if (minutes) {
        var date = new Date();
        date.setTime(date.getTime() + (minutes * 60 * 1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "") + expires + "; path=/";
}

function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

var current_url = window.location.href;
var current_url = new URL(current_url);
var as_user = current_url.searchParams.get('as_user');

if (as_user) {
    setCookie('as_user', 1, 5);
    setCookie('re', 0, 5);
}

var shouldExecuteJwtProcess = false;
var reCookie = getCookie('re');
if (reCookie > 0) {
    shouldExecuteJwtProcess = false;
} else {
    shouldExecuteJwtProcess = true;
}

var eventMethodVerify = window.addEventListener ? "addEventListener" : "attachEvent",
    eventerVerify = window[eventMethodVerify],
    messageEvenVerify = eventMethodVerify === "attachEvent" ? "onmessage" : "message";
eventerVerify(messageEvenVerify, function (event) {
    if (event.data === "close" || event.message === "close")
        SSOVerify.hide();
    else if (event.data.verifyTarget === "verify") {
        SSOVerify.show(event.data.verifyHeight);
    } else if (event.data.toString().indexOf("Token") >= 0) {
        if (shouldExecuteJwtProcess && getCookie('as_user') == null) {
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    var response = xhttp.responseText;
                    if (response == "success") {
                        setCookie('re', 1, 1);
                        location.reload();
                        return;
                    }
                }
            };

            var params = JSON.stringify({ token: event.data });
            xhttp.open("POST", fn_url('sso.auth'), true);
            xhttp.setRequestHeader("Content-type", "application/json; charset=utf-8");
            xhttp.send(params);
        } else {
            document.getElementById('sso_base_url').value = 'https://sso.gjirafa.com';
        }
    }
});

loadAsyncIframe(ssourl + 'Cookie/Index?u=https://gjirafa50.com', 'iframe_cookie');
loadAsyncIframe(ssourl + 'cookie/verifyAccount?u=https://gjirafa50.com', 'verifyAcc');
loadAsyncIframe(ssourl + 'Jwt/Index?host=gjirafa50.com', 'sso_jwt');

document.querySelector('#sso_jwt').style.display = "none";