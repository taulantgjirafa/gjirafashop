var pixel_id = '109070906441547';
var CURRENCY = 'EUR';
var store = $('.store').text();
if (store == 'AL') {
    pixel_id = '164813230973849'
}
var fbGraphApiAccessToken = 'EAAONJZAsDV6YBAJqOxmcP59UdXgvl7VAkls3QZBj6stDpe2Q3iFCrLCMKrI7Vg0ZAyZCO8FTZBLWACFghgcBYKfgKX2dibArjjDhofxmbJDFRKVnOCoPaKf5OCWKnV94WuOrUZAI8NZAZCQORjnNcqPlagHRXRtmXvcLuLfz4QkYZAHRSFZA2cAgD3';
var currentUserEmail = $('[name="logged-in-user-email"]').val();

//FACEBOOK PIXEL INIT CODE
!function (f, b, e, v, n, t, s) {
    if (f.fbq) return;
    n = f.fbq = function () {
        n.callMethod ?
            n.callMethod.apply(n, arguments) : n.queue.push(arguments)
    };
    if (!f._fbq) f._fbq = n;
    n.push = n;
    n.loaded = !0;
    n.version = '2.0';
    n.queue = [];
    t = b.createElement(e);
    t.async = !0;
    t.src = v;
    s = b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t, s)
}(window,
    document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', pixel_id);
fbq('track', 'PageView');

(function (h, o, t, j, a, r) {
    h.hj = h.hj || function () {
        (h.hj.q = h.hj.q || []).push(arguments)
    };
    h._hjSettings = {hjid: 331377, hjsv: 5};
    a = o.getElementsByTagName('head')[0];
    r = o.createElement('script');
    r.async = 1;
    r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
    a.appendChild(r);
})(window, document, '//static.hotjar.com/c/hotjar-', '.js?sv=');

function parse_fbpixel_param(elem, type) {
    if (elem == undefined || type == undefined) {
        return
    } else {
        switch (type) {
            case "price":
                return elem.text().replace(/[^0-9.]/g, "");
            case "title":
                return elem.text().trim().replace('ë', 'e');
            case "product_id":
                return elem.replace(/[^0-9]/g, '');

        }
    }
}


function getUrlParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}

if ($('#d_mode').val() == '1') {

    var breadcrumb=$(".gjanout-category").map(function(){return $.trim($(this).text())}).get();
    breadcrumb[0]="Teknologji";
    if($(".ty-product-bigpicture").length)
        breadcrumb.pop();
    gjdmp.pv(breadcrumb);
    bisko.pv();

    if ($('.ty-grid-list__item').length) {

        var list = 'Category Results';
        if (getUrlParameter('dispatch') == 'products.search')
            list = 'Search Results';
        else if (window.location.pathname == '/')
            list = 'Homepage Results';

        for (var i = 0; i < $('.ty-grid-list__item').length; i++) {
            var $this = $('.ty-grid-list__item').eq(i);
            ga('ec:addImpression', {
                'id': $('.ty-quick-view-button > a', $this).data('ca-view-id'),
                'name': parse_fbpixel_param($('.product-title', $this), "title"),
                'list': list,
                'position': i
            });

        }

        ga('send', {
            hitType: 'event',
            eventCategory: 'Products',
            eventAction: 'Impressions',
            eventLabel: 'HomePage'
        });
    }

//Add to cart - click
    $(document).on('click', '.add-to-cart-new, .ty-product-block__button .ty-btn__add-to-cart, .ty-grid-list__item .ty-btn__add-to-cart', function () {
        let name = $(this).closest('.ty-grid-list__item').find('.product-title').text();
        let value = parseFloat($(this).closest('.ty-grid-list__item').find('.ty-price-num').text().replace(',', ''));
        let quantity = $('.ty-value-changer__input').val() ? $('.ty-value-changer__input').val() : 1;
        let category = $('.ty-breadcrumbs__a.gjanout-category').last().text();
        let ean = $('*[data-feature="EAN"]').next().text() ? $('*[data-feature="EAN"]').next().text() : '';

        if (name === '') {
            name = $('.ty-product-block-title').text();
        }

        if (!value) {
            value = parseFloat($('.price_inner--amount').text().replace(',', ''));
        }

        if (isNaN(value)) {
            value = parseFloat($('.ty-product-block__price-actual .ty-price span:first').text().replace(',', ''));
        }

        var addtocartrandnum = 1 + Math.floor(Math.random() * 1000000)

        fbq('track', 'AddToCart', {
            content_type: 'product',
            content_ids: parse_fbpixel_param($(this).attr('name'), "product_id"),
            content_name: name,
            // value: parse_fbpixel_param($('.ty-product-block__price-actual .ty-price-num'), "price"),
            value: value,
            currency: CURRENCY
        }, {
            eventID: 'addtocart_' + addtocartrandnum
        });

        sendFbConversionApiRequest('AddToCart', currentUserEmail, CURRENCY, value, 'addtocart_' + addtocartrandnum);

        gjdmp.tr("AddToCart", {
            id: parse_fbpixel_param($(this).attr('name'), "product_id"),
            user_id: $('#store_user').val(),
            name: name,
            type: 'product',
            value: value,
            quantity: quantity,
            category: category,
            sku: ean.trim(),
            currency: CURRENCY
        });

        ga('ecommerce:addItem', {
            'id': parse_fbpixel_param($(this).attr('name'), "product_id"),
            'name': parse_fbpixel_param($('.ty-product-block-title'), "title"),
            'price': parse_fbpixel_param($('.ty-product-block__price-actual .ty-price-num'), "price"),
            'quantity': $('.cm-amount').val()
        });
        ga('ecommerce:send');

        gtag_report_conversion();
    });

//////////////////////////// new ///////////////////////////
    var user_id = $('#store_user').val();

//Remove from cart - click
    $(document).on('click', '.ty-twishlist-item__remove', function (e) {
        gjdmp.tr("RemoveFromCart", {
            id: $(this).attr('href').split('product_id=').pop().split('&')[0],
            name: $(this).closest('tr').find('td:eq(1)').find('a').text(),
            value: parseFloat($(this).closest('tr').find('td:eq(4)').find('span.price').text().replace(',', '')),
            quantity: $(this).closest('tr').find('td:eq(3)').find('.ty-value-changer__input').val(),
            user_id: user_id,
            currency: CURRENCY
        });

        // e.preventDefault();
    });

//Remove from cart dropdown - click
    $(document).on('click', '.cm-cart-item-delete a', function(e) {
        gjdmp.tr("RemoveFromCart", {
            id: $(this).closest('.ty-cart-items__list-item').find('.ty-cart-items__list-item-pid').val(),
            name: $(this).closest('.ty-cart-items__list-item').find('.ty-cart-items__list-item-desc a').text(),
            value: parseFloat($(this).closest('.ty-cart-items__list-item').find('.ty-cart-items__list-item-desc p span:nth-child(3)').text().replace(',', '')),
            quantity: $(this).closest('.ty-cart-items__list-item').find('.ty-cart-items__list-item-desc p span:first-child').text(),
            user_id: user_id,
            currency: CURRENCY
        });
    });

//Wishlist product added to cart - click
    $(document).on('click', '.wishlist-quick-view .ty-btn__add-to-cart', function (e) {
        gjdmp.tr("AddToCart", {
            method: 'wishlist',
            id: $(this).attr('name').split('..').pop().split(']')[0],
            name: $(this).closest('.wishlist-single-container').find('.ty-product-list__item-name a').text(),
            value: parseFloat($(this).closest('.wishlist-single-container').find('.ty-product-list__price .ty-price .ty-price-num').text()),
            user_id: user_id,
            type: 'product',
            quantity: 1,
            currency: CURRENCY
        });

        // e.preventDefault();
    });

//Continue shopping - click
    $(document).on('click', 'body .cm-notification-close', function (e) {
        if ($(this).text() == 'Kthehu tek produktet') {
            gjdmp.tr("Continue-Shopping", {
                id: 1,
                user_id: user_id
            });
        }

        // e.preventDefault();
    });

//Cart page
    if ((window.location.href).includes('checkout-cart')) {
        // let products = {};
        //
        // $.each($('.cart-item'), function (i, v) {
        //     products[i + '_name'] = $(v).find('.ty-cart-content__description a').text();
        //     products[i + '_price'] = parseFloat($(v).find('.ty-cart-content__price .price').text());
        // });

        gjdmp.tr("Checkout", {
            step: 'cart',
            id: 0,
            // order_price: $('#sec_cart_total').text(),
            // number_of_items: $('.ty-cart-content__product-title').length,
            user_id: user_id,
            // products: products,
            // discount: $('.discount-price span').text(),
            // promotion_code: $('#applied_promotions ul li').text()
        })
    }

//Checkout page
    if ((window.location.href).includes('checkout-checkout')) {
        // let products = {};
        //
        // $.each($('.ty-order-products__item'), function (i, v) {
        //     products[i + '_name'] = $(v).find('.ty-order-products__a').text();
        //     products[i + '_price'] = parseFloat($(v).find('.ty-order-products__price span').text());
        // });

        gjdmp.tr("Checkout", {
            step: 'checkout',
            id: 0,
            // order_price: $('.ty-checkout-summary__total-sum span').text(),
            // number_of_items: $('.ty-order-products__item').length,
            user_id: user_id,
            // products: products,
            // discount: $('.discount-price span span').text(),
            // promotion_code: $('#applied_promotions ul li').text()
        })
    }

//Promotion click
    $(document).on('click', '.banner__link', function(e) {
        gjdmp.tr("Click", {
            id: 0,
            type: 'Promotion',
            user_id: user_id,
            name: $(this).find('img').attr('title'),
            position: $(this).closest('.owl-item').index()
        });

        // e.preventDefault();
    });

//Product view modal - click
    $(document).on('click', '.ty-quick-view-button a', function(e) {
        gjdmp.tr("ViewContent", {
            type: 'Product',
            method: 'modal',
            id: $(this).closest('.ty-grid-list__item').find('input[name$="[product_id]"]').val(),
            user_id: user_id,
            name: $(this).closest('.ty-grid-list__item').find('.product-title').text(),
            value: parseFloat($(this).closest('.ty-grid-list__item').find('.ty-price-num').text()),
            position: $(this).closest('.ty-grid-list__item').find('input[name="position"]').val()
        });

        // e.preventDefault();
    });

    if (document.title === 'Faqja nuk u gjet (404)') {
        gjdmp.tr("404-error", {
            id: 0,
            user_id: user_id,
            url: window.location.href
        });
    }

    $(document).on('click', '.footer-share-icons a, .share-buttons-wrapper a', function(e) {
        gjdmp.tr("SocialMediaClick", {
            id: 0,
            user_id: user_id,
            name: $(this).attr('href').split('www.').pop().split('.com')[0]
        });
    });

//View Content
    if ($(".ty-product-bigpicture").length || $('.ty-quick-view__wrapper').length) {

        var viewcontentrandnum = 1 + Math.floor(Math.random() * 1000000)

        fbq('track', 'ViewContent', {
            content_ids: parse_fbpixel_param($(".product_id").val(), "product_id"),
            content_type: 'product',
            // value: parse_fbpixel_param($(".ty-price").first(), "price"),
            value: parseFloat($(".price_inner--amount").first().text().replace(/,/g, '')).toFixed(2),
            name: parse_fbpixel_param($(".ty-product-block-title"), "title"),
            currency: CURRENCY
        }, {
            eventID: 'viewcontent_' + viewcontentrandnum
        });

        sendFbConversionApiRequest('ViewContent', currentUserEmail, CURRENCY, parseFloat($(".price_inner--amount").first().text().replace(/,/g, '')).toFixed(2), 'viewcontent_' + viewcontentrandnum);

        gjdmp.tr("ViewContent", {
            id: parse_fbpixel_param($(".product_id").val(), "product_id"),
            user_id: user_id,
            name: parse_fbpixel_param($(".ty-product-block-title"), "title"),
            type: 'product',
            method: 'full-page',
            value: parse_fbpixel_param($(".price_inner--amount").first(), "price"),
            currency: CURRENCY
        });

        if ($('.ty-grid-list__item').length) {
            for (var i = 0; i < $('.ty-grid-list__item').length; i++) {
                var $this = $('.ty-grid-list__item').eq(i);
                ga('ec:addImpression', {
                    'id': $('.ty-quick-view-button > a', $this).data('ca-view-id'),
                    'name': parse_fbpixel_param($('.product-title', $this), "title"),
                    'list': list,
                    'position': i
                });

            }
        }

        ga('ec:addProduct', {
            'id': parse_fbpixel_param($('.product_id').val(), "product_id"),
            'name': parse_fbpixel_param($('.ty-product-block-title'), "title"),
            'category': $('.ty-breadcrumbs__current').text()
        });

        ga('ec:setAction', 'detail');
    }

//Checkout / Vazhdo te pagesa
    $(document).on('click', '.__checkflex_proceed', function () {
        var contents = [];
        var total_items = $('.ty-cart-content tr').length - 1;
        var promo_code = $('#applied_promotions li').text().trim();
        var content_ids = [];
        var gjflex = ($("input[id^='gjflexyes_']:checked").length == 0) ? false : true;

        for (var i = 0; i < total_items; i++) {
            contents.push({
                "id": $('#cart_items .product_id').eq(i).val(),
                "quantity": $('.ty-value-changer__input').eq(i).val(),
                "item_price": parse_fbpixel_param($('[id^="sec_product_subtotal_"]').eq(i), "price")
            });
            content_ids.push(parseInt($('#cart_items .product_id').eq(i).val()));
        }

        fbq('track', 'InitiateCheckout', {
            value: parse_fbpixel_param($('.ty-price'), "price"),
            currency: CURRENCY,
            content_type: 'product',
            content_name: 'Initiated Checkout',
            num_items: total_items,
            promo_code: promo_code,
            content_ids: content_ids,
            gjflex: gjflex
        });
    });

//Add to Wishlist
    $(document).on('click', '.ty-add-to-wish, .ty-btn__add-to-wishlist', function () {
        let id = parse_fbpixel_param($(this).data('ca-dispatch'), "product_id");
        let name = $(this).closest('.ty-grid-list__item').find('.product-title').text();
        let value = parseFloat($(this).closest('.ty-grid-list__item').find('.ty-price-num').text().replace(',', ''));

        if (id == null) {
            id = parse_fbpixel_param($(this).attr('name'), "product_id")
        }

        if (name === '') {
            name = $('.ty-product-block-title').text();
        }

        if (!value) {
            value = parseFloat($('.price_inner--amount').text().replace(',', ''));
        }

        if (isNaN(value)) {
            value = parseFloat($('.ty-product-block__price-actual .ty-price span:first').text().replace(',', ''));
        }

        fbq('track', 'AddToWishlist', {
            value: value,
            currency: CURRENCY,
            content_name: name,
            content_type: 'product',
            content_ids: id,
        });

        gjdmp.tr("AddToWishlist", {
            id: id,
            name: name,
            type: 'product',
            value: value,
            currency: CURRENCY
        });


        // gtag('event', 'add_to_wishlist', {
        //     "content_type": "product",
        //     "value": $(".ty-price").first().text().replace('€', ''),
        //     "currency": 'EUR',
        //     "items": [
        //         {
        //             "id": $(".product_id").val(),
        //             "name": name.replace('ë', 'e'),
        //             "quantity": 1,
        //             "price": $(".ty-price").first().text().replace('€', ''),
        //         }
        //     ]
        // });
    });


    $(document).on('click', '.ui-widget-overlay', function () {
        $(".ui-dialog-content").dialog("close");
    });


//Purchase
    $(document).on('click', '.cm-checkout-place-order', function () {
        var value = parseFloat($(".ty-checkout-summary__total-sum span").text().replace(/,/g, '')).toFixed(2);
        var orders = [];
        var product_ids = [];
        var num_items = $('.ty-checkout-summary__item').text().substr(0, 1);

        $('.order-product-list li').each(function () {// id of ul
            var li = $(this).find('a')//get each a in li
            var id = $('.product_id');
            var row1 = [];
            var row = [];

            row.push(li.text());
            orders.push(row);

            row1.push(id.text());
            product_ids.push(row1);


        });

        var orders_result = [];
        for (i = 0; i < orders.length; i++) {
            orders_result = orders_result.concat(orders[i]);
        }

        var product_id_result = [];
        for (i = 0; i < product_ids.length; i++) {
            product_id_result = product_id_result.concat(product_ids[i]);
        }

        var purchaserandnum = 1 + Math.floor(Math.random() * 1000000)

        fbq('track', 'Purchase', {
            value: value,
            currency: CURRENCY,
            content_ids: product_id_result,
            content_name: orders_result.toString().replace(/ë/g, 'e').replace(/ç/g, 'c'),
            content_type: 'product',
            num_items: num_items
        }, {
            eventID: 'purchase_' + purchaserandnum
        });

        sendFbConversionApiRequest('Purchase', currentUserEmail, CURRENCY, value, 'purchase_' + purchaserandnum);

        gjdmp.tr("Checkout", {
            step: 'payment',
            id: 0,
            user_id: $('#store_user').val(),
        });
    });

    $(document).on('click', '.view_gallery', function(e) {
        let name = $(this).closest('.ty-grid-list__item').find('.product-title').text();

        if (name === '') {
            name = $('.ty-product-block-title').text();
        }

        gjdmp.tr("MoreImages", {
            id: $(this).closest('.ty-grid-list__item').find('input[name$="[product_id]"]').val(),
            user_id: $('#store_user').val(),
            name: name,
            type: 'product',
            currency: CURRENCY,
        });
    });

//Search
    if (getUrlParameter('dispatch') == 'products.search' && getUrlParameter('q') != "") {
        fbq('track', 'Search', {
            value: $('#store_user').val(),
            search_string: getUrlParameter('q'),
            content_category: 'Product Search',
            currency: CURRENCY
        });

        // gtag('event', 'search', {
        //     "search_term": search.substr(search.lastIndexOf('q=') + 2, search.lastIndexOf('&') - 28).replace(/\+/g, ' ')
        // });
    }


//Promotional Code
    if (document.querySelector('[name="coupon_code_form"]')) {
        document.querySelector('[name="coupon_code_form"]').addEventListener('submit', function () {
            $val = $("#coupon_field", this).val();
            fbq('trackCustom', 'Promotional Code', {
                promo_code: $val
            })
        });
    }

    if ($('#landing_id').length) {
        fbq('trackCustom', 'View Offer', {
            content_name: $('#landing_id').val()
        });
    }

//Steps
    if ($('.checkout-steps').length) {
        $(document).ajaxSuccess(function () {

            var val = $(".ty-step__title-active span").text();
            val.trim().split(' ');
            var step_text = $(".ty-step__title-active .ty-step__title-txt").text();
            var payment_id = $('input[name=payment_id]:checked').val();

            if (val[0] == 1) {
                fbq('trackCustom', 'Checkout Step', {
                    Checkout_Step: step_text,
                    // Logged_in_as: $("#relogin-link strong").text(),
                });

                ga('ec:setAction', 'checkout', {
                    'step': 1
                });
            }
            else if (val[0] == 2) {
                fbq('trackCustom', 'Checkout Step', {
                    Checkout_Step: step_text,
                });
                ga('ec:setAction', 'checkout', {
                    'step': 2
                });
            }
            else if (val[0] == 3) {
                fbq('trackCustom', 'Checkout Step', {
                    Checkout_Step: step_text,
                    Transport_method: $('.ty-shipping-options__method label').text().replace(/ë/g, 'e'),
                });
                ga('ec:setAction', 'checkout', {
                    'step': 3
                });
            }
            else if (val[0] == 4) {
                fbq('trackCustom', 'Checkout Step', {
                    Checkout_Step: step_text,
                    Invoice_option: payment_id,
                });
                ga('ec:setAction', 'checkout', {
                    'step': 4
                });
            }
        });
    }
}

$(document).on('click', '.product-delivery-time', function () {
    fbq('trackCustom', 'Estimated delivery time', {});

    ga('send', {
        hitType: 'event',
        eventCategory: 'UX',
        eventAction: 'click',
        eventLabel: 'Estimated delivery time'
    });
});

// $(document).on('click', '.one-click-buy-order', function () {
//     // Track event
//     fbq('track', 'Purchase', {
//         value: parseFloat($("#cr_price span").text().replace(/,/g, '')).toFixed(2),
//         currency: CURRENCY,
//         content_ids: [$("[name='cr_product_data[product_id]']").val()],
//         content_name: $('.ty-cr-product-info-header h4').text(),
//         content_type: 'product',
//         num_items: parseInt($('#cr_amount').text())
//     });
// });

// Page view
gtag('event', 'conversion', {'send_to': 'AW-622454293/qjruCIue59sBEJXM56gC'});

function gtag_report_conversion(url) {
    var callback = function () {
        if (typeof(url) != 'undefined') {
            window.location = url;
        }
    };
    gtag('event', 'conversion', {
        'send_to': 'AW-622454293/Mt2VCKfi19sBEJXM56gC',
        'event_callback': callback
    });
    return false;
}

function sendFbConversionApiRequest(eventName, email, currency, value, eventId) {
    var request = {
        data: [
            {
                event_name: eventName,
                event_id: eventId,
                event_time: Math.round(new Date() / 1000),
                action_source: 'website',
                event_source_url: window.location.href,
                user_data: {
                    em: sha256(email),
                    ph: null,
                    client_user_agent: navigator.userAgent
                },
                custom_data: {
                    currency: currency,
                    value: value
                }
            }
        ]
    };

    $.ajax({
        url: 'https://graph.facebook.com/v11.0/' + pixel_id + '/events?access_token=' + fbGraphApiAccessToken,
        data: JSON.stringify(request),
        dataType: 'JSON',
        contentType: 'application/json',
        type: 'POST',
        success: function(response) {}
    });
}