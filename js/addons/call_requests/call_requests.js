(function(_, $){
    // $.ceEvent('on', 'ce.commoninit', function(context) {
    //
    //     var time_elements = context.find('.cm-cr-mask-time'),
    //         phone_elements = context.find('.cm-cr-mask-phone');
    //
    //     if (time_elements.length === 0 && phone_elements.length === 0) {
    //         return true;
    //     }
    //
    //     time_elements.mask('99:99');
    //
    //     if (phone_elements.length && _.call_requests_phone_masks_list) {
    //         var maskList = $.masksSort(_.call_requests_phone_masks_list, ['#'], /[0-9]|#/, "mask");
    //         var maskOpts = {
    //             inputmask: {
    //                 definitions: {
    //                     '#': {
    //                         validator: "[0-9]",
    //                         cardinality: 1
    //                     }
    //                 },
    //                 showMaskOnHover: false,
    //                 autoUnmask: false
    //             },
    //             match: /[0-9]/,
    //             replace: '#',
    //             list: maskList,
    //             listKey: "mask"
    //         };
    //
    //         phone_elements.each(function() {
    //             if (_.call_phone_mask) {
    //                 $(this).inputmask({
    //                     mask: _.call_phone_mask,
    //                     showMaskOnHover: false,
    //                     autoUnmask: false
    //                 });
    //
    //             } else {
    //                 $(this).inputmasks(maskOpts);
    //             }
    //         });
    //     }
    //
    //     if (_.call_phone_mask) {
    //         $.ceFormValidator('registerValidator', {
    //             class_name: 'cm-cr-mask-phone-lbl',
    //             message: _.tr('call_requests.error_validator_phone'),
    //             func: function(id) {
    //                 var input = $('#' + id);
    //
    //                 if (!$.is.blank(input.val())) {
    //                     return input.inputmask("isComplete");
    //                 } else {
    //                     return true;
    //                 }
    //             }
    //         });
    //     }
    // });

    $.ceEvent('on', 'ce.formpre_call_requests_form', function(form, elm) {

        var val_name = form.find('[name="cr_product_data[name]"]').val().replace(/(^\s+|\s+$)/g,''),
            val_lastname = form.find('[name="cr_product_data[lastname]"]').val().replace(/(^\s+|\s+$)/g,''),
            val_phone = form.find('[name="cr_product_data[phone]"]').val().replace(/(^\s+|\s+$)/g,''),
            val_phone = val_phone.replace(/\s+/g, ''),
            val_email = validateEmail(form.find('[name="cr_product_data[email]"]').val().replace(/(^\s+|\s+$)/g,'')),
            val_address = form.find('[name="cr_product_data[address]"]').val().replace(/(^\s+|\s+$)/g,''),
            val_company = form.find('[name="cr_product_data[fields][43]"]').val().replace(/(^\s+|\s+$)/g,''),
            val_fiscal = form.find('[name="cr_product_data[fields][51]"]').val().replace(/(^\s+|\s+$)/g,''),
            val_terms = form.find('[name="cr_product_data[terms]"]').is(':checked');

        var allow = !!(val_name && val_lastname && val_phone && val_email && val_address && val_terms);

        if (document.getElementById('test1').classList.contains('cm-required') && document.getElementById('test2').classList.contains('cm-required')) {
            allow = !!(val_name && val_lastname && val_phone && val_email && val_address && val_company && val_fiscal && val_terms);
        }

        var error_box = form.find('.cm-cr-error-box');
        var dlg = $.ceDialog('get_last');

        error_box.toggle(!allow);

        if (!validatePhone(val_phone)) {
            $('#spnPhoneStatus').html('<b>Telefoni</b> duhet të përbajë minimum 9 numra.');
            $('#spnPhoneStatus').css('color', '#b94a48');
            allow = false;
        } else {
            $('#spnPhoneStatus').html('');
        }
        dlg.ceDialog('reload');

        if (allow) {
            var product_data = $('[name="' + form.data('caProductForm') + '"]').serializeObject();

            $.each(product_data, function(key, value){
                if (key.match(/product_data/)) {
                    form.append('<input type="hidden" name="' + key + '" value="' + value + '" />');
                }
            });

            $('#spnPhoneStatus').html('');
            $('#ajax_overlay').show();
            $('#ajax_loading_box').show();
        }

        return allow;
    });


})(Tygh, Tygh.$);

function validatePhone(txtPhone) {

    var filter = /^[0-9-+]+$/;
    if(txtPhone.length < 9){
        return false;
    }
    if (filter.test(txtPhone)) {
        return true;
    }
    else {
        return false;
    }
}

function validateEmail(email) {
    // if (/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(email)) {
    //     return true;
    // }

    return !!$.is.email(email);
}