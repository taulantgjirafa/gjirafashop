(function(_, $) {

    function split( val )
    {
        return val.split( /,\s*/ );
    }
    
    function extractLast(term)
    {
        return split(term).pop();
    }

    function init_search_tags(object_name, o_id, o_type, company_id)
    {
        $('#my_search_tags').tagit({
            allowSpaces: true,
            placeholderText: _.tr('sd_order_user_tags.add_a_tag'),
            allowDuplicates: true,
            fieldName: object_name.val(),
            autocomplete: {
                source: function( request, response ) {
                    var _company_id = 0;
                    if (company_id.length) {
                        _company_id = company_id.val();
                    }
                    var params = {
                        data: {
                            tag: extractLast(request.term),
                            object_type: o_type.val(),
                            company_id: _company_id
                        },
                        callback: function(data) {
                            response(data.autocomplete);
                        }
                    };
                    $.ceAjax('request', fn_url('search_tags.list'), params);
                }
            },
            afterTagAdded: function(event, ui) {
                if (!ui.duringInitialization) {
                    var params = {
                        method: 'post',
                        data: {
                            tag: ui.tagLabel,
                            object_id: o_id.val(),
                            type: o_type.val(),
                            company_id: company_id.val()
                        },
                        callback: function(data) {
                            var tag = data.tag_name.toString().toLowerCase();
                            if (tag.indexOf(ui.tagLabel.toLowerCase()) == -1) {
                                ui.tag.remove();
                            }
                        }
                    };
                    if (o_id.val() != 0) {
                        $.ceAjax('request', fn_url('search_tags.update_link'), params);
                    }
                }
            },
            beforeTagRemoved: function(event, ui) {
                var params = {
                    method: 'post',
                    data: {
                        tag: ui.tagLabel,
                        object_id: o_id.val(),
                        object_type: o_type.val()
                    }
                };

                ui.tag.remove();
                if (o_id != 0) {
                    $.ceAjax('request', fn_url('search_tags.delete_link'), params);
                }

                return false;
            }
        });
    }

    $(document).ready(function(){
        if (!('tagit' in $.fn)) {
            $.getScript('js/addons/sd_order_user_tags/lib/tag-it/tag-it.js', function() {
                init_search_tags($("#object_name"), $("#object_id"), $("#object_type"), $("#company_id"));
            });
        } else {
            init_search_tags($("#object_name"), $("#object_id"), $("#object_type"), $("#company_id"));
        }
    });
}(Tygh, Tygh.$));
