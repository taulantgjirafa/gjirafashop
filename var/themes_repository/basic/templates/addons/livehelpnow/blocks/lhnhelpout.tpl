{** block-description:lhnhelpout **}
{assign var="lhncustom1" value=""}
{assign var="lhncustom2" value=""}
{assign var="lhncustom3" value=""}

{if $auth.user_id}
	{assign var="lhnuser" value=$auth.user_id|fn_get_user_info}
	{assign var="lhncustom1" value="Customer Name: `$lhnuser.firstname` `$lhnuser.lastname`"}
	{assign var="lhncustom2" value="Customer Email: `$lhnuser.email`"}
	{assign var="lhncustom3" value="Customer Cart Total: $`$smarty.session.cart.total`<br />Customer Cart Quantity: `$smarty.session.cart.amount` total items"}
{/if}

<script type="text/javascript">
//<![CDATA[
	var lhnCustom1 = encodeURIComponent('{$lhncustom1}'); 
	var lhnCustom2 = encodeURIComponent('{$lhncustom2}'); 
	var lhnCustom3 = encodeURIComponent('{$lhncustom3}');
	var lhnAccountN = '{$block.properties.lhnaccount|lower|replace:"lhn":""|replace:"-1":""}';
	var lhnInviteEnabled = (("{$block.properties.lhnautochat}" == "N") ? "0" : "1"); 
	var lhnWindowN = {$block.properties.lhnchatwindow};
	var lhnInviteN = {$block.properties.lhninvite}; 
	var lhnDepartmentN = {$block.properties.lhndepartment};
	var lhnTheme = '{$block.properties.lhntheme|replace:"lhn":""}'; 
	var lhnHPPanel = (("{$block.properties.lhnslideout}" == "Y") ? true : false);
	var lhnHPKnowledgeBase = (("{$block.properties.lhnknowbase}" == "Y") ? true : false); 
	var lhnHPMoreOptions = (("{$block.properties.lhnmoreop}" == "Y") ? true : false);
	var lhnHPChatButton = (("{$block.properties.lhnchatbutton}" == "Y") ? true : false);
	var lhnHPTicketButton = (("{$block.properties.lhnticket}" == "Y") ? true : false);
	var lhnHPCallbackButton = (("{$block.properties.lhncallback}" == "Y") ? true : false);
	var lhnLO_helpPanel_knowledgeBase_find_answers = "{$block.properties.lhnsearchtitle}";
	var lhnLO_helpPanel_knowledgeBase_please_search = "{$block.properties.lhnsearchmessage}";
	var lhnLO_helpPanel_typeahead_noResults_message = "{$block.properties.lhnnoresults}";
	var lhnLO_helpPanel_typeahead_result_views = "{$block.properties.lhnviews}";
</script>
<script src="//www.livehelpnow.net/lhn/widgets/helpouttab/lhnhelpouttab-current.min.js" type="text/javascript" id="lhnscriptho"></script>
