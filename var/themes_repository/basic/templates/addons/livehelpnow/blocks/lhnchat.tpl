{** block-description:lhnchat **}

{assign var="lhncustom1" value=""}
{assign var="lhncustom2" value=""}
{assign var="lhncustom3" value=""}

{if $auth.user_id}
	{assign var="lhnuser" value=$auth.user_id|fn_get_user_info}
	{assign var="lhncustom1" value="Customer Name: `$lhnuser.firstname` `$lhnuser.lastname`"}
	{assign var="lhncustom2" value="Customer Email: `$lhnuser.email`"}
	{assign var="lhncustom3" value="Customer Cart Total: $`$smarty.session.cart.total`<br />Customer Cart Quantity: `$smarty.session.cart.amount` total items"}
{/if}

<script type="text/javascript">
//<![CDATA[
	var lhnAccountN = "{$block.properties.lhnaccount|lower|replace:'lhn':''}";
	var lhnButtonN = {$block.properties.lhnbutton};
	var lhnInviteEnabled = (("{$block.properties.lhnautochat}" == "N") ? "0" : "1");
	var lhnWindowN = {$block.properties.lhnchatwindow};
	var lhnInviteN = {$block.properties.lhninvite};
	var lhnDepartmentN = {$block.properties.lhndepartment};
	var lhnCustom1 = encodeURIComponent('{$lhncustom1}'); 
	var lhnCustom2 = encodeURIComponent('{$lhncustom2}'); 
	var lhnCustom3 = encodeURIComponent('{$lhncustom3}');
//]]>
</script>
<a href="http://www.LiveHelpNow.net/" target="_blank" style="font-size:10px;" id="lhnHelp">Help Desk Software</a>
<script src="//www.livehelpnow.net/lhn/widgets/chatbutton/lhnchatbutton-current.min.js" type="text/javascript" id="lhnscript"></script>